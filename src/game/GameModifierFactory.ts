import { player_type } from "./component/world/CPlayerArea";

import GameObject from "./GameObject";
import CContainer from "./component/pixi/CContainer";
import CDraggable from "./component/input/CDraggable";
import CGameModifier from "./component/card/CGameModifier";
import CTargetSelector, { target_selection_type } from "./component/input/CTargetSelector";
import CTargetReceptor from "./component/world/CTargetReceptor";
import CHighlightPoiReceptor from "./component/world/poi_receptor/CHighlightPoiReceptor";
import { IRemoteCard } from "../service/socket_io/GameSocketIOController";
import ServiceLocator from "../ServiceLocator";
import { ICard } from "./CardDB";


export default class GameModifierFactory
{
    public create( card: GameObject ): GameObject
    {
        let gameModifier: GameObject = new GameObject( [ new CContainer(), new CGameModifier(), new CDraggable(), new CTargetSelector(), new CTargetReceptor(), new CHighlightPoiReceptor() ] );
        gameModifier.oid = card.oid + "_game_modifier";
        gameModifier.cGameModifier.frontCardId = card.cCard.front.cardId;
        gameModifier.cGameModifier.backCardId = card.cCard.back.cardId;
        gameModifier.cGameModifier.ownerPlayer = card.cCard.ownerPlayer;
        gameModifier.cGameModifier.controllerPlayer = card.cCard.controllerPlayer;
        gameModifier.cGameModifier.isFaceUp = true;
        gameModifier.cTargetSelector.receptor = gameModifier;
        gameModifier.cTargetReceptor.validTargetSelections = [ target_selection_type.UNDERNEATH ];
        gameModifier.init();
        //
        gameModifier.cGameModifier.setEnabled( card.cCard.controllerPlayer == player_type.PLAYER );

        return gameModifier;
    }

    public createFromRemoteCard( remoteCard: IRemoteCard, ownerPlayer: player_type, controllerPlayer: player_type ): GameObject
    {
        let gameModifier: GameObject = new GameObject( [ new CContainer(), new CGameModifier(), new CDraggable(), new CTargetSelector(), new CTargetReceptor(), new CHighlightPoiReceptor() ] );
        gameModifier.oid = remoteCard.oid + "_game_modifier";
        gameModifier.cGameModifier.frontCardId = remoteCard.cid;
        const kCardInfo: ICard = ServiceLocator.cardDb.find( remoteCard.cid );
        gameModifier.cGameModifier.backCardId = kCardInfo.side;
        gameModifier.cGameModifier.ownerPlayer = ownerPlayer;
        gameModifier.cGameModifier.controllerPlayer = controllerPlayer;
        gameModifier.cGameModifier.isFaceUp = true;
        gameModifier.cTargetSelector.receptor = gameModifier;
        gameModifier.cTargetReceptor.validTargetSelections = [ target_selection_type.UNDERNEATH ];
        gameModifier.init();
        //
        gameModifier.cGameModifier.setEnabled( controllerPlayer == player_type.PLAYER );

        return gameModifier;
    }
}