import Component from "../Component";

import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../ServiceLocator";
import * as PIXI from "pixi.js";

import Signal from "../../../lib/signals/Signal";
import IGameObjectDropArea from "../../IGameObjectDropArea";
import GameObject from "../../GameObject";
import IDragShadowTextureCreator from "../../IDragShadowTextureCreator";
import { TweenMax, Circ } from "gsap";
import CContainer from "../pixi/CContainer";
import Utils from "../../../Utils";
import { ISgTokenCounter } from "../../../view/game/SaveGameView";
import { input_event_type } from "../../InputController";


export default class CTokenCounter extends Component implements IGameObjectDropArea, IDragShadowTextureCreator
{
    // #region Attributes //

    // private:

    private _draggedTexture: PIXI.Texture = null;

    private _tokenName: string = null;
    private _minCount: number = null;
    private _maxCount: number = null;
    private _count: number = 0;
    
    private _playerActionType: player_action_type = null;
    private _isOffline: boolean = null;

    // Signals.
    private _onCountUpdated: Signal  = new Signal();
    private _onPlayerInputDetected: Signal  = new Signal();

    // #endregion //


    // #region Properties //

    public get tokenName(): string { return this._tokenName; }
    public get minCount(): number { return this._minCount; }
    public get count(): number { return this._count; }

    public set draggedTexture( value: PIXI.Texture ) { this._draggedTexture = value; }
    public set tokenName( value: string ) { this._tokenName = value; }
    public set minCount( value: number ) { this._minCount = value; }
    public set maxCount( value: number ) { this._maxCount = value; }
    public set count( value: number ) { this._count = value; }
    public set isOffline( value: boolean ) { this._isOffline = value; }
    
    // Signals.
    public get onCountUpdated(): Signal { return this._onCountUpdated; }
    public get onPlayerInputDetected(): Signal { return this._onPlayerInputDetected; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CTokenCounter";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CTokenCounter.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cDraggable != null, "CTokenCounter.ts :: init() :: CDraggable component not found." );
        console.assert( this._draggedTexture != null, "CTokenCounter.ts :: init() :: this._draggedTexture cannot be null." );

        this._isOffline = false;

        this._go.cContainer.c.interactive = true;
        this._go.cContainer.c.buttonMode = true;

        this._go.cDraggable.dragShadowTextureCreator = this;
        ServiceLocator.game.inputController.on( this._go.cContainer.c, input_event_type.TAP, this.onToken_Tapped, this );
        this._go.cDraggable.onDragged.add( this.onToken_Dragged, this );

        this._playerActionType = player_action_type.ADD_TO_TOKEN_COUNTER;
    }

    public end(): void
    {
        this._onCountUpdated.removeAll();
        this._onPlayerInputDetected.removeAll();

        ServiceLocator.game.inputController.off( this._go.cContainer.c, input_event_type.TAP, this.onToken_Tapped, this );
        this._go.cDraggable.onDragged.remove( this.onToken_Dragged, this );
        //this._go.cDraggable.onPostDropped.remove( this.onToken_PostDropped, this );

        super.end();
    }

    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        this._go.cDraggable.setEnabled( isEnabled );
        if ( this._go.cDropArea )
        {
            this._go.cDropArea.setEnabled( isEnabled );
        }
        this._go.cContainer.c.buttonMode = isEnabled;
        this._go.cContainer.c.interactive = isEnabled;
    }
    
    public setCount( count: number, actionScopeType: action_scope_type ): void
    {
        if ( this.isValidCount( count ) )
        {
            const kOldCount: number = this._count;
            this._count = count;
            this._onCountUpdated.dispatch( count, count - kOldCount, false, this._isOffline ? action_scope_type.LOCAL : actionScopeType );

            // Multiplayer.
            if ( !this._isOffline && actionScopeType == action_scope_type.MULTIPLAYER )
            {
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_TOKEN_COUNTER, null, [ this._go.oid, count, false ] );
            }
        }
    }

    public addCount( count: number, actionScopeType: action_scope_type ): void
    {
        let delta: number = null;
        let tempCount: number = null;
        if ( this._count == null )
        {
            delta = 0;
            tempCount = 0;
        }
        else
        {
            delta = count;
            tempCount = this._count + delta;
        }
        if ( this.isValidCount( tempCount ) )
        {
            this._count = tempCount;
            this._onCountUpdated.dispatch( this._count, delta, false, this._isOffline ? action_scope_type.LOCAL : actionScopeType );

            // Multiplayer.
            if ( !this._isOffline && actionScopeType == action_scope_type.MULTIPLAYER )
            {
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ADD_TO_TOKEN_COUNTER, null, [ this._go.oid, count ] );
            }
        }
    }

    public setPlayerActionType( playerActionType: player_action_type ): void 
    { 
        this._playerActionType = playerActionType; 
    }

    public static animate( container: PIXI.Container, delta: number ): void
    {
        if ( !ServiceLocator.game.isAnimated ) { return; }

        if ( container.worldVisible )
        {
            Utils.anim.flushTweensOf( container.scale );
            TweenMax.to( container.scale, 0.15, { x: "+=0.15", y: "+=0.15", yoyo: true, repeat: 1 } );
            
            if ( delta != 0 )
            {
                let gameLayer: GameObject = Utils.game.findBranchGameLayer( container );
                if ( gameLayer )
                {
                    let textWrapper: GameObject = new GameObject( [ new CContainer() ] );
                    textWrapper.init();
                    textWrapper.cContainer.c.position.copyFrom( container.getGlobalPosition() );
                    let text: PIXI.Text = new PIXI.Text( ( delta > 0 ? "+" : "-" ) + Math.abs( delta ).toString(), ServiceLocator.game.textStyler.tokenCounterAnim );
                    if ( delta > 0 )
                    {
                        text.tint = 0x00ff00;

                        // Sfx.
                        ServiceLocator.audioManager.playSfx( "counter_up" );
                    }
                    else if ( delta < 0 )
                    {
                        text.tint = 0xff0000;

                        // Sfx.
                        ServiceLocator.audioManager.playSfx( "counter_down" );
                    }
                    text.anchor.set( 0.5, 1 );
                    textWrapper.cContainer.c.addChild( text );
                    gameLayer.cContainer.addChild( textWrapper );
                    TweenMax.to( textWrapper.cContainer.c, 1, { y: "-=20", ease: Circ.easeOut, callbackScope: this, onComplete: () => { textWrapper.end(); } } );
                }
            }
        }
    }

    public saveGame(): ISgTokenCounter
    {
        return {
            minCount: this._minCount,
            maxCount: this._maxCount,
            count: this._count };
    }

    public loadGame( sgTokenCounter: ISgTokenCounter, pass: number ): void
    {
        this._minCount = sgTokenCounter.minCount;
        this._maxCount = sgTokenCounter.maxCount;
        this.setCount( sgTokenCounter.count, action_scope_type.LOCAL );
    }
    
    // private:

    private isValidCount( count: number ): boolean
    {
        return ( this._minCount == null || count >= this._minCount )
            &&( this._maxCount == null || count <= this._maxCount );
    }

    // #endregion //


    // #region Multiplayer //

    public forceDragOut(): void
    {
        const kTempCount: number = this._count == null ? 0 : this._count - 1;
        if ( this.isValidCount( kTempCount ) )
        {
            this._count = kTempCount;
            this._onCountUpdated.dispatch( this._count, -1, false, action_scope_type.LOCAL );
        }
    }

    // #endregion //


    // #region IGameObjectDropArea //

    public validateDroppedGameObject( dropped: GameObject ): boolean
    {
        return dropped.cTokenCounter && dropped.cTokenCounter.tokenName == this._tokenName;
    }

    public processDroppedGameObject( dropped: GameObject, global: PIXI.IPoint, actionScopeType: action_scope_type ): void
    {
        const kTempCount: number = this._count == null ? 0 : this._count + 1;
        if ( this.isValidCount( kTempCount ) )
        {
            this._count = kTempCount;
            this._onCountUpdated.dispatch( this._count, 1, global != null, this._isOffline ? action_scope_type.LOCAL : actionScopeType );
            this._onPlayerInputDetected.dispatch();

            // Multiplayer.
            if ( !this._isOffline && actionScopeType == action_scope_type.MULTIPLAYER && this._playerActionType != null )
            {
                ServiceLocator.socketIOManager.game.notifyAction( this._playerActionType, null, [ this._go.oid, 1, global != null ] );
            }
        }
    }

    // #endregion //


    // #region IDragShadowTextureCreator //

    public createDragShadowTexture(): PIXI.Texture
    {
        return this._draggedTexture;
    }

    // #endregion //


    // #region Callbacks //

    private onToken_Tapped( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled || ServiceLocator.game.dragShadowManager.isDragging ) { return; }

        let delta: number = null;
        let tempCount: number = null;
        if ( this._count == null )
        {
            delta = 0;
            tempCount = 0;
        }
        else
        {
            delta = event.data.button == 2 ? -1 : 1;
            tempCount = this._count + delta;
        }
        if ( this.isValidCount( tempCount ) )
        {
            this._count = tempCount;
            this._onCountUpdated.dispatch( this._count, delta, true, this._isOffline ? action_scope_type.LOCAL : action_scope_type.MULTIPLAYER );
            this._onPlayerInputDetected.dispatch();

            // Multiplayer.
            if ( !this._isOffline && this._playerActionType != null )
            {
                ServiceLocator.socketIOManager.game.notifyAction( this._playerActionType, null, [ this._go.oid, event.data.button == 2 ? -1 : 1, true ] );
            }
        }

        // Sfx.
        ServiceLocator.audioManager.playSfx( "button_click" );
    }

    private onToken_Dragged( dragged: GameObject, event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled ) { return; }

        const kTempCount: number = this._count == null ? 0 : this._count - 1;
        if ( this.isValidCount( kTempCount ) )
        {
            this._count = kTempCount;
            this._onCountUpdated.dispatch( this._count, -1, true, this._isOffline ? action_scope_type.LOCAL : action_scope_type.MULTIPLAYER );
            this._onPlayerInputDetected.dispatch();

            // Multiplayer.
            if ( !this._isOffline && this._playerActionType != null )
            {
                ServiceLocator.socketIOManager.game.notifyAction( this._playerActionType, null, [ this._go.oid, -1, true ] );
            }
        }
    }

    // #endregion //
}