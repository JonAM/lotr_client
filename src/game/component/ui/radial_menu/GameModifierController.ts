import { option_type, IRadialMenuController, IOption } from "../CRadialMenu";
import { action_scope_type, player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import { player_type } from "../../world/CPlayerArea";
import ServiceLocator from "../../../../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "../../../GameObject";
import CGameModifier from "../../card/CGameModifier";
import LogTargetCard from "../right_menu/action_log/target/LogTargetCard";
import Utils from "../../../../Utils";
import { layer_type } from "../../world/CGameLayerProvider";
import { target_selection_type } from "../../input/CTargetSelector";
import { detail_bar_icon_type } from "../../card/token/CDetailBar";


export default class GameModifierController implements IRadialMenuController
{
    // #region Methods //

    // public:

    public processOption( target: GameObject, option: IOption ): void
    {
        let cGameModifier: CGameModifier = target.cGameModifier;

        switch ( option.id )
        {
            case option_type.TARGET: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( target ), "rad_target", true );
            
                cGameModifier.flash();

                target.cTargetSelector.startSelection( target_selection_type.TARGET );

                cGameModifier.cDetailBar.addToItemCount( detail_bar_icon_type.ABILITY_USES, 1 );

                // Multiplayer.
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.USE_GAME_MODIFIER, null, [ target.oid ] );
                break;
            }

            case option_type.BOW: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( target ), "rad_bow", true );

                cGameModifier.setBowed( true, action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.READY: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( target ), "rad_ready", true );
                
                cGameModifier.setBowed( false, action_scope_type.MULTIPLAYER );
                break;
            }
            case option_type.PUT_FACE_UP: 
            {
                cGameModifier.setFaceUp( action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.PUT_FACE_DOWN: 
            {
                cGameModifier.setFaceDown( action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.EQUIP: 
            {
                target.cTargetSelector.startSelection( target_selection_type.EQUIP );
                break;
            }

            case option_type.UNDERNEATH: 
            {
                target.cTargetSelector.startSelection( target_selection_type.UNDERNEATH );
                break;
            }

            case option_type.DISCARD: 
            {
                cGameModifier.discard( action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.ADD_CUSTOM_TEXT: 
            {
                cGameModifier.cDetailBar.editCustomText();
                break;
            }

            case option_type.ADD_COUNTER: 
            {
                cGameModifier.cDetailBar.addCustomCounter();
                break;
            }

            case option_type.ADD_HIGHLIGHTING: 
            {
                cGameModifier.setHighlighted( true, action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.REMOVE_HIGHLIGHTING: 
            {
                cGameModifier.setHighlighted( false, action_scope_type.MULTIPLAYER );
                break;
            }
        }
    }

    // #endregion //
}