import { action_scope_type, player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import { target_selection_type } from "../../input/CTargetSelector";
import { detail_bar_icon_type } from "../../card/token/CDetailBar";
import { layer_type } from "../../world/CGameLayerProvider";
import { player_type } from "../../world/CPlayerArea";
import ServiceLocator from "../../../../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "../../../GameObject";
import CAttachment from "../../card/token/CAttachment";
import { IOption, IRadialMenuController, option_type } from "../CRadialMenu";
import LogTargetCard from "../right_menu/action_log/target/LogTargetCard";
import Utils from "../../../../Utils";


export default class AttachmentController implements IRadialMenuController
{
    // #region Methods //

    // public:

    public processOption( target: GameObject, option: IOption ): void
    {
        let cAttachment: CAttachment = target.cAttachment;

        switch ( option.id )
        {
            case option_type.TARGET: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( target ), "rad_target", true );
            
                //Utils.anim.bumpFromCorner( target.cContainer.c );
                let vfxLayer: GameObject = Utils.game.findGameProviderLayer( target, layer_type.VFX );
                let flashPos: PIXI.Point = new PIXI.Point().copyFrom( vfxLayer.cContainer.c.toLocal( target.cContainer.c.getGlobalPosition() ) );
                flashPos.x += target.cContainer.c.width * 0.5;
                flashPos.y += target.cContainer.c.height * 0.5;
                Utils.anim.flash( flashPos, target.cContainer.c.width, target.cContainer.c.height, 1.5, vfxLayer );

                target.cTargetSelector.startSelection( target_selection_type.TARGET );

                cAttachment.cDetailBar.addToItemCount( detail_bar_icon_type.ABILITY_USES, 1 );

                // Multiplayer.
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.USE_ATTACHMENT, null, [ target.oid ] );
                break;
            }

            case option_type.UNDERNEATH: 
            {
                target.cTargetSelector.startSelection( target_selection_type.UNDERNEATH );
                break;
            }

            case option_type.BOW: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( target ), "rad_bow", true );

                cAttachment.setBowed( true, action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.READY: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( target ), "rad_ready", true );
                
                cAttachment.setBowed( false, action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.PUT_FACE_DOWN: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logCardFlip( player_type.PLAYER, new LogTargetCard( target ), true );
                cAttachment.setFaceDown( action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.PUT_FACE_UP: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logCardFlip( player_type.PLAYER, new LogTargetCard( target ), true );
                cAttachment.setFaceUp( action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.DISCARD: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( target ), "rad_discard", true );
                
                cAttachment.discard( action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.ADD_CUSTOM_TEXT: 
            {
                cAttachment.cDetailBar.editCustomText();
                break;
            }

            case option_type.ADD_COUNTER: 
            {
                cAttachment.cDetailBar.addCustomCounter();
                break;
            }

            case option_type.ADD_HIGHLIGHTING: 
            {
                cAttachment.setHighlighted( true, action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.REMOVE_HIGHLIGHTING: 
            {
                cAttachment.setHighlighted( false, action_scope_type.MULTIPLAYER );
                break;
            }
        }
    }

    // #endregion //
}