import { option_type, IRadialMenuController, IOption } from "../CRadialMenu";
import { action_scope_type } from "../../../../service/socket_io/GameSocketIOController";
import { player_type } from "../../world/CPlayerArea";
import ServiceLocator from "../../../../ServiceLocator";

import GameObject from "../../../GameObject";
import LogTargetCard from "../right_menu/action_log/target/LogTargetCard";
import CShadowCardMini from "../../card/CShadowCardMini";


export default class ShadowCardMiniController implements IRadialMenuController
{
    // #region Methods //

    // public:

    public processOption( target: GameObject, option: IOption ): void
    {
        let cShadowCardMini: CShadowCardMini = target.cShadowCardMini;

        switch ( option.id )
        {
            case option_type.PUT_FACE_UP: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logCardFlip( player_type.PLAYER, new LogTargetCard( cShadowCardMini.card ), true );
                cShadowCardMini.setFaceUp( action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.PUT_FACE_DOWN: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logCardFlip( player_type.PLAYER, new LogTargetCard( cShadowCardMini.card ), true );
                cShadowCardMini.setFaceDown( action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.DISCARD: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( cShadowCardMini.card ), "rad_discard", true );
                cShadowCardMini.discard( action_scope_type.MULTIPLAYER );
                break;
            }
        }
    }

    // #endregion //
}