import { option_type, IRadialMenuController, IOption } from "../CRadialMenu";
import { action_scope_type, player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import { player_type } from "../../world/CPlayerArea";
import ServiceLocator from "../../../../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "../../../GameObject";
import LogTargetCard from "../right_menu/action_log/target/LogTargetCard";
import Utils from "../../../../Utils";
import CDropArea from "../../input/CDropArea";
import { target_selection_type } from "../../input/CTargetSelector";
import CCardTokenSide, { status_type } from "../../card/token/CCardTokenSide";
import { detail_bar_icon_type } from "../../card/token/CDetailBar";
import CCardToken from "../../card/CCardToken";
import { layer_type } from "../../world/CGameLayerProvider";


export default class CardTokenSideController implements IRadialMenuController
{
    // #region Methods //

    // public:

    public processOption( target: GameObject, option: IOption ): void
    {
        let cCardTokenSide: CCardTokenSide = target.cCardTokenSide;
        let cCardToken: CCardToken = cCardTokenSide.cardToken.cCardToken;

        switch ( option.id )
        {
            case option_type.TARGET: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( cCardTokenSide.cardToken ), "rad_target", true );
            
                //Utils.anim.bumpFromCorner( target.cContainer.c );
                const kVfxLayer: GameObject = Utils.game.findGameProviderLayer( target, layer_type.VFX );
                let flashPos: PIXI.Point = new PIXI.Point().copyFrom( kVfxLayer.cContainer.c.toLocal( target.cContainer.c.getGlobalPosition() ) );
                flashPos.x += cCardToken.calcWidth() * 0.5;
                flashPos.y += cCardToken.calcHeight() * 0.5;
                Utils.anim.flash( flashPos, cCardToken.calcWidth(), cCardToken.calcHeight(), 1.5, Utils.game.findGameProviderLayer( target, layer_type.VFX ) );

                target.cTargetSelector.startSelection( target_selection_type.TARGET );

                cCardToken.cDetailBar.addToItemCount( detail_bar_icon_type.ABILITY_USES, 1 );

                // Multiplayer. 
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.TRIGGER_ABILITY, null, [ target.oid ] );
                break;
            }

            case option_type.EQUIP: 
            {
                target.cTargetSelector.startSelection( target_selection_type.EQUIP );
                break;
            }

            case option_type.UNDERNEATH: 
            {
                target.cTargetSelector.startSelection( target_selection_type.UNDERNEATH );
                break;
            }

            case option_type.BOW: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( cCardTokenSide.cardToken ), "rad_bow", true );
                
                cCardToken.setBowed( true, action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.READY: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( cCardTokenSide.cardToken ), "rad_ready", true );
                
                cCardToken.setBowed( false, action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.PUT_FACE_DOWN: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logCardFlip( player_type.PLAYER, new LogTargetCard( cCardTokenSide.cardToken ), true );
                cCardToken.setFaceDown( action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.PUT_FACE_UP: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logCardFlip( player_type.PLAYER, new LogTargetCard( cCardTokenSide.cardToken ), true );
                cCardToken.setFaceUp( action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.SAURON_ATTACK: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( cCardTokenSide.cardToken ), "rad_sauron_attack", true );
                
                target.cTargetSelector.startSelection( target_selection_type.SAURON_ATTACK );

                ServiceLocator.game.attackBindingManager.removeAllAttackerBindings( target.cCardTokenSide.cardToken, action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.PLAYER_ATTACK: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( cCardTokenSide.cardToken ), "rad_player_attack", true );
                
                target.cTargetSelector.startSelection( target_selection_type.PLAYER_ATTACK );

                ServiceLocator.game.attackBindingManager.removeAllAttackerBindings( target.cCardTokenSide.cardToken, action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.COMMIT: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( cCardTokenSide.cardToken ), "rad_commit", true );
                
                if ( target.cHeroSide )
                {
                    target.cHeroSide.commitToQuest( action_scope_type.MULTIPLAYER );
                }
                else if ( target.cAllySide )
                {
                    target.cAllySide.commitToQuest( action_scope_type.MULTIPLAYER );
                }
                break;
            }

            case option_type.UNCOMMIT: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( cCardTokenSide.cardToken ), "rad_uncommit", true );
                
                if ( target.cHeroSide )
                {
                    target.cHeroSide.uncommitFromQuest( action_scope_type.MULTIPLAYER );
                }
                else if ( target.cAllySide )
                {
                    target.cAllySide.uncommitFromQuest( action_scope_type.MULTIPLAYER );
                }
                break;
            }

            case option_type.DISCARD: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( cCardTokenSide.cardToken ), "rad_discard", true );
                
                cCardToken.discard( action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.ADD_COUNTER: 
            {
                cCardToken.cDetailBar.addCustomCounter();
                break;
            }

            case option_type.ADD_CUSTOM_TEXT: 
            {
                cCardToken.cDetailBar.editCustomText();
                break;
            }

            case option_type.ADD_HIGHLIGHTING:
            {
                cCardToken.setHighlighted( true, action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.REMOVE_HIGHLIGHTING:
            {
                cCardToken.setHighlighted( false, action_scope_type.MULTIPLAYER );
                break;
            }
        }
    }

    // #endregion //
}