import { game_state_id } from "../../../../states/StateId";
import { option_type, IRadialMenuController, IOption } from "../CRadialMenu";
import { action_scope_type } from "../../../../service/socket_io/GameSocketIOController";
import { player_type } from "../../world/CPlayerArea";
import ServiceLocator from "../../../../ServiceLocator";
import { target_selection_type } from "../../input/CTargetSelector";

import GameObject from "../../../GameObject";
import LogTargetCard from "../right_menu/action_log/target/LogTargetCard";
import CActivatedCard from "../../card/CActivatedCard";


export default class ActivatedCardController implements IRadialMenuController
{
    // #region Methods //

    // public:

    public processOption( target: GameObject, option: IOption ): void
    {
        let cActivatedCard: CActivatedCard = target.cActivatedCard;

        switch ( option.id )
        {
            case option_type.TARGET: 
            {
                target.cTargetSelector.startSelection( target_selection_type.TARGET );
                break;
            }

            case option_type.EQUIP: 
            {
                target.cTargetSelector.startSelection( target_selection_type.EQUIP );
                break;
            }

            case option_type.SHADOW_CARD: 
            {
                target.cTargetSelector.startSelection( target_selection_type.SHADOW_CARD );
                break;
            }

            case option_type.UNDERNEATH: 
            {
                target.cTargetSelector.startSelection( target_selection_type.UNDERNEATH );
                break;
            }

            case option_type.DISCARD: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( cActivatedCard.card ), "rad_discard", true );
                cActivatedCard.discard( action_scope_type.MULTIPLAYER );
                break;
            }
        }
    }

    // #endregion //
}