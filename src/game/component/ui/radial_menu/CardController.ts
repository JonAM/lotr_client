import { option_type, IRadialMenuController, IOption } from "../CRadialMenu";

import ServiceLocator from "../../../../ServiceLocator";
import { player_type } from "../../world/CPlayerArea";

import CCard from "../../card/CCard";
import GameObject from "../../../GameObject";
import LogTargetCard from "../right_menu/action_log/target/LogTargetCard";
import { action_scope_type } from "../../../../service/socket_io/GameSocketIOController";


export default class CardController implements IRadialMenuController
{
    // #region Methods //

    // public:

    public processOption( target: GameObject, option: IOption ): void
    {
        let cCard: CCard = target.cCard;

        switch ( option.id )
        {
            case option_type.PUT_FACE_DOWN: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logCardFlip( player_type.PLAYER, new LogTargetCard( target ), true );
                cCard.setFaceDown( action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.PUT_FACE_UP: 
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logCardFlip( player_type.PLAYER, new LogTargetCard( target ), true );
                cCard.setFaceUp( action_scope_type.MULTIPLAYER );
                break;
            }

            case option_type.DISCARD: 
            {
                cCard.discard( action_scope_type.MULTIPLAYER );
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( target ), "rad_discard", true );
                break;
            }
        }
    }

    // #endregion //

}