import Component from "../Component";

import { game_state_id, main_state_id } from "../../../states/StateId";
import { player_action_type } from "../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../ServiceLocator";
import Url from "../../../Url";
import Utils from "../../../Utils";
import { view_layer_id } from "../../../service/ViewManager";
import * as PIXI from "pixi.js";

import CButton from "../input/CButton";
import CSprite from "../pixi/CSprite";
import GameObject from "../../GameObject";
import SaveGameView from "../../../view/game/SaveGameView";
import KeyBindingView from "../../../view/game/KeyBindingView";
import CTokenCounter from "./CTokenCounter";
import CContainer from "../pixi/CContainer";
import CDraggable from "../input/CDraggable";
import SignalBinding from "../../../lib/signals/SignalBinding";
import CTooltipReceptor from "./CTooltipReceptor";
import InfoMessageView, { btn_layout_type } from "../../../view/common/popup/InfoMessageView";
import Session from "../../../Session";
import { IScenario } from "../../ScenarioDB";
import Path from "../../../Path";
import { action_request_type } from "../../AllyActionManager";
import { sound_group_id } from "../../../service/AudioManager";
import GamePreferencesView from "../../../view/game/GamePreferencesView";


export default class COptionsMenu extends Component
{
    // #region Attributes //
    
    // private:

    private _sfxBtn: GameObject = null;
    private _sfxVolumeBtn: GameObject = null;
    private _musicBtn: GameObject = null;
    private _musicVolumeBtn: GameObject = null;
    private _gamePreferencesBtn: GameObject = null;
    private _keyBindingsBtn: GameObject = null;
    private _saveBtn: GameObject = null;
    private _restartBtn: GameObject = null;
    private _abortBtn: GameObject = null;
    private _rulesBtn: GameObject = null;
    private _specialRulesBtn: GameObject = null;
    private _helpBtn: GameObject = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "COptionsMenu";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cGraphics != null, "COptionsMenu.ts :: init() :: CGraphics component not found." );
        
        this._go.cGraphics.g.interactive = true;

        // Sfx button.
        this._sfxBtn = new GameObject( [ new CSprite(), new CButton(), new CTooltipReceptor() ] );
        this._sfxBtn.cSprite.s.anchor.set( 0.5, 0.5 );
        this._sfxBtn.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( ServiceLocator.audioManager.isMuteSfx ? "sfx_off" : "sfx_on" ).data );
        Utils.game.limitSideSize( 50, this._sfxBtn.cSprite.s );
        this._sfxBtn.cTooltipReceptor.text = jQuery.i18n( "SFX" );
        this._sfxBtn.init();
        this._sfxBtn.cContainer.c.position.set( 35, 30 )
        this._go.cContainer.addChild( this._sfxBtn );
        this._sfxBtn.cButton.onClick.add( this.onSfxBtn_Click, this );
        //
        this._sfxVolumeBtn = new GameObject( [ new CContainer(), new CTokenCounter(), new CDraggable() ] );
        this._sfxVolumeBtn.cTokenCounter.count = ServiceLocator.savedData.data.sfxVolume * 10;
        this._sfxVolumeBtn.cTokenCounter.minCount = 0;
        this._sfxVolumeBtn.cTokenCounter.maxCount = 10;
        this._sfxVolumeBtn.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "custom_counter_white" ).data );
        this._sfxVolumeBtn.init();
        this._sfxVolumeBtn.cTokenCounter.setPlayerActionType( null );
        this._sfxVolumeBtn.cContainer.c.position.set( this._sfxBtn.cContainer.c.x + 17, this._sfxBtn.cContainer.c.y + 17 );
        this._go.cContainer.addChild( this._sfxVolumeBtn );
        let icon: PIXI.Sprite = PIXI.Sprite.from( PIXI.Texture.from( ServiceLocator.resourceStack.find( "custom_counter_white" ).data ) );
        icon.anchor.set( 0.5 );
        Utils.game.limitSideSize( 25, icon );
        this._sfxVolumeBtn.cContainer.c.addChild( icon );
        // 
        let text = new PIXI.Text( ( ServiceLocator.savedData.data.sfxVolume * 10 ).toString(), ServiceLocator.game.textStyler.small );
        text.anchor.set( 0.5 );
        this._sfxVolumeBtn.cContainer.c.addChild( text );
        let sb: SignalBinding = this._sfxVolumeBtn.cTokenCounter.onCountUpdated.add( this.onSfxVolume_Updated, this );
        sb.params = [ text ];

        // Music button.
        this._musicBtn = new GameObject( [ new CSprite(), new CButton(), new CTooltipReceptor() ] );
        this._musicBtn.cSprite.s.anchor.set( 0.5, 0.5 );
        this._musicBtn.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( ServiceLocator.audioManager.isMuteMusic ? "music_off" : "music_on" ).data );
        Utils.game.limitSideSize( 50, this._musicBtn.cSprite.s );
        this._musicBtn.cTooltipReceptor.text = jQuery.i18n( "MUSIC" );
        this._musicBtn.init();
        this._musicBtn.cContainer.c.position.set( this._sfxBtn.cContainer.c.x + 50 + 10, 30 );
        this._go.cContainer.addChild( this._musicBtn );
        this._musicBtn.cButton.onClick.add( this.onMusicBtn_Click, this );
        //
        this._musicVolumeBtn = new GameObject( [ new CContainer(), new CTokenCounter(), new CDraggable() ] );
        this._musicVolumeBtn.cTokenCounter.count = ServiceLocator.savedData.data.musicVolume * 10;
        this._musicVolumeBtn.cTokenCounter.minCount = 0;
        this._musicVolumeBtn.cTokenCounter.maxCount = 10;
        this._musicVolumeBtn.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "custom_counter_white" ).data );
        this._musicVolumeBtn.init();
        this._musicVolumeBtn.cTokenCounter.setPlayerActionType( null );
        this._musicVolumeBtn.cContainer.c.position.set( this._musicBtn.cContainer.c.x + 17, this._musicBtn.cContainer.c.y + 17 );
        this._go.cContainer.addChild( this._musicVolumeBtn );
        icon = PIXI.Sprite.from( PIXI.Texture.from( ServiceLocator.resourceStack.find( "custom_counter_white" ).data ) );
        icon.anchor.set( 0.5 );
        Utils.game.limitSideSize( 25, icon );
        this._musicVolumeBtn.cContainer.c.addChild( icon );
        // 
        text = new PIXI.Text( ( ServiceLocator.savedData.data.musicVolume * 10 ).toString(), ServiceLocator.game.textStyler.small );
        text.anchor.set( 0.5 );
        this._musicVolumeBtn.cContainer.c.addChild( text );
        sb = this._musicVolumeBtn.cTokenCounter.onCountUpdated.add( this.onMusicVolume_Updated, this );
        sb.params = [ text ];

        // Game preferences button.
        this._gamePreferencesBtn = new GameObject( [ new CSprite(), new CButton(), new CTooltipReceptor() ] );
        this._gamePreferencesBtn.cSprite.s.anchor.set( 0.5, 0.5 );
        this._gamePreferencesBtn.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "game_preferences" ).data );
        Utils.game.limitSideSize( 50, this._gamePreferencesBtn.cSprite.s );
        this._gamePreferencesBtn.cTooltipReceptor.text = jQuery.i18n( "GAME_PREFERENCES" );
        this._gamePreferencesBtn.init();
        this._gamePreferencesBtn.cContainer.c.position.set( this._musicBtn.cContainer.c.x + 50 + 10 + 25, 30 );
        this._go.cContainer.addChild( this._gamePreferencesBtn );
        this._gamePreferencesBtn.cButton.onClick.add( this.onGamePreferencesBtn_Click, this );

        // Key bindings button.
        this._keyBindingsBtn = new GameObject( [ new CSprite(), new CButton(), new CTooltipReceptor() ] );
        this._keyBindingsBtn.cSprite.s.anchor.set( 0.5, 0.5 );
        this._keyBindingsBtn.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "key_bindings" ).data );
        Utils.game.limitSideSize( 50, this._keyBindingsBtn.cSprite.s );
        this._keyBindingsBtn.cTooltipReceptor.text = jQuery.i18n( "HOT_KEYS" );
        this._keyBindingsBtn.init();
        this._keyBindingsBtn.cContainer.c.position.set( this._gamePreferencesBtn.cContainer.c.x + 50 + 10, 30 );
        this._go.cContainer.addChild( this._keyBindingsBtn );
        this._keyBindingsBtn.cButton.onClick.add( this.onKeyBindingsBtn_Click, this );
        if ( ServiceLocator.game.app.renderer.plugins.interaction.supportsTouchEvents )
        {
            this._keyBindingsBtn.cButton.setEnabled( false );
            this._keyBindingsBtn.cContainer.c.buttonMode = false;
            this._keyBindingsBtn.cContainer.c.alpha = false ? 1 : 0.45;
        }

        // Save button.
        this._saveBtn = new GameObject( [ new CSprite(), new CButton(), new CTooltipReceptor() ] );
        this._saveBtn.cSprite.s.anchor.set( 0.5, 0.5 );
        this._saveBtn.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "save" ).data );
        Utils.game.limitSideSize( 50, this._saveBtn.cSprite.s );
        this._saveBtn.cTooltipReceptor.text = jQuery.i18n( "SAVE_GAME" );
        this._saveBtn.init();
        this._saveBtn.cContainer.c.position.set( this._keyBindingsBtn.cContainer.c.x + 50 + 10, 30 );
        this._go.cContainer.addChild( this._saveBtn );
        this._saveBtn.cButton.onClick.add( this.onSaveBtn_Click, this );

        // Restart button.
        this._restartBtn = new GameObject( [ new CSprite(), new CButton(), new CTooltipReceptor() ] );
        this._restartBtn.cSprite.s.anchor.set( 0.5, 0.5 );
        this._restartBtn.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "restart" ).data );
        Utils.game.limitSideSize( 50, this._restartBtn.cSprite.s );
        this._restartBtn.cTooltipReceptor.text = jQuery.i18n( "TOOLTIP_RESTART_GAME" );
        this._restartBtn.init();
        this._restartBtn.cContainer.c.position.set( this._saveBtn.cContainer.c.x + 50 + 10 + 25, 30 );
        this._go.cContainer.addChild( this._restartBtn );
        this._restartBtn.cButton.onClick.add( this.onRestartBtn_Click, this );

        // Abort button.
        this._abortBtn = new GameObject( [ new CSprite(), new CButton(), new CTooltipReceptor() ] );
        this._abortBtn.cSprite.s.anchor.set( 0.5, 0.5 );
        this._abortBtn.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "abort" ).data );
        Utils.game.limitSideSize( 50, this._abortBtn.cSprite.s );
        this._abortBtn.cTooltipReceptor.text = jQuery.i18n( "TOOLTIP_QUIT_GAME" );
        this._abortBtn.init();
        this._abortBtn.cContainer.c.position.set( this._restartBtn.cContainer.c.x + 50 + 10, 30 );
        this._go.cContainer.addChild( this._abortBtn );
        this._abortBtn.cButton.onClick.add( this.onLeaveBtn_Click, this );

        // Rules button.
        this._rulesBtn = new GameObject( [ new CSprite(), new CButton(), new CTooltipReceptor() ] );
        this._rulesBtn.cSprite.s.anchor.set( 0.5, 0.5 );
        this._rulesBtn.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "rules" ).data );
        Utils.game.limitSideSize( 50, this._rulesBtn.cSprite.s );
        this._rulesBtn.cTooltipReceptor.text = jQuery.i18n( "RULES" );
        this._rulesBtn.cTooltipReceptor.description = jQuery.i18n( "RULES_DESC" );
        this._rulesBtn.init();
        this._rulesBtn.cContainer.c.position.set( this._abortBtn.cContainer.c.x + 50 + 10 + 25, 30 );
        this._go.cContainer.addChild( this._rulesBtn );
        this._rulesBtn.cButton.onClick.add( this.onRulesBtn_Click, this );

        // Special rules button.
        let scenario: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
        if ( scenario.mec )
        {
            this._specialRulesBtn = new GameObject( [ new CSprite(), new CButton(), new CTooltipReceptor() ] );
            this._specialRulesBtn.cSprite.s.anchor.set( 0.5, 0.5 );
            this._specialRulesBtn.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "special_rules" ).data );
            Utils.game.limitSideSize( 50, this._specialRulesBtn.cSprite.s );
            this._specialRulesBtn.cTooltipReceptor.text = jQuery.i18n( "SPECIAL_RULES" );
            this._specialRulesBtn.cTooltipReceptor.description = jQuery.i18n( "SPECIAL_RULES_DESC" );
            this._specialRulesBtn.init();
            this._specialRulesBtn.cContainer.c.position.set( this._rulesBtn.cContainer.c.x + 50 + 10, 30 );
            this._go.cContainer.addChild( this._specialRulesBtn );
            let sb: SignalBinding = this._specialRulesBtn.cButton.onClick.add( this.onSpecialRulesBtn_Click, this );
            sb.params = [ scenario.mec ];
        }

        // Help button.
        this._helpBtn = new GameObject( [ new CSprite(), new CButton(), new CTooltipReceptor() ] );
        this._helpBtn.cSprite.s.anchor.set( 0.5, 0.5 );
        this._helpBtn.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "help" ).data );
        Utils.game.limitSideSize( 50, this._helpBtn.cSprite.s );
        this._helpBtn.cTooltipReceptor.text = jQuery.i18n( "HELP" );
        this._helpBtn.cTooltipReceptor.description = jQuery.i18n( "HELP_DESC" );
        this._helpBtn.init();
        this._helpBtn.cContainer.c.position.set( ( scenario.mec ? this._specialRulesBtn : this._rulesBtn ).cContainer.c.x + 50 + 10, 30 );
        this._go.cContainer.addChild( this._helpBtn );
        this._helpBtn.cButton.onClick.add( this.onHelpBtn_Click, this );

        // Bg.
        this._go.cGraphics.g.lineStyle( 1, 0x000000, 1 );
        this._go.cGraphics.g.beginFill( 0xffa502, 0.8 );
        this._go.cGraphics.g.drawRoundedRect( 0, 0, this._helpBtn.cContainer.c.x + 35, 50 + 10, 10 ); 
        this._go.cGraphics.g.endFill();
    }

    public end(): void
    {
        this._sfxVolumeBtn.end();
        this._sfxVolumeBtn = null;

        this._sfxBtn.end();
        this._sfxBtn = null;

        this._musicVolumeBtn.end();
        this._musicVolumeBtn = null;

        this._musicBtn.end();
        this._musicBtn = null;

        this._gamePreferencesBtn.end();
        this._gamePreferencesBtn = null;

        this._keyBindingsBtn.end();
        this._keyBindingsBtn = null;

        this._saveBtn.end();
        this._saveBtn = null;

        this._restartBtn.end();
        this._restartBtn = null;

        this._abortBtn.end();
        this._abortBtn = null;

        this._rulesBtn.end();
        this._rulesBtn = null;

        if ( this._specialRulesBtn )
        {
            this._specialRulesBtn.end();
            this._specialRulesBtn = null;
        }

        this._helpBtn.end();
        this._helpBtn = null;
    }

    public setSaveButtonEnabled( isEnabled: boolean ): void
    {
        this._saveBtn.cButton.setEnabled( isEnabled );
        this._saveBtn.cContainer.c.buttonMode = isEnabled;
        this._saveBtn.cContainer.c.alpha = isEnabled ? 1 : 0.45;
    }

    // #endregion //


    // #region Input Callbacks //

    private onSfxBtn_Click(): void
    {
        ServiceLocator.audioManager.mute( !ServiceLocator.audioManager.isMuteSfx, sound_group_id.SFX );

        this._sfxBtn.cSprite.s.texture = PIXI.Texture.from( 
            ServiceLocator.resourceStack.find( ServiceLocator.audioManager.isMuteSfx ? "sfx_off" : "sfx_on" ).data );
    
        ServiceLocator.savedData.data.isSfx = !ServiceLocator.audioManager.isMuteSfx;
        ServiceLocator.savedData.save();
    }

    private onSfxVolume_Updated( text: PIXI.Text, count: number, delta: number, isPlayerInput: boolean ): void
    {
        const kVolume: number = count / 10;
        ServiceLocator.audioManager.changeVolume( kVolume, sound_group_id.SFX );

        text.text = count.toString();

        // Sfx.
        ServiceLocator.audioManager.playSfx( delta > 0 ? "counter_up" : "counter_down" );

        ServiceLocator.savedData.data.sfxVolume = kVolume;
        ServiceLocator.savedData.save();
    }

    private onMusicBtn_Click(): void
    {
        ServiceLocator.audioManager.mute( !ServiceLocator.audioManager.isMuteMusic, sound_group_id.MUSIC );

        this._musicBtn.cSprite.s.texture = PIXI.Texture.from( 
            ServiceLocator.resourceStack.find( ServiceLocator.audioManager.isMuteMusic ? "music_off" : "music_on" ).data );
    
        ServiceLocator.savedData.data.isMusic = !ServiceLocator.audioManager.isMuteMusic;
        ServiceLocator.savedData.save();
    }

    private onMusicVolume_Updated( text: PIXI.Text, count: number, delta: number, isPlayerInput: boolean ): void
    {
        const kVolume: number = count / 10;
        ServiceLocator.audioManager.changeVolume( kVolume, sound_group_id.MUSIC );

        text.text = count.toString();

        // Sfx.
        ServiceLocator.audioManager.playSfx( delta > 0 ? "counter_up" : "counter_down" );

        ServiceLocator.savedData.data.musicVolume = kVolume;
        ServiceLocator.savedData.save();
    }

    private onGamePreferencesBtn_Click(): void
    {
        let gamePreferencesView: GamePreferencesView = new GamePreferencesView();
        gamePreferencesView.init();
        ServiceLocator.viewManager.fadeIn( gamePreferencesView, view_layer_id.POPUP );

        this._go.cContainer.c.visible = false;
    }

    private onKeyBindingsBtn_Click(): void
    {
        let keyBindingsView: KeyBindingView = new KeyBindingView();
        keyBindingsView.init();
        ServiceLocator.viewManager.fadeIn( keyBindingsView, view_layer_id.POPUP );

        this._go.cContainer.c.visible = false;
    }

    private onSaveBtn_Click(): void
    {
        let saveGameView: SaveGameView = new SaveGameView();
        saveGameView.viewLayerId = view_layer_id.POPUP;
        saveGameView.init();
        ServiceLocator.viewManager.fadeIn( saveGameView, view_layer_id.POPUP );
        
        this._go.cContainer.c.visible = false;
    }

    private onRestartBtn_Click(): void
    {
        if ( !Session.allyId )
        {
            let restartConfirmationView: InfoMessageView = new InfoMessageView();
            restartConfirmationView.message = jQuery.i18n( "RESTART_CONFIRMATION" );
            restartConfirmationView.btnLayoutType = btn_layout_type.DOUBLE;
            restartConfirmationView.init();
            restartConfirmationView.onClosed.addOnce( this.onRestartConfirmationPopup_Closed, this );
            ServiceLocator.viewManager.fadeIn( restartConfirmationView, view_layer_id.POPUP );
        }
        else
        {
            let waitForAllyConfirmationView: InfoMessageView = new InfoMessageView();
            waitForAllyConfirmationView.alias = "waitForAllyConfirmationView";
            waitForAllyConfirmationView.message = jQuery.i18n( "WAIT_FOR_ALLY_CONFIRMATION" );
            waitForAllyConfirmationView.btnLayoutType = btn_layout_type.DOUBLE;
            waitForAllyConfirmationView.init();
            waitForAllyConfirmationView.root.find( "#infoMessageAcceptBtn" ).hide();
            waitForAllyConfirmationView.onClosed.addOnce( this.onWaitForAllyConfirmationPopup_Closed, this );
            ServiceLocator.viewManager.fadeIn( waitForAllyConfirmationView, view_layer_id.POPUP );

            // Multiplayer.
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ACTION_REQUESTED, null, [ action_request_type.RESTART_GAME ] );
        }
    }

        private onRestartConfirmationPopup_Closed( result: boolean ): void
        {
            if ( result )
            {
                ServiceLocator.game.restart();
            }
        }

        private onWaitForAllyConfirmationPopup_Closed(): void
        {
            // Multiplayer.
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ACTION_REQUEST_CANCELLED, null, null );
        }

    private onLeaveBtn_Click(): void
    {
        let leaveConfirmationView: InfoMessageView = new InfoMessageView();
        leaveConfirmationView.message = jQuery.i18n( "LEAVE_CONFIRMATION" );
        leaveConfirmationView.btnLayoutType = btn_layout_type.DOUBLE;
        leaveConfirmationView.init();
        leaveConfirmationView.onClosed.addOnce( this.onLeaveConfirmationPopup_Closed, this );
        ServiceLocator.viewManager.fadeIn( leaveConfirmationView, view_layer_id.POPUP );
    }

        private onLeaveConfirmationPopup_Closed( result: boolean ): void
        {
            if ( result )
            {
                // Multiplayer.
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.LEAVE_GAME, null, null );

                ServiceLocator.stateMachine.request( main_state_id.GATEWAY );
            }
        }

    private onRulesBtn_Click(): void
    {
        window.open( Url.rulesReference );

        this._go.cContainer.c.visible = false;
    }

    private onSpecialRulesBtn_Click( mecId: string ): void
    {
        window.open( Url.webServer + "/" + Path.kResPdf + "MEC" + mecId + ".pdf" );

        this._go.cContainer.c.visible = false;
    }

    private onHelpBtn_Click(): void
    {
        window.open( Url.help );

        this._go.cContainer.c.visible = false;
    }

    // #endregion //
}