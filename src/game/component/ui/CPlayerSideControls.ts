import Component from "../Component";

import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import { face_policy_type } from "./viewer/CCardView";
import { ISgPlayerSideControls, ISgCard } from "../../../view/game/SaveGameView";
import { location_type } from "../world/CGameWorld";
import { player_type } from "../world/CPlayerArea";
import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";
import * as particles from "pixi-particles";

import GameObject from "../../GameObject";
import CContainer from "../pixi/CContainer";
import CDeckIndicator from "./indicator/CDeckIndicator";
import CIndicatorLock from "./indicator/CIndicatorLock";
import CTokenIndicator from "./indicator/CTokenIndicator";
import CardViewerFactory from "../../CardViewerFactory";
import TopBottomDropController from "./indicator/deck/TopBottomDropController";
import CDeckIndicatorPoiReceptor from "../world/poi_receptor/CDeckIndicatorPoiReceptor";
import LogTargetPlayer from "./right_menu/action_log/target/LogTargetPlayer";
import CTokenIndicatorPoiReceptor from "../world/poi_receptor/CTokenIndicatorPoiReceptor";
import CTooltipReceptor from "./CTooltipReceptor";
import CSprite from "../pixi/CSprite";
import CButton from "../input/CButton";
import CEndTurnButton from "./CEndTurnButton";
import PlayerDeckController from "./viewer/controller/PlayerDeckController";
import PlayerDiscardController from "./viewer/controller/PlayerDiscardController";
import PlayerHandController from "./viewer/controller/PlayerHandController";
import IGameObjectDropArea from "../../IGameObjectDropArea";
import LogTargetFirstPlayer from "./right_menu/action_log/target/LogTargetFirstPlayer";
import DroppedEmitter from "../../DroppedEmitter";
import CDropArea from "../input/CDropArea";
import CActivePlayerButton from "./CActivePlayerButton";
import Session from "../../../Session";
import CHighlightPoiReceptor from "../world/poi_receptor/CHighlightPoiReceptor";
import { IScenario } from "../../ScenarioDB";
import CustomDeckController from "./viewer/controller/CustomDeckController";
import { layer_type } from "../world/CGameLayerProvider";
import CPhaseDiagramButton from "./CPhaseDiagramButton";
import CGraphics from "../pixi/CGraphics";
import { context_button_type } from "./viewer/CViewContextBtn";


export default class CPlayerSideControls extends Component implements IGameObjectDropArea
{
    // #region Attributes //

    // private:

    private _bg: GameObject = null;
    private _phaseDiagramBtn: GameObject = null;
    private _roundNumber: GameObject = null;
    private _threatLevel: GameObject = null;
    private _threatLevelParticleContainer: PIXI.Container = null;
    private _threatLevelAlertEmitter: particles.Emitter = null;
    private _deck: GameObject = null;
    private _customDeck: GameObject = null;
    private _discard: GameObject = null;
    private _hand: GameObject = null;
    private _setAside: GameObject = null;
    private _endTurnBtn: GameObject = null;
    private _activePlayerBtn: GameObject = null;
    private _firstPlayerContainer: PIXI.Container = null;

    private _playerType: player_type = null;

    // #endregion //


    // #region Properties //

    
    public get cPhaseDiagramBtn(): CPhaseDiagramButton { return this._phaseDiagramBtn.cPhaseDiagramButton; }
    public get threatLevel(): GameObject { return this._threatLevel; }
    public get deck(): GameObject { return this._deck; }
    public get customDeck(): GameObject { return this._customDeck; }
    public get discard(): GameObject { return this._discard; }
    public get hand(): GameObject { return this._hand; }
    public get setAside(): GameObject { return this._setAside; }
    public get endTurnBtn(): GameObject { return this._endTurnBtn; }
    public get cEndTurnBtn(): CEndTurnButton { return this._endTurnBtn.cEndTurnButton; }
    public get activePlayerBtn(): GameObject { return this._activePlayerBtn; }
    public get cActivePlayerBtn(): CActivePlayerButton { return this._activePlayerBtn.cActivePlayerButton; }

    public set playerType( value: player_type ) { this._playerType = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CPlayerSideControls";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CPlayerSideControls.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cDropArea != null, "CPlayerSideControls.ts :: init() :: CDropArea component not found." );
        console.assert( this._playerType != null, "CPlayerSideControls.ts :: init() :: this._playerType cannot be null." );

        this._bg = new GameObject( [ new CSprite() ] );
        if ( this._playerType == player_type.PLAYER )
        {
            this._bg.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "player_side_controls" ).data );
        }
        else
        {
            this._bg.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "ally_side_controls" ).data );
            this._bg.cSprite.s.scale.y = -1;
            this._bg.cSprite.s.y = this._bg.cSprite.s.height;
        }
        this._bg.init();
        this._go.cContainer.addChild( this._bg );

        if ( this._playerType == player_type.PLAYER )
        {
            // Phase diagram button.
            this._phaseDiagramBtn = new GameObject( [ new CGraphics(), new CPhaseDiagramButton(), new CButton() ] );
            this._phaseDiagramBtn.init();
            this._go.cContainer.addChild( this._phaseDiagramBtn );
            this._phaseDiagramBtn.cContainer.c.position.set( this._phaseDiagramBtn.cContainer.c.width * 0.5 + 10, 32 );
        
            // Round.
            this._roundNumber = new GameObject( [ new CGraphics(), new CTooltipReceptor() ] );
            this._roundNumber.cGraphics.g.lineStyle( 1, 0x000000, 1 );
            this._roundNumber.cGraphics.g.beginFill( 0xeccc68 );
            this._roundNumber.cGraphics.g.drawCircle( 0, 0, 15 );
            this._roundNumber.cGraphics.g.endFill();
            this._roundNumber.cTooltipReceptor.text = jQuery.i18n( "ROUND_NUMBER" ).replace( "#", "1" );
            this._roundNumber.init();
            this._roundNumber.cContainer.c.position.set( 20, 4 );
            this._roundNumber.cContainer.c.visible = false;
            this._go.cContainer.addChild( this._roundNumber );
            //
            let text: PIXI.Text = new PIXI.Text( "1", ServiceLocator.game.textStyler.normal );
            text.name = "text";
            text.anchor.set( 0.5 );
            this._roundNumber.cContainer.c.addChild( text );
        }

        let arrIndicator: Array<GameObject> = new Array<GameObject>();
        this._threatLevel = this.createThreatLevelIndicator();
        this._go.cContainer.addChild( this._threatLevel );
        if ( this._playerType == player_type.ALLY )
        {
            this._threatLevel.cTokenIndicator.trigger.cDraggable.setEnabled( false );
        }
        this._threatLevel.cTokenIndicator.trigger.cTokenCounter.onCountUpdated.add( this.onThreatCount_Updated, this );
        arrIndicator.push( this._threatLevel );

        this._hand = this.createHandIndicator();
        this._go.cContainer.addChild( this._hand );
        arrIndicator.push( this._hand );

        this._deck = this.createDeckIndicator();
        this._go.cContainer.addChild( this._deck );
        arrIndicator.push( this._deck );
        
        this._discard = this.createDiscardIndicator();
        this._go.cContainer.addChild( this._discard );
        arrIndicator.push( this._discard );

        const kScenario: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
        if ( kScenario.play_area && kScenario.play_area.player_custom_deck )
        {
            this._customDeck = this.createCustomDeckIndicator( jQuery.i18n( kScenario.play_area.player_custom_deck.titleI18n ) );
            this._go.cContainer.addChild( this._customDeck );
            arrIndicator.push( this._customDeck );
        }

        // Position items.
        let m: PIXI.Matrix = new PIXI.Matrix();
        m.identity();
        const kStartAngle: number = ( this._playerType == player_type.PLAYER ? 9 : -9 ) * Math.PI / 180;
        m.rotate( kStartAngle );
        let arcPosition: PIXI.Point = new PIXI.Point( 0, this._playerType == player_type.PLAYER ? -190 : 190 );
        arcPosition = m.apply( arcPosition );
        //
        m.identity();
        let rotationAngle: number = ( 90 - 18 ) / ( arrIndicator.length - 1 ) * Math.PI / 180;
        if ( this._playerType == player_type.ALLY ) 
        {
            rotationAngle *= -1;
        }
        m.rotate( rotationAngle );
        let arcBaseY: number = this._playerType == player_type.PLAYER ? this._bg.cSprite.s.height : 0;
        arcBaseY -= this._threatLevel.cContainer.c.height * 0.5;
        //
        for ( let indicator of arrIndicator )
        {
            indicator.cContainer.c.position.set( arcPosition.x, arcPosition.y + arcBaseY );
            arcPosition = m.apply( arcPosition );
        }

        this._threatLevelParticleContainer = new PIXI.Container();
        this._threatLevelParticleContainer.position.copyFrom( this._threatLevel.cContainer.c.position );
        this._threatLevelParticleContainer.y += 20;
        this._go.cContainer.c.addChildAt( this._threatLevelParticleContainer, this._go.cContainer.c.getChildIndex( this._threatLevel.cContainer.c ) );
        //
        this._threatLevelAlertEmitter = this.createThreatLevelAlertEmitter();

        // Set aside.
        this._setAside = this.createSetAsideIndicator();
        this._setAside.cContainer.c.position.set( this._deck.cContainer.c.x + 30, this._deck.cContainer.c.y );
        this._setAside.cContainer.c.scale.set( 0.5 );
        this._go.cContainer.addChild( this._setAside );

        // End-turn button.
        this._endTurnBtn = this.createEndTurnBtn();
        this._go.cContainer.addChild( this._endTurnBtn );
        this._endTurnBtn.cContainer.c.position.set( this._endTurnBtn.cContainer.c.width * 0.5, this._playerType == player_type.PLAYER ? this._bg.cContainer.c.height - this._endTurnBtn.cContainer.c.height * 0.5 : this._endTurnBtn.cContainer.c.height * 0.5 );
    
        // Ring.
        this._activePlayerBtn = this.createActivePlayerBtn();
        this._go.cContainer.addChild( this._activePlayerBtn );
        this._activePlayerBtn.cContainer.c.position.set( this._endTurnBtn.cContainer.c.width, this._playerType == player_type.PLAYER ? this._bg.cContainer.c.height - this._activePlayerBtn.cContainer.c.height * 0.5 : this._activePlayerBtn.cContainer.c.height * 0.5 );
        if ( ServiceLocator.game.activePlayer == this._playerType )
        {
            this._activePlayerBtn.cActivePlayerButton.show();
        }
        else
        {
            this._activePlayerBtn.cActivePlayerButton.hide();
        }
        
        this._firstPlayerContainer = new PIXI.Container();
        this._firstPlayerContainer.position.set( 
            this._endTurnBtn.cContainer.c.x - 30,
            this._endTurnBtn.cContainer.c.y );
        if ( this._playerType == player_type.ALLY )
        {
            this._firstPlayerContainer.y += this._endTurnBtn.cContainer.c.height * 0.5;
        }
        else
        {
            this._firstPlayerContainer.y -= this._endTurnBtn.cContainer.c.height * 0.5;
        }
        this._go.cContainer.c.addChild( this._firstPlayerContainer );
    }

        private createThreatLevelAlertEmitter(): particles.Emitter
        {
            let emitter: particles.Emitter = new particles.Emitter(
                this._threatLevelParticleContainer,
                [ ServiceLocator.resourceStack.findAsTexture( "particle_spark" ) ],
                ServiceLocator.resourceStack.find( "emitter_threat_level_alert" ).data );
            emitter.autoUpdate = true;
            emitter.emit = false;

            return emitter;
        }

    public end(): void
    {
        if ( this._phaseDiagramBtn )
        {
            this._phaseDiagramBtn.end();
            this._phaseDiagramBtn = null;
        }

        if ( this._roundNumber )
        {
            this._roundNumber.end();
            this._roundNumber = null;
        }

        this._threatLevelAlertEmitter.destroy();
        this._threatLevelAlertEmitter = null;

        this._threatLevelParticleContainer = null;

        this._threatLevel.end();
        this._threatLevel = null;

        this._deck.end();
        this._deck = null;

        this._discard.end();
        this._discard = null;

        this._hand.end();
        this._hand = null;

        this._setAside.end();
        this._setAside = null;

        if ( this._customDeck )
        {
            this._customDeck.end();
            this._customDeck = null;
        }

        this._endTurnBtn.end();
        this._endTurnBtn = null;

        this._activePlayerBtn.end();
        this._activePlayerBtn = null;

        this._bg.end();
        this._bg = null;

        super.end();
    }

    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        this._go.cDropArea.setEnabled( isEnabled );

        this._threatLevel.cTokenIndicator.setEnabled( isEnabled );
        this._deck.cDeckIndicator.setEnabled( isEnabled );
        this._discard.cDeckIndicator.setEnabled( isEnabled );
        this._hand.cDeckIndicator.setEnabled( isEnabled );
        this._setAside.cDeckIndicator.setEnabled( isEnabled );
        this._endTurnBtn.cEndTurnButton.setEnabled( isEnabled );
        this._activePlayerBtn.cActivePlayerButton.setEnabled( isEnabled );
    }

    public onRoundCountUpdated( roundCount: number ): void
    {
        this._roundNumber.cContainer.c.visible = true;

        ( this._roundNumber.cContainer.c.getChildByName( "text" ) as PIXI.Text ).text = roundCount.toString();
        this._roundNumber.cTooltipReceptor.text = jQuery.i18n( "ROUND_NUMBER" ).replace( "#", roundCount.toString() );
    }

    // overrides.
    public saveGame(): ISgPlayerSideControls
    {
        let arrDeckCard: Array<ISgCard> = new Array<ISgCard>();
        for ( let card of this._deck.cDeckIndicator.panel.cViewer.cCardView.findItems() )
        {
            arrDeckCard.push( card.cCard.saveGame() );
        }
        let arrDiscardCard: Array<ISgCard> = new Array<ISgCard>();
        for ( let card of this._discard.cDeckIndicator.panel.cViewer.cCardView.findItems() )
        {
            arrDiscardCard.push( card.cCard.saveGame() );
        }
        let arrHandCard: Array<ISgCard> = new Array<ISgCard>();
        for ( let card of this._hand.cDeckIndicator.panel.cViewer.cCardView.findItems() )
        {
            arrHandCard.push( card.cCard.saveGame() );
        }
        let arrSetAsideCard: Array<ISgCard> = new Array<ISgCard>();
        for ( let card of this._setAside.cDeckIndicator.panel.cViewer.cCardView.findItems() )
        {
            arrSetAsideCard.push( card.cCard.saveGame() );
        }

        let result: ISgPlayerSideControls = {
            threat: this._threatLevel.cTokenIndicator.trigger.cTokenCounter.count,
            deck: this._deck.cDeckIndicator.panel.cViewer.cCardView.saveGame(),
            discard: this._discard.cDeckIndicator.panel.cViewer.cCardView.saveGame(),
            hand: this._hand.cDeckIndicator.panel.cViewer.cCardView.saveGame(),
            setAside: this._setAside.cDeckIndicator.panel.cViewer.cCardView.saveGame(),
            endTurnBtn: this._endTurnBtn.cEndTurnButton.saveGame() };
        if ( this._customDeck )
        {
            result.customDeck = this._customDeck.cDeckIndicator.panel.cViewer.cCardView.saveGame();
        }
        if ( this._phaseDiagramBtn )
        {
            result.phaseDiagramButton = this._phaseDiagramBtn.cPhaseDiagramButton.saveGame();
        }

        return result;
    }

    // overrides.
    public loadGame( sgPlayerSideControls: ISgPlayerSideControls, pass: number ): void
    {
        if ( pass == 0 )
        {
            this._threatLevel.cTokenIndicator.trigger.cTokenCounter.setCount( sgPlayerSideControls.threat, action_scope_type.LOCAL );
            
            this._deck.cDeckIndicator.panel.cViewer.cCardView.loadGame( sgPlayerSideControls.deck, pass );
            this._discard.cDeckIndicator.panel.cViewer.cCardView.loadGame( sgPlayerSideControls.discard, pass );
            this._hand.cDeckIndicator.panel.cViewer.cCardView.loadGame( sgPlayerSideControls.hand, pass );
            if ( sgPlayerSideControls.setAside )
            {
                this._setAside.cDeckIndicator.panel.cViewer.cCardView.loadGame( sgPlayerSideControls.setAside, pass );
            }
            if ( sgPlayerSideControls.customDeck )
            {
                this._customDeck.cDeckIndicator.panel.cViewer.cCardView.loadGame( sgPlayerSideControls.customDeck, pass );
            }
            this._endTurnBtn.cEndTurnButton.loadGame( sgPlayerSideControls.endTurnBtn, pass );
            if ( this._phaseDiagramBtn && sgPlayerSideControls.phaseDiagramButton )
            {
                this._phaseDiagramBtn.cPhaseDiagramButton.loadGame( sgPlayerSideControls.phaseDiagramButton, pass );
            }
            if ( this._roundNumber )
            {
                this.onRoundCountUpdated( ServiceLocator.game.roundCount );
            }

            if ( ServiceLocator.game.activePlayer == this._playerType )
            {
                this._activePlayerBtn.cActivePlayerButton.show();
            }
            else
            {
                this._activePlayerBtn.cActivePlayerButton.hide();
            }
        }
    }

    // private:

    private createThreatLevelIndicator(): GameObject
    {
        let threat: GameObject = new GameObject( [ new CContainer(), new CTokenIndicator(), new CIndicatorLock(), new CTokenIndicatorPoiReceptor(), new CTooltipReceptor() ] );
        threat.oid = this._go.oid + "_threat_level";
        threat.cTokenIndicator.tokenName = "threat_level";
        threat.cIndicator.iconId = "threat_level";
        threat.cTooltipReceptor.text = jQuery.i18n( "THREAT_LEVEL" );
        threat.cTooltipReceptor.description = jQuery.i18n( "THREAT_LEVEL_DESC" );
        threat.init();
        threat.cTokenIndicator.trigger.cTokenCounter.minCount = 0;

        return threat;
    }

    private createHandIndicator(): GameObject
    {
        let cardViewerFactory: CardViewerFactory = new CardViewerFactory();
        let cardViewer: GameObject = cardViewerFactory.create( { 
            oid: this._go.oid + "_hand_viewer",
            title: jQuery.i18n( "HAND" ),
            location: this._playerType == player_type.PLAYER ? location_type.MY_HAND : location_type.ALLY_HAND,
            playerType: this._playerType,
            facePolicy: face_policy_type.FACE_UP,
            contextBtnTypes: this._playerType == player_type.PLAYER ? [ context_button_type.MULLIGAN, context_button_type.SORT_BY_SPHERE, context_button_type.DRAW_X_PLAYER_CARDS, context_button_type.RANDOMLY_DISCARD ] : [],
            customController: this._playerType == player_type.PLAYER ? new PlayerHandController() : null } );
        ServiceLocator.game.root.cGameLayerProvider.add( cardViewer.cContainer.c.parent[ "go" ] as GameObject, layer_type.HUD );

        let hand: GameObject = new GameObject( [ new CContainer(), new CDeckIndicator(), new CIndicatorLock(), new CDeckIndicatorPoiReceptor(), new CTooltipReceptor() ] );
        hand.oid = this._go.oid + "_hand";
        hand.cDeckIndicator.iconId = "hand";
        hand.cDeckIndicator.panel = cardViewer;
        hand.cTooltipReceptor.text = jQuery.i18n( "HAND" );
        hand.init();
        //
        if ( this._playerType == player_type.ALLY )
        {
            hand.cIndicator.trigger.cButton.setEnabled( false );
        }
        else
        {
            hand.cDeckIndicator.cViewer.cCardView.setContextBtnState( [ { buttonType: context_button_type.MULLIGAN, isVisible: false } ] );
        }

        return hand;
    }

    private createDeckIndicator(): GameObject
    {
        let cardViewerFactory: CardViewerFactory = new CardViewerFactory();
        let cardViewer: GameObject = cardViewerFactory.create( { 
            oid: this._go.oid + "_player_deck_viewer",
            title: jQuery.i18n( "PLAYER_DECK" ),
            location: this._playerType == player_type.PLAYER ? location_type.MY_DECK : location_type.ALLY_DECK,
            playerType: this._playerType,
            facePolicy: face_policy_type.FACE_DOWN,
            contextBtnTypes: this._playerType == player_type.PLAYER ? [ context_button_type.REVEAL_X, context_button_type.DISCARD_X, context_button_type.SHUFFLE, context_button_type.SORT_FROM_A_TO_Z ] : [],
            customController: this._playerType == player_type.PLAYER ? new PlayerDeckController() : null } );
        ServiceLocator.game.root.cGameLayerProvider.add( cardViewer.cContainer.c.parent[ "go" ] as GameObject, layer_type.HUD );

        let deck: GameObject = new GameObject( [ new CContainer(), new CDeckIndicator(), new CIndicatorLock(), new CDeckIndicatorPoiReceptor(), new CTooltipReceptor() ] );
        deck.oid = this._go.oid + "_player_deck";
        deck.cDeckIndicator.iconId = "player_deck";
        deck.cDeckIndicator.panel = cardViewer;
        deck.cTooltipReceptor.text = jQuery.i18n( "PLAYER_DECK" );
        deck.init();
        //
        if ( this._playerType == player_type.PLAYER )
        {
            let topBottomDropController: TopBottomDropController = new TopBottomDropController();
            topBottomDropController.cardView = cardViewer.cViewer.cCardView;
            topBottomDropController.init();
            deck.cDeckIndicator.trigger.cDropArea.target = topBottomDropController;
        }
        else
        {
            deck.cIndicator.trigger.cButton.setEnabled( false );
        }

        return deck;
    }

    private createSetAsideIndicator(): GameObject
    {
        let cardViewerFactory: CardViewerFactory = new CardViewerFactory();
        let cardViewer: GameObject = cardViewerFactory.create( { 
            oid: this._go.oid + "_set_aside_deck_viewer",
            title: jQuery.i18n( "SET_ASIDE" ),
            location: this._playerType == player_type.PLAYER ? location_type.MY_SET_ASIDE : location_type.ALLY_SET_ASIDE,
            playerType: this._playerType,
            facePolicy: face_policy_type.FACE_UP,
            customController: new CustomDeckController() } );
        ServiceLocator.game.root.cGameLayerProvider.add( cardViewer.cContainer.c.parent[ "go" ] as GameObject, layer_type.HUD );

        let deck: GameObject = new GameObject( [ new CContainer(), new CDeckIndicator(), new CIndicatorLock(), new CDeckIndicatorPoiReceptor(), new CTooltipReceptor() ] );
        deck.oid = this._go.oid + "_set_aside_deck";
        deck.cDeckIndicator.iconId = "set_aside";
        deck.cDeckIndicator.panel = cardViewer;
        deck.cTooltipReceptor.text = jQuery.i18n( "SET_ASIDE" );
        deck.init();
        //
        if ( this._playerType == player_type.ALLY )
        {
            deck.cIndicator.trigger.cButton.setEnabled( false );
        }

        return deck;
    }

    private createDiscardIndicator(): GameObject
    {
        let cardViewerFactory: CardViewerFactory = new CardViewerFactory();
        let cardViewer: GameObject = cardViewerFactory.create( { 
            oid: this._go.oid + "_player_discard_viewer",
            title: jQuery.i18n( "PLAYER_DISCARD" ),
            location: this._playerType == player_type.PLAYER ? location_type.MY_DISCARD : location_type.ALLY_DISCARD,
            playerType: this._playerType,
            facePolicy: face_policy_type.FACE_UP,
            contextBtnTypes: this._playerType == player_type.PLAYER ? [ context_button_type.RESHUFFLE ] : [],
            customController: this._playerType == player_type.PLAYER ? new PlayerDiscardController() : null } );
        ServiceLocator.game.root.cGameLayerProvider.add( cardViewer.cContainer.c.parent[ "go" ] as GameObject, layer_type.HUD );

        let discard: GameObject = new GameObject( [ new CContainer(), new CDeckIndicator(), new CIndicatorLock(), new CDeckIndicatorPoiReceptor(), new CTooltipReceptor() ] );
        discard.oid = this._go.oid + "_player_discard";
        discard.cDeckIndicator.iconId = "player_discard";
        discard.cDeckIndicator.panel = cardViewer;
        discard.cTooltipReceptor.text = jQuery.i18n( "PLAYER_DISCARD" );
        discard.init();

        return discard;
    }

    private createCustomDeckIndicator( title: string ): GameObject
    {
        let cardViewerFactory: CardViewerFactory = new CardViewerFactory();
        let cardViewer: GameObject = cardViewerFactory.create( { 
            oid: this._go.oid + "_custom_deck_viewer",
            title: title,
            location: this._playerType == player_type.PLAYER ? location_type.MY_CUSTOM_DECK : location_type.ALLY_CUSTOM_DECK,
            playerType: this._playerType,
            facePolicy: face_policy_type.FACE_DOWN,
            contextBtnTypes: this._playerType == player_type.PLAYER ? [ context_button_type.REVEAL_X, context_button_type.DISCARD_X, context_button_type.SHUFFLE, context_button_type.SORT_FROM_A_TO_Z ] : [],
            customController: this._playerType == player_type.PLAYER ? new CustomDeckController() : null } );
        ServiceLocator.game.root.cGameLayerProvider.add( cardViewer.cContainer.c.parent[ "go" ] as GameObject, layer_type.HUD );

        let deck: GameObject = new GameObject( [ new CContainer(), new CDeckIndicator(), new CIndicatorLock(), new CDeckIndicatorPoiReceptor(), new CTooltipReceptor() ] );
        deck.oid = this._go.oid + "_custom_deck";
        deck.cDeckIndicator.iconId = "player_custom_deck";
        deck.cDeckIndicator.panel = cardViewer;
        deck.cTooltipReceptor.text = title;
        deck.init();
        //
        if ( this._playerType == player_type.PLAYER )
        {
            let topBottomDropController: TopBottomDropController = new TopBottomDropController();
            topBottomDropController.cardView = cardViewer.cViewer.cCardView;
            topBottomDropController.init();
            deck.cDeckIndicator.trigger.cDropArea.target = topBottomDropController;
        }
        else
        {
            deck.cIndicator.trigger.cButton.setEnabled( false );
        }

        return deck;
    }

    private createEndTurnBtn(): GameObject
    {
        let endTurnBtn: GameObject = new GameObject( [ new CContainer(), new CButton(), new CEndTurnButton(), new CHighlightPoiReceptor() ] );
        endTurnBtn.cEndTurnButton.playerType = this._playerType;
        endTurnBtn.init();
        if ( this._playerType == player_type.ALLY )
        {
            endTurnBtn.cEndTurnButton.setEnabled( false );
        }

        return endTurnBtn;
    }

    private createActivePlayerBtn(): GameObject
    {
        let activePlayerBtn: GameObject = new GameObject( [ new CContainer(), new CButton(), new CActivePlayerButton(), new CTooltipReceptor(), new CHighlightPoiReceptor() ] );
        activePlayerBtn.cActivePlayerButton.playerType = this._playerType;
        activePlayerBtn.init();
        if ( this._playerType == player_type.ALLY || Session.allyId == null )
        {
            activePlayerBtn.cActivePlayerButton.setEnabled( false );
        }

        return activePlayerBtn;
    }

    // #endregion //


    // #region IGameObjectDropArea //

    public validateDroppedGameObject( dropped: GameObject ): boolean
    {
        return dropped.cFirstPlayerToken != null;
    }

    public processDroppedGameObject( dropped: GameObject, global: PIXI.IPoint, actionScopeType: action_scope_type ): void
    {
        // Log.
        if ( global )
        {
            const kFromIconId: string = Utils.img.findPlayerTextureId( this._playerType^1 );
            const kToIconId: string = Utils.img.findPlayerTextureId( this._playerType );    
            ServiceLocator.game.cGameWorld.cActionLogger.logSequence( 
                player_type.PLAYER, new LogTargetFirstPlayer(), kFromIconId, kToIconId, true ); 
        }

        ServiceLocator.game.firstPlayer = this._playerType;

        if ( Session.allyId )
        {
            // Give control of baggins or fellowship hero to first player.
            let oldFirstPlayerArea: GameObject = ServiceLocator.game.firstPlayer == player_type.PLAYER ? ServiceLocator.game.cGameWorld.allyArea : ServiceLocator.game.cGameWorld.playerArea;
            let newFirstPlayerPlayArea: GameObject = ServiceLocator.game.firstPlayer == player_type.PLAYER ? ServiceLocator.game.cGameWorld.playerArea : ServiceLocator.game.cGameWorld.allyArea;
            let arrActor: Array<GameObject> = oldFirstPlayerArea.cPlayerArea.cHome.findAllActors();
            let arrControlLostActor: Array<GameObject> = new Array<GameObject>();
            for ( let actor of arrActor )
            {
                if ( ( actor.cCardToken.cCurSide.cardInfo.type_code == "hero" || actor.cCardToken.cCurSide.cardInfo.type_code == "objective-ally" )
                    && actor.cCardToken.cCurSide.cardInfo.text.toLowerCase().indexOf( "the first player gains control of" ) != -1 )
                {
                    arrControlLostActor.push( actor );
                }
            }
            if ( arrControlLostActor.length > 0 )
            {
                newFirstPlayerPlayArea.cPlayerArea.cHome.appendActors( arrControlLostActor );
                oldFirstPlayerArea.cPlayerArea.cHome.sort();
            }
        }

        this._firstPlayerContainer.addChild( dropped.cContainer.c );
        dropped.cContainer.c.interactive = this._playerType == player_type.PLAYER;

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.DROP_AT_DROP_AREA, null, [ dropped.oid, this._go.oid, null ] );
        }

        if ( ServiceLocator.game.isAnimated )
        {
            // Sfx.
            ServiceLocator.audioManager.playSfx( "token_dropped" );

            // Vfx.
            let droppedEmitter: DroppedEmitter = new DroppedEmitter();
            droppedEmitter.layer = ServiceLocator.game.world.cGameLayerProvider.get( layer_type.VFX );
            droppedEmitter.init();
            droppedEmitter.playSmall( dropped.cContainer.c.getGlobalPosition().x, dropped.cContainer.c.getGlobalPosition().y );
            Utils.anim.drop( dropped.cContainer.c );
        }
    }

    // #endregion //


    // #region Callbacks //

    private onThreatCount_Updated( count: number, delta: number, isPlayerInput: boolean ): void
    {
        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetPlayer( this._playerType ), "threat_level", delta, count, true );
        }

        if ( count >= 50 )
        {
            this._threatLevelAlertEmitter.emit = true;
        }
        else
        {

            this._threatLevelAlertEmitter.emit = false;
        }
    }

    // #endregion //
}
