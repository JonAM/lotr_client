import Component from "../Component";

import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import { layer_type } from "../world/CGameLayerProvider";
import { location_type } from "../world/CGameWorld";
import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CSprite from "../pixi/CSprite";
import IGameObjectDropArea from "../../IGameObjectDropArea";
import { ISgCardToken } from "../../../view/game/SaveGameView";
import CardTokenFactory from "../../CardTokenFactory";
import DroppedEmitter from "../../DroppedEmitter";
import Signal from "../../../lib/signals/Signal";
import { player_type } from "../world/CPlayerArea";


export default class CDorCuartholLocationHolder extends Component implements IGameObjectDropArea
{
    // #region Attributes //

    // private:

    private _location: location_type = null;

    private _bg: GameObject = null;
    private _locationContainer: PIXI.Container = null;
    private _playerContainer: PIXI.Container = null;

    private _locationToken: GameObject = null;

    // Signals.
    private _onPlayerTokenDropped: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get locationToken(): GameObject { return this._locationToken; }

    // Signals.
    public get onPlayerTokenDropped(): Signal { return this._onPlayerTokenDropped; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CDorCuartholLocationHolder";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CDorCuartholLocationHolder.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cDropArea != null, "CDorCuartholLocationHolder.ts :: init() :: CDropArea component not found." );
        console.assert( this._go.cLogLocation != null, "CDorCuartholLocationHolder.ts :: init() :: cLogLocation component not found." );

        this._go.cDropArea.target = this;
        this._go.cLogLocation.location = location_type.DOR_CUARTHOL_LOCATION;

        this._location = location_type.DOR_CUARTHOL_LOCATION;

        this._bg = new GameObject( [ new CSprite() ] );
        this._bg.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "dor-cuarthol_location_holder" ).data );
        this._bg.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 100, this._bg.cSprite.s );
        this._bg.init();
        this._go.cContainer.addChild( this._bg );

        this._locationContainer = new PIXI.Container();
        this._go.cContainer.c.addChild( this._locationContainer );

        this._playerContainer = new PIXI.Container();
        this._go.cContainer.c.addChild( this._playerContainer );
    }

    public end(): void
    {
        this._onPlayerTokenDropped.removeAll();

        this._bg.end();
        this._bg = null;

        super.end();
    }

    // overrides.
    public saveGame(): ISgCardToken
    {
        return this._locationToken ? this._locationToken.cCardToken.saveGame() : null;
    }

    // overrides.
    public loadGame( sgLocationToken: ISgCardToken, pass: number ): void
    {
        if ( sgLocationToken && pass == 0 )
        {
            let card: GameObject = GameObject.find( sgLocationToken.oid.substr( 0, sgLocationToken.oid.length - 1 ) );
            let cardTokenFactory: CardTokenFactory = new CardTokenFactory();
            this._locationToken = cardTokenFactory.create( card );
            card.end();
            this._locationToken.cCardToken.loadGame( sgLocationToken, pass );

            this._go.cDropArea.forceDrop( this._locationToken, null, action_scope_type.LOCAL );
        }
    }

    // #endregion //


    // #region IGameObjectDropArea //

    public validateDroppedGameObject( dropped: GameObject ): boolean
    {
        return dropped.cCardToken != null && dropped.cCardToken.curSide.cLocationSide != null && this._locationToken == null
            || dropped.cPlayerToken != null;
    }

    public processDroppedGameObject( dropped: GameObject, global: PIXI.IPoint, actionScopeType: action_scope_type ): void
    {
        if ( dropped.cCardToken )
        {
            this._locationToken = dropped;

            this._locationToken.cCardToken.location = this._location;
            this._locationToken.cContainer.c.position.set( 
                -this._locationToken.cCardToken.calcWidth() * 0.5, 
                -this._locationToken.cCardToken.calcHeight() * 0.5 );
            this._locationContainer.addChild( this._locationToken.cContainer.c );
            this._locationToken.cDraggable.onDropped.addOnce( this.onLocationToken_Dropped, this );

            if ( this._locationToken.cCardToken.isAnimated && ServiceLocator.game.isAnimated )
            {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "token_dropped" );
                
                // Vfx.
                this.playLocationDroppedVfx();
                Utils.anim.dropFromCorner( this._locationToken.cContainer.c );
            }
        }
        else if ( dropped.cPlayerToken )
        {
            this._onPlayerTokenDropped.dispatch( this._go );

            this._playerContainer.addChild( dropped.cContainer.c );

            if ( this._locationToken )
            {
                let activeLocationHolder: GameObject = ServiceLocator.game.cGameWorld.findLocation( location_type.ACTIVE_LOCATION );
                if ( activeLocationHolder.cActiveLocationHolder.activeLocationToken )
                {
                    activeLocationHolder.cActiveLocationHolder.activeLocationToken.cCardToken.discard( action_scope_type.LOCAL );
                }
                activeLocationHolder.cDropArea.forceDrop( this._locationToken, null, action_scope_type.LOCAL );
            }

            if ( ServiceLocator.game.isAnimated )
            {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "token_dropped" );
                
                // Vfx.
                this.playPlayerDroppedVfx( dropped );
                Utils.anim.bump( dropped.cContainer.c );
            }
        }

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.DROP_AT_DROP_AREA, null, [ dropped.oid, this._go.oid, null ] );
        }
    }

        private playLocationDroppedVfx(): void
        {
            let droppedEmitter: DroppedEmitter = new DroppedEmitter();
            const kVfxLayer: GameObject = Utils.game.findGameProviderLayer( this._go, layer_type.VFX );
            droppedEmitter.layer = kVfxLayer;
            droppedEmitter.init();
            const kVfxPos: PIXI.IPoint = kVfxLayer.cContainer.c.toLocal( this._bg.cContainer.c.getGlobalPosition() );
            droppedEmitter.playLarge( kVfxPos.x, kVfxPos.y );
        }

        private playPlayerDroppedVfx( playerToken: GameObject ): void
        {
            let droppedEmitter: DroppedEmitter = new DroppedEmitter();
            const kVfxLayer: GameObject = Utils.game.findGameProviderLayer( this._go, layer_type.VFX );
            droppedEmitter.layer = kVfxLayer;
            droppedEmitter.init();
            const kVfxPos: PIXI.IPoint = kVfxLayer.cContainer.c.toLocal( playerToken.cContainer.c.getGlobalPosition() );
            droppedEmitter.playSmall( kVfxPos.x, kVfxPos.y );
        }

    // #endregion //


    // #region Callbacks //

    private onLocationToken_Dropped(): void
    {
        this._locationToken.cContainer.c.position.set( 0 );
        this._locationToken = null;
    }
    
    // #endregion //
}