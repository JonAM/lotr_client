import Component from "../Component";

import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CSprite from "../pixi/CSprite";
import IGameObjectDropArea from "../../IGameObjectDropArea";
import Signal from "../../../lib/signals/Signal";
import { ISgActiveLocationHolder, ISgCardToken } from "../../../view/game/SaveGameView";
import CardTokenFactory from "../../CardTokenFactory";
import DroppedEmitter from "../../DroppedEmitter";
import { layer_type } from "../world/CGameLayerProvider";
import { location_type } from "../world/CGameWorld";
import CContainer from "../pixi/CContainer";
import CMultiItemSelector, { mis_layout_type } from "./CMultiItemSelector";
import { status_type } from "../card/token/CCardTokenSide";
import SignalBinding from "../../../lib/signals/SignalBinding";
import { player_type } from "../world/CPlayerArea";
import CShareableGameElement from "../CShareableGameElement";


export default class CActiveLocationHolder extends Component implements IGameObjectDropArea
{
    // #region Attributes //

    // private:

    private _location: location_type = null;

    private _bg: GameObject = null;
    private _multiItemSelector: GameObject = null;
    private _locationTokenContainer: PIXI.Container = null;

    private _arrLocationToken: Array<GameObject> = null;

    private _isAnimated: boolean = true;

    // Signals.
    private _onActiveLocationUpdated: Signal = new Signal();
    private _onActiveLocationProgressUpdated: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public set location( value: location_type ) { this._location = value; }
    public set isAnimated( value: boolean ) { this._isAnimated = value; }
    
    public get location(): location_type { return this._location; }
    public get activeLocationToken(): GameObject { return this._arrLocationToken.length > 0 ? this._arrLocationToken[ this._multiItemSelector.cMultiItemSelector.selectedIndex ] : null; }
    public get locationTokens(): Array<GameObject> { return this._arrLocationToken; }
    public get selector(): GameObject { return this._multiItemSelector; }

    // Signals.
    public get onActiveLocationUpdated(): Signal {return this._onActiveLocationUpdated; }
    public get onActiveLocationProgressUpdated(): Signal {return this._onActiveLocationProgressUpdated; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CActiveLocationHolder";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CActiveLocationHolder.ts :: init() :: CContainer component not found." );
        console.assert( this._location != null, "CActiveLocationHolder.ts :: init() :: this._location cannot be null." );

        this._arrLocationToken = new Array<GameObject>();

        this._bg = new GameObject( [ new CSprite() ] );
        this._bg.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "active_location_holder" ).data );
        this._bg.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 100, this._bg.cSprite.s );
        this._bg.init();
        this._go.cContainer.addChild( this._bg );

        this._locationTokenContainer = new PIXI.Container();
        this._go.cContainer.c.addChild( this._locationTokenContainer );

        let arrComponent: Array<Component> = [ new CContainer(), new CMultiItemSelector() ];
        if ( this._location == location_type.ACTIVE_LOCATION
            || this._location == location_type.SPLIT_SAURON_ACTIVE_LOCATION )
        {
            arrComponent.push( new CShareableGameElement() );
        }
        this._multiItemSelector = new GameObject( arrComponent );
        this._multiItemSelector.oid = this._go.oid + "_selector";
        this._multiItemSelector.cMultiItemSelector.maxItemCount = 2;
        this._multiItemSelector.cMultiItemSelector.owner = this._go;
        this._multiItemSelector.cMultiItemSelector.layoutType = mis_layout_type.HORIZONTAL;
        this._multiItemSelector.init();
        this._multiItemSelector.cContainer.c.y = -170;
        this._go.cContainer.addChild( this._multiItemSelector );
        this._multiItemSelector.cMultiItemSelector.onItemSelected.add( this.onMultiItemSelectorItem_Selected, this );
    }

    public end(): void
    {
        this._onActiveLocationUpdated.removeAll();
        this._onActiveLocationProgressUpdated.removeAll();

        for ( let locationToken of this._arrLocationToken )
        {
            locationToken.end();
        }
        this._arrLocationToken = null;

        this._multiItemSelector.end();
        this._multiItemSelector = null;

        this._bg.end();
        this._bg = null;

        super.end();
    }

    public hasUnexploredLocation(): boolean
    {
        let result: boolean = false;

        for ( let locationToken of this._arrLocationToken )
        {
            if ( !locationToken.cCardToken.curSide.cLocationSide.hasStatus( status_type.EXPLORED ) )
            {
                result = true;
                break;
            }
        }

        return result;
    }

    public findItems(): Array<GameObject> { return this._arrLocationToken.slice(); }

    public autoSelectUnexploredLocation( actionScope: action_scope_type ): void
    {
        let locationIndex: number = 0;
        for ( let locationToken of this._arrLocationToken )
        {
            if ( !locationToken.cCardToken.curSide.cLocationSide.hasStatus( status_type.EXPLORED ) )
            {
                this._multiItemSelector.cMultiItemSelector.selectItem( locationIndex, actionScope );
                break;
            }

            locationIndex += 1;
        }
    }

    public showActor( actor: GameObject, actionScope: action_scope_type ): void
    {
        let actorIndex: number = 0;
        for ( let locationToken of this._arrLocationToken )
        {
            if ( locationToken == actor )
            {
                break;
            }
            else
            {
                actorIndex += 1;
            }
        }

        if ( actorIndex != this._multiItemSelector.cMultiItemSelector.selectedIndex )
        {
            this._multiItemSelector.cMultiItemSelector.selectItem( actorIndex, actionScope );
        }
    }

    // overrides.
    public saveGame(): ISgActiveLocationHolder
    {
        let arrSgLocationToken: Array<ISgCardToken> = new Array<ISgCardToken>();
        for ( let locationToken of this._arrLocationToken )
        {
            arrSgLocationToken.push( locationToken.cCardToken.saveGame() );
        }

        return { 
            locationTokens: arrSgLocationToken,
            selLocationIndex: this._multiItemSelector.cMultiItemSelector.selectedIndex };
    }

    // overrides.
    public loadGame( sgActiveLocationHolder: ISgActiveLocationHolder, pass: number ): void
    {
        if ( pass == 0 )
        {
            for ( let sgCardToken of sgActiveLocationHolder.locationTokens )
            {
                let card: GameObject = GameObject.find( sgCardToken.oid.substr( 0, sgCardToken.oid.length - 1 ) );
                let cardTokenFactory: CardTokenFactory = new CardTokenFactory();
                let locationToken: GameObject = cardTokenFactory.create( card );
                card.end();
                locationToken.cCardToken.loadGame( sgCardToken, pass );

                this._go.cDropArea.forceDrop( locationToken, null, action_scope_type.LOCAL );
            }

            if ( sgActiveLocationHolder.selLocationIndex != -1 )
            {
                this._multiItemSelector.cMultiItemSelector.selectItem( sgActiveLocationHolder.selLocationIndex, action_scope_type.LOCAL );
            }
        }
    }

    // #endregion //


    // #region IGameObjectDropArea //

    public validateDroppedGameObject( dropped: GameObject ): boolean
    {
        return this._arrLocationToken.length < this._multiItemSelector.cMultiItemSelector.maxItemCount
            && dropped.cCardToken != null
            && dropped.cCardToken.curSide.cLocationSide != null;
    }

    public processDroppedGameObject( locationToken: GameObject, global: PIXI.IPoint, actionScopeType: action_scope_type ): void
    {
        locationToken.cCardToken.location = this._location;
        locationToken.cContainer.c.position.set( 
            -locationToken.cCardToken.calcWidth() * 0.5, 
            -locationToken.cCardToken.calcHeight() * 0.5 );
        locationToken.cDropArea.isPropagate = true;
        this._locationTokenContainer.addChild( locationToken.cContainer.c );
        //
        locationToken.cDraggable.onPostDropped.addOnce( this.onLocationToken_PostDropped, this );
        locationToken.cCardToken.onSizeUpdated.add( this.onLocationTokenSize_Updated, this );
        locationToken.cShareableGameElement.onLocked.add( this.onLocationToken_Locked, this );
        locationToken.cShareableGameElement.onUnlocked.add( this.onLocationToken_Unlocked, this );
        let sb: SignalBinding = locationToken.cCardToken.curSide.cLocationSide.questPoints.cTokenCounter.onCountUpdated.add( this.onLocationTokenProgress_Updated, this );
        sb.params = [ locationToken ];
        sb = locationToken.cCardToken.curSide.cLocationSide.progress.cTokenCounter.onCountUpdated.add( this.onLocationTokenProgress_Updated, this );
        sb.params = [ locationToken ];

        if ( this._arrLocationToken.length > 0 )
        {
            this._arrLocationToken[ this._multiItemSelector.cMultiItemSelector.selectedIndex ].cContainer.c.visible = false;
        }
        this._arrLocationToken.push( locationToken );
        this._multiItemSelector.cMultiItemSelector.addItem();
        this._multiItemSelector.cMultiItemSelector.setSelectedIndex( this._multiItemSelector.cMultiItemSelector.itemCount - 1 );
        this.updateMultiItemSelectorPosition();

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.DROP_AT_DROP_AREA, null, [ locationToken.oid, this._go.oid, null ] );
        }

        if ( this._isAnimated && ServiceLocator.game.isAnimated )
        {
            // Sfx.
            ServiceLocator.audioManager.playSfx( "token_dropped" );
            
            // Vfx.
            this.playDroppedVfx();
            Utils.anim.dropFromCorner( locationToken.cContainer.c );
        }
        
        this.notifyExploredStatus();
    }

        private updateMultiItemSelectorPosition(): void
        {
            this._multiItemSelector.cContainer.c.position.set(
                -this._multiItemSelector.cContainer.c.width * 0.5,
                -this._arrLocationToken[ this._multiItemSelector.cMultiItemSelector.selectedIndex ].cCardToken.cCurSide.bg.cContainer.c.height * 0.5 - 10 );
        }

        private playDroppedVfx(): void
        {
            let droppedEmitter: DroppedEmitter = new DroppedEmitter();
            const kVfxLayer: GameObject = Utils.game.findGameProviderLayer( this._go, layer_type.VFX );
            droppedEmitter.layer = kVfxLayer;
            droppedEmitter.init();
            const kVfxPos: PIXI.IPoint = kVfxLayer.cContainer.c.toLocal( this._bg.cContainer.c.getGlobalPosition() );
            droppedEmitter.playLarge( kVfxPos.x, kVfxPos.y );
        }

    // #endregion //


    // #region Callbacks //

    private onLocationTokenSize_Updated( go: GameObject ): void
    {
        go.cContainer.c.x = -go.cCardToken.calcWidth() * 0.5;
    }

    private onLocationToken_Locked( go: GameObject ): void
    {
        this._multiItemSelector.cMultiItemSelector.setEnabled( false );
    }

    private onLocationToken_Unlocked( go: GameObject ): void
    {
        this._multiItemSelector.cMultiItemSelector.setEnabled( true );
    }

    private onLocationToken_PostDropped( go: GameObject ): void
    {
        if ( go.cCardToken )
        {
            go.cShareableGameElement.onLocked.remove( this.onLocationToken_Locked, this );
            go.cShareableGameElement.onUnlocked.remove( this.onLocationToken_Unlocked, this );
            go.cCardToken.onSizeUpdated.remove( this.onLocationTokenSize_Updated, this );
            go.cCardToken.curSide.cLocationSide.progress.cTokenCounter.onCountUpdated.remove( this.onLocationTokenProgress_Updated, this );
            go.cCardToken.curSide.cLocationSide.questPoints.cTokenCounter.onCountUpdated.remove( this.onLocationTokenProgress_Updated, this );
        }

        this._arrLocationToken.splice( this._arrLocationToken.indexOf( go ), 1 );
        this._multiItemSelector.cMultiItemSelector.removeItem();

        if ( this._arrLocationToken.length > 0 )
        {
            this._arrLocationToken[ this._multiItemSelector.cMultiItemSelector.selectedIndex ].cContainer.c.visible = true;
            this.updateMultiItemSelectorPosition();
        }
       
        this.notifyExploredStatus();
    }

        private notifyExploredStatus(): void
        {
            let isExplored: boolean = true;
            for ( let locationTokenIt of this._arrLocationToken )
            {
                if ( !locationTokenIt.cCardToken.cCurSide.hasStatus( status_type.EXPLORED ) )
                {
                    isExplored = false;
                    break;
                }
            }
            this._onActiveLocationUpdated.dispatch( isExplored );
        }

    private onLocationTokenProgress_Updated( locationToken: GameObject ): void
    {
        this.notifyExploredStatus();
    }

    private onMultiItemSelectorItem_Selected( lastIndex: number, selIndex: number ): void
    {
        if ( lastIndex >= 0 )
        {
            this._arrLocationToken[ lastIndex ].cContainer.c.visible = false;
        }
        this._arrLocationToken[ selIndex ].cContainer.c.visible = true;

        this.updateMultiItemSelectorPosition();
    }

    // #endregion //
}