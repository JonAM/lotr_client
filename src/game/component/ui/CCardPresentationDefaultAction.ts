import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import { button_type } from "../../../states/game/setup/CardPresentationUtils";


export default class CCardPresentationDefaultAction extends Component
{
    // #region Attributes //

    // private:

    private _icon: PIXI.Sprite = null;
    private _buttonType: button_type = null;

    // #endregion //


    public get buttonType(): button_type { return this._buttonType; }


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CCardPresentationDefaultAction";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CCardPresentationDefaultAction.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cTooltipReceptor != null, "CCardPresentationDefaultAction.ts :: init() :: CTooltipReceptor component not found." );

        this._go.cContainer.c.alpha = 0.3;
        
        let bg: PIXI.Graphics = new PIXI.Graphics();
        bg.lineStyle( 1, 0x000000, 1 );
        bg.beginFill( 0xeccc68, 1 );
        bg.drawCircle( 0, 0, 60 );
        bg.endFill();
        this._go.cContainer.c.addChild( bg );

        this._icon = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( "transparent" ) );
        this._icon.anchor.set( 0.5 );
        this._go.cContainer.c.addChild( this._icon );
    }

    public end(): void
    {
        this._icon = null

        super.end();
    }

    public updateIcon( buttonType: button_type ): void
    {
        this._buttonType = buttonType;
        
        const kArrIconId: Array<string> = [ "hand", "rad_play", "rad_target", "rad_equip", "rad_underneath", "rad_shadow_card", "rad_discard", "rad_back", "rad_put_face_up" ];
        this._icon.texture = ServiceLocator.resourceStack.findAsTexture( kArrIconId[ this._buttonType ] );
        Utils.game.limitSideSize( 72, this._icon );

        const kArrTooltipId: Array<string> = [ "DRAW", "PLAY", "TRIGGER_ABILITY", "EQUIP", "PUT_UNDERNEATH", "ADD_AS_SHADOW_CARD", "DISCARD", "BACK", "FLIP" ];
        this._go.cTooltipReceptor.text = jQuery.i18n( kArrTooltipId[ this._buttonType ] );
    }

    // #endregion //
}