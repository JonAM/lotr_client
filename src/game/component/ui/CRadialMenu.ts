import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import Signal from "../../../lib/signals/Signal";
import CardController from "./radial_menu/CardController";
import AttachmentController from "./radial_menu/AttachmentController";
import CTooltipReceptor from "./CTooltipReceptor";
import CGraphics from "../pixi/CGraphics";
import CContainer from "../pixi/CContainer";
import ActivatedCardController from "./radial_menu/ActivatedCardController";
import ShadowCardMiniController from "./radial_menu/ShadowCardMiniController";
import CardTokenSideController from "./radial_menu/CardTokenSideController";
import GameModifierController from "./radial_menu/GameModifierController";
import SignalBinding from "../../../lib/signals/SignalBinding";
import Session from "../../../Session";
import { input_event_type } from "../../InputController";


export default class CRadialMenu extends Component
{
    // #region Attributes //

    // private:

    private _target: GameObject = null;
    private _bg: GameObject = null;
    private _navigationStack: Array<GameObject> = null;
    
    private _cardController: CardController = null;
    private _attachmentController: AttachmentController = null;
    private _cardTokenSideController: CardTokenSideController = null;
    private _activatedCardController: ActivatedCardController = null;
    private _shadowCardMiniController: ShadowCardMiniController = null;
    private _gameModifierController: GameModifierController = null;

    // Signals.
    private _onOptionSelected: Signal = new Signal();
    private _onHidden: Signal = new Signal();

    // Constants.
    private readonly _kButtonSize: number = 30;
    private readonly _kCircleRadius: number = 70;
    private readonly _kRingSize: number = 10;
    private readonly _kArrIconId: Array<string> = [ 
        "bow", "ready", "put_face_down", "put_face_up", "discard",
        "commit", "uncommit", "equip", "target", "shadow_card",
        "underneath", "sauron_attack", "player_attack", "defend", "extras",
        "back", "add_counter", "add_custom_text", "add_highlight", "remove_highlight" ];

    // #endregion //


    // #region Properties //

    public get target(): GameObject { return this._target; }

    // Signals.
    public get onOptionSelected(): Signal { return this._onOptionSelected; }
    public get onHidden(): Signal { return this._onHidden; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();
    
        this._id = "CRadialMenu";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CRadialMenu.ts :: init() :: CContainer component not found." );
    
        this._cardController = new CardController();
        this._attachmentController = new AttachmentController();
        this._cardTokenSideController = new CardTokenSideController();
        this._activatedCardController = new ActivatedCardController();
        this._shadowCardMiniController = new ShadowCardMiniController();
        this._gameModifierController = new GameModifierController();

        this._bg = new GameObject( [ new CGraphics() ] );
        this._bg.init();
        this._bg.cGraphics.g.lineStyle( 2, 0xFEEB77, 1 );
        this._bg.cGraphics.g.beginFill( 0x650A5A, 0.75 );
        this._bg.cGraphics.g.drawCircle( 0, 0, this._kCircleRadius + this._kRingSize );
        this._bg.cGraphics.g.beginHole();
        this._bg.cGraphics.g.drawCircle( 0, 0, this._kCircleRadius - this._kRingSize );
        this._bg.cGraphics.g.endHole();
        this._bg.cGraphics.g.endFill();
        this._go.cContainer.addChild( this._bg );

        this._go.cContainer.c.visible = false;
    }

    public end(): void
    {
        this._onOptionSelected.removeAll();
        this._onHidden.removeAll();

        this._cardController = null;
        this._attachmentController = null;
        this._cardTokenSideController = null;
        this._activatedCardController = null;
        this._shadowCardMiniController = null;
        this._gameModifierController = null;

        if ( this._target )
        {
            this._target.cContainer.onVisibleChanged.remove( this.onTargetVisible_Changed, this );
            this._target.cContainer.onTransformChanged.remove( this.hide, this );
            this._target = null;
        }
    }

    public tryShow( target: GameObject, position: PIXI.Point, arrOption: Array<IOption> ): void
    {
        if ( this._target == target ) { return; }

        // Multiplayer.
        if ( Session.allyId &&  target.cShareableGameElement && target.cShareableGameElement.isEnabled )
        {
            let sb: SignalBinding = target.cShareableGameElement.onLockOpen.addOnce( this.show, this );
            sb.params = [ target, position, arrOption ];
            target.cShareableGameElement.notifyLock();
        }
        else
        {
            this.show( target, position, arrOption );
        }
    }

        private show( target: GameObject, position: PIXI.Point, arrOption: Array<IOption> ): void
        {
            this._go.cContainer.c.position.set( position.x, position.y );
    
            this._target = target;
            this._target.cContainer.onVisibleChanged.addOnce( this.onTargetVisible_Changed, this );
            this._target.cContainer.onTransformChanged.addOnce( this.hide, this );
    
            this.clear();
    
            let mainMenu: GameObject = this.createMenu( arrOption );
            this._bg.cContainer.addChild( mainMenu );
            this._navigationStack = [ mainMenu ];
    
            this._go.cContainer.c.visible = true;
    
            // Listen to events.
            ServiceLocator.game.onOngoingInteractionCanceled.add( this.hide, this );
        }

    public hide()
    {
        if ( !this._go.cContainer.c.visible ) { return; }

        if ( this._target )
        {
            if ( this._target.cShareableGameElement )
            {
                this._target.cShareableGameElement.notifyUnlock();
            }

            this._target.cContainer.onVisibleChanged.remove( this.onTargetVisible_Changed, this );
            this._target.cContainer.onTransformChanged.remove( this.hide, this );

            this._target = null;
        }

        this._go.cContainer.c.visible = false;
        this.clear();
        
        this._onHidden.dispatch();

        // Cleanup events.
        ServiceLocator.game.onOngoingInteractionCanceled.remove( this.hide, this );
    }

    // private:

    private clear(): void
    {
        for ( let i: number = this._bg.cContainer.c.children.length - 1; i >= 0; --i )
        {
            let menuContainer: GameObject = this._bg.cContainer.c.getChildAt( i )[ "go" ] as GameObject;
            for ( let j: number = menuContainer.cContainer.c.children.length - 1; j >= 0; --j )
            {
                let menuOption: GameObject = menuContainer.cContainer.c.getChildAt( j )[ "go" ] as GameObject;
                menuOption.end();
            }
            menuContainer.end();
        }
    }

    private createMenuButton( option: IOption, position: PIXI.Point ): GameObject
    {
        let isSecondary: boolean = option.id == option_type.BACK || option.subMenu != null;
        const kSideSize: number = isSecondary ? this._kButtonSize * 0.75 : this._kButtonSize;

        let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( "rad_" + this._kArrIconId[ option.id ] ).data ) );
        icon.anchor.set( 0.5, 0.5 );
        Utils.game.limitSideSize( kSideSize, icon );
        if ( !option.isEnabled )
        {
            icon.alpha = 0.25;
        }

        let menuButton: GameObject = new GameObject( [ new CGraphics(), new CTooltipReceptor() ] );
        const kArrTooltip: Array<string> = this.findOptionTooltip( option.id );
        menuButton.cTooltipReceptor.text = kArrTooltip[ 0 ];
        menuButton.cTooltipReceptor.description = kArrTooltip[ 1 ];
        menuButton.init();
        menuButton.cGraphics.g.beginFill( 0xFFFFFF, 1 );
        if ( isSecondary )
        {
            menuButton.cGraphics.g.drawRoundedRect( -kSideSize, -kSideSize, kSideSize * 2, kSideSize * 2, 5 );
        }
        else
        {
            menuButton.cGraphics.g.drawCircle( 0, 0, kSideSize );
        }
        menuButton.cGraphics.g.endFill();
        if ( option.isEnabled )
        {
            menuButton.cGraphics.g.buttonMode = true;
            ServiceLocator.game.inputController.on( menuButton.cGraphics.g, input_event_type.TAP, this.onOption_PointerTap, this, option );
        }
        menuButton.cGraphics.g.interactive = true;
        menuButton.cGraphics.g.position.set( position.x, position.y );
        menuButton.cGraphics.g.addChild( icon );

        return menuButton;
    }

    private findOptionTooltip( optionType: option_type ): Array<string>
    {
        let result: Array<string> = [ null, null ];

        switch ( optionType )
        {
            case option_type.BOW: { result[ 0 ] = jQuery.i18n( "BOW" ); break; }
            case option_type.READY: { result[ 0 ] = jQuery.i18n( "READY" ); break; }
            case option_type.PUT_FACE_DOWN: { result[ 0 ] = jQuery.i18n( "PUT_FACE_DOWN" ); break; }
            case option_type.PUT_FACE_UP: { result[ 0 ] = jQuery.i18n( "REVEAL" ); break; }
            case option_type.DISCARD: { result[ 0 ] = jQuery.i18n( "DISCARD" ); break; }
            case option_type.COMMIT: { result[ 0 ] = jQuery.i18n( "COMMIT_TO_QUEST" ); break; }
            case option_type.UNCOMMIT: { result[ 0 ] = jQuery.i18n( "UNCOMMIT_FROM_QUEST" ); break; }
            case option_type.EQUIP: { result[ 0 ] = jQuery.i18n( "EQUIP" ); break; }
            case option_type.TARGET: { result[ 0 ] = jQuery.i18n( "TARGET" ); break; }
            case option_type.SHADOW_CARD: { result[ 0 ] = jQuery.i18n( "ADD_AS_SHADOW_CARD" ); break; }
            case option_type.UNDERNEATH: { result[ 0 ] = jQuery.i18n( "UNDERNEATH" ); break; }
            case option_type.SAURON_ATTACK: { result[ 0 ] = jQuery.i18n( "ATTACK" ); break; }
            case option_type.PLAYER_ATTACK: { result[ 0 ] = jQuery.i18n( "ATTACK" ); break; }
            case option_type.DEFEND: { result[ 0 ] = jQuery.i18n( "DEFEND" ); break; }
            case option_type.EXTRAS: { result[ 0 ] = jQuery.i18n( "EXTRAS" ); break; }
            case option_type.BACK: { result[ 0 ] = jQuery.i18n( "BACK" ); break; }
            case option_type.ADD_COUNTER: { result[ 0 ] = jQuery.i18n( "ADD_COUNTER" ); break; }
            case option_type.ADD_CUSTOM_TEXT: { result[ 0 ] = jQuery.i18n( "ADD_CUSTOM_TEXT" ); break; }
            case option_type.ADD_HIGHLIGHTING: { result[ 0 ] = jQuery.i18n( "ADD_HIGHLIGHTING" ); break; }
            case option_type.REMOVE_HIGHLIGHTING: { result[ 0 ] = jQuery.i18n( "REMOVE_HIGHLIGHTING" ); break; }
        }

        return result;
    }

    private createMenu( arrOption: Array<IOption> ): GameObject
    {
        let result: GameObject = new GameObject( [ new CContainer() ] );
        result.init();

        let m: PIXI.Matrix = new PIXI.Matrix();
        m.translate( 0, -this._kCircleRadius );
        let rotAngle: number = null;
        if ( arrOption.length > 1 )
        {
            rotAngle = 2 * Math.PI / arrOption.length;
        }
        for ( let i: number = 0; i < arrOption.length; ++i )
        {      
            result.cContainer.addChild( this.createMenuButton( arrOption[ i ], new PIXI.Point( m.tx, m.ty ) ) );
            
            if ( rotAngle )
            {
                m.rotate( rotAngle );
            }
        }

        return result;
    }

    private processOption( target: GameObject, option: IOption ): void
    {
        switch ( option.id )
        {
            case option_type.EXTRAS:
            {
                this._navigationStack[ this._navigationStack.length - 1 ].cContainer.c.visible = false;
                let subMenu: GameObject = this.createMenu( option.subMenu );
                this._bg.cContainer.addChild( subMenu );
                this._navigationStack.push( subMenu );
                break;
            }
            
            case option_type.BACK:
            {
                this._navigationStack.pop().end();
                this._navigationStack[ this._navigationStack.length - 1 ].cContainer.c.visible = true;
                break;
            }

            default:
            {
                let radialMenuControler: IRadialMenuController = null;
                if ( this._target.cCardTokenSide )
                {
                    radialMenuControler = this._cardTokenSideController;
                }
                else if ( this._target.cAttachment )
                {
                    radialMenuControler = this._attachmentController;
                }
                else if ( this._target.cCard )
                {
                    radialMenuControler = this._cardController;
                }
                else if ( this._target.cActivatedCard )
                {
                    radialMenuControler = this._activatedCardController;
                }
                else if ( this._target.cShadowCardMini )
                {
                    radialMenuControler = this._shadowCardMiniController;
                }
                else if ( this._target.cGameModifier )
                {
                    radialMenuControler = this._gameModifierController;
                }
                radialMenuControler.processOption( this._target, option );
                break;
            }
        }
    }

    // #endregion //


    // #region Input Callbacks //

    private onOption_PointerTap( option: IOption, event: PIXI.InteractionEvent ): void
    {
        if ( event && ServiceLocator.game.poiMenu.cContainer.c.visible ) { return; }

        // Sfx.
        ServiceLocator.audioManager.playSfx( "button_click" );

        // Cleanup before processing the selected option.
        if ( option.id != option_type.BACK && option.id != option_type.EXTRAS )
        {
            // Unlock only if the following action will not lock again.
            if ( this._target.cShareableGameElement
                && option.id != option_type.ADD_COUNTER
                && option.id != option_type.ADD_CUSTOM_TEXT
                && option.id != option_type.EQUIP
                && option.id != option_type.PLAYER_ATTACK
                && option.id != option_type.SAURON_ATTACK
                && option.id != option_type.TARGET
                && option.id != option_type.UNDERNEATH )
            {
                this._target.cShareableGameElement.notifyUnlock();
            }

            this._target.cContainer.onVisibleChanged.remove( this.onTargetVisible_Changed, this );
            this._target.cContainer.onTransformChanged.remove( this.hide, this );
        }

        this._onOptionSelected.dispatch( option.id, this._target );
        
        this.processOption( this._target, option );

        if ( option.id != option_type.BACK && option.id != option_type.EXTRAS )
        {
            this._target = null; // IMPORTANT: To avoid repeating the cleanup on hide().
            this.hide();
        }
    }

    // #endregion //


    // #region Other Callbacks //

    private onTargetVisible_Changed( isVisible: boolean ): void
    {
        if ( !isVisible )
        {
            this.hide();
        }
    }
}

export const enum option_type
{
    // Main.
    BOW = 0,
    READY,
    PUT_FACE_DOWN,
    PUT_FACE_UP,
    DISCARD,
    COMMIT,
    UNCOMMIT,
    EQUIP,
    TARGET,
    SHADOW_CARD,
    UNDERNEATH,
    SAURON_ATTACK,
    PLAYER_ATTACK,
    DEFEND,
    EXTRAS,
    BACK,
    // Extras.
    ADD_COUNTER,
    ADD_CUSTOM_TEXT,
    ADD_HIGHLIGHTING,
    REMOVE_HIGHLIGHTING
} 

export interface IOption
{
    id: option_type;
    isEnabled: boolean;
    subMenu?: Array<IOption>;
}

export interface IRadialMenuController
{
    processOption( target: GameObject, option: IOption ): void;
}