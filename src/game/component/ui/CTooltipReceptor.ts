import ServiceLocator from "../../../ServiceLocator";
import TouchInputController from "../../input/TouchInputController";
import { input_event_type } from "../../InputController";
import Component from "../Component";


export default class CTooltipReceptor extends Component
{
    // #region Attributes //

    // private:

    private _text: string = null;
    private _description: string = null;

    // #endregion //


    // #region Properties //
 
    public get text(): string { return this._text; }
    public get description(): string { return this._description; }

    public set text( value: string ) { this._text = value; }
    public set description( value: string ) { this._description = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CTooltipReceptor";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._text != null, "CTooltipReceptor.ts :: init() :: this._text cannot be null." );
    
        if ( ServiceLocator.game.inputController instanceof TouchInputController )
        {
            this._go.cContainer.c.interactive = true;

            ServiceLocator.game.inputController.on( this._go.cContainer.c, input_event_type.OVER, this.onPointer_Over, this );
            ServiceLocator.game.inputController.on( this._go.cContainer.c, input_event_type.OUT, this.onPointer_Out, this );
        }
    }

    public end(): void
    {
        if ( ServiceLocator.game.inputController instanceof TouchInputController )
        {
            ServiceLocator.game.inputController.off( this._go.cContainer.c, input_event_type.OVER, this.onPointer_Over, this );
            ServiceLocator.game.inputController.off( this._go.cContainer.c, input_event_type.OUT, this.onPointer_Out, this );
        }

        this._text = null;
        this._description = null;

        super.end();
    }

    // #endregion //


    private onPointer_Over( event: PIXI.InteractionEvent ): void
    {
        ServiceLocator.game.tooltipManager.triggerTooltip( this._go, event.data.global );
    }

    private onPointer_Out( event: PIXI.InteractionEvent ): void
    {
        ServiceLocator.game.tooltipManager.clearTooltip();
    }
}