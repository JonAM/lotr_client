import Component from "../Component";

import * as PIXI from "pixi.js";

import Signal from "../../../lib/signals/Signal";
import GameObject from "../../GameObject";
import CSprite from "../pixi/CSprite";
import CButton from "../input/CButton";
import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import Session from "../../../Session";
import SignalBinding from "../../../lib/signals/SignalBinding";


export default class CMultiItemSelector extends Component
{
    // #region Attributes //

    // private:

    private _maxItemCount: number = null;
    private _owner: GameObject = null;
    private _layoutType: mis_layout_type = null;

    private _bg: PIXI.Graphics = null;
    private _controls: PIXI.Container = null;
    
    private _selectedIndex: number = null;
    private _itemCount: number = null;
    private _arrItemButton: Array<GameObject> = null;

    private _isWaitingLockStatus: boolean = false;

    // Signals.
    private _onItemSelected: Signal = new Signal();

    // Constants.
    private _kBgHeight: number = 20;
    private _kItemWidth: number = 15;
    private _kItemMargin: number = 5;

    // #endregion //


    // #region Properties //

    public get selectedIndex(): number { return this._selectedIndex; }
    public get maxItemCount(): number { return this._maxItemCount; }
    public get owner(): GameObject { return this._owner; }
    public get itemCount(): number { return this._itemCount; }

    public set maxItemCount( value: number ) { this._maxItemCount = value; }
    public set owner( value: GameObject ) { this._owner = value; }
    public set layoutType( value: mis_layout_type ) { this._layoutType = value; }
    
    // // Signals.
    public get onItemSelected(): Signal { return this._onItemSelected; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CMultiItemSelector";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CMultiItemSelector.ts :: init() :: cContainer component not found." );
        console.assert( this._maxItemCount != null, "CMultiItemSelector.ts :: init() :: this._maxItemCount cannot be null." );
        console.assert( this._owner != null, "CMultiItemSelector.ts :: init() :: this._owner cannot be null." );
        console.assert( this._layoutType != null, "CMultiItemSelector.ts :: init() :: this._layoutType cannot be null." );

        this._go.cContainer.c.visible = false;

        this._selectedIndex = -1;
        this._itemCount = 0;
        this._arrItemButton = new Array<GameObject>();

        this._bg = new PIXI.Graphics();
        this._bg.interactive = true;
        this._go.cContainer.c.addChild( this._bg );

        this._controls = new PIXI.Container();
        this._go.cContainer.c.addChild( this._controls );

        for ( let i: number = 0; i < this._maxItemCount; ++i )
        {
            let button: GameObject = new GameObject( [ new CSprite(), new CButton() ] );
            button.cSprite.s.anchor.set( 0.5, 0.5 );
            button.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "black_pearl" ).data );
            Utils.game.limitSideSize( this._kItemWidth, button.cSprite.s );
            button.init();
            if ( this._layoutType == mis_layout_type.HORIZONTAL )
            {
                button.cContainer.c.x = i * ( this._kItemMargin + this._kItemWidth ) + this._kItemMargin + this._kItemWidth * 0.5;
                button.cContainer.c.y = this._kBgHeight * 0.5;
            }
            else
            {
                button.cContainer.c.y = i * ( this._kItemMargin + this._kItemWidth ) + this._kItemMargin + this._kItemWidth * 0.5;
                button.cContainer.c.x = this._kBgHeight * 0.5;
            }
            button.cContainer.c.visible = false;
            this._controls.addChild( button.cContainer.c );
            button.cButton.onClick.add( this.onItemBtn_Click.bind( this, i ) );

            this._arrItemButton.push( button );
        }
    }

    public end(): void
    {
        this._onItemSelected.removeAll();

        for ( let itemButton of this._arrItemButton )
        {
            itemButton.end();
        }
        this._arrItemButton = null;

        this._bg.parent.removeChild( this._bg );
        this._bg = null;

        this._controls.parent.removeChild( this._controls );
        this._controls = null;
    }

    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        for ( let button of this._arrItemButton )
        {
            button.cButton.setEnabled( isEnabled );
        }
    }

    public addItem(): void
    {
        this._itemCount += 1;
        if ( this._itemCount == 1 )
        {
            this._selectedIndex = this._itemCount - 1;
        }

        let addedItemButton: GameObject = this._arrItemButton[ this._itemCount - 1 ];
        addedItemButton.cContainer.c.visible = true;
        addedItemButton.cContainer.c.interactive = this._itemCount != 1;
        addedItemButton.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( addedItemButton.cContainer.c.interactive ? "black_pearl" : "white_pearl" ).data );

        if ( this._itemCount > 1 )
        {
            this._go.cContainer.c.visible = true;
        }

        this.updateBg();
    }

    public removeItem(): void
    {
        this._arrItemButton[ this._itemCount - 1 ].cContainer.c.visible = false;

        this._itemCount -= 1;
        if ( this._selectedIndex > this._itemCount - 1 )
        {
            this._selectedIndex = this._itemCount - 1;
            if ( this._selectedIndex >= 0 )
            {
                let curSelectedButton: GameObject = this._arrItemButton[this._selectedIndex ];
                curSelectedButton.cContainer.c.visible = true;
                curSelectedButton.cContainer.c.interactive = false;
                curSelectedButton.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "white_pearl" ).data );
            }
        }
        
        if ( this._itemCount <= 1 )
        {
            this._go.cContainer.c.visible = false;
        }

        this.updateBg();
    }

    public setSelectedIndex( index: number ): void
    {
        if ( index < 0 ) { return; }

        while ( index > this._itemCount - 1 )
        {
            this.addItem();
        }

        const kLastCardIndex: number = this._selectedIndex;
        if ( kLastCardIndex >= 0 )
        {
            this._arrItemButton[ this._selectedIndex ].cContainer.c.interactive = true;
            this._arrItemButton[ this._selectedIndex ].cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "black_pearl" ).data );
        }
        
        this._selectedIndex = index;
        this._arrItemButton[ index ].cContainer.c.interactive = false;
        this._arrItemButton[ index ].cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "white_pearl" ).data );

    }

    public selectItem( index: number, actionScope: action_scope_type ): void
    {
        const kLastCardIndex: number = this._selectedIndex;
        this.setSelectedIndex( index );

        this._onItemSelected.dispatch( kLastCardIndex, index );

        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SELECT_MULTI_ITEM_SELECTOR, null, [ this._go.oid, index ] );
        }
    }

    // private:

    private updateBg(): void
    {
        this._bg.clear();
        this._bg.lineStyle( 1, 0x000000, 1 );
        this._bg.beginFill( 0xffa502, 0.6 );
        if ( this._layoutType == mis_layout_type.HORIZONTAL )
        {
            this._bg.drawRect( 0, 0, this._itemCount * ( this._kItemWidth + this._kItemMargin ) + this._kItemMargin, this._kBgHeight  ); 
        }
        else
        {
            this._bg.drawRect( 0, 0, this._kBgHeight, this._itemCount * ( this._kItemWidth + this._kItemMargin ) + this._kItemMargin  ); 
        }
        this._bg.endFill();
    }

    // #endregion //
    

    // #region Callbacks //

    private onItemBtn_Click( index: number ): void
    {
        if ( !this._isEnabled ) { return; }

        ServiceLocator.game.cRadialMenu.hide();

        this.trySelectItem( index );
    }

        private trySelectItem( index: number ): void
        {
            // Multiplayer.
            if ( Session.allyId && this._go.cShareableGameElement )
            {
                if ( !this._isWaitingLockStatus && this._go.cShareableGameElement.isEnabled )
                {
                    this._isWaitingLockStatus = true;

                    let sb: SignalBinding = this._go.cShareableGameElement.onLockStatusReceived.addOnce(this.onLockStatus_Received, this );
                    sb.params = [ index ];
                    this._go.cShareableGameElement.notifyLock();
                }
            }
            else
            {
                this.selectItem( index, action_scope_type.MULTIPLAYER );
            }
        }

            private onLockStatus_Received( selItemIndex: number, isLockOpen: boolean ): void
            {
                this._isWaitingLockStatus = false;

                if ( isLockOpen )
                {
                    this.selectItem( selItemIndex, action_scope_type.MULTIPLAYER );

                    this._go.cShareableGameElement.notifyUnlock();
                }
            }

    // #endregion //
}

export const enum mis_layout_type
{
    HORIZONTAL = 0,
    VERTICAL
}