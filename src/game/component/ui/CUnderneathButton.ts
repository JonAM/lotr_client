import Component from "../Component";

import CCardView, { deck_side_type, face_policy_type } from "./viewer/CCardView";
import { location_type } from "../world/CGameWorld";
import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CContainer from "../pixi/CContainer";
import CSprite from "../pixi/CSprite";
import CButton from "../input/CButton";
import CDeckIndicator from "./indicator/CDeckIndicator";
import CardViewerFactory from "../../CardViewerFactory";
import CDeckIndicatorPoiReceptor from "../world/poi_receptor/CDeckIndicatorPoiReceptor";
import CDropArea from "../input/CDropArea";
import IGameObjectDropArea from "../../IGameObjectDropArea";
import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import LogTargetCard from "./right_menu/action_log/target/LogTargetCard";
import { player_type } from "../world/CPlayerArea";
import Signal from "../../../lib/signals/Signal";
import CIndicatorBusy from "./indicator/CIndicatorBusy";
import { layer_type } from "../world/CGameLayerProvider";
import SignalBinding from "../../../lib/signals/SignalBinding";
import Session from "../../../Session";
import CustomDeckController from "./viewer/controller/CustomDeckController";


export default class CUnderneathButton extends Component implements IGameObjectDropArea
{
    // #region Attributes //

    // private:

    private _ownerToken: GameObject = null;
    private _sideLength: number = null;

    private _button: GameObject = null;
    private _text: PIXI.Text = null;
    private _deckIndicator: GameObject = null;

    // Signals.
    private _onItemCountUpdated: Signal = new Signal();

    // #endregion //


    // #region Propeties //

    public get cDeckIndicator(): CDeckIndicator { return this._deckIndicator.cDeckIndicator; }

    public set ownerToken( value: GameObject ) { this._ownerToken = value; }
    public set sideLength( value: number ) { this._sideLength = value; }

    // Signals.
    public get onItemCountUpdated(): Signal { return this._onItemCountUpdated; }
    
    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CUnderneathButton";
    }

    public init(): void
    {        
        console.assert( this._ownerToken != null, "CUnderneathButton.ts :: init() :: this._ownerToken cannot be null." );
        console.assert( this._sideLength != null, "CUnderneathButton.ts :: init() :: this._sideLength cannot be null." );
        console.assert( this._go.cContainer != null, "CUnderneathButton.ts :: init() :: CContainer component not found." );
    
        super.init();

        let g: PIXI.Graphics = new PIXI.Graphics();
        g.drawRect( 0, 0, this._sideLength, this._sideLength );
        this._go.cContainer.c.addChild( g );

        this._button = new GameObject( [ new CSprite(), new CButton(), new CDropArea() ] );
        this._button.oid = this._ownerToken.oid + "_underneath_btn";
        this._button.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "rad_underneath" ).data );  
        this._button.cSprite.s.anchor.set(  0.5 );
        Utils.game.limitSideSize( this._sideLength, this._button.cSprite.s );
        this._button.cDropArea.target = this;
        this._button.init();
        this._button.cContainer.c.position.set( this._go.cContainer.c.width * 0.5, this._go.cContainer.c.height * 0.5 );
        this._go.cContainer.addChild( this._button );

        this._text = new PIXI.Text( "0", ServiceLocator.game.textStyler.small );
        this._text.anchor.set( 0.9, 0.9 );
        this._text.position.set( this._go.cContainer.c.width, this._go.cContainer.c.height );
        this._go.cContainer.c.addChild( this._text );

        this._deckIndicator = this.createDeckIndicator();
        this._deckIndicator.cDeckIndicator.cViewer.cCardView.onItemCountUpdated.add( this.onItemCount_Updated, this );

        // Listen to events.
        this._button.cButton.onClick.add( this.onClick, this );
    }

        private createDeckIndicator(): GameObject
        {
            let title: string = null;
            if ( this._ownerToken.cCardToken )
            {
                title = this._ownerToken.cCardToken.cFrontSide.cardInfo.name;
            }
            else if ( this._ownerToken.cGameModifier )
            {
                title = this._ownerToken.cGameModifier.front.cardInfo.name;
            }

            let location: location_type = null;
            if ( this._ownerToken.cCardToken && this._ownerToken.cCardToken.ownerPlayer != player_type.ALLY
                || this._ownerToken.cGameModifier && this._ownerToken.cGameModifier.controllerPlayer != player_type.ALLY )
            {
                location = location_type.MY_UNDERNEATH;
            }
            else
            {
                location = location_type.ALLY_UNDERNEATH;
            }

            let playerType: player_type = null;
            if ( this._ownerToken.cCardToken )
            {
                playerType = this._ownerToken.cCardToken.ownerPlayer;
            }
            else if ( this._ownerToken.cGameModifier )
            {
                playerType = this._ownerToken.cGameModifier.controllerPlayer;
            }

            let cardViewerFactory: CardViewerFactory = new CardViewerFactory();
            let cardViewer: GameObject = cardViewerFactory.create( { 
                oid: this._ownerToken.oid + "_card_viewer",
                title: title,
                location: location,
                playerType: playerType,
                customController: new CustomDeckController(),
                facePolicy: face_policy_type.NONE } );
            ServiceLocator.game.root.cGameLayerProvider.add( cardViewer.cContainer.c.parent[ "go" ] as GameObject, layer_type.HUD );

            let cardsUnderneath: GameObject = new GameObject( [ new CContainer(), new CDeckIndicator(), new CDeckIndicatorPoiReceptor() ] );
            cardsUnderneath.oid = this._ownerToken.oid + "_cards_underneath";
            cardsUnderneath.cDeckIndicator.panel = cardViewer;
            cardsUnderneath.init();

            return cardsUnderneath;
        }

    public end(): void
    {   
        this._onItemCountUpdated.removeAll();

        this._deckIndicator.cDeckIndicator.cViewer.cCardView.onItemCountUpdated.remove( this.onItemCount_Updated, this );

        this._button.end();
        this._button = null;

        Utils.anim.flushTweensOf( this._deckIndicator.cDeckIndicator.cViewer.cCardView.go.cContainer.c );
        this._deckIndicator.end();
        this._deckIndicator = null;
    }

    public discard( actionScope: action_scope_type ): void
    {
        let arrCard: Array<GameObject> = this._deckIndicator.cDeckIndicator.cViewer.cCardView.findItems();
        for ( let i: number = arrCard.length - 1; i >= 0; --i )
        {
            arrCard[ i ].cCard.discard( actionScope );
        }
    }

    // #endregion //


    // #region IGameObjectDropArea //

    public validateDroppedGameObject( dropped: GameObject ): boolean
    {
        return this._deckIndicator.cDeckIndicator.cViewer.cCardView.validateDroppedGameObject( dropped );
    }

    public processDroppedGameObject( dropped: GameObject, global: PIXI.IPoint, actionScope: action_scope_type ): void
    {
        // Log.
        if ( actionScope == action_scope_type.MULTIPLAYER && global != null )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logTargetSelection( 
                player_type.PLAYER, new LogTargetCard( dropped ), new LogTargetCard( this._ownerToken ), "rad_underneath", false, true );
        }

        let cCardView: CCardView = this._deckIndicator.cDeckIndicator.cViewer.cCardView;
        cCardView.processDroppedGameObject( dropped, CCardView.findPredefinedDropPosition( deck_side_type.TOP ), action_scope_type.LOCAL );

        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.DROP_AT_DROP_AREA, null, [ dropped.oid, this._button.oid, this._go.cContainer.c.toLocal( global ) ] );
        }

        // Sfx.
        ServiceLocator.audioManager.playSfx( "token_dropped" );
    }

    // #endregion //


    // #region Callbacks //

    // private:

    private onClick(): void
    { 
        if ( !this._isEnabled ) { return; }

        // Multiplayer.
        if ( Session.allyId 
            && this._ownerToken.cShareableGameElement
            && this._ownerToken.cShareableGameElement.isEnabled
            && !this._deckIndicator.cDeckIndicator.cViewer.isVisible() )
        {
            this._ownerToken.cShareableGameElement.onLockOpen.addOnce( () => {
                let notifyUnlock: () => void = () => {
                    this._ownerToken.cShareableGameElement.notifyUnlock();
                };
                this._deckIndicator.cDeckIndicator.cViewer.onClosed.addOnce( notifyUnlock, this );
                
                this._deckIndicator.cDeckIndicator.onDeckButton_Click();
            } );
            this._ownerToken.cShareableGameElement.notifyLock();
        }
        else
        {
            this._deckIndicator.cDeckIndicator.onDeckButton_Click();
        }
    }

    private onItemCount_Updated( count: number ): void
    {
        this._text.text = count.toString();

        this._onItemCountUpdated.dispatch( count );
    }

    // #endregion //
}