import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";
import * as particles from "pixi-particles";
import { player_type } from "../world/CPlayerArea";
import { ISgEndTurnBtn } from "../../../view/game/SaveGameView";
import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import GameObject from "../../GameObject";
import CContainer from "../pixi/CContainer";
import { game_state_id } from "../../../states/StateId";
import { layer_type } from "../world/CGameLayerProvider";
import { input_event_type } from "../../InputController";


export default class CEndTurnButton extends Component
{
    // #region Attributes //

    // private:

    private _playerType: player_type = null;

    private _bg: PIXI.Sprite = null;
    private _text: PIXI.Text = null;
    private _icon: PIXI.Sprite = null;
    private _state: end_turn_btn_state = null;
    private _poiSocket: GameObject = null;

    private _textLocaleId: string = null;
    private _completedFlashInterval: number = null;

    // #endregion //


    // #region Properties //

    public get state(): end_turn_btn_state { return this._state; }

    public set playerType( value: player_type ) { this._playerType = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CEndTurnButton";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CEndTurnButton.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cButton != null, "CEndTurnButton.ts :: init() :: CButton component not found." );
        console.assert( this._playerType != null, "CEndTurnButton.ts :: init() :: this._playerType cannot be null." );
        
        this._bg = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( "end_turn_btn" ) );
        this._bg.anchor.set( 0.5 );
        Utils.game.limitSideSize( 120, this._bg );
        this._go.cContainer.c.addChild( this._bg );

        let textStyle: PIXI.TextStyle = ServiceLocator.game.textStyler.normal;
        textStyle.wordWrap = true;
        textStyle.align = "center";
        textStyle.wordWrapWidth = this._go.cContainer.c.width * 0.7;
        this._text = new PIXI.Text( "", textStyle );
        this._text.anchor.set( 0.5 );
        this._go.cContainer.c.addChild( this._text );
        
        this._icon = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( "transparent" ) );
        this._icon.anchor.set( 0.5 );
        Utils.game.limitSideSize( 70, this._icon );
        this._go.cContainer.c.addChild( this._icon );

        // POI socket.
        this._poiSocket = new GameObject( [ new CContainer() ] );
        this._poiSocket.cContainer.c.name = "poi_socket";
        this._poiSocket.init();
        this._poiSocket.cContainer.c.y -= this._go.cContainer.c.height * 0.5 - 5;
        this._go.cContainer.addChild( this._poiSocket );
        //
        this._go.cHighlightPoiReceptor.poiSocket = this._poiSocket;

        // Listen to events.
        ServiceLocator.game.inputController.on( this._go.cContainer.c, input_event_type.DOWN, this.onPointer_Down, this );
        ServiceLocator.game.inputController.on( this._go.cContainer.c, input_event_type.UP, this.onPointer_Up, this );
        ServiceLocator.game.inputController.on( this._go.cContainer.c, input_event_type.OUT, this.onPointer_Up, this );
        ServiceLocator.game.keyBindingManager.onKeyUp.add( this.onBindedKey_Up, this );
    }

    public postInit(): void
    {
        // Listen to events.
        ServiceLocator.game.cGameWorld.cSauronArea.cStaging.onContentChanged.add( this.onGamePhaseDiagramChanged, this );
        ServiceLocator.game.cGameWorld.cAllyArea.cEngaged.onContentChanged.add( this.onGamePhaseDiagramChanged, this );
        ServiceLocator.game.cGameWorld.cPlayerArea.cEngaged.onContentChanged.add( this.onGamePhaseDiagramChanged, this );
    }

    public end(): void
    {
        ServiceLocator.game.keyBindingManager.onKeyUp.remove( this.onBindedKey_Up, this );
        ServiceLocator.game.inputController.off( this._go.cContainer.c, input_event_type.DOWN, this.onPointer_Down, this );
        ServiceLocator.game.inputController.off( this._go.cContainer.c, input_event_type.UP, this.onPointer_Up, this );
        ServiceLocator.game.inputController.off( this._go.cContainer.c, input_event_type.OUT, this.onPointer_Up, this );

        this.clearCompletedFlashInterval();

        this._textLocaleId = null;

        this._poiSocket.end();
        this._poiSocket = null;

        super.end();
    }

    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        this._go.cButton.setEnabled( isEnabled );
    }

    public setText( localeId: string ): void
    {
        this._textLocaleId = localeId;

        this.clearCompletedFlashInterval();

        if ( localeId == "TO_PLANNING_PHASE"
            || localeId == "TO_QUEST_PHASE"
            || localeId == "TO_TRAVEL_PHASE"
            || localeId == "TO_ENCOUNTER_PHASE"
            || localeId == "TO_COMBAT_PHASE"
            || localeId == "TO_REFRESH_PHASE" )
        {
            this._text.style.fill = [
                "white",
                "#" + ( "00" + ServiceLocator.game.playerColors[ this._playerType ].toString( 16 ) ).substr( -6 ) ];
            this._text.style.fillGradientStops = [ 0.4 ];
        }
        else
        {
            this._text.style.fill = "white";
            this._text.style.fillGradientStops = null;
        }

        this._text.text = jQuery.i18n( localeId );

        this._icon.visible = false;
        this._text.visible = true;

        this._state = end_turn_btn_state.TEXT;
    }

    public setWaiting( actionScope: action_scope_type ): void
    {
        this.clearCompletedFlashInterval();

        this._icon.texture = ServiceLocator.resourceStack.findAsTexture( "player_waiting" );
        Utils.game.limitSideSize( 70, this._icon );

        this._state = end_turn_btn_state.WAITING;

        this.setIconState( this._state, actionScope );
    }

        private setIconState( state: end_turn_btn_state, actionScope: action_scope_type ): void
        {
            this._text.visible = false;
            this._icon.visible = true;

            // Multiplayer.
            if ( actionScope == action_scope_type.MULTIPLAYER )
            {
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_END_TURN_BTN_STATE, ServiceLocator.game.stateMachine.currentStateId, [ state ] );
            }
        }

    public toggleCompleted( actionScope: action_scope_type ): void
    {
        this.clearCompletedFlashInterval();

        if ( this._state == end_turn_btn_state.COMPLETED )
        {
            this._icon.visible = false;
            this._text.visible = true;
    
            this._state = end_turn_btn_state.TEXT;

            // Multiplayer.
            if ( actionScope == action_scope_type.MULTIPLAYER )
            {
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_END_TURN_BTN_STATE, ServiceLocator.game.stateMachine.currentStateId, [ end_turn_btn_state.COMPLETED ] );
            }
        }
        else
        {
            this._icon.texture = ServiceLocator.resourceStack.findAsTexture( "player_completed" );
            Utils.game.limitSideSize( 70, this._icon );

            this._state = end_turn_btn_state.COMPLETED;

            if ( this._playerType == player_type.ALLY )
            {
                const kLayer: GameObject = ServiceLocator.game.world.cGameLayerProvider.get( layer_type.VFX );
                Utils.anim.flashCircle( this._icon.getGlobalPosition(), this._icon.width * 0.5, 1.5, kLayer );
                this._completedFlashInterval = window.setInterval( () => { 
                    Utils.anim.flashCircle( this._icon.getGlobalPosition(), this._icon.width * 0.5, 1.5, kLayer ); }, 1500 );
            }

            this.setIconState( this._state, actionScope );
        }
    }

    public onGamePhaseDiagramChanged(): void
    {
        if ( ServiceLocator.game.stateMachine.currentStateId > game_state_id.SETUP
            && ServiceLocator.game.stateMachine.currentStateId < game_state_id.REFRESH )
        {
            let curStateId: number = ServiceLocator.game.stateMachine.currentStateId;
            let parentStateId: game_state_id = null;
            const kSubstateId: number = Utils.game.findGameSubstateId();
            if ( kSubstateId != null )
            {
                parentStateId = curStateId;
                curStateId = kSubstateId;
            }
            
            if ( this._state == end_turn_btn_state.TEXT )
            {
                const kEndTurnButtonText: string = ServiceLocator.game.phaseNavigator.findNexEndTurnButtonI18n( curStateId, parentStateId );
                this.setText( kEndTurnButtonText );
            }
        }
    }

    // overrides.
    public saveGame(): ISgEndTurnBtn
    {
        let result: ISgEndTurnBtn = { state: this._state };
        if ( this._state == end_turn_btn_state.TEXT )
        {
            result.textLocaleId = this._textLocaleId;
        }

        return result;
    }

    // overrides.
    public loadGame( sgEndTurnBtn: ISgEndTurnBtn, pass: number ): void
    {
        switch ( sgEndTurnBtn.state )
        {
            case end_turn_btn_state.TEXT: { this.setText( sgEndTurnBtn.textLocaleId ); break; }
            case end_turn_btn_state.WAITING: { this.setWaiting( action_scope_type.LOCAL ); break; }
            case end_turn_btn_state.COMPLETED: { this.toggleCompleted( action_scope_type.LOCAL ); break; }
        }
    }

    // private: 

    private clearCompletedFlashInterval(): void
    {
        if ( this._completedFlashInterval != null )
        {
            clearInterval( this._completedFlashInterval );
            this._completedFlashInterval = null;
        }
    }

    // #endregion //


    // #region Input Callbacks //

    private onPointer_Down( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled ) { return; }

        this._go.cContainer.c.scale.set( 0.9 );
    }

    private onPointer_Up( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled ) { return; }

        this._go.cContainer.c.scale.set( 1 );
    }

    // #endregion //


    // #region Other Callbacks //

    private onBindedKey_Up( bindingId: string ): void
    {
        if ( bindingId == "endTurn" )
        {
            if ( this._isEnabled && this._state != end_turn_btn_state.WAITING )
            {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "button_click" );

                this._go.cButton.onClick.dispatch( this._go, action_scope_type.MULTIPLAYER, new PIXI.InteractionEvent() );
            }
        }
    }

    // #endregion //
}

export const enum end_turn_btn_state
{
    TEXT = 0,
    WAITING,
    COMPLETED
}