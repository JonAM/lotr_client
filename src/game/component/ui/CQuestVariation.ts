import Component from "../Component";

import { player_type } from "../world/CPlayerArea";
import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import { ISgQuestVariation } from "../../../view/game/SaveGameView";


export default class CQuestVariation extends Component
{
    // #region Attributes //

    // private:

    private _playerWillpowerCounter: GameObject = null;
    private _allyWillpowerCounter: GameObject = null;
    private _sauronWillpowerCounter: GameObject = null;
    private _stagingThreatCounter: GameObject = null;
    
    private _container: PIXI.Container = null;
    private _text: PIXI.Text = null;
    private _icon: PIXI.Sprite = null;

    private _stagingThreat: number = null;
    private _arrPlayerWillpower: Array<number> = null;

    // #endregion //


    public set playerWillpowerCounter( value: GameObject ) { this._playerWillpowerCounter = value; }
    public set allyWillpowerCounter( value: GameObject ) { this._allyWillpowerCounter = value; }
    public set sauronWillpowerCounter( value: GameObject ) { this._sauronWillpowerCounter = value; }
    public set stagingThreatCounter( value: GameObject ) { this._stagingThreatCounter = value; }


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CQuestVariation";
    }
    
    public init(): void
    {
        console.assert( this._go.cContainer != null, "CQuestVariation.ts :: init() :: this._go.cContainer cannot be null." );
        console.assert ( this._stagingThreatCounter != null, "CQuestVariation.ts :: init() :: this._stagingThreatCounter cannot be null." );

        super.init();

        this._stagingThreat = 0;
        this._arrPlayerWillpower = [ 0, 0, 0 ];

        // Container.
        this._container = new PIXI.Container();
        this._container.visible = false;
        this._go.cContainer.c.addChild( this._container );
        
        // Icon.
        this._icon = new PIXI.Sprite();
        this._icon.anchor.set( 0.5 );
        this._container.addChild( this._icon );
        
        // Text.
        this._text = new PIXI.Text( "", ServiceLocator.game.textStyler.normal );
        this._text.anchor.set( 0.5 );
        this._container.addChild( this._text );

        this._go.cContainer.c.visible = false;

        // Listen to events.
        if ( this._playerWillpowerCounter )
        {
            this._playerWillpowerCounter.cPlayerWillpowerCounter.counter.cTokenCounter.onCountUpdated.add( this.onPlayerWillpowerCounter_Updated, this );
        }
        if ( this._allyWillpowerCounter )
        {
            this._allyWillpowerCounter.cPlayerWillpowerCounter.counter.cTokenCounter.onCountUpdated.add( this.onAllyWillpowerCounter_Updated, this );
        }
        if ( this._sauronWillpowerCounter )
        {
            this._sauronWillpowerCounter.cPlayerWillpowerCounter.counter.cTokenCounter.onCountUpdated.add( this.onSauronWillpowerCounter_Updated, this );
        }
        this._stagingThreatCounter.cStagingThreatCounter.counter.cTokenCounter.onCountUpdated.add( this.onStagingThreatCounter_Updated, this );
    }

    public end(): void
    {
        // Cleanup events.
        if ( this._playerWillpowerCounter )
        {
            this._playerWillpowerCounter.cPlayerWillpowerCounter.counter.cTokenCounter.onCountUpdated.remove( this.onPlayerWillpowerCounter_Updated, this );
        } 
        if ( this._allyWillpowerCounter )
        {
            this._allyWillpowerCounter.cPlayerWillpowerCounter.counter.cTokenCounter.onCountUpdated.remove( this.onAllyWillpowerCounter_Updated, this );
        }
        if ( this._sauronWillpowerCounter )
        {
            this._sauronWillpowerCounter.cPlayerWillpowerCounter.counter.cTokenCounter.onCountUpdated.remove( this.onSauronWillpowerCounter_Updated, this );
        }
        this._stagingThreatCounter.cStagingThreatCounter.counter.cTokenCounter.onCountUpdated.remove( this.onStagingThreatCounter_Updated, this );

        this._playerWillpowerCounter = null;
        this._allyWillpowerCounter = null;
        this._sauronWillpowerCounter = null;
        this._stagingThreatCounter = null;

        super.end();
    }

    public updateQuestVariation(): void
    {
        let totalWillpower: number = 0;
        if ( this._playerWillpowerCounter )
        {
            totalWillpower += this._arrPlayerWillpower[ player_type.PLAYER ];
        }
        if ( this._allyWillpowerCounter )
        {
            totalWillpower += this._arrPlayerWillpower[ player_type.ALLY ];
        }
        if ( this._sauronWillpowerCounter )
        {
            totalWillpower += this._arrPlayerWillpower[ player_type.SAURON ];
        }
        const kQuestVariation: number = this._stagingThreat - totalWillpower;
        this._icon.texture = ServiceLocator.resourceStack.findAsTexture( kQuestVariation > 0 ? "threat_level" : "progress_token" );
        Utils.game.limitSideSize( 40, this._icon );
        this._text.text = "+" + Math.abs( kQuestVariation ).toString();

        this._container.visible = kQuestVariation != 0;

        if ( this._go.cContainer.c.visible )
        {
            Utils.anim.bump( this._container );
        }
    }

    public saveGame(): ISgQuestVariation
    {
        return { 
            isPlayerWillpowerCounter: this._playerWillpowerCounter != null,
            isAllyWillpowerCounter: this._allyWillpowerCounter != null,
            isSauronWillpowerCounter: this._sauronWillpowerCounter != null };
    }

    public loadGame( sgQuestVariation: ISgQuestVariation, pass: number ): void
    {
        if ( !sgQuestVariation.isPlayerWillpowerCounter )
        {
            this._playerWillpowerCounter = null;
        }
        if ( !sgQuestVariation.isAllyWillpowerCounter )
        {
            this._allyWillpowerCounter = null;
        }
        if ( !sgQuestVariation.isSauronWillpowerCounter )
        {
            this._sauronWillpowerCounter = null;
        }
        this.updateQuestVariation();
    }

    // #endregion //


    // #region Callbacks //

    private onPlayerWillpowerCounter_Updated( count: number ): void
    {
        this._arrPlayerWillpower[ player_type.PLAYER ] = count;
        this.updateQuestVariation();
    }

    private onAllyWillpowerCounter_Updated( count: number ): void
    {
        this._arrPlayerWillpower[ player_type.ALLY ] = count;
        this.updateQuestVariation();
    }

    private onSauronWillpowerCounter_Updated( count: number ): void
    {
        this._arrPlayerWillpower[ player_type.SAURON ] = count;
        this.updateQuestVariation();
    }

    private onStagingThreatCounter_Updated( count: number ): void
    {
        this._stagingThreat = count;
        this.updateQuestVariation();
    }

    // #endregion //
}