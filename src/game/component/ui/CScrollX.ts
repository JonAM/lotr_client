import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CContainer from "../pixi/CContainer";
import CDampedString from "../CDampedSpring";
import Signal from "../../../lib/signals/Signal";
import Utils from "../../../Utils";
import CGraphics from "../pixi/CGraphics";
import CButton from "../input/CButton";
import TouchInputController from "../../input/TouchInputController";


export default class CScrollX extends Component
{
    // #region Attributes //

    // private:

    private _width: number = null;
    private _height: number = null;
    private _itemWidth: number = null;

    private _content: GameObject = null;
    private _upButton: GameObject = null;
    private _downButton: GameObject = null;
    private _scrollBar: PIXI.Graphics = null;
    private _selStep: number = 0;
    private _ticker: PIXI.Ticker = null;
    private _autoScrollTo: GameObject = null;

    // Binded functions.
    private _bfOnAppViewMouseWheel: ( ev: WheelEvent ) => void = null;

    // Signals.
    private _onAutoScrollCompleted: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public set width( value: number ) { this._width = value; }
    public set height( value: number ) { this._height = value; }
    public set itemWidth( value: number ) { this._itemWidth = value; }

    // Signals.
    public get onAutoScrollCompleted(): Signal { return this._onAutoScrollCompleted; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CScrollX";

        // Binded functions.
        this._bfOnAppViewMouseWheel = this.onAppView_MouseWheel.bind( this );
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CScrollX.ts :: init() :: CContainer component not found." );
        console.assert( this._width != null, "CScrollX.ts :: init() :: this._width cannot be null." );
        console.assert( this._height != null, "CScrollX.ts :: init() :: this._height cannot be null." );
        console.assert( this._itemWidth != null, "CScrollX.ts :: init() :: this._itemWidth cannot be null." );

        let bg: PIXI.Graphics = new PIXI.Graphics();
        bg.beginFill( 0x000000, 0.5 );
        bg.drawRect( 0, 0, this._width, this._height );
        bg.endFill();
        this._go.cContainer.c.addChild( bg );

        this._content = new GameObject( [ new CContainer(), new CDampedString() ] );
        this._content.cDampedSpring.springConstant = 0.1;
        this._content.init();
        this._go.cContainer.addChild( this._content );

        if ( ServiceLocator.game.inputController instanceof TouchInputController )
        {
            this._upButton = new GameObject( [ new CGraphics(), new CButton() ] );
            this._upButton.cContainer.c.visible = false;
            const kGradientWidth: number = Math.floor( this._itemWidth * 0.45 );
            let sprite: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( "transparent" ) );
            sprite.scale.set( kGradientWidth, this._height );
            this._upButton.cContainer.c.addChild( sprite );
            for ( let i: number = 0; i < kGradientWidth; ++i )
            {
                this._upButton.cGraphics.g.lineStyle( 1, 0xffffff, 0.9 * ( 1 - i / kGradientWidth ) );
                this._upButton.cGraphics.g.moveTo( i, 0 );
                this._upButton.cGraphics.g.lineTo( i, this._height );
            }

            this._upButton.init();
            this._go.cContainer.addChild( this._upButton );
            this._upButton.cButton.onClick.add( this.onUpButton_Click, this );
            //
            this._downButton = new GameObject( [ new CGraphics(), new CButton() ] );
            for ( let i: number = 0; i < kGradientWidth; ++i )
            {
                this._downButton.cGraphics.g.lineStyle( 1, 0xffffff, 0.9 * i / kGradientWidth );
                this._downButton.cGraphics.g.moveTo( i, 0 );
                this._downButton.cGraphics.g.lineTo( i, this._height );
            }
            sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( "transparent" ) );
            sprite.scale.set( kGradientWidth, this._height );
            this._downButton.cContainer.c.addChild( sprite );
            this._downButton.init();
            this._downButton.cContainer.c.x = this._width - this._downButton.cContainer.c.width;
            this._go.cContainer.addChild( this._downButton );
            this._downButton.cButton.onClick.add( this.onDownButton_Click, this );
            //
            this.updateScrollButtonsVisibility();
        }

        let mask: PIXI.Graphics = new PIXI.Graphics();
        mask.beginFill( 0xffffff, 1 );
        mask.drawRect( 0, 0, this._width, this._height );
        mask.endFill();
        this._go.cContainer.c.addChild( mask );

        this._content.cContainer.c.mask = mask;
        
        this._scrollBar = new PIXI.Graphics();
        this._scrollBar.y = this._height + 1;
        this._go.cContainer.c.addChild( this._scrollBar );
        this.drawScrollBar();

        // Listen to events.
        ServiceLocator.game.app.view.addEventListener( "mousewheel", this._bfOnAppViewMouseWheel );
    }

    public reinit(): void
    {
        let mask: PIXI.Graphics = this._content.cContainer.c.mask as PIXI.Graphics;
        mask.clear();
        mask.beginFill( 0xffffff, 1 );
        mask.drawRect( 0, 0, this._width, this._height );
        mask.endFill();

        this._content.cContainer.c.x = 0;
        this._selStep = 0;

        this.drawScrollBar();
        this.updateScrollButtonsVisibility();
    }

    public end(): void
    {
        this._onAutoScrollCompleted.removeAll();

        this._content.end();
        this._content = null;

        if ( this._upButton )
        {
            this._upButton.end();
            this._upButton = null;
        }

        if ( this._downButton )
        {
            this._downButton.end();
            this._downButton = null;
        }

        if ( this._ticker )
        {
            PIXI.Ticker.shared.remove( this.updateScrollPosition, this );
            this._ticker = null;
        }

        // Cleanup events.
        ServiceLocator.game.app.view.removeEventListener( "mousewheel", this._bfOnAppViewMouseWheel );
    }

    public hasItems(): boolean
    {
        return this._content.cContainer.c.children.length > 0;
    }

    public findItems(): Array<GameObject>
    {
        let result: Array<GameObject> = new Array<GameObject>();

        for ( let displayObject of this._content.cContainer.c.children )
        {
            result.push( ( displayObject as any ).go as GameObject );
        }

        return result;
    }

    public findItemIndex( item: GameObject ): number
    {
        let result: number = null;

        for ( let i: number = 0; i < this._content.cContainer.c.children.length; ++i )
        {
            let displayObject: PIXI.DisplayObject = this._content.cContainer.c.getChildAt( i );
            if ( ( displayObject as any ).go as GameObject == item )
            {
                result = i;
                break;
            }
        }

        return result;
    }

    public getLastItem(): GameObject
    {
        return ( this._content.cContainer.c.children[ this._content.cContainer.c.children.length - 1 ] as any ).go as GameObject;
    }
    
    public checkPointVisibility( point: PIXI.IPoint ): boolean
    {
        let result: boolean = false;

        let mask: PIXI.Graphics = this._content.cContainer.c.mask as PIXI.Graphics;
        if ( mask.getBounds().contains( point.x, point.y ) )
        {
            result = true;
        }

        return result;
    }

    public isScrollable(): boolean
    {
        return this._width / ( this._content.cContainer.c.children.length * this._itemWidth ) < 1;
    }

    public addItem( item: GameObject ): void
    {
        //console.assert( item.cContainer.c.width == this._itemWidth, "CScrollX.ts :: addItem() :: Invalid item width." );

        this._content.cContainer.addChild( item );
        item.cContainer.c.y = 1;
        item.cContainer.c.x = ( this._content.cContainer.c.children.length - 1 ) * this._itemWidth;
    
        this.drawScrollBar();
        this.updateScrollButtonsVisibility();
        this.updateTickerState();
    }

    public addItemAt( item: GameObject, index: number ): void
    {
        //console.assert( item.cContainer.c.width == this._itemWidth, "CScrollX.ts :: addItemAt() :: Invalid item width." );

        for ( let i: number = index; i < this._content.cContainer.c.children.length; ++i )
        {
            this._content.cContainer.c.getChildAt( i ).x = ( i + 1 ) * this._itemWidth;
        }
        item.cContainer.c.y = 1;
        item.cContainer.c.x = index * this._itemWidth;
        this._content.cContainer.addChildAt( item, index );

        this.drawScrollBar();
        this.updateScrollButtonsVisibility();
        this.updateTickerState();
    }

    public removeItem( item: GameObject ): void
    {
        const kRemovedItemIndex: number = this._content.cContainer.c.getChildIndex( item.cContainer.c );
        this._content.cContainer.c.removeChildAt( kRemovedItemIndex );
        for ( let i: number = kRemovedItemIndex; i < this._content.cContainer.c.children.length; ++i )
        {
            this._content.cContainer.c.getChildAt( i ).x -= this._itemWidth;
        }

        this.drawScrollBar();
        this.updateScrollButtonsVisibility();
        this.updateTickerState();
        this.scrollToStep( this._selStep );

        // If after removing an item the scroll bar disappears, the ticker will be removed too and scrollToStep() will do nothing!
        if ( !this.isScrollable() )
        {
            this._content.cContainer.c.x = 0;
            this._content.cDampedSpring.reset();
        }
    }

    public clear(): void
    {
        for ( let displayObject of this._content.cContainer.c.children )
        {
            ( ( displayObject as any ).go as GameObject ).end();
        }

        this.drawScrollBar();
        this.updateScrollButtonsVisibility();
        this.updateTickerState();
    }

    public scrollToItem( item: GameObject ): void
    {
        if ( this.isScrollable() )
        {
            this._autoScrollTo = item;

            const kItemIndex: number = this.findItemIndex( item );
            this.scrollToStep( kItemIndex );
        }
        else
        {
            this.onAutoScrollCompleted.dispatch();
        }
    }

    // private:

    private updateScrollPosition( dt: number ): void
    {
        this._content.cDampedSpring.update( dt );
        if ( this._content.cDampedSpring.isAwake )
        {
            this.drawScrollBar();
            this.updateScrollButtonsVisibility();
        }

        if ( this._autoScrollTo && !this._content.cDampedSpring.isAwake )
        {
            /*let position: PIXI.Point = new PIXI.Point();
            position.copyFrom( this._autoScrollTo.cContainer.c.position );
            position.x += this._autoScrollTo.cContainer.c.width * 0.5;
            position.y += this._autoScrollTo.cContainer.c.height * 0.5;
            if ( this.checkPointVisibility( position ) )
            {*/
                this._autoScrollTo = null;

                this._onAutoScrollCompleted.dispatch();
            //}
        }
    }

        private updateScrollButtonsVisibility(): void
        {
            if ( !this._upButton ) { return; }

            const kIsScrollable: boolean = this.isScrollable();
            this._upButton.cContainer.c.visible = kIsScrollable && this._selStep > 0;
            this._downButton.cContainer.c.visible = kIsScrollable && this._selStep < this._content.cContainer.c.children.length - Math.floor( this._width / this._itemWidth );
        }

    private scrollToStep( step: number ): void
    {
        step = this.validateStep( step );
        this._selStep = step;
        const kTarget: PIXI.Point = new PIXI.Point( -step * this._itemWidth, this._content.cContainer.c.y );
        this._content.cDampedSpring.setTarget( kTarget );
    }

    private drawScrollBar(): void
    {
        this._scrollBar.clear();
        const kScale: number =  this._width / ( this._content.cContainer.c.children.length * this._itemWidth );
        if ( kScale < 1 )
        {
            this._scrollBar.lineStyle( 1, 0x000000, 1 );
            this._scrollBar.lineTo( -this._content.cContainer.c.x * kScale, 0 );
            this._scrollBar.lineStyle( 1, 0xffffff, 1 );
            this._scrollBar.lineTo( ( -this._content.cContainer.c.x + this._width ) * kScale, 0 );
            this._scrollBar.lineStyle( 1, 0x000000, 1 );
            this._scrollBar.lineTo( this._width, 0 );
        }
    }

    private updateTickerState(): void
    {
        const kScale: number =  this._width / ( this._content.cContainer.c.children.length * this._itemWidth );
        if ( kScale < 1 )
        {
            if ( !this._ticker )
            {
                this._ticker = PIXI.Ticker.shared.add( this.updateScrollPosition, this );
            }
        }
        else
        {
            if ( this._ticker )
            {
                this._ticker.remove( this.updateScrollPosition, this );
                this._ticker = null;
            }
        }
    }

    private validateStep( step: number ): number
    {
        if ( step < 0 )
        {
            step = 0;
        }
        else 
        {
            const kScrollLength: number = Math.max( this._content.cContainer.c.children.length * this._itemWidth - ( this._width - 2 ), 0 );
            const kSelScrollPos: number = step * this._itemWidth;
            if ( kSelScrollPos > kScrollLength )
            {
                step = Math.floor( kScrollLength / this._itemWidth );
            }
        }

        return step;
    }

    // #endregion //


    // #region Input Callbacks //

    private onAppView_MouseWheel( event: WheelEvent ): void
    {
        if ( this._autoScrollTo 
            || !this._isEnabled ) { return; }

        const kGlobal: PIXI.Point = ServiceLocator.game.app.renderer.plugins.interaction.mouse.global;
        if ( this._go.cContainer.c.getBounds().contains( kGlobal.x, kGlobal.y ) )
        {
            let isEventAlreadyManaged: boolean = false;
            let scrollX: GameObject = Utils.game.hitFirstTest( kGlobal, "cScrollX", this._content.cContainer.c );
            if ( scrollX )
            {
                isEventAlreadyManaged = scrollX.cScrollX.isScrollable();
            }
            else
            {
                let scrollY: GameObject = Utils.game.hitFirstTest( kGlobal, "cScrollY", this._content.cContainer.c );
                if ( scrollY )
                {
                    isEventAlreadyManaged = scrollY.cScrollY.isScrollable();
                }
            }

            if ( !isEventAlreadyManaged )
            {
                this._selStep += event.deltaY > 0 ? 1 : -1;
                this.scrollToStep( this._selStep );
            }
        }
    }

    private onUpButton_Click(): void
    {
        this.scrollToStep( this._selStep - 1 );
    }

    private onDownButton_Click(): void
    {
        this.scrollToStep( this._selStep + 1 );
    }

    // #endregion //
}