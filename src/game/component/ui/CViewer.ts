import Component from "../Component";
import { IContentUpdater } from "./CAnchorY";

import { location_type } from "../world/CGameWorld";

import Signal from "../../../lib/signals/Signal";
import GameObject from "../../GameObject";
import CContainer from "../pixi/CContainer";
import ViewerController from "./viewer/ViewerController";
import CMaximizedView from "./viewer/CMaximizedView";
import CMinimizedView from "./viewer/CMinimizedView";
import CDropArea from "../input/CDropArea";
import CardViewerController from "./viewer/controller/CardViewerController";
import CCardView from "./viewer/CCardView";
import ServiceLocator from "../../../ServiceLocator";
import { TweenMax, Sine } from "gsap";
import Utils from "../../../Utils";
import CLogLocation from "../world/CLogLocation";
import CardTrayController from "./viewer/controller/CardTrayController";


export default class CViewer extends Component implements IContentUpdater
{
    // #region Attributes //

    // private:

    private _view: CMaximizedView = null;
    private _title: string = null;
    private _headerIconId: string = null;
    private _location: location_type = null;
    private _controller: ViewerController = null;
    private _isSelfDropAllowed: boolean = false;

    private _maximizedView: GameObject = null;
    private _minimizedView: GameObject = null;
    
    private static _gIsShow: boolean = true;

    // Signals.
    private static _onOtherViewerShown: Signal = new Signal();
    private static _onMinimizeAll: Signal = new Signal();
    private _onVisibilityUpdated: Signal = new Signal();
    private _onOpened: Signal = new Signal();
    private _onClosed: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get location(): location_type { return this._location; }
    public get controller(): ViewerController { return this._controller; }
    public get view(): CMaximizedView { return this._view; }
    // Shortcuts.
    public get cCardView(): CCardView { return this._view as CCardView; }
    
    public set view( value: CMaximizedView ) { this._view = value; }
    public set title( value: string ) { this._title = value; }
    public set headerIconId( value: string ) { this._headerIconId = value; }
    public set location( value: location_type ) { this._location = value; }
    public set controller( value: ViewerController ) { this._controller = value; }
    public set isSelfDropAllowed( value: boolean ) { this._isSelfDropAllowed = value; }
    public static set isShow( value: boolean ) { this._gIsShow = value; }

    // Signals.
    public get onContentUpdated(): Signal { return this._view.onContentUpdated; }
    public get onVisibilityUpdated(): Signal { return this._onVisibilityUpdated; }
    public get onOpened(): Signal { return this._onOpened; }
    public get onClosed(): Signal { return this._onClosed; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CViewer";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CViewer.ts :: init() :: CContainer component not found." );
        console.assert( this._view != null, "CViewer.ts :: init() :: this._view cannot be null." );
        console.assert( this._title != null, "CViewer.ts :: init() :: this._title cannot be null." );
        console.assert( this._location != null, "CViewer.ts :: init() :: this._location cannot be null." );
        console.assert( this._controller != null, "CViewer.ts :: init() :: this._controller cannot be null." );

        this._maximizedView = this.createMaximizedView();
        this._maximizedView.cContainer.c.visible = false;
        this._go.cContainer.addChild( this._maximizedView );
        this._view.onItemDraggedOut.add( this.onItem_DraggedOut, this );
        this._controller.onItemDropped.add( this.onItem_Dropped, this );
        this._controller.onItemDragCanceled.add( this.onItem_DragCanceled, this );

        this._minimizedView = this.createMinimizedView();
        this._minimizedView.cContainer.c.visible = false;
        this._go.cContainer.addChild( this._minimizedView );

        this._controller.viewer = this;
        this._controller.init();

        // Listen to events.
        CViewer._onOtherViewerShown.add( this.onOtherViewer_Shown, this );
        CViewer._onMinimizeAll.add( this.minimize, this );
    }

    public end(): void
    {
        this._onVisibilityUpdated.removeAll();
        this._onOpened.removeAll();
        this._onClosed.removeAll();

        this._controller.end();
        this._controller = null;

        this._maximizedView.end();
        this._maximizedView = null;

        this._minimizedView.end();
        this._minimizedView = null;

        // Cleanup events.
        CViewer._onOtherViewerShown.remove( this.onOtherViewer_Shown, this );
        CViewer._onMinimizeAll.remove( this.minimize, this );
    }

    public isVisible(): boolean
    {
        return this._minimizedView.cContainer.c.visible || this._maximizedView.cContainer.c.visible;
    }
    
    public show(): void
    {
        if ( CViewer._gIsShow && !this._view.isEmpty() )
        {
            CViewer._onOtherViewerShown.dispatch();
    
            this._controller.onViewShown();

            Utils.anim.flushTweensOf( this._maximizedView.cContainer.c );
            this._maximizedView.cContainer.c.alpha = 0;
            this._maximizedView.cContainer.c.x = 100;
            this._maximizedView.cContainer.c.visible = true;
            TweenMax.to( this._maximizedView.cContainer.c, 0.25, { alpha: 1, x: 0, ease: Sine.easeOut } );

            let arrViewerItem: Array<GameObject> = this._maximizedView.cMaximizedView.findItems();
            for ( let viewerItem of arrViewerItem )
            {
                if ( viewerItem.cCard )
                {
                    viewerItem.cCard.curSide.sprite.cCardPreviewable.setEnabled( true );
                }
            }

            this._view.onContentUpdated.dispatch();

            this._onVisibilityUpdated.dispatch( true );
            this._onOpened.dispatch();

            // Sfx.
            ServiceLocator.audioManager.playSfx( "window_open" );
        }
    }

    public hide(): void
    {
        Utils.anim.flushTweensOf( this._maximizedView.cContainer.c );
        if ( this._maximizedView.cContainer.c.visible )
        {
            Utils.game.findGameObjectInBranchByComponentName
            let arrViewerItem: Array<GameObject> = this._maximizedView.cMaximizedView.findItems();
            for ( let viewerItem of arrViewerItem )
            {
                if ( viewerItem.cCard )
                {
                    viewerItem.cCard.curSide.sprite.cCardPreviewable.setEnabled( false );
                }
            }

            TweenMax.to( this._maximizedView.cContainer.c, 0.25, { alpha: 0, x: 100, onComplete: () => { this._maximizedView.cContainer.c.visible = false; }, callbackScope: this, ease: Sine.easeOut } );
        }

        this._minimizedView.cContainer.c.visible = false;
        this._controller.onViewHidden();

        this._onVisibilityUpdated.dispatch( false );
        this._onClosed.dispatch();

        // Sfx.
        ServiceLocator.audioManager.playSfx( "window_close" );
    }

    public toggle(): void
    {
        if ( this._maximizedView.cContainer.c.visible )
        {
            this.hide();
        }
        else if ( this._minimizedView.cContainer.c.visible )
        {
            this.maximize();
        }
        else
        {
            this.show();
        }
    }

    public minimize(): void
    {
        if ( !this._maximizedView.cContainer.c.visible ) { return; }

        this._minimizedView.cContainer.c.visible = true;
        this._maximizedView.cContainer.c.visible = false;

        this._view.onContentUpdated.dispatch();
        this._onVisibilityUpdated.dispatch( false );
    }

    public maximize(): void
    {
        this._minimizedView.cContainer.c.visible = false;
        this._maximizedView.cContainer.c.visible = true;

        this._view.onContentUpdated.dispatch();
        this._onVisibilityUpdated.dispatch( true );
    }

    public enableCloseBtn(): void
    {
        this._minimizedView.cMinimizedView.enableCloseBtn();
        this._maximizedView.cMaximizedView.enableCloseBtn();
    }

    public disableCloseBtn(): void
    {
        this._minimizedView.cMinimizedView.disableCloseBtn();
        this._maximizedView.cMaximizedView.disableCloseBtn();
    }

    public static hideAll(): void
    {
        this._onOtherViewerShown.dispatch();
    }

    public static minimizeAll(): void
    {
        this._onMinimizeAll.dispatch();
    }

    // private:

    private createMaximizedView(): GameObject
    {
        let maximizedView: GameObject = new GameObject( [ new CContainer(), this._view, new CDropArea(), new CLogLocation() ] );
        maximizedView.oid = this._go.oid + "_max";
        maximizedView.cMaximizedView.title = this._title;
        maximizedView.cMaximizedView.headerIconId = this._headerIconId;
        if ( maximizedView.cCardView )
        {
            maximizedView.cCardView.controller = this._controller as CardViewerController;
        }
        else if ( maximizedView.cCardTrayView )
        {
            maximizedView.cCardTrayView.controller = this._controller as CardTrayController;
        }
        maximizedView.cDropArea.target = maximizedView.cMaximizedView;
        maximizedView.cDropArea.isSelfDropAllowed = this._isSelfDropAllowed;
        maximizedView.cLogLocation.location = this._location;
        maximizedView.init();

        return maximizedView;
    }

    private createMinimizedView(): GameObject
    {
        let minimizedView: GameObject = new GameObject( [ new CContainer(), new CMinimizedView() ] );
        minimizedView.oid = this._go.oid + "_min";
        minimizedView.cMinimizedView.title = this._title;
        minimizedView.cMinimizedView.headerIconId = this._headerIconId;
        minimizedView.cMinimizedView.controller = this._controller;
        minimizedView.init();

        return minimizedView;
    }

    // #endregion //


    // #region Other Callbacks //

    private onItem_DraggedOut(): void
    {
        if ( this._controller.findItemCount() > 0 )
        {
            this.minimize();
        }
        else
        {
            this.hide();
        }
    }

    private onItem_Dropped( token: GameObject, hit: GameObject ): void
    {
        if ( this._minimizedView.cContainer.c.visible
            && hit != ServiceLocator.game.cardActivationArea )
        {
            if ( this._controller.findItemCount() > 0 )
            {
                this.maximize();
            }
            else
            {
                this.hide();
            }
        }
    }

    private onItem_DragCanceled(): void
    {
        if ( this._minimizedView.cContainer.c.visible )
        {
            if ( this._controller.findItemCount() > 0 )
            {
                this.maximize();
            }
            else
            {
                this.hide();
            }
        }
    }
    
    private onOtherViewer_Shown(): void
    {
        let isVisible: boolean = this._maximizedView.cContainer.c.visible || this._minimizedView.cContainer.c.visible;
        if ( isVisible )
        {
            this.hide();
        }
    }

    // #endregion //
}

export const enum view_button_type
{
    CLOSE = 0,
    MINIMIZE,
    MAXIMIZE
}