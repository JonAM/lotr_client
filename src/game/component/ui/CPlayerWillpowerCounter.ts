import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CTokenCounter from "./CTokenCounter";
import CGraphics from "../pixi/CGraphics";
import CDropArea from "../input/CDropArea";
import CDraggable from "../input/CDraggable";
import { player_type } from "../world/CPlayerArea";
import CButton from "../input/CButton";
import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import CContainer from "../pixi/CContainer";
import { ISgPlayerWillpowerCounter } from "../../../view/game/SaveGameView";
import CHighlightPoiReceptor from "../world/poi_receptor/CHighlightPoiReceptor";
import { status_type } from "../card/token/CCardTokenSide";
import { view_layer_id } from "../../../service/ViewManager";
import { layer_type } from "../world/CGameLayerProvider";
import CIsolatedArea from "../world/custom_area/CIsolatedArea";
import PointerInputController from "../../input/PointerInputController";
import TouchInputController from "../../input/TouchInputController";


export default class CPlayerWillpowerCounter extends Component
{
    // #region Attributes //

    // private:

    private _playerController: player_type = null;
    private _colorType: player_type = null;
    private _actorArea: GameObject = null;
    private _isCompact: boolean = false;
    
    private _counterWrapper: PIXI.Container = null;
    private _counter: GameObject = null;
    private _icon: PIXI.Sprite = null;
    private _text: PIXI.Text = null;
    private _autoBtn: GameObject = null;
    private _firstCharacterAttributeBtn: GameObject = null;
    private _secondCharacterAttributeBtn: GameObject = null;
    private _autoCount: number = null;
    private _isAuto: boolean = null;
    private _selCharacterAttribute: character_attribute_type = null;
    private _poiSocket: GameObject = null;

    // Constants.
    private _kArrCharacterAttributeIconId: Array<string> = [ "willpower", "attack", "defense" ];

    // #endregion //


    // #region Properties //

    public get counter(): GameObject { return this._counter; }
    public get selCharacterAttribute(): character_attribute_type { return this._selCharacterAttribute; }
    public get isAuto(): boolean { return this._isAuto; }

    public set controller( value: player_type ) { this._playerController = value; }
    public set colorType( value: player_type ) { this._colorType = value; }
    public set actorArea( value: GameObject ) { this._actorArea = value; }
    public set isCompact( value: boolean ) { this._isCompact = value; }
    public set isAuto( value: boolean ) { this._isAuto = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CPlayerWillpowerCounter";
    }
    
    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CPlayerWillpowerCounter.ts :: init() :: CContainer component not found." );
        console.assert( this._playerController == player_type.PLAYER || this._playerController == player_type.ALLY, "CPlayerWillpowerCounter.ts :: init() :: invalid value for this._playerController." );
        console.assert( this._colorType !== null, "CPlayerWillpowerCounter.ts :: init() :: this._colorType cannot be null." );
        console.assert( this._actorArea != null, "CPlayerWillpowerCounter.ts :: init() :: this._actorArea cannot be null." );

        this._selCharacterAttribute = character_attribute_type.WILLPOWER;
        this._autoCount = 0;
        this._isAuto = true;

        this._go.cContainer.c.interactive = true;

        this._counterWrapper = new PIXI.Container();
        this._go.cContainer.c.addChild( this._counterWrapper );

        // Counter.
        this._counter = new GameObject( [ new CGraphics(), new CTokenCounter(), new CDropArea(), new CDraggable(), new CHighlightPoiReceptor() ] );
        this._counter.oid = this._go.oid + "_counter";
        this._counter.cGraphics.g.lineStyle( 2, ServiceLocator.game.playerColors[ this._colorType ], 1 );
        this._counter.cGraphics.g.beginFill( ServiceLocator.game.playerColors[ this._colorType ], 0.4 );
        this._counter.cGraphics.g.drawCircle( 0, 0, this._isCompact ? 15 : 30 );
        this._counter.cGraphics.g.endFill();
        this._counter.cTokenCounter.tokenName = this._kArrCharacterAttributeIconId[ this._selCharacterAttribute ];
        this._counter.cTokenCounter.count = 0;
        this._counter.cTokenCounter.draggedTexture = ServiceLocator.resourceStack.findAsTexture( this._kArrCharacterAttributeIconId[ this._selCharacterAttribute ] );
        this._counter.cDropArea.target = this._counter.cTokenCounter;
        // POI socket.
        this._poiSocket = new GameObject( [ new CContainer() ] );
        this._poiSocket.cContainer.c.name = "poi_socket";
        this._poiSocket.init();
        this._poiSocket.cContainer.c.y -= this._counter.cContainer.c.height * 0.5 - 5;
        this._go.cContainer.addChild( this._poiSocket );
        //
        this._counter.cHighlightPoiReceptor.poiSocket = this._poiSocket;
        //
        this._counter.init();
        this._counterWrapper.addChild( this._counter.cContainer.c );
        this._counter.cTokenCounter.setEnabled( this._playerController == player_type.PLAYER );
        
        // Icon.
        this._icon = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( this._kArrCharacterAttributeIconId[ this._selCharacterAttribute ] ) );
        Utils.game.limitSideSize( this._isCompact ? 25 : 30, this._icon );
        if ( this._isCompact )
        {
            this._icon.anchor.set( 0.5 );
        }
        else
        {
            this._icon.anchor.set( 0.5, 1 );
            this._icon.y = 2;
        }
        this._counterWrapper.addChild( this._icon );
        
        // Text.
        this._text = new PIXI.Text( "0", ServiceLocator.game.textStyler.normal );
        if ( this._isCompact )
        {
            this._text.anchor.set( 0.5 );
        }
        else
        {
            this._text.anchor.set( 0.5, 0 );
        }
        this._counterWrapper.addChild( this._text );

        // Auto button.
        this._autoBtn = new GameObject( [ new CGraphics(), new CButton() ] );
        this._autoBtn.cGraphics.g.lineStyle( 1, 0x000000, 1 );
        this._autoBtn.cGraphics.g.beginFill( 0x00008b );
        this._autoBtn.cGraphics.g.drawCircle( 0, 0, this._isCompact ? 10 : 15 );
        this._autoBtn.cGraphics.g.endFill();
        let autoIcon: PIXI.Sprite = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( "auto" ) );
        Utils.game.limitSideSize( this._isCompact ? 15 : 20, autoIcon );
        autoIcon.anchor.set( 0.5 );
        this._autoBtn.cGraphics.g.addChild( autoIcon );
        this._autoBtn.init();
        let m: PIXI.Matrix = new PIXI.Matrix();
        m.translate( this._counter.cContainer.c.width * 0.5, 0 );
        m.rotate( -Math.PI * 0.25 );
        this._autoBtn.cGraphics.g.position.set( m.tx, m.ty );
        this._autoBtn.cContainer.c.visible = false;
        this._counterWrapper.addChild( this._autoBtn.cContainer.c );
        this._autoBtn.cButton.onClick.add( this.onAutoBtn_Click, this );

        // Character attribute button.
        // First.
        this._firstCharacterAttributeBtn = new GameObject( [ new CGraphics(), new CButton() ] );
        this._firstCharacterAttributeBtn.cGraphics.g.lineStyle( 1, 0x000000, 1 );
        this._firstCharacterAttributeBtn.cGraphics.g.beginFill( 0xeccc68, 1 );
        this._firstCharacterAttributeBtn.cGraphics.g.drawCircle( 0, 0, this._isCompact ? 10 : 15 );
        this._firstCharacterAttributeBtn.cGraphics.g.endFill();
        let icon: PIXI.Sprite = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( this._kArrCharacterAttributeIconId[ character_attribute_type.ATTACK ] ) );
        icon.name = "icon";
        Utils.game.limitSideSize( this._isCompact ? 10 : 20, icon );
        icon.anchor.set( 0.5, 0.5 );
        this._firstCharacterAttributeBtn.cGraphics.g.addChild( icon );
        this._firstCharacterAttributeBtn.init();
        m.identity();
        m.translate( -( this._counter.cContainer.c.width + this._firstCharacterAttributeBtn.cContainer.c.width + 5 ) * 0.5, 0 );
        m.rotate( Math.PI * 0.12 );
        this._firstCharacterAttributeBtn.cGraphics.g.position.set( m.tx, m.ty );
        this._firstCharacterAttributeBtn.cContainer.c.visible = false;
        this._firstCharacterAttributeBtn.cButton.data = character_attribute_type.ATTACK;
        this._go.cContainer.addChild( this._firstCharacterAttributeBtn );
        this._firstCharacterAttributeBtn.cButton.onClick.add( this.onFirstCharacterAttributeBtn_Click, this );
        // Second.
        this._secondCharacterAttributeBtn = new GameObject( [ new CGraphics(), new CButton() ] );
        this._secondCharacterAttributeBtn.cGraphics.g.lineStyle( 1, 0x000000, 1 );
        this._secondCharacterAttributeBtn.cGraphics.g.beginFill( 0xeccc68, 1 );
        this._secondCharacterAttributeBtn.cGraphics.g.drawCircle( 0, 0, this._isCompact ? 10 : 15 );
        this._secondCharacterAttributeBtn.cGraphics.g.endFill();
        icon = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( this._kArrCharacterAttributeIconId[ character_attribute_type.DEFENSE ] ) );
        icon.name = "icon";
        Utils.game.limitSideSize( this._isCompact ? 10 : 20, icon );
        icon.anchor.set( 0.5, 0.5 );
        this._secondCharacterAttributeBtn.cGraphics.g.addChild( icon );
        this._secondCharacterAttributeBtn.init();
        m.identity();
        m.translate( -( this._counter.cContainer.c.width + this._secondCharacterAttributeBtn.cContainer.c.width + 5 ) * 0.5, 0 );
        m.rotate( -Math.PI * 0.12 );
        this._secondCharacterAttributeBtn.cGraphics.g.position.set( m.tx, m.ty );
        this._secondCharacterAttributeBtn.cContainer.c.visible = false;
        this._secondCharacterAttributeBtn.cButton.data = character_attribute_type.DEFENSE;
        this._go.cContainer.addChild( this._secondCharacterAttributeBtn );
        this._secondCharacterAttributeBtn.cButton.onClick.add( this.onSecondCharacterAttributeBtn_Click, this );

        // Listen to events.
        this._counter.cTokenCounter.onCountUpdated.add( this.onCounter_Updated, this );
        this._actorArea.cActorArea.onTokenSet.add( this.onActoreAreaToken_Set, this );
        this._actorArea.cActorArea.onTokenUnset.add( this.onActorAreaToken_Unset, this );
        if ( this._playerController == player_type.PLAYER )
        {
            if ( ServiceLocator.game.inputController instanceof PointerInputController )
            {
                this._go.cContainer.c.on( "pointermove", this.onPointerMove, this );
            }
            else if ( ServiceLocator.game.inputController instanceof TouchInputController )
            {
                this._firstCharacterAttributeBtn.cContainer.c.visible = true;
                this._secondCharacterAttributeBtn.cContainer.c.visible = true;
            }
        }
    }

    public end(): void
    {
        // Cleanup events.
        if ( this._actorArea.cActorArea )
        {
            this._actorArea.cActorArea.onTokenSet.remove( this.onActoreAreaToken_Set, this );
            this._actorArea.cActorArea.onTokenUnset.remove( this.onActorAreaToken_Unset, this );
            let arrActor: Array<GameObject> = this._actorArea.cActorArea.findAllActors();
            for ( let actor of arrActor )
            {
                this.untrackQuestEvents( actor.cCardToken.curSide );
                actor.cCardToken.onFlipped.remove( this.onCardToken_Flipped, this );

                let cTokenCounter: CTokenCounter = this.findCharacterAttributeCounter( actor.cCardToken.curSide, this._selCharacterAttribute );
                if ( cTokenCounter )
                {
                    cTokenCounter.onCountUpdated.remove( this.onCharacterAttribute_Updated, this );
                }
            }
        }
        if ( this._playerController == player_type.PLAYER )
        {
            this._go.cContainer.c.off( "pointermove", this.onPointerMove, this );
        }

        this._text = null;
        this._icon = null;
        this._playerController = null;
        this._colorType = null;
        this._actorArea = null;
        this._isAuto = null;

        this._poiSocket.end();
        this._poiSocket = null;

        this._counter.end();
        this._counter = null;

        this._autoBtn.end();
        this._autoBtn = null;

        this._firstCharacterAttributeBtn.end();
        this._firstCharacterAttributeBtn = null;

        this._secondCharacterAttributeBtn.end();
        this._secondCharacterAttributeBtn = null;
    }

    public show(): void
    {
        this._go.cContainer.c.visible = true;

        let arrActor: Array<GameObject> = this._actorArea.cActorArea.findAllActors();
        for ( let actor of arrActor )
        {
            this.trackQuestEvents( actor.cCardToken.curSide );
            actor.cCardToken.onFlipped.add( this.onCardToken_Flipped, this );
        }
    }

    public hide(): void
    {
        this._go.cContainer.c.visible = false;

        let arrActor: Array<GameObject> = this._actorArea.cActorArea.findAllActors();
        for ( let actor of arrActor )
        {
            this.untrackQuestEvents( actor.cCardToken.curSide );
            actor.cCardToken.onFlipped.remove( this.onCardToken_Flipped, this );
            
            let cTokenCounter: CTokenCounter = this.findCharacterAttributeCounter( actor.cCardToken.curSide, this._selCharacterAttribute );
            if ( cTokenCounter )
            {
                cTokenCounter.onCountUpdated.remove( this.onCharacterAttribute_Updated, this );
            }
        }
    }

    public forceAutoBtnClick(): void
    {
        this._isAuto = true;
        this._autoBtn.cContainer.c.visible = false;

        this.calcAutoCount();
        this._counter.cTokenCounter.setCount( this._autoCount, action_scope_type.LOCAL );
    }

    public forceCharacterAttributeBtnClick( characterAttribute: character_attribute_type ): void
    {
        this.selectCharacterAttribute( characterAttribute );
        this.updateCharacterAttributeBtns();
    }

    public onStagingAreaSplit( playerWillpowerCounter: GameObject ): void
    {
        GameObject.replace( this._go.oid, playerWillpowerCounter );
        GameObject.replace( playerWillpowerCounter.oid, this._go );
        GameObject.replace( this._counter.oid, playerWillpowerCounter.cPlayerWillpowerCounter.counter );
        GameObject.replace( playerWillpowerCounter.cPlayerWillpowerCounter.counter.oid, this._counter );
    }

    public onStagingAreaMerged( playerWillpowerCounter: GameObject ): void
    {
        GameObject.replace( this._go.oid, this._go );
        GameObject.replace( playerWillpowerCounter.oid, playerWillpowerCounter );
        GameObject.replace( this._counter.oid, this._counter );
        GameObject.replace( playerWillpowerCounter.cPlayerWillpowerCounter.counter.oid, playerWillpowerCounter.cPlayerWillpowerCounter.counter );
    
        this.forceAutoBtnClick();        
    }

    public saveGame(): ISgPlayerWillpowerCounter
    {
        return {
            count: this._counter.cTokenCounter.count,
            characterAttribute: this._selCharacterAttribute,
            isAuto: this._isAuto };
    }

    public loadGame( sgPlayerWillpowerCounter: ISgPlayerWillpowerCounter, pass: number ): void
    {  
        this.selectCharacterAttribute( sgPlayerWillpowerCounter.characterAttribute );
        this.updateCharacterAttributeBtns();

        this._isAuto = sgPlayerWillpowerCounter.isAuto;
        if ( this._isAuto )
        {
            this.calcAutoCount();
            this._counter.cTokenCounter.setCount( this._autoCount, action_scope_type.LOCAL );
        }
        else
        {
            this._counter.cTokenCounter.setCount( sgPlayerWillpowerCounter.count, action_scope_type.LOCAL );
            this._autoBtn.cContainer.c.visible = this._playerController == player_type.PLAYER;
        }
    }

    // private:

    private trackQuestEvents( actorSide: GameObject ): void
    {
        if ( actorSide.cHeroSide )
        {
            actorSide.cHeroSide.onCommittedToQuest.add( this.onCardToken_CommittedToQuest, this );
            actorSide.cHeroSide.onUncommittedFromQuest.add( this.onCardToken_UncommittedFromQuest, this );
        }
        else if ( actorSide.cAllySide )
        {
            actorSide.cAllySide.onCommittedToQuest.add( this.onCardToken_CommittedToQuest, this );
            actorSide.cAllySide.onUncommittedFromQuest.add( this.onCardToken_UncommittedFromQuest, this );
        }
    }

    private untrackQuestEvents( actorSide: GameObject ): void
    {
        if ( actorSide.cHeroSide )
        {
            actorSide.cHeroSide.onCommittedToQuest.remove( this.onCardToken_CommittedToQuest, this );
            actorSide.cHeroSide.onUncommittedFromQuest.remove( this.onCardToken_UncommittedFromQuest, this );
        }
        else if ( actorSide.cAllySide )
        {
            actorSide.cAllySide.onCommittedToQuest.remove( this.onCardToken_CommittedToQuest, this );
            actorSide.cAllySide.onUncommittedFromQuest.remove( this.onCardToken_UncommittedFromQuest, this );
        }
    }

    private findCharacterAttributeCounter( characterTokenSide: GameObject, characterAttribute: character_attribute_type ): CTokenCounter
    {
        let result: CTokenCounter = null;

        if ( characterTokenSide.cHeroSide )
        {
            switch ( characterAttribute )
            {
                case character_attribute_type.WILLPOWER: { result = characterTokenSide.cHeroSide.willpower.cTokenCounter; break; }
                case character_attribute_type.ATTACK: { result = characterTokenSide.cHeroSide.attack.cTokenCounter; break; }
                case character_attribute_type.DEFENSE: { result = characterTokenSide.cHeroSide.defense.cTokenCounter; break; }
            }
        }
        else if ( characterTokenSide.cAllySide )
        {
            switch ( characterAttribute )
            {
                case character_attribute_type.WILLPOWER: { result = characterTokenSide.cAllySide.willpower.cTokenCounter; break; }
                case character_attribute_type.ATTACK: { result = characterTokenSide.cAllySide.attack.cTokenCounter; break; }
                case character_attribute_type.DEFENSE: { result = characterTokenSide.cAllySide.defense.cTokenCounter; break; }
            }
        }

        return result;
    }

    private calcAutoCount(): void
    {
        this._autoCount = 0;
        let arrActor: Array<GameObject> = this._actorArea.cActorArea.findAllActors();
        for ( let actor of arrActor )
        {
            if ( actor.cCardToken.cCurSide.hasStatus( status_type.COMMITTED_TO_QUEST ) )
            {
                this._autoCount += this.findCharacterAttributeCounter( actor.cCardToken.curSide, this._selCharacterAttribute ).count;
            }
        }
    }

    private selectCharacterAttribute( characterAttribute: character_attribute_type ): void
    {
        if ( characterAttribute != this._selCharacterAttribute )
        {
            let arrActor: Array<GameObject> = this._actorArea.cActorArea.findAllActors();
            for ( let actor of arrActor )
            {
                let cTokenCounter: CTokenCounter = this.findCharacterAttributeCounter( actor.cCardToken.curSide, this._selCharacterAttribute );
                if ( cTokenCounter )
                {
                    cTokenCounter.onCountUpdated.remove( this.onCharacterAttribute_Updated, this );
                }
                cTokenCounter = this.findCharacterAttributeCounter( actor.cCardToken.curSide, characterAttribute );
                if ( cTokenCounter && actor.cCardToken.cCurSide.hasStatus( status_type.COMMITTED_TO_QUEST ) )
                {
                    cTokenCounter.onCountUpdated.add( this.onCharacterAttribute_Updated, this );
                }
            }

            this._selCharacterAttribute = characterAttribute;

            this.forceAutoBtnClick();
            // Counter.
            this._counter.cTokenCounter.tokenName = this._kArrCharacterAttributeIconId[ this._selCharacterAttribute ];
            this._counter.cTokenCounter.draggedTexture = ServiceLocator.resourceStack.findAsTexture( this._kArrCharacterAttributeIconId[ this._selCharacterAttribute ] );
            // Icon.
            this._icon.texture = ServiceLocator.resourceStack.findAsTexture( this._kArrCharacterAttributeIconId[ this._selCharacterAttribute ] );
            Utils.game.limitSideSize( 30, this._icon );
        }
    }

    private updateCharacterAttributeBtns(): void
    {
        let characterAttributeBtn: GameObject = this._firstCharacterAttributeBtn;
        for ( let i: number = 0; i < character_attribute_type.kCount; ++i )
        {
            if ( i != this._selCharacterAttribute )
            {
                characterAttributeBtn.cButton.data = i;
                let icon: PIXI.Sprite = characterAttributeBtn.cContainer.c.getChildByName( "icon" ) as PIXI.Sprite;
                icon.texture = ServiceLocator.resourceStack.findAsTexture( this._kArrCharacterAttributeIconId[ i ] );
                Utils.game.limitSideSize( 20, icon );

                if ( characterAttributeBtn == this._firstCharacterAttributeBtn )
                {
                    characterAttributeBtn = this._secondCharacterAttributeBtn;
                }
                else
                {
                    break;
                }
            }
        }
    }

    // #endregion //


    // #region Input Callbacks //

    private onPointerMove( event: PIXI.InteractionEvent ): void
    {
        if ( ServiceLocator.game.dragShadowManager.isDragging
            || ServiceLocator.viewManager.findViewCount( view_layer_id.POPUP ) > 0
            || ServiceLocator.viewManager.findViewCount( view_layer_id.TOPMOST ) > 0
            || ServiceLocator.game.root.cGameLayerProvider.get( layer_type.CARD_PRESENTATION ).cContainer.c.children.length > 0
            || ServiceLocator.game.root.cGameLayerProvider.get( layer_type.DISABLED ).cContainer.c.children.length > 0 ) { return; }
        
        if ( this._go.cContainer.c.getBounds().contains( event.data.global.x, event.data.global.y ) )
        {
            this._firstCharacterAttributeBtn.cContainer.c.visible = true;
            this._secondCharacterAttributeBtn.cContainer.c.visible = true;
        }
        else
        {
            this._firstCharacterAttributeBtn.cContainer.c.visible = false;
            this._secondCharacterAttributeBtn.cContainer.c.visible = false;
        }
    }

    private onAutoBtn_Click(): void
    {
        this._isAuto = true;
        this._autoBtn.cContainer.c.visible = false;

        this.calcAutoCount();
        this._counter.cTokenCounter.setCount( this._autoCount, action_scope_type.MULTIPLAYER );

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_AUTO_COUNTER, null, [ this._go.oid, this._isAuto ] );
    }

    private onFirstCharacterAttributeBtn_Click(): void
    {
        const kCharacterAttribute: character_attribute_type = this._firstCharacterAttributeBtn.cButton.data as character_attribute_type;
        this.forceCharacterAttributeBtnClick( kCharacterAttribute );

        if ( ServiceLocator.game.inputController instanceof PointerInputController )
        {
            this._firstCharacterAttributeBtn.cContainer.c.visible = false;
            this._secondCharacterAttributeBtn.cContainer.c.visible = false;
        }

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_PLAYER_WILLPOWER_COUNTER_ATTRIBUTE, null, [ this._go.oid, kCharacterAttribute ] );
    }
    
    private onSecondCharacterAttributeBtn_Click(): void
    {
        const kCharacterAttribute: character_attribute_type = this._secondCharacterAttributeBtn.cButton.data as character_attribute_type;
        this.forceCharacterAttributeBtnClick( kCharacterAttribute );

        if ( ServiceLocator.game.inputController instanceof PointerInputController )
        {
            this._firstCharacterAttributeBtn.cContainer.c.visible = false;
            this._secondCharacterAttributeBtn.cContainer.c.visible = false;
        }

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_PLAYER_WILLPOWER_COUNTER_ATTRIBUTE, null, [ this._go.oid, kCharacterAttribute ] );
    }

    // #endregion //


    // #region Other Callbacks //

    private onCounter_Updated( count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._text.text = count.toString();

        if ( this._playerController == player_type.PLAYER && isPlayerInput )
        {
            this._isAuto = false;
            this._autoBtn.cContainer.c.visible = true;

            // Multiplayer.
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_AUTO_COUNTER, null, [ this._go.oid, this._isAuto ] );
        }

        if ( count > this._autoCount )
        {
            this._text.tint = 0x00ff00;
        }
        else if ( count < this._autoCount )
        {
            this._text.tint = 0xff0000;
        }
        else
        {
            this._text.tint = 0xffffff;
        }

        CTokenCounter.animate( this._counter.cContainer.c.parent, delta );

        /*if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetPlayer( player_type.PLAYER ), this._kArrCharacterAttributeIconId[ this._selCharacterAttribute ], delta, true );
        }*/
    }

    private onActoreAreaToken_Set( actor: GameObject ): void
    {
        this.trackQuestEvents( actor.cCardToken.curSide );

        if ( actor.cCardToken.curSide.cCardTokenSide.hasStatus( status_type.COMMITTED_TO_QUEST ) )
        {
            this.onCardToken_CommittedToQuest( actor.cCardToken.curSide );
        }
    }

    private onActorAreaToken_Unset( actor: GameObject ): void
    {
        this.untrackQuestEvents( actor.cCardToken.curSide );

        actor.cCardToken.onFlipped.remove( this.onCardToken_Flipped, this );

        let cTokenCounter: CTokenCounter = this.findCharacterAttributeCounter( actor.cCardToken.curSide, this._selCharacterAttribute );
        if ( cTokenCounter )
        {
            cTokenCounter.onCountUpdated.remove(  this.onCharacterAttribute_Updated, this );
            
            if ( actor.cCardToken.cCurSide.hasStatus( status_type.COMMITTED_TO_QUEST ) )
            {
                this._autoCount -= cTokenCounter.count;
                if ( this._isAuto )
                {
                    this._counter.cTokenCounter.setCount( this._autoCount, action_scope_type.LOCAL );
                }
            }
        }
    }

    private onCardToken_CommittedToQuest( actorSide: GameObject ): void
    {
        let cTokenCounter: CTokenCounter = this.findCharacterAttributeCounter( actorSide, this._selCharacterAttribute );
        if ( cTokenCounter )
        {
            cTokenCounter.onCountUpdated.add( this.onCharacterAttribute_Updated, this );

            this._autoCount += cTokenCounter.count;
            if ( this._isAuto )
            {
                this._counter.cTokenCounter.setCount( this._autoCount, action_scope_type.LOCAL );
            }
        }
    }

    private onCardToken_UncommittedFromQuest( actorSide: GameObject ): void
    {
        let cTokenCounter: CTokenCounter = this.findCharacterAttributeCounter( actorSide, this._selCharacterAttribute );
        if ( cTokenCounter )
        {
            cTokenCounter.onCountUpdated.remove(  this.onCharacterAttribute_Updated, this );

            this._autoCount -= cTokenCounter.count;
            if ( this._isAuto )
            {
                this._counter.cTokenCounter.setCount( this._autoCount, action_scope_type.LOCAL );
            }
        }
    }

    private onCharacterAttribute_Updated( count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._autoCount += delta;
        if ( this._isAuto )
        {
            this._counter.cTokenCounter.setCount( this._autoCount, action_scope_type.LOCAL );
        }
    }

    private onCardToken_Flipped( frontSide: GameObject, backSide: GameObject ): void
    {
        this.untrackQuestEvents( backSide );

        if ( backSide.cCardTokenSide.hasStatus( status_type.COMMITTED_TO_QUEST ) )
        {
            let cTokenCounter: CTokenCounter = this.findCharacterAttributeCounter( backSide, this._selCharacterAttribute );
            if ( cTokenCounter )
            {
                cTokenCounter.onCountUpdated.remove(  this.onCharacterAttribute_Updated, this );
                
                this._autoCount -= cTokenCounter.count;
                if ( !this._autoBtn.cContainer.c.visible )
                {
                    this._counter.cTokenCounter.setCount( this._autoCount, action_scope_type.LOCAL );
                }
            }
        }

        this.trackQuestEvents( frontSide );
    }

    // #endregion //
}

export const enum character_attribute_type
{
    WILLPOWER = 0,
    ATTACK,
    DEFENSE,
    kCount
}