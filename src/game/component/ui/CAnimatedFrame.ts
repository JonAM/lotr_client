import Component from "../Component";

import * as PIXI from "pixi.js";


export default class CAnimatedFrame extends Component
{
    // #region Attributes //

    // private:

    private _width: number = null;
    private _height: number = null;
    private _lineWidth: number = null;
    
    private _speed: number = null;
    private _ticker: PIXI.Ticker = null;

    private _arrSnake: Array<ISnake> = null;
    private _arrFrameSideLength: Array<number> = null;

    // #endregion //


    // #region Properties //

    public set width( value: number ) { this._width = value; }
    public set height( value: number ) { this._height = value; }
    public set lineWidth( value: number ) { this._lineWidth = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CAnimatedFrame";
    }

    public init(): void
    {
        super.init();

        this._lineWidth = 1;
        this._speed = this._width / 128;

        console.assert( this._go.cContainer != null, "CAnimatedFrame.ts :: init() :: CGraphics component not found." );
        console.assert( this._width != null, "CAnimatedFrame.ts :: init() :: this._width cannot be null." );
        console.assert( this._height != null, "CAnimatedFrame.ts :: init() :: this._height cannot be null." );
    
        this._arrSnake = [
            {
                frameSide: frame_side_type.BOTTOM,
                progress: this._width * 0.5,
                length: Math.max( this._height, this._width ) * 0.5
            },
            {
                frameSide: frame_side_type.TOP,
                progress: this._width * 0.5,
                length: Math.max( this._height, this._width ) * 0.5
            } ];

        this._arrFrameSideLength = [
            this._width,
            this._height,
            this._width,
            this._height ];
    }

    public end(): void
    {
        this.stop();
    }

    public start(): void
    {
        if ( !this._ticker )
        {
            this._ticker = PIXI.Ticker.shared.add( this.updateSnake, this );
        }
    }

        private updateSnake( dt: number ): void
        {
            this._go.cGraphics.g.clear();
            this._go.cGraphics.g.lineStyle( this._lineWidth, 0xffffff, 1 );
            
            for ( let snake of this._arrSnake )
            {
                snake.progress += dt * this._speed;
                if ( snake.progress > this._arrFrameSideLength[ snake.frameSide ] )
                {
                    snake.progress -= this._arrFrameSideLength[ snake.frameSide ]; 
                    snake.frameSide = this.findNextFrameSide( snake.frameSide );
                }

                const kPrevSideSnakeLength: number = snake.length - snake.progress;
                if ( kPrevSideSnakeLength > 0 )
                {
                    this.drawSnake( 0, snake.progress, snake.frameSide );
                    const kPrevFrameSide: frame_side_type = this.findPrevFrameSide( snake.frameSide );
                    this.drawSnake( this._arrFrameSideLength[ kPrevFrameSide ] - kPrevSideSnakeLength, this._arrFrameSideLength[ kPrevFrameSide ], kPrevFrameSide );
                }
                else
                {
                    this.drawSnake( snake.progress - snake.length, snake.progress, snake.frameSide );
                }
            }
        }

            private findPrevFrameSide( frameSide: frame_side_type ): frame_side_type
            {
                let result: frame_side_type = frameSide;

                result -= 1;
                if ( result < frame_side_type.BOTTOM )
                {
                    result = frame_side_type.RIGHT;
                }

                return result;
            }

            private findNextFrameSide( frameSide: frame_side_type ): frame_side_type
            {
                let result: frame_side_type = frameSide;

                result += 1;
                if ( result > frame_side_type.RIGHT )
                {
                    result = frame_side_type.BOTTOM;
                }

                return result;
            }

            private drawSnake( from: number, to: number, frameSide: frame_side_type ): void
            {
                switch ( frameSide )
                {
                    case frame_side_type.BOTTOM:
                        {
                            this._go.cGraphics.g.moveTo( this._width - from, this._height );
                            this._go.cGraphics.g.lineTo( this._width - to, this._height );
                            break;
                        }

                    case frame_side_type.LEFT:
                        {
                            this._go.cGraphics.g.moveTo( 0, this._height - from );
                            this._go.cGraphics.g.lineTo( 0, this._height - to );
                            break;
                        }

                    case frame_side_type.TOP:
                        {
                            this._go.cGraphics.g.moveTo( from, 0 );
                            this._go.cGraphics.g.lineTo( to, 0 );
                            break;
                        }

                    case frame_side_type.RIGHT:
                        {
                            this._go.cGraphics.g.moveTo( this._width, from );
                            this._go.cGraphics.g.lineTo( this._width, to );
                            break;
                        }
                }
            }

    public stop(): void
    {
        if ( this._ticker )
        {
            this._ticker.remove( this.updateSnake, this );
            this._ticker = null;

            this._go.cGraphics.g.clear();
        }
    }

    // #endregion //
}

const enum frame_side_type
{
    BOTTOM = 0,
    LEFT,
    TOP,
    RIGHT
}

interface ISnake
{
    frameSide: frame_side_type;
    progress: number;
    length: number;
}