import Component from "../Component";

import { poi_type } from "../world/CPoi";
import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import { IPoiMenuItem } from "./CPoiMenu";
import Signal from "../../../lib/signals/Signal";


export default class CPoiMenuItem extends Component
{
    // #region Attributes //

    // private:

    private _poiMenuItemType: poi_menu_item_type = null;
    private _menuLevel: number = null;
    private _radius: number = null;
    private _submenu: Array<IPoiMenuItem> = null;
    private _cPoiMenuParentItem: CPoiMenuItem = null;

    // Signals.
    private _onPointerOver: Signal = new Signal();
    private _onPointerOut: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get type(): poi_menu_item_type { return this._poiMenuItemType; }
    public get menuLevel(): number { return this._menuLevel; }
    public get submenu(): Array<IPoiMenuItem> { return this._submenu; }
    public get parent(): CPoiMenuItem { return this._cPoiMenuParentItem; }

    public set type( value: poi_menu_item_type ) { this._poiMenuItemType = value; }
    public set radius( value: number ) { this._radius = value; }
    public set menuLevel( value: number ) { this._menuLevel = value; }
    public set submenu( value: Array<IPoiMenuItem> ) { this._submenu = value; }
    public set parent( value: CPoiMenuItem ) { this._cPoiMenuParentItem = value; }

    // Signals.
    public get onPointerOver(): Signal { return this._onPointerOver; }
    public get onPointerOut(): Signal { return this._onPointerOut; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();
    
        this._id = "CPoiMenuItem";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cGraphics != null, "CPoiMenuItem.ts :: init() :: CGraphics component not found." );
        console.assert( this._poiMenuItemType != null, "CPoiMenuItem.ts :: init() :: this._poiMenuItemType cannot be null." );
        console.assert( this._radius != null, "CPoiMenuItem.ts :: init() :: this._radius cannot be null." );
        console.assert( this._menuLevel != null, "CPoiMenuItem.ts :: init() :: this._menuLevel cannot be null." );
    
        this.draw();
    }

    public end(): void
    {
        this._onPointerOver.removeAll();
        this._onPointerOut.removeAll();
    }

    public decoratePoiParams( params: Array<any> ): void
    {
        switch ( this._poiMenuItemType )
        {
            case poi_menu_item_type.ONE: 
            case poi_menu_item_type.TWO:
            case poi_menu_item_type.THREE: 
            case poi_menu_item_type.FOUR: 
            case poi_menu_item_type.FIVE: 
            {
                params.push( this._poiMenuItemType - poi_menu_item_type.ONE + 1 );
                break;
            }

            case poi_menu_item_type.REMOVE: 
            {
                params[ 0 ] *= -1;
                break;
            }

            case poi_menu_item_type.ATTACK:
            case poi_menu_item_type.DEFENSE:
            case poi_menu_item_type.THREAT:
            case poi_menu_item_type.WILLPOWER:
            {
                params.push( this._poiMenuItemType );
                break;
            }
        }
    }

    public findPoiType(): poi_type
    {
        let result: poi_type = null;

        if ( this._poiMenuItemType <= poi_menu_item_type.RANDOMLY_DISCARD )
        {
            result = this._poiMenuItemType as number;
        }
        else if ( this._poiMenuItemType >= poi_menu_item_type.ONE
            && this._poiMenuItemType <= poi_menu_item_type.FIVE )
        {
            let cPoiMenuItem: CPoiMenuItem = this._cPoiMenuParentItem;
            while ( cPoiMenuItem && result == null )
            {
                switch (  cPoiMenuItem.type )
                {
                    case poi_menu_item_type.ADD:
                    case poi_menu_item_type.REMOVE: { result = poi_type.ADD_VALUE; break; }
                    case poi_menu_item_type.GIVE: { result = poi_type.GIVE_VALUE; break; }
                }

                if ( !result )
                {
                    cPoiMenuItem = cPoiMenuItem.parent;
                }
            }
        }

        return result;
    }

    public createPoiParams(): Array<any>
    {
        let result: Array<any> = null;

        if ( this._poiMenuItemType >= poi_menu_item_type.ONE
            && this._poiMenuItemType <= poi_menu_item_type.FIVE )
        {
            result = new Array<any>();
            let curNode: CPoiMenuItem = this;
            do
            {
                curNode.decoratePoiParams( result );
                curNode = curNode.parent;
            }
            while ( curNode );
        }

        return result;
    }

    // private:

    private draw(): void
    {
        this._go.cGraphics.g.beginFill( 0xFFFFFF, 1 );
        this._go.cGraphics.g.drawCircle( 0, 0, this._radius );
        this._go.cGraphics.g.endFill();
        this._go.cGraphics.g.buttonMode = true;
        this._go.cGraphics.g.interactive = true;
        this._go.cGraphics.g.on( "pointerover", this.onSelf_PointerOver.bind( this ) );
        this._go.cGraphics.g.on( "pointerout", this.onSelf_PointerOut.bind( this ) );

        switch ( this._poiMenuItemType )
        {
            case poi_menu_item_type.ONE: 
            case poi_menu_item_type.TWO:
            case poi_menu_item_type.THREE: 
            case poi_menu_item_type.FOUR: 
            case poi_menu_item_type.FIVE: 
            {
                const kValue: number = this._poiMenuItemType - poi_menu_item_type.ONE + 1;
                let text: PIXI.Text = new PIXI.Text( kValue.toString(), ServiceLocator.game.textStyler.subtitle );
                text.anchor.set( 0.5 );
                this._go.cGraphics.g.addChild( text );
                break;
            }

            case poi_menu_item_type.ADD: 
            {
                let text: PIXI.Text = new PIXI.Text( "+", ServiceLocator.game.textStyler.subtitle );
                text.anchor.set( 0.5 );
                this._go.cGraphics.g.addChild( text );
                break;
            }

            case poi_menu_item_type.REMOVE: 
            {
                let text: PIXI.Text = new PIXI.Text( "-", ServiceLocator.game.textStyler.subtitle );
                text.anchor.set( 0.5 );
                this._go.cGraphics.g.addChild( text );
                break;
            }

            default:
            {
                const kPoiIconId: string = this.findMenuItemIconId( this._poiMenuItemType );
                let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( kPoiIconId ).data ) );
                icon.anchor.set( 0.5, 0.5 );
                Utils.game.limitSideSize( this._radius, icon );
                this._go.cGraphics.g.addChild( icon );
            }
        }
    }

    private findMenuItemIconId( poiMenuItemType: poi_menu_item_type ): string
    {
        let result: string = null;

        switch ( poiMenuItemType )
        {
            case poi_menu_item_type.HIGHLIGHT: { result = "exclamation_mark"; break; }
            case poi_menu_item_type.BOW: { result = "rad_bow"; break; }
            case poi_menu_item_type.READY: { result = "rad_ready"; break; }
            case poi_menu_item_type.DISCARD: { result = "rad_discard"; break; }
            case poi_menu_item_type.GIVE_CONTROL: { result = "rad_give_control"; break; }
            case poi_menu_item_type.REVEAL: { result = "rad_put_face_up"; break; }
            case poi_menu_item_type.RANDOMLY_DISCARD: { result = "randomly_discard"; break; }
            case poi_menu_item_type.CARD_TOKEN_PROFILE_SUBMENU: { result = "profile"; break; }
            case poi_menu_item_type.ATTACK: { result = "attack"; break; }
            case poi_menu_item_type.DEFENSE: { result = "defense"; break; }
            case poi_menu_item_type.THREAT: { result = "threat"; break; }
            case poi_menu_item_type.WILLPOWER: { result = "willpower"; break; }
            case poi_menu_item_type.GIVE: { result = "give"; break; }
        }

        return result;
    }

    // #endregion //


    // #region Input Callbacks //

    private onSelf_PointerOver( event: PIXI.InteractionEvent ): void
    {
        this._onPointerOver.dispatch( this );
    }

    private onSelf_PointerOut( event: PIXI.InteractionEvent ): void
    {
        this._onPointerOut.dispatch( this );
    }

    // #endregion //
}

export const enum poi_menu_item_type
{
    // IMPORTANT: Same order as in poi_type.
    HIGHLIGHT = 0,
    BOW,
    READY,
    DISCARD,
    GIVE_CONTROL,
    REVEAL,
    RANDOMLY_DISCARD,
    // 
    CARD_TOKEN_PROFILE_SUBMENU,
    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE,
    //
    ATTACK,
    DEFENSE,
    THREAT,
    WILLPOWER,
    //
    ADD,
    REMOVE,
    GIVE
}