import Component from "../Component";

import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import { layer_type } from "../world/CGameLayerProvider";
import { location_type } from "../world/CGameWorld";
import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CSprite from "../pixi/CSprite";
import IGameObjectDropArea from "../../IGameObjectDropArea";
import DroppedEmitter from "../../DroppedEmitter";
import CardTokenFactory from "../../CardTokenFactory";
import { ISgCardToken, ISgQuestHolder } from "../../../view/game/SaveGameView";
import Signal from "../../../lib/signals/Signal";
import CContainer from "../pixi/CContainer";
import CMultiItemSelector, { mis_layout_type } from "./CMultiItemSelector";
import CShareableGameElement from "../CShareableGameElement";
import { status_type } from "../card/token/CCardTokenSide";


export default class CQuestHolder extends Component implements IGameObjectDropArea
{
    // #region Attributes //

    // private:

    private _location: location_type = null;
    private _activeLocationHolder: GameObject = null;

    private _bg: GameObject = null;
    private _multiItemSelector: GameObject = null;
    private _questTokenContainer: PIXI.Container = null;

    private _arrQuestToken: Array<GameObject> = null;

    private _isAnimated: boolean = true;

    // Signals.
    private _onQuestTokenSet: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get location(): location_type { return this._location; }
    public get questToken(): GameObject { return this._arrQuestToken.length > 0 ? this._arrQuestToken[ this._multiItemSelector.cMultiItemSelector.selectedIndex ] : null; }
    public get selector(): GameObject { return this._multiItemSelector; }

    public set location( value: location_type ) { this._location = value; }
    public set activeLocationHolder( value: GameObject ) { this._activeLocationHolder = value; }
    public set isAnimated( value: boolean ) { this._isAnimated = value; }

    // Signals.
    public get onQuestTokenSet(): Signal { return this._onQuestTokenSet; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CQuestHolder";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CQuestHolder.ts :: init() :: CContainer component not found." );
        console.assert( this._location != null, "CQuestHolder.ts :: init() :: this._location cannot be null." );

        this._arrQuestToken = new Array<GameObject>();

        this._bg = new GameObject( [ new CSprite() ] );
        this._bg.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "quest_holder" ).data );
        Utils.game.limitSideSize( 100, this._bg.cSprite.s );
        this._bg.cSprite.s.anchor.set( 0.5 );
        this._bg.init();
        this._go.cContainer.addChild( this._bg );

        this._questTokenContainer = new PIXI.Container();
        this._go.cContainer.c.addChild( this._questTokenContainer );

        let arrComponent: Array<Component> = [ new CContainer(), new CMultiItemSelector() ];
        if ( this._location == location_type.QUEST
            || this._location == location_type.PIT_QUEST
            || this._location == location_type.RIDDLE_QUEST
            || this._location == location_type.PURSUIT_QUEST
            || this._location == location_type.SPLIT_SAURON_QUEST )
        {
            arrComponent.push( new CShareableGameElement() );
        }
        this._multiItemSelector = new GameObject( arrComponent );
        this._multiItemSelector.oid = this._go.oid + "_selector";
        this._multiItemSelector.cMultiItemSelector.maxItemCount = 6;
        this._multiItemSelector.cMultiItemSelector.owner = this._go;
        this._multiItemSelector.cMultiItemSelector.layoutType = mis_layout_type.HORIZONTAL;
        this._multiItemSelector.init();
        this._go.cContainer.addChild( this._multiItemSelector );
        this._multiItemSelector.cMultiItemSelector.onItemSelected.add( this.onMultiItemSelectorItem_Selected, this );
    
        // Listen to events.
        if ( this._activeLocationHolder )
        {
            this._activeLocationHolder.cActiveLocationHolder.onActiveLocationUpdated.add( this.onActiveLocation_Updated, this );
        }
    }

    public end(): void
    {
        // Cleanup events.
        if ( this._activeLocationHolder )
        {
            this._activeLocationHolder.cActiveLocationHolder.onActiveLocationUpdated.remove( this.onActiveLocation_Updated, this );
        
            this._activeLocationHolder = null;
        }

        this._onQuestTokenSet.removeAll();

        for ( let questToken of this._arrQuestToken )
        {
            questToken.end();
        }
        this._arrQuestToken = null;

        this._multiItemSelector.end();
        this._multiItemSelector = null;

        this._bg.end();
        this._bg = null;

        super.end();
    }

    public findItems(): Array<GameObject> { return this._arrQuestToken.slice(); }

    public showActor( actor: GameObject, actionScope: action_scope_type ): void
    {
        let actorIndex: number = 0;
        for ( let questToken of this._arrQuestToken )
        {
            if ( questToken == actor )
            {
                break;
            }
            else
            {
                actorIndex += 1;
            }
        }

        if ( actorIndex != this._multiItemSelector.cMultiItemSelector.selectedIndex )
        {
            this._multiItemSelector.cMultiItemSelector.selectItem( actorIndex, actionScope );
        }
    }

    public saveGame(): ISgQuestHolder
    {
        let arrSgQuestToken: Array<ISgCardToken> = new Array<ISgCardToken>();
        for ( let questToken of this._arrQuestToken )
        {
            arrSgQuestToken.push( questToken.cCardToken.saveGame() );
        }

        return { 
            questTokens: arrSgQuestToken,
            selQuestIndex: this._multiItemSelector.cMultiItemSelector.selectedIndex };
    }

    public loadGame( sgQuestHolder: ISgQuestHolder, pass: number ): void
    {
        if ( pass == 0 )
        {
            for ( let sgCardToken of sgQuestHolder.questTokens )
            {
                let questCard: GameObject = GameObject.find( sgCardToken.oid.substr( 0, sgCardToken.oid.length - 1 ) );
                this._go.cDropArea.forceDrop( questCard, null, action_scope_type.LOCAL );
                this._arrQuestToken[ this._arrQuestToken.length - 1 ].cCardToken.loadGame( sgCardToken, pass );
            }

            if ( sgQuestHolder.selQuestIndex != -1 )
            {
                this._multiItemSelector.cMultiItemSelector.selectItem( sgQuestHolder.selQuestIndex, action_scope_type.LOCAL );
            }
        }
    }

    // #endregion //


    // #region IGameObjectDropArea //

    public validateDroppedGameObject( dropped: GameObject ): boolean
    {
        let result: boolean = this._arrQuestToken.length < this._multiItemSelector.cMultiItemSelector.maxItemCount
            
        if ( result ) 
        {
            result = dropped.cCard 
                        && ( dropped.cCard.curSide.cardInfo.type_code == "quest-intro"
                            || dropped.cCard.curSide.cardInfo.type_code == "quest"
                            || dropped.cCard.curSide.cardInfo.type_code == "player-side-quest"
                            || dropped.cCard.curSide.cardInfo.type_code == "encounter-side-quest" )
                    || dropped.cCardToken 
                        && ( dropped.cCardToken.curSide.cQuestIntroSide != null
                            || dropped.cCardToken.curSide.cQuestSide != null
                            || dropped.cCardToken.curSide.cSideQuestSide != null );
        }

        return result;
    }

    public processDroppedGameObject( dropped: GameObject, global: PIXI.IPoint, actionScopeType: action_scope_type ): void
    {
        let questToken: GameObject = null;
        if ( dropped.cCard )
        {
            let ctf: CardTokenFactory = new CardTokenFactory();
            questToken = ctf.create( dropped );
        }
        else if ( dropped.cCardToken )
        {
            questToken = dropped;
        }
        questToken.cCardToken.location = this._location;
        questToken.cDropArea.isPropagate = true;
        questToken.cContainer.c.position.set( -questToken.cCardToken.calcWidth() * 0.5, -questToken.cCardToken.calcHeight() * 0.5 );
        this._questTokenContainer.addChild( questToken.cContainer.c );
        //
        questToken.cDraggable.onPostDropped.addOnce( this.onQuestToken_PostDropped, this );
        questToken.cCardToken.onSizeUpdated.add( this.onQuestTokenSize_Updated, this );
        questToken.cShareableGameElement.onLocked.add( this.onQuestToken_Locked, this );
        questToken.cShareableGameElement.onUnlocked.add( this.onQuestToken_Unlocked, this );
        
        if ( this._activeLocationHolder )
        {
            questToken.cDraggable.onDropped.addOnce( this.onQuestToken_Dropped, this);

            this.setQuestTokenLockOverlayVisibility( questToken, false );
            let arrLocationToken: Array<GameObject> = this._activeLocationHolder.cActiveLocationHolder.findItems();
            for ( let locationToken of arrLocationToken )
            {
                if ( !locationToken.cCardToken.cCurSide.hasStatus( status_type.EXPLORED ) )
                {
                    this.setQuestTokenLockOverlayVisibility( questToken, true );
                    break;
                }
            }
        }
        
        if ( this._arrQuestToken.length > 0 )
        {
            this._arrQuestToken[ this._multiItemSelector.cMultiItemSelector.selectedIndex ].cContainer.c.visible = false;
        }
        this._arrQuestToken.push( questToken );
        this._multiItemSelector.cMultiItemSelector.addItem();
        this._multiItemSelector.cMultiItemSelector.setSelectedIndex( this._multiItemSelector.cMultiItemSelector.itemCount - 1 );
        this.updateMultiItemSelectorPosition();

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.DROP_AT_DROP_AREA, null, [ dropped.oid, this._go.oid, null ] );
        }

        if ( this._isAnimated && ServiceLocator.game.isAnimated )
        {   
            // Sfx.
            ServiceLocator.audioManager.playSfx( "token_dropped" );

            // Vfx.
            this.playDroppedVfx();
            Utils.anim.dropFromCorner( questToken.cContainer.c );
        }

        if ( dropped.cCard )
        {
            dropped.end();
        }

        this._onQuestTokenSet.dispatch( questToken );
    }

        private updateMultiItemSelectorPosition(): void
        {
            this._multiItemSelector.cContainer.c.position.set(
                -this._multiItemSelector.cContainer.c.width * 0.5,
                -this._arrQuestToken[ this._multiItemSelector.cMultiItemSelector.selectedIndex ].cCardToken.cCurSide.bg.cContainer.c.height * 0.5 - 10 );
        }

        private playDroppedVfx(): void
        {
            let droppedEmitter: DroppedEmitter = new DroppedEmitter();
            const kVfxLayer: GameObject = Utils.game.findGameProviderLayer( this._go, layer_type.VFX );
            droppedEmitter.layer = kVfxLayer;
            droppedEmitter.init();
            const kVfxPos: PIXI.IPoint = kVfxLayer.cContainer.c.toLocal( this._bg.cContainer.c.getGlobalPosition() );
            droppedEmitter.playLarge( kVfxPos.x, kVfxPos.y );
        }

    // #endregion //


    // #region Callbacks //

    private onQuestTokenSize_Updated( go: GameObject ): void
    {
        go.cContainer.c.x = -go.cCardToken.calcWidth() * 0.5;
    }

    private onQuestToken_Locked( go: GameObject ): void
    {
        this._multiItemSelector.cMultiItemSelector.setEnabled( false );
    }

    private onQuestToken_Unlocked( go: GameObject ): void
    {
        this._multiItemSelector.cMultiItemSelector.setEnabled( true );
    }

    private onQuestToken_Dropped( go: GameObject ): void
    {
        this.setQuestTokenLockOverlayVisibility( go, false );
    }

        private setQuestTokenLockOverlayVisibility( questToken: GameObject, isVisible: boolean ): void
        {
            if ( questToken.cCardToken.frontSide.cQuestSide )
            {
                questToken.cCardToken.frontSide.cQuestSide.setLockOverlayVisibility( isVisible );
            }
            else if ( questToken.cCardToken.frontSide.cSideQuestSide )
            {
                questToken.cCardToken.frontSide.cSideQuestSide.setLockOverlayVisibility( isVisible );
            }
            else if ( questToken.cCardToken.backSide.cSideQuestSide )
            {
                questToken.cCardToken.backSide.cSideQuestSide.setLockOverlayVisibility( isVisible );
            }
        }

    private onQuestToken_PostDropped( go: GameObject ): void
    {
        if ( go.cCardToken )
        {
            go.cShareableGameElement.onLocked.remove( this.onQuestToken_Locked, this );
            go.cShareableGameElement.onUnlocked.remove( this.onQuestToken_Unlocked, this );
            go.cCardToken.onSizeUpdated.remove( this.onQuestTokenSize_Updated, this );
        }

        this._arrQuestToken.splice( this._arrQuestToken.indexOf( go ), 1 );
        this._multiItemSelector.cMultiItemSelector.removeItem();

        if ( this._arrQuestToken.length > 0 )
        {
            this._arrQuestToken[ this._multiItemSelector.cMultiItemSelector.selectedIndex ].cContainer.c.visible = true;
            this.updateMultiItemSelectorPosition();
        }
    }

    private onMultiItemSelectorItem_Selected( lastIndex: number, selIndex: number ): void
    {
        if ( lastIndex >= 0 )
        {
            this._arrQuestToken[ lastIndex ].cContainer.c.visible = false;
        }
        this._arrQuestToken[ selIndex ].cContainer.c.visible = true;

        this.updateMultiItemSelectorPosition();
    }

    private onActiveLocation_Updated( isExplored: boolean ): void
    {
        for ( let questToken of this._arrQuestToken )
        {
            this.setQuestTokenLockOverlayVisibility( questToken, !isExplored );
        }
    }

    // #endregion //
}