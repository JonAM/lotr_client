import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CButton from "../input/CButton";
import CGraphics from "../pixi/CGraphics";
import CTooltipReceptor from "./CTooltipReceptor";
import Utils from "../../../Utils";
import { ISgCustomPanel, ISgCustomPanelManager } from "../../../view/game/SaveGameView";
import SignalBinding from "../../../lib/signals/SignalBinding";
import { ICustomPanel } from "../../CustomPanelFactory";


export default class CCustomPanelManager extends Component
{
    // #region Attributes //

    // private:

    private _arrCustomPanel: Array<ICustomPanel> = null;

    private _toggleBtnContainer: PIXI.Container = null;
    private _arrToggleBtn: Array<GameObject> = null;

    // #endregion //


    // #region Properties //
    
    public set panels( value: Array<ICustomPanel> ) { this._arrCustomPanel = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();
    
        this._id = "CCustomPanelManager";
    }

    public init(): void
    {
        console.assert( this._go.cContainer != null, "CCustomPanelManager.ts :: init() :: CContainer component not found." );
   
        this._arrToggleBtn = new Array<GameObject>();
        if ( !this._arrCustomPanel )
        {
            this._arrCustomPanel = new Array<ICustomPanel>();
        }

        this._toggleBtnContainer = new PIXI.Container();
        this._go.cContainer.c.addChild( this._toggleBtnContainer );

        let itemIndex: number = 0;
        for ( let item of this._arrCustomPanel )
        {
            let toggleBtn: GameObject = this.createToggleBtn( item.buttonIconId, item.buttonTooltip );
            toggleBtn.cContainer.c.x += ( toggleBtn.cContainer.c.width + 20 ) * itemIndex;
            this._toggleBtnContainer.addChild( toggleBtn.cContainer.c );
            let sb: SignalBinding = toggleBtn.cButton.onClick.add( this.onToggleBtn_Click, this );
            sb.params = [ item.content ];
            //
            this._arrToggleBtn.push( toggleBtn );

            item.content.cContainer.c.position.x = ( ServiceLocator.game.app.screen.width - item.content.cContainer.c.width ) * 0.5;
            item.content.cContainer.c.visible = false;
            this._go.cContainer.addChild( item.content );

            itemIndex += 1;
        }

        this._toggleBtnContainer.x = ServiceLocator.game.app.screen.width * 0.5 - this._toggleBtnContainer.width * 0.5;
    }

        private createToggleBtn( iconId: string, tooltip: string ): GameObject
        {
            let result: GameObject = new GameObject( [ new CGraphics(), new CButton(), new CTooltipReceptor() ] );
            result.cGraphics.g.lineStyle( 1, 0x000000, 1 );
            result.cGraphics.g.beginFill( 0xefe3af, 1 );
            result.cGraphics.g.drawRect( 0, 0, 64, 32 );
            result.cGraphics.g.endFill();
            result.cTooltipReceptor.text = tooltip;
            result.init();

            let icon: PIXI.Sprite = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( iconId ) );
            icon.position.set( result.cContainer.c.width * 0.5, result.cContainer.c.height * 0.5 );
            icon.anchor.set( 0.5 );
            const kWidthRatio: number = icon.width / result.cContainer.c.width;
            const kHeightRatio: number = icon.height / result.cContainer.c.height;
            if ( kWidthRatio > 0.9 && kWidthRatio >= kHeightRatio )
            {
                Utils.game.limitSideSize( 60, icon );
            } 
            else if ( kHeightRatio > 0.9 && kHeightRatio > kWidthRatio )
            {
                Utils.game.limitSideSize( 28, icon );
            }
            result.cContainer.c.addChild( icon );

            return result;
        }

    public end(): void
    {
        for ( let toggleBtn of this._arrToggleBtn )
        {
            toggleBtn.end();
        }
        this._arrToggleBtn = null;

        super.end();
    }

    public find( panelId: string ): GameObject
    {
        let result: GameObject = null;

        for ( let item of this._arrCustomPanel )
        {
            if ( item.content.oid == panelId )
            {
                result = item.content;
                break;
            }
        }

        return result;
    }

    public show( panelId: string ): void
    {
        let panel: GameObject = this.find( panelId );
        if ( !panel.cContainer.c.visible )
        {
            this.onToggleBtn_Click( panel );
        }
    }

    public hide( panelId: string ): void
    {
        let panel: GameObject = this.find( panelId );
        if ( panel.cContainer.c.visible )
        {
            this.onToggleBtn_Click( panel );
        }
    }

    public hideAll(): void
    {
        for ( let item of this._arrCustomPanel )
        {
            if ( item.content.cContainer.c.visible )
            {
                this.onToggleBtn_Click( item.content );
                break;
            }
        }
    }

    public checkPanelVisibility( panelId: string ): boolean
    {
        const kPanelIndex: number = this.findPanelIndex( panelId );
        
        return kPanelIndex != -1 ? this._arrToggleBtn[ kPanelIndex ].cContainer.c.visible : false;
    }

    public setPanelVisibility( panelId: string, isVisible: boolean ): void
    {
        const kPanelIndex: number = this.findPanelIndex( panelId );
        if ( this._arrCustomPanel[ kPanelIndex ].content.cContainer.c.visible )
        {
            this.onToggleBtn_Click( this._arrCustomPanel[ kPanelIndex ].content );
        }
        this._arrToggleBtn[ kPanelIndex ].cContainer.c.visible = isVisible;

        this.repositionToggleBtns();
    }

        private findPanelIndex( panelId: string ): number
        {
            let panelIndex: number = 0;
            while ( panelIndex < this._arrCustomPanel.length && this._arrCustomPanel[ panelIndex ].content.oid != panelId )
            {
                panelIndex += 1;
            }

            return panelIndex < this._arrCustomPanel.length ? panelIndex : -1;
        }

        private repositionToggleBtns(): void
        {
            let nextToggleBtnX: number = 0;
            for ( let toggleBtn of this._arrToggleBtn )
            {
                if ( toggleBtn.cContainer.c.visible )
                {
                    toggleBtn.cContainer.c.x = nextToggleBtnX;
                    nextToggleBtnX = toggleBtn.cContainer.c.x + toggleBtn.cContainer.c.width + 20;
                }
            }
    
            this._toggleBtnContainer.x = ServiceLocator.game.app.screen.width * 0.5 - this._toggleBtnContainer.width * 0.5;
        }

    public addPanelAt( item: ICustomPanel, atIndex: number ): void
    {
        let toggleBtn: GameObject = this.createToggleBtn( item.buttonIconId, item.buttonTooltip );
        this._toggleBtnContainer.addChild( toggleBtn.cContainer.c );
        let sb: SignalBinding = toggleBtn.cButton.onClick.add( this.onToggleBtn_Click, this );
        sb.params = [ item.content ];
        //
        this._arrToggleBtn.splice( atIndex, 0, toggleBtn );

        item.content.cContainer.c.position.x = ( ServiceLocator.game.app.screen.width - item.content.cContainer.c.width ) * 0.5;
        item.content.cContainer.c.visible = false;
        this._go.cContainer.addChild( item.content );
        //
        this._arrCustomPanel.splice( atIndex, 0, item );

        this.repositionToggleBtns();
    }

    public remove( panelId: string ): void
    {
        const kPanelIndex: number = this.findPanelIndex( panelId );

        let panel: GameObject = this._arrCustomPanel[ kPanelIndex ].content;
        if ( panel.cContainer.c.visible )
        {
            this.onToggleBtn_Click( panel );
        }
        panel.end();
        this._arrCustomPanel.splice( kPanelIndex, 1 );

        this._arrToggleBtn[ kPanelIndex ].end();
        this._arrToggleBtn.splice( kPanelIndex, 1 );
        this.repositionToggleBtns();
    }

    public setPanelIconId( panelId: string, iconId: string ): void
    {
        const kPanelIndex: number = this.findPanelIndex( panelId );
        this._arrCustomPanel[ kPanelIndex ].buttonIconId = iconId;

        let toggleBtn: GameObject = this._arrToggleBtn[ kPanelIndex ];
        let icon: PIXI.Sprite = toggleBtn.cContainer.c.getChildAt( 0 ) as PIXI.Sprite;
        icon.texture = ServiceLocator.resourceStack.findAsTexture( iconId );
        const kWidthRatio: number = icon.width / toggleBtn.cContainer.c.width;
        const kHeightRatio: number = icon.height / toggleBtn.cContainer.c.height;
        if ( kWidthRatio > 0.9 && kWidthRatio >= kHeightRatio )
        {
            Utils.game.limitSideSize( 60, icon );
        } 
        else if ( kHeightRatio > 0.9 && kHeightRatio > kWidthRatio )
        {
            Utils.game.limitSideSize( 28, icon );
        }
    }

    public saveGame(): ISgCustomPanelManager
    {
        let arrSgCustomPanel: Array<ISgCustomPanel> = new Array<ISgCustomPanel>();
        let arrToggleBtnIconId: Array<string> = new Array<string>();
        for ( let item of this._arrCustomPanel )
        {
            arrSgCustomPanel.push( item.content.cCustomArea.saveGame() );
            arrToggleBtnIconId.push( item.buttonIconId );
        }
        let arrToggleBtnVisibility: Array<boolean> = new Array<boolean>();
        for ( let toggleBtn of this._arrToggleBtn )
        {
            arrToggleBtnVisibility.push( toggleBtn.cContainer.c.visible );
        }

        return {
            toggleBtnVisibility: arrToggleBtnVisibility,
            toggleBtnIconIds: arrToggleBtnIconId,
            customPanels: arrSgCustomPanel };
    }

    public loadGame( sgCustomPanelManager: ISgCustomPanelManager, pass: number ): void
    {
        this._go.cContainer.c.visible = true;
        for ( let i: number = 0; i < sgCustomPanelManager.toggleBtnVisibility.length; ++i )
        {
            this._arrToggleBtn[ i ].cContainer.c.visible = sgCustomPanelManager.toggleBtnVisibility[ i ];
        }
        this.repositionToggleBtns();

        let itemIndex: number = 0;
        for ( let customPanel of sgCustomPanelManager.customPanels )
        {
            this._arrCustomPanel[ itemIndex ].content.cCustomArea.loadGame( customPanel, pass );  

            ++itemIndex;
        }
        itemIndex = 0;
        for ( let toggleBtnIconId of sgCustomPanelManager.toggleBtnIconIds )
        {
            if ( toggleBtnIconId !=  this._arrCustomPanel[ itemIndex ].buttonIconId )
            {
                this.setPanelIconId( this._arrCustomPanel[ itemIndex ].content.oid, toggleBtnIconId );
            }

            ++itemIndex;
        }
    }

    // #endregion //


    // #region Input Callbacks //

    private onToggleBtn_Click( panel: GameObject ): void
    {
        for ( let item of this._arrCustomPanel )
        {
            if ( item.content != panel )
            {
                item.content.cContainer.c.visible = false;
            }
        }

        panel.cContainer.c.visible = !panel.cContainer.c.visible;

        if ( panel.cContainer.c.visible )
        {
            panel.cCustomArea.onShown();
            this._toggleBtnContainer.y = panel.cContainer.c.height;
        }
        else
        {
            this._toggleBtnContainer.y = 0;
            panel.cCustomArea.onHidden();
        }
    }

    // #endregion //
}