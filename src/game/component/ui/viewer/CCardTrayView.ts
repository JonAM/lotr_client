import CMaximizedView from "./CMaximizedView";

import { action_scope_type, IRemoteCard } from "../../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../../ServiceLocator";
import { view_button_type } from "../CViewer";
import * as PIXI from "pixi.js";

import GameObject from "../../../GameObject";
import CardViewerController from "./controller/CardViewerController";
import { context_button_type } from "./CViewContextBtn";
import { ISgCard, ISgCardView } from "../../../../view/game/SaveGameView";
import CDropArea from "../../input/CDropArea";


export default class CCardTrayView extends CMaximizedView 
{
    // #region Attributes //

    // private:

    // Quick access.
    private _cardViewerController: CardViewerController = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CCardTrayView";
    }

    public init(): void
    {
        super.init();

        this._cardViewerController = this._controller as CardViewerController;

        this.drawBg();
        this.onItemsSorted();
    }

    // overrides.
    public sortItems(): void {}

    public saveGame(): ISgCardView
    {
        let result: ISgCardView = { cards: [] };

        for ( let displayObject of this._itemHolder.cContainer.c.children )
        {
            let card: GameObject = displayObject[ "go" ];
            let sgCard: ISgCard = card.cCard.saveGame()
            sgCard.x = card.cContainer.c.x;
            sgCard.y = card.cContainer.c.y;
            result.cards.push( sgCard );
        }

        return result;
    }

    public loadGame( sgCardView: ISgCardView, pass: number ): void
    {
        if ( pass == 0 )
        {
            for ( let sgCard of sgCardView.cards )
            {
                let card: GameObject = GameObject.find( sgCard.oid );
                this._go.cDropArea.forceDrop( card, new PIXI.Point( CDropArea.kPredefinedDropPositionCode, 0 ), action_scope_type.LOCAL );
                card.cCard.loadGame( sgCard, pass );
                card.cContainer.c.position.set( sgCard.x, sgCard.y );
            }
        }
    }

    // #endregion //


    // #region IGameObjectDropArea //

    public validateDroppedGameObject( token: GameObject ): boolean
    {
        return this._cardViewerController.validateDroppedGameObject( token ); 
    }

    public processDroppedGameObject( card: GameObject, global: PIXI.IPoint, actionScopeType: action_scope_type ): void
    {
        this._cardViewerController.processDroppedGameObject( card, global, actionScopeType );

        // Sfx.
        ServiceLocator.audioManager.playSfx( "token_dropped" );
    }

    // #endregion //

    
    // #region Callbacks //

    // protected:

    protected onViewBtn_Click( buttonType: view_button_type ): void
    {
        this._controller.onViewBtnSelected( buttonType ); 
    }

    protected onContextBtn_Click( buttonType: context_button_type, params: Array<any> ): void
    {
        this._controller.onContextBtnSelected( buttonType, params ); 
    }

    // #endregion //
}