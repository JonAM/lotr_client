import { player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../../ServiceLocator";

import CViewer, { view_button_type } from "../CViewer";
import GameObject from "../../../GameObject";
import Signal from "../../../../lib/signals/Signal";
import { context_button_type } from "./CViewContextBtn";


export default abstract class ViewerController
{
    // #region Attributes //

    // protected:

    protected _viewer: CViewer = null;

    // Quick access.
    protected _itemHolder: GameObject = null;

    // Signals.
    protected _onItemDropped: Signal = new Signal();
    protected _onItemDragCanceled: Signal = new Signal();
    

    // #endregion //


    // #region Properties //
    
    public set viewer( value: CViewer ) { this._viewer = value; }

    // Signals.
    public get onItemDropped(): Signal { return this._onItemDropped; }
    public get onItemDragCanceled(): Signal { return this._onItemDragCanceled; }

    // #endregion //


    // #region Methods //

    // public:

    public init(): void
    {
        console.assert( this._viewer != null, "ViewerController.ts :: init() :: this._viewer cannot be null." );
    
        this._itemHolder = this._viewer.cCardView.itemHolder;
    }

    public end(): void
    {
        this._onItemDropped.removeAll();
        this._onItemDragCanceled.removeAll();

        this._viewer = null;
        this._itemHolder = null;
    }

    // virtual.
    public onViewShown(): void {}

    // virtual.
    public onViewHidden(): void 
    {
        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.VIEWER_CLOSED, null, [ this._viewer.location ] );
    }

    public onViewBtnSelected( buttonType: view_button_type ): boolean
    {
        let result: boolean = true;

        switch ( buttonType )
        {
            case view_button_type.CLOSE: { this._viewer.hide(); break; }
            case view_button_type.MINIMIZE: { this._viewer.minimize(); break; }
            case view_button_type.MAXIMIZE: { this._viewer.maximize(); break; }

            default: { result = false; }
        }

        return result;
    }

    // virtual.
    public onContextBtnSelected( buttonType: context_button_type, params: Array<any> ): boolean { return false; }

    public findItemCount(): number
    {
        return this._itemHolder.cContainer.c.children.length;
    }

    // #endregion
}