import Component from "../../../Component";

import { action_scope_type, IRemoteCard } from "../../../../../service/socket_io/GameSocketIOController";
import { player_type } from "../../../world/CPlayerArea";
import ServiceLocator from "../../../../../ServiceLocator";
import { view_button_type } from "../../CViewer";
import * as PIXI from "pixi.js";

import GameObject from "../../../../GameObject";
import Signal from "../../../../../lib/signals/Signal";
import IGameObjectDropArea from "../../../../IGameObjectDropArea";


export default class CDeckBottomMarker extends Component implements IGameObjectDropArea
{
    // #region Attributes //

    // private:

    // Signals.
    private _onCardDropped: Signal = new Signal();

    // #endregion //


    // #region Properties //

    // Signals.
    public get onCardDropped(): Signal { return this._onCardDropped; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CDeckBottomMarker";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CDeckBottomMarker.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cContainer != null, "CDeckBottomMarker.ts :: init() :: CDropArea component not found." );
    
        this._go.cDropArea.target = this;

        let text: PIXI.Text = new PIXI.Text( jQuery.i18n( "BOTTOM" ), ServiceLocator.game.textStyler.normal );
        text.anchor.set( 0.5, 1 );
        text.rotation = -Math.PI * 0.5;

        let bg: PIXI.Graphics = new PIXI.Graphics();
        bg.lineStyle( 2, 0x000000, 1 );
        bg.beginFill( 0xEFE3AF );
        bg.drawRect( -2, -text.width * 0.5 - 10, -text.height, text.width + 20 );
        bg.endFill();

        this._go.cContainer.c.addChild( bg );
        this._go.cContainer.c.addChild( text );
    }

    public end(): void
    {
        this._onCardDropped.removeAll();

        super.end();
    }

    // #endregion //


    // #region IGameObjectDropArea //

    public validateDroppedGameObject( go: GameObject ): boolean
    {
        return go.cCard != null; 
    }

    public processDroppedGameObject( card: GameObject, global: PIXI.IPoint, actionScopeType: action_scope_type ): void
    {
        this._onCardDropped.dispatch( card, actionScopeType ); 
    }

    // #endregion //
}