import Component from "../../Component";

import ServiceLocator from "../../../../ServiceLocator";
import Utils from "../../../../Utils";
import * as PIXI from "pixi.js";

import Signal from "../../../../lib/signals/Signal";
import GameObject from "../../../GameObject";
import CButton from "../../input/CButton";
import CGraphics from "../../pixi/CGraphics";
import CContainer from "../../pixi/CContainer";
import CTokenCounter from "../CTokenCounter";
import CDraggable from "../../input/CDraggable";
import SignalBinding from "../../../../lib/signals/SignalBinding";


export default class CViewContextBtn extends Component
{
    // #region Attributes //

    // private:

    private _buttonType: context_button_type = null;
    private _isCounter: boolean = false;
    private _isAllBtn: boolean = false;

    private _bgBtn: GameObject = null;
    private _allBtn: GameObject = null;
    private _counter: GameObject = null;

    // Signals.
    private _onClick: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get buttonType(): context_button_type { return this._buttonType; }
    
    public set buttonType( value: context_button_type ) { this._buttonType = value; }
    public set isCounter( value: boolean ) { this._isCounter = value; }
    public set isAllBtn( value: boolean ) { this._isAllBtn = value; }

    // Signals.
    public get onClick(): Signal { return this._onClick; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CViewContextBtn";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CViewContextBtn.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cTooltipReceptor != null, "CViewContextBtn.ts :: init() :: CTooltipReceptor component not found." );
        console.assert( this._buttonType != null, "CViewContextBtn.ts :: init() :: this._buttonType cannot be null." );

        this._bgBtn = new GameObject( [ new CGraphics(), new CButton() ] );
        this._bgBtn.cGraphics.g.lineStyle( 2, 0x000000, 1 );
        this._bgBtn.cGraphics.g.beginFill( 0xEFE3AF );
        this._bgBtn.cGraphics.g.drawRect( 0, 0, 50, 50 );
        this._bgBtn.cGraphics.g.endFill();
        //
        const kArrIconId: Array<string> = [ 
            "shuffle", "randomly_discard", "discard_x", "removed_from_game",
            "reveal_x", "draw_x", "mulligan", "reshuffle", 
            "sort_by_sphere", "sort_from_a_to_z", "switch" ];
        let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( kArrIconId[ this._buttonType ] ).data ) );
        Utils.game.limitSideSize( 30, icon );
        icon.anchor.set( 0.5, 0.5 );
        icon.position.set( 25, 25 );
        this._bgBtn.cGraphics.g.addChild( icon );
        //
        this._bgBtn.init();
        this._go.cContainer.addChild( this._bgBtn );
        //
        this._bgBtn.cButton.onClick.add( this.onBgBtn_Click, this );

        if ( this._isCounter )
        {
            // Counter.
            this._counter = new GameObject( [ new CContainer(), new CTokenCounter(), new CDraggable() ] );
            this._counter.cTokenCounter.count = 1;
            this._counter.cTokenCounter.minCount = 1;
            this._counter.cTokenCounter.maxCount = 99;
            this._counter.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "custom_counter_white" ).data );
            this._counter.init();
            this._counter.cTokenCounter.setPlayerActionType( null );
            //
            icon = PIXI.Sprite.from( PIXI.Texture.from( ServiceLocator.resourceStack.find( "custom_counter_white" ).data ) );
            icon.anchor.set( 0.5 );
            Utils.game.limitSideSize( 20, icon );
            this._counter.cContainer.c.addChild( icon );
            // 
            let text: PIXI.Text = new PIXI.Text( "1", ServiceLocator.game.textStyler.small );
            text.name = "text";
            text.anchor.set( 0.5 );
            this._counter.cContainer.c.addChild( text );
            let sb: SignalBinding = this._counter.cTokenCounter.onCountUpdated.add( this.onCounter_Updated, this );
            sb.params = [ text ];
            //
            this._counter.cContainer.c.position.set( 
                this._go.cContainer.c.width - this._counter.cContainer.c.width * 0.5 - 2, 
                this._go.cContainer.c.height - this._counter.cContainer.c.height * 0.5 - 2 );
            this._go.cContainer.addChild( this._counter );
        }

        if ( this._isAllBtn )
        {
            // All.
            this._allBtn = new GameObject( [ new CGraphics(), new CButton() ] );
            this._allBtn.cGraphics.g.lineStyle( 2, 0x000000, 1 );
            this._allBtn.cGraphics.g.beginFill( 0xeccc68 );
            this._allBtn.cGraphics.g.drawRect( 0, 0, 20, 40 );
            this._allBtn.cGraphics.g.endFill();
            // Text.
            let text: PIXI.Text = new PIXI.Text( jQuery.i18n( "ALL" ), ServiceLocator.game.textStyler.small );
            text.anchor.set( 0.5 );
            text.rotation = Math.PI * 0.5;
            text.position.set( this._allBtn.cContainer.c.width * 0.5, this._allBtn.cContainer.c.height * 0.5 );
            this._allBtn.cContainer.c.addChild( text );
            //
            this._allBtn.init();
            this._allBtn.cContainer.c.position.set( this._bgBtn.cContainer.c.width - 1, 5 );
            this._go.cContainer.addChild( this._allBtn );
            //
            this._allBtn.cButton.onClick.add( this.onAllBtn_Click, this );
        }
    }

    public end(): void
    {
        this._onClick.removeAll();

        if ( this._allBtn )
        {
            this._allBtn.end();
            this._allBtn = null;
        }

        if ( this._counter )
        {
            this._counter.end();
            this._counter = null;
        }

        this._bgBtn.end();
        this._bgBtn = null;

        this._buttonType = null;
        this._isCounter = false;
        this._isAllBtn = false;
    }

    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        this._go.cContainer.c.alpha = isEnabled ? 1 : 0.5;
        this._go.cContainer.c.interactive = isEnabled;
        
        this._bgBtn.cButton.setEnabled( isEnabled );

        if ( this._allBtn )
        {
            this._allBtn.cButton.setEnabled( isEnabled );
        }

        if ( this._counter )
        {
            this._counter.cTokenCounter.setEnabled( isEnabled );
        }
    }

    // #endregion //
    

    // #region Input Callbacks //

    private onBgBtn_Click(): void
    {
        let count: number = null;
        if ( this._counter )
        {
            count = this._counter.cTokenCounter.count;
        }

        this._onClick.dispatch( this._buttonType, count ? [ count ] : null );
    }

    private onAllBtn_Click(): void
    {
        if ( this._buttonType == context_button_type.RANDOMLY_DISCARD )
        {
            this._onClick.dispatch( context_button_type.DISCARD_X, [ 99 ] );
        }
        else
        {
            this._onClick.dispatch( this._buttonType, [ 99 ] );
        }
    }

    // #endregion //


    // #region Other Callbacks //

    private onCounter_Updated( text: PIXI.Text, count: number, delta: number, isPlayerInput: boolean ): void
    {
        text.text = count.toString();

        // Sfx.
        ServiceLocator.audioManager.playSfx( delta > 0 ? "counter_up" : "counter_down" );
    }

    // #endregion //
}

export const enum context_button_type
{
    SHUFFLE = 0,
    RANDOMLY_DISCARD,
    DISCARD_X,
    REMOVE_ALL,
    REVEAL_X,
    DRAW_X_PLAYER_CARDS,
    MULLIGAN,
    RESHUFFLE,
    SORT_BY_SPHERE,
    SORT_FROM_A_TO_Z,
    SWITCH
}