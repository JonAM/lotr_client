import Component from "../../Component";

import { player_type } from "../../world/CPlayerArea";
import ServiceLocator from "../../../../ServiceLocator";
import Utils from "../../../../Utils";
import { view_button_type } from "../CViewer";
import * as PIXI from "pixi.js";

import Signal from "../../../../lib/signals/Signal";
import GameObject from "../../../GameObject";
import CContainer from "../../pixi/CContainer";
import CButton from "../../input/CButton";
import CGraphics from "../../pixi/CGraphics";
import SignalBinding from "../../../../lib/signals/SignalBinding";
import IGameObjectDropArea from "../../../IGameObjectDropArea";
import ViewerController from "./ViewerController";
import CTooltipReceptor from "../CTooltipReceptor";
import { action_scope_type } from "../../../../service/socket_io/GameSocketIOController";
import CViewContextBtn, { context_button_type } from "./CViewContextBtn";


export default abstract class CMaximizedView extends Component implements IGameObjectDropArea
{
    // #region Attributes //

    // private:

    private _title: string = null;
    private _headerIconId: string = null;
    private _width: number = null;
    private _height: number = null;
    private _arrContextBtnType: Array<context_button_type> = new Array<context_button_type>();

    private _headerBg: GameObject = null;
    private _bodyBg: PIXI.Graphics = null;
    private _viewBtnContainer: PIXI.Container = null;

    // Signals.
    private _onItemDraggedOut: Signal = new Signal();
    private _onItemCountUpdated: Signal = new Signal();
    private _onContentUpdated: Signal = new Signal();

    // protected:
    
    protected _controller: ViewerController = null;

    protected _body: PIXI.Container = null;
    protected _itemHolder: GameObject = null;
    protected _contextBtnContainer: PIXI.Container = null;

    // #endregion //


    // #region Properties //

    public get itemHolder(): GameObject { return this._itemHolder; }
    public get body(): PIXI.Container { return this._body; }
    public get width(): number { return this._width; }
    public get height(): number { return this._height; }
    
    public set controller( value: ViewerController ) { this._controller = value; }
    public set title( value: string ) { this._title = value; }
    public set headerIconId( value: string ) { this._headerIconId = value; }
    public set width( value: number ) { this._width = value; }
    public set height( value: number ) { this._height = value; }
    public set contextBtnTypes( value: Array<context_button_type> ) { this._arrContextBtnType = value; }

    // Signals.
    public get onItemDraggedOut(): Signal { return this._onItemDraggedOut; }
    public get onItemCountUpdated(): Signal { return this._onItemCountUpdated; }
    public get onContentUpdated(): Signal { return this._onContentUpdated; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CMaximizedView";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CMaximizedView.ts :: init() :: CContainer component not found." );
        console.assert( this._controller != null, "CMaximizedView.ts :: init() :: this._controller cannot be null." );
        console.assert( this._title != null, "CMaximizedView.ts :: init() :: this._title cannot be null." );
        
        this._go.cContainer.c.interactive = true;
        this._go.cContainer.c.on( "pointermove", this.onPointerMove.bind( this ) );

        let header: PIXI.Container = this.createHeader();
        this._go.cContainer.c.addChild( header );

        this._body = this.createBody();
        this._body.y = header.height;
        this._go.cContainer.c.addChild( this._body );

        this._viewBtnContainer = this.createViewButtons();
        header.addChild( this._viewBtnContainer );

        this._contextBtnContainer = this.createContextButtons();
        this._body.addChild( this._contextBtnContainer );
    }

    public end(): void
    {
        this._headerBg.end();
        this._headerBg = null;

        this._title = null;
        this._arrContextBtnType = null;

        this._itemHolder.end();
        this._itemHolder = null;

        // Owned signals.
        this._onItemCountUpdated.removeAll();
        this._onContentUpdated.removeAll();
    }

    public isEmpty(): boolean
    {
        return this._itemHolder.cContainer.c.children.length == 0;
    }

    public showContextButtons(): void
    {
        this._contextBtnContainer.visible = true;
    }

    public hideContextButtons(): void
    {
        this._contextBtnContainer.visible = false;
    }

    public setContextBtnState( arrContextBtnState: Array<IContextBtnState> ): void
    {
        let isDirty: boolean = false;
        for ( let buttonState of arrContextBtnState )
        {
            let button: GameObject = this.findContextBtn( buttonState.buttonType );
            if ( buttonState.isVisible !== undefined )
            {
                button.cContainer.c.visible = buttonState.isVisible;
                isDirty = true;
            }
            else if ( buttonState.isEnabled !== undefined )
            {
                button.cViewContextBtn.setEnabled( buttonState.isEnabled );
            }
        }

        if ( isDirty )
        {
            this.repositionContextButtons();
        }
    }

        private findContextBtn( buttonType: context_button_type ): GameObject
        {
            let result: GameObject = null;

            for ( let displayObject of this._contextBtnContainer.children )
            {
                let go: GameObject = ( displayObject as any ).go as GameObject;
                if ( go.cViewContextBtn.buttonType == buttonType )
                {
                    result = go;
                    break;
                }
            }

            return result;
        }

    public setContextBtnsEnabled( isEnabled: boolean ): void
    {
        for ( let displayObject of this._contextBtnContainer.children )
        {
            let go: GameObject = ( displayObject as any ).go as GameObject;
            go.cViewContextBtn.setEnabled( isEnabled );
        }
    }

    public findItems(): Array<GameObject>
    {
        let result: Array<GameObject> = new Array<GameObject>();

        for ( let displayObject of this._itemHolder.cContainer.c.children )
        {
            result.push( displayObject[ "go" ] );
        }

        return result;
    }

    public enableCloseBtn(): void
    {
        let closeBtnContainer: PIXI.Container = this._viewBtnContainer.getChildByName( view_button_type.CLOSE.toString() ) as PIXI.Container;
        closeBtnContainer.alpha = 1;
        closeBtnContainer.interactive = true;
        closeBtnContainer.buttonMode = true;
    }

    public disableCloseBtn(): void
    {
        let closeBtnContainer: PIXI.Container = this._viewBtnContainer.getChildByName( view_button_type.CLOSE.toString() ) as PIXI.Container;
        closeBtnContainer.alpha = 0.5;
        closeBtnContainer.interactive = false;
        closeBtnContainer.buttonMode = false;
    }

    public abstract sortItems(): void;

    // private:

    private createHeader(): PIXI.Container
    {
        let title: PIXI.Container = new PIXI.Container();

        let text: PIXI.Text = new PIXI.Text( this._title, ServiceLocator.game.textStyler.normal );
        text.position.set( 10, 5 );
        let titleEndX: number = text.x + text.width;
        let icon: PIXI.Sprite = null;
        if ( this._headerIconId )
        {
            icon = PIXI.Sprite.from( ServiceLocator.resourceStack.find( this._headerIconId ).data );
            Utils.game.limitSideSize( text.height, icon );
            icon.position.set( text.x + text.width + 10, text.y );
            titleEndX += 10 + icon.width;
        }

        this._headerBg = new GameObject( [ new CGraphics(), new CButton() ] );
        this._headerBg.cGraphics.g.lineStyle( 1, 0x000000, 1 );
        this._headerBg.cGraphics.g.beginFill( 0xffa502 );
        this._headerBg.cGraphics.g.lineTo( titleEndX + 5, 0 );
        this._headerBg.cGraphics.g.lineTo( titleEndX + 50, text.height + 10 );
        this._headerBg.cGraphics.g.lineTo( 0, text.height + 10 );
        this._headerBg.cGraphics.g.lineTo( 0, 0 );
        this._headerBg.cGraphics.g.endFill();
        this._headerBg.init();
        this._headerBg.cButton.onClick.add( this.onHeaderBg_Click, this );

        title.addChild( this._headerBg.cGraphics.c );
        title.addChild( text ); 
        if ( icon )
        {
            title.addChild( icon ); 
        }

        return title;
    }

    private createBody(): PIXI.Container
    {
        let body: PIXI.Container = new PIXI.Container();

        this._bodyBg = new PIXI.Graphics();
        body.addChild( this._bodyBg );

        this._itemHolder = new GameObject( [ new CContainer() ] );
        this._itemHolder.init();
        this._itemHolder.cContainer.c.position.set( 10, 10 );
        body.addChild( this._itemHolder.cContainer.c );

        return body;
    }

    private createViewButtons(): PIXI.Container
    {
        let buttonContainer: PIXI.Container = new PIXI.Container();

        const kArrIconId: Array<string> = [ "close", "minimize" ];
        const kArrButtonType: Array<view_button_type> =  [ view_button_type.CLOSE, view_button_type.MINIMIZE ];
        let buttonIndex: number = 0;
        for ( let buttonType of kArrButtonType )
        {
            let buttonAux: GameObject = new GameObject( [ new CGraphics(), new CButton(), new CTooltipReceptor() ] );
            buttonAux.cGraphics.g.name = buttonType.toString();
            buttonAux.cGraphics.g.lineStyle( 2, 0x000000, 1 );
            buttonAux.cGraphics.g.beginFill( 0xEFE3AF );
            buttonAux.cGraphics.g.drawRect( 0, 0, 40, 40 );
            buttonAux.cGraphics.g.endFill();
            let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( kArrIconId[ buttonIndex ] ).data ) );
            Utils.game.limitSideSize( 20, icon );
            icon.anchor.set( 0.5, 0.5 );
            icon.position.set( 20, 20 );
            buttonAux.cGraphics.g.addChild( icon );
            let sb: SignalBinding = buttonAux.cButton.onClick.add( this.onViewBtn_Click, this );
            sb.params = [ buttonType ];
            const kArrTooltip: Array<string> = this.findViewButtonTooltip( buttonType );
            buttonAux.cTooltipReceptor.text = kArrTooltip[ 0 ];
            buttonAux.cTooltipReceptor.description = kArrTooltip[ 1 ];
            buttonAux.init();
            buttonAux.cGraphics.g.position.set( -( ( buttonIndex + 1 ) * 40 + buttonIndex * 10 + 3 ), 0 );
            buttonContainer.addChild( buttonAux.cGraphics.g );

            ++buttonIndex;
        }
        
        return buttonContainer;
    }

    private findViewButtonTooltip( optionType: view_button_type ): Array<string>
    {
        let result: Array<string> = [ null, null ];

        switch ( optionType )
        {
            case view_button_type.MINIMIZE: { result[ 0 ] = jQuery.i18n( "MINIMIZE" ); break; }
            case view_button_type.CLOSE: { result[ 0 ] = jQuery.i18n( "CLOSE" ); break; }
        }

        return result;
    }

    private createContextButtons(): PIXI.Container
    {
        let buttonContainer: PIXI.Container = new PIXI.Container();

        for ( let buttonType of this._arrContextBtnType )
        {
            let buttonAux: GameObject = new GameObject( [ new CContainer(), new CViewContextBtn(), new CTooltipReceptor() ] );
            buttonAux.cViewContextBtn.buttonType = buttonType;
            buttonAux.cViewContextBtn.isCounter = buttonType == context_button_type.DRAW_X_PLAYER_CARDS
                || buttonType == context_button_type.REVEAL_X
                || buttonType == context_button_type.DISCARD_X;
            buttonAux.cViewContextBtn.isAllBtn = buttonType == context_button_type.DRAW_X_PLAYER_CARDS
                || buttonType == context_button_type.REVEAL_X
                || buttonType == context_button_type.DISCARD_X
                || buttonType == context_button_type.RANDOMLY_DISCARD;
            const kArrTooltip: Array<string> = this.findContextButtonTooltip( buttonType );
            buttonAux.cTooltipReceptor.text = kArrTooltip[ 0 ];
            buttonAux.cTooltipReceptor.description = kArrTooltip[ 1 ];
            buttonAux.init();
            buttonAux.cViewContextBtn.onClick.add( this.onContextBtn_Click, this );
            //
            buttonAux.cContainer.c.position.set( 0, buttonContainer.children.length * 60 );
            buttonContainer.addChild( buttonAux.cContainer.c );
        }
        
        return buttonContainer;
    }

        private findContextButtonTooltip( optionType: context_button_type ): Array<string>
        {
            let result: Array<string> = [ null, null ];

            switch ( optionType )
            {
                case context_button_type.SHUFFLE: { result[ 0 ] = jQuery.i18n( "SHUFFLE" ); result[ 1 ] = jQuery.i18n( "SHUFFLE_DESC" ); break; }
                case context_button_type.RANDOMLY_DISCARD: { result[ 0 ] = jQuery.i18n( "RANDOMLY_DISCARD" ); break; }
                case context_button_type.DISCARD_X: { result[ 0 ] = jQuery.i18n( "DISCARD_X" ); break; }
                case context_button_type.REMOVE_ALL: { result[ 0 ] = jQuery.i18n( "REMOVE_ALL" ); break; }
                case context_button_type.REVEAL_X: { result[ 0 ] = jQuery.i18n( "REVEAL_X" ); break; }
                case context_button_type.DRAW_X_PLAYER_CARDS: { result[ 0 ] = jQuery.i18n( "DRAW_X_PLAYER_CARDS" ); break; }
                case context_button_type.MULLIGAN: { result[ 0 ] = jQuery.i18n( "MULLIGAN" ); result[ 1 ] = jQuery.i18n( "MULLIGAN_DESC" ); break; }
                case context_button_type.RESHUFFLE: { result[ 0 ] = jQuery.i18n( "RESHUFFLE" ); result[ 1 ] = jQuery.i18n( "RESHUFFLE_DESC" ); break; }
                case context_button_type.SORT_BY_SPHERE: { result[ 0 ] = jQuery.i18n( "SORT_BY_SPHERE" ); break; }
                case context_button_type.SORT_FROM_A_TO_Z: { result[ 0 ] = jQuery.i18n( "SORT_FROM_A_TO_Z" ); break; }
                case context_button_type.SWITCH: { result[ 0 ] = jQuery.i18n( "SWITCH" ); break; }
            }

            return result;
        }

    private repositionContextButtons(): void
    {
        let buttonCounter: number = 0;
        for ( let displayObject of this._contextBtnContainer.children )
        {
            displayObject.y = buttonCounter * 60;

            if ( displayObject.visible )
            {
                buttonCounter += 1;
            }
        }
    }

    // protected:

    // virtual.
    protected drawBg(): void
    {
        this._bodyBg.clear();
        this._bodyBg.lineStyle( 2, 0x000000, 1 );
        this._bodyBg.beginFill( 0xEFE3AF, 0.7 );
        const kBgWidth: number = this.calcBgWidth();
        const kBgHeight: number = this.calcBgHeight();
        this._bodyBg.drawRect( 0, 0, kBgWidth, kBgHeight );
        this._bodyBg.endFill();
    }

        private calcBgWidth(): number
        {
            let bgWidth: number = null;

            if ( this._width == null )
            {
                bgWidth = this._itemHolder.cContainer.c.width + 20;
                const kBgMinWidth: number = this._headerBg.cGraphics.g.width + this._viewBtnContainer.width + 20;
                if ( bgWidth < kBgMinWidth )
                {
                    bgWidth = kBgMinWidth;
                }
            }
            else
            {
                bgWidth = this._width;
            }

            return bgWidth;
        }

        private calcBgHeight(): number 
        {
            let bgHeight: number = null;

            if ( this._height == null )
            {
                bgHeight = this._itemHolder.cContainer.c.height + 20;
            }
            else
            {
                bgHeight = this._height;
            }

            return bgHeight;
        }

    // #endregion //


    // #region IGameObjectDropArea //

    public abstract validateDroppedGameObject( token: GameObject ): boolean;
    public abstract processDroppedGameObject( card: GameObject, global: PIXI.IPoint, actionScopeType: action_scope_type ): void

    // #endregion //


    // #region Input Callbacks //

    private onPointerMove( event: PIXI.InteractionEvent ): void
    {
        if ( ServiceLocator.game.dragShadowManager.dragShadow
            && ServiceLocator.game.dragShadowManager.dragShadow.cDragShadow.from != null
            && ServiceLocator.game.dragShadowManager.dragShadow.cDragShadow.from.cContainer.c.parent == this._itemHolder.cContainer.c
            && !this._go.cContainer.c.getBounds().contains( event.data.global.x, event.data.global.y ) )
        {
            this._onItemDraggedOut.dispatch();
        }
    }

    private onHeaderBg_Click(): void
    {
        this._controller.onViewBtnSelected( view_button_type.MINIMIZE );
    }

    // #endregion //


    // #region Other Callbacks //

    protected abstract onViewBtn_Click( buttonType: view_button_type ): void;
    protected abstract onContextBtn_Click( buttonType: context_button_type, params: Array<any> ): void;

    // virtual.
    protected onItemsSorted(): void
    {
        this._viewBtnContainer.x = this._bodyBg.width;
        this._contextBtnContainer.position.set( this._bodyBg.width, 10 );

        this._onContentUpdated.dispatch();
    }

    // #endregion //
}

interface IContextBtnState
{
    buttonType: context_button_type;
    isVisible?: boolean;
    isEnabled?: boolean;
}