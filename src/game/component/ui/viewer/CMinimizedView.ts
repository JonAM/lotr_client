import Component from "../../Component";

import ServiceLocator from "../../../../ServiceLocator";
import Utils from "../../../../Utils";
import { view_button_type } from "../CViewer";
import * as PIXI from "pixi.js";

import CButton from "../../input/CButton";
import CGraphics from "../../pixi/CGraphics";
import ViewerController from "./ViewerController";
import SignalBinding from "../../../../lib/signals/SignalBinding";
import GameObject from "../../../GameObject";
import CTooltipReceptor from "../CTooltipReceptor";


export default class CMinimizedView extends Component
{
    // #region Attributes //

    // private:

    private _title: string = null;
    private _bg: GameObject = null;
    private _headerIconId: string = null;
    private _controller: ViewerController = null;

    private _closeBtn: GameObject = null;

    // #endregion //


    // #region Properties //

    public set title( value: string ) { this._title = value; }
    public set headerIconId( value: string ) { this._headerIconId = value; }
    public set controller( value: ViewerController ) { this._controller = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CMinimizedView";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CMinimizedView.ts :: init() :: CContainer component not found." );
        console.assert( this._title != null, "CMinimizedView.ts :: init() :: this._title cannot be null." );
        console.assert( this._controller != null, "CMinimizedView.ts :: init() :: this._controller cannot be null." );

        let text: PIXI.Text = new PIXI.Text( this._title, ServiceLocator.game.textStyler.normal );
        text.position.set( 10, 5 );
        let titleWidth: number = text.width;
        let icon: PIXI.Sprite = null;
        if ( this._headerIconId )
        {
            icon = PIXI.Sprite.from( ServiceLocator.resourceStack.find( this._headerIconId ).data );
            Utils.game.limitSideSize( text.height, icon );
            icon.position.set( text.x + text.width + 10, text.y );
            titleWidth += 10 + icon.width;
        }

        let viewBtnContainer: PIXI.Container = this.createViewButtons();
        viewBtnContainer.x = titleWidth + 40;
        
        this._bg = new GameObject( [ new CGraphics(), new CButton() ] );
        this._bg.cGraphics.g.lineStyle( 1, 0x000000, 1 );
        this._bg.cGraphics.g.beginFill( 0xffa502 );
        this._bg.cGraphics.g.drawRect( 0, 0, viewBtnContainer.x + viewBtnContainer.width + 10, text.height + 10 );
        this._bg.cGraphics.g.endFill();
        this._bg.init();
        //
        this._bg.cButton.onClick.add( this.onBg_Click, this );

        this._go.cContainer.addChild( this._bg );
        this._go.cContainer.c.addChild( text); 
        if ( icon )
        {
            this._go.cContainer.c.addChild( icon ); 
        }
        this._go.cContainer.c.addChild( viewBtnContainer );
    }

    public end(): void
    {
        this._bg.end();
        this._bg = null;

        this._controller = null;
        this._title = null;
        this._headerIconId = null;
    }

    public enableCloseBtn(): void
    {
        this._closeBtn.cContainer.c.alpha = 1;
        this._closeBtn.cContainer.c.interactive = true;
        this._closeBtn.cContainer.c.buttonMode = true;
    }

    public disableCloseBtn(): void
    {
        this._closeBtn.cContainer.c.alpha = 0.5;
        this._closeBtn.cContainer.c.interactive = false;
        this._closeBtn.cContainer.c.buttonMode = false;
    }

    // private:

    private createViewButtons(): PIXI.Container
    {
        let buttonContainer: PIXI.Container = new PIXI.Container();

        new PIXI.Container();
        const kArrIconId: Array<string> = [ "maximize", "close" ];
        const kArrButtonType: Array<view_button_type> = [ view_button_type.MAXIMIZE, view_button_type.CLOSE ];
        let buttonIndex: number = 0;
        for ( let buttonType of kArrButtonType )
        {
            let buttonAux: GameObject = new GameObject( [ new CGraphics(), new CButton(), new CTooltipReceptor() ] );
            buttonAux.cGraphics.g.name = buttonType.toString();
            buttonAux.cGraphics.g.lineStyle( 2, 0x000000, 1 );
            buttonAux.cGraphics.g.beginFill( 0xEFE3AF );
            buttonAux.cGraphics.g.drawRect( 0, 0, 40, 40 );
            buttonAux.cGraphics.g.endFill();
            let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( kArrIconId[ buttonIndex ] ).data ) );
            Utils.game.limitSideSize( 20, icon );
            icon.anchor.set( 0.5, 0.5 );
            icon.position.set( 20, 20 );
            buttonAux.cGraphics.g.addChild( icon );
            let sb: SignalBinding = buttonAux.cButton.onClick.add( this.onViewBtn_Click, this );
            sb.params = [ buttonType ];
            const kArrTooltip: Array<string> = this.findButtonTooltip( buttonType );
            buttonAux.cTooltipReceptor.text = kArrTooltip[ 0 ];
            buttonAux.cTooltipReceptor.description = kArrTooltip[ 1 ];
            buttonAux.init();
            buttonAux.cGraphics.g.x = 50 * buttonIndex;
            buttonContainer.addChild( buttonAux.cGraphics.g );

            if ( buttonType == view_button_type.CLOSE )
            {
                this._closeBtn = buttonAux;
            }

            ++buttonIndex;
        }
        
        return buttonContainer;
    }

    private findButtonTooltip( optionType: view_button_type ): Array<string>
    {
        let result: Array<string> = [ null, null ];

        switch ( optionType )
        {
            case view_button_type.MAXIMIZE: { result[ 0 ] = jQuery.i18n( "MAXIMIZE" ); break; }
            case view_button_type.CLOSE: { result[ 0 ] = jQuery.i18n( "CLOSE" ); break; }
        }

        return result;
    }

    // #endregion //


    // #region Callbacks //

    private onBg_Click(): void
    {
        this._controller.onViewBtnSelected( view_button_type.MAXIMIZE );
    }

    private onViewBtn_Click( buttonType: view_button_type ): void
    {
        this._controller.onViewBtnSelected( buttonType ); 
    }

    // #endregion //
}