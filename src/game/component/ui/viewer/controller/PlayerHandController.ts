import CardViewerController from "./CardViewerController";

import { ICard } from "../../../../CardDB";
import ServiceLocator from "../../../../../ServiceLocator";

import GameObject from "../../../../GameObject";


export default class PlayerHandController extends CardViewerController
{
    // #region Methods //

    // public:

    public init(): void
    {
        super.init();

        // Listen to events.
        ServiceLocator.game.keyBindingManager.onKeyDown.add( this.onBindedKey_Down, this );
    }

    public end(): void
    {
        ServiceLocator.game.keyBindingManager.onKeyDown.remove( this.onBindedKey_Down, this );
   
        super.end();
    }
    
    // virtual.
    public validateDroppedGameObject( go: GameObject ): boolean
    {
        let result: boolean = super.validateDroppedGameObject( go );
        if ( result )
        {
            let cardInfo: ICard = null;
            if ( go.cCard )
            { 
                cardInfo = go.cCard.back.cardInfo;
            }
            else if ( go.cCardToken )
            {
                cardInfo = go.cCardToken.backSide.cCardTokenSide.cardInfo;
            } 
            else if ( go.cAttachment )
            {
                cardInfo = go.cAttachment.back.cardInfo;
            }
            result = cardInfo.type_code == "player-back";
        }

        return result; 
    }

    // #endregion //


    // #region Callbacks //

    // protected:

    // virtual.
    protected onCard_Dragged( card: GameObject ): void
    {
        super.onCard_Dragged( card );

        ServiceLocator.game.cardActivationArea.cCardActivationArea.show();
    }

    // virtual.
    protected onCard_DragCancelled( card: GameObject ): void
    {
        super.onCard_DragCancelled( card );

        ServiceLocator.game.cardActivationArea.cCardActivationArea.hide();
    }

    // virtual.
    protected onCard_PostDropped( card: GameObject, hit: GameObject ): void
    {
        super.onCard_PostDropped( card, hit );

        ServiceLocator.game.cardActivationArea.cCardActivationArea.hide();
    }

    // private:

    private onBindedKey_Down( bindingId: string ): void
    {
        if ( bindingId == "hand" )
        {
            this._viewer.toggle();

            ServiceLocator.game.tooltipManager.validateCurrentTooltipReceptor( this._viewer.go.cContainer.c.getBounds() );
        }
    }

    // #endregion //
}