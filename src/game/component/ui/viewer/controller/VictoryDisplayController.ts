import CardViewerController from "./CardViewerController";

import GameObject from "../../../../GameObject";
//import { ICard } from "../../../../CardDB";
import ServiceLocator from "../../../../../ServiceLocator";


export default class VictoryDisplayController extends CardViewerController
{
    // #region Methods //

    // public:
    
    // virtual.
    /*public validateDroppedGameObject( go: GameObject ): boolean
    {
        let result: boolean = super.validateDroppedGameObject( go );
        if ( result )
        {
            let cardInfo: ICard = null;
            if ( go.cCard )
            { 
                cardInfo = go.cCard.curSide.cardInfo;
            }
            else if ( go.cCardToken )
            {
                cardInfo = go.cCardToken.cCurSide.cardInfo;
            } 
            else if ( go.cAttachment )
            {
                cardInfo = go.cAttachment.curSide.cardInfo;
            }
            result = cardInfo.victory != undefined;
        }

        return result; 
    }*/

    // #endregion //


    // #region Callbacks //

    // protected:

    // virtual.
    protected onCard_Dragged( card: GameObject ): void
    {
        super.onCard_Dragged( card );

        ServiceLocator.game.cardActivationArea.cCardActivationArea.show();
    }

    // virtual.
    protected onCard_DragCancelled( card: GameObject ): void
    {
        super.onCard_DragCancelled( card );

        ServiceLocator.game.cardActivationArea.cCardActivationArea.hide();
    }

    // virtual.
    protected onCard_PostDropped( card: GameObject, hit: GameObject ): void
    {
        super.onCard_PostDropped( card, hit );

        ServiceLocator.game.cardActivationArea.cCardActivationArea.hide();
    }

    // #endregion //
}