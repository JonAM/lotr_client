import GameObject from "../../../../GameObject";
import CardViewerController from "./CardViewerController";
import { ICard } from "../../../../CardDB";
import ServiceLocator from "../../../../../ServiceLocator";


export default class SauronDeckController extends CardViewerController
{
    // #region Methods //

    // public:
    
    // virtual.
    public validateDroppedGameObject( go: GameObject ): boolean
    {
        let result: boolean = super.validateDroppedGameObject( go );
        if ( result )
        {
            let backSideCode: string = null;
            if ( go.cCard )
            { 
                backSideCode = go.cCard.back.cardId;
            }
            else if ( go.cCardToken )
            {
                backSideCode = go.cCardToken.cBackSide.cardId;
            } 
            else if ( go.cAttachment )
            {
                backSideCode = go.cAttachment.back.cardId;
            }
            else if ( go.cShadowCardMini )
            {
                backSideCode = go.cShadowCardMini.card.cCard.back.cardId;
            }
            result = backSideCode == "999003";
        }

        return result; 
    }

    // #endregion //


    // #region Callbacks //

    // protected:

    // virtual.
    protected onCard_Dragged( card: GameObject ): void
    {
        super.onCard_Dragged( card );

        ServiceLocator.game.cardActivationArea.cCardActivationArea.show();
    }

    // virtual.
    protected onCard_DragCancelled( card: GameObject ): void
    {
        super.onCard_DragCancelled( card );

        ServiceLocator.game.cardActivationArea.cCardActivationArea.hide();
    }

    // virtual.
    protected onCard_PostDropped( card: GameObject, hit: GameObject ): void
    {
        super.onCard_PostDropped( card, hit );

        ServiceLocator.game.cardActivationArea.cCardActivationArea.hide();
    }

    // #endregion //
}