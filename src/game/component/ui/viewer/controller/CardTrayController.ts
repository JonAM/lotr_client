import ViewerController from "../ViewerController";

import { player_action_type, action_scope_type } from "../../../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../../../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "../../../../GameObject";
import CardFactory from "../../../../CardFactory";
import CDropArea from "../../../input/CDropArea";
import { player_type } from "../../../world/CPlayerArea";


export default class CardTrayController extends ViewerController
{
    // #region Methods //

    // public:

    public validateDroppedGameObject( go: GameObject ): boolean
    {
        return go.cCard != null 
            || go.cCardToken != null && go.cCardToken.cFrontSide.cardInfo.pack_code != "Custom" 
            || go.cAttachment != null
            || go.cShadowCardMini != null
            || go.cGameModifier != null; 
    }

    public processDroppedGameObject( dropped: GameObject, global: PIXI.IPoint, actionScopeType: action_scope_type ): void
    {
        let card: GameObject = this.createDroppedCard( dropped, actionScopeType );
        card.cCard.setEnabled( true );
        card.cShareableGameElement.setEnabled( true );
        let isNewCard: boolean = card.cCard.location != this._viewer.location;
        card.cCard.location = this._viewer.location;
        this._itemHolder.cContainer.c.addChild( card.cContainer.c );
        //
        card.cDraggable.onDragCancelled.add( this.onCard_DragCancelled, this );
        // Avoid receiving this event on the same frame the card was dropped.
        window.setTimeout( () => { card.cDraggable.onPostDropped.add( this.onCard_PostDropped, this ); } );

        if ( isNewCard )
        {
            card.cContainer.c.position.set( 0 );
        }
        else
        {
            card.cContainer.c.position.copyFrom( this._viewer.view.body.toLocal( global ) );
            if ( card.cContainer.c.x > this._viewer.view.width - card.cContainer.c.width )
            {
                card.cContainer.c.x = this._viewer.view.width - card.cContainer.c.width - 20; 
            }
            if ( card.cContainer.c.y > this._viewer.view.height - card.cContainer.c.height )
            {
                card.cContainer.c.y = this._viewer.view.height - card.cContainer.c.height - 20; 
            }
        }
        
        if ( ServiceLocator.game.isAnimated && card.cCard.isAnimated )
        {
            card.cCard.bump();
        }
        this._viewer.cCardView.onItemCountUpdated.dispatch( this._itemHolder.cContainer.c.children.length, !isNewCard ? 0 : 1 );

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.DROP_AT_DROP_AREA, null, [ dropped.oid, this._viewer.cCardView.go.oid, global.x != CDropArea.kPredefinedDropPositionCode ? this._viewer.cCardView.go.cContainer.c.toLocal( global ) : global ] );
        }

        if ( !dropped.cCard )
        {
            dropped.end();
        }
    }

        private createDroppedCard( dropped: GameObject, actionScope: action_scope_type ): GameObject
        {
            let card: GameObject = null;
            if ( dropped.cCardToken )
            {
                let cardFactory = new CardFactory();
                card = cardFactory.createFromCardToken( dropped );  
                if ( dropped.cCardToken.isFaceUp )
                {
                    card.cCard.setFaceUp( action_scope_type.LOCAL );
                }  

                // If it has cards attached, move them to their corresponding discard piles.
                let arrAttachment: Array<GameObject> = dropped.cCardToken.attachmentHolder.cScrollY.findItems();
                for ( let attachment of arrAttachment )
                {
                    let card: GameObject = cardFactory.createFromAttachment( attachment );
                    card.cCard.discard( actionScope );
                }
                dropped.cCardToken.discardUnderneath( actionScope );
                if ( dropped.cCardToken.frontSide.cEnemySide )
                {
                    dropped.cCardToken.frontSide.cEnemySide.discardShadowCards( action_scope_type.LOCAL );
                }
                if ( dropped.cCardToken.backSide.cEnemySide )
                {
                    dropped.cCardToken.backSide.cEnemySide.discardShadowCards( action_scope_type.LOCAL );
                }
                ServiceLocator.game.attackBindingManager.removeAllBindings( dropped, actionScope );
            }
            else if ( dropped.cAttachment )
            {
                let cardFactory: CardFactory = new CardFactory();
                card = cardFactory.createFromAttachment( dropped );
                if ( dropped.cAttachment.isFaceUp )
                {
                    card.cCard.setFaceUp( action_scope_type.LOCAL );
                }
            }
            else if ( dropped.cShadowCardMini )
            {
                card = dropped.cShadowCardMini.card;
                dropped.cShadowCardMini.onDiscarded.dispatch( dropped );
            }
            else if ( dropped.cGameModifier )
            {
                let cardFactory: CardFactory = new CardFactory();
                card = cardFactory.createFromGameModifier( dropped );
                dropped.cGameModifier.discardUnderneath( action_scope_type.LOCAL );
            }
            else
            {
                card = dropped;
            }

            return card;
        }

    // #endregion //

    
    // #region Other Callbacks //

    // protected:

    // virtual.
    protected onCard_DragCancelled( card: GameObject ): void
    {
        this._onItemDragCanceled.dispatch();
    }

    // virtual.
    protected onCard_PostDropped( dropped: GameObject, dropArea: GameObject ): void
    {
        if ( dropArea != this._viewer.cCardView.go )
        {
            if ( dropped.cCard.ownerPlayer != player_type.SAURON )
            {
                dropped.cShareableGameElement.setEnabled( false );
            }

            if ( this._itemHolder.cContainer.c.children.length == 0 )
            {
                this._viewer.hide();
            }

            this._viewer.cCardView.onItemCountUpdated.dispatch( this._itemHolder.cContainer.c.children.length, -1 );
        }

        this._onItemDropped.dispatch( dropped, dropArea );
    }

    // #endregion //
}