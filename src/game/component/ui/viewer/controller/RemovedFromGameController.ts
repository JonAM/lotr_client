import GameObject from "../../../../GameObject";
import CardViewerController from "./CardViewerController";
import { player_type } from "../../../world/CPlayerArea";
import { action_scope_type } from "../../../../../service/socket_io/GameSocketIOController";
import { ICard } from "../../../../CardDB";
import ServiceLocator from "../../../../../ServiceLocator";


export default class RemovedFromGameController extends CardViewerController
{
    // #region Callbacks //

    // protected:

    // virtual.
    protected onCard_Dragged( card: GameObject ): void
    {
        super.onCard_Dragged( card );

        ServiceLocator.game.cardActivationArea.cCardActivationArea.show();
    }

    // virtual.
    protected onCard_DragCancelled( card: GameObject ): void
    {
        super.onCard_DragCancelled( card );

        ServiceLocator.game.cardActivationArea.cCardActivationArea.hide();
    }

    // virtual.
    protected onCard_PostDropped( card: GameObject, hit: GameObject ): void
    {
        super.onCard_PostDropped( card, hit );

        ServiceLocator.game.cardActivationArea.cCardActivationArea.hide();
    }

    // #endregion //
}