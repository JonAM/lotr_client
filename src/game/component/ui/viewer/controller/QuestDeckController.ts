import GameObject from "../../../../GameObject";
import CardViewerController from "./CardViewerController";
import { ICard } from "../../../../CardDB";
import ServiceLocator from "../../../../../ServiceLocator";


export default class QuestDeckController extends CardViewerController
{
    // #region Methods //

    // public:
    
    // virtual.
    public validateDroppedGameObject( go: GameObject ): boolean
    {
        let result: boolean = super.validateDroppedGameObject( go );
        if ( result )
        {
            let frontSideTypeCode: string = null;
            if ( go.cCard )
            { 
                frontSideTypeCode = go.cCard.front.cardInfo.type_code;
            }
            else if ( go.cCardToken )
            {
                frontSideTypeCode = go.cCardToken.cFrontSide.cardInfo.type_code;
            } 
            else if ( go.cAttachment )
            {
                frontSideTypeCode = go.cAttachment.front.cardInfo.type_code;
            }
            result = frontSideTypeCode == "quest";
        }

        return result; 
    }

    // #endregion //


    // #region Callbacks //

    // protected:

    // virtual.
    protected onCard_Dragged( card: GameObject ): void
    {
        super.onCard_Dragged( card );

        ServiceLocator.game.cardActivationArea.cCardActivationArea.show();
    }

    // virtual.
    protected onCard_DragCancelled( card: GameObject ): void
    {
        super.onCard_DragCancelled( card );

        ServiceLocator.game.cardActivationArea.cCardActivationArea.hide();
    }

    // virtual.
    protected onCard_PostDropped( card: GameObject, hit: GameObject ): void
    {
        super.onCard_PostDropped( card, hit );

        ServiceLocator.game.cardActivationArea.cCardActivationArea.hide();
    }

    // #endregion //
}