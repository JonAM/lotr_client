import ViewerController from "../ViewerController";

import CCardView, { deck_side_type, face_policy_type } from "../CCardView";
import { location_type } from "../../../world/CGameWorld";
import { player_action_type, IRemoteCard, action_scope_type } from "../../../../../service/socket_io/GameSocketIOController";
import { player_type } from "../../../world/CPlayerArea";
import ServiceLocator from "../../../../../ServiceLocator";
import Utils from "../../../../../Utils";
import { view_layer_id } from "../../../../../service/ViewManager";
import * as PIXI from "pixi.js";

import GameObject from "../../../../GameObject";
import CardFactory from "../../../../CardFactory";
import LogTargetCard from "../../right_menu/action_log/target/LogTargetCard";
import LogTargetDeck, { deck_type } from "../../right_menu/action_log/target/LogTargetDeck";
import CDropArea from "../../../input/CDropArea";
import CViewer from "../../CViewer";
import { option_type } from "../../CRadialMenu";
import Session from "../../../../../Session";
import { layer_type } from "../../../world/CGameLayerProvider";
import { context_button_type } from "../CViewContextBtn";
import PointerInputController from "../../../../input/PointerInputController";
import TouchInputController from "../../../../input/TouchInputController";
import { input_event_type } from "../../../../InputController";


export default class CardViewerController extends ViewerController
{
    // #region Attributes //

    // private:

    private _curCard: GameObject = null;
    private _curCardState: ICurrentCardState = null;
    private _interactionType: interaction_type = null;

    // Constants.
    private readonly _kCurCardHeightDiff: number = 30;

    // #endregion //


    // #region Methods //

    // public:

    public end(): void
    {
        if ( ServiceLocator.game.inputController instanceof PointerInputController )
        {
            ServiceLocator.game.app.stage.off( "pointermove", this.onStage_PointerMove, this );
        }
        ServiceLocator.game.inputController.off( ServiceLocator.game.app.stage, input_event_type.UP, this.onStage_PointerUp, this );
        ServiceLocator.game.inputController.offChildren( this._itemHolder.cContainer.c, input_event_type.TAP, this.onCard_Tap, this );

        super.end();
    }

    // virtual.
    public onViewShown(): void 
    {
        this._interactionType = interaction_type.PREVIEW;

        if ( ServiceLocator.game.inputController instanceof PointerInputController )
        {
            ServiceLocator.game.app.stage.on( "pointermove", this.onStage_PointerMove, this );
        }
        ServiceLocator.game.inputController.on( ServiceLocator.game.app.stage, input_event_type.UP, this.onStage_PointerUp, this );
        ServiceLocator.game.inputController.onChildren( this._itemHolder.cContainer.c, input_event_type.TAP, this.onCard_Tap, this );
    }

    // virtual.
    public onViewHidden(): void 
    {
        // When a hotkey is used to hide the hand but a card was selected.
        if ( ServiceLocator.game.radialMenu.cContainer.c.visible && ServiceLocator.game.cRadialMenu.target.cCard )
        {
            ServiceLocator.game.cRadialMenu.hide();
        }

        if ( this._itemHolder.cContainer.c.children.length > 0 )
        {
           this.clearCurCardState();
        }

        if ( ServiceLocator.game.inputController instanceof PointerInputController )
        {
            ServiceLocator.game.app.stage.off( "pointermove", this.onStage_PointerMove, this );
        }
        ServiceLocator.game.inputController.off( ServiceLocator.game.app.stage, input_event_type.UP, this.onStage_PointerUp, this );
        ServiceLocator.game.inputController.offChildren( this._itemHolder.cContainer.c, input_event_type.TAP, this.onCard_Tap, this );

        //ServiceLocator.game.cRadialMenu.onHidden.remove( this.onRadialMenu_Hidden, this );
        ServiceLocator.game.cRadialMenu.onOptionSelected.remove( this.onRadialMenuOption_Selected, this );

        super.onViewHidden();
    }

    public setCards( arrRemoteCardId: Array<IRemoteCard> ): void
    {
        for ( let remoteCardId of arrRemoteCardId )
        {
            let cardFactory: CardFactory = new CardFactory();
            let card: GameObject = cardFactory.create( {
                oid: remoteCardId.oid,
                frontCardId: remoteCardId.cid,
                backCardId: ServiceLocator.cardDb.find( remoteCardId.cid ).side,
                ownerPlayer: this._viewer.cCardView.playerType,
                controllerPlayer: this._viewer.cCardView.playerType == player_type.SAURON ? player_type.PLAYER : this._viewer.cCardView.playerType,
                location: this._viewer.location,
                isFaceUp: remoteCardId.isFaceUp } );
            card.cDraggable.onDragged.add( this.onCard_Dragged, this );
            card.cDraggable.onDragCancelled.add( this.onCard_DragCancelled, this );
            card.cDraggable.onPostDropped.add( this.onCard_PostDropped, this );

            let isPlayer: boolean = this._viewer.cCardView.playerType == player_type.PLAYER || this._viewer.cCardView.playerType == player_type.SAURON;
            card.cContainer.c.buttonMode = isPlayer;
            card.cCard.isAnimated = false;
            if ( this._viewer.cCardView.facePolicy == face_policy_type.FACE_UP )
            {
                card.cCard.setFaceUp( action_scope_type.LOCAL );
            }
            else if ( this._viewer.cCardView.facePolicy == face_policy_type.FACE_DOWN )
            {
                card.cCard.setFaceDown( action_scope_type.LOCAL );
            }
            card.cCard.isAnimated = true;
            card.cCard.setEnabled( isPlayer );

            this._itemHolder.cContainer.c.addChildAt( card.cContainer.c, 0 );
        }
        this.clearCurCardState();
        if ( arrRemoteCardId.length > 0 )
        {
            this._viewer.cCardView.sortItems();
        }

        this._viewer.cCardView.onItemCountUpdated.dispatch( this._itemHolder.cContainer.c.children.length, 0 );
    }

    public drawCard(): GameObject
    {
        let result: GameObject = null;

        let cardCount: number = this._itemHolder.cContainer.c.children.length;
        if ( cardCount > 0 )
        {
            result = this._itemHolder.cContainer.c.getChildAt( this._itemHolder.cContainer.c.children.length - 1 )[ "go" ];
            ServiceLocator.game.root.cGameLayerProvider.get( layer_type.UNDER_TABLE ).cDropArea.forceDrop( result, null, action_scope_type.MULTIPLAYER );
        }

        return result;
    }

    public shuffle( actionScopeType: action_scope_type ): void
    {
        Utils.game.shuffle( this._itemHolder.cContainer.c.children );
        this.clearCurCardState();
        this._viewer.cCardView.sortItems();

        if ( this._viewer.cCardView.facePolicy == face_policy_type.FACE_DOWN )
        {
            for ( let displayObject of this._itemHolder.cContainer.c.children )
            {
                displayObject[ "go" ].cCard.setFaceDown( action_scope_type.LOCAL );
            }
        }

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SHUFFLE_DECK, null, [ this._viewer.location ] );
        }
    }

    public revealCards( count: number, actionScopeType: action_scope_type ): void
    {
        let arrCard: Array<PIXI.DisplayObject> = this._itemHolder.cContainer.c.children;

        let cardIndex: number = arrCard.length - 1;
        let revealedCount: number = 0;
        while ( cardIndex >= 0 && revealedCount < count )
        {
            let card: GameObject = arrCard[ cardIndex ][ "go" ] as GameObject;
            card.cCard.isAnimated = false;
            card.cCard.setFaceUp( action_scope_type.LOCAL );
            card.cCard.isAnimated = true;

            --cardIndex;
            ++revealedCount;
        }

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.REVEAL_X, null, [ this._viewer.location, count ] );
        }
    }

    public drawPlayerCards( count: number, actionScopeType: action_scope_type ): void
    {
        let playerDeckIndicator: GameObject = this._viewer.location == location_type.MY_HAND ? ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.deck : ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.deck; 
        let arrPlayerDeckCard: Array<GameObject> = playerDeckIndicator.cDeckIndicator.cViewer.cCardView.findItems();

        let cardIndex: number = arrPlayerDeckCard.length - 1;
        let drawnCount: number = 0;
        while ( cardIndex >= 0 && drawnCount < count )
        {
            let card: GameObject = arrPlayerDeckCard[ cardIndex ];
            card.cCard.isAnimated = false;
            this._viewer.cCardView.go.cDropArea.forceDrop( card, CCardView.findPredefinedDropPosition( deck_side_type.TOP ), action_scope_type.LOCAL );
            card.cCard.isAnimated = true;

            --cardIndex;
            ++drawnCount;
        }

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.DRAW_X_PLAYER_CARDS, null, [ this._viewer.location, count ] );
        }
    }

    public discardCards( count: number, actionScopeType: action_scope_type ): void
    {
        let arrCard: Array<PIXI.DisplayObject> = this._itemHolder.cContainer.c.children;
        let cardIndex: number = arrCard.length - 1;
        let discardedCount: number = 0;
        while ( cardIndex >= 0 && discardedCount < count )
        {
            let card: GameObject = arrCard[ cardIndex ][ "go" ] as GameObject;
            card.cCard.isAnimated = false;
            Utils.game.discard( card, action_scope_type.LOCAL );
            card.cCard.isAnimated = true;

            --cardIndex;
            ++discardedCount;
        }

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.DISCARD_X, null, [ this._viewer.location, count ] );
        }
    }

    public removeCards( actionScopeType: action_scope_type ): void
    {
        let targetDeckIndicator: GameObject = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.removedFromGame; 
        
        let arrCard: Array<PIXI.DisplayObject> = this._itemHolder.cContainer.c.children;
        for ( let i: number = arrCard.length - 1; i >= 0; --i )
        {
            let card: GameObject = arrCard[ i ][ "go" ] as GameObject;
            targetDeckIndicator.cDeckIndicator.cViewer.cCardView.go.cDropArea.forceDrop( card, CCardView.findPredefinedDropPosition( deck_side_type.TOP ), action_scope_type.LOCAL );
        }

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.REMOVE_ALL_CARDS, null, [ this._viewer.location ] );
        }
    }

    public switchDecks( actionScopeType: action_scope_type ): void
    {
        let targetDeckIndicator: GameObject = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.deck; 
        let arrTargetDeckCard: Array<GameObject> = targetDeckIndicator.cDeckIndicator.cViewer.cCardView.findItems();

        let arrCard: Array<PIXI.DisplayObject> = this._itemHolder.cContainer.c.children;
        for ( let i: number = arrCard.length - 1; i >= 0; --i )
        {
            let card: GameObject = arrCard[ i ][ "go" ] as GameObject;
            targetDeckIndicator.cDeckIndicator.cViewer.cCardView.go.cDropArea.forceDrop( card, CCardView.findPredefinedDropPosition( deck_side_type.TOP ), action_scope_type.LOCAL );
        }

        for ( let targetDeckCard of arrTargetDeckCard )
        {
            this._viewer.cCardView.go.cDropArea.forceDrop( targetDeckCard, CCardView.findPredefinedDropPosition( deck_side_type.TOP ), action_scope_type.LOCAL );
        }

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SWITCH_DECKS, null, [ this._viewer.location ] );
        }
    }

    public reshuffle( actionScopeType: action_scope_type ): void
    {
        let targetDeckIndicator: GameObject = null;
        switch ( this._viewer.location )
        {
            case location_type.MY_DISCARD: { targetDeckIndicator = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.deck; break; }
            case location_type.ALLY_DISCARD: { targetDeckIndicator = ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.deck; break; }
            case location_type.SAURON_DISCARD: { targetDeckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.deck; break; }
            case location_type.SAURON_CUSTOM_DISCARD_0: { targetDeckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.customDeck; break; }
        }

        // Put discarded cards back on deck.
        let arrDiscardedCard: Array<PIXI.DisplayObject> = this._itemHolder.cContainer.c.children;
        for ( let i: number = arrDiscardedCard.length - 1; i >= 0; --i )
        {
            let discardedCard: GameObject = arrDiscardedCard[ i ][ "go" ] as GameObject;
            targetDeckIndicator.cDeckIndicator.cViewer.cCardView.go.cDropArea.forceDrop( discardedCard, CCardView.findPredefinedDropPosition( deck_side_type.TOP ), action_scope_type.LOCAL );
        }

        // Shuffle.
        ( targetDeckIndicator.cDeckIndicator.cViewer.controller as CardViewerController ).shuffle( action_scope_type.LOCAL );

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.RESHUFFLE_DECK, null, [ this._viewer.location ] );
        }
    }

    public sortBySphere( actionScopeType: action_scope_type ): void
    {
        const kArrSphereCode: Array<string> = this.findSphereOrder();

        let arrCard: Array<PIXI.DisplayObject> = this._itemHolder.cContainer.c.children;
        let isSorted: boolean = null;
        do
        {
            isSorted = true;
            for ( let i: number = 0; i < arrCard.length - 1; ++i )
            {
                let left: GameObject = arrCard[ i ][ "go" ] as GameObject;
                let right: GameObject = arrCard[ i + 1 ][ "go" ] as GameObject;
                const kLeftSphereIndex: number = kArrSphereCode.indexOf( left.cCard.front.cardInfo.sphere_code );
                const kRightSphereIndex: number = kArrSphereCode.indexOf( right.cCard.front.cardInfo.sphere_code );
                if ( kLeftSphereIndex < kRightSphereIndex
                    || kLeftSphereIndex == kRightSphereIndex && left.cCard.front.cardInfo.cost < right.cCard.front.cardInfo.cost )
                {
                    arrCard[ i ] = right.cContainer.c;
                    arrCard[ i + 1 ] = left.cContainer.c;
                    
                    isSorted = false;
                }
            }
        } 
        while ( !isSorted );
        this._viewer.cCardView.sortItems();

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SORT_HAND_BY_SPHERE, null, [ this._viewer.location ] );
        }
    }

        private findSphereOrder(): Array<string>
        {
            const arrSphereCode: Array<string> = [ "lore", "spirit", "leadership", "tactics" ];
            const arrHeroSphereCount: Array<number> = [ 0, 0, 0, 0 ];
            const kArrHeroId: Array<string> = this._viewer.location == location_type.MY_HAND ? Session.playerDeck.hero : Session.allyDeck.hero;
            for ( let heroId of kArrHeroId )
            {
                const kHeroSphereCode: string = ServiceLocator.cardDb.find( heroId ).sphere_code;
                arrHeroSphereCount[ arrSphereCode.indexOf( kHeroSphereCode ) ] += 1;
            }
            let isSorted: boolean = null;
            do
            {
                isSorted = true;
                for ( let i: number = 0; i < arrHeroSphereCount.length - 1; ++i )
                {
                    let left: number = arrHeroSphereCount[ i ];
                    let right: number = arrHeroSphereCount[ i + 1 ];
                    if ( left < right )
                    {
                        arrHeroSphereCount[ i ] = right;
                        arrHeroSphereCount[ i + 1 ] = left;

                        const kTemp: string = arrSphereCode[ i + 1 ];
                        arrSphereCode[ i + 1 ] = arrSphereCode[ i ];
                        arrSphereCode[ i ] = kTemp;
                        
                        isSorted = false;
                    }
                }
            } 
            while ( !isSorted );
            arrSphereCode.push( "neutral", "none" );

            return arrSphereCode;
        }

    public sortFromAtoZ( actionScopeType: action_scope_type ): void
    {
        let arrCard: Array<PIXI.DisplayObject> = this._itemHolder.cContainer.c.children;
        let isSorted: boolean = null;
        do
        {
            isSorted = true;
            for ( let i: number = 0; i < arrCard.length - 1; ++i )
            {
                let left: GameObject = arrCard[ i ][ "go" ] as GameObject;
                let right: GameObject = arrCard[ i + 1 ][ "go" ] as GameObject;
                if ( left.cCard.front.cardInfo.name < right.cCard.front.cardInfo.name )
                {
                    arrCard[ i ] = right.cContainer.c;
                    arrCard[ i + 1 ] = left.cContainer.c;

                    isSorted = false;
                }
            }
        } 
        while ( !isSorted );
        this._viewer.cCardView.sortItems();

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SORT_DECK_FROM_A_TO_Z, null, [ this._viewer.location ] );
        }
    }

    // virtual.
    public validateDroppedGameObject( go: GameObject ): boolean
    {
        return go.cCard != null 
            || go.cCardToken != null && go.cCardToken.cFrontSide.cardInfo.pack_code != "Custom" 
            || go.cAttachment != null
            || go.cGameModifier != null
            || go.cShadowCardMini != null; 
    }

    // virtual.
    public processDroppedGameObject( dropped: GameObject, global: PIXI.IPoint, actionScopeType: action_scope_type ): void
    {
        let card: GameObject = this.createDroppedCard( dropped, actionScopeType );

        let isPlayer: boolean = this._viewer.cCardView.playerType == player_type.PLAYER || this._viewer.cCardView.playerType == player_type.SAURON;
        card.cContainer.c.buttonMode = isPlayer;
        card.cCard.setEnabled( isPlayer );
        
        // Face policy.
        if ( card.cCard.location != this._viewer.location )
        {
            if ( this._viewer.cCardView.facePolicy == face_policy_type.FACE_DOWN )
            {
                card.cCard.setFaceDown( action_scope_type.LOCAL );
            }
            else if ( this._viewer.cCardView.facePolicy == face_policy_type.FACE_UP )
            {
                card.cCard.setFaceUp( action_scope_type.LOCAL )
            }
        }
        card.cCard.location = this._viewer.location;

        this.clearCurCardState();
        this._interactionType = interaction_type.PREVIEW;

        let insertIndex: number = this.findDroppedInsertIndex( global );
        // If dragging and dropping on the same viewer, the dragged card is already in the container the moment it is dropped.
        let oldIndex: number = this._itemHolder.cContainer.c.children.indexOf( card.cContainer.c );
        if ( oldIndex != -1 && oldIndex < insertIndex )
        {
            insertIndex -= 1;
        }
        this._itemHolder.cContainer.c.addChildAt( card.cContainer.c, insertIndex );
        this._viewer.cCardView.sortItems();

        this.postProcessDroppedCard( card );
        card.cDraggable.onDragged.add( this.onCard_Dragged, this );
        card.cDraggable.onDragCancelled.add( this.onCard_DragCancelled, this );
        // Avoid receiving this event on the same frame the card was dropped.
        window.setTimeout( () => { card.cDraggable.onPostDropped.add( this.onCard_PostDropped, this ); } );
        if ( ServiceLocator.game.isAnimated && card.cCard.isAnimated )
        {
            card.cCard.bump();
        }
        this._viewer.cCardView.onItemCountUpdated.dispatch( this._itemHolder.cContainer.c.children.length, oldIndex != -1 ? 0 : 1 );

        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            this._curCard = null;

            // Multiplayer.
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.DROP_AT_DROP_AREA, null, [ dropped.oid, this._viewer.cCardView.go.oid, global.x != CDropArea.kPredefinedDropPositionCode ? this._viewer.cCardView.go.cContainer.c.toLocal( global ) : global ] );
        }

        if ( !dropped.cCard )
        {
            dropped.end();
        }
    }

        private createDroppedCard( dropped: GameObject, actionScope: action_scope_type ): GameObject
        {
            let card: GameObject = null;
            if ( dropped.cCardToken )
            {
                let cardFactory = new CardFactory();
                card = cardFactory.createFromCardToken( dropped );  
                if ( dropped.cCardToken.isFaceUp )
                {
                    card.cCard.setFaceUp( action_scope_type.LOCAL );
                }  

                // If it has cards attached, move them to their corresponding discard piles.
                let arrAttachment: Array<GameObject> = dropped.cCardToken.attachmentHolder.cScrollY.findItems();
                for ( let attachment of arrAttachment )
                {
                    let card: GameObject = cardFactory.createFromAttachment( attachment );
                    card.cCard.discard( action_scope_type.LOCAL );
                }
                dropped.cCardToken.discardUnderneath( action_scope_type.LOCAL );
                if ( dropped.cCardToken.frontSide.cEnemySide )
                {
                    dropped.cCardToken.frontSide.cEnemySide.discardShadowCards( action_scope_type.LOCAL );
                }
                else if ( dropped.cCardToken.backSide.cEnemySide )
                {
                    dropped.cCardToken.backSide.cEnemySide.discardShadowCards( action_scope_type.LOCAL );
                }
                ServiceLocator.game.attackBindingManager.removeAllBindings( dropped, action_scope_type.LOCAL );
            }
            else if ( dropped.cAttachment )
            {
                let cardFactory: CardFactory = new CardFactory();
                card = cardFactory.createFromAttachment( dropped );
                card.cCard.setFaceUp( action_scope_type.LOCAL );
            }
            else if ( dropped.cShadowCardMini )
            {
                card = dropped.cShadowCardMini.card;
                dropped.cShadowCardMini.onDiscarded.dispatch( dropped );
            }
            else if ( dropped.cGameModifier )
            {
                let cardFactory: CardFactory = new CardFactory();
                card = cardFactory.createFromGameModifier( dropped );
                dropped.cGameModifier.discardUnderneath( action_scope_type.LOCAL );
            }
            else
            {
                card = dropped;
            }

            return card;
        }

        private findDroppedInsertIndex( global: PIXI.IPoint ): number
        {
            let insertIndex: number = null;

            if ( global.x == CDropArea.kPredefinedDropPositionCode )
            {
                if ( global.y == deck_side_type.TOP )
                {
                    insertIndex = this._itemHolder.cContainer.c.children.length;
                }
                else if ( global.y == deck_side_type.BOTTOM )
                {
                    insertIndex = 0;
                }
            }
            else 
            {
                insertIndex = this._itemHolder.cContainer.c.children.length;
                if ( this._itemHolder.cContainer.c.getBounds().contains( global.x, global.y ) )
                {
                    let local: PIXI.IPoint = this._itemHolder.cContainer.c.toLocal( global );
                    for ( let i: number = this._itemHolder.cContainer.c.children.length - 1; i >= 0; --i )
                    {
                        const kCardContainer: PIXI.Container = this._itemHolder.cContainer.c.getChildAt( i ) as PIXI.Container;
                        if ( local.x < kCardContainer.x + kCardContainer.width 
                            && local.y >= kCardContainer.y - 5 && local.y <= kCardContainer.y + kCardContainer.height + 5 )
                        {
                            break;
                        }

                        --insertIndex;
                    }
                }
            }

            return insertIndex;
        }

    // virtual.
    public onContextBtnSelected( buttonType: context_button_type, params: Array<number> ): boolean
    {
        let result: boolean = true;

        let deckType: deck_type = null;
        switch ( this._viewer.location )
        {
            case location_type.MY_DECK: { deckType = deck_type.PLAYER; break; }
            case location_type.MY_DISCARD: { deckType = deck_type.PLAYER_DISCARD; break; }
            case location_type.SAURON_DECK_0: { deckType = deck_type.SAURON_0; break; }
            case location_type.SAURON_DECK_1: { deckType = deck_type.SAURON_1; break; }
            case location_type.SAURON_DISCARD: { deckType = deck_type.SAURON_DISCARD; break; }
            case location_type.SAURON_CUSTOM_DECK_0: { deckType = deck_type.SAURON_CUSTOM_0; break; }
            case location_type.SAURON_CUSTOM_DISCARD_0: { deckType = deck_type.SAURON_CUSTOM_DISCARD_0; break; }
            case location_type.SAURON_CUSTOM_DECK_1: { deckType = deck_type.SAURON_CUSTOM_1; break; }
            case location_type.SAURON_CUSTOM_DISCARD_1: { deckType = deck_type.SAURON_CUSTOM_DISCARD_1; break; }
            case location_type.QUEST_DECK: { deckType = deck_type.QUEST_DECK; break; }
            case location_type.MY_CUSTOM_DECK: { deckType = deck_type.PLAYER_CUSTOM; break; }
        }

        switch ( buttonType )
        {
            case context_button_type.REVEAL_X: 
            { 
                this.revealCards( params[ 0 ] as number, action_scope_type.MULTIPLAYER ); 
                break; 
            }

            case context_button_type.DRAW_X_PLAYER_CARDS: 
            { 
                this.drawPlayerCards( params[ 0 ] as number, action_scope_type.MULTIPLAYER );
                break; 
            }

            case context_button_type.DISCARD_X: 
            { 
                this.discardCards( params[ 0 ] as number, action_scope_type.MULTIPLAYER ); 
                if ( this._itemHolder.cContainer.c.children.length == 0 )
                {
                    this._viewer.hide(); 
                }
                break; 
            }

            case context_button_type.REMOVE_ALL: 
            { 
                this.removeCards( action_scope_type.MULTIPLAYER ); 
                this._viewer.hide(); 
                break; 
            }
            
            case context_button_type.SHUFFLE: 
            { 
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetDeck( deckType ), "shuffle", true );
                this.shuffle( action_scope_type.MULTIPLAYER ); 
                this._viewer.hide(); 
                break; 
            }

            case context_button_type.SWITCH: 
            { 
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetDeck( deckType ), "switch", true );
                this.switchDecks( action_scope_type.MULTIPLAYER ); 
                this._viewer.hide(); 
                break; 
            }

            case context_button_type.RANDOMLY_DISCARD: { this.randomlyDiscard(); break; }
            case context_button_type.MULLIGAN: { this.mulligan(); break; }

            case context_button_type.RESHUFFLE: 
            { 
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetDeck( deckType ), "reshuffle", true );
                this.reshuffle( action_scope_type.MULTIPLAYER ); 
                this._viewer.hide(); 
                break; 
            }

            case context_button_type.SORT_BY_SPHERE: { this.sortBySphere( action_scope_type.MULTIPLAYER ); break; }
            case context_button_type.SORT_FROM_A_TO_Z: { this.sortFromAtoZ( action_scope_type.MULTIPLAYER ); break; }

            default: { result = false; }
        }

        return result;
    }

    // protected:

    // virtual.
    protected postProcessDroppedCard( token: GameObject ): void {} 

    // private:

    private mulligan(): void
    {
        this._viewer.hide();

        window.setTimeout( () => { 
            // Put the old cards back at the bottom of their deck.
            let cHandViewer = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.hand.cDeckIndicator.cViewer;
            let arrHandCard: Array<GameObject> = cHandViewer.cCardView.findItems();
            let cDeckViewer: CViewer = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.deck.cDeckIndicator.cViewer;
            for ( let card of arrHandCard )
            {
                cDeckViewer.cCardView.go.cDropArea.forceDrop( card, CCardView.findPredefinedDropPosition( deck_side_type.BOTTOM ), action_scope_type.MULTIPLAYER );
            }

            // Draw a new hand.
            for ( let i: number = 0; i < 6; ++i )
            {
                let newCard: GameObject = cDeckViewer.cCardView.drawCard();
                newCard.cCard.isAnimated = false;
                cHandViewer.cCardView.go.cDropArea.forceDrop( newCard, CCardView.findPredefinedDropPosition( deck_side_type.TOP ), action_scope_type.MULTIPLAYER );
                newCard.cCard.isAnimated = true;
            }

            ( cDeckViewer.controller as CardViewerController ).shuffle( action_scope_type.MULTIPLAYER );

            this._viewer.cCardView.setContextBtnState( [ { buttonType: context_button_type.MULLIGAN, isVisible: false } ] );
            this._viewer.show(); 
        
        }, 400 ); // after hide animation is completed.
    }

    private randomlyDiscard(): void
    {
        let cardIndex: number = Math.floor( Math.random() * this._itemHolder.cContainer.c.children.length );
        let card: GameObject = this._itemHolder.cContainer.c.getChildAt( cardIndex )[ "go" ];
        card.cCard.discard( action_scope_type.MULTIPLAYER );

        ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( card ), "randomly_discard", true );
    }

    private hitTest( global: PIXI.Point, componentName: string, root: PIXI.Container ): Array<GameObject>
    {
        let result: Array<GameObject> = new Array<GameObject>();

        for ( let displayObject of root.children )
        {
            if ( displayObject.interactive 
                && displayObject[ "go" ] != undefined
                && ( ( displayObject as any ).go as GameObject )[ componentName ] != undefined 
                && this.findCardBounds( displayObject ).contains( global.x, global.y ) )
            {
                result.push( ( displayObject as any ).go as GameObject );
            }
            else if ( displayObject instanceof PIXI.Container && displayObject.children.length > 0 )
            {
                result = result.concat( this.hitTest( global, componentName, displayObject ) );
            }
        }

        return result;
    }

    private findCardBounds( displayObject: PIXI.DisplayObject ): PIXI.Rectangle
    {
        let result: PIXI.Rectangle = displayObject.getBounds();

        if ( this._curCard && displayObject == this._curCard.cContainer.c )
        {
            result.height += this._kCurCardHeightDiff;
        }

        return result;
    }

    private saveCurCardState( card: GameObject): void
    {
        this._curCardState = { 
            index: this._itemHolder.cContainer.c.getChildIndex( card.cContainer.c ),
            y: card.cContainer.c.y };
    }

    private loadCurCardState( card: GameObject ): void
    {
        this._itemHolder.cContainer.c.setChildIndex( card.cContainer.c, this._curCardState.index );
        card.cContainer.c.y = this._curCardState.y;
    }

    private clearCurCardState(): void
    {
        if ( this._curCard )
        {
            this.loadCurCardState( this._curCard );
            this._curCard = null;
            this._curCardState = null;
        }
    }

    // #endregion //


    // #region Input Callbacks //

    private onStage_PointerMove( event: PIXI.InteractionEvent ): void
    {
        if ( this._interactionType != interaction_type.PREVIEW 
            || ServiceLocator.game.dragShadowManager.isDragging
            || ServiceLocator.viewManager.findViewCount( view_layer_id.POPUP ) > 0
            || ServiceLocator.viewManager.findViewCount( view_layer_id.TOPMOST ) > 0
            || ServiceLocator.game.root.cGameLayerProvider.get( layer_type.CARD_PRESENTATION ).cContainer.c.children.length > 0
            || ServiceLocator.game.root.cGameLayerProvider.get( layer_type.DISABLED ).cContainer.c.children.length > 0
            || ( event as any ).isPointerUp ) 
        { 
            ( event as any ).isPointerUp = false;
            return; 
        }

        let arrHitCard: Array<GameObject> = this.hitTest( event.data.global, "cCard", this._itemHolder.cContainer.c );
        let curCard: GameObject = null;
        for ( let hitCard of arrHitCard )
        {
            if ( curCard == null || hitCard.cContainer.c.x < curCard.cContainer.c.x )
            {
                curCard = hitCard;
            }
        }
        if ( curCard )
        {
            if ( curCard != this._curCard )
            {
                if ( this._curCard )
                {
                    this.loadCurCardState( this._curCard );
                }
    
                this.saveCurCardState( curCard );
                this._curCard = curCard;
                this._curCard.cContainer.c.y -= this._kCurCardHeightDiff;
                this._itemHolder.cContainer.c.setChildIndex( this._curCard.cContainer.c, this._itemHolder.cContainer.c.children.length - 1 );
            }
        }
        else
        {
            this.clearCurCardState();
        }
    }

    private onStage_PointerUp( event: PIXI.InteractionEvent ): void
    {
        ( event as any ).isPointerUp = true; // PIXI: HACK: pointerup event fires pointermove event too ???
        
        if ( this._interactionType == interaction_type.CARD_LOCKED )
        {
            let arrHitCard: Array<GameObject> = this.hitTest( event.data.global, "cCard", this._itemHolder.cContainer.c );
            if ( arrHitCard.length == 0 )
            {
                this.clearCurCardState();
                this._interactionType = interaction_type.PREVIEW;
            }
        }
    }

    // #endregion //

    
    // #region Other Callbacks //

    // protected:

    // virtual.
    protected onCard_Dragged( card: GameObject ): void
    {   
        this.clearCurCardState();

        this._interactionType = interaction_type.BUSY;
    }

    // virtual.
    protected onCard_DragCancelled( card: GameObject ): void
    {
        this._interactionType = interaction_type.PREVIEW;

        this._onItemDragCanceled.dispatch();
    }

    // virtual.
    protected onCard_PostDropped( dropped: GameObject, dropArea: GameObject ): void
    {
        if ( dropArea != this._viewer.cCardView.go )
        {
            if ( this._itemHolder.cContainer.c.children.length > 0 )
            {
                this._viewer.cCardView.sortItems();
            }
            else
            {
                this._viewer.hide();
            }

            this._viewer.cCardView.onItemCountUpdated.dispatch( this._itemHolder.cContainer.c.children.length, -1 );
        }

        this._interactionType = interaction_type.PREVIEW;

        this._onItemDropped.dispatch( dropped, dropArea );
    }

    // private:

    private onCard_Tap( event: PIXI.InteractionEvent, hit: PIXI.DisplayObject ): void
    {
        let card: GameObject = null;
        while ( !hit[ "go" ] || !hit[ "go" ].cCard )
        {
            hit = hit.parent;
        }
        card = hit[ "go" ];

        if ( ( !event.target[ "go" ] 
                || !Utils.game.findGameObjectInBranchByComponentName( event.target[ "go" ], "CPoi" ) )
            && ( this._interactionType == interaction_type.PREVIEW 
                || this._interactionType == interaction_type.CARD_LOCKED && this._curCard != card ) )
        {
            if ( card != this._curCard )
            {
                if ( this._curCard )
                {
                    this.loadCurCardState( this._curCard );
                }
                this.saveCurCardState( card );
                this._curCard = card;
                this._curCard.cContainer.c.y -= this._kCurCardHeightDiff;
                this._itemHolder.cContainer.c.setChildIndex( this._curCard.cContainer.c, this._itemHolder.cContainer.c.children.length - 1 );
            }

            this._interactionType = interaction_type.CARD_LOCKED;
        
            //ServiceLocator.game.cRadialMenu.onHidden.addOnce( this.onRadialMenu_Hidden, this );
            ServiceLocator.game.cRadialMenu.onOptionSelected.add( this.onRadialMenuOption_Selected, this );
        }

        // Sfx.
        ServiceLocator.audioManager.playSfx( "button_click" );
    }

    private onRadialMenu_Hidden(): void
    {
        this.clearCurCardState();
        this._interactionType = interaction_type.PREVIEW;
    }

    private onRadialMenuOption_Selected( optionType: option_type ): void
    {
        if ( optionType == option_type.DISCARD
            || optionType == option_type.PUT_FACE_DOWN )
        {
            this.clearCurCardState();
            this._interactionType = interaction_type.PREVIEW;
        }
    }

    // #endregion //
}

interface ICurrentCardState
{
    index: number;
    y: number;
}

const enum interaction_type
{
    PREVIEW = 0,
    CARD_LOCKED,
    BUSY
}