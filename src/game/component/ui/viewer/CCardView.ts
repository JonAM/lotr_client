import CMaximizedView from "./CMaximizedView";

import { action_scope_type, IRemoteCard } from "../../../../service/socket_io/GameSocketIOController";
import { player_type } from "../../world/CPlayerArea";
import ServiceLocator from "../../../../ServiceLocator";
import { view_button_type } from "../CViewer";
import * as PIXI from "pixi.js";

import GameObject from "../../../GameObject";
import CardViewerController from "./controller/CardViewerController";
import CContainer from "../../pixi/CContainer";
import CDeckBottomMarker from "./card/CDeckBottomMarker";
import CDropArea from "../../input/CDropArea";
import { context_button_type } from "./CViewContextBtn";
import { ISgCardView } from "../../../../view/game/SaveGameView";


export default class CCardView extends CMaximizedView 
{
    // #region Attributes //

    // private:

    private _playerType: player_type = null;
    private _facePolicy: face_policy_type = face_policy_type.NONE;

    private _topMarker: PIXI.Container = null;
    private _bottomMarker: GameObject = null;

    // Quick access.
    private _cardViewerController: CardViewerController = null;

    // Constants.
    private readonly _kMaxRowItems: number = 35;

    // #endregion //


    // #region Properties //

    public get playerType(): player_type { return this._playerType; }
    public get facePolicy(): face_policy_type { return this._facePolicy; }
    
    public set playerType( value: player_type ) { this._playerType = value; }
    public set facePolicy( value: face_policy_type ) { this._facePolicy = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CCardView";
    }

    public init(): void
    {
        super.init();

        console.assert( this._playerType != null, "CCardView.ts :: init() :: this._playerType cannot be null." );

        this._cardViewerController = this._controller as CardViewerController;

        this._topMarker = this.createTopMarker();
        this._body.addChild( this._topMarker );

        this._bottomMarker = this.createBottomMarker();
        this._body.addChild( this._bottomMarker.cContainer.c );
    }

    public end(): void
    {
        this._bottomMarker.end();
        this._bottomMarker = null;

        super.end();
    }

    public hideDeckMarkers(): void
    {
        this._topMarker.visible = false;
        this._bottomMarker.cContainer.c.visible = false;
    }

    // facade.
    public setCards( arrRemoteCardId: Array<IRemoteCard> ): void
    {
        if ( arrRemoteCardId.length > 0 )
        {
            this._cardViewerController.setCards( arrRemoteCardId );
        }
    }

    // facade.
    public drawCard(): GameObject
    {
        return this._cardViewerController.drawCard();
    }

    public sortItems(): void
    {
        let cardCount: number = 0;
        let insertPos: PIXI.Point = new PIXI.Point( 0, 0 );
        for ( let i: number = this._itemHolder.cContainer.c.children.length - 1; i >= 0; --i )
        {
            let card: PIXI.DisplayObject = this._itemHolder.cContainer.c.getChildAt( i );
            card.position.set( insertPos.x, insertPos.y );
            
            ++cardCount;
            if ( cardCount == this._kMaxRowItems )
            {
                cardCount = 0;
                insertPos.x = 0;
                insertPos.y += ( card as PIXI.Container ).height + 10;
            }
            else
            {
                insertPos.x += 40;
            }
        }

        super.drawBg();
        const kCardHalfHeight: number = ( this._itemHolder.cContainer.c.getChildAt( 0 ) as PIXI.Container ).height * 0.5;
        this._topMarker.position.set( 0, 10 + kCardHalfHeight );
        const kBodyBg: PIXI.Container = this._itemHolder.cContainer.c.parent.children[ 0 ] as PIXI.Container;
        this._bottomMarker.cContainer.c.position.set( kBodyBg.width, kBodyBg.height - 10 - kCardHalfHeight );
        super.onItemsSorted();
    }

    public saveGame(): ISgCardView
    {
        let result: ISgCardView = { cards: [] };

        for ( let displayObject of this._itemHolder.cContainer.c.children )
        {
            result.cards.push( displayObject[ "go" ].cCard.saveGame() );
        }

        return result;
    }

    public loadGame( sgCardView: ISgCardView, pass: number ): void
    {
        if ( pass == 0 )
        {
            for ( let sgCard of sgCardView.cards )
            {
                let card: GameObject = GameObject.find( sgCard.oid );
                this._go.cDropArea.forceDrop( card, new PIXI.Point( CDropArea.kPredefinedDropPositionCode, 0 ), action_scope_type.LOCAL );
                card.cCard.loadGame( sgCard, pass );
            }
        }
    }

    public static findPredefinedDropPosition( deckSideType: deck_side_type ): PIXI.Point
    {
        return new PIXI.Point( CDropArea.kPredefinedDropPositionCode, deckSideType );
    }

    // private:

    private createTopMarker(): PIXI.Container 
    {
        let text: PIXI.Text = new PIXI.Text( jQuery.i18n( "TOP" ), ServiceLocator.game.textStyler.normal );
        text.anchor.set( 0.5, 1 );
        text.rotation = Math.PI * 0.5;

        let bg: PIXI.Graphics = new PIXI.Graphics();
        bg.lineStyle( 2, 0x000000, 1 );
        bg.beginFill( 0xEFE3AF );
        bg.drawRect( 0, -text.width * 0.5 - 10, text.height, text.width + 20 );
        bg.endFill();

        let topMarker: PIXI.Container = new PIXI.Container();
        topMarker.addChild( bg );
        topMarker.addChild( text );
         
        return topMarker;
    }

    private createBottomMarker(): GameObject
    {
        let bottomMarker: GameObject = new GameObject( [ new CContainer(), new CDeckBottomMarker(), new CDropArea() ] );
        bottomMarker.init();

        if ( this._playerType == player_type.ALLY )
        {
            bottomMarker.cDropArea.setEnabled( false );
        }
        else
        { 
            bottomMarker.cDeckBottomMarker.onCardDropped.add( this.onBottomMarker_CardDropped, this );
        }

        return bottomMarker;
    }

    // #endregion //


    // #region IGameObjectDropArea //

    public validateDroppedGameObject( token: GameObject ): boolean
    {
        return this._cardViewerController.validateDroppedGameObject( token ); 
    }

    public processDroppedGameObject( card: GameObject, global: PIXI.IPoint, actionScopeType: action_scope_type ): void
    {
        this._cardViewerController.processDroppedGameObject( card, global, actionScopeType );

        // Sfx.
        ServiceLocator.audioManager.playSfx( "token_dropped" );
    }

    // #endregion //

    
    // #region Callbacks //

    // protected:

    protected onViewBtn_Click( buttonType: view_button_type ): void
    {
        this._controller.onViewBtnSelected( buttonType ); 
    }

    protected onContextBtn_Click( buttonType: context_button_type, params: Array<any> ): void
    {
        this._controller.onContextBtnSelected( buttonType, params ); 
    }

    // private:

    private onBottomMarker_CardDropped( card: GameObject, actionScopeType: action_scope_type ): void
    {
        this._cardViewerController.processDroppedGameObject( card, CCardView.findPredefinedDropPosition( deck_side_type.BOTTOM ), actionScopeType ); 
    }

    // #endregion //
}

export const enum deck_side_type
{
    TOP = 0,
    BOTTOM
}

export const enum face_policy_type
{
    NONE = 0,
    FACE_UP,
    FACE_DOWN
}