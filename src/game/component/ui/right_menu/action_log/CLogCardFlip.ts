import CLogAction from "./CLogAction";

import { log_layout_type } from "../CActionLogger";
import { player_type } from "../../../world/CPlayerArea";
import ServiceLocator from "../../../../../ServiceLocator";
import Utils from "../../../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../../../GameObject";
import CContainer from "../../../pixi/CContainer";
import { ISgLogAction } from "../../../../../view/game/SaveGameView";
import LogTargetCard from "./target/LogTargetCard";
import { ICard } from "../../../../CardDB";


export default class CLogCardFlip extends CLogAction
{
    // #region Attributes //

    // private:

    private _target: LogTargetCard = null;

    // #endregion //


    // #region Properties //

    public set target( value: LogTargetCard ) { this._target = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CLogCardFlip";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cLogActionPreviewable != null, "CLogCardFlip.ts :: init() :: CLogActionPreviewable component not found." );
        console.assert( this._target != null, "CLogCardFlip.ts :: init() :: this._target cannot be null." );

        let card: PIXI.Sprite = this._target.createIconSprite();
        Utils.game.limitSideSize( 60, card );
        this._content.cContainer.c.addChild( card );

        let icon: PIXI.Sprite = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( Utils.game.isCardFrontSide( this._target.id ) ? "rad_put_face_down" : "rad_put_face_up" ) );
        Utils.game.limitSideSize( 30, icon );
        icon.anchor.set( 0.5 );
        icon.position.set( this._content.cContainer.c.width * 0.5 );
        icon.x += 10;
        this._content.cContainer.c.addChild( icon );

        let arrow: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( "arrow" ).data ) );
        Utils.game.limitSideSize( 30, arrow );
        arrow.anchor.set( 1, 0.5 );
        arrow.position.set( this._content.cContainer.c.width * 0.5 );
        this._content.cContainer.c.addChild( arrow );

        // Listen to events.
        this._go.cLogActionPreviewable.onPreviewShown.add( this.onPreview_Shown, this );
        this._go.cLogActionPreviewable.onPreviewHidden.add( this.onPreview_Hidden, this );
    }

    public end(): void
    {        
        // Cleanup events.
        this._go.cLogActionPreviewable.onPreviewShown.remove( this.onPreview_Shown, this );
        this._go.cLogActionPreviewable.onPreviewHidden.remove( this.onPreview_Hidden, this );

        this._target = null;
    
        super.end();
    }

    public createPreview(): GameObject
    {
        let result: GameObject = new GameObject( [ new CContainer() ] );
        result.init();

        let fromSprite: PIXI.Sprite = this._target.createPreviewSprite();
        result.cContainer.c.addChild( fromSprite );
        
        let arrow: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( "arrow" ).data ) );
        Utils.game.setSideSize( 60, arrow );
        arrow.anchor.y = 0.5;
        arrow.position.set( fromSprite.width + 5, fromSprite.height * 0.5 );
        result.cContainer.c.addChild( arrow );

        let contextIcon: PIXI.Sprite = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( Utils.game.isCardFrontSide( this._target.id ) ? "rad_put_face_down" : "rad_put_face_up" ) );
        Utils.game.setSideSize( 60, contextIcon );
        contextIcon.anchor.set( 0.5 );
        contextIcon.position.set( 
            arrow.x + arrow.width * 0.5, 
            arrow.y - ( arrow.height + contextIcon.height ) * 0.5 - 10 );
        result.cContainer.c.addChild( contextIcon );

        let toSprite: PIXI.Sprite = new PIXI.Sprite( Utils.img.findCardTexture( this._target.flipSideId ) );
        toSprite.anchor.y = 0.5;
        toSprite.position.set( arrow.x + arrow.width + 5, toSprite.height * 0.5 );
        result.cContainer.c.addChild( toSprite );

        if ( fromSprite.height > toSprite.height )
        {
            toSprite.y += ( fromSprite.height - toSprite.height ) * 0.5;
        }
        else if ( toSprite.height > fromSprite.height )
        {
            let dy: number = ( toSprite.height - fromSprite.height ) * 0.5;
            fromSprite.y += dy;
            arrow.y += dy;
            contextIcon.y += dy;
        }

        return result;
    }

    // overrides.
    public saveGame(): ISgLogAction
    {
        let logAction: ISgLogAction = super.saveGame();
        logAction.logCardFlip = { target: this._target.saveGame() };

        return logAction;
    }

    // overrides.
    public serialize(): Array<any>
    {
        return [ 
            log_layout_type.CARD_FLIP, 
            this._who ^ 1, 
            this._target.serialize() ];
    }

    // overrides.
    public deserialize( params: Array<any> ): void
    {
        this._who = params[ 0 ] as player_type;
        this._target = Utils.actionLog.deserializeTarget( params[ 1 ] as Array<any> ) as LogTargetCard;
    } 

    // #endregion //


    // #region Callbacks //

    private onPreview_Shown(): void
    {
        this._target.onPreviewShown();
    }

    private onPreview_Hidden(): void
    {
        this._target.onPreviewHidden();
    }

    // #endregion //
}