import CLogAction from "./CLogAction";

import { log_layout_type } from "../CActionLogger";
import Utils from "../../../../../Utils";
import ServiceLocator from "../../../../../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "../../../../GameObject";
import LogTarget from "./LogTarget";
import CContainer from "../../../pixi/CContainer";
import { player_type } from "../../../world/CPlayerArea";
import { ISgLogAction } from "../../../../../view/game/SaveGameView";


export default class CLogCounter extends CLogAction
{
    // #region Attributes //

    // private:

    private _target: LogTarget = null;
    private _iconId: string = null;
    private _count: number = null;
    private _total: number = null;

    private _countText: PIXI.Text = null;

    // #endregion //


    // #region Properties //

    public get target(): LogTarget { return this._target; }
    public get iconId(): string { return this._iconId; }
    public get count(): number { return this._count; }
    public get total(): number { return this._total; }

    public set target( value: LogTarget ) { this._target = value; }
    public set iconId( value: string ) { this._iconId = value; }
    public set count( value: number ) { this._count = value; }
    public set total( value: number ) { this._total = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CLogCounter";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cLogActionPreviewable != null, "CLogCounter.ts :: init() :: CLogActionPreviewable component not found." );
        console.assert( this._target != null, "CLogCounter.ts :: init() :: this._target cannot be null." );
        console.assert( this._iconId != null, "CLogCounter.ts :: init() :: this._iconId cannot be null." );
        console.assert( this._count != null, "CLogCounter.ts :: init() :: this._counter cannot be null." );

        let targetSprite: PIXI.Sprite = this._target.createIconSprite();
        Utils.game.limitSideSize( 60, targetSprite );
        this._content.cContainer.c.addChild( targetSprite );

        let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( this._iconId ).data ) );
        Utils.game.limitSideSize( 40, icon );
        icon.anchor.set( 0.5 );
        icon.position.set( this._content.cContainer.c.width * 0.5 );
        this._content.cContainer.c.addChild( icon );

        this._countText = new PIXI.Text( ( this._count > 0 ? "+" : "-" ) + Math.abs( this._count ).toString(), ServiceLocator.game.textStyler.normal );
        this._countText.anchor.set( 0.5 );
        this._countText.position.set( this._content.cContainer.c.width * 0.5 );
        this._content.cContainer.c.addChild( this._countText );

        // Listen to events.
        this._go.cLogActionPreviewable.onPreviewShown.add( this.onPreview_Shown, this );
        this._go.cLogActionPreviewable.onPreviewHidden.add( this.onPreview_Hidden, this );
    }

    public reinit(): void
    {
        this._countText.text = ( this._count > 0 ? "+" : "-" ) + Math.abs( this._count ).toString();
    }

    public end(): void
    {        
        // Cleanup events.
        this._go.cLogActionPreviewable.onPreviewShown.remove( this.onPreview_Shown, this );
        this._go.cLogActionPreviewable.onPreviewHidden.remove( this.onPreview_Hidden, this );
    
        super.end();
    }

    // overrides.
    public createPreview(): GameObject
    {
        let result: GameObject = new GameObject( [ new CContainer() ] );
        result.init();

        let targetSprite: PIXI.Sprite = this._target.createPreviewSprite();
        result.cContainer.c.addChild( targetSprite );

        let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( this._iconId ).data ) );
        Utils.game.setSideSize( 60, icon );
        icon.anchor.set( 1 );
        icon.position.set( result.cContainer.c.width + 10, result.cContainer.c.height + 10 );
        result.cContainer.c.addChild( icon );

        let deltaText: PIXI.Text = new PIXI.Text( ( this._count > 0 ? "+" : "-" ) + Math.abs( this._count ).toString(), ServiceLocator.game.textStyler.subtitle );
        deltaText.anchor.set( 0.5 );
        deltaText.position.set( icon.x - icon.width * 0.5, icon.y - icon.height * 0.5 );
        result.cContainer.c.addChild( deltaText );

        if ( this._total != null )
        {
            let textStyle: PIXI.TextStyle = ServiceLocator.game.textStyler.normal.clone();
            textStyle.fontStyle = "italic";
            let totalText: PIXI.Text = new PIXI.Text( "(" + this._total.toString() + ")", textStyle );
            totalText.anchor.set( 0.5, 1 );
            totalText.position.set( icon.x - icon.width * 0.5, icon.y );
            result.cContainer.c.addChild( totalText );

            deltaText.y -= 10;
        }

        return result;
    }

    public equals( cLogCounter: CLogCounter ): boolean
    {
        return this._iconId == cLogCounter.iconId && this._target.equals( cLogCounter.target );
    }

    public updateCountText( count: number ): void
    {
        const kNewCount: number = parseInt( this._countText.text ) + count;
        this._countText.text = ( kNewCount > 0 ? "+" : "-" ) + Math.abs( kNewCount ).toString();
    }

    // overrides.
    public saveGame(): ISgLogAction
    {
        let logAction: ISgLogAction = super.saveGame();
        logAction.logCounter = { 
            target: this._target.saveGame(),
            iconId: this._iconId,
            count: this._count,
            total: this._total };

        return logAction;
    }

    // overrides.
    public serialize(): Array<any>
    {
        return [ log_layout_type.COUNTER, this._who ^ 1, this._target.serialize(), this._iconId, this._count, this._total ];
    }

    // overrides.
    public deserialize( params: Array<any> ): void
    {
        this._who = params[ 0 ] as player_type;
        this._target = Utils.actionLog.deserializeTarget( params[ 1 ] as Array<any> );
        this._iconId = params[ 2 ] as string;
        this._count = params[ 3 ] as number;
        this._total = params[ 4 ] as number;
    }

    // #endregion //


    // #region Callbacks //

    private onPreview_Shown(): void
    {
        this._target.onPreviewShown();
    }

    private onPreview_Hidden(): void
    {
        this._target.onPreviewHidden();
    }

    // #endregion //
}