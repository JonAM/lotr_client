import { ISgLogTarget } from "../../../../../view/game/SaveGameView";


export default abstract class LogTarget
{
   protected _type: log_target_type = null;

   
   public get type(): log_target_type { return this._type; }
   

   public abstract createIconSprite(): PIXI.Sprite;
   public abstract createPreviewSprite(): PIXI.Sprite;

   // virtual.
   public saveGame(): ISgLogTarget
   {
      return { type: this._type };
   }

   // virtual.
   public loadGame( sgLogTarget: ISgLogTarget, pass: number ): void {}

   public abstract serialize(): Array<any>;
   public abstract deserialize( serialized: Array<any> ): void;

   // virtual.
   public equals( target: LogTarget ): boolean
   {
      return this._type == target.type;
   }

   // virtual.
   public onPreviewShown(): void {}

   // virtual.
   public onPreviewHidden(): void {}
}

export const enum log_target_type
{
   CARD = 0,
   FIRST_PLAYER,
   PLAYER,
   DECK
}