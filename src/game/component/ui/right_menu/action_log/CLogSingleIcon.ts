import CLogAction from "./CLogAction";

import { log_layout_type } from "../CActionLogger";
import Utils from "../../../../../Utils";
import ServiceLocator from "../../../../../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "../../../../GameObject";
import LogTarget from "./LogTarget";
import CContainer from "../../../pixi/CContainer";
import { player_type } from "../../../world/CPlayerArea";
import { ISgLogAction } from "../../../../../view/game/SaveGameView";


export default class CLogSingleIcon extends CLogAction
{
    // #region Attributes //

    // private:

    private _target: LogTarget = null;
    private _iconId: string = null;

    // #endregion //


    // #region Properties //

    public set target( value: LogTarget ) { this._target = value; }
    public set iconId( value: string ) { this._iconId = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CLogSingleIcon";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cLogActionPreviewable != null, "CLogSingleIcon.ts :: init() :: CLogActionPreviewable component not found." );
        console.assert( this._target != null, "CLogSingleIcon.ts :: init() :: this._target cannot be null." );
        console.assert( this._iconId != null, "CLogSingleIcon.ts :: init() :: this._iconId cannot be null." );

        let targetSprite: PIXI.Sprite = this._target.createIconSprite();
        Utils.game.limitSideSize( 60, targetSprite );
        this._content.cContainer.c.addChild( targetSprite );

        let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( this._iconId ).data ) );
        Utils.game.limitSideSize( 40, icon );
        icon.anchor.set( 0.5 );
        icon.position.set( this._content.cContainer.c.width * 0.5 );
        this._content.cContainer.c.addChild( icon );

        // Listen to events.
        this._go.cLogActionPreviewable.onPreviewShown.add( this.onPreview_Shown, this );
        this._go.cLogActionPreviewable.onPreviewHidden.add( this.onPreview_Hidden, this );
    }

    public end(): void
    {        
        // Cleanup events.
        this._go.cLogActionPreviewable.onPreviewShown.remove( this.onPreview_Shown, this );
        this._go.cLogActionPreviewable.onPreviewHidden.remove( this.onPreview_Hidden, this );
    
        super.end();
    }

    public createPreview(): GameObject
    {
        let result: GameObject = new GameObject( [ new CContainer() ] );
        result.init();

        let targetSprite: PIXI.Sprite = this._target.createPreviewSprite();
        result.cContainer.c.addChild( targetSprite );

        let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( this._iconId ).data ) );
        Utils.game.setSideSize( 60, icon );
        icon.anchor.set( 1 );
        icon.position.set( result.cContainer.c.width + 10, result.cContainer.c.height + 10 );
        result.cContainer.c.addChild( icon );

        return result;
    }

    // overrides.
    public saveGame(): ISgLogAction
    {
        let logAction: ISgLogAction = super.saveGame();
        logAction.logSingleIcon = { 
            target: this._target.saveGame(),
            iconId: this._iconId };

        return logAction;
    }

    // overrides.
    public serialize(): Array<any>
    {
        return [ log_layout_type.SINGLE_ICON, this._who ^ 1, this._target.serialize(), this._iconId ];
    }

    // overrides.
    public deserialize( params: Array<any> ): void
    {
        this._who = params[ 0 ] as player_type;
        this._target = Utils.actionLog.deserializeTarget( params[ 1 ] as Array<any> );
        this._iconId = params[ 2 ] as string;
    }

    // #endregion //


    // #region Callbacks //

    private onPreview_Shown(): void
    {
        this._target.onPreviewShown();
    }

    private onPreview_Hidden(): void
    {
        this._target.onPreviewHidden();
    }

    // #endregion //
}