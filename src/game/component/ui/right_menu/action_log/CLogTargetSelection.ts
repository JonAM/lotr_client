import CLogAction from "./CLogAction";

import { log_layout_type } from "../CActionLogger";
import Utils from "../../../../../Utils";
import ServiceLocator from "../../../../../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "../../../../GameObject";
import LogTarget from "./LogTarget";
import CContainer from "../../../pixi/CContainer";
import { player_type } from "../../../world/CPlayerArea";
import { ISgLogAction } from "../../../../../view/game/SaveGameView";


export default class CLogTargetSelection extends CLogAction
{
    // #region Attributes //

    // private:

    private _from: LogTarget = null;
    private _to: LogTarget = null;
    private _contextIconId: string = null;
    private _isReverse: boolean = false;

    // #endregion //


    // #region Properties //

    public set from( value: LogTarget ) { this._from = value; }
    public set to( value: LogTarget ) { this._to = value; }
    public set contextIconId( value: string ) { this._contextIconId = value; }
    public set isReverse( value: boolean ) { this._isReverse = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CLogTargetSelection";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cLogActionPreviewable != null, "CLogTargetSelection.ts :: init() :: CLogActionPreviewable component not found." );
        console.assert( this._from != null, "CLogTargetSelection.ts :: init() :: this._from cannot be null." );
        console.assert( this._to != null, "CLogTargetSelection.ts :: init() :: this._to cannot be null." );
        console.assert( this._contextIconId != null, "CLogTargetSelection.ts :: init() :: this._contextIconId cannot be null." );

        let toSprite: PIXI.Sprite = this._to.createIconSprite();
        Utils.game.limitSideSize( 60, toSprite );
        this._content.cContainer.c.addChild( toSprite );

        let icon: PIXI.Sprite = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( this._contextIconId ) );
        Utils.game.limitSideSize( 30, icon );
        icon.anchor.set( 0.5 );
        icon.position.set( this._content.cContainer.c.width * 0.5 );
        icon.x += 10;
        this._content.cContainer.c.addChild( icon );

        let arrow: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( "arrow" ).data ) );
        Utils.game.limitSideSize( 30, arrow );
        arrow.anchor.set( 1, 0.5 );
        arrow.position.set( this._content.cContainer.c.width * 0.5 );
        this._content.cContainer.c.addChild( arrow );

        // Listen to events.
        this._go.cLogActionPreviewable.onPreviewShown.add( this.onPreview_Shown, this );
        this._go.cLogActionPreviewable.onPreviewHidden.add( this.onPreview_Hidden, this );
    }

    public end(): void
    {        
        // Cleanup events.
        this._go.cLogActionPreviewable.onPreviewShown.remove( this.onPreview_Shown, this );
        this._go.cLogActionPreviewable.onPreviewHidden.remove( this.onPreview_Hidden, this );

        this._from = null;
        this._to = null;
        this._contextIconId = null;
        this._isReverse = null;
    
        super.end();
    }

    public createPreview(): GameObject
    {
        let result: GameObject = new GameObject( [ new CContainer() ] );
        result.init();

        let fromSprite: PIXI.Sprite = this._from.createPreviewSprite();
        result.cContainer.c.addChild( fromSprite );
        
        let arrow: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( "arrow" ).data ) );
        Utils.game.setSideSize( 60, arrow );
        arrow.anchor.y = 0.5;
        if ( this._isReverse )
        {
            arrow.scale.x *= -1;
            arrow.anchor.x = 1;
        }
        arrow.position.set( fromSprite.width + 5, fromSprite.height * 0.5 );
        result.cContainer.c.addChild( arrow );

        let contextIcon: PIXI.Sprite = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( this._contextIconId ) );
        Utils.game.setSideSize( 60, contextIcon );
        contextIcon.anchor.set( 0.5 );
        contextIcon.position.set( 
            arrow.x + arrow.width * 0.5, 
            arrow.y - ( arrow.height + contextIcon.height ) * 0.5 - 10 );
        result.cContainer.c.addChild( contextIcon );

        let toSprite: PIXI.Sprite = this._to.createPreviewSprite();
        toSprite.anchor.y = 0.5;
        toSprite.position.set( arrow.x + arrow.width + 5, fromSprite.height * 0.5 );
        result.cContainer.c.addChild( toSprite );

        return result;
    }

    // overrides.
    public saveGame(): ISgLogAction
    {
        let logAction: ISgLogAction = super.saveGame();
        logAction.logTargetSelection = { 
            from: this._from.saveGame(),
            to: this._to.saveGame(),
            contextIconId: this._contextIconId,
            isReverse: this._isReverse };

        return logAction;
    }

    // overrides.
    public serialize(): Array<any>
    {
        return [ 
            log_layout_type.TARGET_SELECTION, 
            this._who ^ 1, 
            this._from.serialize(), 
            this._to.serialize(), 
            this._contextIconId,
            this._isReverse ];
    }

    // overrides.
    public deserialize( params: Array<any> ): void
    {
        this._who = params[ 0 ] as player_type;
        this._from = Utils.actionLog.deserializeTarget( params[ 1 ] as Array<any> );
        this._to = Utils.actionLog.deserializeTarget( params[ 2 ] as Array<any> );
        this._contextIconId = params[ 3 ] as string;
        this._isReverse = params[ 4 ] as boolean;
    } 

    // #endregion //


    // #region Callbacks //

    private onPreview_Shown(): void
    {
        this._from.onPreviewShown();
        this._to.onPreviewShown();
    }

    private onPreview_Hidden(): void
    {
        this._from.onPreviewHidden();
        this._to.onPreviewHidden();
    }

    // #endregion //
}