import CLogAction from "./CLogAction";

import { log_layout_type } from "../CActionLogger";
import { player_type } from "../../../world/CPlayerArea";
import ServiceLocator from "../../../../../ServiceLocator";
import Session from "../../../../../Session";
import * as PIXI from "pixi.js";
import Utils from "../../../../../Utils";
import { ISgLogAction } from "../../../../../view/game/SaveGameView";


export default class CLogActivePlayer extends CLogAction
{
    // #region Attributes //

    // private:

    private _playerType: player_type = null;

    // #endregion //


    // #region Properties //

    public set playerType( value: player_type ) { this._playerType = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CLogActivePlayer";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cTextPreviewable != null, "CLogActivePlayer.ts :: init() :: CTextPreviewable component not found." );
        console.assert( this._playerType != null, "CLogActivePlayer.ts :: init() :: this._playerType cannot be null." );

        this._go.cTextPreviewable.text = jQuery.i18n( "IS_THE_ACTIVE_PLAYER" ).replace( "#", this._playerType == player_type.PLAYER ? Session.playerId : Session.allyId ); 

        let g: PIXI.Graphics = new PIXI.Graphics();
        g.beginFill( 0xffffff );
        g.drawRect( 0, 0, 60, 60 );
        g.endFill();
        this._content.cContainer.c.addChild( g );

        g = new PIXI.Graphics();
        g.lineStyle( 1, 0x000000 );
        g.beginFill( ServiceLocator.game.playerColors[ this._playerType ], 0.25 );
        g.drawRect( 0, 0, 60, 60 );
        g.endFill();
        this._content.cContainer.c.addChild( g );

        let baseTexture: PIXI.BaseTexture = ServiceLocator.resourceStack.findAsTexture( Utils.img.findPlayerTextureId( this._playerType ) ).baseTexture;
        const kSideLength: number = baseTexture.width - 130;
        let icon: PIXI.Sprite = new PIXI.Sprite( new PIXI.Texture( 
            baseTexture, 
            new PIXI.Rectangle( 65, 65, kSideLength, kSideLength ),
            new PIXI.Rectangle( 0, 0, kSideLength, kSideLength ) ) );
        Utils.game.limitSideSize( 60, icon );
        icon.anchor.set( 0.5 );
        icon.position.set( this._content.cContainer.c.width * 0.5 );
        this._content.cContainer.c.addChild( icon );
    }

    // overrides.
    public saveGame(): ISgLogAction
    {
        let logAction: ISgLogAction = super.saveGame();
        logAction.logActivePlayer = { playerType: this._playerType };

        return logAction;
    }

    // overrides.
    public serialize(): Array<any>
    {
        return [ log_layout_type.ACTIVE_PLAYER, this._playerType ^ 1 ];
    }

    // overrides.
    public deserialize( params: Array<any> ): void
    {
        this._playerType = params[ 0 ] as player_type;
    }

    // #endregion //
}