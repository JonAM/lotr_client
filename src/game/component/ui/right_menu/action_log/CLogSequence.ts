import CLogAction from "./CLogAction";

import { log_layout_type } from "../CActionLogger";
import Utils from "../../../../../Utils";
import ServiceLocator from "../../../../../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "../../../../GameObject";
import LogTarget from "./LogTarget";
import CContainer from "../../../pixi/CContainer";
import { player_type } from "../../../world/CPlayerArea";
import { ISgLogAction } from "../../../../../view/game/SaveGameView";


export default class CLogSequence extends CLogAction
{
    // #region Attributes //

    // private:

    private _target: LogTarget = null;
    private _fromIconId: string = null;
    private _toIconId: string = null;

    // #endregion //


    // #region Properties //

    public set target( value: LogTarget ) { this._target = value; }
    public set fromIconId( value: string ) { this._fromIconId = value; }
    public set toIconId( value: string ) { this._toIconId = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CLogSequence";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cLogActionPreviewable != null, "CLogSequence.ts :: init() :: CLogActionPreviewable component not found." );
        console.assert( this._target != null, "CLogSequence.ts :: init() :: this._target cannot be null." );
        console.assert( this._fromIconId != null, "CLogSequence.ts :: init() :: this._fromIconId cannot be null." );
        console.assert( this._toIconId != null, "CLogSequence.ts :: init() :: this._toIconId cannot be null." );

        let targetSprite: PIXI.Sprite = this._target.createIconSprite();
        Utils.game.limitSideSize( 60, targetSprite );
        this._content.cContainer.c.addChild( targetSprite );

        let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( this._toIconId ).data ) );
        Utils.game.limitSideSize( 30, icon );
        icon.anchor.set( 0.5 );
        icon.position.set( this._content.cContainer.c.width * 0.5 );
        icon.x += 10;
        this._content.cContainer.c.addChild( icon );

        let arrow: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( "arrow" ).data ) );
        Utils.game.limitSideSize( 30, arrow );
        arrow.anchor.set( 1, 0.5 );
        arrow.position.set( this._content.cContainer.c.width * 0.5 );
        this._content.cContainer.c.addChild( arrow );

        // Listen to events.
        this._go.cLogActionPreviewable.onPreviewShown.add( this.onPreview_Shown, this );
        this._go.cLogActionPreviewable.onPreviewHidden.add( this.onPreview_Hidden, this );
    }

    public end(): void
    {        
        // Cleanup events.
        this._go.cLogActionPreviewable.onPreviewShown.remove( this.onPreview_Shown, this );
        this._go.cLogActionPreviewable.onPreviewHidden.remove( this.onPreview_Hidden, this );
    
        super.end();
    }

    public createPreview(): GameObject
    {
        let result: GameObject = new GameObject( [ new CContainer() ] );
        result.init();

        let targetSprite: PIXI.Sprite = this._target.createPreviewSprite();
        result.cContainer.c.addChild( targetSprite );

        let iconContainer: PIXI.Container = new PIXI.Container();
        //
        let fromSprite: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( this._fromIconId ).data ) );
        Utils.game.setSideSize( 60, fromSprite );
        iconContainer.addChild( fromSprite );
        //
        let arrow: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( "arrow" ).data ) );
        Utils.game.limitSideSize( 30, arrow );
        arrow.position.set( fromSprite.width + 5, ( fromSprite.height - arrow.height ) * 0.5 );
        iconContainer.addChild( arrow );
        //
        let toSprite: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( this._toIconId ).data ) );
        Utils.game.limitSideSize( 60, toSprite );
        toSprite.position.set( arrow.x + arrow.width + 5, 0 );
        iconContainer.addChild( toSprite );
        //
        iconContainer.pivot.set( iconContainer.width, iconContainer.height );
        iconContainer.position.set( result.cContainer.c.width + 10, result.cContainer.c.height + 10 );
        result.cContainer.c.addChild( iconContainer );

        return result;
    }

    // overrides.
    public saveGame(): ISgLogAction
    {
        let logAction: ISgLogAction = super.saveGame();
        logAction.logSequence = { 
            target: this._target.saveGame(),
            fromIconId: this._fromIconId,
            toIconId: this._toIconId };

        return logAction;
    }

    // overrides.
    public serialize(): Array<any>
    {
        return [ log_layout_type.SEQUENCE, this._who ^ 1, this._target.serialize(), this._fromIconId, this._toIconId ];
    }

    // overrides.
    public deserialize( params: Array<any> ): void
    {
        this._who = params[ 0 ] as player_type;
        this._target = Utils.actionLog.deserializeTarget( params[ 1 ] as Array<any> );
        this._fromIconId = params[ 2 ] as string;
        this._toIconId = params[ 3 ] as string;
    }

    // #endregion //


    // #region Callbacks //

    private onPreview_Shown(): void
    {
        this._target.onPreviewShown();
    }

    private onPreview_Hidden(): void
    {
        this._target.onPreviewHidden();
    }

    // #endregion //
}