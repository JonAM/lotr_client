import LogTarget, { log_target_type } from "../LogTarget";

import { player_type } from "../../../../world/CPlayerArea";
import Utils from "../../../../../../Utils";
import * as PIXI from "pixi.js";
import { ISgLogTarget } from "../../../../../../view/game/SaveGameView";
import ServiceLocator from "../../../../../../ServiceLocator";


export default class LogTargetPlayer extends LogTarget
{
    // #region Attributes //

    // private:

    private _playerType: player_type = null;

    // #endregion //


    // #region Properties //

    public get playerType(): player_type { return this._playerType; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor( playerType: player_type = null )
    {
        super();

        this._type = log_target_type.PLAYER;
        
        if ( playerType != null )
        {
            this._playerType = playerType;
        }
    }

    public createIconSprite(): PIXI.Sprite
    {
        let baseTexture: PIXI.BaseTexture = ServiceLocator.resourceStack.findAsTexture( Utils.img.findPlayerTextureId( this._playerType ) ).baseTexture;
        const kSideLength: number = baseTexture.width - 130;

        return new PIXI.Sprite( new PIXI.Texture( 
            baseTexture, 
            new PIXI.Rectangle( 65, 65, kSideLength, kSideLength ),
            new PIXI.Rectangle( 0, 0, kSideLength, kSideLength ) ) );
    }

    public createPreviewSprite(): PIXI.Sprite
    {
        return new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( Utils.img.findPlayerTextureId( this._playerType ) ) );
    }

    // overrides.
    public saveGame(): ISgLogTarget
    {
        let logTarget: ISgLogTarget = super.saveGame();
        logTarget.logTargetPlayer = { playerType: this._playerType };

        return logTarget;
    }

    // overrides.
    public loadGame( sgLogTarget: ISgLogTarget, pass: number ): void
    {
        if ( pass == 0 )
        {
            this._playerType = sgLogTarget.logTargetPlayer.playerType;
        }
    }

    // overrides.
    public equals( target: LogTarget ): boolean
    {
        let result: boolean = super.equals( target );
        if ( result )
        {
            result = this._playerType == ( target as LogTargetPlayer ).playerType;
        }

        return result;
    }

    // overrides.
    public serialize(): Array<any>
    {
        return [ log_target_type.PLAYER, this.playerType ^ 1 ];
    }

    // overrides.
    public deserialize( serialized: Array<any> ): void
    {
        this._playerType = serialized[ 0 ] as player_type;
    }

    // #endregion //
}