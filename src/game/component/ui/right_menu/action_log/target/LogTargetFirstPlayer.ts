import LogTarget, { log_target_type } from "../LogTarget";

import ServiceLocator from "../../../../../../ServiceLocator";
import * as PIXI from "pixi.js";


export default class LogTargetFirstPlayer extends LogTarget
{
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._type = log_target_type.FIRST_PLAYER;
    }

    public createIconSprite(): PIXI.Sprite
    {
        return this.createFirstPlayerSquare();
    }

    public createPreviewSprite(): PIXI.Sprite
    {
        return new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( "first_player" ).data ) );;
    }

    // overrides.
    public serialize(): Array<any>
    {
        return [ log_target_type.FIRST_PLAYER ];
    }

    // overrides.
    public deserialize( serialized: Array<any> ): void {}

    // private:

    private createFirstPlayerSquare(): PIXI.Sprite
    {
        let baseTexture: PIXI.BaseTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "first_player" ).data ).baseTexture;

        const kSquareSide: number = 70;
        return new PIXI.Sprite( new PIXI.Texture( 
            baseTexture, 
            new PIXI.Rectangle( kSquareSide, ( baseTexture.height - kSquareSide ) * 0.5, kSquareSide, kSquareSide ),
            new PIXI.Rectangle( 0, 0, baseTexture.width, baseTexture.width ) ) );
    }

    // #endregion //
}