import LogTarget, { log_target_type } from "../LogTarget";

import ServiceLocator from "../../../../../../ServiceLocator";
import Utils from "../../../../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../../../../GameObject";
import { ISgLogTarget } from "../../../../../../view/game/SaveGameView";
import CCardToken from "../../../../card/CCardToken";


export default class LogTargetCard extends LogTarget
{
    // #region Attributes //

    // private:

    private _id: string = null;
    
    private _oid: string = null;
    private _flipSideId: string = null;

    // #endregion //


    // #region Properties //
    
    public set id( value: string ) { this._id = value; }

    public get id(): string { return this._id; }
    public get oid(): string { return this._oid; }
    public get flipSideId(): string { return this._flipSideId; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor( go: GameObject = null )
    {
        super();

        this._type = log_target_type.CARD;

        if ( go )
        {
            console.assert( go.cCard != null || go.cCardToken != null || go.cCardTokenSide != null || go.cAttachment != null || go.cChariotToken != null || go.cGameModifier != null, "LogTargetCard.ts :: constructor() :: CCard, CCardToken, CCardTokenSide, CAttachment, CChariotToken or CGameModifier component not found." );

            this._oid = go.oid;
            if ( go.cCard )
            {
                this._id = go.cCard.curSide.cardId;
                this._flipSideId = go.cCard.curSide.cardId == go.cCard.front.cardId ? go.cCard.back.cardId : go.cCard.front.cardId;
            }
            else if ( go.cCardToken )
            {
                this._id = go.cCardToken.cCurSide.cardId;
                this._flipSideId = go.cCardToken.cCurSide.cardId == go.cCardToken.cFrontSide.cardId ? go.cCardToken.cBackSide.cardId : go.cCardToken.cFrontSide.cardId;
            }
            else if ( go.cCardTokenSide )
            {
                this._id = go.cCardTokenSide.cardId;
                let cCardToken: CCardToken = go.cCardTokenSide.cardToken.cCardToken;
                this._flipSideId = go.cCardTokenSide.cardId == cCardToken.cFrontSide.cardId ? cCardToken.cBackSide.cardId : cCardToken.cFrontSide.cardId;
            }
            else if ( go.cAttachment )
            {
                this._id = go.cAttachment.curSide.cardId;
                this._flipSideId = go.cAttachment.curSide.cardId == go.cAttachment.front.cardId ? go.cAttachment.back.cardId : go.cAttachment.front.cardId;
            }
            else if ( go.cChariotToken )
            {
                this._id = go.cChariotToken.cardId;
            }
            else if ( go.cGameModifier )
            {
                this._id = go.cGameModifier.curSide.cardId;
                this._flipSideId = go.cGameModifier.curSide.cardId == go.cGameModifier.front.cardId ? go.cGameModifier.back.cardId : go.cGameModifier.front.cardId;
            }
        }
    }

    public createIconSprite(): PIXI.Sprite
    {
        return new PIXI.Sprite( Utils.img.createCardPortrait( this._id, { isSquare: true } ) );
    }

    public createPreviewSprite(): PIXI.Sprite
    {
        return new PIXI.Sprite( Utils.img.findCardTexture( this._id ) );
    }

    // overrides.
    public saveGame(): ISgLogTarget
    {
        let logTarget: ISgLogTarget = super.saveGame();
        logTarget.logTargetCard = { oid: this._oid, id: this._id, flipSideId: this._flipSideId };

        return logTarget;
    }

    // overrides.
    public loadGame( sgLogTarget: ISgLogTarget, pass: number ): void
    {
        if ( pass == 0 )
        {
            this._oid = sgLogTarget.logTargetCard.oid;
            this._id = sgLogTarget.logTargetCard.id;
            this._flipSideId = sgLogTarget.logTargetCard.flipSideId;
        }
    }

    // overrides.
    public equals( target: LogTarget ): boolean
    {
        let result: boolean = super.equals( target );
        if ( result )
        {
            result = this._oid == ( target as LogTargetCard ).oid;
        }

        return result;
    }

    // overrides.
    public onPreviewShown(): void
    {
        let go: GameObject = GameObject.find( this._oid );
        if ( go && go.cContainer.c.worldVisible )
        {
            if ( go.cCard )
            {
                go.cCard.setSelectedVfx( true );
            }
            else if ( go.cCardToken )
            {
                go.cCardToken.setSelectedVfx( true );
            }
            else if ( go.cCardTokenSide )
            {
                go.cCardTokenSide.cardToken.cCardToken.setSelectedVfx( true );
            }
            else if ( go.cAttachment )
            {
                let cardToken: GameObject = Utils.game.findGameObjectInBranchByComponentName( go, "CCardToken" );
                if ( cardToken )
                {
                    cardToken.cCardToken.setSelectedVfx( true );
                }
                go.cAttachment.setSelectedVfx( true );
            }
            else if ( go.cChariotToken )
            {
                go.cChariotToken.setSelectedVfx( true );
            }
        }
    }

    // overrides.
    public onPreviewHidden(): void
    {
        let go: GameObject = GameObject.find( this._oid );
        if ( go )
        {
            
            if ( go.cCard )
            {
                go.cCard.setSelectedVfx( false );
            }
            else if ( go.cCardToken )
            {
                go.cCardToken.setSelectedVfx( false );
            }
            else if ( go.cCardTokenSide )
            {
                go.cCardTokenSide.cardToken.cCardToken.setSelectedVfx( false );
            }
            else if ( go.cAttachment )
            {
                let cardToken: GameObject = Utils.game.findGameObjectInBranchByComponentName( go, "CCardToken" );
                if ( cardToken )
                {
                    cardToken.cCardToken.setSelectedVfx( false );
                }
                go.cAttachment.setSelectedVfx( false );
            }
            else if ( go.cChariotToken )
            {
                go.cChariotToken.setSelectedVfx( false );
            }
        }
    }

    // overrides.
    public serialize(): Array<any>
    {
        return [ log_target_type.CARD, this._oid, this._id, this._flipSideId ];
    }

    // overrides.
    public deserialize( serialized: Array<any> ): void
    {
        this._oid = serialized[ 0 ] as string;
        this._id = serialized[ 1 ] as string;
        this._flipSideId = serialized[ 2 ] as string;
    }

    // #endregion //
}