import LogTarget, { log_target_type } from "../LogTarget";

import * as PIXI from "pixi.js";
import ServiceLocator from "../../../../../../ServiceLocator";
import { ISgLogTarget } from "../../../../../../view/game/SaveGameView";


export default class LogTargetDeck extends LogTarget
{
    // #region Attributes //

    // private:

    private _deckType: deck_type = null;

    // #endregion //


    // #region Properties //

    public get deckType(): deck_type { return this._deckType; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor( deckType: deck_type = null )
    {
        super();

        this._type = log_target_type.DECK;
        
        if ( deckType != null )
        {
            this._deckType = deckType;
        }
    }

    public createIconSprite(): PIXI.Sprite
    {
        return this.createDeckSquare( this._deckType );
    }

    public createPreviewSprite(): PIXI.Sprite
    {
        return new PIXI.Sprite( this.findDeckTexture( this._deckType ) );
    }

    // overrides.
    public equals( target: LogTarget ): boolean
    {
        let result: boolean = super.equals( target );
        if ( result )
        {
            result = this._deckType == ( target as LogTargetDeck ).deckType;
        }

        return result;
    }

    // overrides.
    public saveGame(): ISgLogTarget
    {
        let logTarget: ISgLogTarget = super.saveGame();
        logTarget.logTargetDeck = { deckType: this._deckType };

        return logTarget;
    }

    // overrides.
    public loadGame( sgLogTarget: ISgLogTarget, pass: number ): void
    {
        if ( pass == 0 )
        {
            this._deckType = sgLogTarget.logTargetDeck.deckType;
        }
    }

    // overrides.
    public serialize(): Array<any>
    {
        return [ log_target_type.DECK, this._deckType ];
    }

    // overrides.
    public deserialize( serialized: Array<any> ): void
    {
        this._deckType = serialized[ 0 ] as deck_type;
    }

    // private:

    private createDeckSquare( deckType: deck_type ): PIXI.Sprite
    {
        let baseTexture: PIXI.BaseTexture = this.findDeckTexture( deckType ).baseTexture;
        const kSideLength: number = baseTexture.width - 40;

        return new PIXI.Sprite( new PIXI.Texture( 
            baseTexture, 
            new PIXI.Rectangle( 20, ( baseTexture.height - kSideLength ) * 0.5, kSideLength, kSideLength ),
            new PIXI.Rectangle( 0, 0, kSideLength, kSideLength ) ) );
    }

    private findDeckTexture( deckType: deck_type ): PIXI.Texture
    {
        let textureId: string = null;

        switch ( deckType )
        {
            case deck_type.PLAYER: { textureId = "card_player_back"; break; }
            case deck_type.SAURON_0: { textureId = "card_sauron_back"; break; }
            case deck_type.SAURON_1: { textureId = "card_sauron_back"; break; } // TODO:
            case deck_type.SAURON_DISCARD: { textureId = "card_sauron_discard_back"; break; } 
            case deck_type.PLAYER_DISCARD: { textureId = "card_player_discard_back"; break; }
            case deck_type.SAURON_CUSTOM_0: { textureId = "card_sauron_back"; break; } // TODO:
            case deck_type.SAURON_CUSTOM_DISCARD_0: { textureId = "card_sauron_discard_back"; break; } // TODO:
            case deck_type.SAURON_CUSTOM_1: { textureId = "card_sauron_back"; break; } // TODO:
            case deck_type.SAURON_CUSTOM_DISCARD_1: { textureId = "card_sauron_discard_back"; break; } // TODO:
            case deck_type.QUEST_DECK: { textureId = "card_quest_back"; break; } // TODO:
            case deck_type.PLAYER_CUSTOM: { textureId = "card_player_back"; break; } // TODO:
        }

        return PIXI.Texture.from( ServiceLocator.resourceStack.find( textureId ).data );
    }

    // #endregion //
}

export const enum deck_type
{
    PLAYER = 0,
    SAURON_0,
    SAURON_1,
    SAURON_DISCARD,
    PLAYER_DISCARD,
    SAURON_CUSTOM_0,
    SAURON_CUSTOM_DISCARD_0,
    SAURON_CUSTOM_1,
    SAURON_CUSTOM_DISCARD_1,
    QUEST_DECK,
    PLAYER_CUSTOM
}