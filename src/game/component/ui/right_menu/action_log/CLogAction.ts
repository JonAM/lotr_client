import Component from "../../../Component";

import { player_type } from "../../../world/CPlayerArea";
import Session from "../../../../../Session";
import * as PIXI from "pixi.js";

import GameObject from "../../../../GameObject";
import CContainer from "../../../pixi/CContainer";
import { ISgLogAction } from "../../../../../view/game/SaveGameView";
import ServiceLocator from "../../../../../ServiceLocator";


export default abstract class CLogAction extends Component
{
    // #region Attributes //

    // protected:

    protected _who: player_type = null;
    protected _content: GameObject = null;

    protected _poiSocket: GameObject = null;

    // #endregion //


    // #region Properties //

    public get who(): player_type { return this._who; }

    public set who( value: player_type ) { this._who = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CLogAction";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CLogAction.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cHighlightPoiReceptor != null, "CLogAction.ts :: init() :: CHighlightPoiReceptor component not found." );

        this._go.cContainer.c.interactive = true;

        let frame: PIXI.Graphics = new PIXI.Graphics();
        let borderColor: number = 0xa4b0be;
        if ( this._who != null )
        {
            borderColor = ServiceLocator.game.playerColors[ this._who ];
        }
        frame.lineStyle( 1, 0x000000 );
        frame.beginFill( borderColor );
        frame.drawRect( 0, 0, 70, 70 );
        frame.endFill();
        this._go.cContainer.c.addChild( frame );

        this._content = new GameObject( [ new CContainer() ] );
        this._content.init();
        this._content.cContainer.c.position.set( 5 );
        this._go.cContainer.addChild( this._content );

        this._poiSocket = new GameObject( [ new CContainer() ] );
        this._poiSocket.init();
        this._poiSocket.cContainer.c.position.set( this._go.cContainer.c.width * 0.5, 5 );
        this._go.cContainer.addChild( this._poiSocket );
        this._go.cHighlightPoiReceptor.poiSocket = this._poiSocket;
    }

    public end(): void
    {
        this._poiSocket.end();
        this._poiSocket = null;

        this._content.end();
        this._content = null;

        super.end();
    }

    // virtual.
    public createPreview(): GameObject  { return null; }

    // virtual.
    public saveGame(): ISgLogAction
    {
        return { 
            oid: this._go.oid,
            who: this._who };
    }

    public abstract serialize(): Array<any>;
    public abstract deserialize( params: Array<any> ): void;

    // #endregion //
}