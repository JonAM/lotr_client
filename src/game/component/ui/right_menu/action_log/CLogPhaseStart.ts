import CLogAction from "./CLogAction";

import { game_state_id } from "../../../../../states/StateId";
import { log_layout_type } from "../CActionLogger";
import ServiceLocator from "../../../../../ServiceLocator";
import * as PIXI from "pixi.js";
import { ISgLogAction } from "../../../../../view/game/SaveGameView";
import Utils from "../../../../../Utils";


export default class CLogPhaseStart extends CLogAction
{
    // #region Attributes //

    // private:

    private _gameStateId: game_state_id = null;

    // #endregion //


    // #region Properties //

    public set gameStateId( value: game_state_id ) { this._gameStateId = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CLogPhaseStart";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cTextPreviewable != null, "CLogPhaseStart.ts :: init() :: CTextPreviewable component not found." );
        console.assert( this._gameStateId != null, "CLogPhaseStart.ts :: init() :: this._gameStateId cannot be null." );

        let g: PIXI.Graphics = new PIXI.Graphics();
        g.lineStyle( 1, 0x000000 );
        g.beginFill( 0xeccc68 );
        g.drawRect( 0, 0, 60, 60 );
        g.endFill();
        this._content.cContainer.c.addChild( g );

        let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( "ring" ).data ) );
        Utils.game.limitSideSize( 55, icon );
        icon.anchor.set( 0.5 );
        icon.position.set( this._content.cContainer.c.width * 0.5 );
        this._content.cContainer.c.addChild( icon );
        
        let gamePhaseStr: string = null;
        switch ( this._gameStateId )
        {
            case game_state_id.SETUP: 
            { 
                gamePhaseStr = "S"; 
                this._go.cTextPreviewable.text = jQuery.i18n( "SETUP" ); 
                break; 
            }

            case game_state_id.RESOURCE: 
            { 
                gamePhaseStr = "Res"; 
                this._go.cTextPreviewable.text = jQuery.i18n( "RESOURCE" ); 
                break; 
            }

            case game_state_id.PLANNING: 
            { 
                gamePhaseStr = "P"; 
                this._go.cTextPreviewable.text = jQuery.i18n( "PLANNING" ); 
                break; 
            }

            case game_state_id.QUEST: 
            { 
                gamePhaseStr = "Q"; 
                this._go.cTextPreviewable.text = jQuery.i18n( "QUEST" ); 
                break; 
            }

            case game_state_id.TRAVEL: 
            { 
                gamePhaseStr = "T"; 
                this._go.cTextPreviewable.text = jQuery.i18n( "TRAVEL" ); 
                break; 
            }

            case game_state_id.ENCOUNTER: 
            { 
                gamePhaseStr = "E"; 
                this._go.cTextPreviewable.text = jQuery.i18n( "ENCOUNTER" ); 
                break; 
            }

            case game_state_id.COMBAT: 
            { 
                gamePhaseStr = "C"; 
                this._go.cTextPreviewable.text = jQuery.i18n( "COMBAT" ); 
                break; 
            }

            case game_state_id.REFRESH: 
            { 
                gamePhaseStr = "Ref"; 
                this._go.cTextPreviewable.text = jQuery.i18n( "REFRESH" ); 
                break; 
            }
        }

        let text: PIXI.Text = new PIXI.Text( gamePhaseStr, ServiceLocator.game.textStyler.subtitle );
        text.anchor.set( 0.5, 1 );
        text.position.set( this._content.cContainer.c.width * 0.5, this._content.cContainer.c.height );
        this._content.cContainer.c.addChild( text );
    }

    // overrides.
    public saveGame(): ISgLogAction
    {
        let logAction: ISgLogAction = super.saveGame();
        logAction.logPhaseStart = { gameStateId: this._gameStateId };

        return logAction;
    }

    // overrides.
    public serialize(): Array<any>
    {
        return [ log_layout_type.PHASE_START, this._gameStateId ];
    }

    // overrides.
    public deserialize( params: Array<any> ): void
    {
        this._gameStateId = params[ 0 ] as game_state_id;
    }

    // #endregion //
}