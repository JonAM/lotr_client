import Component from "../../Component";

import GameObject from "../../../GameObject";
import { game_state_id } from "../../../../states/StateId";
import { player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import { player_type } from "../../world/CPlayerArea";
import ServiceLocator from "../../../../ServiceLocator";
import { TweenMax } from "gsap";
import Utils from "../../../../Utils";
import * as PIXI from "pixi.js";

import CContainer from "../../pixi/CContainer";
import CScrollY from "../CScrollY";
import CLogActivePlayer from "./action_log/CLogActivePlayer";
import CLogPhaseStart from "./action_log/CLogPhaseStart";
import CGraphics from "../../pixi/CGraphics";
import CTextPreviewable from "../../input/CTextPreviewable";
import CLogActionPreviewable from "../../input/CLogActionPreviewable";
import { IOppActionListener, IOpponentAction } from "../../../AllyActionManager";
import LogTarget from "./action_log/LogTarget";
import CLogSingleIcon from "./action_log/CLogSingleIcon";
import CLogSequence from "./action_log/CLogSequence";
import CLogCounter from "./action_log/CLogCounter";
import Signal from "../../../../lib/signals/Signal";
import { ISgActionLogger, ISgLogAction } from "../../../../view/game/SaveGameView";
import CHighlightPoiReceptor from "../../world/poi_receptor/CHighlightPoiReceptor";
import CLogTargetSelection from "./action_log/CLogTargetSelection";
import LogTargetCard from "./action_log/target/LogTargetCard";
import CLogCardFlip from "./action_log/CLogCardFlip";
import CButton from "../../input/CButton";
import CSprite from "../../pixi/CSprite";
import CTooltipReceptor from "../CTooltipReceptor";


export default class CActionLogger extends Component implements IOppActionListener
{
    // #region Attributes //

    // private:

    private _bg: PIXI.Graphics = null;
    private _last: GameObject = null;
    private _history: GameObject = null;

    private _toggleVisibilityBtn: GameObject = null;

    private static _gLogActionCount: number = 0;

    // Signals.
    private _onActionLogged: Signal = new Signal();

    // #endregion //


    // #region Properties //

    // Signals.
    public get onActionLogged(): Signal { return this._onActionLogged; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CActionLogger";
    }

    public init(): void
    {
        super.init();

        CActionLogger._gLogActionCount = 0;

        // Bg.
        this._bg = new PIXI.Graphics();
        this._go.cContainer.c.addChild( this._bg );

        // Last action.
        this._last = new GameObject( [ new CGraphics() ] );
        this._last.init();
        this._last.cGraphics.g.lineStyle( 1, 0x000000, 1 );
        this._last.cGraphics.g.beginFill( 0xeccc68, 1 );
        this._last.cGraphics.g.drawRect( 0, 0, 70, 70 );
        this._last.cGraphics.g.endFill();
        this._last.cContainer.c.pivot.set( 35 );
        this._last.cContainer.c.position.set( 45, 45 );
        this._bg.addChild( this._last.cContainer.c );

        // History.
        this._history = new GameObject( [ new CContainer(), new CScrollY() ] );
        this._history.cScrollY.width = 60;
        const kScrollYH: number = 420; // 7 items.
        this._history.cScrollY.height = kScrollYH;
        this._history.cScrollY.itemHeight = 60;
        this._history.init();
        this._history.cContainer.c.position.set( 
            this._last.cContainer.c.x - 30, 
            this._last.cContainer.c.y + this._last.cContainer.c.height * 0.5 );
        this._bg.addChildAt( this._history.cContainer.c, 0 );

        // Bg.
        this._bg.lineStyle( 1, 0x000000, 1 );
        this._bg.beginFill( 0xEFE3AF );
        //this._bg.drawRect( 0, 0, 90, 330 );
        this._bg.lineTo( 90, 0 );
        this._bg.lineTo( 90, 90 );
        this._bg.lineTo( 85, 90 );
        this._bg.lineTo( 85, kScrollYH + 90 );
        this._bg.lineTo( 5, kScrollYH + 90 );
        this._bg.lineTo( 5, 90 );
        this._bg.lineTo( 0, 90 );
        this._bg.closePath();
        this._bg.endFill();

        // Toggle visibility button.
        this._toggleVisibilityBtn = new GameObject( [ new CGraphics(), new CButton(), new CTooltipReceptor() ] );
        this._toggleVisibilityBtn.cGraphics.g.lineStyle( 1, 0x000000, 1 );
        this._toggleVisibilityBtn.cGraphics.g.beginFill( 0xeccc68 );
        this._toggleVisibilityBtn.cGraphics.g.drawCircle( 0, 0, 15 );
        this._toggleVisibilityBtn.cGraphics.g.endFill();
        this._toggleVisibilityBtn.cTooltipReceptor.text = jQuery.i18n( "HIDE_ACTION_LOG" );
        this._toggleVisibilityBtn.init();
        this._toggleVisibilityBtn.cGraphics.g.position.set( this._bg.width - 15, this._bg.height + 20 );
        this._go.cContainer.addChild( this._toggleVisibilityBtn );
        this._toggleVisibilityBtn.cButton.onClick.add( this.onToggleVisibilityBtn_Click, this );
        // Icon
        let eyeIcon: PIXI.Sprite = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( "eye" ) );
        Utils.game.limitSideSize( 20, eyeIcon );
        eyeIcon.anchor.set( 0.5, 0.5 );
        this._toggleVisibilityBtn.cGraphics.g.addChild( eyeIcon );
        // Red line.
        let redLine: PIXI.Graphics = new PIXI.Graphics();
        redLine.name = "red_line";
        redLine.lineStyle( 8, 0xff0000, 0.8 );
        redLine.moveTo( -15, -15 );
        redLine.lineTo( 15, 15 );
        this._toggleVisibilityBtn.cGraphics.g.addChild( redLine );
        // // Mask.
        let mask: PIXI.Graphics = new PIXI.Graphics();
        mask.beginFill();
        mask.drawCircle( 0, 0, 14 );
        mask.endFill();
        this._toggleVisibilityBtn.cGraphics.g.addChild( mask );
        redLine.mask = mask;

        // Listen to events.
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.LOG_ACTION ] );
    }

    public end(): void
    {
        this._onActionLogged.removeAll();

        this._toggleVisibilityBtn.end();
        this._toggleVisibilityBtn = null;

        this._last.end();
        this._last = null;

        this._history.end();
        this._history = null;

        // Cleanup events.
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.LOG_ACTION ] );

        super.end();
    }

    public logPhaseStart( gameStateId: game_state_id ): void
    {
        if ( !this._isEnabled ) { return; }

        let logAction: GameObject = new GameObject( [ new CContainer(), new CLogPhaseStart(), new CTextPreviewable(), new CHighlightPoiReceptor() ] );   
        logAction.oid = CActionLogger.nextLogActionOid();
        logAction.cLogPhaseStart.gameStateId = gameStateId;
        logAction.init();

        this.addToLog( logAction, false );
    }

    public logActivePlayer( playerType: player_type ): void
    {
        if ( !this._isEnabled ) { return; }

        let logAction: GameObject = new GameObject( [ new CContainer(), new CLogActivePlayer(), new CTextPreviewable(), new CHighlightPoiReceptor() ] );   
        logAction.oid = CActionLogger.nextLogActionOid();
        logAction.cLogActivePlayer.playerType = playerType;
        logAction.init();

        this.addToLog( logAction, false );
    }

    public logSingleIcon( who: player_type, target: LogTarget, iconId: string, isMultiplayer: boolean = false ): void
    {
        if ( !this._isEnabled ) { return; }

        let logAction: GameObject = new GameObject( [ new CContainer(), new CLogSingleIcon(), new CLogActionPreviewable(), new CHighlightPoiReceptor() ] );   
        logAction.oid = CActionLogger.nextLogActionOid();
        logAction.cLogAction.who = who;
        logAction.cLogSingleIcon.target = target;
        logAction.cLogSingleIcon.iconId = iconId;
        logAction.init();

        this.addToLog( logAction, isMultiplayer );
    }

    public logSequence( who: player_type, target: LogTarget, fromIconId: string, toIconId: string, isMultiplayer: boolean = false ): void
    {
        if ( !this._isEnabled ) { return; }

        let logAction: GameObject = new GameObject( [ new CContainer(), new CLogSequence(), new CLogActionPreviewable(), new CHighlightPoiReceptor() ] );   
        logAction.oid = CActionLogger.nextLogActionOid();
        logAction.cLogAction.who = who;
        logAction.cLogSequence.target = target;
        logAction.cLogSequence.fromIconId = fromIconId;
        logAction.cLogSequence.toIconId = toIconId;
        logAction.init();

        this.addToLog( logAction, isMultiplayer );
    }

    public logCounter( who: player_type, target: LogTarget, iconId: string, count: number, total: number, isMultiplayer: boolean = false ): void
    {
        if ( !this._isEnabled ) { return; }

        let logAction: GameObject = new GameObject( [ new CContainer(), new CLogCounter(), new CLogActionPreviewable(), new CHighlightPoiReceptor() ] );   
        logAction.oid = CActionLogger.nextLogActionOid();
        logAction.cLogAction.who = who;
        logAction.cLogCounter.target = target;
        logAction.cLogCounter.iconId = iconId;
        logAction.cLogCounter.count = count;
        logAction.cLogCounter.total = total;
        logAction.init();

        this.addToLog( logAction, isMultiplayer );
    }

    public logTargetSelection( who: player_type, from: LogTarget, to: LogTarget, contextIconId: string, isReverse: boolean, isMultiplayer: boolean = false ): void
    {
        if ( !this._isEnabled ) { return; }
        
        let logAction: GameObject = new GameObject( [ new CContainer(), new CLogTargetSelection(), new CLogActionPreviewable(), new CHighlightPoiReceptor() ] );   
        logAction.oid = CActionLogger.nextLogActionOid();
        logAction.cLogAction.who = who;
        logAction.cLogTargetSelection.from = from;
        logAction.cLogTargetSelection.to = to;
        logAction.cLogTargetSelection.contextIconId = contextIconId;
        logAction.cLogTargetSelection.isReverse = isReverse;
        logAction.init();

        this.addToLog( logAction, isMultiplayer );
    }

    public logCardFlip( who: player_type, target: LogTargetCard, isMultiplayer: boolean = false ): void
    {
        if ( !this._isEnabled ) { return; }
        
        let logAction: GameObject = new GameObject( [ new CContainer(), new CLogCardFlip(), new CLogActionPreviewable(), new CHighlightPoiReceptor() ] );   
        logAction.oid = CActionLogger.nextLogActionOid();
        logAction.cLogAction.who = who;
        logAction.cLogCardFlip.target = target;
        logAction.init();

        this.addToLog( logAction, isMultiplayer );
    }

    public saveGame(): ISgActionLogger
    {
        let arrLogAction: Array<ISgLogAction> = new Array<ISgLogAction>();
        let isLast: boolean = this._last.cContainer.c.children.length > 0;
        if ( isLast )
        {
            arrLogAction.push( ( this._last.cContainer.c.children[ 0 ][ "go" ] as GameObject ).cLogAction.saveGame() );
        }
        for ( let logAction of this._history.cScrollY.findItems() )
        {
            arrLogAction.push( logAction.cLogAction.saveGame() );
        }

        return {
            isLast: isLast,
            history: arrLogAction,
            isVisible: this._bg.visible };
    }

    public loadGame( sgActionLogger: ISgActionLogger, pass: number ): void
    {
        if ( pass == 0 )
        {
            for ( let i: number = sgActionLogger.history.length - 1; i >= 0; --i )
            {
                let sgLogAction: ISgLogAction = sgActionLogger.history[ i ];
                if ( sgLogAction.logActivePlayer )
                {
                    this.logActivePlayer( sgLogAction.logActivePlayer.playerType );
                }
                else if ( sgLogAction.logTargetSelection )
                {
                    let from: LogTarget = Utils.actionLog.deserializeTarget( [ sgLogAction.logTargetSelection.from.type ] );
                    from.loadGame( sgLogAction.logTargetSelection.from, pass );
                    let to: LogTarget = Utils.actionLog.deserializeTarget( [ sgLogAction.logTargetSelection.to.type ] );
                    to.loadGame( sgLogAction.logTargetSelection.to, pass );
                    this.logTargetSelection( sgLogAction.who, from, to, sgLogAction.logTargetSelection.contextIconId, sgLogAction.logTargetSelection.isReverse, false );
                }
                else if ( sgLogAction.logCounter )
                {
                    let target: LogTarget = Utils.actionLog.deserializeTarget( [ sgLogAction.logCounter.target.type ] );
                    target.loadGame( sgLogAction.logCounter.target, pass );
                    this.logCounter( sgLogAction.who, target, sgLogAction.logCounter.iconId, sgLogAction.logCounter.count, sgLogAction.logCounter.total, false );
                }
                else if ( sgLogAction.logPhaseStart )
                {
                    this.logPhaseStart( sgLogAction.logPhaseStart.gameStateId );
                }
                else if ( sgLogAction.logSequence )
                {
                    let target: LogTarget = Utils.actionLog.deserializeTarget( [ sgLogAction.logSequence.target.type ] );
                    target.loadGame( sgLogAction.logSequence.target, pass );
                    this.logSequence( sgLogAction.who, target, sgLogAction.logSequence.fromIconId, sgLogAction.logSequence.toIconId, false ); 
                }
                else if ( sgLogAction.logSingleIcon )
                {
                    let target: LogTarget = Utils.actionLog.deserializeTarget( [ sgLogAction.logSingleIcon.target.type ] );
                    target.loadGame( sgLogAction.logSingleIcon.target, pass );
                    this.logSingleIcon( sgLogAction.who, target, sgLogAction.logSingleIcon.iconId, false );
                }
                else if ( sgLogAction.logCardFlip )
                {
                    let target: LogTarget = Utils.actionLog.deserializeTarget( [ sgLogAction.logCardFlip.target.type ] );
                    target.loadGame( sgLogAction.logCardFlip.target, pass );
                    this.logCardFlip( sgLogAction.who, target as LogTargetCard, false );
                }
            }

            if ( !sgActionLogger.isLast )
            {
                let lastLogAction: GameObject = null;
                if ( this._last.cContainer.c.children.length > 0 )
                {
                    lastLogAction = this._last.cContainer.c.children[ 0 ][ "go" ] as GameObject;
                    this.pushDownLog( lastLogAction );
                }
            }

            this._bg.visible = sgActionLogger.isVisible;
            let redLine: PIXI.Container = this._toggleVisibilityBtn.cContainer.c.getChildByName( "red_line" ) as PIXI.Container;
            redLine.visible = this._bg.visible;
        }
    }

    // private:

    private addToLog( logAction: GameObject, isMultiplayer: boolean = false ): void
    {
        // Multiplayer.
        if ( isMultiplayer )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.LOG_ACTION, null, logAction.cLogAction.serialize() );
        }

        let lastLogAction: GameObject = null;
        if ( this._last.cContainer.c.children.length > 0 )
        {
            lastLogAction = this._last.cContainer.c.children[ 0 ][ "go" ] as GameObject;
        }

        if ( logAction.cLogCounter 
            && lastLogAction 
            && lastLogAction.cLogCounter
            && lastLogAction.cLogCounter.equals( logAction.cLogCounter ) )
        {
            logAction.cLogCounter.count += lastLogAction.cLogCounter.count;
            if ( logAction.cLogCounter.count == 0 )
            {
                logAction.end();
                logAction = null;

                const kArrHistoryItem: Array<GameObject> = this._history.cScrollY.findItems();
                if ( kArrHistoryItem.length > 0 )
                {
                    logAction = kArrHistoryItem[ 0 ];
                    logAction.cContainer.c.scale.set( 1 );
                    this._history.cScrollY.removeItemAt( 0 );
                }
            }
            else
            {
                logAction.cLogCounter.reinit();
            }

            lastLogAction.end();
            lastLogAction = null;

            this._onActionLogged.dispatch( logAction );
        }

        if ( lastLogAction )
        {
            const kArrHistoryItem: Array<GameObject> = this._history.cScrollY.findItems();
            if ( kArrHistoryItem.length > 100 )
            {
                this._history.cScrollY.removeItemAt( this._history.cScrollY.findItems().length - 1 );
            }
            this.pushDownLog( lastLogAction );

            this._onActionLogged.dispatch( logAction );
        }

        if ( logAction )
        {
            this._last.cContainer.addChild( logAction );
        }

        this.animate();
    }

    private pushDownLog( lastLogAction: GameObject ): void
    {
        Utils.game.limitSideSize( 60, lastLogAction.cContainer.c )
        this._history.cScrollY.addItemAt( lastLogAction, 0 );
        lastLogAction.cContainer.onTransform_Changed();
    }

    private animate(): void
    {
        if ( this._last.cContainer.c.worldVisible )
        {
            Utils.anim.flushTweensOf( this._last.cContainer.c.scale );
            TweenMax.to( this._last.cContainer.c.scale, 0.15, { x: "+=0.15", y: "+=0.15", yoyo: true, repeat: 1 } );
        }
    }

    private static nextLogActionOid(): string
    {
        let result: string = "loc_action_" + this._gLogActionCount.toString();

        this._gLogActionCount += 1;

        return result;
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.LOG_ACTION )
        {
            let logAction: GameObject = Utils.actionLog.deserializeAction( action.args, CActionLogger.nextLogActionOid() );
            this.addToLog( logAction, false );
        }
    }

    // #endregion //


    // #region Input Callbacks //

    private onToggleVisibilityBtn_Click(): void
    {
        let redLine: PIXI.Container = this._toggleVisibilityBtn.cContainer.c.getChildByName( "red_line" ) as PIXI.Container;
        redLine.visible = !redLine.visible;

        this._bg.visible = redLine.visible;
        this._toggleVisibilityBtn.cTooltipReceptor.text = jQuery.i18n( redLine.visible ? "HIDE_ACTION_LOG" : "SHOW_ACTION_LOG" );
    }

    // #endregion //
}

export const enum log_layout_type
{
    PHASE_START = 0,
    ACTIVE_PLAYER,
    SINGLE_ICON,
    SEQUENCE,
    COUNTER,
    TARGET_SELECTION,
    CARD_FLIP
}