import Component from "../Component";

import { player_type } from "../world/CPlayerArea";
import { poi_menu_item_type } from "./CPoiMenuItem";
import ServiceLocator from "../../../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import PoiFactory from "../../PoiFactory";
import CGraphics from "../pixi/CGraphics";
import CPoiMenuItem from "./CPoiMenuItem";
import CTooltipReceptor from "./CTooltipReceptor";
import { layer_type } from "../world/CGameLayerProvider";


export default class CPoiMenu extends Component
{
    // #region Attributes //

    // private:

    private _selPoiItem: GameObject = null;
    private _poiReceptor: GameObject = null;
    private _bg: PIXI.Graphics = null;
    private _arrMenuContainer: Array<PIXI.Container> = null;

    // Constants.
    private readonly _kMenuItemRadius: number = 30;
    private readonly _kCircleRadius: number = 120;

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();
    
        this._id = "CPoiMenu";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CPoiMenu.ts :: init() :: CContainer component not found." );
    
        this._arrMenuContainer = new Array<PIXI.Container>();

        this._bg = new PIXI.Graphics();
        this._go.cContainer.c.addChild( this._bg );

        this._go.cContainer.c.visible = false;
    }

    public show( poiReceptor: GameObject, position: PIXI.Point ): void
    {   
        let arrPoiMenuItem: Array<IPoiMenuItem> = poiReceptor.cPoiReceptor.findPoiMenuItems();
        if ( arrPoiMenuItem )
        {
            this._go.cContainer.c.position.set( position.x, position.y );

            this._poiReceptor = poiReceptor;
            this._poiReceptor.cContainer.onTransformChanged.addOnce( this.hide, this );
            this._poiReceptor.onEnded.addOnce( this.hide, this );
            if ( this._poiReceptor.cCardToken )
            {
                this._poiReceptor.cCardToken.onFlipped.addOnce( this.hide, this );
            }

            this._bg.removeChildren();

            let mainMenu: PIXI.Container = this.createMainMenu( arrPoiMenuItem );
            this._bg.addChild( mainMenu );
            this._arrMenuContainer.push( mainMenu );

            this._go.cContainer.c.visible = true;

            // Listen to events.
            ServiceLocator.game.onOngoingInteractionCanceled.add( this.hide, this );
        }
    }

    public hide()
    {
        if ( !this._go.cContainer.c.visible ) { return; }

        if ( this._selPoiItem )
        {
            if ( !this._selPoiItem.cPoiMenuItem.submenu )
            {
                let poiFactory: PoiFactory = new PoiFactory();
                let poi: GameObject = poiFactory.create( 
                    this._poiReceptor, 
                    poiFactory.generateOid( this._poiReceptor ), 
                    this._selPoiItem.cPoiMenuItem.findPoiType(), 
                    this._selPoiItem.cPoiMenuItem.createPoiParams(), 
                    player_type.PLAYER, 
                    player_type.ALLY,
                    this._poiReceptor );
                poi.cPoi.show( this._poiReceptor, player_type.PLAYER );
            }

            this._selPoiItem = null;
        }

        this._poiReceptor.cContainer.onTransformChanged.remove( this.hide, this );
        this._poiReceptor.onEnded.remove( this.hide, this );
        if ( this._poiReceptor.cCardToken )
        {
            this._poiReceptor.cCardToken.onFlipped.remove( this.hide, this );
        }
        this._poiReceptor = null;

        for ( let menuContainer of this._arrMenuContainer )
        {
            while ( menuContainer.children.length > 0 )
            {
                ( menuContainer.children[ 0 ][ "go" ] as GameObject ).end();
            }
        }
        this._arrMenuContainer.splice( 0 );

        this._bg.removeChildren();

        this._go.cContainer.c.visible = false;

        // Cleanup events.
        ServiceLocator.game.onOngoingInteractionCanceled.remove( this.hide, this );
    }

    // private:

    private createMainMenu( arrMenuItem: Array<IPoiMenuItem> ): PIXI.Container
    {
        let result: PIXI.Container = new PIXI.Container();

        const kStartingPosition: PIXI.Point = new PIXI.Point( 0, -this._kCircleRadius );
        const kRotationAngle: number = this.findSubmenuRotationAngle( kStartingPosition );
        const kArrPosition: Array<PIXI.Point> = this.findButtonPositions( arrMenuItem.length, kStartingPosition, kRotationAngle );
        
        this._bg.clear();
        this._bg.lineStyle( 2, 0xFEEB77, 1 );
        this._bg.beginFill( 0x650A5A, 0.75 );
        for ( let i: number = 0; i < kArrPosition.length; ++i )
        {
            this._bg.lineTo( kArrPosition[ i ].x, kArrPosition[ i ].y );
        }
        this._bg.closePath();
        this._bg.endFill();

        result = this.createSubmenu( arrMenuItem, kArrPosition, null );

        return result;
    }

    private createSubmenu( arrMenuItem: Array<IPoiMenuItem>, arrPosition: Array<PIXI.Point>, parentNode: CPoiMenuItem ): PIXI.Container
    {
        let result: PIXI.Container = new PIXI.Container();

        for ( let i: number = 0; i < arrPosition.length; ++i )
        {
            let menuItem: GameObject = this.createMenuItem( arrMenuItem[ i ], this._arrMenuContainer.length, parentNode );
            menuItem.cContainer.c.position.copyFrom( arrPosition[ i ] );
            result.addChild( menuItem.cContainer.c );
            menuItem.cPoiMenuItem.onPointerOver.add( this.onMenuItem_PointerOver, this );
            menuItem.cPoiMenuItem.onPointerOut.add( this.onMenuItem_PointerOut, this );
        }

        return result;
    }

    private findButtonPositions( buttonCount: number, startingPosition: PIXI.Point, rotationAngle: number ): Array<PIXI.Point>
    {
        let result: Array<PIXI.Point> = new Array<PIXI.Point>();

        let g: PIXI.Point = this._go.cContainer.c.getGlobalPosition();
        let gameLayerBounds: PIXI.Rectangle = ServiceLocator.game.root.cGameLayerProvider.get( layer_type.GAME ).cContainer.c.getBounds().clone();
        gameLayerBounds.width = gameLayerBounds.width - 60;
        gameLayerBounds.x = 30;
        gameLayerBounds.height = gameLayerBounds.height - 60;
        gameLayerBounds.y = 30;
        let candidatePosition: PIXI.Point = startingPosition.clone();
        let m: PIXI.Matrix = new PIXI.Matrix();
        m.rotate( rotationAngle );
        do 
        {
            if ( gameLayerBounds.contains( g.x + candidatePosition.x, g.y + candidatePosition.y ) )
            {
                result.push( candidatePosition.clone() );
            }
            else
            {
                result.splice( 0 );
            }

            if ( result.length < buttonCount )
            {
                candidatePosition = m.apply( candidatePosition );
            }
        } 
        while ( result.length < buttonCount );
        
        return result;
    }

    private createMenuItem( poiMenuItem: IPoiMenuItem, menuLevel: number, parentNode: CPoiMenuItem ): GameObject
    {
        let menuItem: GameObject = new GameObject( [ new CGraphics(), new CPoiMenuItem(), new CTooltipReceptor() ] );
        menuItem.cPoiMenuItem.type = poiMenuItem.type;
        menuItem.cPoiMenuItem.menuLevel = menuLevel;
        menuItem.cPoiMenuItem.radius = this._kMenuItemRadius;
        if ( poiMenuItem.submenu != undefined )
        {
            menuItem.cPoiMenuItem.submenu = poiMenuItem.submenu;
        }
        menuItem.cPoiMenuItem.parent = parentNode;
        const kArrTooltip: Array<string> = this.findPoiTooltip( poiMenuItem.type );
        menuItem.cTooltipReceptor.text = kArrTooltip[ 0 ];
        menuItem.cTooltipReceptor.description = kArrTooltip[ 1 ];
        menuItem.init();

        return menuItem;
    }

    private findPoiTooltip( optionType: poi_menu_item_type ): Array<string>
    {
        let result: Array<string> = [ null, null ];

        switch ( optionType )
        {
            case poi_menu_item_type.HIGHLIGHT: { result[ 0 ] = jQuery.i18n( "HIGHLIGHT" ); break; }
            case poi_menu_item_type.BOW: { result[ 0 ] = jQuery.i18n( "BOW" ); break; }
            case poi_menu_item_type.READY: { result[ 0 ] = jQuery.i18n( "READY" ); break; }
            case poi_menu_item_type.GIVE_CONTROL: { result[ 0 ] = jQuery.i18n( "GIVE_CONTROL" ); break; }
            case poi_menu_item_type.DISCARD: { result[ 0 ] = jQuery.i18n( "DISCARD" ); break; }
            case poi_menu_item_type.REVEAL: { result[ 0 ] = jQuery.i18n( "REVEAL" ); break; }
            case poi_menu_item_type.RANDOMLY_DISCARD: { result[ 0 ] = jQuery.i18n( "RANDOMLY_DISCARD" ); break; }
            case poi_menu_item_type.CARD_TOKEN_PROFILE_SUBMENU: { result[ 0 ] = jQuery.i18n( "CHARACTER_ATTRIBUTES" ); break; }
            case poi_menu_item_type.ONE: { result[ 0 ] = jQuery.i18n( "ONE" ); break; }
            case poi_menu_item_type.TWO: { result[ 0 ] = jQuery.i18n( "TWO" ); break; }
            case poi_menu_item_type.THREE: { result[ 0 ] = jQuery.i18n( "THREE" ); break; }
            case poi_menu_item_type.FOUR: { result[ 0 ] = jQuery.i18n( "FOUR" ); break; }
            case poi_menu_item_type.FIVE: { result[ 0 ] = jQuery.i18n( "FIVE" ); break; }
            case poi_menu_item_type.ATTACK: { result[ 0 ] = jQuery.i18n( "ATTACK" ); break; }
            case poi_menu_item_type.DEFENSE: { result[ 0 ] = jQuery.i18n( "DEFENSE" ); break; }
            case poi_menu_item_type.WILLPOWER: { result[ 0 ] = jQuery.i18n( "WILLPOWER" ); break; }
            case poi_menu_item_type.THREAT: { result[ 0 ] = jQuery.i18n( "THREAT" ); break; }
            case poi_menu_item_type.ADD: { result[ 0 ] = jQuery.i18n( "ADD" ); break; }
            case poi_menu_item_type.REMOVE: { result[ 0 ] = jQuery.i18n( "REMOVE" ); break; }
            case poi_menu_item_type.GIVE: { result[ 0 ] = jQuery.i18n( "GIVE" ); break; }
        }

        return result;
    }

    private findSubmenuStartingPosition( menuItem: PIXI.Container ): PIXI.Point
    {
        let startingPosition: PIXI.Point = new PIXI.Point( menuItem.position.x, menuItem.position.y );
        const kVectorModulus: number = Math.sqrt( startingPosition.x * startingPosition.x + startingPosition.y * startingPosition.y );
        const kUnitVector: PIXI.Point = new PIXI.Point( startingPosition.x / kVectorModulus, startingPosition.y / kVectorModulus );
        startingPosition.x += kUnitVector.x * this._kMenuItemRadius * 2;
        startingPosition.y += kUnitVector.y * this._kMenuItemRadius * 2;

        return startingPosition;
    }

    private findSubmenuRotationAngle( startingPosition: PIXI.Point ): number
    {
        const kVectorModulus: number = Math.sqrt( Math.pow( startingPosition.x, 2 ) + Math.pow( startingPosition.y, 2 ) );
        return 2 * Math.acos( kVectorModulus / ( Math.sqrt( kVectorModulus * kVectorModulus + this._kMenuItemRadius * this._kMenuItemRadius ) ) );
    }

    // #endregion //


    // #region Input Callbacks //

    private onMenuItem_PointerOver( cPoiMenuItem: CPoiMenuItem ): void
    {
        for ( let i: number = this._arrMenuContainer.length - 1; i > cPoiMenuItem.menuLevel; --i )
        {
            let menuContainer: PIXI.Container = this._arrMenuContainer[ i ];
            while( menuContainer.children.length > 0 )
            {
                ( menuContainer.children[ 0 ][ "go" ] as GameObject ).end();
            }
            menuContainer.parent.removeChild( menuContainer );
            this._arrMenuContainer.splice( i, 1 );
        }

        for ( let displayObject of this._arrMenuContainer[ cPoiMenuItem.menuLevel ].children )
        {
            displayObject.alpha = displayObject == cPoiMenuItem.go.cContainer.c || !cPoiMenuItem.submenu ? 1 : 0.5;
        }

        if ( cPoiMenuItem.submenu )
        {
            const kStartingPosition: PIXI.Point = this.findSubmenuStartingPosition( cPoiMenuItem.go.cContainer.c );
            const kRotationAngle: number = this.findSubmenuRotationAngle( kStartingPosition );
            const kArrPosition: Array<PIXI.Point> = this.findButtonPositions( cPoiMenuItem.submenu.length, kStartingPosition, kRotationAngle );
            let submenuContainer: PIXI.Container = this.createSubmenu( cPoiMenuItem.submenu, kArrPosition, cPoiMenuItem );
            this._bg.addChild( submenuContainer );
            this._arrMenuContainer.push( submenuContainer );
        }

        this._selPoiItem = cPoiMenuItem.go;
    }

    private onMenuItem_PointerOut( cPoiMenuItem: CPoiMenuItem ): void
    {
        this._selPoiItem = null;
    }

    // #endregion //
}

export interface IPoiMenuItem
{
    type: poi_menu_item_type;
    submenu?: Array<IPoiMenuItem>;
}