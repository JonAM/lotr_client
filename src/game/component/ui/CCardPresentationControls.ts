import Component from "../Component";

import { button_type } from "../../../states/game/setup/CardPresentationUtils";
import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CTooltipReceptor from "./CTooltipReceptor";
import CGraphics from "../pixi/CGraphics";
import CButton from "../input/CButton";
import SignalBinding from "../../../lib/signals/SignalBinding";
import Signal from "../../../lib/signals/Signal";


export default class CCardPresentationControls extends Component
{
    // #region Attributes //

    // private:

    private _arrButton: Array<GameObject> = null;

    // Signals.
    private _onButtonClick: Signal = new Signal();

    // #endregion //


    // Signals.
    public get onButtonClick(): Signal { return this._onButtonClick; }


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CCardPresentationControls";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CCardPresentationControls.ts :: init() :: CContainer component not found." );

        this._arrButton = new Array<GameObject>();

        const kArrIconId: Array<string> = [ "hand", "rad_play", "rad_target", "rad_equip", "rad_underneath", "rad_shadow_card", "rad_discard", "rad_back" ];
        const kArrTooltipId: Array<string> = [ "DRAW", "PLAY", "TRIGGER_ABILITY", "EQUIP", "PUT_UNDERNEATH", "ADD_AS_SHADOW_CARD", "DISCARD", "BACK" ];
        for ( let i: button_type = button_type.DRAW; i <= button_type.CANCEL; ++i )
        {
            let button: GameObject = this.createContextBtn( kArrIconId[ i ], kArrTooltipId[ i ] );
            button.cContainer.c.x = 30;
            button.cContainer.c.visible = false;
            this._go.cContainer.addChild( button );
            let sb: SignalBinding = button.cButton.onClick.add( this.onButton_Click, this );
            sb.params = [ i ];

            this._arrButton.push( button );
        }
    }

    public end(): void
    {
        this._onButtonClick.removeAll();

        for ( let button of this._arrButton )
        {
            button.end();
        }
        this._arrButton = null;

        super.end();
    }

    public draw( arrButtonType: Array<button_type> ): void
    {
        for ( let button of this._arrButton )
        {
            button.cContainer.c.visible = false;
        }

        let buttonIndex: number = 0;
        for ( let buttonType of arrButtonType )
        {
            let button: GameObject = this._arrButton[ buttonType ];
            button.cContainer.c.y = 30 + buttonIndex * 80;
            button.cContainer.c.visible = true;

            ++buttonIndex;
        }
    }

    // private:

    private createContextBtn( iconId: string, tooltipId: string ): GameObject
    {
        let result: GameObject = null;

        result = new GameObject( [ new CGraphics(), new CButton(), new CTooltipReceptor() ] );
        result.cGraphics.g.lineStyle( 1, 0x000000, 1 );
        result.cGraphics.g.beginFill( 0xeccc68, 1 );
        result.cGraphics.g.drawCircle( 0, 0, 30 );
        result.cGraphics.g.endFill();
        let icon: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( iconId ) );
        Utils.game.limitSideSize( 40, icon );
        icon.anchor.set( 0.5 );
        result.cContainer.c.addChild( icon );
        result.cTooltipReceptor.text = jQuery.i18n( tooltipId );
        result.init();

        return result;
    }

    // #endregion //


    // #region Callbacks //

    private onButton_Click( buttonType: button_type ): void
    {
        this._onButtonClick.dispatch( buttonType );
    }
}