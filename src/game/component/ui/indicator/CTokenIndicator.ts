import CIndicator from "../CIndicator";

import ServiceLocator from "../../../../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "../../../GameObject";
import CContainer from "../../pixi/CContainer";
import CTokenCounter from "../CTokenCounter";
import CDropArea from "../../input/CDropArea";
import CDraggable from "../../input/CDraggable";
import { ISgTokenIndicator } from "../../../../view/game/SaveGameView";
import { player_type } from "../../world/CPlayerArea";
import { action_scope_type } from "../../../../service/socket_io/GameSocketIOController";


export default class CTokenIndicator extends CIndicator
{
    // #region Attributes //

    // private:

    private _tokenName: string = null;
    private _poiSocket: GameObject = null;

    // #endregion //


    // #region Properties //

    public set tokenName( value: string ) { this._tokenName = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CTokenIndicator";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CTokenIndicator.ts :: init() :: CContainer component not found." );

        this._trigger = new GameObject( [ new CContainer(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._trigger.oid = this._go.oid + "_trigger";
        this._trigger.cTokenCounter.tokenName = this._tokenName;
        this._trigger.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( this._iconId ).data );
        this._trigger.cDropArea.target = this._trigger.cTokenCounter;
        this._trigger.cDraggable.anchor = new PIXI.Point( 0.5, 0 );
        this._trigger.init();
        this._go.cContainer.addChild( this._trigger );
        this._trigger.cDropArea.setEnabled( this._isDropArea );
        this._trigger.cTokenCounter.onCountUpdated.add( this.onCount_Updated, this );

        super.createTriggerBox();

        if ( this._go.cTokenIndicatorPoiReceptor )
        {
            // POI socket.
            this._poiSocket = new GameObject( [ new CContainer() ] );
            this._poiSocket.init();
            this._go.cContainer.addChild( this._poiSocket );
            this._poiSocket.cContainer.c.position.set( 0, 5 );
            this._go.cTokenIndicatorPoiReceptor.poiSocket = this._poiSocket;
        }
    }

    public end(): void
    {
        if ( this._poiSocket )
        {
            this._poiSocket.end();
            this._poiSocket = null;
        }

        super.end();
    }

    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        this._trigger.cTokenCounter.setEnabled( isEnabled );
        this._trigger.cDropArea.setEnabled( isEnabled );
        this._trigger.cDraggable.setEnabled( isEnabled );
    }

    // overrides.
    public setIconId( iconId: string ): void
    {
        super.setIconId( iconId );

        if ( iconId )
        {
            this._trigger.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( this._iconId ).data );
        }
    }

    // overrides.
    public saveGame(): ISgTokenIndicator
    {
        let result: ISgTokenIndicator = { count: this._trigger.cTokenCounter.count };

        if ( this._go.cIndicatorLock )
        {
            result.lock = this._go.cIndicatorLock.saveGame();
        }

        return result;
    }

    // overrides.
    public loadGame( sgTokenIndicator: ISgTokenIndicator, pass: number ): void
    {
        if ( this._go.cIndicatorLock )
        {
            this._go.cIndicatorLock.loadGame( sgTokenIndicator.lock, pass );
        }
        this._trigger.cTokenCounter.setCount( sgTokenIndicator.count, action_scope_type.LOCAL );
    }

    // #endregion //
}