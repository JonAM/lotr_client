import Component from "../../Component";

import ServiceLocator from "../../../../ServiceLocator";
import * as PIXI from "pixi.js";
import Utils from "../../../../Utils";
import { ISgIndicatorLock } from "../../../../view/game/SaveGameView";


export default class CIndicatorLock extends Component
{
    // #region Attributes //

    // private:

    private _lockType: lock_type = lock_type.LOCK;

    private _lock: PIXI.Graphics = null;

    // #endregion //


    // #region Properties //

    public set lockType( value: lock_type ) { this._lockType = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CIndicatorLock";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CIndicatorLock.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cIndicator != null, "CIndicatorLock.ts :: init() :: CIndicator component not found." );
    }

    public isLocked(): boolean
    {
        return this._lock && this._lock.visible;
    }

    public setLocked( isLocked: boolean ): void
    {
        if ( !this._lock )
        {
            let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( this._lockType == lock_type.LOCK ? "lock" : "info_check" ).data ) );
            Utils.game.limitSideSize( 15, icon );
            icon.anchor.set( 0.5, 0.5 );
            
            this._lock = new PIXI.Graphics();
            this._lock.lineStyle( 1, 0x000000, 1 );
            this._lock.beginFill( this._lockType == lock_type.LOCK ? 0xffff00 : 0x008b00 );
            this._lock.drawCircle( 0, 0, 10 );
            this._lock.endFill();
            this._lock.position.set( 16, 40 );
            this._lock.addChild( icon );
            this._go.cContainer.c.addChild( this._lock );
        }

        this._lock.visible = isLocked;

        this._go.cIndicator.trigger.cContainer.c.interactive = !isLocked;
        this._go.cIndicator.trigger.cContainer.c.buttonMode = !isLocked;
        this._go.cIndicator.trigger.cDropArea.setEnabled( !isLocked && this._go.cIndicator.isDropArea );
        if ( this._go.cIndicator.trigger.cDraggable )
        {
            this._go.cIndicator.trigger.cDraggable.setEnabled( !isLocked );
        }
    }

    public setLockType( lockType: lock_type ): void
    {
        this._lockType = lockType;
        if ( this._lock )
        {
            this._lock.clear();
            this._lock.lineStyle( 1, 0x000000, 1 );
            this._lock.beginFill( this._lockType == lock_type.LOCK ? 0xffff00 : 0x008b00 );
            this._lock.drawCircle( 0, 0, 10 );
            this._lock.endFill();

            this._lock.removeChildren();
            let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( this._lockType == lock_type.LOCK ? "lock" : "info_check" ).data ) );
            Utils.game.limitSideSize( 15, icon );
            icon.anchor.set( 0.5, 0.5 );
            this._lock.addChild( icon );
        }
    }

    // overrides.
    public saveGame(): ISgIndicatorLock
    {
        return {
            lockType: this._lockType,
            isLocked: this._lock && this._lock.visible };
    }

    // overrides.
    public loadGame( sgIndicatorLock: ISgIndicatorLock, pass: number ): void
    {
        this.setLockType( sgIndicatorLock.lockType );
        this.setLocked( sgIndicatorLock.isLocked );
    }

    // #endregion //
}

export const enum lock_type
{
    LOCK = 0,
    READY 
}