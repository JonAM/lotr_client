import Component from "../../Component";

import ServiceLocator from "../../../../ServiceLocator";
import * as PIXI from "pixi.js";
import Utils from "../../../../Utils";
import { player_type } from "../../world/CPlayerArea";


export default class CIndicatorBusy extends Component
{
    // #region Attributes //

    // private:

    private _bg: PIXI.Graphics = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CIndicatorBusy";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CIndicatorBusy.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cIndicator != null, "CIndicatorBusy.ts :: init() :: CIndicator component not found." );
    }

    public isBusy(): boolean
    {
        return this._bg && this._bg.visible;
    }

    public setBusy( isBusy: boolean ): void
    {
        if ( !this._bg )
        {
            let icon: PIXI.Sprite = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( "eye" ) );
            Utils.game.limitSideSize( 15, icon );
            icon.anchor.set( 0.5, 0.5 );
            
            this._bg = new PIXI.Graphics();
            this._bg.lineStyle( 1, 0x000000, 1 );
            this._bg.beginFill( ServiceLocator.game.playerColors[ player_type.ALLY ] );
            this._bg.drawCircle( 0, 0, 10 );
            this._bg.endFill();
            this._bg.position.set( 16, 15 );
            this._bg.addChild( icon );
            this._go.cContainer.c.addChild( this._bg );
        }

        this._bg.visible = isBusy;
    }

    // #endregion //
}