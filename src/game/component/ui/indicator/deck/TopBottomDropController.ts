import IGameObjectDropArea from "../../../../IGameObjectDropArea";

import { deck_side_type } from "../../viewer/CCardView";
import { player_type } from "../../../world/CPlayerArea";
import ServiceLocator from "../../../../../ServiceLocator";

import GameObject from "../../../../GameObject";
import CCardView from "../../viewer/CCardView";
import { action_scope_type } from "../../../../../service/socket_io/GameSocketIOController";
import { layer_type } from "../../../world/CGameLayerProvider";


export default class TopBottomDropController implements IGameObjectDropArea
{
    // #region Attributes //

    // private:

    private _cCardView: CCardView = null;

    // #endregion //


    // #region Properties //

    public set cardView( value: CCardView ) { this._cCardView = value; }

    // #endregion //


    // #region Methods //

    public init(): void
    {
        console.assert( this._cCardView != null, "TopBottomDropController.ts :: init() :: this._cCardView cannot be null." );
    }

    public end(): void
    {
        this._cCardView = null;
    }

    // #endregion //


    // #region IGameObjectDropArea //

    public validateDroppedGameObject( token: GameObject ): boolean
    {
        return this._cCardView.validateDroppedGameObject( token );
    }

    public processDroppedGameObject( card: GameObject, global: PIXI.IPoint, actionScopeType: action_scope_type ): void
    {
        ServiceLocator.game.root.cGameLayerProvider.add( card, layer_type.UNDER_TABLE );

        card.cContainer.c.visible = false;
        ServiceLocator.game.cContextMenu.open( global, [ 
            { title: jQuery.i18n( "TOP" ), cb: this.onDropTop_Selected.bind( this, card, actionScopeType ) },
            { title: jQuery.i18n( "BOTTOM" ), cb: this.onDropBottom_Selected.bind( this, card, actionScopeType ) } ] );
    }

    // #endregion //


    // #region Callbacks //

    public onDropTop_Selected( card: GameObject, actionScopeType: action_scope_type ): void
    {
        card.cContainer.c.visible = true;

        this._cCardView.processDroppedGameObject( card, CCardView.findPredefinedDropPosition( deck_side_type.TOP ), actionScopeType );
    }

    public onDropBottom_Selected( card: GameObject, actionScopeType: action_scope_type ): void
    {
        card.cContainer.c.visible = true;

        this._cCardView.processDroppedGameObject( card, CCardView.findPredefinedDropPosition( deck_side_type.BOTTOM ), actionScopeType );
    }

    // #endregion //
}