import CIndicator from "../CIndicator";

import * as PIXI from "pixi.js";

import GameObject from "../../../GameObject";
import CContainer from "../../pixi/CContainer";
import CDropArea from "../../input/CDropArea";
import CButton from "../../input/CButton";
import Signal from "../../../../lib/signals/Signal";
import CViewer from "../CViewer";
import CDragDetector from "../../input/CDragDetector";
import { player_type } from "../../world/CPlayerArea";
import ServiceLocator from "../../../../ServiceLocator";
import { action_scope_type } from "../../../../service/socket_io/GameSocketIOController";
import { location_type } from "../../world/CGameWorld";
import CCardView, { deck_side_type } from "../viewer/CCardView";
import CardViewerController from "../viewer/controller/CardViewerController";
import CLogLocation from "../../world/CLogLocation";
import Session from "../../../../Session";
import SignalBinding from "../../../../lib/signals/SignalBinding";


export default class CDeckIndicator extends CIndicator
{
    // #region Attributes //

    // private:

    private _panel: GameObject = null;
    private _poiSocket: GameObject = null;
    private _isOpenOnClick: boolean = true;

    // Signals.
    private _onSelected: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get panel(): GameObject { return this._panel; }
    public get cViewer(): CViewer { return this._panel.cViewer; }

    public set panel( value: GameObject ) { this._panel = value; }
    public set isOpenOnClick( value: boolean ) { this._isOpenOnClick = value; }

    // Signals.
    public get onSelected(): Signal { return this._onSelected; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CDeckIndicator";
    }

    public init(): void
    {
        super.init();

        console.assert( this._panel != null, "CDeckIndicator.ts :: init() :: this._panel cannot be null." );

        this._trigger = new GameObject( [ new CContainer(), new CButton(), new CDropArea(), new CDragDetector(), new CLogLocation() ] );
        this._trigger.cDropArea.target = this._panel.cViewer.cCardView;
        this._trigger.cLogLocation.location = this._panel.cViewer.location;
        this._trigger.init();
        this._go.cContainer.addChild( this._trigger );
        this._trigger.cDropArea.setEnabled( this._isDropArea );
        this._trigger.cButton.onClick.add( this.onDeckButton_Click, this );
        if ( this._panel.cViewer.cCardView.playerType == player_type.PLAYER
            || this._panel.cViewer.cCardView.playerType == player_type.SAURON )
        {
            this._trigger.cDragDetector.onDragged.add( this.onDeckTrigger_Dragged, this );
        }

        super.createTriggerBox();

        if ( this._go.cDeckIndicatorPoiReceptor )
        {
            // POI socket.
            this._poiSocket = new GameObject( [ new CContainer() ] );
            this._poiSocket.init();
            this._poiSocket.cContainer.c.position.set( 0, 5 );
            this._go.cContainer.addChild( this._poiSocket );
            this._go.cDeckIndicatorPoiReceptor.poiSocket = this._poiSocket;
        }
        
        // Listen to events.
        this._panel.cViewer.cCardView.onItemCountUpdated.add( this.onCount_Updated, this );
        this._panel.cViewer.onOpened.add( this.onViewer_Opened, this );
        this._panel.cViewer.onClosed.add( this.onViewer_Closed, this );
    }

    public end(): void
    {
        this._panel.end();
        this._panel = null;

        if ( this._poiSocket )
        {
            this._poiSocket.end();
            this._poiSocket = null;
        }

        super.end();
    }

    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        this._trigger.cButton.setEnabled( isEnabled );
        this._trigger.cDropArea.setEnabled( isEnabled );
        this._trigger.cDragDetector.setEnabled( isEnabled );
    }

    // #endregion //


    // #region Callbacks //

    public onDeckButton_Click(): void
    {
        if ( this._go.cIndicatorBusy && this._go.cIndicatorBusy.isBusy() ) { return; }

        if ( this._isOpenOnClick )
        {
            this._panel.cViewer.toggle();
        }
        else
        {
            this._onSelected.dispatch();
        }
    }

    private onDeckTrigger_Dragged( dragged: GameObject, event: PIXI.InteractionEvent ): void
    {
        if ( this._go.cIndicatorBusy && this._go.cIndicatorBusy.isBusy() ) { return; }

        // Multiplayer.
        if ( Session.allyId && this._go.cShareableGameElement && this._go.cShareableGameElement.isEnabled )
        {
            let sb: SignalBinding = this._go.cShareableGameElement.onLockOpen.addOnce( this.dragCard, this );
            sb.params = [ event ];
            this._go.cShareableGameElement.notifyLock();
        }
        else
        {
            this.dragCard( event );
        }
    }

        private dragCard(  event: PIXI.InteractionEvent ): void
        {
            let cardCount: number = this._panel.cViewer.controller.findItemCount();
            if ( cardCount == 0 && this._panel.cViewer.location == location_type.SAURON_DECK_0 )
            {
                // Run out of cards.
                // Put discarded cards back on deck.
                let cDiscardViewer: CViewer = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.discard.cDeckIndicator.cViewer;
                let discardChildren: Array<PIXI.DisplayObject> = cDiscardViewer.cCardView.itemHolder.cContainer.c.children;
                for ( let i: number = discardChildren.length - 1; i >= 0; --i )
                {
                    let discardedCard: GameObject = discardChildren[ i ][ "go" ] as GameObject;
                    this._panel.cViewer.cCardView.go.cDropArea.forceDrop( discardedCard, CCardView.findPredefinedDropPosition( deck_side_type.TOP ), action_scope_type.MULTIPLAYER );
                }

                ( this._panel.cViewer.controller as CardViewerController ).shuffle( action_scope_type.MULTIPLAYER );    
            
                cardCount = this._panel.cViewer.controller.findItemCount();
            }

            if ( cardCount > 0 )
            {
                let card: GameObject = this._panel.cViewer.cCardView.itemHolder.cContainer.c.children[ cardCount - 1 ][ "go" ] as GameObject;
                if ( Session.allyId && !this._panel.cViewer.isVisible() && this._go.cShareableGameElement )
                {
                    let cancelIndicatorBusy: () => void = () => { this._go.cShareableGameElement.notifyUnlock(); };
                    card.cDraggable.onDropped.add( cancelIndicatorBusy, this );
                    card.cDraggable.onDragCancelled.add( cancelIndicatorBusy, this );
                }
                //
                let fakeEvent: PIXI.InteractionEvent = new PIXI.InteractionEvent();
                fakeEvent.data = new PIXI.InteractionData();
                fakeEvent.data.global = card.cContainer.c.getGlobalPosition().clone();
                card.cDraggable.forceDrag( fakeEvent );
                
                ServiceLocator.game.dragShadowManager.dragShadow.cContainer.c.position.set(
                    event.data.global.x - ServiceLocator.game.dragShadowManager.dragShadow.cDragShadow.from.cDraggable.offset.x * ServiceLocator.game.dragShadowManager.dragShadow.cContainer.c.width, 
                    event.data.global.y - ServiceLocator.game.dragShadowManager.dragShadow.cDragShadow.from.cDraggable.offset.y * ServiceLocator.game.dragShadowManager.dragShadow.cContainer.c.height );
                ServiceLocator.game.dragShadowManager.dragShadow.cDampedSpring.reset();
                ServiceLocator.game.dragShadowManager.dragShadow.cDampedSpring.setTarget( event.data.global );
            
                ServiceLocator.game.dragShadowManager.forceStagePointerMove();
            }
        }

    private onViewer_Opened(): void
    {
        if ( Session.allyId && this._go.cShareableGameElement )
        {
            // Multiplayer.
            this._go.cShareableGameElement.notifyLock();
        }
    }

    private onViewer_Closed(): void
    {
        if ( Session.allyId && this._go.cShareableGameElement )
        {
            // Multiplayer.
            this._go.cShareableGameElement.notifyUnlock();
        }
    }

    // #endregion //
}