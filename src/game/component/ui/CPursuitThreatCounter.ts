import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CTokenCounter from "./CTokenCounter";
import CGraphics from "../pixi/CGraphics";
import CDropArea from "../input/CDropArea";
import CDraggable from "../input/CDraggable";
import { player_type } from "../world/CPlayerArea";
import CButton from "../input/CButton";
import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import { ISgStagingThreatCounter } from "../../../view/game/SaveGameView";
import { IOppActionListener, IOpponentAction } from "../../AllyActionManager";
import CContainer from "../pixi/CContainer";


export default class CPursuitThreatCounter extends Component implements IOppActionListener
{
    // #region Attributes //

    // private:

    private _staging: GameObject = null;
    
    private _counter: GameObject = null;
    private _icon: PIXI.Sprite = null;
    private _text: PIXI.Text = null;
    private _autoBtn: GameObject = null;
    private _autoCount: number = null;
    private _poiSocket: GameObject = null;

    // #endregion //


    // #region Properties //

    public get counter(): GameObject { return this._counter; }

    public set staging( value: GameObject ) { this._staging = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CPursuitThreatCounter";
    }
    
    public init(): void
    {
        super.init();

        console.assert( this._staging != null, "CPursuitThreatCounter.ts :: init() :: this._staging cannot be null." );

        this._autoCount = 0;

        // Counter.
        this._counter = new GameObject( [ new CGraphics(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._counter.oid = this._go.oid + "_counter";
        this._counter.cGraphics.g.lineStyle( 2, ServiceLocator.game.playerColors[ player_type.SAURON ], 1 );
        this._counter.cGraphics.g.beginFill( ServiceLocator.game.playerColors[ player_type.SAURON ], 0.4 );
        this._counter.cGraphics.g.drawCircle( 0, 0, 30 );
        this._counter.cGraphics.g.endFill();
        this._counter.cTokenCounter.tokenName = "threat";
        this._counter.cTokenCounter.count = 0;
        this._counter.cTokenCounter.draggedTexture = ServiceLocator.resourceStack.findAsTexture( "threat" );
        this._counter.cDropArea.target = this._counter.cTokenCounter;
        this._counter.init();
        this._go.cContainer.addChild( this._counter );
        
        // Icon.
        this._icon = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( "threat" ) );
        Utils.game.limitSideSize( 30, this._icon );
        this._icon.anchor.set( 0.5, 1 );
        this._icon.y = 5;
        this._go.cContainer.c.addChild( this._icon );
        
        // Text.
        this._text = new PIXI.Text( "0", ServiceLocator.game.textStyler.normal );
        this._text.anchor.set( 0.5, 0 );
        this._go.cContainer.c.addChild( this._text );

        // Auto button.
        this._autoBtn = new GameObject( [ new CGraphics(), new CButton() ] );
        this._autoBtn.cGraphics.g.lineStyle( 1, 0x000000, 1 );
        this._autoBtn.cGraphics.g.beginFill( 0x00008b );
        this._autoBtn.cGraphics.g.drawCircle( 0, 0, 15 );
        this._autoBtn.cGraphics.g.endFill();
        let autoIcon: PIXI.Sprite = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( "auto" ) );
        Utils.game.limitSideSize( 20, autoIcon );
        autoIcon.anchor.set( 0.5, 0.5 );
        this._autoBtn.cGraphics.g.addChild( autoIcon );
        this._autoBtn.init();
        this._autoBtn.cGraphics.g.position.set( 
            this._go.cContainer.c.x + 20,
            this._go.cContainer.c.y - 20 );
        this._autoBtn.cContainer.c.visible = false;
        this._go.cContainer.addChild( this._autoBtn );

        // POI socket.
        this._poiSocket = new GameObject( [ new CContainer() ] );
        this._poiSocket.cContainer.c.name = "poi_socket";
        this._poiSocket.init();
        this._poiSocket.cContainer.c.y -= this._counter.cContainer.c.height * 0.5 - 5;
        this._go.cContainer.addChild( this._poiSocket );
        //
        this._go.cHighlightPoiReceptor.poiSocket = this._poiSocket;

        // Listen to events.
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.SET_PURSUIT_THREAT_COUNTER_AUTO, player_action_type.SHOW_PURSUIT_THREAT_COUNTER_AUTO ] );
        this._autoBtn.cButton.onClick.add( this.onAutoBtn_Click, this );
        this._counter.cTokenCounter.onCountUpdated.add( this.onCounter_Updated, this );
        this._staging.cSauronActorArea.onTokenSet.add( this.onStagingAreaToken_Set, this );
        this._staging.cSauronActorArea.onTokenUnset.add( this.onStagingAreaToken_Unset, this );
    }

    public end(): void
    {
        // Cleanup events.
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.SET_PURSUIT_THREAT_COUNTER_AUTO, player_action_type.SHOW_PURSUIT_THREAT_COUNTER_AUTO ] );
        this._staging.cSauronActorArea.onTokenSet.remove( this.onStagingAreaToken_Set, this );
        this._staging.cSauronActorArea.onTokenUnset.remove( this.onStagingAreaToken_Unset, this );
            
        this._text = null;
        this._icon = null;

        this._counter.end();
        this._counter = null;

        this._autoBtn.end();
        this._autoBtn = null;

        this._poiSocket.end();
        this._poiSocket = null;

        super.end();
    }

    // overrides.
    public saveGame(): ISgStagingThreatCounter
    {
        return {
            count: this._counter.cTokenCounter.count,
            isAuto: !this._autoBtn.cContainer.c.visible };
    }

    // overrides.
    public loadGame( sgStagingThreatCounter: ISgStagingThreatCounter, pass: number ): void
    {  
        if ( sgStagingThreatCounter.isAuto )
        {
            this.forceAutoBtnClick();
        }
        else
        {
            this._counter.cTokenCounter.setCount( sgStagingThreatCounter.count, action_scope_type.LOCAL );
            this._autoBtn.cContainer.c.visible = true;
        }
    }

    // private:

    private forceAutoBtnClick(): void
    {
        this._autoBtn.cContainer.c.visible = false;

        this.calcAutoCount();
        this._counter.cTokenCounter.setCount( this._autoCount, action_scope_type.LOCAL );
    }

        private calcAutoCount(): void
        {
            this._autoCount = 0;
            let arrCardToken: Array<GameObject> = this._staging.cSauronActorArea.findAllActors();
            for ( let cardToken of arrCardToken )
            {
                if ( cardToken.cCardToken.curSide.cLocationSide )
                {
                    this._autoCount += cardToken.cCardToken.curSide.cLocationSide.threat.cTokenCounter.count;
                }
                else if ( cardToken.cCardToken.curSide.cEnemySide )
                {
                    this._autoCount += cardToken.cCardToken.curSide.cEnemySide.threat.cTokenCounter.count;
                }
            }
        }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.SET_PURSUIT_THREAT_COUNTER_AUTO )
        {
           this.forceAutoBtnClick();
        }
        else if ( action.playerActionType == player_action_type.SHOW_PURSUIT_THREAT_COUNTER_AUTO )
        {
           this._autoBtn.cContainer.c.visible = true;
        }
    }

    // #endregion //


    // #region Callbacks //

    private onCounter_Updated( count: number, delta: number, isPlayerInput: boolean, actionScope: action_scope_type ): void
    {
        this._text.text = count.toString();

        if ( isPlayerInput )
        {
            this._autoBtn.cContainer.c.visible = true;

            // Multiplayer.
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SHOW_PURSUIT_THREAT_COUNTER_AUTO, null, null );
        }

        if ( count > this._autoCount )
        {
            this._text.tint = 0x00ff00;
        }
        else if ( count < this._autoCount )
        {
            this._text.tint = 0xff0000;
        }
        else
        {
            this._text.tint = 0xffffff;
        }

        CTokenCounter.animate( this._counter.cContainer.c.parent, delta );

        /*if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetPlayer( player_type.PLAYER ), "threat", delta, true );
        }*/
    }

    private onStagingAreaToken_Set( cardToken: GameObject ): void
    {
        this.trackThreatEvents( cardToken.cCardToken.curSide );

        cardToken.cCardToken.onFlipped.add( this.onCardToken_Flipped, this );
    }

        private trackThreatEvents( cardTokenSide: GameObject ): void
        {
            let threat: number = null;
            if ( cardTokenSide.cEnemySide )
            {
                cardTokenSide.cEnemySide.threat.cTokenCounter.onCountUpdated.add(  this.onCardTokenThreat_Updated, this );
                threat = cardTokenSide.cEnemySide.threat.cTokenCounter.count;
            }
            else if ( cardTokenSide.cLocationSide )
            {
                cardTokenSide.cLocationSide.threat.cTokenCounter.onCountUpdated.add(  this.onCardTokenThreat_Updated, this );
                threat = cardTokenSide.cLocationSide.threat.cTokenCounter.count;
            }

            if ( threat != null )
            {
                this._autoCount += threat;
                if ( !this._autoBtn.cContainer.c.visible )
                {
                    this._counter.cTokenCounter.setCount( this._autoCount, action_scope_type.LOCAL );
                }
            }
        }

    private onStagingAreaToken_Unset( cardToken: GameObject ): void
    {
        this.untrackThreatEvents( cardToken.cCardToken.curSide );
        
        cardToken.cCardToken.onFlipped.remove( this.onCardToken_Flipped, this );
    }

        private untrackThreatEvents( cardTokenSide: GameObject ): void
        {
            let threat: number = null;
            if ( cardTokenSide.cEnemySide )
            {
                cardTokenSide.cEnemySide.threat.cTokenCounter.onCountUpdated.remove(  this.onCardTokenThreat_Updated, this );
                threat = cardTokenSide.cEnemySide.threat.cTokenCounter.count;
            }
            else if ( cardTokenSide.cLocationSide )
            {
                cardTokenSide.cLocationSide.threat.cTokenCounter.onCountUpdated.remove(  this.onCardTokenThreat_Updated, this );
                threat = cardTokenSide.cLocationSide.threat.cTokenCounter.count;
            }
    
            if ( threat != null )
            {
                this._autoCount -= threat;
                if ( !this._autoBtn.cContainer.c.visible )
                {
                    this._counter.cTokenCounter.setCount( this._autoCount, action_scope_type.LOCAL );
                }
            }
        }

    private onAutoBtn_Click(): void
    {
        this.forceAutoBtnClick();

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_PURSUIT_THREAT_COUNTER_AUTO, null, null );
    }

    private onCardTokenThreat_Updated( count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._autoCount += delta;
        if ( !this._autoBtn.cContainer.c.visible )
        {
            this._counter.cTokenCounter.setCount( this._autoCount, action_scope_type.LOCAL );
        }
    }

    private onCardToken_Flipped( frontSide: GameObject, backSide: GameObject ): void
    {
        this.untrackThreatEvents( backSide );
        this.trackThreatEvents( frontSide );
    }

    // #endregion //
}