import Component from "../Component";

import Signal from "../../../lib/signals/Signal";


export default class CAnchorY extends Component
{
    // #region Attributes //

    // private:

    private _child: IContentUpdater = null;

    private _anchorY: number = 0;

    // #endregion //


    // #region Properties //

    public get child() { return this._child; }
    
    public set child( value: IContentUpdater ) { this._child = value; }
    public set anchorY( value: number ) { this._anchorY = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CAnchorY";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CAnchorY.ts :: init() :: CContainer component not found." );
        console.assert( this._child != null, "CAnchorY.ts :: init() :: this._child cannot be null." );
    
        this._go.cContainer.addChild( ( this._child as any as Component ).go );
        this.onChildContentUpdated();

        // Listen to events.
        this._child.onContentUpdated.add( this.onChildContentUpdated, this );
    }

    public end(): void
    {
        this._child = null;
    }

    // #endregion //


    // #region Callbacks //

    public onChildContentUpdated(): void
    {
        //this._go.cContainer.c.pivot.y = this._anchorY * this._go.cContainer.c.height;
        this._go.cContainer.c.pivot.y = this._anchorY * ( this._child as any as Component ).go.cContainer.c.height;
    }

    // #endregion //
}

export interface IContentUpdater
{
    readonly onContentUpdated: Signal;
}