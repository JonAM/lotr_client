import Component from "../Component";

import { location_type } from "../world/CGameWorld";
import { player_type } from "../world/CPlayerArea";
import ServiceLocator from "../../../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CContainer from "../pixi/CContainer";
import CDeckIndicator from "./indicator/CDeckIndicator";
import CIndicatorLock from "./indicator/CIndicatorLock";
import CardViewerFactory from "../../CardViewerFactory";
import TopBottomDropController from "./indicator/deck/TopBottomDropController";
import CDeckIndicatorPoiReceptor from "../world/poi_receptor/CDeckIndicatorPoiReceptor";
import { face_policy_type } from "./viewer/CCardView";
import CTooltipReceptor from "./CTooltipReceptor";
import CSprite from "../pixi/CSprite";
import SauronDeckController from "./viewer/controller/SauronDeckController";
import SauronDiscardController from "./viewer/controller/SauronDiscardController";
import VictoryDisplayController from "./viewer/controller/VictoryDisplayController";
import RemovedFromGameController from "./viewer/controller/RemovedFromGameController";
import CViewer from "./CViewer";
import CCardTrayView from "./viewer/CCardTrayView";
import CardTrayController from "./viewer/controller/CardTrayController";
import CAnchorY from "./CAnchorY";
import CIndicatorBusy from "./indicator/CIndicatorBusy";
import { ISgCard, ISgSauronSideControls } from "../../../view/game/SaveGameView";
import CDropArea from "../input/CDropArea";
import { action_scope_type } from "../../../service/socket_io/GameSocketIOController";
import { IScenario } from "../../ScenarioDB";
import Session from "../../../Session";
import CustomDeckController from "./viewer/controller/CustomDeckController";
import { layer_type } from "../world/CGameLayerProvider";
import QuestDeckController from "./viewer/controller/QuestDeckController";
import CGraphics from "../pixi/CGraphics";
import CShareableGameElement from "../CShareableGameElement";
import { context_button_type } from "./viewer/CViewContextBtn";


export default class CSauronSideControls extends Component
{
    // #region Attributes //

    // private:

    private _bg: GameObject = null;
    private _deck: GameObject = null;
    private _secondaryDeck: GameObject = null;
    private _discard: GameObject = null;
    private _customDeck: GameObject = null;
    private _customDiscard: GameObject = null;
    private _secondaryCustomDeck: GameObject = null;
    private _secondaryCustomDiscard: GameObject = null;
    private _questDeck: GameObject = null;
    private _questDiscard: GameObject = null;
    private _removedFromGame: GameObject = null;
    private _victoryDisplay: GameObject = null;
    private _setAside: GameObject = null;
    private _tray: GameObject = null;

    // #endregion //


    // #region Properties //

    public get deck(): GameObject { return this._deck; }
    public get secondaryDeck(): GameObject { return this._secondaryDeck; }
    public get discard(): GameObject { return this._discard; }
    public get customDeck(): GameObject { return this._customDeck; }
    public get customDiscard(): GameObject { return this._customDiscard; }
    public get secondaryCustomDeck(): GameObject { return this._secondaryCustomDeck; }
    public get secondaryCustomDiscard(): GameObject { return this._secondaryCustomDiscard; }
    public get questDeck(): GameObject { return this._questDeck; }
    public get questDiscard(): GameObject { return this._questDiscard; }
    public get removedFromGame(): GameObject { return this._removedFromGame; }
    public get victoryDisplay(): GameObject { return this._victoryDisplay; }
    public get setAside(): GameObject { return this._setAside; }
    public get tray(): GameObject { return this._tray; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CSauronSideControls";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CSauronSideControls.ts :: init() :: CContainer component not found." );

        this._bg = new GameObject( [ new CGraphics() ] );
        this._go.cContainer.addChild( this._bg );

        let arrRow: Array<Array<GameObject>> = new Array<Array<GameObject>>();
        let arrIndicator: Array<GameObject> = new Array<GameObject>();

        // Top.
        this._discard = this.createDiscardIndicator();
        this._bg.cContainer.addChild( this._discard );
        arrIndicator.push( this._discard );

        this._deck = this.createDeckIndicator();
        this._bg.cContainer.addChild( this._deck );
        arrIndicator.push( this._deck );

        arrRow.push( arrIndicator );

        const kScenario: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
        if ( kScenario.play_area && kScenario.play_area.sauron && kScenario.play_area.sauron.secondary_encounter_deck )
        {
            arrIndicator = new Array<GameObject>();

            this._secondaryDeck = this.createSecondaryDeckIndicator( jQuery.i18n( kScenario.play_area.sauron.secondary_encounter_deck.titleI18n ) );
            this._bg.cContainer.addChild( this._secondaryDeck );
            arrIndicator.push( null, this._secondaryDeck );

            arrRow.push( arrIndicator );
        }

        if ( kScenario.play_area && kScenario.play_area.sauron && kScenario.play_area.sauron.custom_deck )
        {
            arrIndicator = new Array<GameObject>();

            if ( kScenario.play_area.sauron.custom_discard )
            {
                this._customDiscard = this.createCustomDiscardIndicator( jQuery.i18n( kScenario.play_area.sauron.custom_discard.titleI18n ) );
                this._bg.cContainer.addChild( this._customDiscard );
                arrIndicator.push( this._customDiscard );
            }

            this._customDeck = this.createCustomDeckIndicator( jQuery.i18n( kScenario.play_area.sauron.custom_deck.titleI18n ) );
            this._bg.cContainer.addChild( this._customDeck );
            arrIndicator.push( this._customDeck );

            arrRow.push( arrIndicator );
        }

        if ( kScenario.play_area && kScenario.play_area.sauron && kScenario.play_area.sauron.secondary_custom_deck )
        {
            arrIndicator = new Array<GameObject>();

            if ( kScenario.play_area.sauron.secondary_custom_discard )
            {
                this._secondaryCustomDiscard = this.createSecondaryCustomDiscardIndicator( jQuery.i18n( kScenario.play_area.sauron.secondary_custom_discard.titleI18n ) );
                this._bg.cContainer.addChild( this._secondaryCustomDiscard );
                arrIndicator.push( this._secondaryCustomDiscard );
            }

            this._secondaryCustomDeck = this.createSecondaryCustomDeckIndicator( jQuery.i18n( kScenario.play_area.sauron.secondary_custom_deck.titleI18n ) );
            this._bg.cContainer.addChild( this._secondaryCustomDeck );
            arrIndicator.push( this._secondaryCustomDeck );

            arrRow.push( arrIndicator );
        }
        
        arrIndicator = new Array<GameObject>();
        //
        this._questDiscard = this.createQuestDiscardIndicator();
        this._bg.cContainer.addChild( this._questDiscard );
        arrIndicator.push( this._questDiscard );
        //
        this._questDeck = this.createQuestDeckIndicator();
        this._bg.cContainer.addChild( this._questDeck );
        arrIndicator.push( this._questDeck );
        //
        arrRow.push( arrIndicator );

        // Position items.
        const kItemMargin: number = 15;
        let usedSpace: number = kItemMargin * ( arrRow.length - 1 );
        for ( let row of arrRow )
        {
            let maxHeight: number = 0;
            for ( let indicator of row )
            {
                if ( indicator )
                {
                    maxHeight = Math.max( maxHeight, indicator.cContainer.c.height );
                }
            }
            usedSpace += maxHeight;
        }
        // Draw bg.
        const kTopBgHeight: number = usedSpace + 10 + 20;
        const kTopY: number = -( 10 + kTopBgHeight );
        this._bg.cGraphics.g.lineStyle( 4, 0x000000, 1 );
        this._bg.cGraphics.g.beginFill( 0xbcb183, 1 ); 
        this._bg.cGraphics.g.drawRect( 0, kTopY, 20, kTopBgHeight );
        this._bg.cGraphics.g.endFill();
        this._bg.cGraphics.g.beginFill( 0xefe4b0, 1 ); 
        this._bg.cGraphics.g.moveTo( 20, kTopY );
        this._bg.cGraphics.g.lineTo( 70, kTopY );
        this._bg.cGraphics.g.lineTo( 70, -( 10 + 20 ) );
        this._bg.cGraphics.g.lineTo( 20, -10 );
        this._bg.cGraphics.g.lineTo( 20, kTopY );
        this._bg.cGraphics.g.endFill();
        //
        let itemY: number = kTopY + 10;
        for ( let row of arrRow )
        {
            let maxHeight: number = 0;
            for ( let indicator of row )
            {
                if ( indicator )
                {
                    maxHeight = Math.max( maxHeight, indicator.cContainer.c.height );
                }
            }
            for ( let i: number = row.length - 1; i >= 0; --i )
            {
                let indicator: GameObject = row[ i ];
                if ( indicator )
                {
                    const kDeltaHeight: number = maxHeight - indicator.cContainer.c.height;
                    indicator.cContainer.c.position.set( row.length == 1 ? 70 : 20 + i * 50, itemY + kDeltaHeight );
                }
            }

            itemY += maxHeight + kItemMargin;
        }

        // Bottom.
        arrRow = new Array<Array<GameObject>>();

        arrIndicator = new Array<GameObject>();
        //
        this._removedFromGame = this.createRemovedFromGameIndicator();
        this._bg.cContainer.addChild( this._removedFromGame );
        arrIndicator.push( this._removedFromGame );
        //
        this._setAside = this.createSetAsideIndicator();
        this._bg.cContainer.addChild( this._setAside );
        arrIndicator.push( this._setAside );
        //
        arrRow.push( arrIndicator );

        arrIndicator = new Array<GameObject>();
        //
        this._tray = this.createTrayIndicator();
        this._bg.cContainer.addChild( this._tray );
        arrIndicator.push( this._tray );
        //
        this._victoryDisplay = this.createVictoryDisplayIndicator();
        this._bg.cContainer.addChild( this._victoryDisplay );
        arrIndicator.push( this._victoryDisplay );
        // 
        arrRow.push( arrIndicator );

        // Position items.
        usedSpace = kItemMargin * ( arrRow.length - 1 );
        for ( let row of arrRow )
        {
            let maxHeight: number = 0;
            for ( let indicator of row )
            {
                if ( indicator )
                {
                    maxHeight = Math.max( maxHeight, indicator.cContainer.c.height );
                }
            }
            usedSpace += maxHeight;
        }
        // Draw bg.
        const kBottomBgHeight: number = usedSpace + 10 + 20;
        const kBottomY: number = 10;
        this._bg.cGraphics.g.beginFill( 0xbcb183, 1 ); 
        this._bg.cGraphics.g.drawRect( 0, kBottomY, 20, kBottomBgHeight );
        this._bg.cGraphics.g.endFill();
        this._bg.cGraphics.g.beginFill( 0xefe4b0, 1 ); 
        this._bg.cGraphics.g.moveTo( 20, kBottomY );
        this._bg.cGraphics.g.lineTo( 70, kBottomY + 20 );
        this._bg.cGraphics.g.lineTo( 70, kBottomY + kBottomBgHeight );
        this._bg.cGraphics.g.lineTo( 20, kBottomY + kBottomBgHeight );
        this._bg.cGraphics.g.lineTo( 20, kBottomY );
        this._bg.cGraphics.g.endFill();
        //
        itemY = kBottomY + 20;
        for ( let row of arrRow )
        {
            let maxHeight: number = 0;
            for ( let indicator of row )
            {
                if ( indicator )
                {
                    maxHeight = Math.max( maxHeight, indicator.cContainer.c.height );
                }
            }
            for ( let i: number = row.length - 1; i >= 0; --i )
            {
                let indicator: GameObject = row[ i ];
                if ( indicator )
                {
                    const kDeltaHeight: number = maxHeight - indicator.cContainer.c.height;
                    indicator.cContainer.c.position.set( 20 + i * 50, itemY + kDeltaHeight );
                }
            }

            itemY += maxHeight + kItemMargin;
        }
    }

    public end(): void
    {
        this._deck.end();
        this._deck = null;

        if ( this._secondaryDeck )
        {
            this._secondaryDeck.end();
            this._secondaryDeck = null;
        }

        this._discard.end();
        this._discard = null;

        if ( this._customDeck )
        {
            this._customDeck.end();
            this._customDeck = null;
        }

        if ( this._customDiscard )
        {
            this._customDiscard.end();
            this._customDiscard = null;
        }

        if ( this._secondaryCustomDeck )
        {
            this._secondaryCustomDeck.end();
            this._secondaryCustomDeck = null;
        }

        if ( this._secondaryCustomDiscard )
        {
            this._secondaryCustomDiscard.end();
            this._secondaryCustomDiscard = null;
        }

        this._removedFromGame.end();
        this._removedFromGame = null;

        this._victoryDisplay.end();
        this._victoryDisplay = null;

        this._questDeck.end();
        this._questDeck = null;

        this._questDiscard.end();
        this._questDiscard = null;

        this._setAside.end();
        this._setAside = null;

        this._tray.end();
        this._tray = null;

        this._bg.end();
        this._bg = null;

        super.end();
    }

    public saveGame(): ISgSauronSideControls
    {
        let arrTrayCard: Array<ISgCard> = new Array<ISgCard>();
        for ( let card of this._tray.cDeckIndicator.panel.cViewer.cCardView.findItems() )
        {
            let sgCard: ISgCard = card.cCard.saveGame();
            sgCard.x = card.cContainer.c.x;
            sgCard.y = card.cContainer.c.y;
            arrTrayCard.push( sgCard );
        }

        return {
            deck: this._deck.cDeckIndicator.panel.cViewer.cCardView.saveGame(),
            secondaryDeck: this._secondaryDeck ? this._secondaryDeck.cDeckIndicator.panel.cViewer.cCardView.saveGame() : null,
            discard: this._discard.cDeckIndicator.panel.cViewer.cCardView.saveGame(),
            customDeck: this._customDeck ? this._customDeck.cDeckIndicator.panel.cViewer.cCardView.saveGame() : null,
            customDiscard: this._customDiscard ? this._customDiscard.cDeckIndicator.panel.cViewer.cCardView.saveGame() : null,
            secondaryCustomDeck: this._secondaryCustomDeck ? this._secondaryCustomDeck.cDeckIndicator.panel.cViewer.cCardView.saveGame() : null,
            secondaryCustomDiscard: this._secondaryCustomDiscard ? this._secondaryCustomDiscard.cDeckIndicator.panel.cViewer.cCardView.saveGame() : null,
            questDeck: this._questDeck.cDeckIndicator.panel.cViewer.cCardView.saveGame(),
            questDiscard: this._questDiscard.cDeckIndicator.panel.cViewer.cCardView.saveGame(),
            setAside: this._setAside.cDeckIndicator.panel.cViewer.cCardView.saveGame(),
            removedFromGame: this._removedFromGame.cDeckIndicator.panel.cViewer.cCardView.saveGame(),
            victoryDisplay: this._victoryDisplay.cDeckIndicator.panel.cViewer.cCardView.saveGame(),
            tray: this._tray.cDeckIndicator.panel.cViewer.cCardView.saveGame() };
    }

    public loadGame( sgSauronSideControls: ISgSauronSideControls, pass: number ): void
    {
        if ( pass == 0 )
        {
            this._deck.cDeckIndicator.panel.cViewer.cCardView.loadGame( sgSauronSideControls.deck, pass );
            if ( this._secondaryDeck )
            {
                this._secondaryDeck.cDeckIndicator.panel.cViewer.cCardView.loadGame( sgSauronSideControls.secondaryDeck, pass );
            }
            this._discard.cDeckIndicator.panel.cViewer.cCardView.loadGame( sgSauronSideControls.discard, pass );
            if ( this._customDeck )
            {
                this._customDeck.cDeckIndicator.panel.cViewer.cCardView.loadGame( sgSauronSideControls.customDeck, pass );
            }
            if ( this._customDiscard )
            {
                this._customDiscard.cDeckIndicator.panel.cViewer.cCardView.loadGame( sgSauronSideControls.customDiscard, pass );
            }
            if ( this._secondaryCustomDeck )
            {
                this._secondaryCustomDeck.cDeckIndicator.panel.cViewer.cCardView.loadGame( sgSauronSideControls.secondaryCustomDeck, pass );
            }
            if ( this._secondaryCustomDiscard )
            {
                this._secondaryCustomDiscard.cDeckIndicator.panel.cViewer.cCardView.loadGame( sgSauronSideControls.secondaryCustomDiscard, pass );
            }
            this._questDeck.cDeckIndicator.panel.cViewer.cCardView.loadGame( sgSauronSideControls.questDeck, pass );
            this._questDiscard.cDeckIndicator.panel.cViewer.cCardView.loadGame( sgSauronSideControls.questDiscard, pass );
            this._setAside.cDeckIndicator.panel.cViewer.cCardView.loadGame( sgSauronSideControls.setAside, pass );
            this._removedFromGame.cDeckIndicator.panel.cViewer.cCardView.loadGame( sgSauronSideControls.removedFromGame, pass );
            this._victoryDisplay.cDeckIndicator.panel.cViewer.cCardView.loadGame( sgSauronSideControls.victoryDisplay, pass );
            this._tray.cDeckIndicator.panel.cViewer.cCardView.loadGame( sgSauronSideControls.tray, pass );
        }
    }

    // private:

    private createDeckIndicator(): GameObject
    {
        let cardViewerFactory: CardViewerFactory = new CardViewerFactory();
        let cardViewer: GameObject = cardViewerFactory.create( { 
            oid: this._go.oid + "_encounter_deck_viewer",
            title: jQuery.i18n( "ENCOUNTER_DECK" ),
            location: location_type.SAURON_DECK_0,
            playerType: player_type.SAURON,
            facePolicy: face_policy_type.FACE_DOWN,
            contextBtnTypes: [ context_button_type.REVEAL_X, context_button_type.DISCARD_X, context_button_type.SHUFFLE, context_button_type.SORT_FROM_A_TO_Z ],
            customController: new SauronDeckController() } );
        ServiceLocator.game.root.cGameLayerProvider.add( ( cardViewer.cContainer.c.parent as any ).go as GameObject, layer_type.HUD );

        let deck: GameObject = new GameObject( [ new CContainer(), new CDeckIndicator(), new CIndicatorLock(), new CIndicatorBusy(), new CShareableGameElement(), new CDeckIndicatorPoiReceptor(), new CTooltipReceptor() ] );
        deck.oid = this._go.oid + "_deck";
        deck.cDeckIndicator.iconId = "encounter_deck";
        deck.cDeckIndicator.panel = cardViewer;
        deck.cTooltipReceptor.text = jQuery.i18n( "ENCOUNTER_DECK" );
        deck.init();

        let topBottomDropController: TopBottomDropController = new TopBottomDropController();
        topBottomDropController.cardView = cardViewer.cViewer.cCardView;
        topBottomDropController.init();
        deck.cDeckIndicator.trigger.cDropArea.target = topBottomDropController;

        return deck;
    }

    private createSecondaryDeckIndicator( title: string ): GameObject
    {
        let cardViewerFactory: CardViewerFactory = new CardViewerFactory();
        let cardViewer: GameObject = cardViewerFactory.create( { 
            oid: this._go.oid + "_secondary_encounter_deck_viewer",
            title: title,
            location: location_type.SAURON_DECK_1,
            playerType: player_type.SAURON,
            facePolicy: face_policy_type.FACE_DOWN,
            contextBtnTypes: [ context_button_type.REVEAL_X, context_button_type.DISCARD_X, context_button_type.SHUFFLE, context_button_type.SORT_FROM_A_TO_Z, context_button_type.SWITCH ],
            customController: new SauronDeckController() } );
        ServiceLocator.game.root.cGameLayerProvider.add( ( cardViewer.cContainer.c.parent as any ).go as GameObject, layer_type.HUD );

        let deck: GameObject = new GameObject( [ new CContainer(), new CDeckIndicator(), new CIndicatorLock(), new CIndicatorBusy(), new CShareableGameElement(), new CDeckIndicatorPoiReceptor(), new CTooltipReceptor() ] );
        deck.oid = this._go.oid + "_secondary_encounter_deck";
        deck.cDeckIndicator.iconId = "secondary_encounter_deck";
        deck.cDeckIndicator.panel = cardViewer;
        deck.cTooltipReceptor.text = title;
        deck.init();

        let topBottomDropController: TopBottomDropController = new TopBottomDropController();
        topBottomDropController.cardView = cardViewer.cViewer.cCardView;
        topBottomDropController.init();
        deck.cDeckIndicator.trigger.cDropArea.target = topBottomDropController;

        return deck;
    }

    private createDiscardIndicator(): GameObject
    {
        let cardViewerFactory: CardViewerFactory = new CardViewerFactory();
        let cardViewer: GameObject = cardViewerFactory.create( { 
            oid: this._go.oid + "_encounter_discard_viewer",
            title: jQuery.i18n( "ENCOUNTER_DISCARD" ),
            location: location_type.SAURON_DISCARD,
            playerType: player_type.SAURON,
            facePolicy: face_policy_type.FACE_UP,
            contextBtnTypes: [ context_button_type.REMOVE_ALL, context_button_type.RESHUFFLE ],
            customController: new SauronDiscardController() } );
        ServiceLocator.game.root.cGameLayerProvider.add( ( cardViewer.cContainer.c.parent as any ).go as GameObject, layer_type.HUD );

        let discard: GameObject = new GameObject( [ new CContainer(), new CDeckIndicator(), new CIndicatorLock(), new CIndicatorBusy(), new CShareableGameElement(), new CDeckIndicatorPoiReceptor(), new CTooltipReceptor() ] );
        discard.oid = this._go.oid + "_encounter_discard";
        discard.cDeckIndicator.iconId = "encounter_discard";
        discard.cDeckIndicator.panel = cardViewer;
        discard.cTooltipReceptor.text = jQuery.i18n( "ENCOUNTER_DISCARD" );
        discard.init();

        return discard;
    }

    private createCustomDeckIndicator( title: string ): GameObject
    {
        let cardViewerFactory: CardViewerFactory = new CardViewerFactory();
        let cardViewer: GameObject = cardViewerFactory.create( { 
            oid: this._go.oid + "_custom_deck_viewer",
            title: title,
            location: location_type.SAURON_CUSTOM_DECK_0,
            playerType: player_type.SAURON,
            facePolicy: face_policy_type.FACE_DOWN,
            contextBtnTypes: [ context_button_type.REVEAL_X, context_button_type.DISCARD_X, context_button_type.SHUFFLE, context_button_type.SORT_FROM_A_TO_Z ],
            customController: new CustomDeckController() } );
        ServiceLocator.game.root.cGameLayerProvider.add( ( cardViewer.cContainer.c.parent as any ).go as GameObject, layer_type.HUD );

        let deck: GameObject = new GameObject( [ new CContainer(), new CDeckIndicator(), new CIndicatorLock(), new CIndicatorBusy(), new CShareableGameElement(), new CDeckIndicatorPoiReceptor(), new CTooltipReceptor() ] );
        deck.oid = this._go.oid + "_custom_deck";
        deck.cDeckIndicator.iconId = "sauron_custom_deck";
        deck.cDeckIndicator.panel = cardViewer;
        deck.cTooltipReceptor.text = title;
        deck.init();

        let topBottomDropController: TopBottomDropController = new TopBottomDropController();
        topBottomDropController.cardView = cardViewer.cViewer.cCardView;
        topBottomDropController.init();
        deck.cDeckIndicator.trigger.cDropArea.target = topBottomDropController;

        return deck;
    }

    private createCustomDiscardIndicator( title: string ): GameObject
    {
        let cardViewerFactory: CardViewerFactory = new CardViewerFactory();
        let cardViewer: GameObject = cardViewerFactory.create( { 
            oid: this._go.oid + "_custom_discard_viewer",
            title: title,
            location: location_type.SAURON_CUSTOM_DISCARD_0,
            playerType: player_type.SAURON,
            facePolicy: face_policy_type.FACE_UP,
            contextBtnTypes: [ context_button_type.REMOVE_ALL, context_button_type.RESHUFFLE ],
            customController: new CustomDeckController() } );
        ServiceLocator.game.root.cGameLayerProvider.add( ( cardViewer.cContainer.c.parent as any ).go as GameObject, layer_type.HUD );

        let discard: GameObject = new GameObject( [ new CContainer(), new CDeckIndicator(), new CIndicatorLock(), new CIndicatorBusy(), new CShareableGameElement(), new CDeckIndicatorPoiReceptor(), new CTooltipReceptor() ] );
        discard.oid = this._go.oid + "_custom_discard";
        discard.cDeckIndicator.iconId = "sauron_custom_discard";
        discard.cDeckIndicator.panel = cardViewer;
        discard.cTooltipReceptor.text = title;
        discard.init();

        return discard;
    }

    private createSecondaryCustomDeckIndicator( title: string ): GameObject
    {
        let cardViewerFactory: CardViewerFactory = new CardViewerFactory();
        let cardViewer: GameObject = cardViewerFactory.create( { 
            oid: this._go.oid + "_secondary_custom_deck_viewer",
            title: title,
            location: location_type.SAURON_CUSTOM_DECK_1,
            playerType: player_type.SAURON,
            facePolicy: face_policy_type.FACE_DOWN,
            contextBtnTypes: [ context_button_type.REVEAL_X, context_button_type.DISCARD_X, context_button_type.SHUFFLE, context_button_type.SORT_FROM_A_TO_Z ],
            customController: new CustomDeckController() } );
        ServiceLocator.game.root.cGameLayerProvider.add( ( cardViewer.cContainer.c.parent as any ).go as GameObject, layer_type.HUD );

        let deck: GameObject = new GameObject( [ new CContainer(), new CDeckIndicator(), new CIndicatorLock(), new CIndicatorBusy(), new CShareableGameElement(), new CDeckIndicatorPoiReceptor(), new CTooltipReceptor() ] );
        deck.oid = this._go.oid + "_secondary_custom_deck";
        deck.cDeckIndicator.iconId = "secondary_sauron_custom_deck";
        deck.cDeckIndicator.panel = cardViewer;
        deck.cTooltipReceptor.text = title;
        deck.init();

        let topBottomDropController: TopBottomDropController = new TopBottomDropController();
        topBottomDropController.cardView = cardViewer.cViewer.cCardView;
        topBottomDropController.init();
        deck.cDeckIndicator.trigger.cDropArea.target = topBottomDropController;

        return deck;
    }

    private createSecondaryCustomDiscardIndicator( title: string ): GameObject
    {
        let cardViewerFactory: CardViewerFactory = new CardViewerFactory();
        let cardViewer: GameObject = cardViewerFactory.create( { 
            oid: this._go.oid + "_secondary_custom_discard_viewer",
            title: title,
            location: location_type.SAURON_CUSTOM_DISCARD_1,
            playerType: player_type.SAURON,
            facePolicy: face_policy_type.FACE_UP,
            contextBtnTypes: [ context_button_type.RESHUFFLE ],
            customController: new CustomDeckController() } );
        ServiceLocator.game.root.cGameLayerProvider.add( ( cardViewer.cContainer.c.parent as any ).go as GameObject, layer_type.HUD );

        let discard: GameObject = new GameObject( [ new CContainer(), new CDeckIndicator(), new CIndicatorLock(), new CIndicatorBusy(), new CShareableGameElement(), new CDeckIndicatorPoiReceptor(), new CTooltipReceptor() ] );
        discard.oid = this._go.oid + "_secondary_custom_discard";
        discard.cDeckIndicator.iconId = "secondary_sauron_custom_discard";
        discard.cDeckIndicator.panel = cardViewer;
        discard.cTooltipReceptor.text = title;
        discard.init();

        return discard;
    }

    private createRemovedFromGameIndicator(): GameObject
    {
        let cardViewerFactory: CardViewerFactory = new CardViewerFactory();
        let cardViewer: GameObject = cardViewerFactory.create( { 
            oid: this._go.oid + "_removed_viewer",
            title: jQuery.i18n( "REMOVED_FROM_GAME" ),
            location: location_type.REMOVED_FROM_GAME,
            playerType: player_type.SAURON,
            facePolicy: face_policy_type.FACE_UP,
            customController: new RemovedFromGameController() } );
        ServiceLocator.game.root.cGameLayerProvider.add( ( cardViewer.cContainer.c.parent as any ).go as GameObject, layer_type.HUD );

        let removedFromGame: GameObject = new GameObject( [ new CContainer(), new CDeckIndicator(), new CIndicatorLock(), new CIndicatorBusy(), new CShareableGameElement(), new CDeckIndicatorPoiReceptor(), new CTooltipReceptor() ] );
        removedFromGame.oid = this._go.oid + "_removed_from_game";
        removedFromGame.cDeckIndicator.iconId = "removed_from_game";
        removedFromGame.cDeckIndicator.panel = cardViewer;
        removedFromGame.cTooltipReceptor.text = jQuery.i18n( "REMOVED_FROM_GAME" );
        removedFromGame.init();

        return removedFromGame;
    }

    private createVictoryDisplayIndicator(): GameObject
    {
        let cardViewerFactory: CardViewerFactory = new CardViewerFactory();
        let cardViewer: GameObject = cardViewerFactory.create( { 
            oid: this._go.oid + "_victory_display_viewer",
            title: jQuery.i18n( "VICTORY_DISPLAY" ),
            location: location_type.VICTORY_DISPLAY,
            playerType: player_type.SAURON,
            facePolicy: face_policy_type.FACE_UP,
            customController: new VictoryDisplayController() } );
        ServiceLocator.game.root.cGameLayerProvider.add( ( cardViewer.cContainer.c.parent as any ).go as GameObject, layer_type.HUD );

        let victoryDisplay: GameObject = new GameObject( [ new CContainer(), new CDeckIndicator(), new CIndicatorLock(), new CIndicatorBusy(), new CShareableGameElement(), new CDeckIndicatorPoiReceptor(), new CTooltipReceptor() ] );
        victoryDisplay.oid = this._go.oid + "_victory_display";
        victoryDisplay.cDeckIndicator.iconId = "victory_display";
        victoryDisplay.cDeckIndicator.panel = cardViewer;
        victoryDisplay.cTooltipReceptor.text = jQuery.i18n( "VICTORY_DISPLAY" );
        victoryDisplay.init();

        return victoryDisplay;
    }

    private createQuestDeckIndicator(): GameObject
    {
        let cardViewerFactory: CardViewerFactory = new CardViewerFactory();
        let cardViewer: GameObject = cardViewerFactory.create( { 
            oid: this._go.oid + "_quest_deck_viewer",
            title: jQuery.i18n( "QUEST_DECK" ),
            location: location_type.QUEST_DECK,
            playerType: player_type.SAURON,
            facePolicy: face_policy_type.FACE_DOWN,
            contextBtnTypes: [ context_button_type.SHUFFLE ],
            customController: new QuestDeckController() } );
        ServiceLocator.game.root.cGameLayerProvider.add( ( cardViewer.cContainer.c.parent as any ).go as GameObject, layer_type.HUD );

        let questDeck: GameObject = new GameObject( [ new CContainer(), new CDeckIndicator(), new CIndicatorLock(), new CIndicatorBusy(), new CShareableGameElement(), new CDeckIndicatorPoiReceptor(), new CTooltipReceptor() ] );
        questDeck.oid = this._go.oid + "_quest_deck";
        questDeck.cDeckIndicator.iconId = "quest_deck";
        questDeck.cDeckIndicator.panel = cardViewer;
        questDeck.cTooltipReceptor.text = jQuery.i18n( "QUEST_DECK" );
        questDeck.init();

        let topBottomDropController: TopBottomDropController = new TopBottomDropController();
        topBottomDropController.cardView = cardViewer.cViewer.cCardView;
        topBottomDropController.init();
        questDeck.cDeckIndicator.trigger.cDropArea.target = topBottomDropController;

        return questDeck;
    }

    private createQuestDiscardIndicator(): GameObject
    {
        let cardViewerFactory: CardViewerFactory = new CardViewerFactory();
        let cardViewer: GameObject = cardViewerFactory.create( { 
            oid: this._go.oid + "_quest_discard_viewer",
            title: jQuery.i18n( "QUEST_DISCARD" ),
            location: location_type.QUEST_DISCARD,
            playerType: player_type.SAURON,
            facePolicy: face_policy_type.FACE_UP,
            customController: new QuestDeckController() } );
        ServiceLocator.game.root.cGameLayerProvider.add( ( cardViewer.cContainer.c.parent as any ).go as GameObject, layer_type.HUD );

        let discard: GameObject = new GameObject( [ new CContainer(), new CDeckIndicator(), new CIndicatorLock(), new CIndicatorBusy(), new CShareableGameElement(), new CDeckIndicatorPoiReceptor(), new CTooltipReceptor() ] );
        discard.oid = this._go.oid + "_quest_discard";
        discard.cDeckIndicator.iconId = "quest_discard";
        discard.cDeckIndicator.panel = cardViewer;
        discard.cTooltipReceptor.text = jQuery.i18n( "QUEST_DISCARD" );
        discard.init();

        return discard;
    }

    private createSetAsideIndicator(): GameObject
    {
        let cardViewerFactory: CardViewerFactory = new CardViewerFactory();
        let cardViewer: GameObject = cardViewerFactory.create( { 
            oid: this._go.oid + "_set_aside_viewer",
            title: jQuery.i18n( "SET_ASIDE" ),
            location: location_type.SET_ASIDE,
            playerType: player_type.SAURON,
            facePolicy: face_policy_type.NONE,
            customController: new CustomDeckController() } );
        ServiceLocator.game.root.cGameLayerProvider.add( ( cardViewer.cContainer.c.parent as any ).go as GameObject, layer_type.HUD );

        let setAside: GameObject = new GameObject( [ new CContainer(), new CDeckIndicator(), new CIndicatorLock(), new CIndicatorBusy(), new CShareableGameElement(), new CDeckIndicatorPoiReceptor(), new CTooltipReceptor() ] );
        setAside.oid = this._go.oid + "_set_aside";
        setAside.cDeckIndicator.iconId = "set_aside";
        setAside.cDeckIndicator.panel = cardViewer;
        setAside.cTooltipReceptor.text = jQuery.i18n( "SET_ASIDE" );
        setAside.init();

        let topBottomDropController: TopBottomDropController = new TopBottomDropController();
        topBottomDropController.cardView = cardViewer.cViewer.cCardView;
        topBottomDropController.init();
        setAside.cDeckIndicator.trigger.cDropArea.target = topBottomDropController;

        return setAside;
    }

    private createTrayIndicator(): GameObject
    {
        let cardViewer: GameObject = new GameObject( [ new CContainer(), new CViewer() ] );
        cardViewer.oid = this._go.oid + "_tray_viewer";
        let cardTrayView: CCardTrayView = new CCardTrayView();
        cardTrayView.height = 900;
        cardTrayView.width = 1600;
        cardViewer.cViewer.view = cardTrayView;
        cardViewer.cViewer.title = jQuery.i18n( "TRAY" );
        cardViewer.cViewer.location = location_type.CARD_TRAY;
        cardViewer.cViewer.controller = new CardTrayController();
        cardViewer.cViewer.isSelfDropAllowed = true;
        cardViewer.init();

        let anchorY: GameObject = new GameObject( [ new CContainer(), new CAnchorY() ] );
        anchorY.cAnchorY.child = cardViewer.cViewer;
        anchorY.cContainer.c.x = 220;
        anchorY.cContainer.c.y = 1070;
        anchorY.cAnchorY.anchorY = 1;
        anchorY.init();
        ServiceLocator.game.root.cGameLayerProvider.add( ( cardViewer.cContainer.c.parent as any ).go as GameObject, layer_type.HUD );

        let tray: GameObject = new GameObject( [ new CContainer(), new CDeckIndicator(), new CIndicatorLock(), new CDeckIndicatorPoiReceptor(), new CTooltipReceptor() ] );
        tray.oid = this._go.oid + "_tray";
        tray.cDeckIndicator.iconId = "tray";
        tray.cDeckIndicator.panel = cardViewer;
        tray.cTooltipReceptor.text = jQuery.i18n( "TRAY" );
        tray.init();

        return tray;
    }

    // #endregion //
}
