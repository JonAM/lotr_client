import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CTokenCounter from "./CTokenCounter";
import CGraphics from "../pixi/CGraphics";
import CDropArea from "../input/CDropArea";
import CDraggable from "../input/CDraggable";
import { player_type } from "../world/CPlayerArea";
import CButton from "../input/CButton";
import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import CSprite from "../pixi/CSprite";
import ICharacterTokenSide from "../card/ICharacterTokenSide";
import Signal from "../../../lib/signals/Signal";
import { ISgAttackBindingCounter } from "../../../view/game/SaveGameView";
import { IOppActionListener, IOpponentAction } from "../../AllyActionManager";
import CTooltipReceptor from "./CTooltipReceptor";
import PointerInputController from "../../input/PointerInputController";
import TouchInputController from "../../input/TouchInputController";


export default class CAttackBindingCounter extends Component implements IOppActionListener
{
    // #region Attributes //

    // private:

    private _playerType: player_type = null;
    private _defender: GameObject = null;
    private _arrAttacker: Array<GameObject> = null;
    
    private _counter: GameObject = null;
    private _text: PIXI.Text = null;
    private _autoBtn: GameObject = null;
    private _defendedBtn: GameObject = null;
    private _okBtn: GameObject = null;
    private _cancelBtn: GameObject = null;
    private _deadIcon: PIXI.Sprite = null;

    private _isAuto: boolean = null;
    private _isDefended: boolean = null;

    // Signals.
    private _onAttackCompleted: Signal = new Signal();
    private _onAttackCanceled: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public set playerType( value: player_type ) { this._playerType = value; }
    public set defender( value: GameObject ) { this._defender = value; }
    public set attacker( value: GameObject ) { this._arrAttacker = [ value ]; }
    public set isAuto( value: boolean ) { this._isAuto = value; }

    public get isAuto(): boolean { return this._isAuto; }

    // Signals.
    public get onAttackCompleted(): Signal { return this._onAttackCompleted; }
    public get onAttackCanceled(): Signal { return this._onAttackCanceled; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CAttackBindingCounter";
    }
    
    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CAttackBindingCounter.ts :: init() :: CContainer component not found." );
        console.assert( this._playerType != null, "CAttackBindingCounter.ts :: init() :: this._playerType cannot be null." );
        console.assert( this._defender != null, "CAttackBindingCounter.ts :: init() :: this._defender cannot be null." );
        console.assert( this._arrAttacker.length == 1, "CAttackBindingCounter.ts :: init() :: attacker not set." );

        this._isAuto = true;
        this._isDefended = true;

        this._go.cContainer.c.interactive = true;

        // Bg.
        let bg: PIXI.Graphics = new PIXI.Graphics();
        let color: number = ServiceLocator.game.playerColors[ this._arrAttacker[ 0 ].cCardToken.controllerPlayer ];
        if ( this._arrAttacker[ 0 ].cCardToken.ownerPlayer == player_type.SAURON )
        {
            color = ServiceLocator.game.playerColors[ player_type.SAURON ];
        }
        bg.beginFill( color );
        bg.drawCircle( 0, 0, 40 );
        bg.beginFill( 0xffffff );
        bg.drawCircle( 0, 0, 30 );
        bg.endFill();
        this._go.cContainer.c.addChild( bg );

        // Wound.
        let woundContainer: PIXI.Container = new PIXI.Container();
        this._go.cContainer.c.addChild( woundContainer );
        // // Counter.
        this._counter = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._counter.oid = this._go.oid + "_counter";
        this._counter.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "wound_token" );
        this._counter.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 50, this._counter.cSprite.s );
        this._counter.cTokenCounter.tokenName = "wound";
        this._counter.cTokenCounter.count = this.calcAutoCount();
        this._counter.cTokenCounter.minCount = 0;
        this._counter.cTokenCounter.draggedTexture = ServiceLocator.resourceStack.findAsTexture( "wound_token" );
        this._counter.cDropArea.target = this._counter.cTokenCounter;
        this._counter.init();
        woundContainer.addChild( this._counter.cContainer.c );
        this._counter.cTokenCounter.onCountUpdated.add( this.onCounter_Updated, this );
        this._counter.cTokenCounter.setEnabled( this._playerType == player_type.PLAYER );
        // // Dead icon.
        this._deadIcon = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( "status_dead" ) );
        this._deadIcon.anchor.set( 0.5 );
        this._deadIcon.alpha = 0.5;
        Utils.game.limitSideSize( 40, this._deadIcon );
        woundContainer.addChild( this._deadIcon );
        // // Text.
        this._text = new PIXI.Text( "+" + this._counter.cTokenCounter.count.toString(), ServiceLocator.game.textStyler.normal );
        this._text.name = "text";
        this._text.anchor.set( 0.5 );
        woundContainer.addChild( this._text );
        // // Auto button.
        this._autoBtn = new GameObject( [ new CGraphics(), new CButton() ] );
        this._autoBtn.cGraphics.g.lineStyle( 1, 0x000000, 1 );
        this._autoBtn.cGraphics.g.beginFill( 0x00008b );
        this._autoBtn.cGraphics.g.drawCircle( 0, 0, 15 );
        this._autoBtn.cGraphics.g.endFill();
        let autoIcon: PIXI.Sprite = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( "auto" ) );
        Utils.game.limitSideSize( 20, autoIcon );
        autoIcon.anchor.set( 0.5, 0.5 );
        this._autoBtn.cGraphics.g.addChild( autoIcon );
        this._autoBtn.init();
        this._autoBtn.cGraphics.g.position.set( 25, -25 );
        this._autoBtn.cContainer.c.visible = false;
        woundContainer.addChild( this._autoBtn.cContainer.c );
        this._autoBtn.cButton.onClick.add( this.onAutoBtn_Click, this );
        // // Defended button.
        this._defendedBtn = new GameObject( [ new CGraphics(), new CButton(), new CTooltipReceptor() ] );
        this._defendedBtn.cGraphics.g.lineStyle( 1, 0x000000, 1 );
        this._defendedBtn.cGraphics.g.beginFill( 0xeccc68 );
        this._defendedBtn.cGraphics.g.drawCircle( 0, 0, 15 );
        this._defendedBtn.cGraphics.g.endFill();
        this._defendedBtn.cTooltipReceptor.text = jQuery.i18n( "DEFENDED" );
        this._defendedBtn.init();
        this._defendedBtn.cButton.setEnabled( this._playerType != player_type.ALLY );
        this._defendedBtn.cGraphics.g.position.set( 25, -25 );
        woundContainer.addChild( this._defendedBtn.cContainer.c );
        this._defendedBtn.cButton.onClick.add( this.onDefendedBtn_Click, this );
        // // // Icon
        let defendedIcon: PIXI.Sprite = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( "defense" ) );
        Utils.game.limitSideSize( 20, defendedIcon );
        defendedIcon.anchor.set( 0.5, 0.5 );
        this._defendedBtn.cGraphics.g.addChild( defendedIcon );
        // // // Undefended mark.
        let undefendedMark: PIXI.Graphics = new PIXI.Graphics();
        undefendedMark.name = "undefended";
        undefendedMark.lineStyle( 8, 0xff0000, 0.8 );
        undefendedMark.moveTo( -15, -15 );
        undefendedMark.lineTo( 15, 15 );
        this._defendedBtn.cGraphics.g.addChild( undefendedMark );
        undefendedMark.visible = false;
        // // // Mask.
        let mask: PIXI.Graphics = new PIXI.Graphics();
        mask.beginFill();
        mask.drawCircle( 0, 0, 14 );
        mask.endFill();
        this._defendedBtn.cGraphics.g.addChild( mask );
        undefendedMark.mask = mask;

        // Action buttons.
        // Ok.
        let container: PIXI.Container = new PIXI.Container();
        let m: PIXI.Matrix = new PIXI.Matrix();
        m.translate( 0, 40 );
        container.position.set( m.tx, m.ty );
        this._go.cContainer.c.addChild( container );
        //
        this._okBtn = new GameObject( [ new CSprite(), new CButton() ] );
        this._okBtn.cSprite.s.anchor.set( 0.5 );
        this._okBtn.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "ok" );
        Utils.game.limitSideSize( 30, this._okBtn.cSprite.s );
        this._okBtn.init();
        this._okBtn.cContainer.c.visible = false;
        container.addChild( this._okBtn.cContainer.c );
        this._okBtn.cButton.onClick.add( this.onOkBtn_Click, this );
        // Cancel.
        container = new PIXI.Container();
        m.rotate( -Math.PI * 0.35 );
        container.position.set( m.tx, m.ty );
        this._go.cContainer.c.addChild( container );
        //
        this._cancelBtn = new GameObject( [ new CSprite(), new CButton() ] );
        this._cancelBtn.cSprite.s.anchor.set( 0.5 );
        this._cancelBtn.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "cancel" );
        Utils.game.limitSideSize( 30, this._cancelBtn.cSprite.s );
        this._cancelBtn.init();
        this._cancelBtn.cContainer.c.visible = false;
        container.addChild( this._cancelBtn.cContainer.c );
        this._cancelBtn.cButton.onClick.add( this.onCancelBtn_Click, this );

        this.updateDeadIcon();

        // Listen to events.
        ( this._defender.cCardToken.cCurSide as unknown as ICharacterTokenSide ).defense.cTokenCounter.onCountUpdated.add( this.onWoundCount_Updated, this );
        ( this._defender.cCardToken.cCurSide as unknown as ICharacterTokenSide ).health.cTokenCounter.onCountUpdated.add( this.onDefenderHealthCounter_Updated, this );
        ( this._defender.cCardToken.cCurSide as unknown as ICharacterTokenSide ).wound.cTokenCounter.onCountUpdated.add( this.onDefenderWoundCounter_Updated, this );
        this._defender.onEnded.add( this.onWoundCount_Updated, this );
        ( this._arrAttacker[ 0 ].cCardToken.cCurSide as unknown as ICharacterTokenSide ).attack.cTokenCounter.onCountUpdated.add( this.onWoundCount_Updated, this );
        this._arrAttacker[ 0 ].onEnded.add( this.onWoundCount_Updated, this );
        if ( this._playerType == player_type.PLAYER )
        {
            if ( ServiceLocator.game.inputController instanceof PointerInputController )
            {
                this._go.cContainer.c.on( "pointermove", this.onPointerMove, this );
            }
            else if ( ServiceLocator.game.inputController instanceof TouchInputController )
            {
                this._okBtn.cContainer.c.visible = true;
                this._cancelBtn.cContainer.c.visible = true;
            }
        }
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.TOGGLE_DEFENDED_ATTACK ] );
    }

    public end(): void
    {
        this._onAttackCompleted.removeAll();
        this._onAttackCanceled.removeAll();

        // Cleanup events.
        ( this._defender.cCardToken.cCurSide as unknown as ICharacterTokenSide ).defense.cTokenCounter.onCountUpdated.remove( this.onWoundCount_Updated, this );
        ( this._defender.cCardToken.cCurSide as unknown as ICharacterTokenSide ).health.cTokenCounter.onCountUpdated.remove( this.onDefenderHealthCounter_Updated, this );
        ( this._defender.cCardToken.cCurSide as unknown as ICharacterTokenSide ).wound.cTokenCounter.onCountUpdated.remove( this.onDefenderWoundCounter_Updated, this );
        this._defender.onEnded.remove( this.onWoundCount_Updated, this );
        for ( let attacker of this._arrAttacker )
        {
            ( attacker.cCardToken.cCurSide as unknown as ICharacterTokenSide ).attack.cTokenCounter.onCountUpdated.remove( this.onWoundCount_Updated, this );
            attacker.onEnded.remove( this.onWoundCount_Updated, this );
        }
        if ( this._playerType == player_type.PLAYER )
        {
            this._go.cContainer.c.off( "pointermove", this.onPointerMove, this );
        }
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.TOGGLE_DEFENDED_ATTACK ] );

        this._playerType = null;
        this._defender = null;
        this._text = null;
        this._arrAttacker = null;
        this._isAuto = null;
        this._isDefended = null;

        this._counter.end();
        this._counter = null;

        this._autoBtn.end();
        this._autoBtn = null;

        this._defendedBtn.end();
        this._defendedBtn = null;

        this._okBtn.end();
        this._okBtn = null;

        this._cancelBtn.end();
        this._cancelBtn = null;
    }

    public addAttacker( attacker: GameObject ): void
    {
        // Listen to events.
        ( attacker.cCardToken.cCurSide as unknown as ICharacterTokenSide ).attack.cTokenCounter.onCountUpdated.add( this.onWoundCount_Updated, this );
        attacker.onEnded.add( this.onWoundCount_Updated, this );

        this._arrAttacker.push( attacker );

        this.onWoundCount_Updated();
    }

    public removeAttacker( attacker: GameObject ): void
    {
        // Cleanup events.
        ( attacker.cCardToken.cCurSide as unknown as ICharacterTokenSide ).attack.cTokenCounter.onCountUpdated.remove( this.onWoundCount_Updated, this );
        attacker.onEnded.remove( this.onWoundCount_Updated, this );

        this._arrAttacker.splice( this._arrAttacker.indexOf( attacker ), 1 );

        this.onWoundCount_Updated();
    }

    public onAllyAutoUpdated( isAuto: boolean ): void
    {
        this._isAuto = isAuto;

        this._defendedBtn.cContainer.c.visible = isAuto;
    }

    // overrides.
    public saveGame(): ISgAttackBindingCounter
    {
        let arrAttackerOid: Array<string> = new Array<string>();
        for ( let attacker of this._arrAttacker )
        {
            arrAttackerOid.push( attacker.oid );
        }

        return {  
            playerType: this._playerType,
            defenderOid: this._defender.oid,
            attackerOids: arrAttackerOid,
            count: this._counter.cTokenCounter.count,
            isAuto: this._isAuto,
            isDefended: this._isDefended,
            isVisible: this._go.cContainer.c.visible };
    }

    // overrides.
    public loadGame( sgAttackBindingCounter: ISgAttackBindingCounter, pass: number ): void
    {
        this._go.cContainer.c.visible = sgAttackBindingCounter.isVisible;

        this._isDefended = sgAttackBindingCounter.isDefended;
        this._isAuto = sgAttackBindingCounter.isAuto;

        this._defendedBtn.cContainer.c.visible = this._isAuto;
        this.updateDefendedIcon( this._isDefended );

        if ( this._isAuto )
        {
            this._counter.cTokenCounter.setCount( this.calcAutoCount(), action_scope_type.LOCAL );
        }
        else
        {
            this._counter.cTokenCounter.setCount( sgAttackBindingCounter.count, action_scope_type.LOCAL );
            this._autoBtn.cContainer.c.visible = this._playerType == player_type.PLAYER;
        }
        this.updateDeadIcon();
    }

        private updateDefendedIcon( isDefended: boolean ): void
        {
            ( this._defendedBtn.cContainer.c.getChildByName( "undefended" ) as PIXI.Container ).visible = !isDefended;
            this._defendedBtn.cTooltipReceptor.text = jQuery.i18n( isDefended ? "DEFENDED" : "UNDEFENDED" );
        }

    // private:

    private calcAutoCount(): number
    {
        let result: number = this._isDefended ? -( this._defender.cCardToken.cCurSide as unknown as ICharacterTokenSide ).defense.cTokenCounter.count : 0;
        for ( let attacker of this._arrAttacker )
        {
            result += ( attacker.cCardToken.cCurSide as unknown as ICharacterTokenSide ).attack.cTokenCounter.count;
        }
        if ( result < 0 )
        {
            result = 0;
        }

        return result;
    }

    private updateDeadIcon(): void
    {
        let defenderSide: ICharacterTokenSide = this._defender.cCardToken.cCurSide as unknown as ICharacterTokenSide;
        this._deadIcon.visible = this._counter.cTokenCounter.count + defenderSide.wound.cTokenCounter.count >= defenderSide.health.cTokenCounter.count;
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.TOGGLE_DEFENDED_ATTACK
            && action.args[ 0 ] as string == this._go.oid )
        {
            this.toggleDefendedBtn( action_scope_type.LOCAL );
        }
    }

    // #endregion //


    // #region Input Callbacks //

    private onPointerMove( event: PIXI.InteractionEvent ): void
    {
        if ( ServiceLocator.game.dragShadowManager.isDragging ) { return; }

        if ( this._go.cContainer.c.getBounds().contains( event.data.global.x, event.data.global.y ) )
        {
            this._okBtn.cContainer.c.visible = true;
            this._cancelBtn.cContainer.c.visible = true;
        }
        else
        {
            this._okBtn.cContainer.c.visible = false;
            this._cancelBtn.cContainer.c.visible = false;
        }
    }

    private onAutoBtn_Click(): void
    {
        this._isAuto = true;
        this._autoBtn.cContainer.c.visible = false;

        this._defendedBtn.cContainer.c.visible = true;

        this._counter.cTokenCounter.setCount( this.calcAutoCount(), action_scope_type.MULTIPLAYER );
        
        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_AUTO_COUNTER, null, [ this._go.oid, this._isAuto ] );
    }

    private onDefendedBtn_Click(): void
    {
        this.toggleDefendedBtn( action_scope_type.MULTIPLAYER );
    }

        private toggleDefendedBtn( actionScope: action_scope_type ): void
        {
            this._isDefended = !this._isDefended;
            this.updateDefendedIcon( this._isDefended );

            this._counter.cTokenCounter.setCount( this.calcAutoCount(), action_scope_type.LOCAL );
            
            // Multiplayer.
            if ( actionScope == action_scope_type.MULTIPLAYER )
            {
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.TOGGLE_DEFENDED_ATTACK, null, [ this._go.oid ] );
            }
        }

    private onOkBtn_Click(): void
    {
        this._onAttackCompleted.dispatch( this._counter.cTokenCounter.count, this._defender, this._arrAttacker, this._isDefended );
    }
    
    private onCancelBtn_Click(): void
    {
        this._onAttackCanceled.dispatch( this._defender );
    }

    // #endregion //


    // #region Other Callbacks //

    private onCounter_Updated( count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._text.text = "+" + count.toString();

        if ( this._playerType == player_type.PLAYER && isPlayerInput )
        {
            this._isAuto = false;
            this._autoBtn.cContainer.c.visible = true;

            this._defendedBtn.cContainer.c.visible = false;

            // Multiplayer.
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_AUTO_COUNTER, null, [ this._go.oid, this._isAuto ] );
        }

        const kAutoCount: number = this.calcAutoCount();
        if ( count > kAutoCount )
        {
            this._text.tint = 0x00ff00;
        }
        else if ( count < kAutoCount )
        {
            this._text.tint = 0xff0000;
        }
        else
        {
            this._text.tint = 0xffffff;
        }

        CTokenCounter.animate( this._counter.cContainer.c.parent, delta );

        this.updateDeadIcon();
    }

    private onWoundCount_Updated(): void
    {
        if ( this._isAuto )
        {
            this._counter.cTokenCounter.setCount( this.calcAutoCount(), action_scope_type.LOCAL );
        }
    }

    private onDefenderHealthCounter_Updated(): void
    {
        this.updateDeadIcon();
    }

    private onDefenderWoundCounter_Updated(): void
    {
        this.updateDeadIcon();
    }

    // #endregion //
}