import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CGraphics from "../pixi/CGraphics";
import { combat_state_id, encounter_state_id, game_state_id, quest_state_id } from "../../../states/StateId";
import CImagePreviewable from "../input/CImagePreviewable";
import PhaseDiagramView from "../../../view/game/PhaseDiagramView";
import { view_layer_id } from "../../../service/ViewManager";
import { ISgPhaseDiagramButton } from "../../../view/game/SaveGameView";
import CContainer from "../pixi/CContainer";


export default class CPhaseDiagramButton extends Component
{
    // #region Attributes //

    // private:
    
    private _vfx: GameObject = null;
    private _button: GameObject = null;
    private _phaseText: PIXI.Text = null;
    private _subphaseText: PIXI.Text = null;
    private _subphaseIndicator: GameObject = null; 

    private _phaseId: game_state_id = null;
    private _subphaseId: quest_state_id | encounter_state_id | combat_state_id = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CPhaseDiagramButton";
    }
    
    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CPhaseDiagramButton.ts :: init() :: cContainer component not found." );
        console.assert( this._go.cButton != null, "CPhaseDiagramButton.ts :: init() :: CButton component not found." );

        this._vfx = new GameObject( [ new CContainer() ] );
        this._vfx.init();
        this._go.cContainer.addChild( this._vfx );

        this._button = new GameObject( [ new CGraphics(), new CImagePreviewable() ] );
        this._button.cGraphics.g.lineStyle( 1, 0x000000 );
        this._button.cGraphics.g.beginFill( 0xeccc68 );
        this._button.cGraphics.g.drawCircle( 0, 0, 35 );
        this._button.cGraphics.g.endFill();
        this._button.init();
        this._go.cContainer.addChild( this._button );
        
        let inside: PIXI.Graphics = new PIXI.Graphics();
        inside.lineStyle( 1, 0x000000 );
        inside.beginFill( 0xeccc68 );
        inside.drawCircle( 0, 0, 30 );
        inside.endFill();
        this._button.cContainer.c.addChild( inside );

        let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( "ring" ).data ) );
        Utils.game.limitSideSize( 55, icon );
        icon.anchor.set( 0.5 );
        inside.addChild( icon );

        this._phaseText = new PIXI.Text( "", ServiceLocator.game.textStyler.subtitle );
        this._phaseText.anchor.set( 0.5 );
        this._phaseText.y = inside.height * 0.5;
        this._go.cContainer.c.addChild( this._phaseText );

        this._subphaseText = new PIXI.Text( "", ServiceLocator.game.textStyler.normal );
        this._subphaseText.anchor.set( 0.5 );
        this._subphaseText.position.y = this._phaseText.y + this._phaseText.height * 0.5 + this._subphaseText.height * 0.5;
        this._subphaseText.visible = false;
        this._go.cContainer.c.addChildAt( this._subphaseText, this._go.cContainer.c.getChildIndex( this._phaseText ) );

        this._subphaseIndicator = new GameObject( [ new CGraphics(), new CImagePreviewable() ] );
        this._subphaseIndicator.cGraphics.g.lineStyle( 2 );
        this._subphaseIndicator.cGraphics.g.beginFill( 0xefe4b0 );
        this._subphaseIndicator.cGraphics.g.drawCircle( 0, 0, 15 );
        this._subphaseIndicator.cGraphics.g.endFill();
        this._subphaseIndicator.init();
        this._subphaseIndicator.cContainer.c.position.set( inside.width * 0.45, -inside.height * 0.45 );
        this._subphaseIndicator.cContainer.c.visible = false;
        this._go.cContainer.addChild( this._subphaseIndicator );
        //
        let subphaseIcon: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( "transparent" ) );
        subphaseIcon.name = "icon";
        subphaseIcon.anchor.set( 0.5 );
        this._subphaseIndicator.cContainer.c.addChild( subphaseIcon );

        // Listen to events.
        this._go.cButton.onClick.add( this.onPhaseDiagramBtn_Click, this );
    }

    public end(): void
    {
        this._vfx.end();
        this._vfx = null;

        this._button.end();
        this._button = null;

        this._subphaseIndicator.end();
        this._subphaseIndicator = null;

        this._phaseText = null;

        super.end();
    }

    public setGamePhase( gameStateId: game_state_id ): void
    {
        this._phaseId = gameStateId;

        switch ( gameStateId )
        {
            case game_state_id.SETUP: 
            { 
                this._phaseText.text = jQuery.i18n( "SETUP" ); 
                this._button.cImagePreviewable.imageId = "transparent";
                break; 
            }

            case game_state_id.RESOURCE: 
            { 
                this._phaseText.text = jQuery.i18n( "RESOURCE" ); 
                this._button.cImagePreviewable.imageId = "phase_resource";
                break; 
            }

            case game_state_id.PLANNING: 
            { 
                this._phaseText.text = jQuery.i18n( "PLANNING" ); 
                this._button.cImagePreviewable.imageId = "phase_planning";
                break; 
            }

            case game_state_id.QUEST: 
            { 
                this._phaseText.text = jQuery.i18n( "QUEST" ); 
                this._button.cImagePreviewable.imageId = "phase_quest";
                break; 
            }

            case game_state_id.TRAVEL: 
            { 
                this._phaseText.text = jQuery.i18n( "TRAVEL" ); 
                this._button.cImagePreviewable.imageId = "phase_travel";
                break; 
            }

            case game_state_id.ENCOUNTER: 
            { 
                this._phaseText.text = jQuery.i18n( "ENCOUNTER" ); 
                this._button.cImagePreviewable.imageId = "phase_encounter";
                break; 
            }

            case game_state_id.COMBAT: 
            { 
                this._phaseText.text = jQuery.i18n( "COMBAT" ); 
                this._button.cImagePreviewable.imageId = "phase_combat";
                break; 
            }

            case game_state_id.REFRESH: 
            { 
                this._phaseText.text = jQuery.i18n( "REFRESH" ); 
                this._button.cImagePreviewable.imageId = "phase_refresh";
                break; 
            }
        }

        this._phaseText.scale.set( 1 );
        if ( this._phaseText.width > 80 )
        {
            this._phaseText.scale.set( 80 / this._phaseText.width );
        }

        this._subphaseId = null;
        this._subphaseText.visible = false;
        this._subphaseIndicator.cContainer.c.visible = false;

        // Vfx.
        Utils.anim.flashCircle( new PIXI.Point( 0, 0 ), 35, 1.5, this._vfx );
    }

    public setQuestSubphase( gameSubstateId: quest_state_id  ): void
    {
        this._subphaseId = gameSubstateId;

        switch ( this._subphaseId )
        {
            case quest_state_id.CHARACTER_COMMITMENT: { this._subphaseText.text = jQuery.i18n( "COMMIT" ); break; }
            case quest_state_id.STAGING: { this._subphaseText.text = jQuery.i18n( "STAGING" ); break; }
            case quest_state_id.QUEST_RESOLUTION: { this._subphaseText.text = jQuery.i18n( "RESOLUTION" ); break }
        }

        this.formatSubphaseText();
        
        this._subphaseText.visible = true;
        this._subphaseIndicator.cContainer.c.visible = false;

        // Vfx.
        Utils.anim.flashCircle( new PIXI.Point( 0, 0 ), 35, 1.5, this._vfx );
    }

        private formatSubphaseText(): void
        {
            this._subphaseText.scale.set( 1 );
            if ( this._subphaseText.width > 80 )
            {
                this._subphaseText.scale.set( 80 / this._subphaseText.width );
            }
            this._subphaseText.position.y = this._phaseText.y + this._phaseText.height * 0.5 + this._subphaseText.height * 0.5 - 10;    
        }

    public setEncounterSubphase( gameSubstateId: encounter_state_id ): void
    {
        this._subphaseId = gameSubstateId;

        switch ( this._subphaseId )
        {
            case encounter_state_id.OPTIONAL_ENGAGEMENT: { this._subphaseText.text = jQuery.i18n( "OPTIONAL" ); break; }
            case encounter_state_id.ENGAGEMENT_CHECKS: { this._subphaseText.text = jQuery.i18n( "CHECKS" ); break; }
        }

        this.formatSubphaseText();

        this._subphaseText.visible = true;
        this._subphaseIndicator.cContainer.c.visible = false;

        // Vfx.
        Utils.anim.flashCircle( new PIXI.Point( 0, 0 ), 35, 1.5, this._vfx );
    }

    public setCombatSubphase( gameSubstateId: combat_state_id ): void
    {
        this._subphaseId = gameSubstateId;

        switch ( this._subphaseId )
        {
            case combat_state_id.ENEMY_ATTACKS: 
            { 
                this._subphaseText.text = jQuery.i18n( "ENEMY" ); 

                let subphaseIcon: PIXI.Sprite = this._subphaseIndicator.cContainer.c.getChildByName( "icon" ) as PIXI.Sprite;
                subphaseIcon.texture = ServiceLocator.resourceStack.findAsTexture( "rad_sauron_attack" );
                this._subphaseIndicator.cImagePreviewable.imageId = "phase_enemy_attack_resolution";
                
                Utils.game.limitSideSize( 20, subphaseIcon );
                break; 
            }

            case combat_state_id.PLAYER_ATTACKS: 
            { 
                this._subphaseText.text = jQuery.i18n( "PLAYER" ); 

                let subphaseIcon: PIXI.Sprite = this._subphaseIndicator.cContainer.c.getChildByName( "icon" ) as PIXI.Sprite;
                subphaseIcon.texture = ServiceLocator.resourceStack.findAsTexture( "rad_player_attack" );
                this._subphaseIndicator.cImagePreviewable.imageId = "phase_player_attack_resolution"; 
                
                Utils.game.limitSideSize( 20, subphaseIcon );
                break; 
            }
        }

        this.formatSubphaseText();

        this._subphaseText.visible = true;
        this._subphaseIndicator.cContainer.c.visible = true;

        // Vfx.
        Utils.anim.flashCircle( new PIXI.Point( 0, 0 ), 35, 1.5, this._vfx );
    }

    // overrides.
    public saveGame(): ISgPhaseDiagramButton
    {
        return {
            gameStateId: this._phaseId,
            gameSubstateId: this._subphaseId };
    }

    // overrides.
    public loadGame( sgPhaseDiagramButton: ISgPhaseDiagramButton, pass: number ): void
    {
        if ( pass == 0 )
        {
            this.setGamePhase( sgPhaseDiagramButton.gameStateId );
            if ( sgPhaseDiagramButton.gameSubstateId != null )
            {
                switch ( this._phaseId )
                {
                    case game_state_id.QUEST: { this.setQuestSubphase( sgPhaseDiagramButton.gameSubstateId ); break; }
                    case game_state_id.ENCOUNTER: { this.setEncounterSubphase( sgPhaseDiagramButton.gameSubstateId ); break; }
                    case game_state_id.COMBAT: { this.setCombatSubphase( sgPhaseDiagramButton.gameSubstateId ); break; }
                }
            }
        }
    }

    // #endregion //


    // #region Input Callbacks //

    private onPhaseDiagramBtn_Click(): void
    {
        let phaseDiagramView: PhaseDiagramView = new PhaseDiagramView();
        phaseDiagramView.init();
        ServiceLocator.viewManager.fadeIn( phaseDiagramView, view_layer_id.POPUP );
    }

    // #endregion //
}