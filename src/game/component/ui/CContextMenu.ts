import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CContainer from "../pixi/CContainer";
import CButton from "../input/CButton";
import SignalBinding from "../../../lib/signals/SignalBinding";


export default class CContextMenu extends Component
{
    // #region Attributes //

    private _isOptionSelected: boolean = false;
    private _menuWidth: number = null;

    // private:

    // Constants.
    private readonly _kOptionHeight: number = 30;

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();
    
        this._id = "CContextMenu";
    }

    public init(): void
    {
        console.assert( this._go.cContainer != null, "CContextMenu.ts :: init() :: CContainer component not found." );
   
        this._go.cContainer.c.visible = false;
    }

    public open( global: PIXI.IPoint, arrContextMenuOption: Array<IContextMenuOption> ): void
    {
        this._isOptionSelected = false;
        if ( this._go.cContainer.c.visible )
        {
            this.cleanOptions();
        }
        else
        {
            this._go.cContainer.c.visible = true;
        }
        this._menuWidth = this.calcMenuWidth( arrContextMenuOption );
        this.fillMenuWithOptions( arrContextMenuOption );
        this.setMenuPivot( global );
        this._go.cContainer.c.position.set( global.x, global.y );
    }

    public close()
    {
        if ( !this._go.cContainer.c.visible ) { return; }

        if ( !this._isOptionSelected )
        {
            this._go.cContainer.c.getChildAt( 0 )[ "go" ].cButton.onClick.dispatch();
        }
        this.cleanOptions();

        this._go.cContainer.c.visible = false;
    }

    // private:

    private calcMenuWidth( arrContextMenuOption: Array<IContextMenuOption> ): number
    {
        let result: number = 0;

        let longestTitle: string = null;
        for ( let contextMenuOption of arrContextMenuOption )
        {
            if ( !longestTitle || contextMenuOption.title.length > longestTitle.length )
            {
                longestTitle = contextMenuOption.title;
            }
        }

        const kTitleAux: PIXI.Text = new PIXI.Text( longestTitle, ServiceLocator.game.textStyler.normal );
        result = kTitleAux.width + 10;

        return result;
    }

    private cleanOptions(): void
    {
        for ( let displayObject of this._go.cContainer.c.children )
        {
            let option: GameObject = ( displayObject as any ).go as GameObject;
            option.end();
        }
        this._go.cContainer.c.removeChildren();
    }

    private fillMenuWithOptions( arrContextMenuOption: Array<IContextMenuOption> ): void
    {
        for ( let contextMenuOption of arrContextMenuOption )
        {
            let option: GameObject = this.createOption( contextMenuOption );
            this._go.cContainer.addChild( option );
            option.cContainer.c.y = this._kOptionHeight * ( this._go.cContainer.c.children.length - 1 );
        }
    }

    private createOption( contextMenuOption: IContextMenuOption ): GameObject
    {
        let result: GameObject = new GameObject( [ new CContainer(), new CButton() ] );
        result.init();
        let sb: SignalBinding = result.cButton.onClick.add( this.onOption_Selected, this );
        sb.params = [ contextMenuOption.cb ];

        let bg: PIXI.Graphics = new PIXI.Graphics();
        bg.lineStyle( 1, 0x000000, 1 );
        bg.beginFill( 0xeccc68 );
        bg.drawRect( 0, 0, this._menuWidth, this._kOptionHeight );
        result.cContainer.c.addChild( bg );

        let title: PIXI.Text = new PIXI.Text( contextMenuOption.title, ServiceLocator.game.textStyler.normal );
        title.anchor.set( 0, 0.5 );
        title.position.set( 5, this._kOptionHeight * 0.5 );
        result.cContainer.c.addChild( title );

        return result;
    }

    private setMenuPivot( global: PIXI.IPoint ): void
    {
        if ( global.x + this._go.cContainer.c.width > ServiceLocator.game.app.screen.width )
        {
            this._go.cContainer.c.pivot.x = this._go.cContainer.c.width;
        }
        if ( global.y + this._go.cContainer.c.height > ServiceLocator.game.app.screen.height )
        {
            this._go.cContainer.c.pivot.y = this._go.cContainer.c.height;
        }
    }

    // #endregion //


    // #region Callbacks //

    private onOption_Selected( cb: () => void ): void
    {
        this._isOptionSelected = true;
        this.close();

        cb();
    }

    // #endregion //
}

export interface IContextMenuOption
{
    title: string;
    cb(): void;
}