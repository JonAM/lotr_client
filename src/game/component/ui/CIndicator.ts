import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CTokenCounter from "./CTokenCounter";


export default abstract class CIndicator extends Component
{
    // #region Attributes //

    // protected:

    protected _iconId: string = null;
    protected _isDropArea: boolean = true;

    protected _trigger: GameObject = null;

    // private:
    
    private _isBonus: boolean = false;
    private _icon: PIXI.Sprite = null;
    private _box: PIXI.Graphics = null;
    private _text: PIXI.Text = null;

    // #endregion //


    // #region Properties //

    public get isDropArea(): boolean { return this._isDropArea; }
    public get trigger(): GameObject { return this._trigger; }

    public set isBonus( value: boolean ) { this._isBonus = value; }
    public set iconId( value: string ) { this._iconId = value; }
    public set isDropArea( value: boolean ) { this._isDropArea = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CIndicator";
    }
    
    public init(): void
    {
        super.init();
    }

    public end(): void
    {
        this._trigger.end();
        this._trigger = null;

        this._icon = null;
        this._box = null;
        this._text = null;
    }

    public setUnknown(): void
    {
        this._text.text = "?";
    }

    // virtual.
    public setIconId( iconId: string ): void
    {
        this._iconId = iconId;

        if ( this._iconId )
        {
            this._icon.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( this._iconId ).data );
            Utils.game.limitSideSize( 40, this._icon );
            this._icon.visible = true;
        }
        else
        {
            this._icon.visible = false;
        }
    }

    public setTextColor( color: number ): void
    {
        this._text.tint = color;
    }

    // protected:

    protected createTriggerBox(): void
    {
        const kIconId: string = this._iconId ? this._iconId : "transparent";
        this._icon = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( kIconId ).data ) );
        this._icon.name = "icon";
        Utils.game.limitSideSize( 40, this._icon );
        this._icon.anchor.set( 0.5, 0 );
        this._trigger.cContainer.c.addChild( this._icon );
        if ( !this._iconId )
        {
            this._icon.visible = false;
        }

        this._box = new PIXI.Graphics();
        this._box.lineStyle( 1, 0x000000, 1 );
        this._box.beginFill( 0xeccc68, 1 );
        this._box.drawRect( -16, -13, 32, 26 );
        this._box.endFill();
        this._box.position.set( 0, this._iconId ? this._icon.height + 13 - 5 : 13 );
        this._trigger.cContainer.c.addChild( this._box );

        this._text = new PIXI.Text( "0", ServiceLocator.game.textStyler.normal );
        this._text.anchor.set( 0.5 );
        this._box.addChild( this._text );
    }

    // #endregion //


    // #region Callbacks //

    protected onCount_Updated( count: number, delta: number ): void
    {
        let countStr: string = count.toString();
        if ( this._isBonus && count > 0 )
        {
            countStr = "+" + countStr;
        }
        this._text.text = countStr;

        CTokenCounter.animate( this._box, delta );
    }

    // #endregion //
}