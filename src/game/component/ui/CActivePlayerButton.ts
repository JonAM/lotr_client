import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";
import * as particles from "pixi-particles";
import { player_type } from "../world/CPlayerArea";
import { ISgEndTurnBtn } from "../../../view/game/SaveGameView";
import { player_action_type } from "../../../service/socket_io/GameSocketIOController";
import GameObject from "../../GameObject";
import CContainer from "../pixi/CContainer";
import { isBindingName } from "typescript";
import { input_event_type } from "../../InputController";


export default class CActivePlayerButton extends Component
{
    // #region Attributes //

    // private:

    private _playerType: player_type = null;

    private _particleContainer: PIXI.Container = null;
    private _activePlayerEmitter: particles.Emitter = null;
    private _poiSocket: GameObject = null;

    // #endregion //


    // #region Properties //

    public set playerType( value: player_type ) { this._playerType = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CActivePlayerButton";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CActivePlayerButton.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cButton != null, "CActivePlayerButton.ts :: init() :: CButton component not found." );
        console.assert( this._go.cTooltipReceptor != null, "CActivePlayerButton.ts :: init() :: CTooltipReceptor component not found." );
        console.assert( this._playerType != null, "CActivePlayerButton.ts :: init() :: this._playerType cannot be null." );
        
        this._go.cTooltipReceptor.text = jQuery.i18n( "ACTIVE_PLAYER" );

        let bg: PIXI.Graphics = new PIXI.Graphics();
        bg.lineStyle( 2, ServiceLocator.game.playerColors[ this._playerType ], 1 );
        bg.beginFill(  ServiceLocator.game.playerColors[ this._playerType ], 0.6 );
        bg.drawCircle( 0, 0, 30 );
        this._go.cContainer.c.addChild( bg );

        this._particleContainer = new PIXI.Container();
        this._go.cContainer.c.addChild( this._particleContainer );

        this._activePlayerEmitter = this.createActivePlayerEmitter();
        
        let icon: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( "active_player" ) );
        icon.anchor.set( 0.5 );
        Utils.game.limitSideSize( 60, icon );
        this._go.cContainer.c.addChild( icon );

        // POI socket.
        this._poiSocket = new GameObject( [ new CContainer() ] );
        this._poiSocket.cContainer.c.name = "poi_socket";
        this._poiSocket.init();
        this._poiSocket.cContainer.c.y -= bg.height * 0.5 - 5;
        this._go.cContainer.addChild( this._poiSocket );
        //
        this._go.cHighlightPoiReceptor.poiSocket = this._poiSocket;

        // Listen to events.
        ServiceLocator.game.inputController.on( this._go.cContainer.c, input_event_type.DOWN, this.onPointer_Down, this );
        ServiceLocator.game.inputController.on( this._go.cContainer.c, input_event_type.UP, this.onPointer_Up, this );
        ServiceLocator.game.inputController.on( this._go.cContainer.c, input_event_type.OUT, this.onPointer_Up, this );
        this._go.cButton.onClick.add( this.onClick, this );
    }

    public end(): void
    {
        this._go.cButton.onClick.remove( this.onClick, this );
        ServiceLocator.game.inputController.off( this._go.cContainer.c, input_event_type.DOWN, this.onPointer_Down, this );
        ServiceLocator.game.inputController.off( this._go.cContainer.c, input_event_type.UP, this.onPointer_Up, this );
        ServiceLocator.game.inputController.off( this._go.cContainer.c, input_event_type.OUT, this.onPointer_Up, this );

        this._activePlayerEmitter.destroy();
        this._activePlayerEmitter = null;

        this._poiSocket.end();
        this._poiSocket = null;

        super.end();
    }

    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        this._go.cButton.setEnabled( isEnabled );
    }

    public show(): void
    {
        this._activePlayerEmitter.emit = true;

        this._go.cContainer.c.visible = true;

        if ( ServiceLocator.game.isAnimated )
        {
            // Sfx.
            ServiceLocator.audioManager.playSfx( "player_activated" );
        }
    }

    public hide(): void
    {
        this._activePlayerEmitter.emit = false;

        this._go.cContainer.c.visible = false;
    }

    // private:

    private createActivePlayerEmitter(): particles.Emitter
    {
        let emitter: particles.Emitter = new particles.Emitter(
            this._particleContainer,
            [ 
                
                PIXI.Texture.from( ServiceLocator.resourceStack.find( "particle_smoke" ).data ), 
                PIXI.Texture.from( ServiceLocator.resourceStack.find( "particle_default" ).data ) 
            ],
            ServiceLocator.resourceStack.find( "emitter_active_player" ).data );
        let arrColor: Array<number> = new Array<number>();
        PIXI.utils.hex2rgb( ServiceLocator.game.playerColors[ this._playerType ], arrColor );
        emitter.startColor.value.r = arrColor[ 0 ] * 255;
        emitter.startColor.value.g = arrColor[ 1 ] * 255;
        emitter.startColor.value.b = arrColor[ 2 ] * 255;
        emitter.autoUpdate = true;
        emitter.emit = false;

        return emitter;
    }

    // #endregion //


    // #region Input Callbacks //

    private onPointer_Down( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled ) { return; }

        this._go.cContainer.c.scale.set( 0.9 );
    }

    private onPointer_Up( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled ) { return; }

        this._go.cContainer.c.scale.set( 1 );
    }

    private onClick(): void
    {
        ServiceLocator.game.setActivePlayer( player_type.ALLY );

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_ACTIVE_PLAYER, null, null );
    }

    // #endregion //
}