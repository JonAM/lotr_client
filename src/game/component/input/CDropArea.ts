import Component from "../Component";

import { player_type } from "../world/CPlayerArea";
import ServiceLocator from "../../../ServiceLocator";
import * as PIXI from "pixi.js";

import IGameObjectDropArea from "../../IGameObjectDropArea";
import GameObject from "../../GameObject";
import Signal from "../../../lib/signals/Signal";
import Utils from "../../../Utils";
import LogTargetCard from "../ui/right_menu/action_log/target/LogTargetCard";
import { location_type } from "../world/CGameWorld";
import LogTarget from "../ui/right_menu/action_log/LogTarget";
import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import LogTargetPlayer from "../ui/right_menu/action_log/target/LogTargetPlayer";


export default class CDropArea extends Component
{
    // #region Attributes //

    // private:

    private _target: IGameObjectDropArea = null;
    private _isPropagate: boolean = false;
    private _isSelfDropAllowed: boolean = false;

    // Bindend functions.
    private _bfOnPointer_Up: Function = null;

    // Signals.
    private _onDroppedOut: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get target(): IGameObjectDropArea { return this._target; }
    public get isPropagate(): boolean { return this._isPropagate; }
    public static get kPredefinedDropPositionCode(): number { return -9999; }

    public set target( value: IGameObjectDropArea ) { this._target = value; }
    public set isPropagate( value: boolean ) { this._isPropagate = value; }
    public set isSelfDropAllowed( value: boolean ) { this._isSelfDropAllowed = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CDropArea";

        // Binded functions.
        this._bfOnPointer_Up = this.onPointer_Up.bind( this )
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CDropArea.ts :: init() :: CContainer component not found." );
        console.assert( this._target != null, "CDropArea.ts :: init() :: this._target cannot be null." );

        this._go.cContainer.c.interactive = true;

        // Listen to events.
        this._go.cContainer.c.on( "pointerup", this._bfOnPointer_Up );
    }

    public end(): void
    {
        this._onDroppedOut.removeAll();

        // Cleanup events.
        this._go.cContainer.c.off( "pointerup", this._bfOnPointer_Up );
    }

    public forceDrop( dropped: GameObject, global: PIXI.IPoint, actionScopeType: action_scope_type ): void
    {
        dropped.cDraggable.drop( this._go, actionScopeType );
        let onPostDropped: Signal = dropped.cDraggable.onPostDropped;
        dropped.cDraggable.resetOnPostDropped();
        dropped.cDraggable.removeListeners();

        this._target.processDroppedGameObject( dropped, global, actionScopeType );

        onPostDropped.dispatch( dropped, this._go, actionScopeType );
        onPostDropped.removeAll();
    }

    // private:

    private findPropagatedDropArea(): CDropArea
    {
        let result: CDropArea = null;

        let current: PIXI.Container = this._go.cContainer.c.parent;
        while ( !result && current != ServiceLocator.game.app.stage )
        {
            let go: GameObject = current[ "go" ];
            if ( go && go.cDropArea )
            {
                result = go.cDropArea;
            }
            else
            {
                current = current.parent;
            }
        }

        return result;
    }

    private log( dropped: GameObject, fromLocation: location_type, toLocation: location_type ): void
    {
        let logTarget: LogTarget = null;
        if ( dropped.cCard || dropped.cAttachment || dropped.cCardToken || dropped.cGameModifier )
        {
            logTarget = new LogTargetCard( dropped );
        }
        else if ( dropped.cShadowCardMini )
        {
            logTarget= new LogTargetCard( dropped.cShadowCardMini.card );
        }
        else if ( dropped.cPlayerToken )
        {
            logTarget = new LogTargetPlayer( player_type.PLAYER );
        }

        const kFromIconId: string = Utils.img.findLocationTextureId( fromLocation );
        const kToIconId: string = Utils.img.findLocationTextureId( toLocation );
        ServiceLocator.game.cGameWorld.cActionLogger.logSequence( 
            player_type.PLAYER, logTarget, kFromIconId, kToIconId, true );
    }

    // #endregion //


    // #region Input Callbacks //

    public onPointer_Up( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled ) { return; }
        
        if ( ServiceLocator.game.dragShadowManager.dragShadow )
        {
            let isDone: boolean = false;
            const kDropped: GameObject = ServiceLocator.game.dragShadowManager.dragShadow.cDragShadow.from;
            
            const kFrom: GameObject = Utils.game.findGameObjectInBranchByComponentName( kDropped, "CDropArea" )
            if ( !this._isSelfDropAllowed && kFrom == this._go )
            {
                kDropped.cDraggable.cancel();
                isDone = true;
            }

            if ( !isDone && this._target.validateDroppedGameObject( kDropped, kFrom, event.data.global ) )
            {
                if ( this._go.cLogLocation )
                {
                    let from: GameObject = Utils.game.findGameObjectInBranchByComponentName( kDropped, "CLogLocation" );
                    if ( from && from != this._go )
                    {
                        this.log( kDropped, from.cLogLocation.location, this._go.cLogLocation.location );
                    }
                }

                this.forceDrop( kDropped, event.data.global, action_scope_type.MULTIPLAYER );

                isDone = true;
            }
            
            if ( !isDone )
            {
                let propagatedDropArea: CDropArea = null;
                if ( this._isPropagate )
                {
                    propagatedDropArea = this.findPropagatedDropArea();
                }
                
                if ( propagatedDropArea )
                {
                    propagatedDropArea.onPointer_Up( event );
                }
                else
                {
                    kDropped.cDraggable.cancel();
                }
            }

            ServiceLocator.game.dragShadowManager.setDragShadow( null );
        }
    }

    // #endregion //
}