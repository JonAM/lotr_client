import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import * as PIXI from "pixi.js";
import Signal from "../../../lib/signals/Signal";
import { input_event_type } from "../../InputController";
import { view_layer_id } from "../../../service/ViewManager";


export default class CLogActionPreviewable extends Component
{
    // #region Attributes //

    // private:

    // Signals.
    private _onPreviewShown: Signal = new Signal();
    private _onPreviewHidden: Signal = new Signal();

    // #endregion //


    // #region Properties //

    // Signals.
    public get onPreviewShown(): Signal { return this._onPreviewShown; }
    public get onPreviewHidden(): Signal { return this._onPreviewHidden; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CLogActionPreviewable";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CLogActionPreviewable.ts :: init() :: CContainer component not found." );

        this._go.cContainer.c.interactive = true;

        // Attach event handlers.
        ServiceLocator.game.inputController.on( this._go.cContainer.c, input_event_type.OVER, this.onPointer_Over, this );
        ServiceLocator.game.inputController.on( this._go.cContainer.c, input_event_type.OUT, this.onPointer_Out, this );
    }

    public end(): void
    {
        this._onPreviewShown.removeAll();
        this._onPreviewHidden.removeAll();

        // Detach event handlers.
        ServiceLocator.game.inputController.off( this._go.cContainer.c, input_event_type.OVER, this.onPointer_Over, this );
        ServiceLocator.game.inputController.off( this._go.cContainer.c, input_event_type.OUT, this.onPointer_Out, this );
    }

    // #endregion //

    
    // #region Input Callbacks //

    private onPointer_Over( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled ) { return; }

        if ( !ServiceLocator.game.dragShadowManager.dragShadow
            && ServiceLocator.viewManager.findViewCount( view_layer_id.POPUP ) == 0
            && ServiceLocator.viewManager.findViewCount( view_layer_id.TOPMOST ) == 0 )
        {
            ServiceLocator.game.actionLogItemPreview.cLogActionPreview.show( this._go.cLogAction, event.data.global );
            this._onPreviewShown.dispatch();
        }
    }

    private onPointer_Out( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled ) { return; }
        
        ServiceLocator.game.actionLogItemPreview.cLogActionPreview.hide();
        this._onPreviewHidden.dispatch();
    }

    // #endregion //
}