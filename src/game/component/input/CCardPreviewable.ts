import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import * as PIXI from "pixi.js";
import { input_event_type } from "../../InputController";
import { view_layer_id } from "../../../service/ViewManager";


export default class CCardPreviewable extends Component
{
    // #region Attributes //

    // private:

    private _cardId: string = null;
    private _textureId: string = null;

    private _kBlackList: Array<string> = null;

    // #endregion //


    // #region Properties //

    public get cardId(): string { return this._cardId; }
    public get textureId(): string { return this._textureId; }

    public set cardId( value: string ) { this._cardId = value; }
    public set textureId( value: string ) { this._textureId = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CCardPreviewable";

        this._kBlackList = [ "999002", "999003" ];
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CCardPreviewable.ts :: init() :: CContainer component not found." );

        this._go.cContainer.c.interactive = true;

        // Attach event handlers.
        ServiceLocator.game.inputController.on( this._go.cContainer.c, input_event_type.OVER, this.onPointer_Over, this );
        ServiceLocator.game.inputController.on( this._go.cContainer.c, input_event_type.OUT, this.onPointer_Out, this );
    }

    public end(): void
    {
        // Detach event handlers.
        ServiceLocator.game.inputController.off( this._go.cContainer.c, input_event_type.OVER, this.onPointer_Over, this );
        ServiceLocator.game.inputController.off( this._go.cContainer.c, input_event_type.OUT, this.onPointer_Out, this );
    }

    public refresh(): void
    {
        const kMouseGlobal: PIXI.Point = ServiceLocator.game.app.renderer.plugins.interaction.mouse.global;
        if ( this._go.cContainer.c.getBounds().contains( kMouseGlobal.x, kMouseGlobal.y ) )
        {  
            this.onPointer_Over( null );
        }
    }

    // #endregion //


    // #region Input Callbacks //

    private onPointer_Over( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled ) { return; }

        if ( !ServiceLocator.game.dragShadowManager.dragShadow 
            && ServiceLocator.viewManager.findViewCount( view_layer_id.POPUP ) == 0
            && ServiceLocator.viewManager.findViewCount( view_layer_id.TOPMOST ) == 0
            && ( !this._go.cDraggable || !this._go.cDraggable.isDragging )
            && this._kBlackList.indexOf( this._cardId ) == -1 )
        {
            ServiceLocator.game.cardPreview.cCardPreview.show( this._cardId, this._go );
        }
    }

    private onPointer_Out( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled ) { return; }
        
        ServiceLocator.game.cardPreview.cCardPreview.hide();
    }

    // #endregion //
}