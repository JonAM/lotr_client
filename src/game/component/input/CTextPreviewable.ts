import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import * as PIXI from "pixi.js";
import { input_event_type } from "../../InputController";
import PointerInputController from "../../input/PointerInputController";
import { view_layer_id } from "../../../service/ViewManager";


export default class CTextPreviewable extends Component
{
    // #region Attributes //

    // private:

    private _text: string = null;
    private _description: string = null;

    // #endregion //


    // #region Properties //

    public get text(): string { return this._text; }
    public get description(): string { return this._description; }

    public set text( value: string ) { this._text = value; }
    public set description( value: string ) { this._description = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CTextPreviewable";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CTextPreviewable.ts :: init() :: CContainer component not found." );

        this._go.cContainer.c.interactive = true;

        // Attach event handlers.
        ServiceLocator.game.inputController.on( this._go.cContainer.c, input_event_type.OVER, this.onPointer_Over, this );
        ServiceLocator.game.inputController.on( this._go.cContainer.c, input_event_type.OUT, this.onPointer_Out, this );
        if ( ServiceLocator.game.inputController instanceof PointerInputController )
        {
            this._go.cContainer.c.on( "pointerdown", this.onPointer_Out, this );
        }
    }

    public end(): void
    {
        // Detach event handlers.
        ServiceLocator.game.inputController.off( this._go.cContainer.c, input_event_type.OVER, this.onPointer_Over, this );
        ServiceLocator.game.inputController.off( this._go.cContainer.c, input_event_type.OUT, this.onPointer_Out, this );
        if ( ServiceLocator.game.inputController instanceof PointerInputController )
        {
            this._go.cContainer.c.off( "pointerdown", this.onPointer_Out, this );
        }
    }

    // #endregion //


    // #region Input Callbacks //

    private onPointer_Over( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled ) { return; }

        if ( !ServiceLocator.game.dragShadowManager.dragShadow 
            && ServiceLocator.viewManager.findViewCount( view_layer_id.POPUP ) == 0
            && ServiceLocator.viewManager.findViewCount( view_layer_id.TOPMOST ) == 0
            && ( !this._go.cDraggable || !this._go.cDraggable.isDragging ) )
        {
            ServiceLocator.game.textPreview.cTextPreview.show( this, event.data.global );
        }
    }

    private onPointer_Out( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled ) { return; }
        
        ServiceLocator.game.textPreview.cTextPreview.hide();
    }

    // #endregion //
}