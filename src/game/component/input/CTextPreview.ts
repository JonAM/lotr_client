import ServiceLocator from "../../../ServiceLocator";
import * as PIXI from "pixi.js";

import Component from "../Component";
import CTextPreviewable from "./CTextPreviewable";
import GameObject from "../../GameObject";
import Utils from "../../../Utils";


export default class CTextPreview extends Component
{
    // #region Attributes //

    // private:

    private _textPreviewable: GameObject = null;
    private _bg: PIXI.Graphics = null;
    private _text: PIXI.Text = null;
    private _description: PIXI.Text = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CTextPreview";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CTextPreview.ts :: init() :: CContainer component not found." );

        this._go.cContainer.c.visible = false;

        this._bg = new PIXI.Graphics();
        this._go.cContainer.c.addChild( this._bg );

        let textStyle: PIXI.TextStyle = ServiceLocator.game.textStyler.normal;
        textStyle.wordWrap = true;
        textStyle.wordWrapWidth = 300;
        this._text = new PIXI.Text( "", textStyle );
        this._text.position.set( 10, 10 );
        this._go.cContainer.c.addChild( this._text );

        textStyle = ServiceLocator.game.textStyler.tooltipDescription;
        textStyle.wordWrap = true;
        textStyle.wordWrapWidth = 300;
        this._description = new PIXI.Text( "", textStyle );
        this._description.position.set( 10, this._text.y + this._text.height + 5 );
        this._go.cContainer.c.addChild( this._description );
        this._description.visible = false;
    }

    public show( cTextPreviewable: CTextPreviewable, position: PIXI.Point ): void
    {
        if ( !this._go.cContainer.c.visible )
        {
            this._text.text = cTextPreviewable.text;
            this._description.visible = cTextPreviewable.description != null;
            if ( this._description.visible )
            {
                this._description.text = cTextPreviewable.description;
            }
            this._textPreviewable = cTextPreviewable.go;
            this._textPreviewable.onEnded.addOnce( this.onTextPreviewable_Ended, this );

            this._bg.clear();
            this._bg.lineStyle( 1, 0x00000, 1 );
            this._bg.beginFill( 0xefe4b0, 1 );
            this._bg.drawRect( 0, 0, this._go.cContainer.c.width + 20, this._go.cContainer.c.height + 20 );
            this._bg.endFill();
        }
        this._go.cContainer.c.visible = true;

        Utils.game.setPreviewPosition( this._go.cContainer.c, this._textPreviewable.cContainer.c.getGlobalPosition(), this._textPreviewable.cContainer.c.getLocalBounds() );
    }

    public hide(): void
    {
        this._go.cContainer.c.visible = false;
        if ( this._textPreviewable )
        {
            this._textPreviewable.onEnded.remove( this.onTextPreviewable_Ended, this );
            this._textPreviewable = null;
        }
    }

    // #endregion //


    // #region Callbacks //

    private onTextPreviewable_Ended( go: GameObject ): void
    {
        if ( go == this._textPreviewable )
        {
            this.hide();
        }
    }

    // #endregion //
}