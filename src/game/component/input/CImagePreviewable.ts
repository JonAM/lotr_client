import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import * as PIXI from "pixi.js";
import { input_event_type } from "../../InputController";
import { view_layer_id } from "../../../service/ViewManager";


export default class CImagePreviewable extends Component
{
    // #region Attributes //

    // private:

    private _imageId: string = null;

    // #endregion //


    // #region Properties //
    
    public get imageId(): string { return this._imageId; }

    public set imageId( value: string ) { this._imageId = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CImagePreviewable";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CImagePreviewable.ts :: init() :: CContainer component not found." );

        this._go.cContainer.c.interactive = true;

        // Attach event handlers.
        ServiceLocator.game.inputController.on( this._go.cContainer.c, input_event_type.OVER, this.onPointer_Over, this );
        ServiceLocator.game.inputController.on( this._go.cContainer.c, input_event_type.OUT, this.onPointer_Out, this );
    }

    public end(): void
    {
        // Detach event handlers.
        ServiceLocator.game.inputController.off( this._go.cContainer.c, input_event_type.OVER, this.onPointer_Over, this );
        ServiceLocator.game.inputController.off( this._go.cContainer.c, input_event_type.OUT, this.onPointer_Out, this );
    }

    // #endregion //


    // #region Input Callbacks //

    private onPointer_Over( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled 
            || ServiceLocator.game.dragShadowManager.dragShadow 
            || ServiceLocator.viewManager.findViewCount( view_layer_id.POPUP ) > 0
            || ServiceLocator.viewManager.findViewCount( view_layer_id.TOPMOST ) > 0
            || ( this._go.cDraggable && this._go.cDraggable.isDragging ) ) { return; }

        ServiceLocator.game.imagePreview.cImagePreview.show( this._imageId, this._go );
    }

    private onPointer_Out( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled ) { return; }
        
        if ( ServiceLocator.game.imagePreview.cImagePreview.isTarget( this._go ) ) 
        {
            ServiceLocator.game.imagePreview.cImagePreview.hide(); 
        }
    }

    // #endregion //
}