import Component from "../Component";

import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../ServiceLocator";
import * as PIXI from "pixi.js";

import Signal from "../../../lib/signals/Signal";
import GameObject from "../../GameObject";
import CDragShadow from "./CDragShadow";
import CContainer from "../pixi/CContainer";
import IDragShadowTextureCreator from "../../IDragShadowTextureCreator";
import CDampedSpring from "../CDampedSpring";
import { layer_type } from "../world/CGameLayerProvider";
import SignalBinding from "../../../lib/signals/SignalBinding";
import Session from "../../../Session";


export default class CDraggable extends Component
{
    // #region Attributes //

    // private:

    private _dragShadowTextureCreator: IDragShadowTextureCreator = null;
    private _isTapOnly: boolean = false;
    private _isDragFilter: boolean = true;
    private _anchor: PIXI.Point = null;

    private _offset: PIXI.IPoint = null;
    private _dragShadow: GameObject = null;
    private _dragFilter: PIXI.filters.NoiseFilter = null;

    private _pointerDownPosition: PIXI.Point = null;

    // Bindend functions.
    private _bfOnPointer_Down: Function = null;
    private _bfOnPointer_Up: Function = null;
    private _bfOnPointer_UpOutside: Function = null;
    private _bfOnPointer_Move: Function = null;

    // Signals.
    private _onDragged: Signal = new Signal();
    private _onPostDragged: Signal = new Signal();
    private _onDragCancelled: Signal = new Signal();
    private _onDropped: Signal = new Signal();
    private _onPostDropped: Signal = new Signal();

    // Constants.
    private _kDragAlpha: number = 0.45;

    // #endregion //


    // #region Properties //

    public get isDragging(): boolean { return this._dragShadow != null; }
    public get offset(): PIXI.IPoint { return this._offset; }
 
    public set dragShadowTextureCreator( value: IDragShadowTextureCreator ) { this._dragShadowTextureCreator = value; }
    public set isTapOnly( value: boolean ) { this._isTapOnly = value; }
    public set isDragFilter( value: boolean ) { this._isDragFilter = value; }
    public set anchor( value: PIXI.Point ) { this._anchor = value; }

    // Signals.
    public get onDragged(): Signal { return this._onDragged; }
    public get onPostDragged(): Signal { return this._onPostDragged; }
    public get onDragCancelled(): Signal { return this._onDragCancelled; }
    public get onDropped(): Signal { return this._onDropped; }
    public get onPostDropped(): Signal { return this._onPostDropped; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CDraggable";

        // Binded functions.
        this._bfOnPointer_Down = this.onPointer_Down.bind( this );
        this._bfOnPointer_Up = this.onPointer_Up.bind( this );
        this._bfOnPointer_UpOutside = this.onPointer_UpOutside.bind( this );
        this._bfOnPointer_Move = this.onPointer_Move.bind( this );
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CDraggable.ts :: init() :: CContainer or CSprite components not found." );

        this._go.cContainer.c.interactive = true;
        this._go.cContainer.c.buttonMode = true;

        // Attach event handlers.
        this._go.cContainer.c.on( "pointerdown", this._bfOnPointer_Down );
        this._go.cContainer.c.on( "pointerup", this._bfOnPointer_Up );
        this._go.cContainer.c.on( "pointerupoutside", this._bfOnPointer_UpOutside );
    }

    public end(): void
    {
        this.removeListeners();

        // Cleanup event handlers.
        this._go.cContainer.c.off( "pointerdown", this._bfOnPointer_Down );
        this._go.cContainer.c.off( "pointerup", this._bfOnPointer_Up );
        this._go.cContainer.c.off( "pointerupoutside", this._bfOnPointer_UpOutside );
        this._go.cContainer.c.off( "pointermove", this._bfOnPointer_Move );
    }

    public forceDrag( event: PIXI.InteractionEvent ): void
    {
        if ( this._isTapOnly || !this._isEnabled || ServiceLocator.game.poiMenu.cContainer.c.visible ) { return; }
        
        this._pointerDownPosition = new PIXI.Point( event.data.global.x, event.data.global.y );
        this.startDrag( event );
    }

    public drop( dropArea: GameObject, actionScopeType: action_scope_type ): void
    {
        let isForced: boolean = this._dragShadow == null;
        if ( !isForced )
        {
            this.endDrag();
        }
        
        this._onDropped.dispatch( this._go, dropArea, actionScopeType );
    }

    public cancel(): void
    {
        let isForced: boolean = this._dragShadow == null;
        if ( !isForced )
        {
            this.endDrag();
        }

        this._onDragCancelled.dispatch( this._go );
    }

    public setDragFilter(): void
    {
        this._dragFilter = new PIXI.filters.NoiseFilter();
        if ( !this._go.cContainer.c.filters )
        {
            this._go.cContainer.c.filters = [ this._dragFilter ];
        }
        else
        {
            this._go.cContainer.c.filters.push( this._dragFilter );
        }
    }

    public removeDragFilter(): void
    {
        if ( this._go.cContainer.c.filters )
        {
            this._go.cContainer.c.filters.splice( this._go.cContainer.c.filters.indexOf( this._dragFilter ), 1 );
            this._dragFilter = null;
        }
    }

    public removeListeners(): void
    {
        this._onDragged.removeAll();
        this._onPostDragged.removeAll();
        this._onDragCancelled.removeAll();
        this._onDropped.removeAll();
        this._onPostDropped.removeAll();
    }

    public resetOnPostDropped(): void
    {
        this._onPostDropped = new Signal();
    }

    // overrides.
    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        this._go.cContainer.c.buttonMode = isEnabled;
    }

    // private: 

    private tryStartDrag( event: PIXI.InteractionEvent ): void
    {
        // Multiplayer.
        let isShareable: boolean = this._go.cShareableGameElement && this._go.cShareableGameElement.isEnabled;
        if ( Session.allyId && isShareable )
        {
            let sb: SignalBinding = this._go.cShareableGameElement.onLockOpen.addOnce( this.startDrag, this );
            sb.params = [ event ];
            this._go.cShareableGameElement.notifyLock();
        }
        else
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_DRAG_FILTER, null, [ this._go.oid ] );

            this.startDrag( event );
        }
    }

        private startDrag( event: PIXI.InteractionEvent ): void
        {
            // "pointerup" event happened before lock status response.
            if ( this._pointerDownPosition == null ) 
            { 
                // Multiplayer.
                if ( this._go.cShareableGameElement && this._go.cShareableGameElement.isEnabled )
                {
                    this._go.cShareableGameElement.notifyUnlock();
                }
                return; 
            } 

            ServiceLocator.game.dragShadowManager.isDragging = true;
            
            this._onDragged.dispatch( this._go, event );
            
            this._dragShadow = null;
            this._pointerDownPosition = null;
            this._go.cContainer.c.off( "pointermove", this._bfOnPointer_Move );

            // OJO: Is it safe to allow self dropping?
            /*this._go.cContainer.c.interactive = false;
            this._go.cContainer.c.interactiveChildren = false;*/

            this.setOffset( event );
            if ( this._isDragFilter )
            {
                this.setDragFilter();
            }
            this.setDragShadow( event );

            this._onPostDragged.dispatch( this._go, event );
        }

            private setOffset( event: PIXI.InteractionEvent ): void
            {
                this._offset = new PIXI.Point();
                if ( this._go.cContainer.c.parent )
                {
                    const kGlobal: PIXI.Point = this._go.cContainer.c.getGlobalPosition();
                    // TEST: Fixed offset.
                    //const kLocal: PIXI.Point = new PIXI.Point( event.data.global.x - kGlobal.x, event.data.global.y - kGlobal.y );
                    const kLocal: PIXI.Point = new PIXI.Point( -10, -10 );
                    if ( !this._anchor )
                    {
                        this._anchor = new PIXI.Point();
                        if ( this._go.cSprite )
                        {
                            this._anchor.copyFrom( this._go.cSprite.s.anchor );
                        }
                    }
                    this._offset.x = ( kLocal.x + ( this._anchor.x * this._go.cContainer.c.width ) ) / this._go.cContainer.c.width;
                    this._offset.y = ( kLocal.y + ( this._anchor.y * this._go.cContainer.c.height ) ) / this._go.cContainer.c.height;
                }
            }

            private setDragShadow( event: PIXI.InteractionEvent ): void
            {
                this._dragShadow = new GameObject( [ new CContainer(), new CDragShadow(), new CDampedSpring() ] );
                this._dragShadow.cDragShadow.from = this._go;
                if ( this._dragShadowTextureCreator )
                {
                    this._dragShadow.cDragShadow.texture = this._dragShadowTextureCreator.createDragShadowTexture();
                }
                else
                {
                    let dragShadowTexture = PIXI.RenderTexture.create( { width: this._go.cContainer.c.width, height: this._go.cContainer.c.height } );
                    let pos: PIXI.Point = new PIXI.Point();
                    this._go.cContainer.c.position.copyTo( pos );
                    this._go.cContainer.c.position.set( 0 );
                    ServiceLocator.game.app.renderer.render( this._go.cContainer.c, dragShadowTexture );      
                    pos.copyTo( this._go.cContainer.c.position );
                    this._dragShadow.cDragShadow.texture = dragShadowTexture;
                }
                this._dragShadow.cDragShadow.alpha = this._kDragAlpha;
                this._dragShadow.cDampedSpring.springConstant = 0.2;
                this._dragShadow.init();
                this._dragShadow.cContainer.c.position.set( 
                    event.data.global.x - this._offset.x * this._dragShadow.cContainer.c.width, 
                    event.data.global.y - this._offset.y * this._dragShadow.cContainer.c.height );
                ServiceLocator.game.root.cGameLayerProvider.add( this._dragShadow, layer_type.HUD );

                ServiceLocator.game.dragShadowManager.setDragShadow( this._dragShadow );
            }

    private endDrag(): void
    {
        // OJO: Is it safe to allow self dropping?
        /*this._go.cContainer.c.interactive = true;
        this._go.cContainer.c.interactiveChildren = true;*/

        this._dragShadow.end();
        this._dragShadow = null;

        if ( this._isDragFilter )
        {
            this.removeDragFilter();
        }

        // Multiplayer.
        if ( this._go.cShareableGameElement && this._go.cShareableGameElement.isEnabled )
        {
            this._go.cShareableGameElement.notifyUnlock();
        }
        else
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.REMOVE_DRAG_FILTER, null, [ this._go.oid ] );
        }
    }

    // #endregion //


    // #region Input Callbacks //

    private onPointer_Down( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled || ServiceLocator.game.poiMenu.cContainer.c.visible || ServiceLocator.game.dragShadowManager.isDetectingDrag ) { return; }

        ServiceLocator.game.dragShadowManager.isDetectingDrag = true;

        this._pointerDownPosition = new PIXI.Point( event.data.global.x, event.data.global.y );
        this._go.cContainer.c.on( "pointermove", this._bfOnPointer_Move );
    }

    private onPointer_Up( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled ) { return; }
        
        this._go.cContainer.c.off( "pointermove", this._bfOnPointer_Move );
        this._pointerDownPosition = null;
    }

    private onPointer_UpOutside( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled ) { return; }

        this._pointerDownPosition = null;
        this._go.cContainer.c.off( "pointermove", this._bfOnPointer_Move );
    }

    private onPointer_Move( event: PIXI.InteractionEvent ): void
    {
        if ( this._isTapOnly || !this._isEnabled || ServiceLocator.game.poiMenu.cContainer.c.visible ) { return; }
        
        if ( this._pointerDownPosition )
        {
            const kDistance: number = Math.pow( event.data.global.x - this._pointerDownPosition.x, 2 ) 
                + Math.pow( event.data.global.y - this._pointerDownPosition.y, 2 );
            if ( kDistance > 400 )
            {
                this._go.cContainer.c.off( "pointermove", this._bfOnPointer_Move );
                this.tryStartDrag( event );
            }
        }
    }

    // #endregion //
}