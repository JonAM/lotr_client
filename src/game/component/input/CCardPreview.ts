import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";
import GameObject from "../../GameObject";
import { ICard } from "../../CardDB";


export default class CCardPreview extends Component
{
    // #region Attributes //

    // private:

    private _card: PIXI.Sprite = null;
    private _errataBg: PIXI.Graphics = null;
    private _errataText: PIXI.Text = null;

    private _target: GameObject = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CCardPreview";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CCardPreview.ts :: init() :: CContainer component not found." );

        this._card = new PIXI.Sprite();
        this._go.cContainer.c.addChild( this._card );

        this._errataBg = new PIXI.Graphics();
        this._go.cContainer.c.addChild( this._errataBg );
        //
        let errataTitle: PIXI.Text = new PIXI.Text( jQuery.i18n( "ERRATA" ), ServiceLocator.game.textStyler.normal );
        errataTitle.position.set( 10 );
        this._errataBg.addChild( errataTitle );
        //
        this._errataText = new PIXI.Text( "" );
        this._errataText.position.set( 10, errataTitle.y + errataTitle.height + 5 );
        this._errataBg.addChild( this._errataText );

        this._go.cContainer.c.visible = false;

        // Listen to events.
        ServiceLocator.game.onOngoingInteractionCanceled.add( this.hide, this );
    }

    public end(): void
    {
        // Cleanup events.
        ServiceLocator.game.onOngoingInteractionCanceled.remove( this.hide, this );

        super.end();
    }

    public isVisible(): boolean
    {
        return this._go.cContainer.c.visible;
    }

    public isTarget( go: GameObject ): boolean
    {
        return this._target == go;
    }

    public show( cardId: string, target: GameObject ): void
    {
        if ( !this._go.cContainer.c.visible )
        {
            this._card.texture = Utils.img.findCardTexture( cardId );
            
            //this._card.scale.set( 1.25 );

            const kCardInfo: ICard = ServiceLocator.cardDb.find( cardId );
            if ( kCardInfo.has_errata )
            {
                let textStyle: PIXI.TextStyle = ServiceLocator.game.textStyler.tooltipDescription;
                textStyle.wordWrap = true;
                textStyle.wordWrapWidth = this._card.width * 0.5 - 20;
                this._errataText.style = textStyle;
                this._errataText.text = kCardInfo.text;
                
                this._errataBg.clear();
                this._errataBg.lineStyle( 1, 0x00000, 1 );
                this._errataBg.beginFill( 0xefe4b0, 1 );
                this._errataBg.drawRect( 0, 0, this._card.width * 0.5, this._errataBg.height + 20 );
                this._errataBg.endFill();

                this._errataBg.x = this._card.width;

                this._errataBg.visible = true;
            }
            else
            {
                this._errataText.text = "";
                this._errataBg.clear();
                this._errataBg.x = 0;
                this._errataBg.visible = false;
            }

            this._target = target;
            this._target.cContainer.onTransformChanged.add( this.onTargetTransform_Changed, this );
            this._target.onEnded.addOnce( this.onTarget_Ended, this );
        }
        this._go.cContainer.c.visible = true;

        const kTargetLocalBounds: PIXI.Rectangle = new PIXI.Rectangle( 0, 0, this._target.cContainer.c.width, this._target.cContainer.c.height );
        Utils.game.setPreviewPosition( this._go.cContainer.c, this._target.cContainer.c.getGlobalPosition(), kTargetLocalBounds );
    }

    public hide(): void
    {
        this._go.cContainer.c.visible = false;
        if ( this._target )
        {
            this._target.cContainer.onTransformChanged.remove( this.onTargetTransform_Changed, this );
            this._target.onEnded.remove( this.onTarget_Ended, this );
            this._target = null;
        }
    }

    // #endregion //


    // #region Callbacks //

    private onTargetTransform_Changed(): void
    {
        const kTargetLocalBounds: PIXI.Rectangle = new PIXI.Rectangle( 0, 0, this._target.cContainer.c.width, this._target.cContainer.c.height );
        Utils.game.setPreviewPosition( this._go.cContainer.c, this._target.cContainer.c.getGlobalPosition(), kTargetLocalBounds );
    }

    private onTarget_Ended( go: GameObject ): void
    {
        if ( go == this._target )
        {
            this.hide();
        }
    }

    // #endregion //
}