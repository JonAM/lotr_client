import Component from "../Component";

import { GlowFilter  } from "pixi-filters";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";


export default class CDragShadow extends Component
{
    // #region Attributes //

    // private:

    private _from: GameObject = null;
    private _texture: PIXI.Texture = null;

    private _alpha: number = 1;
    private _sprite: PIXI.Sprite = null;
    private _isGlow: boolean = false;

    // #endregion //


    // #region Properties //

    public get from(): GameObject { return this._from; }
    public get isGlow(): boolean { return this._isGlow; }

    public set from( value: GameObject ) { this._from = value; }
    public set texture( value: PIXI.Texture ) { this._texture = value; }
    public set alpha( value: number ) { this._alpha = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CDragShadow";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CDragShadow.ts :: init() :: CContainer or CSprite components not found." );
        console.assert( this._from != null, "CDragShadow.ts :: init() :: this._from cannot be null." );
        console.assert( this._texture != null, "CDragShadow.ts :: init() :: this._texture cannot be null." );
    
        this._sprite = PIXI.Sprite.from( this._texture );
        this._sprite.alpha = this._alpha;
        this._go.cContainer.c.addChild( this._sprite );
    }

    public end(): void
    {
        this._from = null;
        this._texture = null;
        this._alpha = 1;
        this._sprite = null;
        this._isGlow = false;

        super.end();
    }

    public setGlow(): void
    {
        if ( this._isGlow ) { return; }

        let glowFilter: GlowFilter = new GlowFilter( { color: 0xffffff } );
        glowFilter.padding = 5;
        this._go.cContainer.c.filters = [ glowFilter ];

        this._sprite.alpha = 1;
        this._isGlow = true;
    }

    public clearGlow(): void
    {
        if ( !this._isGlow ) { return; }

        this._go.cContainer.c.filters = [];

        this._sprite.alpha = this._alpha;
        this._isGlow = false;
    }

    // #endregion //
}