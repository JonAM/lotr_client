import ServiceLocator from "../../../ServiceLocator";
import * as PIXI from "pixi.js";

import Component from "../Component";
import CLogAction from "../ui/right_menu/action_log/CLogAction";
import GameObject from "../../GameObject";
import Utils from "../../../Utils";


export default class CLogActionPreview extends Component
{
    // #region Attributes //

    // private:

    private _logAction: GameObject = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CLogActionPreview";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CLogActionPreview.ts :: init() :: CContainer component not found." );
    }

    public show( cLogAction: CLogAction, position: PIXI.Point ): void
    {
        if ( this._go.cContainer.c.children.length == 0 )
        {    
            this._logAction = cLogAction.go;
            this._logAction.onEnded.addOnce( this.onLogAction_Ended, this );

            this._go.cContainer.addChild( cLogAction.createPreview() );
        }

        Utils.game.setPreviewPosition( this._go.cContainer.c, this._logAction.cContainer.c.getGlobalPosition(), this._logAction.cContainer.c.getLocalBounds() );
    }

    public hide(): void
    {
        if ( this._logAction )
        {
            this._logAction.onEnded.remove( this.onLogAction_Ended, this );
            this._logAction.cLogActionPreviewable.onPreviewHidden.dispatch();
            this._logAction = null;
        }

        this._go.cContainer.c.removeChildren();
    }

    public isVisible(): boolean
    {
        return this._go.cContainer.c.children.length > 0;
    }

    // #endregion //


    // #region Callbacks //

    private onLogAction_Ended( go: GameObject ): void
    {
        if ( go == this._logAction )
        {
            this.hide();
        }
    }

    // #endregion //
}