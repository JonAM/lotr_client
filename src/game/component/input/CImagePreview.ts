import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";
import GameObject from "../../GameObject";
import { ICard } from "../../CardDB";
import GameUtils from "../../../util/GameUtils";


export default class CImagePreview extends Component
{
    // #region Attributes //

    // private:

    private _image: PIXI.Sprite = null;

    private _target: GameObject = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CImagePreview";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CImagePreview.ts :: init() :: CContainer component not found." );

        this._image = new PIXI.Sprite();
        this._go.cContainer.c.addChild( this._image );

        this._go.cContainer.c.visible = false;

        // Listen to events.
        ServiceLocator.game.onOngoingInteractionCanceled.add( this.hide, this );
    }

    public end(): void
    {
        // Cleanup events.
        ServiceLocator.game.onOngoingInteractionCanceled.remove( this.hide, this );

        super.end();
    }

    public isVisible(): boolean
    {
        return this._go.cContainer.c.visible;
    }

    public isTarget( go: GameObject ): boolean
    {
        return this._target == go;
    }

    public show( imageId: string, target: GameObject ): void
    {
        if ( !this._go.cContainer.c.visible || this._target != target )
        {
            this._image.texture = ServiceLocator.resourceStack.findAsTexture( imageId );
            
            this._target = target;
            this._target.cContainer.onTransformChanged.add( this.onTargetTransform_Changed, this );
            this._target.onEnded.addOnce( this.onTarget_Ended, this );
        }
        this._go.cContainer.c.visible = true;

        Utils.game.setPreviewPosition( this._go.cContainer.c, this._target.cContainer.c.getGlobalPosition(), this._target.cContainer.c.getLocalBounds() );
    }

    public hide(): void
    {
        this._go.cContainer.c.visible = false;
        if ( this._target )
        {
            this._target.cContainer.onTransformChanged.remove( this.onTargetTransform_Changed, this );
            this._target.onEnded.remove( this.onTarget_Ended, this );
            this._target = null;
        }
    }

    // #endregion //


    // #region Callbacks //

    private onTargetTransform_Changed(): void
    {
        Utils.game.setPreviewPosition( this._go.cContainer.c, this._target.cContainer.c.getGlobalPosition(), this._target.cContainer.c.getLocalBounds() );
    }

    private onTarget_Ended( go: GameObject ): void
    {
        if ( go == this._target )
        {
            this.hide();
        }
    }

    // #endregion //
}