import Component from "../Component";

import { player_type } from "../world/CPlayerArea";
import ServiceLocator from "../../../ServiceLocator";
import * as PIXI from "pixi.js";

import Signal from "../../../lib/signals/Signal";
import GameObject from "../../GameObject";
import CGraphics from "../pixi/CGraphics";
import Utils from "../../../Utils";
import CSprite from "../pixi/CSprite";
import ICharacterTokenSide from "../card/ICharacterTokenSide";
import { IAttackBinding } from "../../AttackBindingManager";
import CViewer from "../ui/CViewer";
import { layer_type } from "../world/CGameLayerProvider";
import FloatingMessage from "../../FloatingMessage";
import { detail_bar_icon_type } from "../card/token/CDetailBar";
import SignalBinding from "../../../lib/signals/SignalBinding";
import { input_event_type } from "../../InputController";
import PointerInputController from "../../input/PointerInputController";
import TouchInputController from "../../input/TouchInputController";


export default class CTargetSelector extends Component
{
    // #region Attributes //

    // private:

    private _receptor: GameObject = null;

    private _disablerBg: GameObject = null;
    private _arrow: GameObject = null;
    private _arrowHead: GameObject = null;
    private _icon: GameObject = null;
    private _target: GameObject = null;
    private _targetSelectionType: target_selection_type = null;
    private _woundContainer: PIXI.Container = null;
    private _isArrowHead: boolean = null;

    // Signals.
    private _onTargetSelected: Signal = new Signal();

    // #endregion //


    // #region Properties //
    
    public set receptor( value: GameObject ) { this._receptor = value; }
 
    // Signals.
    public get onTargetSelected(): Signal { return this._onTargetSelected; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CTargetSelector";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CTargetSelector.ts :: init() :: CContainer component not found." );
        console.assert( this._receptor != null, "CTargetSelector.ts :: init() :: this._receptor cannot be null." );

        this._isArrowHead = true;
    }

    public end(): void
    {
        this._onTargetSelected.removeAll();

        this.removeSelectionControls();

        this._target = null;
        this._targetSelectionType = null;
    }

    public startSelection( targetSelectionType: target_selection_type ): void
    {
        this._targetSelectionType = targetSelectionType;

        CViewer.minimizeAll();
        ServiceLocator.game.dragShadowManager.isDragging = true;

        let scrollY: GameObject = Utils.game.findGameObjectInBranchByComponentName( this._go, "cScrollY" );
        if ( scrollY )
        {
            scrollY.cScrollY.setEnabled( false );
        }

        this.createSelectionControls();
        //
        const kTextureId: Array<string> = [ "rad_target", "rad_equip", "rad_underneath", "rad_shadow_card", "rad_player_attack", "rad_sauron_attack" ];
        this._icon.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( kTextureId[ targetSelectionType ] );
        Utils.game.limitSideSize( 40, this._icon.cSprite.s );
        let to: PIXI.Point = null;
        if ( ServiceLocator.game.inputController instanceof PointerInputController )
        {
            to = ServiceLocator.game.app.renderer.plugins.interaction.mouse.global;
        } 
        else if ( ServiceLocator.game.inputController instanceof TouchInputController )
        {
            to = this.findFromPosition();
        }
        this._icon.cContainer.c.position.copyFrom( to );
        //
        const kFrom: PIXI.Point = this.findFromPosition();
        this.draw( kFrom, to );

        if ( ServiceLocator.game.inputController instanceof PointerInputController )
        {
            this._go.cContainer.c.on( "pointermove", this.onPointer_Move, this );
            ServiceLocator.game.inputController.on( this._disablerBg.cContainer.c, input_event_type.TAP, this.onSelection_Ended, this );
        } 
        else if ( ServiceLocator.game.inputController instanceof TouchInputController )
        {
            ServiceLocator.game.inputController.on( this._disablerBg.cContainer.c, input_event_type.TAP, this.onDisablerBg_Tap, this );
        }

        // Listen to events.
        ServiceLocator.game.onOngoingInteractionCanceled.add( this.onSelection_Ended, this );
        this._go.cContainer.onTransformChanged.add( this.onSelection_Ended, this );
    }

        private createSelectionControls(): void
        {
            this._disablerBg = new GameObject( [ new CSprite() ] );
            this._disablerBg.cContainer.c.interactive = true;
            this._disablerBg.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "transparent" );
            this._disablerBg.cContainer.c.scale.set( ServiceLocator.game.app.screen.width, ServiceLocator.game.app.screen.height );
            this._disablerBg.init();
            ServiceLocator.game.root.cGameLayerProvider.add( this._disablerBg, layer_type.DISABLED );

            this._arrow = new GameObject( [ new CGraphics() ] );
            this._arrow.init();
            ServiceLocator.game.root.cGameLayerProvider.add( this._arrow, layer_type.TARGET_SELECTION );

            this._arrowHead = new GameObject( [ new CGraphics() ] );
            this._arrowHead.init();
            ServiceLocator.game.root.cGameLayerProvider.add( this._arrowHead, layer_type.TARGET_SELECTION );

            this._icon = new GameObject( [ new CSprite() ] );
            this._icon.cSprite.s.anchor.set( 0.5 );
            this._icon.init();
            ServiceLocator.game.root.cGameLayerProvider.add( this._icon, layer_type.TARGET_SELECTION );
        }

        private findFromPosition(): PIXI.Point
        {
            let from: PIXI.Point = this._go.cContainer.c.getGlobalPosition();
            if ( this._go.cCardTokenSide )
            {
                from.x += this._go.cCardTokenSide.bg.cContainer.c.width * 0.5;
                from.y += this._go.cCardTokenSide.bg.cContainer.c.height * 0.5;
            }
            else if ( this._go.cGameModifier )
            {
                from.x += this._go.cGameModifier.calcWidth() * 0.5;
                from.y += this._go.cGameModifier.calcHeight() * 0.5;
            }
            else
            {
                from.x += this._go.cContainer.c.width * 0.5;
                from.y += this._go.cContainer.c.height * 0.5;
            }

            return from;
        }

    // private: 

    private removeSelectionControls(): void
    {
        if ( this._disablerBg )
        {
            this._disablerBg.end();
            this._disablerBg = null;
        }

        if ( this._arrow )
        {
            this._arrow.end();
            this._arrow = null;
        }

        if ( this._arrowHead )
        {
            this._arrowHead.end();
            this._arrowHead = null;
        }

        if ( this._icon )
        {
            this._icon.end();
            this._icon = null;
        }
    }

    private draw( from: PIXI.Point, to: PIXI.Point ): void
    {
        this._arrow.cGraphics.g.clear();
        let color: number = ServiceLocator.game.playerColors[ player_type.PLAYER ];
        if ( this._targetSelectionType == target_selection_type.SAURON_ATTACK )
        {
            color = ServiceLocator.game.playerColors[ player_type.SAURON ];
        }
        this._arrow.cGraphics.g.lineStyle( 10, color );
        this._arrow.cGraphics.g.moveTo( from.x, from.y );
        // Line.
        this._arrow.cGraphics.g.lineTo( to.x, to.y );
        // Base.
        this._arrow.cGraphics.g.beginFill( color );
        this._arrow.cGraphics.g.drawCircle( from.x, from.y, 10 );
        this._arrow.cGraphics.g.endFill();
        if ( this._isArrowHead )
        {
            this._arrowHead.cGraphics.g.clear();
            this._arrowHead.cGraphics.g.beginFill( color );
            this._arrowHead.cGraphics.g.drawCircle( to.x, to.y, 40 );
            this._arrowHead.cGraphics.g.beginFill( 0xffffff );
            this._arrowHead.cGraphics.g.drawCircle( to.x, to.y, 30 );
            this._arrowHead.cGraphics.g.endFill();
        }
    }

    // #endregion //


    // #region Input Callbacks //

    private onPointer_Move( event: PIXI.InteractionEvent ): void
    {
        const kFrom: PIXI.Point = this.findFromPosition();
        let to: PIXI.Point = null;
        let hit: GameObject = this.hitTest( event.data.global );
        if ( hit 
            && hit != this._receptor 
            && hit.cTargetReceptor.validate( this._targetSelectionType ) )
        {
            if ( this._target != hit )
            {
                hit.cTargetReceptor.onFocusReceived( this._go );

                if ( this._target )
                {
                    // Cleanup events.
                    this._target.cContainer.onTransformChanged.remove( this.onSelection_Ended, this );
                    this._target.onEnded.remove( this.onSelection_Ended, this );
                }

                // Listen to events.
                hit.cContainer.onTransformChanged.add( this.onSelection_Ended, this );
                hit.onEnded.add( this.onSelection_Ended, this );
            }
            this._target = hit;

            to = new PIXI.Point();
            to.copyFrom( this._target.cTargetReceptor.targetSocket.cContainer.c.getGlobalPosition() );
            
            if ( this._targetSelectionType == target_selection_type.PLAYER_ATTACK
                || this._targetSelectionType == target_selection_type.SAURON_ATTACK )
            {
                let attackBindingCounter: GameObject = ServiceLocator.game.attackBindingManager.findBindingCounter( hit.cCardTokenSide.cardToken );
                if ( !attackBindingCounter || !attackBindingCounter.cAttackBindingCounter.isAuto )
                {
                    if ( !this._woundContainer )
                    {
                        this.createWoundContainer();
                    }
                    this._woundContainer.visible = true;
                    this._woundContainer.position.copyFrom( to );

                    const kWoundPreview: number = this.calcWoundPreview( this._go );
                    ( this._woundContainer.getChildByName( "text" ) as PIXI.Text ).text = "+" + kWoundPreview.toString();
                }
                else
                {
                    this._isArrowHead = false;
                    this._arrowHead.cGraphics.g.clear();

                    Utils.game.findGameProviderLayer( hit, layer_type.ATTACK_BINDING_LINK ).cContainer.addChild( this._arrow );
                }
                this._icon.cContainer.c.visible = false;
            }
        }
        else
        {
            if ( this._target )
            {
                if ( this._woundContainer )
                {
                    this._woundContainer.visible = false;
                }
                this._icon.cContainer.c.visible = true;
                this._isArrowHead = true;
                ServiceLocator.game.root.cGameLayerProvider.get( layer_type.TARGET_SELECTION ).cContainer.c.addChildAt( this._arrow.cContainer.c, 0 );

                this._target.cTargetReceptor.onFocusLost( this._go );

                // Cleanup events.
                this._target.cContainer.onTransformChanged.remove( this.onSelection_Ended, this );
                this._target.onEnded.remove( this.onSelection_Ended, this );

                this._target = null;
            }

            to = event.data.global;
        }
        this.draw( kFrom, to );
        this._icon.cContainer.c.position.copyFrom( to );
    }

        private hitTest( global: PIXI.Point ): GameObject
        {
            const kCustomPanelLayer: GameObject = ServiceLocator.game.root.cGameLayerProvider.get( layer_type.CUSTOM_PANEL );
            let hit: GameObject = Utils.game.hitFirstTest( global, "cCustomArea", kCustomPanelLayer.cContainer.c );
            if ( hit )
            {
                hit = Utils.game.hitFirstTest( global, "cTargetReceptor", hit.cContainer.c );
            }
            else
            {
                hit = Utils.game.hitFirstTest( global, "cTargetReceptor", ServiceLocator.game.world.cContainer.c );
            }
            if ( hit
                && ( this._targetSelectionType == target_selection_type.PLAYER_ATTACK
                    || this._targetSelectionType == target_selection_type.SAURON_ATTACK ) )
            {
                if ( Utils.game.findGameObjectInBranchByComponentName( this._go, "CGameLayerProvider" )
                    != Utils.game.findGameObjectInBranchByComponentName( hit, "CGameLayerProvider" ) )
                {
                    hit = null;
                }
            }

            return hit;
        }

        private calcWoundPreview( attackerSide: GameObject ): number
        {
            let woundPreview: number = null;
            let isTargetSpectral: boolean = this._target.cCardTokenSide.cardToken.cCardToken.cDetailBar.findItem( detail_bar_icon_type.SPECTRAL ) != null;
            if ( isTargetSpectral )
            {
                woundPreview = attackerSide.cCardTokenSide[ "_willpower" ].cTokenCounter.count;
            }
            else
            {
                woundPreview = ( attackerSide.cCardTokenSide as unknown as ICharacterTokenSide ).attack.cTokenCounter.count;
            }
            
            let arrAttackBinding: Array<IAttackBinding> = ServiceLocator.game.attackBindingManager.findDefenderBindings( this._target.cCardTokenSide.cardToken );
            for ( let attackBinding of arrAttackBinding )
            {
                if ( isTargetSpectral )
                {
                    woundPreview += attackBinding.attacker.cCardToken.cCurSide[ "_willpower" ].cTokenCounter.count;
                }
                else
                {
                    woundPreview += ( attackBinding.attacker.cCardToken.cCurSide as unknown as ICharacterTokenSide ).attack.cTokenCounter.count;
                }
            }
            woundPreview -= ( this._target.cCardTokenSide as unknown as ICharacterTokenSide ).defense.cTokenCounter.count;
            if ( woundPreview < 0 )
            {
                woundPreview = 0;
            }

            return woundPreview;
        }

        private createWoundContainer(): void
        {
            this._woundContainer = new PIXI.Container();
            let icon: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( "wound_token" ) );
            icon.anchor.set(  0.5 );
            Utils.game.limitSideSize( 50, icon );
            this._woundContainer.addChild( icon );
            let text: PIXI.Text = new PIXI.Text( "", ServiceLocator.game.textStyler.normal );
            text.name = "text";
            text.anchor.set( 0.5 );
            this._woundContainer.addChild( text );
            ServiceLocator.game.root.cGameLayerProvider.get( layer_type.TARGET_SELECTION ).cContainer.c.addChild( this._woundContainer );
        }

    private onDisablerBg_Tap( event: PIXI.InteractionEvent ): void
    {
        let hit: GameObject = this.hitTest( event.data.global );
        if ( hit 
            && hit != this._receptor 
            && hit.cTargetReceptor.validate( this._targetSelectionType )
            && this._target != hit )
        {
            this.onPointer_Move( event );
            if ( this._targetSelectionType != target_selection_type.PLAYER_ATTACK
                && this._targetSelectionType != target_selection_type.SAURON_ATTACK )
            {
                this.onSelection_Ended();
            }
        }
        else
        {
            if ( !hit || !hit.cTargetReceptor.validate( this._targetSelectionType ) )
            {
                this._target = null
            }
            this.onSelection_Ended();
        }
    }

    // #endregion //


    // #region Other Callbacks //

    private onSelection_Ended(): void
    {
        if ( this._targetSelectionType == null ) { return; }

        // Multiplayer.
        if ( this._receptor.cShareableGameElement )
        {
            this._receptor.cShareableGameElement.notifyUnlock();
        }

        // Cleanup events.
        ServiceLocator.game.onOngoingInteractionCanceled.remove( this.onSelection_Ended, this );
        this._go.cContainer.onTransformChanged.remove( this.onSelection_Ended, this );

        let isValid: boolean = true;
        if ( this._targetSelectionType == target_selection_type.EQUIP 
            && this._target )
        {
            let actorArea: GameObject = Utils.game.findGameObjectInBranchByComponentName( this._target, "cActorArea" );
            if ( actorArea )
            {
                isValid = actorArea.cActorArea.findInsertTabIndex( 50 ) != -1;
                if ( !isValid )
                {
                    let floatinMessage = new FloatingMessage();
                    floatinMessage.show( jQuery.i18n( "NOT_ENOUGH_SPACE" ), ServiceLocator.game.app.renderer.plugins.interaction.mouse.global );
                }
            }
        }

        if ( isValid
            && this._targetSelectionType == target_selection_type.SHADOW_CARD
            && this._target )
        {
            isValid = this._target.cEnemySide != null;
            if ( isValid && this._target.cEnemySide.shadowCardContainer.children.length == 3 )
            {
                isValid = false;
                let floatinMessage = new FloatingMessage();
                floatinMessage.show( jQuery.i18n( "NOT_ENOUGH_SPACE" ), ServiceLocator.game.app.renderer.plugins.interaction.mouse.global );
            }
        }

        if ( isValid )
        {
            let scrollY: GameObject = Utils.game.findGameObjectInBranchByComponentName( this._go, "cScrollY" );
            if ( scrollY )
            {
                scrollY.cScrollY.setEnabled( true );
            }

            this._go.cContainer.c.off( "pointermove", this.onPointer_Move, this );
            ServiceLocator.game.inputController.off( this._disablerBg.cContainer.c, input_event_type.TAP, this.onSelection_Ended, this );
            this.removeSelectionControls();

            if ( this._target )
            {
                // Cleanup events.
                this._target.cContainer.onTransformChanged.remove( this.onSelection_Ended, this );
                this._target.onEnded.remove( this.onSelection_Ended, this );

                this._onTargetSelected.dispatch( this._target, this._targetSelectionType );
        
                this._target = null;
            }
            this._targetSelectionType = null;

            if ( this._woundContainer )
            {
                this._woundContainer.parent.removeChild( this._woundContainer );
                this._woundContainer = null;
            }
            ServiceLocator.game.dragShadowManager.isDragging = false;
            this._isArrowHead = true;
        }
    }

    // #endregion //
}

export const enum target_selection_type
{
    TARGET = 0,
    EQUIP,
    UNDERNEATH,
    SHADOW_CARD,
    PLAYER_ATTACK,
    SAURON_ATTACK
}