import Component from "../Component";

import * as PIXI from "pixi.js";

import Signal from "../../../lib/signals/Signal";
import ServiceLocator from "../../../ServiceLocator";


export default class CDragDetector extends Component
{
    // #region Attributes //

    // private:

    private _isTapOnly: boolean = false;
    private _pointerDownPosition: PIXI.Point = null;

    // Bindend functions.
    private _bfOnPointer_Down: Function = null;
    private _bfOnPointer_Up: Function = null;
    private _bfOnPointer_Move: Function = null;

    // Signals.
    private _onDragged: Signal = new Signal();
    private _onTapped: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public set isTapOnly( value: boolean ) { this._isTapOnly = value; }

    // Signals.
    public get onDragged(): Signal { return this._onDragged; }
    public get onTapped(): Signal { return this._onTapped; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CDragDetector";

        // Binded functions.
        this._bfOnPointer_Down = this.onPointer_Down.bind( this );
        this._bfOnPointer_Up = this.onPointer_Up.bind( this );
        this._bfOnPointer_Move = this.onPointer_Move.bind( this );
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CDragDetector.ts :: init() :: CContainer or CSprite components not found." );

        this._go.cContainer.c.interactive = true;
        this._go.cContainer.c.buttonMode = true;

        // Attach event handlers.
        this._go.cContainer.c.on( "pointerdown", this._bfOnPointer_Down );
        this._go.cContainer.c.on( "pointerup", this._bfOnPointer_Up );
        this._go.cContainer.c.on( "pointerupoutside", this._bfOnPointer_Up );
    }

    public end(): void
    {
        this._onDragged.removeAll();
        this._onTapped.removeAll();

        // Cleanup event handlers.
        this._go.cContainer.c.off( "pointerdown", this._bfOnPointer_Down );
        this._go.cContainer.c.off( "pointerup", this._bfOnPointer_Up );
        this._go.cContainer.c.off( "pointerupoutside", this._bfOnPointer_Up );
        this._go.cContainer.c.off( "pointermove", this._bfOnPointer_Move );
    }

    // overrides.
    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        this._go.cContainer.c.buttonMode = isEnabled;
    }

    // #endregion //


    // #region Input Callbacks //

    private onPointer_Down( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled || ServiceLocator.game.dragShadowManager.isDetectingDrag ) { return; }

        ServiceLocator.game.dragShadowManager.isDetectingDrag = true;

        this._pointerDownPosition = new PIXI.Point( event.data.global.x, event.data.global.y );
        this._go.cContainer.c.on( "pointermove", this._bfOnPointer_Move );
    }

    private onPointer_Up( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled ) { return; }

        if ( this._pointerDownPosition )
        {
            // Sfx.
            ServiceLocator.audioManager.playSfx( "button_click" );

            this._onTapped.dispatch( this._go, event );
        }
        
        this._pointerDownPosition = null;
        this._go.cContainer.c.off( "pointermove", this._bfOnPointer_Move );
    }

    private onPointer_Move( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled ) { return; }
        
        if ( this._pointerDownPosition && !this._isTapOnly )
        {
            let distance: number = Math.pow( event.data.global.x - this._pointerDownPosition.x, 2 ) 
                + Math.pow( event.data.global.y - this._pointerDownPosition.y, 2 );
            if ( distance > 400 )
            {
                this._pointerDownPosition = null;

                ServiceLocator.game.dragShadowManager.isDragging = true;
                
                this._onDragged.dispatch( this._go, event );
            }
        }
    }

    // #endregion //
}
