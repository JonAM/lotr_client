import Component from "../Component";

import { player_type } from "../world/CPlayerArea";
import * as PIXI from "pixi.js";

import Signal from "../../../lib/signals/Signal";
import ServiceLocator from "../../../ServiceLocator";
import { action_scope_type } from "../../../service/socket_io/GameSocketIOController";
import { input_event_type } from "../../InputController";


export default class CButton extends Component
{
    // #region Attributes //

    // private:

    private _data: any = null;

    // Signals.
    private _onClick: Signal = new Signal();

    // #endregion //


    // #region Properties //
    
    public get data(): any { return this._data; }

    public set data( value: any) { this._data = value; }

    // Signals.
    public get onClick(): Signal { return this._onClick; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();
    
        this._id = "CButton";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CButton.ts :: init() :: CContainer component not found." );
    
        this._go.cContainer.c.interactive = true;
        this._go.cContainer.c.buttonMode = true;

        ServiceLocator.game.inputController.on( this._go.cContainer.c, input_event_type.TAP, this.onPointer_Tap, this );
    }

    public end(): void
    {
        this._onClick.removeAll();

        ServiceLocator.game.inputController.off( this._go.cContainer.c, input_event_type.TAP, this.onPointer_Tap, this );

        this._data = null;
    }

    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        this._go.cContainer.c.buttonMode = isEnabled;
    }

    // #endregion //


    // #region Multiplayer //

    public forcePointerTap(): void
    {
        this._onClick.dispatch( this._go, action_scope_type.LOCAL, null );
    }

    // #endregion //


    // #region Input Callbacks //

    private onPointer_Tap( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled 
            || ServiceLocator.game.poiMenu.cContainer.c.visible
            || ServiceLocator.game.dragShadowManager.isDragging ) 
        { 
            return; 
        }

        // Sfx.
        ServiceLocator.audioManager.playSfx( "button_click" );

        this._onClick.dispatch( this._go, action_scope_type.MULTIPLAYER, event );
    }

    // #endregion //
}