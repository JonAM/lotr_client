import Component from "./Component";

import * as PIXI from "pixi.js";
import GameObject from "../GameObject";
import Utils from "../../Utils";
import ServiceLocator from "../../ServiceLocator";
import { action_scope_type, player_action_type } from "../../service/socket_io/GameSocketIOController";
import Signal from "../../lib/signals/Signal";
import { IOppActionListener, IOpponentAction } from "../AllyActionManager";


export default class CShareableGameElement extends Component implements IOppActionListener
{
    // #region Attributes //

    // private:
    
    private _lockOid: string = null;
    private _noiseFilter: PIXI.filters.NoiseFilter = null;

    // Signals.
    private _onLocked: Signal = new Signal();
    private _onUnlocked: Signal = new Signal();
    private _onLockOpen: Signal = new Signal();
    private _onLockStatusReceived: Signal = new Signal();

    // #endregion //


    public get onLocked(): Signal { return this._onLocked; }
    public get onUnlocked(): Signal { return this._onUnlocked; }
    public get onLockOpen(): Signal { return this._onLockOpen; }
    public get onLockStatusReceived(): Signal { return this._onLockStatusReceived; }


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CShareableGameElement";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CShareableGameElement.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cDeckIndicator == null || this._go.cDeckIndicator != null && this._go.cIndicatorBusy != null, "CShareableGameElement.ts :: init() :: CIndicatorBusy component not found." );
    }

    public end(): void
    {
        this._onLocked.removeAll();
        this._onUnlocked.removeAll();
        this._onLockOpen.removeAll();
        this._onLockStatusReceived.removeAll();

        // Cleanup events.
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.LOCK_STATUS_RECEIVED ] );

        this._lockOid = null;
        if ( this._noiseFilter )
        {
            this._go.cContainer.c.filters.splice( this._go.cContainer.c.filters.indexOf( this._noiseFilter ), 1 );
            this._noiseFilter = null;
        }

        super.end();
    }

    public lock(): void
    {
        if ( !this._isEnabled ) { return; }

        if ( this._go.cCardToken )
        {
            let actorArea: GameObject = Utils.game.findGameObjectInBranchByComponentName( this._go, "CActorArea" );
            if ( actorArea )
            {
                actorArea.cActorArea.setBusy( true );
            }
            else
            {
                this._go.cCardToken.setEnabled( false );
            }
            this.setNoiseFilter();
        }
        else if ( this._go.cCard )
        {
            this._go.cCard.setEnabled( false );
            this.setNoiseFilter();
        }
        else if ( this._go.cGameModifier )
        {
            this._go.cGameModifier.setEnabled( false );
            this.setNoiseFilter();
        }
        else if ( this._go.cPlayerToken || this._go.cChariotToken )
        {
            this._go.cDraggable.setEnabled( false );
            this.setNoiseFilter();
        }
        else if ( this._go.cDeckIndicator )
        {
            this._go.cIndicatorBusy.setBusy( true );
        }
        else if ( this._go.cCardActivationArea )
        {
            this._go.cCardActivationArea.setEnabled( false );
        }
        else if ( this._go.cMultiItemSelector )
        {
            this._go.cMultiItemSelector.setEnabled( false );
        }

        this._onLocked.dispatch( this._go );
    }

        private setNoiseFilter(): void
        {
            if ( !this._noiseFilter )
            {
                this._noiseFilter = new PIXI.filters.NoiseFilter();
                if ( !this._go.cContainer.c.filters )
                {
                    this._go.cContainer.c.filters = [ this._noiseFilter ];
                }
                else
                {
                    this._go.cContainer.c.filters.push( this._noiseFilter );
                }
            }
        }

    public unlock(): void
    {
        if ( !this._isEnabled ) { return; }

        if ( this._go.cCardToken )
        {
            let actorArea: GameObject = Utils.game.findGameObjectInBranchByComponentName( this._go, "CActorArea" );
            if ( actorArea )
            {
                actorArea.cActorArea.setBusy( false );
            }
            else
            {
                this._go.cCardToken.setEnabled( true );
            }
            this.clearNoiseFilter();
        }
        else if ( this._go.cCard )
        {
            this._go.cCard.setEnabled( true );
            this.clearNoiseFilter();
        }
        else if ( this._go.cGameModifier )
        {
            this._go.cGameModifier.setEnabled( true );
            this.clearNoiseFilter();
        }
        else if ( this._go.cPlayerToken || this._go.cChariotToken )
        {
            this._go.cDraggable.setEnabled( true );
            this.clearNoiseFilter();
        }
        else if ( this._go.cDeckIndicator )
        {
            this._go.cIndicatorBusy.setBusy( false );
        }
        else if ( this._go.cCardActivationArea )
        {
            this._go.cCardActivationArea.setEnabled( true );
        }
        else if ( this._go.cMultiItemSelector )
        {
            this._go.cMultiItemSelector.setEnabled( true );
        }

        this._onUnlocked.dispatch( this._go );
    }

        private clearNoiseFilter(): void
        {
            if ( this._noiseFilter )
            {
                this._go.cContainer.c.filters.splice( this._go.cContainer.c.filters.indexOf( this._noiseFilter ), 1 );
                this._noiseFilter = null;
            }
        }

    public notifyLock(): void
    {
        if ( !this._isEnabled ) { return; }

        let shareableGameElement: GameObject = this._go;
        if ( this._go.cShadowCardMini || this._go.cAttachment || this._go.cCardTokenSide )
        {
            shareableGameElement = Utils.game.findGameObjectInBranchByComponentName( this._go, "CCardToken" );
            if ( !shareableGameElement.cShareableGameElement )
            {
                shareableGameElement = null;
            }
        }
        if ( shareableGameElement )
        {
            this._lockOid = shareableGameElement.oid;

            // Listen to events.
            ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.LOCK_STATUS_RECEIVED ] );
            
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.LOCK_GAME_ELEMENT, null, [ this._lockOid ] );
        }
    }

    public notifyUnlock(): void
    {
        if ( !this._isEnabled ) { return; }

        let shareableGameElement: GameObject = this._go;
        if ( this._go.cShadowCardMini || this._go.cAttachment || this._go.cCardTokenSide )
        {
            shareableGameElement = Utils.game.findGameObjectInBranchByComponentName( this._go, "CCardToken" );
            if ( !shareableGameElement.cShareableGameElement )
            {
                shareableGameElement = null;
            }
        }
        if ( shareableGameElement )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.UNLOCK_GAME_ELEMENT, null, [ shareableGameElement.oid ] );
        }
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.LOCK_STATUS_RECEIVED
            && this._lockOid == action.args[ 1 ] )
        {
            this._lockOid = null;

            // Cleanup events.
            ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.LOCK_STATUS_RECEIVED ] );

            const kIsLockOpen: boolean = !action.args[ 0 ];
            if ( kIsLockOpen )
            {
                this._onLockOpen.dispatch();
            }
            else
            {
                this._onLockOpen.removeAll();
            }

            this._onLockStatusReceived.dispatch( kIsLockOpen );
        }
    }

    // #endregion //
}
