import Component from "../Component";

import { location_type } from "./CGameWorld";
import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import CActorArea from "./CActorArea";
import GameObject from "../../GameObject";
import CContainer from "../pixi/CContainer";
import CGraphics from "../pixi/CGraphics";
import CDropArea from "../input/CDropArea";
import CLogLocation from "./CLogLocation";
import CSauronSideControls from "../ui/CSauronSideControls";
import { player_type } from "./CPlayerArea";
import CQuestHolder from "../ui/CQuestHolder";
import CActiveLocationHolder from "../ui/CActiveLocationHolder";
import CSauronActorArea from "./actor_area/CSauronActorArea";
import CStagingThreatCounter from "../ui/CStagingThreatCounter";
import CPlayerWillpowerCounter from "../ui/CPlayerWillpowerCounter";
import { ISgPlayerWillpowerCounter, ISgSauronPlayArea } from "../../../view/game/SaveGameView";
import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import CHighlightPoiReceptor from "./poi_receptor/CHighlightPoiReceptor";
import CQuestVariation from "../ui/CQuestVariation";
import CIsolatedArea from "./custom_area/CIsolatedArea";
import { game_state_id } from "../../../states/StateId";
import { IOppActionListener, IOpponentAction } from "../../AllyActionManager";
import CSplitSauronArea from "./custom_area/CSplitSauronArea";
import Session from "../../../Session";


export default class CSauronArea extends Component implements IOppActionListener
{
    // #region Attributes //

    // private:

    private _staging: GameObject = null;
    private _sauronWillpowerCounter: GameObject = null;
    private _playerWillpowerCounter: GameObject = null;
    private _allyWillpowerCounter: GameObject = null;
    private _stagingThreatCounter: GameObject = null;
    private _questVariation: GameObject = null;
    private _sideControls: GameObject = null;
    private _questHolder: GameObject = null;
    private _activeLocationHolder: GameObject = null;
    private _nightmareSetup: GameObject = null;

    // #endregion //


    // #region Properties //

    public get staging(): GameObject { return this._staging; }
    public get cStaging(): CSauronActorArea { return this._staging.cSauronActorArea; }
    public get sauronWillpowerCounter(): GameObject { return this._sauronWillpowerCounter; }
    public get cSauronWillpowerCounter(): CPlayerWillpowerCounter { return this._sauronWillpowerCounter.cPlayerWillpowerCounter; }
    public get playerWillpowerCounter(): GameObject { return this._playerWillpowerCounter; }
    public get cPlayerWillpowerCounter(): CPlayerWillpowerCounter { return this._playerWillpowerCounter.cPlayerWillpowerCounter; }
    public get allyWillpowerCounter(): GameObject { return this._allyWillpowerCounter; }
    public get cAllyWillpowerCounter(): CPlayerWillpowerCounter { return this._allyWillpowerCounter.cPlayerWillpowerCounter; }
    public get stagingThreatCounter(): GameObject { return this._stagingThreatCounter; }
    public get cStagingThreatCounter(): CStagingThreatCounter { return this._stagingThreatCounter.cStagingThreatCounter; }
    public get questVariation(): GameObject { return this._questVariation; }
    public get cQuestVariation(): CQuestVariation { return this._questVariation.cQuestVariation; }
    public get sideControls(): GameObject { return this._sideControls; }
    public get cSideControls(): CSauronSideControls { return this._sideControls.cSauronSideControls; }
    public get questHolder(): GameObject { return this._questHolder; }
    public get cQuestHolder(): CQuestHolder { return this._questHolder.cQuestHolder; }
    public get activeLocationHolder(): GameObject { return this._activeLocationHolder; }
    public get cActiveLocationHolder(): CActiveLocationHolder { return this._activeLocationHolder.cActiveLocationHolder; }

    // #endregion //

    
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CSauronArea";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CSauronArea.ts :: init() :: CContainer component not found." );

        this._staging = this.createStaging();
        this._staging.cContainer.c.position.set( 220, 438 );
        this._go.cContainer.addChild( this._staging );

        this._stagingThreatCounter = this.createStagingThreatCounter();
        this._stagingThreatCounter.cContainer.c.position.set( 
            this._staging.cContainer.c.x - this._stagingThreatCounter.cContainer.c.width * 0.5 - 10, 
            this._staging.cContainer.c.y + this._staging.cContainer.c.height * 0.5 );
        this._go.cContainer.addChild( this._stagingThreatCounter );

        this._allyWillpowerCounter = this.createAllyWillpowerCounter();
        this._allyWillpowerCounter.cContainer.c.visible = false;
        this._allyWillpowerCounter.cContainer.c.position.set( 
            this._stagingThreatCounter.cContainer.c.x,  
            this._stagingThreatCounter.cContainer.c.y - this._stagingThreatCounter.cContainer.c.height * 0.5 - this._allyWillpowerCounter.cContainer.c.height * 0.5 - 10 );
        this._go.cContainer.addChild( this._allyWillpowerCounter );

        this._playerWillpowerCounter = this.createPlayerWillpowerCounter();
        this._playerWillpowerCounter.cContainer.c.visible = false;
        this._playerWillpowerCounter.cContainer.c.position.set( 
            this._stagingThreatCounter.cContainer.c.x, 
            this._stagingThreatCounter.cContainer.c.y + this._stagingThreatCounter.cContainer.c.height * 0.5 + this._playerWillpowerCounter.cContainer.c.height * 0.5 + 10 );
        this._go.cContainer.addChild( this._playerWillpowerCounter );

        this._sauronWillpowerCounter = this.createSauronWillpowerCounter();
        this._sauronWillpowerCounter.cContainer.c.visible = false;
        this._sauronWillpowerCounter.cContainer.c.position.set( this._staging.cContainer.c.x, this._staging.cContainer.c.y + this._staging.cContainer.c.height * 0.75 );
        this._go.cContainer.addChild( this._sauronWillpowerCounter );

        this._questVariation = this.createQuestVariation();
        this._questVariation.cContainer.c.position.set( 
            this._stagingThreatCounter.cContainer.c.x - this._stagingThreatCounter.cContainer.c.width * 0.5 - this._questVariation.cContainer.c.width * 0.5 - 10,
            this._stagingThreatCounter.cContainer.c.y );
        this._go.cContainer.addChild( this._questVariation );

        this._activeLocationHolder = this.createActiveLocationHolder();
        //
        this._questHolder = this.createQuestHolder( this._activeLocationHolder );
        this._questHolder.cContainer.c.position.set( 220, 327 );
        this._go.cContainer.addChild( this._questHolder );
        //
        this._activeLocationHolder.cContainer.c.position.set( 220, ServiceLocator.game.app.screen.height - 327 );
        this._go.cContainer.addChild( this._activeLocationHolder );

        this._sideControls = this.createSauronSideControls();
        this._sideControls.cContainer.c.position.set( 0, ServiceLocator.game.app.screen.height * 0.5 );
        this._go.cContainer.addChild( this._sideControls );

        // Listen to events.
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.SPLIT_STAGING_AREA, player_action_type.MERGE_STAGING_AREA ] );
    }
    
        private createStaging(): GameObject
        {
            let staging: GameObject = new GameObject( [ new CGraphics(), new CSauronActorArea(), new CDropArea(), new CLogLocation() ] );
            staging.oid = "staging_area";
            staging.cActorArea.location = location_type.STAGING_AREA;
            staging.cActorArea.width = 1690;
            staging.cActorArea.lineColor = ServiceLocator.game.playerColors[ player_type.SAURON ];
            staging.cActorArea.padding = new PIXI.Point( 35, 5 );
            staging.cActorArea.actorMargin = 30;
            staging.cDropArea.target = staging.cActorArea;
            staging.cDropArea.isSelfDropAllowed = true;
            staging.cLogLocation.location = location_type.STAGING_AREA;
            staging.init();

            let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( "staging" ).data ) );
            Utils.game.limitSideSize( 30, icon );
            icon.position.set( 5 );
            staging.cContainer.c.addChildAt( icon, 1 );

            return staging
        }

        private createStagingThreatCounter(): GameObject
        {
            let stagingThreatCounter: GameObject = new GameObject( [ new CContainer(), new CStagingThreatCounter(), new CHighlightPoiReceptor() ] );
            stagingThreatCounter.oid = "staging_threat_counter";
            stagingThreatCounter.cStagingThreatCounter.staging = this._staging;
            stagingThreatCounter.init();

            return stagingThreatCounter
        }

        private createAllyWillpowerCounter(): GameObject
        {
            let allyWillpowerCounter: GameObject = new GameObject( [ new CContainer(), new CPlayerWillpowerCounter() ] );
            allyWillpowerCounter.oid = "player_willpower_counter_" + GameObject.findPlayerSufix( player_type.ALLY );
            allyWillpowerCounter.cPlayerWillpowerCounter.controller = player_type.ALLY;
            allyWillpowerCounter.cPlayerWillpowerCounter.colorType = player_type.ALLY;
            allyWillpowerCounter.cPlayerWillpowerCounter.actorArea = ServiceLocator.game.cGameWorld.cAllyArea.home;
            allyWillpowerCounter.init();

            return allyWillpowerCounter
        }

        private createPlayerWillpowerCounter(): GameObject
        {
            let playerWillpowerCounter: GameObject = new GameObject( [ new CContainer(), new CPlayerWillpowerCounter() ] );
            playerWillpowerCounter.oid = "player_willpower_counter_" + GameObject.findPlayerSufix( player_type.PLAYER );
            playerWillpowerCounter.cPlayerWillpowerCounter.controller = player_type.PLAYER;
            playerWillpowerCounter.cPlayerWillpowerCounter.colorType = player_type.PLAYER;
            playerWillpowerCounter.cPlayerWillpowerCounter.actorArea = ServiceLocator.game.cGameWorld.cPlayerArea.home;
            playerWillpowerCounter.init();

            return playerWillpowerCounter
        }

        private createSauronWillpowerCounter(): GameObject
        {
            let sauronWillpowerCounter: GameObject = new GameObject( [ new CContainer(), new CPlayerWillpowerCounter() ] );
            sauronWillpowerCounter.oid = "sauron_willpower_counter";
            sauronWillpowerCounter.cPlayerWillpowerCounter.controller = player_type.PLAYER;
            sauronWillpowerCounter.cPlayerWillpowerCounter.colorType = player_type.SAURON;
            sauronWillpowerCounter.cPlayerWillpowerCounter.actorArea = ServiceLocator.game.cGameWorld.cSauronArea.staging;
            sauronWillpowerCounter.cPlayerWillpowerCounter.isCompact = true;
            sauronWillpowerCounter.init();

            return sauronWillpowerCounter
        }

        private createQuestVariation(): GameObject
        {
            let questVariation: GameObject = new GameObject( [ new CContainer(), new CQuestVariation() ] );
            questVariation.cQuestVariation.playerWillpowerCounter = this._playerWillpowerCounter;
            questVariation.cQuestVariation.allyWillpowerCounter = this._allyWillpowerCounter;
            questVariation.cQuestVariation.stagingThreatCounter = this._stagingThreatCounter;
            questVariation.cQuestVariation.sauronWillpowerCounter = this._sauronWillpowerCounter;
            questVariation.init();

            return questVariation
        }

        private createSauronSideControls(): GameObject
        {
            let sideControls: GameObject = new GameObject( [ new CContainer(), new CSauronSideControls() ] );
            sideControls.oid = "sauron_side_controls";
            sideControls.init();

            return sideControls;
        }

        private createQuestHolder( activeLocationHolder: GameObject ): GameObject
        {
            let holder: GameObject = new GameObject( [ new CContainer(), new CQuestHolder(), new CDropArea(), new CLogLocation() ] );
            holder.oid = "quest_holder";
            holder.cQuestHolder.location = location_type.QUEST;
            holder.cQuestHolder.activeLocationHolder = activeLocationHolder;
            holder.cDropArea.target = holder.cQuestHolder;
            holder.cLogLocation.location = location_type.QUEST;
            holder.init();

            return holder;
        }

        private createActiveLocationHolder(): GameObject
        {
            let holder: GameObject = new GameObject( [ new CContainer(), new CActiveLocationHolder(), new CDropArea(), new CLogLocation() ] );
            holder.oid = "active_location_holder";
            holder.cActiveLocationHolder.location = location_type.ACTIVE_LOCATION;
            holder.cDropArea.target = holder.cActiveLocationHolder;
            holder.cLogLocation.location = location_type.ACTIVE_LOCATION;
            holder.init();

            return holder;
        }

    public end(): void
    {
        // Cleanup events.
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.SPLIT_STAGING_AREA, player_action_type.MERGE_STAGING_AREA ] );

        if ( this._nightmareSetup )
        {
            this._nightmareSetup.end();
            this._nightmareSetup;
        }

        this._questVariation.end();
        this._questVariation = null;

        this._stagingThreatCounter.end();
        this._stagingThreatCounter = null;

        this._staging.end();
        this._staging = null;

        this._allyWillpowerCounter.end();
        this._allyWillpowerCounter = null;

        this._playerWillpowerCounter.end();
        this._playerWillpowerCounter = null;
        
        this._sauronWillpowerCounter.end();
        this._sauronWillpowerCounter = null;

        this._sideControls.end();
        this._sideControls = null;

        this._questHolder.end();
        this._questHolder = null;

        this._activeLocationHolder.end();
        this._activeLocationHolder = null;
    }

    public split( who: player_type ): void
    {
        if ( ServiceLocator.game.cGameWorld.cCustomPanelManager.checkPanelVisibility( "isolated" ) )
        {
            // My ally was already isolated.
            if ( who == player_type.PLAYER )
            {
                this.splitSolo();
            }
            else if ( who == player_type.ALLY )
            {
                // Move tokens from isolated to split sauron.
                let cIsolatedArea: CIsolatedArea = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "isolated" ).cIsolatedArea;
                let cSplitSauronArea: CSplitSauronArea = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "split_sauron" ).cSplitSauronArea;
        
                //this._staging.cSauronActorArea.onSplit(); 
                cSplitSauronArea.cStagingThreatCounter.forceAutoBtnClick(); // clear last used value.

                // Move actors in staging area to split sauron area.
                const kStagingThreatCounterValue: number = cIsolatedArea.stagingThreatCounter.cStagingThreatCounter.counter.cTokenCounter.count;
                cSplitSauronArea.cStaging.appendActors( cIsolatedArea.staging.cSauronActorArea.clearActors() );
                //
                cSplitSauronArea.cStagingThreatCounter.counter.cTokenCounter.setCount( kStagingThreatCounterValue, action_scope_type.LOCAL );
                if ( cIsolatedArea.stagingThreatCounter.cStagingThreatCounter.isAutoBtn() )
                {
                    cSplitSauronArea.cStagingThreatCounter.showAutoBtn();
                }
                cIsolatedArea.stagingThreatCounter.cStagingThreatCounter.forceAutoBtnClick(); // clear last used value.
                
                // Move items in quest holder to split sauron area.
                const kSelQuestIndex: number = cIsolatedArea.questHolder.cQuestHolder.selector.cMultiItemSelector.selectedIndex;
                let arrQuestToken: Array<GameObject> = cIsolatedArea.questHolder.cQuestHolder.findItems();
                for ( let questToken of arrQuestToken )
                {
                    cSplitSauronArea.questHolder.cDropArea.forceDrop( questToken, null, action_scope_type.LOCAL );
                }
                if ( kSelQuestIndex >= 0 )
                {
                    cSplitSauronArea.cQuestHolder.selector.cMultiItemSelector.selectItem( kSelQuestIndex, action_scope_type.LOCAL );
                }

                // Move items in active location holder to split sauron area.
                const kSelLocationIndex: number = cIsolatedArea.activeLocationHolder.cActiveLocationHolder.selector.cMultiItemSelector.selectedIndex;
                let arrLocationToken: Array<GameObject> = cIsolatedArea.activeLocationHolder.cActiveLocationHolder.findItems();
                for ( let locationToken of arrLocationToken )
                {
                    cSplitSauronArea.activeLocationHolder.cDropArea.forceDrop( locationToken, null, action_scope_type.LOCAL );
                }
                if ( kSelLocationIndex >= 0 )
                {
                    cSplitSauronArea.cActiveLocationHolder.selector.cMultiItemSelector.selectItem( kSelLocationIndex, action_scope_type.LOCAL );
                }

                // Show custom panel.
                ServiceLocator.game.cGameWorld.cCustomPanelManager.setPanelVisibility( "split_sauron", true );
                ServiceLocator.game.cGameWorld.cCustomPanelManager.setPanelIconId( "isolated", "isolated_staging_area" );
            }
        }
        else
        {
            // No player was isolated.
            let cIsolatedArea: CIsolatedArea = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "isolated" ).cIsolatedArea;

            const kSgAllyWillpowerCounter: ISgPlayerWillpowerCounter = { 
                count: this._allyWillpowerCounter.cPlayerWillpowerCounter.counter.cTokenCounter.count,
                characterAttribute: this._allyWillpowerCounter.cPlayerWillpowerCounter.selCharacterAttribute,
                isAuto: this._allyWillpowerCounter.cPlayerWillpowerCounter.isAuto };

            if ( who == player_type.PLAYER )
            {
                this._staging.cSauronActorArea.onSplit();
            }
            cIsolatedArea.cStagingThreatCounter.forceAutoBtnClick(); // clear last used value.

            // Move actors in ally's home and engaged areas to isolated area.
            cIsolatedArea.cHome.appendActors( ServiceLocator.game.cGameWorld.cAllyArea.cHome.clearActors() );
            cIsolatedArea.cEngaged.appendActors( ServiceLocator.game.cGameWorld.cAllyArea.cEngaged.clearActors() );
            
            // The player who is leaving the existing staging area.
            if ( who == player_type.PLAYER )
            {
                // Move actors in staging area to isolated area.
                const kStagingThreatCounterValue: number = this._stagingThreatCounter.cStagingThreatCounter.counter.cTokenCounter.count;
                cIsolatedArea.cStaging.appendActors( this._staging.cSauronActorArea.clearActors() );
                //
                cIsolatedArea.cStagingThreatCounter.counter.cTokenCounter.setCount( kStagingThreatCounterValue, action_scope_type.LOCAL );
                if ( this._stagingThreatCounter.cStagingThreatCounter.isAutoBtn() )
                {
                    cIsolatedArea.cStagingThreatCounter.showAutoBtn();
                }
                this._stagingThreatCounter.cStagingThreatCounter.forceAutoBtnClick(); // clear last used value.
                
                // Move items in quest holder to isolated area.
                const kSelQuestIndex: number = this._questHolder.cQuestHolder.selector.cMultiItemSelector.selectedIndex;
                let arrQuestToken: Array<GameObject> = this._questHolder.cQuestHolder.findItems();
                for ( let questToken of arrQuestToken )
                {
                    cIsolatedArea.questHolder.cDropArea.forceDrop( questToken, null, action_scope_type.LOCAL );
                }
                if ( kSelQuestIndex >= 0 )
                {
                    cIsolatedArea.cQuestHolder.selector.cMultiItemSelector.selectItem( kSelQuestIndex, action_scope_type.LOCAL );
                }

                // Move items in active location holder to isolated area.
                const kSelLocationIndex: number = this._activeLocationHolder.cActiveLocationHolder.selector.cMultiItemSelector.selectedIndex;
                let arrLocationToken: Array<GameObject> = this._activeLocationHolder.cActiveLocationHolder.findItems();
                for ( let locationToken of arrLocationToken )
                {
                    cIsolatedArea.activeLocationHolder.cDropArea.forceDrop( locationToken, null, action_scope_type.LOCAL );
                }
                if ( kSelLocationIndex >= 0 )
                {
                    cIsolatedArea.cActiveLocationHolder.selector.cMultiItemSelector.selectItem( kSelLocationIndex, action_scope_type.LOCAL );
                }
            }

            // Show custom panel.
            ServiceLocator.game.cGameWorld.cCustomPanelManager.setPanelVisibility( "isolated", true );
            ServiceLocator.game.cGameWorld.cCustomPanelManager.setPanelIconId( "isolated", who == player_type.PLAYER ? "main_staging_area" : "isolated_staging_area" );
            // Hide ally play area.
            ServiceLocator.game.cGameWorld.cAllyArea.home.cContainer.c.visible = false;
            ServiceLocator.game.cGameWorld.cAllyArea.engaged.cContainer.c.visible = false;

            // Bind player's staging threat counter and willpower counter to those in the isolated area.
            this._stagingThreatCounter.cStagingThreatCounter.onStagingAreaSplit();
            this._allyWillpowerCounter.cPlayerWillpowerCounter.onStagingAreaSplit( cIsolatedArea.allyWillpowerCounter );
            this._sauronWillpowerCounter.cPlayerWillpowerCounter.onStagingAreaSplit( cIsolatedArea.sauronWillpowerCounter  );
            // Ignore ally's willpower in player's quest variation.
            this._questVariation.cQuestVariation.allyWillpowerCounter = null;
            this._questVariation.cQuestVariation.updateQuestVariation();
            //
            if ( ServiceLocator.game.stateMachine.currentStateId == game_state_id.QUEST )
            {
                this._allyWillpowerCounter.cPlayerWillpowerCounter.hide();
                this._allyWillpowerCounter.cPlayerWillpowerCounter.forceAutoBtnClick();

                cIsolatedArea.cAllyWillpowerCounter.show();
                cIsolatedArea.cSauronWillpowerCounter.show();
                cIsolatedArea.questVariation.cContainer.c.visible = true;

                cIsolatedArea.cAllyWillpowerCounter.loadGame( kSgAllyWillpowerCounter, 0 );
            }
        }

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn.onGamePhaseDiagramChanged();
        if ( Session.allyId )
        {
            ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.onGamePhaseDiagramChanged();
        }
    }

    public splitSolo(): void
    {
        let cSplitSauronArea: CSplitSauronArea = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "split_sauron" ).cSplitSauronArea;
        
        this._staging.cSauronActorArea.onSplit();
        cSplitSauronArea.cStagingThreatCounter.forceAutoBtnClick(); // clear last used value.

        // Move actors in staging area to split sauron area.
        const kStagingThreatCounterValue: number = this._stagingThreatCounter.cStagingThreatCounter.counter.cTokenCounter.count;
        cSplitSauronArea.cStaging.appendActors( this._staging.cSauronActorArea.clearActors() );
        //
        cSplitSauronArea.cStagingThreatCounter.counter.cTokenCounter.setCount( kStagingThreatCounterValue, action_scope_type.LOCAL );
        if ( this._stagingThreatCounter.cStagingThreatCounter.isAutoBtn() )
        {
            cSplitSauronArea.cStagingThreatCounter.showAutoBtn();
        }
        this._stagingThreatCounter.cStagingThreatCounter.forceAutoBtnClick(); // clear last used value.
        
        // Move items in quest holder to split sauron area.
        const kSelQuestIndex: number = this._questHolder.cQuestHolder.selector.cMultiItemSelector.selectedIndex;
        let arrQuestToken: Array<GameObject> = this._questHolder.cQuestHolder.findItems();
        for ( let questToken of arrQuestToken )
        {
            cSplitSauronArea.questHolder.cDropArea.forceDrop( questToken, null, action_scope_type.LOCAL );
        }
        if ( kSelQuestIndex >= 0 )
        {
            cSplitSauronArea.cQuestHolder.selector.cMultiItemSelector.selectItem( kSelQuestIndex, action_scope_type.LOCAL );
        }

        // Move items in active location holder to split sauron area.
        const kSelLocationIndex: number = this._activeLocationHolder.cActiveLocationHolder.selector.cMultiItemSelector.selectedIndex;
        let arrLocationToken: Array<GameObject> = this._activeLocationHolder.cActiveLocationHolder.findItems();
        for ( let locationToken of arrLocationToken )
        {
            cSplitSauronArea.activeLocationHolder.cDropArea.forceDrop( locationToken, null, action_scope_type.LOCAL );
        }
        if ( kSelLocationIndex >= 0 )
        {
            cSplitSauronArea.cActiveLocationHolder.selector.cMultiItemSelector.selectItem( kSelLocationIndex, action_scope_type.LOCAL );
        }

        // Show custom panel.
        ServiceLocator.game.cGameWorld.cCustomPanelManager.setPanelVisibility( "split_sauron", true );
    }

    public merge( who: player_type ): void
    {
        if ( ServiceLocator.game.cGameWorld.cCustomPanelManager.checkPanelVisibility( "split_sauron" ) )
        {
            // Both players were isolated.
            if ( who == player_type.PLAYER )
            {
                this.mergeSolo();
            }
            else
            {
                // Move tokens from split sauron to isolated.
                let cSplitSauronArea: CSplitSauronArea = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "split_sauron" ).cSplitSauronArea;
                let cIsolatedArea: CIsolatedArea = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "isolated" ).cIsolatedArea;

                //this._staging.cSauronActorArea.onMerged();

                // Hide custom panel.
                ServiceLocator.game.cGameWorld.cCustomPanelManager.setPanelVisibility( "split_sauron", false );

                // Move actors in isolated area to staging area.
                cIsolatedArea.staging.cSauronActorArea.prependActors( cSplitSauronArea.cStaging.clearActors() );

                // Discard quest items.
                let arrQuestToken: Array<GameObject> = cIsolatedArea.questHolder.cQuestHolder.findItems();
                for ( let questToken of arrQuestToken )
                {
                    Utils.game.discard( questToken, action_scope_type.LOCAL );
                }
                // Discard active location items.
                let arrLocationToken: Array<GameObject> = cIsolatedArea.activeLocationHolder.cActiveLocationHolder.findItems();
                for ( let locationToken of arrLocationToken )
                {
                    Utils.game.discard( locationToken, action_scope_type.LOCAL );
                }

                // Move items in quest holder from split sauron area to sauron area.
                const kSelQuestIndex: number = cSplitSauronArea.cQuestHolder.selector.cMultiItemSelector.selectedIndex;
                arrQuestToken = cSplitSauronArea.cQuestHolder.findItems();
                for ( let questToken of arrQuestToken )
                {
                    cIsolatedArea.questHolder.cDropArea.forceDrop( questToken, null, action_scope_type.LOCAL );
                }
                if ( kSelQuestIndex >= 0 )
                {
                    cIsolatedArea.questHolder.cQuestHolder.selector.cMultiItemSelector.selectItem( kSelQuestIndex, action_scope_type.LOCAL );
                }
                // Move items in active location holder from isolated area to sauron area.
                const kSelLocationIndex: number = cSplitSauronArea.cActiveLocationHolder.selector.cMultiItemSelector.selectedIndex;
                arrLocationToken = cSplitSauronArea.cActiveLocationHolder.findItems();
                for ( let locationToken of arrLocationToken )
                {
                    cIsolatedArea.activeLocationHolder.cDropArea.forceDrop( locationToken, null, action_scope_type.LOCAL );
                }
                if ( kSelLocationIndex >= 0 )
                {
                    cIsolatedArea.activeLocationHolder.cActiveLocationHolder.selector.cMultiItemSelector.selectItem( kSelLocationIndex, action_scope_type.LOCAL );
                }
            }
        }
        else
        {
            // Only one player was isolated.
            let cIsolatedArea: CIsolatedArea = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "isolated" ).cIsolatedArea;

            const kSgAllyWillpowerCounter: ISgPlayerWillpowerCounter = { 
                count: cIsolatedArea.cAllyWillpowerCounter.counter.cTokenCounter.count,
                characterAttribute: cIsolatedArea.cAllyWillpowerCounter.selCharacterAttribute,
                isAuto: cIsolatedArea.cAllyWillpowerCounter.isAuto };

            this._staging.cSauronActorArea.onMerged();

            // Hide custom panel.
            ServiceLocator.game.cGameWorld.cCustomPanelManager.setPanelVisibility( "isolated", false );
            // Show ally's play area.
            ServiceLocator.game.cGameWorld.cAllyArea.home.cContainer.c.visible = true;
            ServiceLocator.game.cGameWorld.cAllyArea.engaged.cContainer.c.visible = true;

            // Move actors in isolated home and engaged areas to ally's play area. 
            ServiceLocator.game.cGameWorld.cAllyArea.cHome.appendActors( cIsolatedArea.cHome.clearActors() );
            ServiceLocator.game.cGameWorld.cAllyArea.cEngaged.appendActors( cIsolatedArea.cEngaged.clearActors() );
            
            // The player who is joining the existing staging area.
            if ( who == player_type.PLAYER )
            {
                // Move actors in isolated area to staging area.
                this._staging.cSauronActorArea.prependActors( cIsolatedArea.cStaging.clearActors() );

                // Discard quest items.
                let arrQuestToken: Array<GameObject> = this._questHolder.cQuestHolder.findItems();
                for ( let questToken of arrQuestToken )
                {
                    Utils.game.discard( questToken, action_scope_type.LOCAL );
                }
                // Discard active location items.
                let arrLocationToken: Array<GameObject> = this._activeLocationHolder.cActiveLocationHolder.findItems();
                for ( let locationToken of arrLocationToken )
                {
                    Utils.game.discard( locationToken, action_scope_type.LOCAL );
                }

                // Move items in quest holder from isolated area to sauron area.
                const kSelQuestIndex: number = cIsolatedArea.cQuestHolder.selector.cMultiItemSelector.selectedIndex;
                arrQuestToken = cIsolatedArea.cQuestHolder.findItems();
                for ( let questToken of arrQuestToken )
                {
                    this._questHolder.cDropArea.forceDrop( questToken, null, action_scope_type.LOCAL );
                }
                if ( kSelQuestIndex >= 0 )
                {
                    this._questHolder.cQuestHolder.selector.cMultiItemSelector.selectItem( kSelQuestIndex, action_scope_type.LOCAL );
                }
                // Move items in active location holder from isolated area to sauron area.
                const kSelLocationIndex: number = cIsolatedArea.cActiveLocationHolder.selector.cMultiItemSelector.selectedIndex;
                arrLocationToken = cIsolatedArea.cActiveLocationHolder.findItems();
                for ( let locationToken of arrLocationToken )
                {
                    this._activeLocationHolder.cDropArea.forceDrop( locationToken, null, action_scope_type.LOCAL );
                }
                if ( kSelLocationIndex >= 0 )
                {
                    this._activeLocationHolder.cActiveLocationHolder.selector.cMultiItemSelector.selectItem( kSelLocationIndex, action_scope_type.LOCAL );
                }
            }
            else
            {
                // Move actors in isolated area to staging area.
                this._staging.cSauronActorArea.appendActors( cIsolatedArea.cStaging.clearActors() );
                
                // Discard quest items in isolated area.
                let arrQuestToken: Array<GameObject> = cIsolatedArea.cQuestHolder.findItems();
                for ( let questToken of arrQuestToken )
                {
                    Utils.game.discard( questToken, action_scope_type.LOCAL );
                }
                // Discard active location items in isolated area..
                let arrLocationToken: Array<GameObject> = cIsolatedArea.cActiveLocationHolder.findItems();
                for ( let locationToken of arrLocationToken )
                {
                    Utils.game.discard( locationToken, action_scope_type.LOCAL );
                }
            }

            // Unbind player's staging threat counter and willpower counter from those in the isolated area.
            this._stagingThreatCounter.cStagingThreatCounter.onStagingAreaMerged();
            this._allyWillpowerCounter.cPlayerWillpowerCounter.onStagingAreaMerged( cIsolatedArea.allyWillpowerCounter );
            this._sauronWillpowerCounter.cPlayerWillpowerCounter.onStagingAreaMerged( cIsolatedArea.sauronWillpowerCounter );
            // Track ally's willpower in player's quest variation.
            this._questVariation.cQuestVariation.allyWillpowerCounter = this._allyWillpowerCounter;
            this._questVariation.cQuestVariation.updateQuestVariation();
            //
            if ( ServiceLocator.game.stateMachine.currentStateId == game_state_id.QUEST )
            {
                this._allyWillpowerCounter.cPlayerWillpowerCounter.show();
                this._allyWillpowerCounter.cPlayerWillpowerCounter.loadGame( kSgAllyWillpowerCounter, 0 );

                cIsolatedArea.cAllyWillpowerCounter.hide();
                cIsolatedArea.cSauronWillpowerCounter.hide();
                cIsolatedArea.questVariation.cContainer.c.visible = false;
            }
        }

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn.onGamePhaseDiagramChanged();
        if ( Session.allyId )
        {
            ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.onGamePhaseDiagramChanged();
        }
    }

    public mergeSolo(): void
    {
        let cSplitSauronArea: CSplitSauronArea = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "split_sauron" ).cSplitSauronArea;

        this._staging.cSauronActorArea.onMerged();

        // Hide custom panel.
        ServiceLocator.game.cGameWorld.cCustomPanelManager.setPanelVisibility( "split_sauron", false );

        // Move actors in isolated area to staging area.
        this._staging.cSauronActorArea.prependActors( cSplitSauronArea.cStaging.clearActors() );

        // Discard quest items.
        let arrQuestToken: Array<GameObject> = this._questHolder.cQuestHolder.findItems();
        for ( let questToken of arrQuestToken )
        {
            Utils.game.discard( questToken, action_scope_type.LOCAL );
        }
        // Discard active location items.
        let arrLocationToken: Array<GameObject> = this._activeLocationHolder.cActiveLocationHolder.findItems();
        for ( let locationToken of arrLocationToken )
        {
            Utils.game.discard( locationToken, action_scope_type.LOCAL );
        }

        // Move items in quest holder from split sauron area to sauron area.
        const kSelQuestIndex: number = cSplitSauronArea.cQuestHolder.selector.cMultiItemSelector.selectedIndex;
        arrQuestToken = cSplitSauronArea.cQuestHolder.findItems();
        for ( let questToken of arrQuestToken )
        {
            this._questHolder.cDropArea.forceDrop( questToken, null, action_scope_type.LOCAL );
        }
        if ( kSelQuestIndex >= 0 )
        {
            this._questHolder.cQuestHolder.selector.cMultiItemSelector.selectItem( kSelQuestIndex, action_scope_type.LOCAL );
        }
        // Move items in active location holder from isolated area to sauron area.
        const kSelLocationIndex: number = cSplitSauronArea.cActiveLocationHolder.selector.cMultiItemSelector.selectedIndex;
        arrLocationToken = cSplitSauronArea.cActiveLocationHolder.findItems();
        for ( let locationToken of arrLocationToken )
        {
            this._activeLocationHolder.cDropArea.forceDrop( locationToken, null, action_scope_type.LOCAL );
        }
        if ( kSelLocationIndex >= 0 )
        {
            this._activeLocationHolder.cActiveLocationHolder.selector.cMultiItemSelector.selectItem( kSelLocationIndex, action_scope_type.LOCAL );
        }
    }

    // overrides.
    public saveGame(): ISgSauronPlayArea
    {
        return {
            sideControls: this._sideControls.cSauronSideControls.saveGame(),
            staging: this._staging.cActorArea.saveGame(),
            questHolder: this._questHolder.cQuestHolder.saveGame(),
            activeLocationHolder: this._activeLocationHolder.cActiveLocationHolder.saveGame(),
            stagingThreatCounter: this._stagingThreatCounter.cStagingThreatCounter.saveGame(),
            playerWillpowerCounter: this._playerWillpowerCounter.cPlayerWillpowerCounter.saveGame(),
            allyWillpowerCounter: this._allyWillpowerCounter.cPlayerWillpowerCounter.saveGame(),
            sauronWillpowerCounter: this._sauronWillpowerCounter.cPlayerWillpowerCounter.saveGame(),
            questVariation: this._questVariation.cQuestVariation.saveGame() };
    }

    // overrides.
    public loadGame( sgSauronPlayArea: ISgSauronPlayArea, pass: number ): void
    {
        this._sideControls.cSauronSideControls.loadGame( sgSauronPlayArea.sideControls, pass );
        this._staging.cActorArea.loadGame( sgSauronPlayArea.staging, pass );
        this._questHolder.cQuestHolder.loadGame( sgSauronPlayArea.questHolder, pass );
        this._activeLocationHolder.cActiveLocationHolder.loadGame( sgSauronPlayArea.activeLocationHolder, pass );
        this._stagingThreatCounter.cStagingThreatCounter.loadGame( sgSauronPlayArea.stagingThreatCounter, pass );
        this._playerWillpowerCounter.cPlayerWillpowerCounter.loadGame( sgSauronPlayArea.playerWillpowerCounter, pass );
        this._allyWillpowerCounter.cPlayerWillpowerCounter.loadGame( sgSauronPlayArea.allyWillpowerCounter, pass );
        this._sauronWillpowerCounter.cPlayerWillpowerCounter.loadGame( sgSauronPlayArea.sauronWillpowerCounter, pass );
        this._questVariation.cQuestVariation.loadGame( sgSauronPlayArea.questVariation, pass );
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.SPLIT_STAGING_AREA )
        {
            this.split( player_type.ALLY );
        }
        else if ( action.playerActionType == player_action_type.MERGE_STAGING_AREA )
        {
            this.merge( player_type.ALLY );
        }
    }
}
