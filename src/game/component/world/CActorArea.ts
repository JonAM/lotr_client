import Component from "../Component";
import IGameObjectDropArea from "../../IGameObjectDropArea";

import { location_type } from "./CGameWorld";
import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import { player_type } from "./CPlayerArea";
import ServiceLocator from "../../../ServiceLocator";
import * as PIXI from "pixi.js";

import Signal from "../../../lib/signals/Signal";
import GameObject from "../../GameObject";
import CContainer from "../pixi/CContainer";
import CardTokenFactory from "../../CardTokenFactory";
import { TweenMax, Sine, gsap } from "gsap";
import Utils from "../../../Utils";
import CDropArea from "../input/CDropArea";
import { ISgActorArea, ISgActorTab, ISgCardToken, ISgGameModifier } from "../../../view/game/SaveGameView";
import DroppedEmitter from "../../DroppedEmitter";
import { layer_type } from "./CGameLayerProvider";
import FloatingMessage from "../../FloatingMessage";
import CMultiItemSelector, { mis_layout_type } from "../ui/CMultiItemSelector";
import CActorTab from "./CActorTab";
import GameModifierFactory from "../../GameModifierFactory";


export default class CActorArea extends Component implements IGameObjectDropArea
{
    // #region Attributes //

    // private:

    protected _location: location_type = null;
    private _width: number = null;
    private _lineColor: number = null;

    private _padding: PIXI.Point = null;
    protected _actorMargin: number = 0;
    private _busyIcon: PIXI.Graphics = null;

    private _bg: PIXI.Graphics = null;
    private _content: GameObject = null;
    private _gameModifierContainer: PIXI.Container = null;
    private _multiItemSelector: GameObject = null;
    private _arrActorTab: Array<GameObject> = null;

    // Quick access.
    private _selActorTab: GameObject = null;

    // Signals.
    private _onTokenSet: Signal = new Signal();
    private _onTokenUnset: Signal = new Signal();
    private _onContentChanged: Signal = new Signal();

    // Constants.
    private readonly _kMaxTabs: number = 3;

    // #endregion //


    // #region Properties //

    public get location(): location_type { return this._location; }
    public get selActorTab(): GameObject { return this._selActorTab; }
    public get padding(): PIXI.Point { return this._padding; }
    public get actorMargin(): number { return this._actorMargin; }
    public get multiItemSelector(): GameObject { return this._multiItemSelector; }

    public set location( value: location_type ) { this._location = value; }
    public set width( value: number ) { this._width = value; }
    public set lineColor( value: number ) { this._lineColor = value; }
    public set padding( value: PIXI.Point ) { this._padding = value; }
    public set actorMargin( value: number ) { this._actorMargin = value; }

    // Signals.
    public get onTokenSet(): Signal { return this._onTokenSet; }
    public get onTokenUnset(): Signal { return this._onTokenUnset; }
    public get onContentChanged(): Signal { return this._onContentChanged; }


    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CActorArea";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cGraphics != null, "CActorArea.ts :: init() :: CGraphics component not found." );
        console.assert( this._location != null, "CActorArea.ts :: init() :: this._location cannot be null." );
        console.assert( this._width != null, "CActorArea.ts :: init() :: this._width cannot be null." );
        console.assert( this._lineColor != null, "CActorArea.ts :: init() :: this._lineColor cannot be null." );
        
        if ( !this._padding )
        {
            this._padding = new PIXI.Point();
        }

        this._bg = new PIXI.Graphics();
        this._bg.beginFill( 0x000000, 0.25 );
        this._bg.drawRect( 0, 0, this._width, 204 );
        this._bg.endFill();
        this._go.cGraphics.g.addChild( this._bg );

        this._go.cGraphics.g.lineStyle( 2, this._lineColor, 1 );
        this._go.cGraphics.g.drawRect( 0, 0, this._width, 204 );

        this._content = new GameObject( [ new CContainer() ] );
        this._content.cContainer.c.position.copyFrom( this._padding );
        this._content.init();
        this._go.cContainer.addChild( this._content );

        this._arrActorTab = new Array<GameObject>();
        for ( let i: number = 0; i < this._kMaxTabs; ++i )
        {
            let actorTab: GameObject = new GameObject( [ new CContainer(), new CActorTab() ] );
            actorTab.cContainer.c.visible = i == 0;
            actorTab.cActorTab.width = this._width - this._content.cContainer.c.x * 2;
            actorTab.cActorTab.actorArea = this._go;
            actorTab.cActorTab.isAnimated = i == 0;
            actorTab.init();
            this._content.cContainer.addChild( actorTab );
            actorTab.cActorTab.onSizeUpdated.add( this.onActorTabSize_Updated, this );

            this._arrActorTab.push( actorTab );
        }
        this._selActorTab = this._arrActorTab[ 0 ];

        this._gameModifierContainer = new PIXI.Container();
        this._gameModifierContainer.position.set( 5, 45 );
        this._go.cContainer.c.addChild( this._gameModifierContainer );

        let icon: PIXI.Sprite = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( "eye" ) );
        Utils.game.limitSideSize( 25, icon );
        icon.anchor.set( 0.5, 0.5 );
        //
        this._busyIcon = new PIXI.Graphics();
        this._busyIcon.lineStyle( 1, 0x000000, 1 );
        this._busyIcon.beginFill( ServiceLocator.game.playerColors[ player_type.ALLY ] );
        this._busyIcon.drawCircle( 0, 0, 15 );
        this._busyIcon.endFill();
        this._busyIcon.position.set( 20, 55 );
        this._busyIcon.addChild( icon );
        this._busyIcon.visible = false;
        this._go.cContainer.c.addChild( this._busyIcon );

        this._multiItemSelector = new GameObject( [ new CContainer(), new CMultiItemSelector() ] );
        this._multiItemSelector.oid = this._go.oid + "_selector";
        this._multiItemSelector.cMultiItemSelector.maxItemCount = 3;
        this._multiItemSelector.cMultiItemSelector.owner = this._go;
        this._multiItemSelector.cMultiItemSelector.layoutType = mis_layout_type.VERTICAL;
        this._multiItemSelector.init();
        this._multiItemSelector.cContainer.c.position.set( this._width - 25, 5 );
        this._go.cContainer.addChild( this._multiItemSelector );
        //
        this._multiItemSelector.cMultiItemSelector.addItem();

        // Listen to events.
        this._multiItemSelector.cMultiItemSelector.onItemSelected.add( this.onTab_Selected, this );
    }

    public end(): void
    {
        // Cleanup events.
        this._multiItemSelector.cMultiItemSelector.onItemSelected.remove( this.onTab_Selected, this );

        this._onTokenSet.removeAll();
        this._onTokenUnset.removeAll();
        this._onContentChanged.removeAll();

        for ( let tab of this._arrActorTab )
        {
            tab.end();
        }
        this._arrActorTab = null;

        this._content.end();
        this._content = null;

        this._multiItemSelector.end();
        this._multiItemSelector = null;

        for ( let displayObject of this._gameModifierContainer.children )
        {
            ( ( displayObject as any ).go as GameObject ).end();
        }
    }

    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        this._multiItemSelector.cMultiItemSelector.setEnabled( isEnabled );
        this._go.cDropArea.setEnabled( isEnabled );
    }

    public findGameModifiers(): Array<GameObject>
    {
        let result: Array<GameObject> = new Array<GameObject>();

        for ( let displayObject of this._gameModifierContainer.children )
        {
            result.push( ( displayObject as any ).go as GameObject );
        }

        return result;
    }
    
    public findAllActors(): Array<GameObject> 
    { 
        let result: Array<GameObject> = new Array<GameObject>();

        for ( let tab of this._arrActorTab )
        {
            result = result.concat( tab.cActorTab.actors );
        }

        return result; 
    }

    public findInsertTabIndex( requiredSpace: number ): number
    {
        let result: number = 0;

        for ( let tab of this._arrActorTab )
        {
            const kRemainingSpace: number = tab.cActorTab.calcRemainingSpace();
            if ( kRemainingSpace >= requiredSpace )
            {
                break;
            }

            ++result;
        }

        return result < this._arrActorTab.length ? result : -1;
    }

    public addGameModifier( gameModifier: GameObject ): void
    { 
        gameModifier.onEnded.addOnce( this.onGameModifier_Discarded, this );

        gameModifier.cContainer.c.position.set( 0, this._gameModifierContainer.children.length * ( gameModifier.cGameModifier.calcHeight() + 10 ) );
        this._gameModifierContainer.addChild( gameModifier.cContainer.c );

        if ( ServiceLocator.game.isAnimated )
        {
            // Sfx.
            ServiceLocator.audioManager.playSfx( "token_dropped" );

            // Vfx.
            let droppedEmitter: DroppedEmitter = new DroppedEmitter();
            const kVfxLayer: GameObject = Utils.game.findGameProviderLayer( this._go, layer_type.VFX );
            droppedEmitter.layer = kVfxLayer;
            droppedEmitter.init();
            let vfxPos: PIXI.IPoint = kVfxLayer.cContainer.c.toLocal( gameModifier.cContainer.c.getGlobalPosition() );
            vfxPos.x += gameModifier.cGameModifier.calcWidth() * 0.5;
            vfxPos.y += gameModifier.cGameModifier.calcHeight() * 0.5;
            droppedEmitter.playSmall( vfxPos.x, vfxPos.y );

            Utils.anim.dropFromCorner( gameModifier.cContainer.c );
        }
    }

    public makeRoomForNewGameModifier(): void
    {
        if ( this._gameModifierContainer.children.length >= 2 )
        {
            ( this._gameModifierContainer.getChildAt( 1 )[ "go" ] as GameObject ).cGameModifier.discard( action_scope_type.LOCAL );
        }
    }

    public findNextGameModifierDropPosition( gameModifier: GameObject ): PIXI.Point
    {
        let result: PIXI.Point = new PIXI.Point();

        const kGameLayerProvider: GameObject = Utils.game.findGameObjectInBranchByComponentName( this._go, "CGameLayerProvider" );
        const kGameModifierContainerPos: PIXI.IPoint = kGameLayerProvider.cContainer.c.toLocal( this._gameModifierContainer.getGlobalPosition() );
        result.x = kGameLayerProvider.cContainer.c.x + kGameModifierContainerPos.x;
        result.y = kGameLayerProvider.cContainer.c.y + kGameModifierContainerPos.y + this._gameModifierContainer.children.length * ( gameModifier.cGameModifier.calcHeight() + 10 );

        return result;
    }

    // Multiplayer.
    public setBusy( isBusy: boolean ): void
    {
        this.setEnabled( !isBusy );

        let arrActor: Array<GameObject> = this._selActorTab.cActorTab.actors;
        for ( let actor of arrActor )
        {
            actor.cCardToken.setEnabled( !isBusy );
        }

        this._busyIcon.visible = isBusy;
    }

    public prependActors( arrActor: Array<GameObject> ): void
    {
        if ( arrActor.length == 0 ) { return; }

        const kPrevActorCount: number = this._selActorTab.cActorTab.actors.length;

        const kInsertTabIndex: number = this.findInsertTabIndex( arrActor[ 0 ].cCardToken.calcWidth() );
        for ( let i: number = kInsertTabIndex; i < this._arrActorTab.length; ++i )
        {
            let insertTab: GameObject = this._arrActorTab[ i ];
            let remainingSpace: number = insertTab.cActorTab.calcRemainingSpace();
            let arrInsertActor: Array<GameObject> = new Array<GameObject>();
            while ( arrActor.length > 0 && remainingSpace >= this._actorMargin + arrActor[ 0 ].cCardToken.calcWidth() )
            {
                let insertActor: GameObject = arrActor.shift();
                arrInsertActor.push( insertActor );

                remainingSpace -= this._actorMargin + insertActor.cCardToken.calcWidth();
            }

            insertTab.cActorTab.prependActors( arrInsertActor );

            if ( arrActor.length == 0 )
            {
                break;
            }
        }

        this.updateMultiItemSelector();
        if ( kPrevActorCount == this._selActorTab.cActorTab.actors.length )
        {
            this.onTab_Selected( -1, this._multiItemSelector.cMultiItemSelector.itemCount - 1 );
            this._multiItemSelector.cMultiItemSelector.setSelectedIndex( this._multiItemSelector.cMultiItemSelector.itemCount - 1 );
        }
    }

        private updateMultiItemSelector(): void
        {
            let usedTabCount: number = 1;
            for ( let i: number = usedTabCount; i < this._arrActorTab.length; ++i )
            {
                if ( this._arrActorTab[ i ].cActorTab.actors.length > 0 )
                {
                    usedTabCount += 1;
                }
                else
                {
                    break;
                }
            }

            if ( this._multiItemSelector.cMultiItemSelector.itemCount > usedTabCount )
            {
                while ( this._multiItemSelector.cMultiItemSelector.itemCount > usedTabCount )
                {
                    this._multiItemSelector.cMultiItemSelector.removeItem();
                }
            }
            else
            {
                while ( this._multiItemSelector.cMultiItemSelector.itemCount < usedTabCount )
                {
                    this._multiItemSelector.cMultiItemSelector.addItem();
                }
            }
        }


    public appendActors( arrActor: Array<GameObject> ): void
    {
        if ( arrActor.length == 0 ) { return; }

        const kPrevActorCount: number = this._selActorTab.cActorTab.actors.length;

        const kInsertTabIndex: number = this.findInsertTabIndex( arrActor[ 0 ].cCardToken.calcWidth() );
        for ( let i: number = kInsertTabIndex; i < this._arrActorTab.length; ++i )
        {
            let insertTab: GameObject = this._arrActorTab[ i ];
            let remainingSpace: number = insertTab.cActorTab.calcRemainingSpace();
            let arrInsertActor: Array<GameObject> = new Array<GameObject>();
            while ( arrActor.length > 0 && remainingSpace >= this._actorMargin + arrActor[ 0 ].cCardToken.calcWidth() )
            {
                let insertActor: GameObject = arrActor.shift();
                arrInsertActor.push( insertActor );

                remainingSpace -= this._actorMargin + insertActor.cCardToken.calcWidth();
            }

            insertTab.cActorTab.appendActors( arrInsertActor );

            if ( arrActor.length == 0 )
            {
                break;
            }
        }

        this.updateMultiItemSelector();
        if ( kPrevActorCount == this._selActorTab.cActorTab.actors.length )
        {
            this.onTab_Selected( -1, this._multiItemSelector.cMultiItemSelector.itemCount - 1 );
            this._multiItemSelector.cMultiItemSelector.setSelectedIndex( this._multiItemSelector.cMultiItemSelector.itemCount - 1 );
        }
    }

    public sort(): void
    {
        for ( let tab of this._arrActorTab )
        {
            let remainingSpace: number = tab.cActorTab.calcRemainingSpace();
            let nextTabIndex: number = this._arrActorTab.indexOf( tab ) + 1;
            let arrMovedActor: Array<GameObject> = new Array<GameObject>();
            for ( let i: number = nextTabIndex; i < this._arrActorTab.length; ++i )
            {
                let nextTab: GameObject = this._arrActorTab[ i ];
                while ( nextTab.cActorTab.actors.length > 0 && remainingSpace >= nextTab.cActorTab.actors[ 0 ].cCardToken.calcWidth() + this._actorMargin )
                {
                    let movedActor: GameObject = nextTab.cActorTab.actors.shift();
                    nextTab.cActorTab.removeActorListeners( movedActor );
                    arrMovedActor.push( movedActor );

                    remainingSpace -= movedActor.cCardToken.calcWidth() + this._actorMargin;
                }
            }

            for ( let movedActor of arrMovedActor )
            {
                tab.cActorTab.addActorAtIndex( movedActor, null );
            }
            tab.cActorTab.sort( arrMovedActor.length > 0 ? arrMovedActor : null );
        }

        this.updateMultiItemSelector();
        if ( this._arrActorTab.indexOf( this._selActorTab ) > this._multiItemSelector.cMultiItemSelector.itemCount - 1 )
        {
            this.onTab_Selected( -1, this._multiItemSelector.cMultiItemSelector.itemCount - 1 );
            this._multiItemSelector.cMultiItemSelector.setSelectedIndex( this._multiItemSelector.cMultiItemSelector.itemCount - 1 );
        }
    }

    public clearActors(): Array<GameObject>
    {
        let result: Array<GameObject> = new Array<GameObject>();

        for ( let tab of this._arrActorTab )
        {
            while ( tab.cActorTab.actors.length > 0 )
            {
                let actor: GameObject = tab.cActorTab.actors.shift();
                tab.cActorTab.removeActorListeners( actor );

                this._onTokenUnset.dispatch( actor );

                result.push( actor );
            }
            tab.cContainer.c.removeChildren();
        }

        this.updateMultiItemSelector();
        this.onTab_Selected( -1, 0 );
        this._multiItemSelector.cMultiItemSelector.setSelectedIndex( 0 );
        
        return result;
    }

    public showDropTab( actionScope: action_scope_type ): void
    {
        let tabIndex: number = this.findInsertTabIndex( 130 + this._actorMargin );
        if ( tabIndex != this._arrActorTab.indexOf( this._selActorTab ) )
        {
            this._multiItemSelector.cMultiItemSelector.selectItem( tabIndex, actionScope );
        }
    }

    public showActor( actor: GameObject, actionScope: action_scope_type ): void
    {
        let tabIndex: number = 0;
        for ( let tab of this._arrActorTab )
        {
            if ( tab.cActorTab.actors.indexOf( actor ) != -1 )
            {
                break;
            }
            else
            {
                tabIndex += 1;
            }
        }

        if ( tabIndex != this._arrActorTab.indexOf( this._selActorTab ) )
        {
            this._multiItemSelector.cMultiItemSelector.selectItem( tabIndex, actionScope );
        }
    }

    // virtual.
    public saveGame(): ISgActorArea
    {
        let arrSgActorTab: Array<ISgActorTab> = new Array<ISgActorTab>();
        for ( let actorTab of this._arrActorTab )
        {
            arrSgActorTab.push( actorTab.cActorTab.saveGame() );
        }

        let arrGameModifier: Array<ISgGameModifier> = new Array<ISgGameModifier>();
        for ( let displayObject of this._gameModifierContainer.children )
        {
            arrGameModifier.push( ( ( displayObject as any ).go as GameObject ).cGameModifier.saveGame() );
        }

        return { 
            isVisible: this._go.cContainer.c.visible,
            selTabIndex: this._arrActorTab.indexOf( this._selActorTab ),
            tabs: arrSgActorTab, 
            gameModifiers: arrGameModifier };
    }

    // virtual.
    public loadGame( sgActorArea: ISgActorArea, pass: number ): void
    {
        if ( pass == 0 )
        {
            this._go.cContainer.c.visible = sgActorArea.isVisible;

            let tabIndex: number = 0;
            for ( let sgActorTab of sgActorArea.tabs )
            {
                this._arrActorTab[ tabIndex ].cActorTab.loadGame( sgActorTab, pass );

                ++tabIndex;
            }

            if ( sgActorArea.gameModifiers.length > 0 )
            {
                let gameModifierFactory: GameModifierFactory = new GameModifierFactory();
                for ( let sgGameModifier of sgActorArea.gameModifiers )
                {
                    let card: GameObject = GameObject.find( sgGameModifier.oid.substr( 0, sgGameModifier.oid.indexOf( "_game_modifier" ) ) );
                    let gameModifier: GameObject = gameModifierFactory.create( card );
                    card.end();
                    gameModifier.cGameModifier.loadGame( sgGameModifier, pass );
                    
                    this.addGameModifier( gameModifier );
                }
            }

            let index: number = 0;
            for ( let displayObject of this._gameModifierContainer.children )
            {
                let gameModifier: GameObject = ( displayObject as any ).go as GameObject;
                gameModifier.cGameModifier.loadGame( sgActorArea.gameModifiers[ index ], pass );

                ++index;
            }

            this.updateMultiItemSelector();
            this._multiItemSelector.cMultiItemSelector.selectItem( sgActorArea.selTabIndex, action_scope_type.LOCAL );
        }
    }

    // #endregion //


    // #region IGameObjectDropArea //

    // virtual.
    public validateDroppedGameObject( dropped: GameObject, from: GameObject, global: PIXI.Point ): boolean
    {
        let result: boolean = true;

        if ( global && dropped.cCardToken )
        {
            result = this._go != from;
            if ( !result )
            {
                const kInsertIndex: number = this._selActorTab.cActorTab.findActorInsertIndex( global );
                const kCurIndex: number = this._selActorTab.cActorTab.actors.indexOf( dropped );
                const kIndexVar: number = kInsertIndex - kCurIndex;
                if ( ( kIndexVar > 1 || kIndexVar < 0 )
                    && ( kInsertIndex - 1 != kCurIndex || kCurIndex != this._selActorTab.cActorTab.actors.length - 1 ) )
                {
                    this._selActorTab.cActorTab.setActorIndex( dropped, kInsertIndex );

                    // Multiplayer.
                    ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_CHARACTER_AREA_TOKEN_INDEX, null, [ dropped.oid, kInsertIndex ] );
                }
            }
            else
            {
                let requiredSpace: number = this._actorMargin;
                if ( dropped.cCardToken )
                {
                    requiredSpace += dropped.cCardToken.calcWidth();
                }
                else 
                {
                    requiredSpace += 130;
                }
                result = this.findInsertTabIndex( requiredSpace ) != -1;
                if ( !result )
                {
                    let floatinMessage = new FloatingMessage();
                    floatinMessage.show( jQuery.i18n( "NOT_ENOUGH_SPACE" ), ServiceLocator.game.app.renderer.plugins.interaction.mouse.global );
                }
                
                if ( result )
                {
                    // To avoid the unexpected effect when an actor from a different actor area is dropped at the end of another actor area that is full.
                    // The dropped actor is placed at the last postition and the actor that already was there is moved to the next tab!
                    const kActorInsertIndex: number = this._selActorTab.cActorTab.findActorInsertIndex( global );
                    result = this._selActorTab.cActorTab.calcRemainingSpace() >= requiredSpace
                        || kActorInsertIndex < this._selActorTab.cActorTab.actors.length;
                }
            }
        }

        if ( result )
        {
            result = 
                dropped.cCardToken != null
                || dropped.cAttachment != null 
                    && ( dropped.cAttachment.curSide.cardInfo.type_code != "quest-intro"
                        && dropped.cAttachment.curSide.cardInfo.type_code != "quest" );
        }

        return result;
    }

    // virtual.
    public processDroppedGameObject( dropped: GameObject, global: PIXI.IPoint, actionScope: action_scope_type ): void
    {
        // Obtain card token.
        let actor: GameObject = null;
        if ( dropped.cCardToken ) 
        {
            dropped.cContainer.c.position.set( 0 ); // for example, if dropping from active location holder.
            //dropped.cDropArea.isPropagate = false;
            actor = dropped;
        }
        else if ( dropped.cCard )
        {
            let ctf: CardTokenFactory = new CardTokenFactory();
            actor = ctf.create( dropped );
        }
        else if ( dropped.cAttachment )
        {
            let ctf: CardTokenFactory = new CardTokenFactory();
            actor = ctf.createFromAttachment( dropped );
        }
        else if ( dropped.cShadowCardMini )
        {
            let ctf: CardTokenFactory = new CardTokenFactory();
            actor = ctf.create( dropped.cShadowCardMini.card );
            dropped.cShadowCardMini.onDiscarded.dispatch( dropped );
        }
        actor.cCardToken.location = this._location;

        // Token positioning.
        if ( this._selActorTab.cActorTab.calcRemainingSpace() < actor.cCardToken.calcWidth() + this._actorMargin )
        {
            let movedActor: GameObject = this._selActorTab.cActorTab.actors.pop();
            this._selActorTab.cActorTab.removeActorListeners( movedActor );

            const kNextTabIndex: number = this._arrActorTab.indexOf( this._selActorTab ) + 1;
            let nextActorTab: GameObject = this._arrActorTab[ kNextTabIndex ];
            this.recExpandActorTabs( nextActorTab, movedActor );
        }
        const kInsertIndex: number = this._selActorTab.cActorTab.findActorInsertIndex( global as PIXI.Point );
        this._selActorTab.cActorTab.addActorAtIndex( actor, kInsertIndex );
        this._selActorTab.cActorTab.sort( [ actor ] );
        
        actor.cCardToken.isAnimated = false;
        actor.cCardToken.setFaceUp( action_scope_type.LOCAL ); 
        actor.cCardToken.isAnimated = true;
        actor.cDropArea.setEnabled( true ); 

        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            let local: PIXI.IPoint = global;
            if ( global.x != CDropArea.kPredefinedDropPositionCode )
            {
                local = this._go.cContainer.c.toLocal( global );
            }
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.DROP_AT_DROP_AREA, null, [ dropped.oid, this._go.oid, local ] );
        }

        this._onTokenSet.dispatch( actor );
        this._onContentChanged.dispatch();
        
        if ( dropped.cCard || dropped.cAttachment || dropped.cShadowCardMini )
        {
            dropped.end();
        }

        this.updateMultiItemSelector();
    }

    // #endregion //


    // #region Callbacks //

    // IMPORTANT: Always due to player input!
    private onActorTabSize_Updated( actorTab: GameObject ): void
    {
        if ( actorTab.cActorTab.calcWidth() > this._width - this._content.cContainer.c.x * 2 )
        {
            // Attachment added.
            let movedActor: GameObject = actorTab.cActorTab.actors.pop();
            actorTab.cActorTab.removeActorListeners( movedActor );

            const kNextTabIndex: number = this._arrActorTab.indexOf( actorTab ) + 1;
            let nextActorTab: GameObject = this._arrActorTab[ kNextTabIndex ];
            this.recExpandActorTabs( nextActorTab, movedActor );

            actorTab.cActorTab.sort( null );
        }
        else
        {
            // Attachment discarded, actor discarded or moved.
            const kNextTabIndex: number = this._arrActorTab.indexOf( actorTab ) + 1;
            let isSorted: boolean = false;
            if ( kNextTabIndex < this._arrActorTab.length )
            {
                let nextActorTab: GameObject = this._arrActorTab[ kNextTabIndex ];
                isSorted = this.recCompactActorTabs( actorTab, nextActorTab );
            }

            if ( !isSorted )
            {
                actorTab.cActorTab.sort( null );
            }
        }

        this.updateMultiItemSelector();
        if ( this._arrActorTab.indexOf( this._selActorTab ) > this._multiItemSelector.cMultiItemSelector.itemCount - 1 )
        {
            this.onTab_Selected( -1, this._multiItemSelector.cMultiItemSelector.itemCount - 1 );
            this._multiItemSelector.cMultiItemSelector.setSelectedIndex( this._multiItemSelector.cMultiItemSelector.itemCount - 1 );
        }
    }

        private recExpandActorTabs( nextActorTab: GameObject, movedActor: GameObject ): void
        {
            if ( movedActor )
            {
                nextActorTab.cActorTab.addActorAtIndex( movedActor, 0 );
            
                const kRemainingSpace: number = nextActorTab.cActorTab.calcRemainingSpace();
                if ( kRemainingSpace < movedActor.cCardToken.calcWidth() + this._actorMargin )
                {
                    const kNextTabIndex: number = this._arrActorTab.indexOf( nextActorTab ) + 1;
                    if ( kNextTabIndex < this._arrActorTab.length )
                    {
                        let recNextActorTab: GameObject = this._arrActorTab[ kNextTabIndex ];
                        const kRecMovedActor: GameObject = nextActorTab.cActorTab.actors.pop();
                        this.recExpandActorTabs( recNextActorTab, kRecMovedActor );
                    }
                }
            }

            nextActorTab.cActorTab.sort( null );
        }

        private recCompactActorTabs( prevActorTab: GameObject, nextActorTab: GameObject ): boolean
        {
            let isPrevActorTabSorted: boolean = false;

            if ( nextActorTab.cActorTab.actors.length > 0 )
            {
                let movedActor: GameObject = nextActorTab.cActorTab.actors[ 0 ];
                if ( movedActor.cCardToken.calcWidth() <= prevActorTab.cActorTab.calcRemainingSpace() )
                {
                    let movedActor: GameObject = nextActorTab.cActorTab.actors.shift();
                    nextActorTab.cActorTab.removeActorListeners( movedActor );
                    prevActorTab.cActorTab.addActorAtIndex( movedActor, null );
                    prevActorTab.cActorTab.sort( [ movedActor ] );

                    isPrevActorTabSorted = true;

                    const kNextTabIndex: number = this._arrActorTab.indexOf( nextActorTab ) + 1;
                    if ( kNextTabIndex < this._arrActorTab.length )
                    {
                        let recNextActorTab: GameObject = this._arrActorTab[ kNextTabIndex ];
                        this.recCompactActorTabs( nextActorTab, recNextActorTab );
                    }
                }

                nextActorTab.cActorTab.sort( null );
            }

            return isPrevActorTabSorted;
        }

    private onTab_Selected( lastIndex: number, selIndex: number ): void
    {
        this._selActorTab.cContainer.c.visible = false;
        this._selActorTab.cActorTab.isAnimated = false;

        this._selActorTab = this._arrActorTab[ selIndex ];
        
        this._selActorTab.cContainer.c.visible = true;
        this._selActorTab.cActorTab.isAnimated = true;
    }

    private onGameModifier_Discarded( gameModifier: GameObject ): void
    {
        let index: number = 0;
        gameModifier.cContainer.c.position.set( 0, this._gameModifierContainer.children.length * ( gameModifier.cGameModifier.calcHeight() + 10 ) )
        for ( let displayObject of this._gameModifierContainer.children )
        {
            displayObject.y = ( ( ( displayObject as PIXI.Container )[ "go" ] as GameObject ).cGameModifier.calcHeight() + 10 ) * index;

            if ( displayObject[ "go" ] != gameModifier )
            {
                ++index;
            }
        }
    }

    // #endregion //
}