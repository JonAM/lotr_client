import Component from "../Component";
import IGameObjectDropArea from "../../IGameObjectDropArea";

import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CardActivationController from "./card_activation_area/CardActivationController";
import Signal from "../../../lib/signals/Signal";
import { location_type } from "./CGameWorld";
import CardDrawController from "./card_activation_area/CardDrawController";
import { layer_type } from "./CGameLayerProvider";
import FloatingMessage from "../../FloatingMessage";
import Utils from "../../../Utils";
import Session from "../../../Session";
import SignalBinding from "../../../lib/signals/SignalBinding";


export default class CCardActivationArea extends Component implements IGameObjectDropArea
{
    // #region Attributes //

    // private:

    private _cardActivationController: CardActivationController = null;
    private _cardDrawController: CardDrawController = null;

    private _isWaitingLockStatus: boolean = false;
    private _isLockOwner: boolean = false;

    // Signals.
    private _onCompleted: Signal = new Signal();

    // #endregion //


    // #region Properties //

    // Signals.
    public get onCompleted(): Signal { return this._onCompleted; }

    // #endregion //


     // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CCardActivationArea";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cSprite != null, "CCardActivationArea.ts :: init() :: CGraphics component not found." );
    
        this._go.cContainer.c.interactive = true;
        this._go.cContainer.c.visible = false;
        
        this._go.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "transparent" ).data );
        this._go.cSprite.s.x = ServiceLocator.game.cGameWorld.cPlayerArea.sideControls.cContainer.c.width;
        this._go.cSprite.s.scale.set( ServiceLocator.game.app.screen.width - this._go.cSprite.s.x, ServiceLocator.game.app.screen.height ); 
    }

    public end(): void
    {
        this._onCompleted.removeAll();

        this._isWaitingLockStatus = false;
        this._isLockOwner = false;

        ServiceLocator.game.dragShadowManager.onDragShadowMoved.remove( this.onDragShadow_Moved, this );

        super.end();
    }

    public show(): void
    {
        this._go.cContainer.c.visible = true;

        ServiceLocator.game.dragShadowManager.onDragShadowMoved.add( this.onDragShadow_Moved, this );
    }

    public hide(): void
    {
        this._go.cContainer.c.visible = false;

        ServiceLocator.game.dragShadowManager.onDragShadowMoved.remove( this.onDragShadow_Moved, this );

        // Multiplayer.
        if ( Session.allyId && this._isLockOwner )
        {
            this._go.cShareableGameElement.notifyUnlock();
            this._isLockOwner = false;
        }
    }

    public forceCardActivation( card: GameObject, from: location_type, actionScope: action_scope_type ): void
    {
        //ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( dropped ), "rad_play", true );

        // Sfx.
        ServiceLocator.audioManager.playSfx( "card_played" );

        let underTableLayer: GameObject = ServiceLocator.game.root.cGameLayerProvider.get( layer_type.UNDER_TABLE );
        underTableLayer.cDropArea.forceDrop( card, null, actionScope );

        this._cardActivationController = new CardActivationController();
        this._cardActivationController.card = card;
        this._cardActivationController.presentFrom = from;
        this._cardActivationController.init();
        this._cardActivationController.onCompleted.addOnce( this.onCardActivation_Completed, this );

        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.CARD_ACTIVATION_PRESENT, null, [ card.oid, from ] );
        }
    }

    public forceDrawCard( card: GameObject ): void
    {
        //ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( dropped ), "rad_play", true );

        // Sfx.
        ServiceLocator.audioManager.playSfx( "card_played" );
        
        let underTableLayer: GameObject = ServiceLocator.game.root.cGameLayerProvider.get( layer_type.UNDER_TABLE );
        underTableLayer.cDropArea.forceDrop( card, null, action_scope_type.MULTIPLAYER );

        this._cardDrawController = new CardDrawController();
        this._cardDrawController.card = card;
        this._cardDrawController.presentationFrom = location_type.MY_DECK;
        this._cardDrawController.init();
        this._cardDrawController.onCompleted.addOnce( this.onCardDraw_Completed, this );
    }

    // #endregion //


    // #region IGameObjectDropArea //

    public validateDroppedGameObject( dropped: GameObject, from: GameObject, global: PIXI.Point ): boolean
    {
        let result: boolean = this._isEnabled 
            && !this._isWaitingLockStatus 
            && dropped.cCard != null;

        if ( result )
        {
            result = ServiceLocator.game.root.cGameLayerProvider.get( layer_type.ACTIVATED_CARD ).cContainer.c.children.length == 0;
        }       
        if ( result && dropped.cCard.back.cardInfo.type_code == "quest" )
        {
            let questHolder: GameObject = Utils.game.hitFirstTest( global, "cQuestHolder", ServiceLocator.game.root.cContainer.c );
            if ( questHolder )
            {
                window.setTimeout( () => { questHolder.cDropArea.forceDrop( dropped, null, action_scope_type.MULTIPLAYER ); } );

                result = false;
            }
        }

        // Multiplayer.
        if ( Session.allyId && !result && this._isLockOwner )
        {
            this._go.cShareableGameElement.notifyUnlock();
            this._isLockOwner = false;
        }

        return result;
    }

    public processDroppedGameObject( dropped: GameObject, global: PIXI.IPoint, actionScope: action_scope_type ): void
    {
        //ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( dropped ), "rad_play", true );
            
        if ( dropped.cCard.location == location_type.MY_DECK && !ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.deck.cDeckIndicator.cViewer.isVisible() )
        {
            // Silently draw a card.
            let underTableLayer: GameObject = ServiceLocator.game.root.cGameLayerProvider.get( layer_type.UNDER_TABLE );
            underTableLayer.cDropArea.forceDrop( dropped, null, action_scope_type.LOCAL );

            this._cardDrawController = new CardDrawController();
            this._cardDrawController.card = dropped;
            this._cardDrawController.init();
            this._cardDrawController.onCompleted.addOnce( this.onCardDraw_Completed, this );
        }
        else
        {
            let underTableLayer: GameObject = ServiceLocator.game.root.cGameLayerProvider.get( layer_type.UNDER_TABLE );
            underTableLayer.cDropArea.forceDrop( dropped, null, actionScope );

            this._cardActivationController = new CardActivationController();
            this._cardActivationController.card = dropped;
            this._cardActivationController.init();
            this._cardActivationController.onCompleted.addOnce( this.onCardActivation_Completed, this );

            // Multiplayer.
            if ( actionScope == action_scope_type.MULTIPLAYER )
            {
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.CARD_ACTIVATION_PRESENT, null, [ dropped.oid, dropped.cCard.location ] );

                if ( Session.allyId && this._isLockOwner )
                {
                    this._go.cShareableGameElement.notifyUnlock();
                    this._isLockOwner = false;
                }
            }
        }
    }

    // #endregion //

    private onCardActivation_Completed(): void
    {
        this._cardActivationController.end();
        this._cardActivationController = null;

        this._onCompleted.dispatch();
    }

    private onCardDraw_Completed(): void
    {
        this._cardDrawController.end();
        this._cardDrawController = null;

        this._onCompleted.dispatch();
    }

    private onDragShadow_Moved( dragShadow: GameObject, global: PIXI.Point ): void
    {
        if ( this._isEnabled 
            && this._go.cContainer.c.getBounds().contains( global.x, global.y )
            && ServiceLocator.game.root.cGameLayerProvider.get( layer_type.ACTIVATED_CARD ).cContainer.c.children.length == 0
            && ( dragShadow.cDragShadow.from.cCard.back.cardInfo.type_code != "quest" 
                || Utils.game.hitFirstTest( global, "cQuestHolder", ServiceLocator.game.root.cContainer.c ) == null ) )
        {
            // Multiplayer.
            if ( Session.allyId )
            {
                if ( !this._isWaitingLockStatus && !this._isLockOwner )
                {
                    this._isWaitingLockStatus = true;

                    let sb: SignalBinding = this._go.cShareableGameElement.onLockStatusReceived.addOnce( this.onLockStatus_Received, this );
                    sb.params = [ dragShadow ];
                    this._go.cShareableGameElement.notifyLock();
                }
            }
            else
            {    
                dragShadow.cDragShadow.setGlow();
            }
        }
        else
        {
            // Multiplayer.
            if ( Session.allyId && this._isLockOwner )
            {
                this._go.cShareableGameElement.notifyUnlock();
                this._isLockOwner = false;
            }
            else
            {
                dragShadow.cDragShadow.clearGlow();
            }
        }
    }

        private onLockStatus_Received( dragShadow: GameObject, isLockOpen: boolean ): void
        {
            this._isWaitingLockStatus = false;

            if ( isLockOpen )
            {
                // IMPORTANT: If the server response is received after the dragging has ended, dragShadow.cDragShadow may be null at this point!
                if ( dragShadow.cDragShadow )
                {
                    dragShadow.cDragShadow.setGlow();
                }
                this._isLockOwner = true;
            }
        }
}