import Component from "../Component";

import { location_type } from "./CGameWorld";
import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import CActorArea from "./CActorArea";
import GameObject from "../../GameObject";
import CContainer from "../pixi/CContainer";
import CGraphics from "../pixi/CGraphics";
import CDropArea from "../input/CDropArea";
import CLogLocation from "./CLogLocation";
import CPlayerSideControls from "../ui/CPlayerSideControls";
import CPlayerActorArea from "./actor_area/CPlayerActorArea";
import CEngagedActorArea from "./actor_area/CEngagedActorArea";
import { ISgPlayerPlayArea } from "../../../view/game/SaveGameView";
import Session from "../../../Session";
import CGameModifier from "../card/CGameModifier";
import CHighlightPoiReceptor from "./poi_receptor/CHighlightPoiReceptor";
import { ICard } from "../../CardDB";
import CTargetSelector, { target_selection_type } from "../input/CTargetSelector";
import CTargetReceptor from "./CTargetReceptor";
import CDraggable from "../input/CDraggable";


export default class CPlayerArea extends Component
{
    // #region Attributes //

    // private:

    private _playerType: player_type = null;

    private _engaged: GameObject = null;
    private _home: GameObject = null; 
    private _sideControls: GameObject = null;

    // #endregion //


    // #region Properties //

    public get engaged(): GameObject { return this._engaged; }
    public get cEngaged(): CEngagedActorArea { return this._engaged.cEngagedActorArea; }
    public get home(): GameObject { return this._home; }
    public get cHome(): CActorArea { return this._home.cActorArea; }
    public get sideControls(): GameObject { return this._sideControls; }
    public get cSideControls(): CPlayerSideControls { return this._sideControls.cPlayerSideControls; }

    public set playerType( value: player_type ) { this._playerType = value; }

    // #endregion //

    
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CPlayerArea";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CPlayerArea.ts :: init() :: CContainer component not found." );
        console.assert( this._playerType != null, "CPlayerArea.ts :: init() :: this._playerType cannot be null." );

        this._engaged = this.createEngaged();
        this._engaged.cContainer.c.position.set( 340, this._playerType == player_type.PLAYER ? 0 : 224 );
        this._go.cContainer.addChild( this._engaged );

        this._home = this.createHome();
        this._home.cContainer.c.position.set( 220, this._playerType == player_type.PLAYER ? 214 : 10 );
        this._go.cContainer.addChild( this._home );

        if ( this._playerType == player_type.ALLY )
        {
            this._engaged.cActorArea.multiItemSelector.cMultiItemSelector.setEnabled( false );
            this._home.cActorArea.multiItemSelector.cMultiItemSelector.setEnabled( false );
        }

        this._sideControls = this.createPlayerSideControls();
        if ( this._playerType == player_type.PLAYER )
        {
            this._sideControls.cContainer.c.y = 116;
        }
        this._go.cContainer.addChild( this._sideControls );
    }

    public end(): void
    {
        this._engaged.end();
        this._engaged = null;

        this._home.end();
        this._home = null;

        this._sideControls.end();
        this._sideControls = null;
    }

    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        this._engaged.cEngagedActorArea.setEnabled( isEnabled );
        this._home.cPlayerActorArea.setEnabled( isEnabled );
        this._sideControls.cPlayerSideControls.setEnabled( isEnabled );
    }

    // overrides.
    public saveGame(): ISgPlayerPlayArea
    {
        return {
            home: this._home.cActorArea.saveGame(),
            engaged: this._engaged.cActorArea.saveGame(),
            sideControls: this._sideControls.cPlayerSideControls.saveGame() };
    }

    // overrides.
    public loadGame( sgPlayerPlayArea: ISgPlayerPlayArea, pass: number ): void
    {
        this._home.cActorArea.loadGame( sgPlayerPlayArea.home, pass );
        this._engaged.cActorArea.loadGame( sgPlayerPlayArea.engaged, pass );
        this._sideControls.cPlayerSideControls.loadGame( sgPlayerPlayArea.sideControls, pass );
    }

    // private:

    private createHome(): GameObject
    {
        let home: GameObject = new GameObject( [ new CGraphics(), new CPlayerActorArea(), new CDropArea(), new CLogLocation() ] );
        home.oid = "home_" + GameObject.findPlayerSufix( this._playerType );
        const kLocation: location_type = this._playerType == player_type.PLAYER ? location_type.MY_HOME : location_type.ALLY_HOME;
        home.cActorArea.location = kLocation;
        home.cActorArea.width = 1690;
        home.cActorArea.lineColor = ServiceLocator.game.playerColors[ this._playerType ];
        home.cActorArea.padding = new PIXI.Point( 35, 5 );
        home.cActorArea.actorMargin = 30;
        home.cDropArea.target = home.cActorArea;
        home.cDropArea.isSelfDropAllowed = true;
        home.cLogLocation.location = kLocation;
        home.init();

        let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( "home" ).data ) );
        Utils.game.limitSideSize( 30, icon );
        icon.position.set( 5 );
        home.cContainer.c.addChildAt( icon, 1 );

        return home;
    }

    private createEngaged(): GameObject
    {
        let engaged: GameObject = new GameObject( [ new CGraphics(), new CEngagedActorArea(), new CDropArea(), new CLogLocation() ] );
        engaged.oid = "engaged_" + GameObject.findPlayerSufix( this._playerType );
        const kLocation: location_type = this._playerType == player_type.PLAYER ? location_type.MY_ENGAGED : location_type.ALLY_ENGAGED;
        engaged.cActorArea.location = kLocation;
        engaged.cActorArea.width = 1450;
        engaged.cActorArea.lineColor = ServiceLocator.game.playerColors[ this._playerType ];
        engaged.cActorArea.padding = new PIXI.Point( 35, 5 );
        engaged.cActorArea.actorMargin = 30;
        engaged.cDropArea.target = engaged.cActorArea;
        engaged.cDropArea.isSelfDropAllowed = true;
        engaged.cLogLocation.location = kLocation;
        engaged.init();

        let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( "engaged" ).data ) );
        Utils.game.limitSideSize( 30, icon );
        icon.position.set( 5 );
        engaged.cContainer.c.addChildAt( icon, 1 );

        return engaged
    }

    private createPlayerSideControls(): GameObject
    {
        let sideControls: GameObject = new GameObject( [ new CContainer(), new CPlayerSideControls(), new CDropArea() ] );
        sideControls.oid = "side_controls_" + GameObject.findPlayerSufix( this._playerType );
        sideControls.cPlayerSideControls.playerType = this._playerType;
        sideControls.cDropArea.target = sideControls.cPlayerSideControls;
        sideControls.init();

        return sideControls;
    }

    // #endregion //
}

export const enum player_type
{
    PLAYER = 0,
    ALLY,
    SAURON
}