import CCustomArea from "../CCustomArea";

import { player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../../ServiceLocator";

import { ISgCustomPanel } from "../../../../view/game/SaveGameView";
import GameObject from "../../../GameObject";
import { IOppActionListener, IOpponentAction } from "../../../AllyActionManager";
import CContainer from "../../pixi/CContainer";
import CGraphics from "../../pixi/CGraphics";
import CButton from "../../input/CButton";
import SignalBinding from "../../../../lib/signals/SignalBinding";
import CSprite from "../../pixi/CSprite";
import Utils from "../../../../Utils";


export default class CInvestigationList extends CCustomArea implements IOppActionListener
{
    // #region Attributes //

    // private:

    private _arrCheckBtn: Array<GameObject> = null;
    private _arrCheckBtnState: Array<boolean> = null;

    // Constants.
    private readonly _kPanelWidth: number = 500;
    private readonly _kPanelHeight: number = 490;

    // #endregion //

    
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CInvestigationList";
    }

    public init(): void
    {
        super.init();

        this._arrCheckBtn = new Array<GameObject>();
        this._arrCheckBtnState = new Array<boolean>();
        
        this._go.cContainer.c.interactive = true;

        this._go.cGraphics.g.lineStyle( 2, 0x000000, 1 );
        this._go.cGraphics.g.beginFill( 0xefe3af, 0.7 );
        this._go.cGraphics.g.drawRect( 0, 0, this._kPanelWidth, this._kPanelHeight );
        this._go.cGraphics.g.endFill();

        // Title.
        let text: PIXI.Text = new PIXI.Text( jQuery.i18n( "INVESTIGATION_LIST" ), ServiceLocator.game.textStyler.title );
        text.anchor.x = 0.5;
        text.position.set( this._kPanelWidth * 0.5, 30 );
        let nextTextY: number = text.y + text.height;
        this._go.cContainer.c.addChild( text );
        // Suspects.
        // Header.
        text = new PIXI.Text( jQuery.i18n( "SUSPECTS" ) + ":", ServiceLocator.game.textStyler.subtitle );
        text.position.set( 30, nextTextY + 40 );
        nextTextY = text.y + text.height;
        this._go.cContainer.c.addChild( text );
        // List
        const kArrSuspect: Array<string> = [ "Todd the Troll", "Susie the Spider", "Wolf-pelt Jake", "Johnny Goblin-fingers", "Old Orc-eyes" ];
        for ( let suspect of kArrSuspect )
        {
            let checkBtn: GameObject = this.createCheckBtn();
            checkBtn.cContainer.c.position.set( 60, nextTextY + 20 );
            nextTextY = checkBtn.cContainer.c.y + checkBtn.cContainer.c.height;
            this._go.cContainer.addChild( checkBtn );
            let sb: SignalBinding = checkBtn.cButton.onClick.add( this.onListItemBtn_Click, this );
            sb.params = [ this._arrCheckBtn.length ];
            //
            this._arrCheckBtn.push( checkBtn );
            this._arrCheckBtnState.push( false );

            text = new PIXI.Text( suspect, ServiceLocator.game.textStyler.normal );
            text.position.set( checkBtn.cContainer.c.x + checkBtn.cContainer.c.width + 20, checkBtn.cContainer.c.y + checkBtn.cContainer.c.height * 0.5 )
            this._go.cContainer.c.addChild( text );
        }
        // Hideouts.
        // Header.
        text = new PIXI.Text( jQuery.i18n( "HIDEOUTS" ) + ":", ServiceLocator.game.textStyler.subtitle );
        text.position.set( 30, nextTextY + 40 );
        nextTextY = text.y + text.height;
        this._go.cContainer.c.addChild( text );
        // List
        const kArrHideout: Array<string> = [ "Bill Ferny's House", "Staddle Hiding Hole", "Combe Storehouse", "Archet Trading Post", "Chetwood Campsite" ];
        for ( let hideout of kArrHideout )
        {
            let checkBtn: GameObject = this.createCheckBtn();
            checkBtn.cContainer.c.position.set( 60, nextTextY + 20 );
            nextTextY = checkBtn.cContainer.c.y + checkBtn.cContainer.c.height;
            this._go.cContainer.addChild( checkBtn );
            let sb: SignalBinding = checkBtn.cButton.onClick.add( this.onListItemBtn_Click, this );
            sb.params = [ this._arrCheckBtn.length ];
            //
            this._arrCheckBtn.push( checkBtn );
            this._arrCheckBtnState.push( false );

            text = new PIXI.Text( hideout, ServiceLocator.game.textStyler.normal );
            text.position.set( checkBtn.cContainer.c.x + checkBtn.cContainer.c.width + 20, checkBtn.cContainer.c.y + checkBtn.cContainer.c.height * 0.5 )
            this._go.cContainer.c.addChild( text );
        }

        // Listen to events.
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.TOGGLE_INVESTIGATION_LIST_ITEM ] );
    }

        private createCheckBtn(): GameObject
        {
            let result: GameObject = new GameObject( [ new CSprite(), new CButton() ] );
            result.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "unchecked" );
            Utils.game.limitSideSize( 25, result.cSprite.s );
            result.init();

            return result;
        }

    public end(): void
    {
        // Cleanup events.
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.TOGGLE_INVESTIGATION_LIST_ITEM ] );

        for ( let checkBtn of this._arrCheckBtn )
        {
            checkBtn.end();
        }

        super.end();
    }

    // overrides.
    public saveGame(): ISgCustomPanel
    {
        let arrIndex: Array<number> = new Array<number>();
        for ( let i: number = 0; i < this._arrCheckBtnState.length; ++i )
        {
            if ( this._arrCheckBtnState[ i ] )
            {
                arrIndex.push( i );
            }
        }

        return { investigation_list: { checked_items: arrIndex } };
    }

    // overrides.
    public loadGame( sgCustomPanel: ISgCustomPanel, pass: number ): void
    {
        for ( let index of sgCustomPanel.investigation_list.checked_items )
        {
            this.toggleListItem( index );
        }
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.TOGGLE_INVESTIGATION_LIST_ITEM )
        {
            this.toggleListItem( action.args[ 0 ] as number );
        }
    }

        private toggleListItem( index: number ): void
        {
            this._arrCheckBtnState[ index ] = !this._arrCheckBtnState[ index ];

            let checkBtn: GameObject = this._arrCheckBtn[ index ];
            if ( this._arrCheckBtnState[ index ] )
            {
                checkBtn.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "checked" );
            }
            else
            {
                checkBtn.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "unchecked" );
            }
            Utils.game.limitSideSize( 25, checkBtn.cSprite.s );
        }

    // #endregion //


    // #region Input Callbacks //

    private onListItemBtn_Click( index: number ): void
    {
        this.toggleListItem( index );

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.TOGGLE_INVESTIGATION_LIST_ITEM, null, [ index ] );
    }

    // #endregion //
}