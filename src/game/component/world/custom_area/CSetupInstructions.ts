import CCustomArea from "../CCustomArea";

import { player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../../ServiceLocator";
import Utils from "../../../../Utils";
import * as PIXI from "pixi.js";

import { ISgCustomPanel } from "../../../../view/game/SaveGameView";
import GameObject from "../../../GameObject";
import { IOppActionListener, IOpponentAction } from "../../../AllyActionManager";
import CButton from "../../input/CButton";
import SignalBinding from "../../../../lib/signals/SignalBinding";
import CSprite from "../../pixi/CSprite";
import { ICard } from "../../../CardDB";
import Session from "../../../../Session";
import { IScenario } from "../../../ScenarioDB";
import Signal from "../../../../lib/signals/Signal";
import CContainer from "../../pixi/CContainer";


export default class CSetupInstructions extends CCustomArea implements IOppActionListener
{
    // #region Attributes //

    // private:

    private _arrCheckBtn: Array<GameObject> = null;
    private _arrCheckBtnState: Array<boolean> = null;

    // Signals.
    private _onCompleted: Signal = new Signal();

    // Constants.
    private readonly _kPanelWidth: number = 1000;

    // #endregion //


    public get onCompleted(): Signal { return this._onCompleted; }

    
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CSetupInstructions";
    }

    public init(): void
    {
        super.init();

        this._arrCheckBtn = new Array<GameObject>();
        this._arrCheckBtnState = new Array<boolean>();
        
        this._go.cContainer.c.interactive = true;

        // Title.
        let text: PIXI.Text = new PIXI.Text( jQuery.i18n( "BEFORE_YOU_START_THE_GAME" ), ServiceLocator.game.textStyler.title );
        text.anchor.x = 0.5;
        text.position.set( this._kPanelWidth * 0.5, 30 );
        let nextTextY: number = text.y + text.height - 10;
        this._go.cContainer.c.addChild( text );

        let textStyle: PIXI.TextStyle = ServiceLocator.game.textStyler.normal;
        textStyle.wordWrap = true;
        textStyle.wordWrapWidth = this._kPanelWidth - 60;
        text = new PIXI.Text( jQuery.i18n( "FOLLOW_SETUP_INSTRUCTIONS" ), textStyle );
        text.position.set( 30, nextTextY + 30 );
        nextTextY = text.y + text.height - 10;
        this._go.cContainer.c.addChild( text );

        let arrSetupInstructionGroup: Array<ISetupIntructionGroup> = new Array<ISetupIntructionGroup>();

        // Nightmare mode.
        const kScenarioInfo: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
        if ( kScenarioInfo.type == "nightmare" ) 
        {
            const kArrGameModifier: Array<GameObject> = ServiceLocator.game.cGameWorld.cSauronArea.cStaging.findGameModifiers();
            let nightmareText: string = null;
            for ( let gameModifier of kArrGameModifier )
            {
                if ( gameModifier.cGameModifier.front.cardInfo.type_code == "nightmare-setup" )
                {
                    nightmareText = gameModifier.cGameModifier.front.cardInfo.text;
                    break;
                }
            }
            const kSetupIndex: number = nightmareText.indexOf( "<b>Setup:</b>" );
            if ( kSetupIndex != -1 )
            {
                const kBIndex: number = nightmareText.indexOf( "<b>", kSetupIndex + 14 );
                let setupText: string =  nightmareText.substring( kSetupIndex + 14, kBIndex >= 0 ? kBIndex : undefined );
                setupText = setupText.substring( 0, setupText.lastIndexOf( "." ) + 1 );
                setupText = setupText.replace( "<b>", "" ).replace( "</b>", "" ).trim();
                arrSetupInstructionGroup.push( { title: jQuery.i18n( "NIGHTMARE_MODE" ), instructions: setupText } );
            }
        }
        // Campaign mode.
        if ( Session.isCampaignMode )
        {
            const kArrGameModifier: Array<GameObject> = ServiceLocator.game.cGameWorld.cSauronArea.cStaging.findGameModifiers();
            let campaignText: string = null;
            for ( let gameModifier of kArrGameModifier )
            {
                if ( gameModifier.cGameModifier.front.cardInfo.type_code == "campaign" )
                {
                    campaignText = gameModifier.cGameModifier.front.cardInfo.text;
                    break;
                }
            }
            const kSetupIndex: number = campaignText.indexOf( "<b>Setup:</b>" );
            if ( kSetupIndex != -1 )
            {
                const kBIndex: number = campaignText.indexOf( "<b>", kSetupIndex + 14 );
                let setupText: string = campaignText.substring( kSetupIndex + 14, kBIndex >= 0 ? kBIndex : undefined );
                setupText = setupText.substring( 0, setupText.lastIndexOf( "." ) + 1 );
                setupText = setupText.replace( "<b>", "" ).replace( "</b>", "" ).trim();
                arrSetupInstructionGroup.push( { title: jQuery.i18n( "CAMPAIGN_MODE" ), instructions: setupText } );
            }
        }
        // Gencon setup.
        const kArrGameModifier: Array<GameObject> = ServiceLocator.game.cGameWorld.cSauronArea.cStaging.findGameModifiers();
        let genconText: string = null;
        for ( let gameModifier of kArrGameModifier )
        {
            if ( gameModifier.cGameModifier.front.cardInfo.type_code == "gencon-setup" )
            {
                genconText = gameModifier.cGameModifier.front.cardInfo.text;
                break;
            }
        }
        if ( genconText )
        {
            const kSetupIndex: number = genconText.indexOf( "<b>Setup:</b>" );
            if ( kSetupIndex != -1 )
            {
                const kBIndex: number = genconText.indexOf( "<b>", kSetupIndex + 14 );
                let setupText: string = genconText.substring( kSetupIndex + 14, kBIndex >= 0 ? kBIndex : undefined );
                setupText = setupText.substring( 0, setupText.lastIndexOf( "." ) + 1 );
                setupText = setupText.replace( "<b>", "" ).replace( "</b>", "" ).trim();
                setupText = setupText.replace( /[\r|\n|•]/g, "" );
                arrSetupInstructionGroup.push( { title: jQuery.i18n( "GENCON_SETUP" ), instructions: setupText } );
            }
        }
        // Quest.
        const kQuestText: string = ServiceLocator.game.cGameWorld.cSauronArea.cQuestHolder.questToken.cCardToken.cCurSide.cardInfo.text;
        const kSetupIndex: number = kQuestText.indexOf( "<b>Setup:</b>" );
        if ( kSetupIndex != -1 )
        {
            const kSideBIndex: number = kQuestText.indexOf( "<b>Side B</b>" );
            let setupText: string = kQuestText.substring( kSetupIndex + 14, kSideBIndex >= 0 ? kSideBIndex : undefined );
            setupText = setupText.replace( "<b>", "" ).replace( "</b>", "" ).trim();
            arrSetupInstructionGroup.push( { title: jQuery.i18n( "QUEST" ), instructions: setupText } );
        }

        for ( let setupInstructionGroup of arrSetupInstructionGroup )
        {
            if ( setupInstructionGroup.instructions )
            {
                text = new PIXI.Text( setupInstructionGroup.title, ServiceLocator.game.textStyler.subtitle );
                text.position.set( 30, nextTextY + 30 );
                nextTextY = text.y + text.height - 10;
                this._go.cContainer.c.addChild( text );

                let arrPendingTask: Array<string> = Utils.split( setupInstructionGroup.instructions, ". " );
                for ( let i: number = arrPendingTask.length - 1; i >= 0; --i )
                {
                    arrPendingTask[ i ] = arrPendingTask[ i ].trim().replace( /~/g, "" );
                    if ( arrPendingTask[ i ].substr( arrPendingTask[ i ].length - 1, 1 ) != "." )
                    {
                        arrPendingTask[ i ] += ".";
                    }
                    if ( arrPendingTask[ i ].indexOf( "(" ) == 0 || arrPendingTask[ i ].indexOf( "Underhill" ) == 0 )
                    {
                        arrPendingTask[ i - 1 ] += " " + arrPendingTask[ i ];
                        arrPendingTask.splice( i, 1 );
                    }
                }

                for ( let pendingTask of arrPendingTask )
                {
                    let isChecked: boolean = false;
                    const kPendingTaskLowercase: string = pendingTask.toLowerCase();
                    
                    if ( kScenarioInfo.setup.length > 0 )
                    {
                        isChecked = kPendingTaskLowercase.indexOf( "attach 1 encounter to each objective card" ) == -1
                            && kPendingTaskLowercase.indexOf( "move each hero to" ) == -1
                            && kPendingTaskLowercase.indexOf( "remove each remaining objective-hero from" ) == -1
                            && kPendingTaskLowercase.indexOf( "shuffle all other faceup attachments" ) == -1
                            && kPendingTaskLowercase.indexOf( "advance to a random stage" ) == -1
                            && kPendingTaskLowercase.indexOf( "recorded in the campaign log" ) == -1
                            && kPendingTaskLowercase.indexOf( "place x resource tokens on this card" ) == -1;
                        if ( isChecked )
                        {
                            const kRegex: RegExp = /(players|each player(, except the first player,)?|each other player|the first player|the opposing team)( (may|must))? (chooses?|search(es)?|gives?|reveals?|adds?|attach(es)?|takes? control of|discards? card from|prepares?)/;
                            isChecked = !kRegex.test( kPendingTaskLowercase )
                                || /search(es)?.+for a copy of/.test( kPendingTaskLowercase )
                                || /the first player adds (massing|grimrede)/.test( kPendingTaskLowercase );
                        }
                        if ( isChecked )
                        {
                            const kRegex: RegExp = /reveal .+ card/;
                            isChecked = !kRegex.test( kPendingTaskLowercase );
                        }
                        if ( isChecked )
                        {
                            const kRegex: RegExp = /attach .+ to (a|the) hero/;
                            isChecked = !kRegex.test( kPendingTaskLowercase );
                        }
                        if ( isChecked )
                        {
                            const kRegex: RegExp = /remove .+ from the campaign pool/;
                            isChecked = !kRegex.test( kPendingTaskLowercase );
                        }
                    }

                    // Check button-container.
                    let checkBtn: GameObject = new GameObject( [ new CContainer(), new CButton() ] );
                    checkBtn.cContainer.c.position.set( 30, nextTextY + 15 );
                    checkBtn.init();
                    this._go.cContainer.addChild( checkBtn );
                    //
                    if ( !isChecked )
                    {
                        let sb: SignalBinding = checkBtn.cButton.onClick.add( this.onListItemBtn_Click, this );
                        sb.params = [ this._arrCheckBtn.length ];
                    }
                    else
                    {
                        checkBtn.cButton.setEnabled( false );
                    }
                    //
                    this._arrCheckBtn.push( checkBtn );
                    this._arrCheckBtnState.push( isChecked );
                    
                    // Checkbox.
                    let checkBox: PIXI.Sprite = this.createCheckBox( isChecked );
                    checkBtn.cContainer.c.addChild( checkBox );
                    // Text.
                    let textStyle: PIXI.TextStyle = ServiceLocator.game.textStyler.normal;
                    textStyle.wordWrap = true;
                    textStyle.wordWrapWidth = this._kPanelWidth - 30 - ( checkBtn.cContainer.c.x + checkBtn.cContainer.c.width + 20 );
                    text = new PIXI.Text( pendingTask, textStyle );
                    text.position.x = checkBtn.cContainer.c.width + 20;
                    checkBtn.cContainer.c.addChild( text );

                    const kCheckBtnNextY: number = checkBtn.cContainer.c.height;
                    const kTextNextY: number = text.height - 10;
                    nextTextY = checkBtn.cContainer.c.y + ( kCheckBtnNextY > kTextNextY ? kCheckBtnNextY : kTextNextY );
                }
            }
        }

        text = new PIXI.Text( "• • •", ServiceLocator.game.textStyler.subtitle );
        text.anchor.x = 0.5;
        text.position.set( this._kPanelWidth * 0.5, nextTextY + 30 );
        nextTextY = text.y + text.height - 10 + 30;
        this._go.cContainer.c.addChild( text );

        if ( Session.isCampaignMode )
        {
            // Check button-container.
            let checkBtn: GameObject = new GameObject( [ new CContainer(), new CButton() ] );
            checkBtn.cContainer.c.position.set( 30, nextTextY );
            checkBtn.init();
            this._go.cContainer.addChild( checkBtn );
            //
            let sb: SignalBinding = checkBtn.cButton.onClick.add( this.onListItemBtn_Click, this );
            sb.params = [ this._arrCheckBtn.length ];
            //
            this._arrCheckBtn.push( checkBtn );
            this._arrCheckBtnState.push( false );
            // // Checkbox.
            let checkBox: PIXI.Sprite = this.createCheckBox( false );
            checkBtn.cContainer.c.addChild( checkBox );
            // // Text.
            textStyle = ServiceLocator.game.textStyler.normal;
            textStyle.wordWrap = true;
            textStyle.wordWrapWidth = this._kPanelWidth - 30 - ( checkBtn.cContainer.c.width + 20 );
            text = new PIXI.Text( jQuery.i18n( "SETUP_BOON_AND_BURDEN_CARDS" ), textStyle );
            text.position.x = checkBtn.cContainer.c.width + 20;
            checkBtn.cContainer.c.addChild( text );

            const kCheckBtnNextY: number = checkBtn.cContainer.c.height;
            const kTextNextY: number = text.height - 10;
            nextTextY = checkBtn.cContainer.c.y + ( kCheckBtnNextY > kTextNextY ? kCheckBtnNextY : kTextNextY ) + 15;
        }

        if ( kScenarioInfo.treasure_sets )
        {
            // Check button-container.
            let checkBtn: GameObject = new GameObject( [ new CContainer(), new CButton() ] );
            checkBtn.cContainer.c.position.set( 30, nextTextY );
            checkBtn.init();
            this._go.cContainer.addChild( checkBtn );
            //
            let sb: SignalBinding = checkBtn.cButton.onClick.add( this.onListItemBtn_Click, this );
            sb.params = [ this._arrCheckBtn.length ];
            //
            this._arrCheckBtn.push( checkBtn );
            this._arrCheckBtnState.push( false );
            // // Checkbox.
            let checkBox: PIXI.Sprite = this.createCheckBox( false );
            checkBtn.cContainer.c.addChild( checkBox );
            // // Text.
            textStyle = ServiceLocator.game.textStyler.normal;
            textStyle.wordWrap = true;
            textStyle.wordWrapWidth = this._kPanelWidth - 30 - ( checkBtn.cContainer.c.width + 20 );
            text = new PIXI.Text( jQuery.i18n( "SETUP_TREASURE_CARDS" ), textStyle );
            text.position.x = checkBtn.cContainer.c.width + 20;
            checkBtn.cContainer.c.addChild( text );

            const kCheckBtnNextY: number = checkBtn.cContainer.c.height;
            const kTextNextY: number = text.height - 10;
            nextTextY = checkBtn.cContainer.c.y + ( kCheckBtnNextY > kTextNextY ? kCheckBtnNextY : kTextNextY ) + 15;
        }

        // Flip quest card.
        // Check button-container.
        let checkBtn: GameObject = new GameObject( [ new CContainer(), new CButton() ] );
        checkBtn.cContainer.c.position.set( 30, nextTextY );
        checkBtn.init();
        this._go.cContainer.addChild( checkBtn );
        //
        let sb: SignalBinding = checkBtn.cButton.onClick.add( this.onListItemBtn_Click, this );
        sb.params = [ this._arrCheckBtn.length ];
        //
        this._arrCheckBtn.push( checkBtn );
        this._arrCheckBtnState.push( false );
        // // Checkbox.
        let checkBox: PIXI.Sprite = this.createCheckBox( false );
        checkBtn.cContainer.c.addChild( checkBox );
        // // Text.
        textStyle = ServiceLocator.game.textStyler.normal;
        textStyle.wordWrap = true;
        textStyle.wordWrapWidth = this._kPanelWidth - 30 - ( checkBtn.cContainer.c.width + 20 );
        text = new PIXI.Text( jQuery.i18n( "FLIP_QUEST_CARD" ), textStyle );
        text.position.x = checkBtn.cContainer.c.width + 20;
        checkBtn.cContainer.c.addChild( text );

        // Bg.
        this._go.cGraphics.g.lineStyle( 2, 0x000000, 1 );
        this._go.cGraphics.g.beginFill( 0xefe3af, 0.7 );
        this._go.cGraphics.g.drawRect( 0, 0, this._kPanelWidth, this._go.cContainer.c.height + 60 );
        this._go.cGraphics.g.endFill();

        // Listen to events.
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.TOGGLE_SETUP_LIST_ITEM ] );
    }

        private createCheckBox( isChecked: boolean ): PIXI.Sprite
        {
            let result: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( isChecked ? "checked" : "unchecked" ) );
            result.name = "checkbox";
            Utils.game.limitSideSize( 25, result );

            return result;
        }

    public end(): void
    {
        this._onCompleted.removeAll();

        // Cleanup events.
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.TOGGLE_SETUP_LIST_ITEM ] );

        for ( let checkBtn of this._arrCheckBtn )
        {
            checkBtn.end();
        }

        super.end();
    }

    // overrides.
    public saveGame(): ISgCustomPanel
    {
        return null;
    }

    // overrides.
    public loadGame( sgCustomPanel: ISgCustomPanel, pass: number ): void {}

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.TOGGLE_SETUP_LIST_ITEM )
        {
            this.checkListItem( action.args[ 0 ] as number );
        }
    }

        private checkListItem( index: number ): void
        {
            this._arrCheckBtnState[ index ] = true;

            let checkBtn: GameObject = this._arrCheckBtn[ index ];
            checkBtn.cButton.setEnabled( false );

            let checkBox: PIXI.Sprite = checkBtn.cContainer.c.getChildByName( "checkbox" ) as PIXI.Sprite;
            checkBox.texture = ServiceLocator.resourceStack.findAsTexture( "checked" );
            Utils.game.limitSideSize( 25, checkBox );

            let isCompleted: boolean = true;
            for ( let checkBtnState of this._arrCheckBtnState )
            {
                if ( !checkBtnState )
                {
                    isCompleted = false;
                    break;
                }
            }

            if ( isCompleted )
            {
                this._onCompleted.dispatch();
            }
        }

    // #endregion //


    // #region Input Callbacks //

    private onListItemBtn_Click( index: number ): void
    {
        this.checkListItem( index );

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.TOGGLE_SETUP_LIST_ITEM, null, [ index ] );
    }

    // #endregion //
}

interface ISetupIntructionGroup
{
    title: string;
    instructions: string;
}