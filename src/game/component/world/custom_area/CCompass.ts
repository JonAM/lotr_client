import CCustomArea from "../CCustomArea";

import { player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../../ServiceLocator";
import Utils from "../../../../Utils";
import * as PIXI from "pixi.js";

import { ISgCompass, ISgCustomPanel } from "../../../../view/game/SaveGameView";
import GameObject from "../../../GameObject";
import CContainer from "../../pixi/CContainer";
import CButton from "../../input/CButton";
import { IOppActionListener, IOpponentAction } from "../../../AllyActionManager";
import { layer_type } from "../CGameLayerProvider";
import { view_layer_id } from "../../../../service/ViewManager";
import Component from "../../Component";


export default class CCompass extends Component implements IOppActionListener
{
    // #region Attributes //

    // private:

    private _sprite: PIXI.Sprite = null;
    private _rotateLeftBtn: GameObject = null;
    private _rotateRightBtn: GameObject = null;

    // #endregion //

    
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CCompass";
    }

    public init(): void
    {
        console.assert( this._go.cContainer != null, "CCompass.ts :: init() :: CContainer component not found." );

        super.init();
        
        this._go.cContainer.c.interactive = true;

        // Heading.
        this._sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( "heading" ) );
        this._sprite.anchor.set( 0.5 );
        this._go.cContainer.c.addChild( this._sprite );

        // Rotate left button.
        this._rotateLeftBtn = new GameObject( [ new CContainer(), new CButton() ] );
        this._rotateLeftBtn.cContainer.c.alpha = 0.3;
        this._rotateLeftBtn.init();
        this._rotateLeftBtn.cButton.onClick.add( this.onRotateLeftBtn_Click, this );
        //
        let bg: PIXI.Graphics = new PIXI.Graphics();
        bg.lineStyle( 1, 0x000000, 1 );
        bg.beginFill( 0xeccc68, 1 );
        bg.drawCircle( 0, 0, 60 );
        bg.endFill();
        this._rotateLeftBtn.cContainer.c.addChild( bg );
        //
        let icon: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( "rad_rotate" ) );
        Utils.game.limitSideSize( 100, icon );
        icon.scale.x *= -1;
        icon.anchor.set( 0.5 );
        this._rotateLeftBtn.cContainer.c.addChild( icon );
        //
        this._rotateLeftBtn.cContainer.c.x = -this._sprite.width * 0.25;
        this._go.cContainer.addChild( this._rotateLeftBtn );

        // Rotate right button.
        this._rotateRightBtn = new GameObject( [ new CContainer(), new CButton() ] );
        this._rotateRightBtn.cContainer.c.alpha = 0.3;
        this._rotateRightBtn.init();
        this._rotateRightBtn.cButton.onClick.add( this.onRotateRightBtn_Click, this );
        //
        bg = new PIXI.Graphics();
        bg.lineStyle( 1, 0x000000, 1 );
        bg.beginFill( 0xeccc68, 1 );
        bg.drawCircle( 0, 0, 60 );
        bg.endFill();
        this._rotateRightBtn.cContainer.c.addChild( bg );
        //
        icon = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( "rad_rotate" ) );
        Utils.game.limitSideSize( 100, icon );
        icon.anchor.set( 0.5 );
        this._rotateRightBtn.cContainer.c.addChild( icon );
        //
        this._rotateRightBtn.cContainer.c.x = this._sprite.width * 0.25;
        this._go.cContainer.addChild( this._rotateRightBtn );

        // Listen to events.
        this._go.cContainer.c.on( "pointermove", this.onPointerMove, this );
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.ROTATE_HEADING ] );
    }

    public end(): void
    {
        // Cleanup events.
        this._go.cContainer.c.off( "pointermove", this.onPointerMove, this );
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.ROTATE_HEADING ] );

        this._rotateLeftBtn.end();
        this._rotateLeftBtn = null;

        this._rotateRightBtn.end();
        this._rotateRightBtn = null;

        super.end();
    }

    // overrides.
    public saveGame(): ISgCompass
    {
        return { rotation: this._sprite.rotation };
    }

    // overrides.
    public loadGame( sgCompass: ISgCompass, pass: number ): void
    {
        this._sprite.rotation = sgCompass.rotation;
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.ROTATE_HEADING )
        {
            if ( ( action.args[ 0 ] as string ) == "left" )
            {
                this.onRotateLeftBtn_Click();
            }
            else
            {
                this.onRotateRightBtn_Click();
            }
        }
    }

    // #endregion //


    // #region Input Callbacks //

    private onPointerMove( event: PIXI.InteractionEvent ): void
    {
        if ( ServiceLocator.game.dragShadowManager.isDragging
            || ServiceLocator.viewManager.findViewCount( view_layer_id.POPUP ) > 0
            || ServiceLocator.viewManager.findViewCount( view_layer_id.TOPMOST ) > 0
            || ServiceLocator.game.root.cGameLayerProvider.get( layer_type.CARD_PRESENTATION ).cContainer.c.children.length > 0
            || ServiceLocator.game.root.cGameLayerProvider.get( layer_type.DISABLED ).cContainer.c.children.length > 0 ) { return; }

        if ( this._sprite.getBounds().contains( event.data.global.x, event.data.global.y ) )
        {
            this._rotateLeftBtn.cContainer.c.visible = true;
            this._rotateRightBtn.cContainer.c.visible = true;
        }
        else
        {
            this._rotateLeftBtn.cContainer.c.visible = false;
            this._rotateRightBtn.cContainer.c.visible = false;
        }
    }

    private onRotateLeftBtn_Click(): void
    {
        this._sprite.rotation += Math.PI * 0.5;

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ROTATE_HEADING, null, [ "left" ] );
    }

    private onRotateRightBtn_Click(): void
    {
        this._sprite.rotation += Math.PI * 0.5;

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ROTATE_HEADING, null, [ "right" ] );
    }

    // #endregion //
}