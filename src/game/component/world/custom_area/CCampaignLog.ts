import CCustomArea from "../CCustomArea";

import { player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../../ServiceLocator";
import Utils from "../../../../Utils";
import * as PIXI from "pixi.js";

import { ISgCustomPanel } from "../../../../view/game/SaveGameView";
import CampaignLogView from "../../../../view/common/CampaignLogView";
import { view_layer_id } from "../../../../service/ViewManager";
import CampaignLogVO from "../../../../vo/CampaignLogVO";
import { IOppActionListener, IOpponentAction } from "../../../AllyActionManager";
import EditableCampaignLog from "../../../../view/common/campaign_log/EditableCampaignLog";
import { campaing_log_row_type } from "../../../../view/common/campaign_log/CampaignLogController";


export default class CCampaignLog extends CCustomArea implements IOppActionListener
{
    private _campaignLogVO: CampaignLogVO = null;

    private _campaignLogView: CampaignLogView = null;

    // Constants.
    private readonly _kArrVoAttributeId: Array<string> = [ 
        "_arrPlayerHeroId", "_arrAllyHeroId", "_arrFallenHeroId",
        "_arrCompletedScenario", "_arrBoon", "_arrBurden" ];


    public get campaignLogVO(): CampaignLogVO { return this._campaignLogVO; }

    public set campaignLogVO( value: CampaignLogVO ) { this._campaignLogVO = value; }


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CCampaignLog";
    }

    public init(): void
    {
        super.init();

        // Listen to events.
        ServiceLocator.game.allyActionManager.addListener( this, [ 
            player_action_type.CAMPAIGN_LOG_ADD_ROW,
            player_action_type.CAMPAIGN_LOG_REMOVE_ROW,
            player_action_type.CAMPAIGN_LOG_SET_NOTES,
            player_action_type.CAMPAIGN_LOG_SET_THREAT_PENALTY ] );
    }

    public end(): void
    {
        // Listen to events.
        ServiceLocator.game.allyActionManager.removeListener( this, [ 
            player_action_type.CAMPAIGN_LOG_ADD_ROW,
            player_action_type.CAMPAIGN_LOG_REMOVE_ROW,
            player_action_type.CAMPAIGN_LOG_SET_NOTES,
            player_action_type.CAMPAIGN_LOG_SET_THREAT_PENALTY ] );

        super.end();
    }

    // overrides.
    public onShown(): void
    {
        this._campaignLogView = new CampaignLogView();
        this._campaignLogView.isEditable = true;
        this._campaignLogView.campaignLogVO = this._campaignLogVO;
        this._campaignLogView.onClosed.add( this.onCampaignLogView_Closed, this );
        this._campaignLogView.init();
        ServiceLocator.viewManager.fadeIn( this._campaignLogView, view_layer_id.POPUP );
    }

        private onCampaignLogView_Closed(): void
        {
            this._campaignLogView = null;

            ServiceLocator.game.cGameWorld.cCustomPanelManager.hide( "campaign_log" );
        }

    // overrides.
    public onHidden(): void
    {
        if ( this._campaignLogView )
        {
            ServiceLocator.viewManager.fadeOut( this._campaignLogView );

            this._campaignLogView = null;
        }
    }

    // overrides.
    public saveGame(): ISgCustomPanel
    {
        return { campaign_log: this._campaignLogVO.serialize() };
    }

    // overrides.
    public loadGame( sgCustomPanel: ISgCustomPanel, pass: number ): void 
    {
        if ( sgCustomPanel.campaign_log )
        {
            this._campaignLogVO = new CampaignLogVO();
            this._campaignLogVO.parse( sgCustomPanel.campaign_log );
        }
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        switch ( action.playerActionType )
        {
            case player_action_type.CAMPAIGN_LOG_ADD_ROW:
                {
                    const kArrValue: Array<string> = action.args[ 0 ] as Array<string>;
                    const kRowType: campaing_log_row_type = action[ 1 ] as campaing_log_row_type;
                    if ( kRowType <= campaing_log_row_type.FALLEN_HEROES )
                    {
                        this._campaignLogVO[ this._kArrVoAttributeId[ kRowType ] ].push( kArrValue[ 0 ] );
                    }
                    else
                    {
                        this._campaignLogVO[ this._kArrVoAttributeId[ kRowType ] ].push( { id: kArrValue[ 0 ], permanentHeroId: kArrValue[ 1 ] } );
                    }

                    if ( this._campaignLogView )
                    {
                        ( this._campaignLogView.controller as EditableCampaignLog ).addRow( kArrValue, kRowType );
                    }

                    break;
                }

            case player_action_type.CAMPAIGN_LOG_REMOVE_ROW:
                {
                    const kRowIndex: number = action.args[ 0 ] as number;
                    const kRowType: campaing_log_row_type = action[ 1 ] as campaing_log_row_type;
                    this._campaignLogVO[ this._kArrVoAttributeId[ kRowType ] ].splice( kRowIndex, 1 );

                    if ( this._campaignLogView )
                    {
                        ( this._campaignLogView.controller as EditableCampaignLog ).removeRow( kRowIndex, kRowType );
                    }

                    break;
                }

            case player_action_type.CAMPAIGN_LOG_SET_NOTES:
                {
                    this._campaignLogVO.notes = action.args[ 0 ] as string;

                    if ( this._campaignLogView )
                    {
                        ( this._campaignLogView.controller as EditableCampaignLog ).setNotes( this._campaignLogVO.notes );
                    }

                    break;
                }

            case player_action_type.CAMPAIGN_LOG_SET_THREAT_PENALTY:
                {
                    this._campaignLogVO.threatPenalty = action.args[ 0 ] as number;

                    if ( this._campaignLogView )
                    {
                        ( this._campaignLogView.controller as EditableCampaignLog ).setThreatPenalty(  this._campaignLogVO.threatPenalty );
                    }

                    break;
                }
        }
    }

    // #endregion //
}