import CCustomArea from "../CCustomArea";

import { player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../../ServiceLocator";
import Utils from "../../../../Utils";
import * as PIXI from "pixi.js";

import { ISgCustomPanel } from "../../../../view/game/SaveGameView";
import GameObject from "../../../GameObject";
import { IOppActionListener, IOpponentAction } from "../../../AllyActionManager";
import CButton from "../../input/CButton";
import SignalBinding from "../../../../lib/signals/SignalBinding";
import CSprite from "../../pixi/CSprite";
import { ICard } from "../../../CardDB";
import Session from "../../../../Session";
import { IScenario } from "../../../ScenarioDB";


export default class CSetupInstructions extends CCustomArea implements IOppActionListener
{
    // #region Attributes //

    // private:

    private _arrCheckBtn: Array<GameObject> = null;
    private _arrCheckBtnState: Array<boolean> = null;

    // Constants.
    private readonly _kPanelWidth: number = 1000;

    // #endregion //

    
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CSetupInstructions";
    }

    public init(): void
    {
        super.init();

        this._arrCheckBtn = new Array<GameObject>();
        this._arrCheckBtnState = new Array<boolean>();
        
        this._go.cContainer.c.interactive = true;

        // Title.
        let text: PIXI.Text = new PIXI.Text( jQuery.i18n( "SETUP_INSTRUCTIONS" ), ServiceLocator.game.textStyler.title );
        text.anchor.x = 0.5;
        text.position.set( this._kPanelWidth * 0.5, 30 );
        let nextTextY: number = text.y + text.height - 10;
        this._go.cContainer.c.addChild( text );

        // Nightmare mode.
        const kScenarioInfo: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
        if ( kScenarioInfo.type == "nightmare" ) 
        {
            const kArrGameModifier: Array<GameObject> = ServiceLocator.game.cGameWorld.cSauronArea.cStaging.findGameModifiers();
            let nightmareText: string = null;
            for ( let gameModifier of kArrGameModifier )
            {
                if ( gameModifier.cGameModifier.front.cardInfo.type_code == "nightmare-setup" )
                {
                    nightmareText = gameModifier.cGameModifier.front.cardInfo.text;
                    break;
                }
            }
            const kSetupIndex: number = nightmareText.indexOf( "<b>Setup:</b>" );
            if ( kSetupIndex != -1 )
            {
                text = new PIXI.Text( jQuery.i18n( "NIGHTMARE_MODE"), ServiceLocator.game.textStyler.subtitle );
                text.position.set( 30, nextTextY + 30 );
                nextTextY = text.y + text.height - 10;
                this._go.cContainer.c.addChild( text );

                const kBIndex: number = nightmareText.indexOf( "<b>", kSetupIndex + 14 );
                let setupText: string =  nightmareText.substring( kSetupIndex + 14, kBIndex >= 0 ? kBIndex : undefined );
                setupText = setupText.substring( 0, setupText.lastIndexOf( "." ) + 1 );
                setupText = setupText.replace( "<b>", "" ).replace( "</b>", "" );
                let textStyle: PIXI.TextStyle = ServiceLocator.game.textStyler.normal;
                textStyle.wordWrap = true;
                textStyle.wordWrapWidth = this._kPanelWidth - 60;
                text = new PIXI.Text( setupText, textStyle );
                text.position.set( 30, nextTextY + 15 )
                nextTextY = text.y + text.height - 10;
                this._go.cContainer.c.addChild( text );
            }
        }
        // Campaign mode.
        if ( Session.isCampaignMode )
        {
            const kArrGameModifier: Array<GameObject> = ServiceLocator.game.cGameWorld.cSauronArea.cStaging.findGameModifiers();
            let campaignText: string = null;
            for ( let gameModifier of kArrGameModifier )
            {
                if ( gameModifier.cGameModifier.front.cardInfo.type_code == "campaign" )
                {
                    campaignText = gameModifier.cGameModifier.front.cardInfo.text;
                    break;
                }
            }
            const kSetupIndex: number = campaignText.indexOf( "<b>Setup:</b>" );
            if ( kSetupIndex != -1 )
            {
                text = new PIXI.Text( jQuery.i18n( "CAMPAIGN_MODE"), ServiceLocator.game.textStyler.subtitle );
                text.position.set( 30, nextTextY + 30 );
                nextTextY = text.y + text.height - 10;
                this._go.cContainer.c.addChild( text );

                const kBIndex: number = campaignText.indexOf( "<b>", kSetupIndex + 14 );
                let setupText: string = campaignText.substring( kSetupIndex + 14, kBIndex >= 0 ? kBIndex : undefined );
                setupText = setupText.substring( 0, setupText.lastIndexOf( "." ) + 1 );
                setupText = setupText.replace( "<b>", "" ).replace( "</b>", "" );
                let textStyle: PIXI.TextStyle = ServiceLocator.game.textStyler.normal;
                textStyle.wordWrap = true;
                textStyle.wordWrapWidth = this._kPanelWidth - 60;
                text = new PIXI.Text( setupText, textStyle );
                text.position.set( 30, nextTextY + 15 )
                nextTextY = text.y + text.height - 10;
                this._go.cContainer.c.addChild( text );
            }
        }
        // Gencon setup.
        const kArrGameModifier: Array<GameObject> = ServiceLocator.game.cGameWorld.cSauronArea.cStaging.findGameModifiers();
        let genconText: string = null;
        for ( let gameModifier of kArrGameModifier )
        {
            if ( gameModifier.cGameModifier.front.cardInfo.type_code == "gencon-setup" )
            {
                genconText = gameModifier.cGameModifier.front.cardInfo.text;
                break;
            }
        }
        if ( genconText )
        {
            const kSetupIndex: number = genconText.indexOf( "<b>Setup:</b>" );
            if ( kSetupIndex != -1 )
            {
                text = new PIXI.Text( jQuery.i18n( "GENCON_SETUP" ), ServiceLocator.game.textStyler.subtitle );
                text.position.set( 30, nextTextY + 30 );
                nextTextY = text.y + text.height - 10;
                this._go.cContainer.c.addChild( text );

                const kBIndex: number = genconText.indexOf( "<b>", kSetupIndex + 14 );
                let setupText: string = genconText.substring( kSetupIndex + 14, kBIndex >= 0 ? kBIndex : undefined );
                setupText = setupText.substring( 0, setupText.lastIndexOf( "." ) + 1 );
                setupText = setupText.replace( "<b>", "" ).replace( "</b>", "" );
                let textStyle: PIXI.TextStyle = ServiceLocator.game.textStyler.normal;
                textStyle.wordWrap = true;
                textStyle.wordWrapWidth = this._kPanelWidth - 60;
                text = new PIXI.Text( setupText, textStyle );
                text.position.set( 30, nextTextY + 15 )
                nextTextY = text.y + text.height - 10;
                this._go.cContainer.c.addChild( text );
            }
        }
        // Quest.
        const kQuestText: string = ServiceLocator.game.cGameWorld.cSauronArea.cQuestHolder.questToken.cCardToken.cCurSide.cardInfo.text;
        const kSetupIndex: number = kQuestText.indexOf( "<b>Setup:</b>" );
        if ( kSetupIndex != -1 )
        {
            text = new PIXI.Text( jQuery.i18n( "QUEST"), ServiceLocator.game.textStyler.subtitle );
            text.position.set( 30, nextTextY + 30 );
            nextTextY = text.y + text.height - 10;
            this._go.cContainer.c.addChild( text );

            const kSideBIndex: number = kQuestText.indexOf( "<b>Side B</b>" );
            let textStyle: PIXI.TextStyle = ServiceLocator.game.textStyler.normal;
            textStyle.wordWrap = true;
            textStyle.wordWrapWidth = this._kPanelWidth - 60;
            let setupText: string = kQuestText.substring( kSetupIndex + 14, kSideBIndex >= 0 ? kSideBIndex : undefined );
            setupText = setupText.replace( "<b>", "" ).replace( "</b>", "" );
            text = new PIXI.Text( setupText, textStyle );
            text.position.set( 30, nextTextY + 15 )
            nextTextY = text.y + text.height - 10;
            this._go.cContainer.c.addChild( text );
        }

        // Before you start.
        // Title.
        text = new PIXI.Text( jQuery.i18n( "BEFORE_YOU_START_THE_GAME" ), ServiceLocator.game.textStyler.title );
        text.anchor.x = 0.5;
        text.position.set( this._kPanelWidth * 0.5, nextTextY + 30 );
        nextTextY = text.y + text.height - 10 + 15;
        this._go.cContainer.c.addChild( text );
        // Checklist.
        let arrPendingTask: Array<string> = new Array<string>();
        if ( !kScenarioInfo.setup || kScenarioInfo.setup.length == 0 )
        {
            arrPendingTask.push( jQuery.i18n( "FOLLOW_SETUP_INSTRUCTIONS" ) );
        }
        else
        {
            arrPendingTask = arrPendingTask.concat( kScenarioInfo.setup_pending_tasks );
        }
        arrPendingTask.push( jQuery.i18n( "FLIP_QUEST_CARD" ) );
        for ( let pendingTask of arrPendingTask )
        {
            // Checkbox.
            let checkBtn: GameObject = this.createCheckBtn();
            checkBtn.cContainer.c.position.set( 30, nextTextY + 15 );
            this._go.cContainer.addChild( checkBtn );
            let sb: SignalBinding = checkBtn.cButton.onClick.add( this.onListItemBtn_Click, this );
            sb.params = [ this._arrCheckBtn.length ];
            //
            this._arrCheckBtn.push( checkBtn );
            this._arrCheckBtnState.push( false );
            // Text.
            let textStyle: PIXI.TextStyle = ServiceLocator.game.textStyler.normal;
            textStyle.wordWrap = true;
            textStyle.wordWrapWidth = this._kPanelWidth - 30 - ( checkBtn.cContainer.c.x + checkBtn.cContainer.c.width + 20 );
            text = new PIXI.Text( pendingTask, textStyle );
            text.anchor.y = 0.5;
            text.position.set( 
                checkBtn.cContainer.c.x + checkBtn.cContainer.c.width + 20, 
                checkBtn.cContainer.c.y + checkBtn.cContainer.c.height * 0.5 );
            this._go.cContainer.c.addChild( text );

            const kCheckBtnNextY: number = checkBtn.cContainer.c.y + checkBtn.cContainer.c.height;
            const kTextNextY: number = text.y + text.height - 10;
            nextTextY = ( kCheckBtnNextY > kTextNextY ? kCheckBtnNextY : kTextNextY );
        }

        // Bg.
        this._go.cGraphics.g.lineStyle( 2, 0x000000, 1 );
        this._go.cGraphics.g.beginFill( 0xefe3af, 0.7 );
        this._go.cGraphics.g.drawRect( 0, 0, this._kPanelWidth, this._go.cContainer.c.height + 60 );
        this._go.cGraphics.g.endFill();

        // Listen to events.
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.TOGGLE_SETUP_LIST_ITEM ] );
    }

        private createCheckBtn(): GameObject
        {
            let result: GameObject = new GameObject( [ new CSprite(), new CButton() ] );
            result.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "unchecked" );
            Utils.game.limitSideSize( 25, result.cSprite.s );
            result.init();

            return result;
        }

    public end(): void
    {
        // Cleanup events.
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.TOGGLE_SETUP_LIST_ITEM ] );

        for ( let checkBtn of this._arrCheckBtn )
        {
            checkBtn.end();
        }

        super.end();
    }

    // overrides.
    public saveGame(): ISgCustomPanel
    {
        return null;
    }

    // overrides.
    public loadGame( sgCustomPanel: ISgCustomPanel, pass: number ): void {}

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.TOGGLE_SETUP_LIST_ITEM )
        {
            this.toggleListItem( action.args[ 0 ] as number );
        }
    }

        private toggleListItem( index: number ): void
        {
            this._arrCheckBtnState[ index ] = !this._arrCheckBtnState[ index ];

            let checkBtn: GameObject = this._arrCheckBtn[ index ];
            if ( this._arrCheckBtnState[ index ] )
            {
                checkBtn.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "checked" );
            }
            else
            {
                checkBtn.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "unchecked" );
            }
            Utils.game.limitSideSize( 25, checkBtn.cSprite.s );
        }

    // #endregion //


    // #region Input Callbacks //

    private onListItemBtn_Click( index: number ): void
    {
        this.toggleListItem( index );

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.TOGGLE_SETUP_LIST_ITEM, null, [ index ] );
    }

    // #endregion //
}