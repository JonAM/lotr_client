import CCustomArea from "../CCustomArea";

import { location_type } from "../CGameWorld";
import ServiceLocator from "../../../../ServiceLocator";
import Utils from "../../../../Utils";
import * as PIXI from "pixi.js";

import { ISgCardToken, ISgCustomPanel, ISgIslandMap } from "../../../../view/game/SaveGameView";
import GameObject from "../../../GameObject";
import CDropArea from "../../input/CDropArea";
import CContainer from "../../pixi/CContainer";
import CIslandLocationHolder from "../../ui/CIslandLocationHolder";
import CLogLocation from "../CLogLocation";
import CardTokenFactory from "../../../CardTokenFactory";
import { action_scope_type } from "../../../../service/socket_io/GameSocketIOController";
import { detail_bar_icon_type } from "../../card/token/CDetailBar";
import CPlayerToken from "../CPlayerToken";
import CDraggable from "../../input/CDraggable";
import CHighlightPoiReceptor from "../poi_receptor/CHighlightPoiReceptor";
import CTooltipReceptor from "../../ui/CTooltipReceptor";
import { player_type } from "../CPlayerArea";
import Session from "../../../../Session";
import CShareableGameElement from "../../CShareableGameElement";


export default class CIslandMap extends CCustomArea
{
    // #region Attributes //

    // private:

    private _threatLayer: PIXI.Container = null;
    private _playerToken: GameObject = null;

    private _arrLocationHolder: Array<GameObject> = null;
    private _arrActiveLocationHolder: Array<GameObject> = null;
    private _playerLocationIndex: number = null;
    private _stagingThreat: number = null;
    private _isNightmare: boolean = null;

    // Constants.
    private readonly _kPanelHeight: number = 800;

    // #endregion //


    // #region Properties //

    public get stagingThreat(): number { return this._stagingThreat; }

    // #endregion //

    
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CIslandMap";
    }

    public init(): void
    {
        super.init();

        this._stagingThreat = 0;
        this._isNightmare = ServiceLocator.scenarioDb.findScenario( Session.scenarioId ).type == "nightmare";
        
        this._go.cContainer.c.interactive = true;

        this._go.cGraphics.g.lineStyle( 2, 0x000000, 1 );
        this._go.cGraphics.g.beginFill( 0xefe3af, 0.7 );
        const kPanelWidth: number = this._isNightmare ? 1548 : 1300;
        this._go.cGraphics.g.drawRect( 0, 0, kPanelWidth, this._kPanelHeight );
        this._go.cGraphics.g.endFill();

        this._threatLayer = new PIXI.Container();
        this._go.cContainer.c.addChild( this._threatLayer );

        this._arrLocationHolder = new Array<GameObject>();
        const kRowSize: number = this._isNightmare ? 6 : 5;
        const kLocationHolderWidth: number = ( kPanelWidth - 60 ) / kRowSize;
        const kLocationHolderHeight: number = ( this._kPanelHeight - 60 ) / 3;
        let locationHolderPos: PIXI.Point = new PIXI.Point( 30 + kLocationHolderWidth * 0.5, 30 + kLocationHolderHeight * 0.5 );
        const kLocationHolderCount: number = this._isNightmare ? 18 : 15;
        for ( let i: number = 0; i < kLocationHolderCount; ++i )
        {
            let locationHolder: GameObject = new GameObject( [ new CContainer(), new CIslandLocationHolder(), new CDropArea(), new CLogLocation() ] );
            locationHolder.oid = "island_location_holder_" + i.toString();
            locationHolder.init();
            locationHolder.cContainer.c.position.copyFrom( locationHolderPos );
            this._go.cContainer.addChild( locationHolder );
            locationHolder.cIslandLocationHolder.onPlayerTokenDropped.add( this.onPlayerToken_Dropped, this );

            this._arrLocationHolder.push( locationHolder );

            if ( ( i + 1 ) % kRowSize == 0 )
            {
                locationHolderPos.x = 30 + kLocationHolderWidth * 0.5;
                locationHolderPos.y += kLocationHolderHeight;
            }
            else
            {
                locationHolderPos.x += kLocationHolderWidth;
            }
        }

         // Player token.
         this._playerToken = new GameObject( [ new CContainer(), new CPlayerToken(), new CShareableGameElement(), new CDraggable(), new CHighlightPoiReceptor(), new CTooltipReceptor() ] );
         this._playerToken.oid = "player_token";
         this._playerToken.cDraggable.dragShadowTextureCreator = this._playerToken.cPlayerToken;
         this._playerToken.cTooltipReceptor.text = jQuery.i18n( "PLAYER" );
         this._playerToken.init();
    }

    public end(): void
    {
        for ( let locationHolder of this._arrLocationHolder )
        {
            locationHolder.end();
        }
        this._arrLocationHolder = null;

        this._playerToken.end();
        this._playerToken = null;
    }

    public generate(): void
    {
        let arrCard: Array<GameObject> = ServiceLocator.game.cGameWorld.findLocation( location_type.SET_ASIDE ).cDeckIndicator.cViewer.cCardView.findItems();
        let arrJungleCard: Array<GameObject> = [];
        let arrTempleCard: Array<GameObject> = [];
        let arrTempleEdgeCard: Array<GameObject> = [];
        for ( let card of arrCard )
        {
            card.cCard.isAnimated = false;
            card.cCard.setFaceDown( action_scope_type.LOCAL );
            card.cCard.isAnimated = true;

            if ( card.cCard.front.cardInfo.type_code == "location" )
            {
                if ( card.cCard.front.cardInfo.code == "012068A"
                    || card.cCard.front.cardInfo.code == "012069A" ) 
                {
                    arrTempleCard.push( card );
                }
                else if ( this._isNightmare 
                    && ( card.cCard.front.cardInfo.code == "764002A"
                        || card.cCard.front.cardInfo.code == "764003A"
                        || card.cCard.front.cardInfo.code == "764004A" ) )
                {
                    arrTempleEdgeCard.push( card );
                }
                else
                {
                    arrJungleCard.push( card );
                }
            }
        }

        Utils.game.shuffle( arrTempleCard );
        Utils.game.shuffle( arrJungleCard );
        Utils.game.shuffle( arrTempleEdgeCard );

        let ctf: CardTokenFactory = new CardTokenFactory();
        let locationIndex: number = 0;
        const kRowSize: number = this._isNightmare ? 6 : 5;
        for ( let locationHolder of this._arrLocationHolder )
        {
            let locationCard: GameObject = null;
            let locationToken: GameObject = null;
            if ( ( locationIndex + 1 ) % kRowSize == 0 )
            {
                locationCard = arrTempleCard.pop();
                locationToken = ctf.create( locationCard );
                if ( locationToken.cCardToken.cBackSide.cardId == "012068B" )
                {
                    locationToken.cCardToken.cDetailBar.removeItem( detail_bar_icon_type.UNIQUE );
                }
            }
            else if ( this._isNightmare && Utils.arrayHasItem( [ 4, 5, 10, 11, 16, 17 ], locationIndex + 1 ) )
            {
                locationCard = arrTempleEdgeCard.pop();
                locationToken = ctf.create( locationCard );
            }
            else
            {
                locationCard = arrJungleCard.pop();
                locationToken = ctf.create( locationCard );
            }
            locationToken.cDropArea.isPropagate = true;

            Utils.game.dropUnderTableAndEndCard( locationCard );
            
            locationToken.cCardToken.isAnimated = false;
            locationHolder.cDropArea.forceDrop( locationToken, null, action_scope_type.LOCAL );
            locationToken.cCardToken.isAnimated = true;

            ++locationIndex;
        }

        this._arrLocationHolder[ 0 ].cDropArea.forceDrop( this._playerToken, null, action_scope_type.LOCAL );

        for ( let locationHolder of this._arrLocationHolder )
        {
            locationHolder.cIslandLocationHolder.onThreatCountUpdated.add( this.onLocationThreatCount_Updated, this );
        }
    }

    // overrides.
    public saveGame(): ISgCustomPanel
    {
        let arrSgCardToken: Array<ISgCardToken> = new Array<ISgCardToken>();
        for ( let locationHolder of this._arrLocationHolder )
        {
            let sgCardToken: ISgCardToken = null;
            const kLocationToken: GameObject = locationHolder.cIslandLocationHolder.locationToken;
            if ( kLocationToken )
            {
                sgCardToken = kLocationToken.cCardToken.saveGame();
            }
            arrSgCardToken.push( sgCardToken );
        }
        
        return { islandMap : {
            locations: arrSgCardToken,
            playerLocationIndex: this._playerLocationIndex } };
    }

    // overrides.
    public loadGame( sgCustomPanel: ISgCustomPanel, pass: number ): void
    {
        const kSgIslandMap: ISgIslandMap = sgCustomPanel.islandMap;
        let ctf: CardTokenFactory = new CardTokenFactory();
        let locationIndex: number = 0;
        for ( let locationHolder of this._arrLocationHolder )
        {
            const kSgCardToken: ISgCardToken = kSgIslandMap.locations[ locationIndex ];
            let locationCard: GameObject = GameObject.find( kSgCardToken.oid.substr( 0, kSgCardToken.oid.length - 1 ) );
            //
            let locationToken: GameObject = ctf.create( locationCard );
            Utils.game.dropUnderTableAndEndCard( locationCard );
            locationToken.cCardToken.loadGame( kSgCardToken, pass );
            if ( locationToken.cCardToken.cBackSide.cardId == "012068B" )
            {
                locationToken.cCardToken.cDetailBar.removeItem( detail_bar_icon_type.UNIQUE );
            }
            locationToken.cDropArea.isPropagate = true;
            locationHolder.cDropArea.forceDrop( locationToken, null, action_scope_type.LOCAL );

            ++locationIndex;
        }

        this._arrLocationHolder[ kSgIslandMap.playerLocationIndex ].cDropArea.forceDrop( this._playerToken, null, action_scope_type.LOCAL );

        for ( let locationHolder of this._arrLocationHolder )
        {
            locationHolder.cIslandLocationHolder.onThreatCountUpdated.add( this.onLocationThreatCount_Updated, this );
        }
    }

    // private:

    private findActiveLocatioHolders( fromIndex: number ): Array<GameObject>
    {
        let result: Array<GameObject> = new Array<GameObject>();

        const kLocationHolderCount: number = this._isNightmare ? 18 : 15;
        const kRowSize: number = this._isNightmare ? 6 : 5;

        let upIndex: number = fromIndex - kRowSize;
        if ( upIndex >= 0 )
        {
            result.push( this._arrLocationHolder[ upIndex ] );
        }

        let leftIndex: number = fromIndex - 1;
        if ( leftIndex >= 0 && fromIndex % kRowSize > leftIndex % kRowSize  )
        {
            result.push( this._arrLocationHolder[ leftIndex ] );
        }

        let rightIndex: number = fromIndex + 1;
        if ( rightIndex < kLocationHolderCount && fromIndex % kRowSize < rightIndex % kRowSize )
        {
            result.push( this._arrLocationHolder[ rightIndex ] );
        }

        let downIndex: number = fromIndex + kRowSize;
        if ( downIndex < kLocationHolderCount )
        {
            result.push( this._arrLocationHolder[ downIndex ] );
        }

        return result;
    }

    // #endregion //


    // #region Callbacks //

    private onPlayerToken_Dropped( islandLocationHolder: GameObject ): void
    {
        let activeLocationToken: GameObject = ServiceLocator.game.cGameWorld.findLocation( location_type.ACTIVE_LOCATION ).cActiveLocationHolder.activeLocationToken;
        if ( activeLocationToken && this._playerLocationIndex != null )
        {
            if ( activeLocationToken.cCardToken.curSide.cLocationSide )
            {
                activeLocationToken.cCardToken.curSide.cLocationSide.progress.cTokenCounter.setCount( 0, action_scope_type.LOCAL );
            }
            this._arrLocationHolder[ this._playerLocationIndex ].cDropArea.forceDrop( activeLocationToken, null, action_scope_type.LOCAL );
        }

        this._playerLocationIndex = this._arrLocationHolder.indexOf( islandLocationHolder );

        this._arrActiveLocationHolder = this.findActiveLocatioHolders( this._playerLocationIndex );
        this.drawThreatDiagram( this._playerLocationIndex, this._arrActiveLocationHolder );

        const kStagingThreat: number = this.calcStagingThreat();
        ServiceLocator.game.cGameWorld.cSauronArea.cStagingThreatCounter.onIslandMapThreatUpdated( kStagingThreat - this._stagingThreat );
        this._stagingThreat = kStagingThreat;
    }

        private drawThreatDiagram( fromIndex: number, arrActiveLocationHolder: Array<GameObject> ): void
        {
            this._threatLayer.removeChildren();

            let g: PIXI.Graphics = new PIXI.Graphics();
            this._threatLayer.addChild( g );

            for ( let locationHolder of arrActiveLocationHolder )
            {
                const kToIndex: number = this._arrLocationHolder.indexOf( locationHolder );
                this.drawThreatConnection( fromIndex, kToIndex, g );
            }
        }

            private drawThreatConnection( fromIndex: number, toIndex: number, g: PIXI.Graphics ): void
            {
                const kFromLocationHolder: GameObject = this._arrLocationHolder[ fromIndex ];
                g.moveTo( kFromLocationHolder.cContainer.c.x, kFromLocationHolder.cContainer.c.y );
                g.lineStyle( 2, ServiceLocator.game.playerColors[ player_type.SAURON ] );
                const kToLocationHolder: GameObject = this._arrLocationHolder[ toIndex ];
                g.lineTo( kToLocationHolder.cContainer.c.x, kToLocationHolder.cContainer.c.y );

                let icon: PIXI.Sprite = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( "threat" ) );   
                icon.anchor.set( 0.5 );
                Utils.game.limitSideSize( 30, icon );
                icon.position.copyFrom( kFromLocationHolder.cContainer.c.position );
                if ( fromIndex > toIndex )
                {
                    if ( fromIndex - toIndex == 1 )
                    {
                        icon.x -= ( kFromLocationHolder.cContainer.c.x - kToLocationHolder.cContainer.c.x ) * 0.5;
                    }
                    else
                    {
                        icon.y -= ( kFromLocationHolder.cContainer.c.y - kToLocationHolder.cContainer.c.y ) * 0.5;
                    }
                }
                else 
                {
                    if ( toIndex - fromIndex == 1 )
                    {
                        icon.x += ( kToLocationHolder.cContainer.c.x - kFromLocationHolder.cContainer.c.x ) * 0.5;
                    }
                    else
                    {
                        icon.y += ( kToLocationHolder.cContainer.c.y - kFromLocationHolder.cContainer.c.y ) * 0.5;
                    }
                }
                this._threatLayer.addChild( icon );
            }

        private calcStagingThreat(): number
        {
            let result: number = 0;
    
            for ( let locationHolder of this._arrActiveLocationHolder )
            {
                let locationToken: GameObject = locationHolder.cIslandLocationHolder.locationToken;
                if ( locationToken && locationToken.cCardToken.curSide.cLocationSide )
                {
                    result += locationToken.cCardToken.curSide.cLocationSide.threat.cTokenCounter.count;
                }
            }
    
            return result;
        }

    private onLocationThreatCount_Updated( locationHolder: GameObject ): void
    {
        if ( this._arrActiveLocationHolder.indexOf( locationHolder ) != -1 )
        {
            const kStagingThreat: number = this.calcStagingThreat();
            ServiceLocator.game.cGameWorld.cSauronArea.cStagingThreatCounter.onIslandMapThreatUpdated( kStagingThreat - this._stagingThreat );
            this._stagingThreat = kStagingThreat;
        }
    }

    // #endregion //
}