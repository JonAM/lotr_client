import CCustomArea from "../CCustomArea";

import { action_scope_type } from "../../../../service/socket_io/GameSocketIOController";
import { player_type } from "../CPlayerArea";
import ServiceLocator from "../../../../ServiceLocator";
import Utils from "../../../../Utils";
import * as PIXI from "pixi.js";

import { ISgCircuit, ISgCustomPanel, ISgTrackStage } from "../../../../view/game/SaveGameView";
import GameObject from "../../../GameObject";
import CTrackStage from "./circuit/CTrackStage";
import CChariotToken from "./circuit/CChariotToken";
import CHighlightPoiReceptor from "../poi_receptor/CHighlightPoiReceptor";
import CContainer from "../../pixi/CContainer";
import CCardPreviewable from "../../input/CCardPreviewable";
import CDraggable from "../../input/CDraggable";
import CVictoryBtn from "../../card/token/CVictoryBtn";
import CShareableGameElement from "../../CShareableGameElement";


export default class CCircuit extends CCustomArea
{
    // #region Attributes //

    // private:

    private _challengersToken: GameObject = null;
    private _wainridersToken: GameObject = null;
    private _victoryBtn: GameObject = null;
    private _arrTrackStage: Array<GameObject> = null;

    // Constants.
    private readonly _kPanelWidth: number = 951;
    private readonly _kPanelHeight: number = 475;

    // #endregion //


    // #region Properties //

    public get challengersToken(): GameObject { return this._challengersToken; }
    public get wainridersToken(): GameObject { return this._wainridersToken; }

    // #endregion //

    
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CCircuit";
    }

    public init(): void
    {
        super.init();
        
        this._go.cContainer.c.interactive = true;

        this._go.cGraphics.g.lineStyle( 2, 0x000000, 1 );
        this._go.cGraphics.g.beginFill( 0xefe3af, 0.7 );
        this._go.cGraphics.g.drawRect( 0, 0, this._kPanelWidth, this._kPanelHeight );
        this._go.cGraphics.g.endFill();

        let bg: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( "circuit_bg" ) );
        this._go.cContainer.c.addChild( bg );

        this._arrTrackStage = this.createCircuit( bg );

        this._challengersToken = new GameObject( [ new CContainer(), new CChariotToken(), new CShareableGameElement(), new CDraggable(), new CCardPreviewable(), new CHighlightPoiReceptor() ] );
        this._challengersToken.cChariotToken.cardId = "022065";
        this._challengersToken.cChariotToken.playerType = player_type.PLAYER;
        this._challengersToken.cDraggable.dragShadowTextureCreator = this._challengersToken.cChariotToken;
        this._challengersToken.init();
        
        this._wainridersToken = new GameObject( [ new CContainer(), new CChariotToken(), new CShareableGameElement(), new CDraggable(), new CCardPreviewable(), new CHighlightPoiReceptor() ] );
        this._wainridersToken.cChariotToken.cardId = "022064";
        this._wainridersToken.cChariotToken.playerType = player_type.SAURON;
        this._wainridersToken.cDraggable.dragShadowTextureCreator = this._wainridersToken.cChariotToken;
        this._wainridersToken.init();
    }

        private createCircuit( bg: PIXI.Container ): Array<GameObject>
        {
            let result: Array<GameObject> = new Array<GameObject>();

            const kArrQuestId: Array<string> = [ 
                "022075", "022076", "022077", 
                "022078", "022079", "022080" ]; 
            const kArrPosition: Array<PIXI.Point> = [ 
                new PIXI.Point( 235, 308 ), new PIXI.Point( 545, 329 ), new PIXI.Point( 832, 338 ), 
                new PIXI.Point( 790, 117 ), new PIXI.Point( 406, 127 ), new PIXI.Point( 111, 189 ) ];
            for ( let i: number = 0; i < 6; ++i )
            {
                let trackStage: GameObject = new GameObject( [ new CContainer(), new CTrackStage() ] );
                trackStage.cTrackStage.position = i +1;
                trackStage.cTrackStage.questId = kArrQuestId[ i ];
                trackStage.cTrackStage.circuit = this._go;
                trackStage.init();
                trackStage.cContainer.c.position.copyFrom( kArrPosition[ i ] );
                bg.addChild( trackStage.cContainer.c );

                result.push( trackStage );
            }

            return result;
        }

    public end(): void
    {
        for ( let trackStage of this._arrTrackStage )
        {
            trackStage.end();
        }
        this._arrTrackStage = null;

        this._wainridersToken.end();
        this._wainridersToken = null;

        this._challengersToken.end();
        this._challengersToken = null;

        if ( this._victoryBtn )
        {
            this._victoryBtn.end();
            this._victoryBtn = null;
        }
    }

    public startRace(): void
    {
        this._arrTrackStage[ 0 ].cTrackStage.frame.cDropArea.forceDrop( this._challengersToken, null, action_scope_type.LOCAL );
        this._arrTrackStage[ 0 ].cTrackStage.frame.cDropArea.forceDrop( this._wainridersToken, null, action_scope_type.LOCAL );
    }

    public advanceChariotToken( playerType: player_type, fromPosition: number ): void
    {
        const kNextTrackStageIndex: number = fromPosition == 6 ? 0 : fromPosition;
        const kChariotToken: GameObject = playerType == player_type.PLAYER ? this._challengersToken : this._wainridersToken;
        this._arrTrackStage[ kNextTrackStageIndex ].cTrackStage.frame.cDropArea.forceDrop( kChariotToken, null, action_scope_type.LOCAL );
    }

    public endRace( winner: player_type ): void
    {
        // Bg.
        let bg: PIXI.Graphics = new PIXI.Graphics();
        bg.beginFill( 0x000000, 0.5 );
        bg.drawRect( 0, 0, this._go.cContainer.c.width, this._go.cContainer.c.height );
        bg.endFill();
        this._go.cContainer.c.addChild( bg );
        bg.interactive = true;

        // Title.
        let title: PIXI.Text = new PIXI.Text( jQuery.i18n( "THE_RACE_IS_OVER" ), ServiceLocator.game.textStyler.title );
        title.anchor.set( 0.5 );
        title.position.set( this._go.cContainer.c.width * 0.5, this._go.cContainer.c.height * 0.5 - 150 );
        this._go.cContainer.c.addChild( title );

        // Challengers.
        let challengers: PIXI.Container = new PIXI.Container();
        challengers.name = "end_race_challengers";
        //
        let challengersPortrait: PIXI.Sprite = new PIXI.Sprite( Utils.img.createCardPortrait( "022065" ) );
        challengersPortrait.anchor.set( 0.5 );
        challengers.addChild( challengersPortrait );
        //
        let challengersFrame: PIXI.Graphics = new PIXI.Graphics();
        challengersFrame.lineStyle( 2, ServiceLocator.game.playerColors[ player_type.PLAYER ] );
        challengersFrame.drawRect( -challengers.width * 0.5, -challengers.height * 0.5, challengers.width, challengers.height );
        challengers.addChild( challengersFrame );
        //
        challengers.position.set( this._go.cContainer.c.width * 0.5 - 100, this._go.cContainer.c.height * 0.5 + 30 );
        this._go.cContainer.c.addChild( challengers );

        // Wainriders.
        let wainriders: PIXI.Container = new PIXI.Container();
        //
        let wainridersPortrait: PIXI.Sprite = new PIXI.Sprite( Utils.img.createCardPortrait( "022064" ) );
        wainridersPortrait.anchor.set( 0.5 );
        wainriders.addChild( wainridersPortrait );
        //
        let wainridersFrame: PIXI.Graphics = new PIXI.Graphics();
        wainridersFrame.lineStyle( 2, ServiceLocator.game.playerColors[ player_type.SAURON ] );
        wainridersFrame.drawRect( -wainriders.width * 0.5, -wainriders.height * 0.5, wainriders.width, wainriders.height );
        wainriders.addChild( wainridersFrame );
        //
        wainriders.position.set( this._go.cContainer.c.width * 0.5 + 100, this._go.cContainer.c.height * 0.5 + 30 );
        this._go.cContainer.c.addChild( wainriders );

        // Laurel.
        let laurel: PIXI.Sprite = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( "laurel" ) );
        laurel.anchor.set( 0.5, 1 );
        Utils.game.limitSideSize( challengers.width * 0.6, laurel );
        laurel.position.set( 0, challengers.height * 0.5 + 10 );
        if ( winner == player_type.PLAYER )
        {
            challengers.addChild( laurel );

            wainriders.scale.set( 0.7 );

            this._victoryBtn = new GameObject( [ new CContainer(), new CVictoryBtn() ] );
            this._victoryBtn.init();
            this._victoryBtn.cContainer.c.position.set( -challengersPortrait.width * 0.5 + 15, -challengersPortrait.height * 0.5 + 15 );
            challengers.addChild( this._victoryBtn.cContainer.c );
        }
        else
        {
            wainriders.addChild( laurel );

            challengers.scale.set( 0.7 );
        }
    }

    // overrides.
    public saveGame(): ISgCustomPanel
    {
        let arrSgTrackStage: Array<ISgTrackStage> = new Array<ISgTrackStage>();
        for ( let trackStage of this._arrTrackStage )
        {
            arrSgTrackStage.push( trackStage.cTrackStage.saveGame() );
        }

        return { circuit: { 
            trackStages: arrSgTrackStage,
            challengersToken: this._challengersToken.cChariotToken.saveGame(),
            wainridersToken: this._wainridersToken.cChariotToken.saveGame() } };
    }

    // overrides.
    public loadGame( sgCustomPanel: ISgCustomPanel, pass: number ): void
    {
        const kSgCircuit: ISgCircuit = sgCustomPanel.circuit;
        let trackStageIndex: number = 0;
        for ( let sgTrackStage of kSgCircuit.trackStages )
        {
            this._arrTrackStage[ trackStageIndex ].cTrackStage.loadGame( sgTrackStage, pass );

            trackStageIndex += 1;
        }

        this._challengersToken.cChariotToken.loadGame( kSgCircuit.challengersToken, pass );
        this._wainridersToken.cChariotToken.loadGame( kSgCircuit.wainridersToken, pass );

        this._arrTrackStage[ this._challengersToken.cChariotToken.lastTrackPosition - 1 ].cTrackStage.frame.cDropArea.forceDrop( this._challengersToken, null, action_scope_type.LOCAL );
        this._arrTrackStage[ this._wainridersToken.cChariotToken.lastTrackPosition - 1 ].cTrackStage.frame.cDropArea.forceDrop( this._wainridersToken, null, action_scope_type.LOCAL );
    }

    // #endregion //
}