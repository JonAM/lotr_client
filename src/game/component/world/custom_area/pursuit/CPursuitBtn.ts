import Component from "../../../Component";

import ServiceLocator from "../../../../../ServiceLocator";
import * as PIXI from "pixi.js";
import { ISgPursuitBtn } from "../../../../../view/game/SaveGameView";


export default class CPursuitBtn extends Component
{
    // #region Attributes //

    // private:

    private _icon: PIXI.Sprite = null;
    private _isPursuitFar: boolean = true;

    // #endregion //


    // #region Properties //

    public get isPursuitFar(): boolean { return this._isPursuitFar; }

    // #endregion //

    
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CPursuitBtn";
    }

    public init(): void
    {        
        console.assert( this._go.cGraphics != null, "CPursuitBtn.ts :: init() :: CGraphics component not found." );
        console.assert( this._go.cButton != null, "CPursuitBtn.ts :: init() :: CButton component not found." );
        console.assert( this._go.cTooltipReceptor != null, "CPursuitBtn.ts :: init() :: CTooltipReceptor component not found." );
        
        super.init();
        
        this._go.cGraphics.g.lineStyle( 1, 0x000000, 1 );
        this._go.cGraphics.g.beginFill( 0xefe3af, 1 );
        this._go.cGraphics.g.drawRect( -40, -20, 80, 40 );
        this._go.cGraphics.g.endFill();

        this._icon = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( "pursuit_far" ) );
        this._icon.anchor.set( 0.5 );
        this._go.cContainer.c.addChild( this._icon );

        this._go.cTooltipReceptor.text = jQuery.i18n( "FAR" );

        // Listen to events.
        this._go.cButton.onClick.add( this.onPursuitBtn_Click, this );
    }

    public end(): void
    {
        // Cleanup events.
        this._go.cButton.onClick.add( this.onPursuitBtn_Click, this );

        super.end();
    }

    public saveGame(): ISgPursuitBtn
    {
        return { isPursuitFar: this._isPursuitFar };
    }

    public loadGame( sgPursuitBtn: ISgPursuitBtn, pass: number ): void
    {
       this._isPursuitFar = sgPursuitBtn.isPursuitFar;

       this.updateTitleAndIcon( this._isPursuitFar );
    }

        private updateTitleAndIcon( isPursuitFar: boolean ): void
        {
            this._go.cTooltipReceptor.text = jQuery.i18n( isPursuitFar ? "FAR" : "CLOSE" );
            this._icon.texture = ServiceLocator.resourceStack.findAsTexture(  isPursuitFar ? "pursuit_far" : "pursuit_close" );
        }

    // #endregion //


    // #region Callbacks //

    private onPursuitBtn_Click(): void
    {
        this._isPursuitFar = !this._isPursuitFar;

        this.updateTitleAndIcon( this._isPursuitFar );
    }

    // #endregion //
}