import Component from "../../Component";

import { location_type } from "../CGameWorld";
import ServiceLocator from "../../../../ServiceLocator";
import Utils from "../../../../Utils";
import * as PIXI from "pixi.js";

import CActorArea from "../CActorArea";
import GameObject from "../../../GameObject";
import CContainer from "../../pixi/CContainer";
import CGraphics from "../../pixi/CGraphics";
import CDropArea from "../../input/CDropArea";
import CLogLocation from "../CLogLocation";
import CPlayerActorArea from "../actor_area/CPlayerActorArea";
import CEngagedActorArea from "../actor_area/CEngagedActorArea";
import { ISgCustomPanel, ISgIsolatedArea, ISgPlayerPlayArea, ISgSplitSauronArea } from "../../../../view/game/SaveGameView";
import CSauronActorArea from "../actor_area/CSauronActorArea";
import CQuestHolder from "../../ui/CQuestHolder";
import CActiveLocationHolder from "../../ui/CActiveLocationHolder";
import { player_type } from "../CPlayerArea";
import CCustomArea from "../CCustomArea";
import CStagingThreatCounter from "../../ui/CStagingThreatCounter";
import CHighlightPoiReceptor from "../poi_receptor/CHighlightPoiReceptor";
import CPlayerWillpowerCounter from "../../ui/CPlayerWillpowerCounter";
import CQuestVariation from "../../ui/CQuestVariation";
import CShareableGameElement from "../../CShareableGameElement";


export default class CSplitSauronArea extends CCustomArea
{
    // #region Attributes //

    // private:

    private _staging: GameObject = null; 
    private _stagingThreatCounter: GameObject = null;
    private _questHolder: GameObject = null;
    private _activeLocationHolder: GameObject = null;

    // Constants.
    private readonly _kPanelWidth: number = 1750;
    private readonly _kPanelHeight: number = 500;

    // #endregion //


    // #region Properties //

    public get staging(): GameObject { return this._staging; }
    public get cStaging(): CSauronActorArea { return this._staging.cSauronActorArea; }
    public get stagingThreatCounter(): GameObject { return this._stagingThreatCounter; }
    public get cStagingThreatCounter(): CStagingThreatCounter { return this._stagingThreatCounter.cStagingThreatCounter; }
    public get questHolder(): GameObject { return this._questHolder; }
    public get cQuestHolder(): CQuestHolder { return this._questHolder.cQuestHolder; }
    public get activeLocationHolder(): GameObject { return this._activeLocationHolder; }
    public get cActiveLocationHolder(): CActiveLocationHolder { return this._activeLocationHolder.cActiveLocationHolder; }

    // #endregion //

    
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CSplitSauronArea";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CSplitSauronArea.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cGameLayerProvider != null, "CSplitSauronArea.ts :: init() :: CGamePlayerProvider component not found." );

        this._go.cContainer.c.interactive = true;

        this._go.cGraphics.g.lineStyle( 2, 0x000000, 1 );
        this._go.cGraphics.g.beginFill( 0xefe3af, 0.7 );
        this._go.cGraphics.g.drawRect( 0, 0, this._kPanelWidth, this._kPanelHeight );
        this._go.cGraphics.g.endFill();

        this._staging = this.createStaging();
        this._staging.cContainer.c.position.set( ( this._go.cContainer.c.width - this._staging.cContainer.c.width ) * 0.5, 30 );
        this._go.cContainer.addChild( this._staging );

        this._stagingThreatCounter = this.createStagingThreatCounter();
        this._stagingThreatCounter.cContainer.c.position.set( 
            this._go.cContainer.c.width * 0.5, 
            this._go.cContainer.c.height - 60 - 180 * 0.5 + this._stagingThreatCounter.cContainer.c.height * 0.5 + 10 );
        this._go.cContainer.addChild( this._stagingThreatCounter );

        this._activeLocationHolder = this.createActiveLocationHolder();
        //
        this._questHolder = this.createQuestHolder( this._activeLocationHolder );
        this._questHolder.cContainer.c.position.set( 
            this._go.cContainer.c.width * 0.5 - 180 - 30, 
            this._go.cContainer.c.height - 60 - 180 * 0.5 );
        this._go.cContainer.addChild( this._questHolder );
        //
        this._activeLocationHolder.cContainer.c.position.set( 
            this._go.cContainer.c.width * 0.5 + 180 + 30, 
            this._questHolder.cContainer.c.y );
        this._go.cContainer.addChild( this._activeLocationHolder );
    }

        private createStaging(): GameObject
        {
            let staging: GameObject = new GameObject( [ new CGraphics(), new CSauronActorArea(), new CDropArea(), new CLogLocation() ] );
            staging.oid = this._go.oid + "_staging_area";
            staging.cActorArea.location = location_type.SPLIT_SAURON_STAGING_AREA;
            staging.cActorArea.width = 1690;
            staging.cActorArea.lineColor = ServiceLocator.game.playerColors[ player_type.SAURON ];
            staging.cActorArea.padding = new PIXI.Point( 35, 5 );
            staging.cActorArea.actorMargin = 30;
            staging.cDropArea.target = staging.cActorArea;
            staging.cDropArea.isSelfDropAllowed = true;
            staging.cLogLocation.location = location_type.SPLIT_SAURON_STAGING_AREA;
            staging.init();

            let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( "staging" ).data ) );
            Utils.game.limitSideSize( 30, icon );
            icon.position.set( 5 );
            staging.cContainer.c.addChildAt( icon, 1 );

            return staging
        }

        private createStagingThreatCounter(): GameObject
        {
            let stagingThreatCounter: GameObject = new GameObject( [ new CContainer(), new CStagingThreatCounter(), new CHighlightPoiReceptor() ] );
            stagingThreatCounter.oid = this._go.oid + "_staging_threat_counter";
            stagingThreatCounter.cStagingThreatCounter.staging = this._staging;
            stagingThreatCounter.init();

            return stagingThreatCounter
        }

        private createQuestHolder( activeLocationHolder: GameObject ): GameObject
        {
            let holder: GameObject = new GameObject( [ new CContainer(), new CQuestHolder(), new CDropArea(), new CLogLocation() ] );
            holder.oid = this._go.oid + "_quest_holder";
            holder.cQuestHolder.location = location_type.SPLIT_SAURON_QUEST;
            holder.cQuestHolder.activeLocationHolder = activeLocationHolder;
            holder.cDropArea.target = holder.cQuestHolder;
            holder.cLogLocation.location = location_type.SPLIT_SAURON_QUEST;
            holder.init();

            return holder;
        }

        private createActiveLocationHolder(): GameObject
        {
            let holder: GameObject = new GameObject( [ new CContainer(), new CActiveLocationHolder(), new CDropArea(), new CLogLocation() ] );
            holder.oid = this._go.oid + "_active_location_holder";
            holder.cActiveLocationHolder.location = location_type.SPLIT_SAURON_ACTIVE_LOCATION;
            holder.cDropArea.target = holder.cActiveLocationHolder;
            holder.cLogLocation.location = location_type.SPLIT_SAURON_ACTIVE_LOCATION;
            holder.init();

            return holder;
        }

    public end(): void
    {
        this._staging.end();
        this._staging = null;

        this._stagingThreatCounter.end();
        this._stagingThreatCounter = null;

        this._questHolder.end();
        this._questHolder = null;

        this._activeLocationHolder.end();
        this._activeLocationHolder = null;
    }

    // overrides.
    public saveGame(): ISgCustomPanel
    {
        return { splitSauronArea : { 
            staging: this._staging.cActorArea.saveGame(),
            stagingThreatCounter: this._stagingThreatCounter.cStagingThreatCounter.saveGame(),
            questHolder: this._questHolder.cQuestHolder.saveGame(),
            activeLocationHolder: this._activeLocationHolder.cActiveLocationHolder.saveGame() } };
    }

    // overrides.
    public loadGame( sgCustomPanel: ISgCustomPanel, pass: number ): void
    {
        const kSgSplitSauronArea: ISgSplitSauronArea = sgCustomPanel.splitSauronArea;
        this._staging.cActorArea.loadGame( kSgSplitSauronArea.staging, pass );
        this._stagingThreatCounter.cStagingThreatCounter.loadGame( kSgSplitSauronArea.stagingThreatCounter, pass );
        this._questHolder.cQuestHolder.loadGame( kSgSplitSauronArea.questHolder, pass );
        this._activeLocationHolder.cActiveLocationHolder.loadGame( kSgSplitSauronArea.activeLocationHolder, pass );
    }

    // #endregion //
}