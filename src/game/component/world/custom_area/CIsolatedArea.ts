import { location_type } from "../CGameWorld";
import ServiceLocator from "../../../../ServiceLocator";
import Utils from "../../../../Utils";
import * as PIXI from "pixi.js";

import CActorArea from "../CActorArea";
import GameObject from "../../../GameObject";
import CContainer from "../../pixi/CContainer";
import CGraphics from "../../pixi/CGraphics";
import CDropArea from "../../input/CDropArea";
import CLogLocation from "../CLogLocation";
import CPlayerActorArea from "../actor_area/CPlayerActorArea";
import CEngagedActorArea from "../actor_area/CEngagedActorArea";
import { ISgCustomPanel, ISgIsolatedArea } from "../../../../view/game/SaveGameView";
import CSauronActorArea from "../actor_area/CSauronActorArea";
import CQuestHolder from "../../ui/CQuestHolder";
import CActiveLocationHolder from "../../ui/CActiveLocationHolder";
import { player_type } from "../CPlayerArea";
import CCustomArea from "../CCustomArea";
import CStagingThreatCounter from "../../ui/CStagingThreatCounter";
import CHighlightPoiReceptor from "../poi_receptor/CHighlightPoiReceptor";
import CPlayerWillpowerCounter from "../../ui/CPlayerWillpowerCounter";
import CQuestVariation from "../../ui/CQuestVariation";


export default class CIsolatedArea extends CCustomArea
{
    // #region Attributes //

    // private:

    private _home: GameObject = null; 
    private _engaged: GameObject = null;
    private _staging: GameObject = null; 
    private _stagingThreatCounter: GameObject = null;
    private _allyWillpowerCounter: GameObject = null;
    private _sauronWillpowerCounter: GameObject = null;
    private _questVariation: GameObject = null;
    private _questHolder: GameObject = null;
    private _activeLocationHolder: GameObject = null;

    // Constants.
    private readonly _kPanelWidth: number = 1750;
    private readonly _kPanelHeight: number = 940;

    // #endregion //


    // #region Properties //

    public get home(): GameObject { return this._home; }
    public get cHome(): CActorArea { return this._home.cActorArea; }
    public get engaged(): GameObject { return this._engaged; }
    public get cEngaged(): CEngagedActorArea { return this._engaged.cEngagedActorArea; }
    public get staging(): GameObject { return this._staging; }
    public get cStaging(): CSauronActorArea { return this._staging.cSauronActorArea; }
    public get stagingThreatCounter(): GameObject { return this._stagingThreatCounter; }
    public get cStagingThreatCounter(): CStagingThreatCounter { return this._stagingThreatCounter.cStagingThreatCounter; }
    public get allyWillpowerCounter(): GameObject { return this._allyWillpowerCounter; }
    public get cAllyWillpowerCounter(): CPlayerWillpowerCounter { return this._allyWillpowerCounter.cPlayerWillpowerCounter; }
    public get sauronWillpowerCounter(): GameObject { return this._sauronWillpowerCounter; }
    public get cSauronWillpowerCounter(): CPlayerWillpowerCounter { return this._sauronWillpowerCounter.cPlayerWillpowerCounter; }
    public get questVariation(): GameObject { return this._questVariation; }
    public get cQuestVariation(): CQuestVariation { return this._questVariation.cQuestVariation; }
    public get questHolder(): GameObject { return this._questHolder; }
    public get cQuestHolder(): CQuestHolder { return this._questHolder.cQuestHolder; }
    public get activeLocationHolder(): GameObject { return this._activeLocationHolder; }
    public get cActiveLocationHolder(): CActiveLocationHolder { return this._activeLocationHolder.cActiveLocationHolder; }

    // #endregion //

    
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CIsolatedArea";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CIsolatedArea.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cGameLayerProvider != null, "CIsolatedArea.ts :: init() :: CGamePlayerProvider component not found." );

        this._go.cContainer.c.interactive = true;

        this._go.cGraphics.g.lineStyle( 2, 0x000000, 1 );
        this._go.cGraphics.g.beginFill( 0xefe3af, 0.7 );
        this._go.cGraphics.g.drawRect( 0, 0, this._kPanelWidth, this._kPanelHeight );
        this._go.cGraphics.g.endFill();

        this._home = this.createHome();
        this._home.cContainer.c.position.set( ( this._go.cContainer.c.width - this._home.cContainer.c.width ) * 0.5, 30 );
        this._go.cContainer.addChild( this._home );
        this._home.cActorArea.multiItemSelector.cMultiItemSelector.setEnabled( false );

        this._engaged = this.createEngaged();
        this._engaged.cContainer.c.position.set( ( this._go.cContainer.c.width - this._engaged.cContainer.c.width ) * 0.5, 244 );
        this._go.cContainer.addChild( this._engaged );
        this._engaged.cActorArea.multiItemSelector.cMultiItemSelector.setEnabled( false );

        this._staging = this.createStaging();
        this._staging.cContainer.c.position.set( ( this._go.cContainer.c.width - this._staging.cContainer.c.width ) * 0.5, 458 );
        this._go.cContainer.addChild( this._staging );
        this._staging.cActorArea.multiItemSelector.cMultiItemSelector.setEnabled( false );

        this._stagingThreatCounter = this.createStagingThreatCounter();
        this._stagingThreatCounter.cContainer.c.position.set( 
            this._go.cContainer.c.width * 0.5, 
            this._go.cContainer.c.height - 60 - 180 * 0.5 + this._stagingThreatCounter.cContainer.c.height * 0.5 + 10 );
        this._go.cContainer.addChild( this._stagingThreatCounter );

        this._allyWillpowerCounter = this.createAllyWillpowerCounter();
        this._allyWillpowerCounter.cContainer.c.visible = false;
        this._allyWillpowerCounter.cContainer.c.position.set( 
            this._go.cContainer.c.width * 0.5, 
            this._go.cContainer.c.height - 60 - 180 * 0.5 - this._allyWillpowerCounter.cContainer.c.height * 0.5 - 10 );
        this._go.cContainer.addChild( this._allyWillpowerCounter );

        this._sauronWillpowerCounter = this.createSauronWillpowerCounter();
        this._sauronWillpowerCounter.cContainer.c.visible = false;
        this._sauronWillpowerCounter.cContainer.c.position.set( 
            this._allyWillpowerCounter.cContainer.c.x,
            this._allyWillpowerCounter.cContainer.c.y - this._allyWillpowerCounter.cContainer.c.height * 0.5 - this._sauronWillpowerCounter.cContainer.c.height * 0.5 - 10  );
        this._go.cContainer.addChild( this._sauronWillpowerCounter );

        this._questVariation = this.createQuestVariation();
        this._questVariation.cContainer.c.position.set( 
            this._stagingThreatCounter.cContainer.c.x - this._stagingThreatCounter.cContainer.c.width * 0.5 - this._questVariation.cContainer.c.width * 0.5 - 10,
            this._stagingThreatCounter.cContainer.c.y );
        this._go.cContainer.addChild( this._questVariation );

        this._activeLocationHolder = this.createActiveLocationHolder();
        //
        this._questHolder = this.createQuestHolder( this._activeLocationHolder );
        this._questHolder.cContainer.c.position.set( 
            this._go.cContainer.c.width * 0.5 - 180 - 30, 
            this._go.cContainer.c.height - 60 - 180 * 0.5 );
        this._go.cContainer.addChild( this._questHolder );
        //
        this._activeLocationHolder.cContainer.c.position.set( 
            this._go.cContainer.c.width * 0.5 + 180 + 30, 
            this._questHolder.cContainer.c.y );
        this._go.cContainer.addChild( this._activeLocationHolder );
    }

        private createHome(): GameObject
        {
            let home: GameObject = new GameObject( [ new CGraphics(), new CPlayerActorArea(), new CDropArea(), new CLogLocation() ] );
            home.oid = this._go.oid + "_home";
            home.cActorArea.location = location_type.ISOLATED_HOME;
            home.cActorArea.width = 1690;
            home.cActorArea.lineColor = ServiceLocator.game.playerColors[ player_type.ALLY ];
            home.cActorArea.padding = new PIXI.Point( 35, 5 );
            home.cActorArea.actorMargin = 30;
            home.cDropArea.target = home.cActorArea;
            home.cDropArea.isSelfDropAllowed = true;
            home.cLogLocation.location = location_type.ISOLATED_HOME;
            home.init();

            let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( "home" ).data ) );
            Utils.game.limitSideSize( 30, icon );
            icon.position.set( 5 );
            home.cContainer.c.addChildAt( icon, 1 );

            return home;
        }

        private createEngaged(): GameObject
        {
            let engaged: GameObject = new GameObject( [ new CGraphics(), new CEngagedActorArea(), new CDropArea(), new CLogLocation() ] );
            engaged.oid = this._go.oid + "_engaged";
            engaged.cActorArea.location = location_type.ISOLATED_ENGAGED;
            engaged.cActorArea.width = 1550;
            engaged.cActorArea.lineColor = ServiceLocator.game.playerColors[ player_type.ALLY ];
            engaged.cActorArea.padding = new PIXI.Point( 35, 5 );
            engaged.cActorArea.actorMargin = 30;
            engaged.cDropArea.target = engaged.cActorArea;
            engaged.cDropArea.isSelfDropAllowed = true;
            engaged.cLogLocation.location = location_type.ISOLATED_ENGAGED;
            engaged.init();

            let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( "engaged" ).data ) );
            Utils.game.limitSideSize( 30, icon );
            icon.position.set( 5 );
            engaged.cContainer.c.addChildAt( icon, 1 );

            return engaged
        }

        private createStaging(): GameObject
        {
            let staging: GameObject = new GameObject( [ new CGraphics(), new CSauronActorArea(), new CDropArea(), new CLogLocation() ] );
            staging.oid = this._go.oid + "_staging_area";
            staging.cActorArea.location = location_type.ISOLATED_STAGING_AREA;
            staging.cActorArea.width = 1690;
            staging.cActorArea.lineColor = ServiceLocator.game.playerColors[ player_type.SAURON ];
            staging.cActorArea.padding = new PIXI.Point( 35, 5 );
            staging.cActorArea.actorMargin = 30;
            staging.cDropArea.target = staging.cActorArea;
            staging.cDropArea.isSelfDropAllowed = true;
            staging.cLogLocation.location = location_type.ISOLATED_STAGING_AREA;
            staging.init();

            let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( "staging" ).data ) );
            Utils.game.limitSideSize( 30, icon );
            icon.position.set( 5 );
            staging.cContainer.c.addChildAt( icon, 1 );

            return staging
        }

        private createStagingThreatCounter(): GameObject
        {
            let stagingThreatCounter: GameObject = new GameObject( [ new CContainer(), new CStagingThreatCounter(), new CHighlightPoiReceptor() ] );
            stagingThreatCounter.oid = this._go.oid + "_staging_threat_counter";
            stagingThreatCounter.cStagingThreatCounter.staging = this._staging;
            stagingThreatCounter.init();

            return stagingThreatCounter
        }

        private createAllyWillpowerCounter(): GameObject
        {
            let allyWillpowerCounter: GameObject = new GameObject( [ new CContainer(), new CPlayerWillpowerCounter() ] );
            allyWillpowerCounter.oid = this._go.oid + "_player_willpower_counter";
            allyWillpowerCounter.cPlayerWillpowerCounter.controller = player_type.ALLY;
            allyWillpowerCounter.cPlayerWillpowerCounter.colorType = player_type.ALLY;
            allyWillpowerCounter.cPlayerWillpowerCounter.actorArea = this._home;
            allyWillpowerCounter.init();

            return allyWillpowerCounter
        }

        private createSauronWillpowerCounter(): GameObject
        {
            let sauronWillpowerCounter: GameObject = new GameObject( [ new CContainer(), new CPlayerWillpowerCounter() ] );
            sauronWillpowerCounter.oid = this._go.oid + "_sauron_willpower_counter";
            sauronWillpowerCounter.cPlayerWillpowerCounter.controller = player_type.ALLY;
            sauronWillpowerCounter.cPlayerWillpowerCounter.colorType = player_type.SAURON;
            sauronWillpowerCounter.cPlayerWillpowerCounter.actorArea = this._staging;
            sauronWillpowerCounter.cPlayerWillpowerCounter.isCompact = true;
            sauronWillpowerCounter.init();

            return sauronWillpowerCounter
        }

        private createQuestVariation(): GameObject
        {
            let questVariation: GameObject = new GameObject( [ new CContainer(), new CQuestVariation() ] );
            questVariation.cQuestVariation.allyWillpowerCounter = this._allyWillpowerCounter;
            questVariation.cQuestVariation.stagingThreatCounter = this._stagingThreatCounter;
            questVariation.cQuestVariation.sauronWillpowerCounter = this._sauronWillpowerCounter;
            questVariation.init();

            return questVariation
        }

        private createQuestHolder( activeLocationHolder: GameObject ): GameObject
        {
            let holder: GameObject = new GameObject( [ new CContainer(), new CQuestHolder(), new CDropArea(), new CLogLocation() ] );
            holder.oid = this._go.oid + "_quest_holder";
            holder.cQuestHolder.location = location_type.ISOLATED_QUEST;
            holder.cQuestHolder.activeLocationHolder = activeLocationHolder;
            holder.cDropArea.target = holder.cQuestHolder;
            holder.cLogLocation.location = location_type.ISOLATED_QUEST;
            holder.init();

            return holder;
        }

        private createActiveLocationHolder(): GameObject
        {
            let holder: GameObject = new GameObject( [ new CContainer(), new CActiveLocationHolder(), new CDropArea(), new CLogLocation() ] );
            holder.oid = this._go.oid + "_active_location_holder";
            holder.cActiveLocationHolder.location = location_type.ISOLATED_ACTIVE_LOCATION;
            holder.cDropArea.target = holder.cActiveLocationHolder;
            holder.cLogLocation.location = location_type.ISOLATED_ACTIVE_LOCATION;
            holder.init();

            return holder;
        }

    public end(): void
    {
        this._home.end();
        this._home = null;

        this._engaged.end();
        this._engaged = null;

        this._staging.end();
        this._staging = null;

        this._stagingThreatCounter.end();
        this._stagingThreatCounter = null;

        this._questHolder.end();
        this._questHolder = null;

        this._activeLocationHolder.end();
        this._activeLocationHolder = null;
    }

    // overrides.
    public saveGame(): ISgCustomPanel
    {
        return { isolatedArea : { 
            home: this._home.cActorArea.saveGame(),
            engaged: this._engaged.cActorArea.saveGame(),
            staging: this._staging.cActorArea.saveGame(),
            stagingThreatCounter: this._stagingThreatCounter.cStagingThreatCounter.saveGame(),
            allyWillpowerCounter: this._allyWillpowerCounter.cPlayerWillpowerCounter.saveGame(),
            sauronWillpowerCounter: this._sauronWillpowerCounter.cPlayerWillpowerCounter.saveGame(),
            questVariation: this._questVariation.cQuestVariation.saveGame(),
            questHolder: this._questHolder.cQuestHolder.saveGame(),
            activeLocationHolder: this._activeLocationHolder.cActiveLocationHolder.saveGame() } };
    }

    // overrides.
    public loadGame( sgCustomPanel: ISgCustomPanel, pass: number ): void
    {
        const kSgIsolatedArea: ISgIsolatedArea = sgCustomPanel.isolatedArea;
        this._home.cActorArea.loadGame( kSgIsolatedArea.home, pass );
        this._engaged.cActorArea.loadGame( kSgIsolatedArea.engaged, pass );
        this._staging.cActorArea.loadGame( kSgIsolatedArea.staging, pass );
        this._stagingThreatCounter.cStagingThreatCounter.loadGame( kSgIsolatedArea.stagingThreatCounter, pass );
        this._allyWillpowerCounter.cPlayerWillpowerCounter.loadGame( kSgIsolatedArea.allyWillpowerCounter, pass );
        this._sauronWillpowerCounter.cPlayerWillpowerCounter.loadGame( kSgIsolatedArea.sauronWillpowerCounter, pass );
        this._questVariation.cQuestVariation.loadGame( kSgIsolatedArea.questVariation, pass );
        this._questHolder.cQuestHolder.loadGame( kSgIsolatedArea.questHolder, pass );
        this._activeLocationHolder.cActiveLocationHolder.loadGame( kSgIsolatedArea.activeLocationHolder, pass );
    }

    // #endregion //
}