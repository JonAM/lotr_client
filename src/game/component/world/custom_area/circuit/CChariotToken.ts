import Component from "../../../Component";
import IDragShadowTextureCreator from "../../../../IDragShadowTextureCreator";

import { OutlineFilter } from "pixi-filters";
import { player_type } from "../../CPlayerArea";
import ServiceLocator from "../../../../../ServiceLocator";
import Utils from "../../../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../../../GameObject";
import CContainer from "../../../pixi/CContainer";
import { ISgChariotToken } from "../../../../../view/game/SaveGameView";


export default class CChariotToken extends Component implements IDragShadowTextureCreator
{
    // #region Attributes //

    // private:

    private _cardId: string = null;
    private _playerType: player_type = null;

    private _lastTrackPosition: number = null;
    private _poiSocket: GameObject = null;

    // #endregion //


    // #region Properties //

    public set cardId( value: string ) { this._cardId = value; }
    public set playerType( value: player_type ) { this._playerType = value; }
    public set lastTrackPosition( value: number ) { this._lastTrackPosition = value; }

    public get cardId(): string { return this._cardId; }
    public get playerType(): player_type { return this._playerType; }
    public get lastTrackPosition(): number { return this._lastTrackPosition; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CChariotToken";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CChallengersToken.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cDraggable != null, "CChallengersToken.ts :: init() :: CDraggable component not found." );
        console.assert( this._go.cCardPreviewable != null, "CChallengersToken.ts :: init() :: CCardPreviewable component not found." );
        console.assert( this._go.cHighlightPoiReceptor != null, "CChallengersToken.ts :: init() :: CHighlightPoiReceptor component not found." );
        console.assert( this._cardId != null, "CChallengersToken.ts :: init() :: this._cardId cannot be null." );
        console.assert( this._playerType != null, "CChallengersToken.ts :: init() :: this._playerType cannot be null." );

        this._go.cCardPreviewable.cardId = this._cardId;

        // Sprite.
        let sprite: PIXI.Sprite = new PIXI.Sprite( Utils.img.createCardPortrait( this._cardId ) );
        sprite.name = "sprite";
        Utils.game.limitSideSize( 50, sprite );
        sprite.anchor.set( 0.5 );
        this._go.cContainer.c.addChild( sprite );

        // Frame.
        let frame: PIXI.Graphics = new PIXI.Graphics();
        frame.lineStyle( 1, ServiceLocator.game.playerColors[ this._playerType ] );
        frame.drawRect( -sprite.width * 0.5, -sprite.height * 0.5, sprite.width, sprite.height )
        this._go.cContainer.c.addChild( frame );

        // POI socket.
        this._poiSocket = new GameObject( [ new CContainer() ] );
        this._poiSocket.cContainer.c.name = "poi_socket";
        this._poiSocket.init();
        this._poiSocket.cContainer.c.y -= sprite.height * 0.5 - 5;
        this._go.cContainer.addChild( this._poiSocket );
        
        this._go.cHighlightPoiReceptor.poiSocket = this._poiSocket;
    }

    public end(): void
    {
        this._poiSocket.end();
        this._poiSocket = null;

        super.end();
    }

    public isFinalLap(): boolean
    {
        return this._go.cContainer.c.getChildByName( "final_lap" ) != null;
    }

    public setFinalLap(): void
    {
        if ( !this._go.cContainer.c.getChildByName( "final_lap" ) )
        {
            let flag: PIXI.Sprite = new PIXI.Sprite( ServiceLocator.resourceStack.findAsTexture( "final_lap" ) );
            flag.name = "final_lap";
            Utils.game.limitSideSize( 30, flag );
            flag.anchor.set( 0, 1 );
            flag.position.set( -this._go.cContainer.c.width * 0.5 - 5,  this._go.cContainer.c.height * 0.5 + 5 );
            this._go.cContainer.c.addChild( flag );
        }
    }

    public setSelectedVfx( isVisible: boolean ): void
    {
        if ( isVisible )
        {
            const kColor: number = ServiceLocator.game.playerColors[ this._playerType ];
            let outlineFilter: OutlineFilter = new OutlineFilter( 5, kColor, 0.5 );
            outlineFilter.padding = 5;
            this._go.cContainer.c.filters = [ outlineFilter ];
        }
        else
        {
            this._go.cContainer.c.filters = [];
        }
    }

    public saveGame(): ISgChariotToken
    {
        return { 
            lastTrackPosition: this._lastTrackPosition,
            isFinalLap: this.isFinalLap() };
    }

    public loadGame( sgChariotToken: ISgChariotToken, pass: number ): void
    {
        this._lastTrackPosition = sgChariotToken.lastTrackPosition;
        if ( sgChariotToken.isFinalLap )
        {
            this.setFinalLap();
        }
    }

    // #endregion //


    // #region IDragShadowTextureCreator //

    public createDragShadowTexture(): PIXI.Texture
    {
        let source: PIXI.Sprite = this._go.cContainer.c.getChildByName( "sprite" ) as PIXI.Sprite;
        let dragShadowTexture = PIXI.RenderTexture.create( { width: source.width, height: source.height } );
        let pos: PIXI.Point = new PIXI.Point();
        source.position.copyTo( pos );
        source.position.set( source.width * 0.5, source.height * 0.5 );
        ServiceLocator.game.app.renderer.render( source, dragShadowTexture );    
        pos.copyTo( source.position );

        return dragShadowTexture;
    }

    // #endregion //
}
