import Component from "../../../Component";
import IGameObjectDropArea from "../../../../IGameObjectDropArea";

import { action_scope_type, player_action_type } from "../../../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../../../ServiceLocator";
import Utils from "../../../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../../../GameObject";
import DroppedEmitter from "../../../../DroppedEmitter";
import { layer_type } from "../../CGameLayerProvider";
import CContainer from "../../../pixi/CContainer";
import CSprite from "../../../pixi/CSprite";
import CTokenCounter from "../../../ui/CTokenCounter";
import CDropArea from "../../../input/CDropArea";
import CDraggable from "../../../input/CDraggable";
import { player_type } from "../../CPlayerArea";
import LogTargetCard from "../../../ui/right_menu/action_log/target/LogTargetCard";
import { ICard } from "../../../../CardDB";
import CGraphics from "../../../pixi/CGraphics";
import CCardPreviewable from "../../../input/CCardPreviewable";
import CHighlightPoiReceptor from "../../poi_receptor/CHighlightPoiReceptor";
import Session from "../../../../../Session";
import { ISgTrackStage } from "../../../../../view/game/SaveGameView";


export default class CTrackStage extends Component implements IGameObjectDropArea
{
    // #region Attributes /
    
    // private:

    private _position: number = null;
    private _questId: string = null;
    private _circuit: GameObject = null;

    private _isVisited: boolean = false;

    private _frame: GameObject = null;
    private _portrait: PIXI.Sprite = null;
    private _challengersCounter: GameObject = null;
    private _wainridersCounter: GameObject = null;
    private _questPoints: GameObject = null;
    private _challengersSocket: PIXI.Container = null;
    private _wainridersSocket: PIXI.Container = null;
    
    private _poiSocket: GameObject = null;

    // Constants.
    private readonly _kQuestPointsPerPlayer: number = 4;

    // #endregion //


    // #region Properties //

    public set position( value: number ) { this._position = value; }
    public set questId( value: string ) { this._questId = value; }
    public set circuit( value: GameObject ) { this._circuit = value; }

    public get frame(): GameObject { return this._frame; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CTrackStage";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CTrackStage.ts :: init() :: CContainer component not found." );
        console.assert( this._position != null, "CTrackStage.ts :: init() :: this._position cannot be null." );
        console.assert( this._questId != null, "CTrackStage.ts :: init() :: this._questId cannot be null." );
        console.assert( this._circuit != null, "CTrackStage.ts :: init() :: this._circuit cannot be null." );

        // POI socket.
        this._poiSocket = new GameObject( [ new CContainer() ] );
        this._poiSocket.cContainer.c.name = "poi_socket";
        this._poiSocket.init();
        this._poiSocket.cContainer.c.y = -30;
        this._go.cContainer.addChild( this._poiSocket );

        // Frame
        this._frame = new GameObject( [ new CGraphics(), new CDropArea(), new CCardPreviewable(), new CHighlightPoiReceptor() ] );
        this._frame.oid = this._go.oid + "_frame";
        this._frame.cGraphics.g.lineStyle( 2, 0x000000 );
        this._frame.cGraphics.g.beginFill( 0xc0c0c0 );
        this._frame.cGraphics.g.drawCircle( 0, 0, 35 );
        this._frame.cGraphics.g.endFill();
        this._frame.cCardPreviewable.cardId = this._questId + "A";
        this._frame.cDropArea.target = this;
        this._frame.cHighlightPoiReceptor.poiSocket = this._poiSocket;
        this._frame.init();
        this._go.cContainer.addChild( this._frame );

        // Inside.
        let inside: PIXI.Graphics = new PIXI.Graphics();
        inside.lineStyle( 2, 0x000000 );
        inside.beginFill( 0xeccc68 );
        inside.drawCircle( 0, 0, 30 );
        inside.endFill();
        this._go.cContainer.c.addChild( inside );

        // Portrait.
        this._portrait = new PIXI.Sprite( Utils.img.createCardPortrait( this._questId + "A" ) );
        Utils.game.limitSideSize( 120, this._portrait );
        this._portrait.anchor.set( 0.5 );
        inside.addChild( this._portrait );
        // Mask.
        let mask: PIXI.Graphics = new PIXI.Graphics();
        mask.beginFill();
        mask.drawCircle( 0, 0, 30 );
        mask.endFill();
        inside.addChild( mask );
        inside.mask = mask;

        // Position text.
        let positionText: PIXI.Text = new PIXI.Text( this._position.toString(), ServiceLocator.game.textStyler.subtitle );
        positionText.anchor.set( 0.5 );
        positionText.y = 30;
        this._go.cContainer.c.addChild( positionText );

        // Challengers progress.
        let container: PIXI.Container = new PIXI.Container();
        // Counter.
        this._challengersCounter = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._challengersCounter.oid = this._go.oid + "_challengers_progress";
        this._challengersCounter.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "progress_token" );
        this._challengersCounter.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 40, this._challengersCounter.cSprite.s );
        this._challengersCounter.cTokenCounter.tokenName = "progress";
        this._challengersCounter.cTokenCounter.count = 0;
        this._challengersCounter.cTokenCounter.minCount = 0;
        this._challengersCounter.cTokenCounter.draggedTexture = ServiceLocator.resourceStack.findAsTexture( "progress_token" );
        this._challengersCounter.cDropArea.target = this._challengersCounter.cTokenCounter;
        this._challengersCounter.init();
        container.addChild( this._challengersCounter.cContainer.c );
        // Text.
        let counterText: PIXI.Text = new PIXI.Text( "0", ServiceLocator.game.textStyler.normal );
        counterText.anchor.set( 0.5 );
        container.addChild( counterText );
        //
        this._challengersCounter.cTokenCounter.onCountUpdated.add( this.onChallengersProgressCount_Updated.bind( this, this._challengersCounter, counterText ) );
        //
        container.visible = false;
        container.position.set( -35, -35 );
        this._go.cContainer.c.addChild( container );

        // Wainriders progress.
        container = new PIXI.Container();
        // Counter.
        this._wainridersCounter = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._wainridersCounter.oid = this._go.oid + "_wainriders_progress";
        this._wainridersCounter.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "wound_token" );
        this._wainridersCounter.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 40, this._wainridersCounter.cSprite.s );
        this._wainridersCounter.cTokenCounter.tokenName = "wound";
        this._wainridersCounter.cTokenCounter.count = 0;
        this._wainridersCounter.cTokenCounter.minCount = 0;
        this._wainridersCounter.cTokenCounter.draggedTexture = ServiceLocator.resourceStack.findAsTexture( "wound_token" );
        this._wainridersCounter.cDropArea.target = this._wainridersCounter.cTokenCounter;
        this._wainridersCounter.init();
        container.addChild( this._wainridersCounter.cContainer.c );
        // Text.
        counterText = new PIXI.Text( "0", ServiceLocator.game.textStyler.normal );
        counterText.anchor.set( 0.5 );
        container.addChild( counterText );
        //
        this._wainridersCounter.cTokenCounter.onCountUpdated.add( this.onWainridersProgressCount_Updated.bind( this, this._wainridersCounter, counterText ) );
        //
        container.visible = false;
        container.position.set( 35, -35 );
        this._go.cContainer.c.addChild( container );

        // Quest points.
        container = new PIXI.Container();
        // Counter.
        this._questPoints = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._questPoints.oid = this._go.oid + "_quest_points";
        this._questPoints.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "quest_points" ).data );
        this._questPoints.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 40, this._questPoints.cSprite.s );
        this._questPoints.cTokenCounter.tokenName = "quest_points";
        this._questPoints.cTokenCounter.count = this._kQuestPointsPerPlayer + ( Session.allyId ? this._kQuestPointsPerPlayer : 0 );
        this._questPoints.cTokenCounter.minCount = 0;
        this._questPoints.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "quest_points" ).data );
        this._questPoints.cDropArea.target = this._questPoints.cTokenCounter;
        this._questPoints.init();
        container.addChild( this._questPoints.cContainer.c );
        // Text.
        counterText = new PIXI.Text( this._questPoints.cTokenCounter.count.toString(), ServiceLocator.game.textStyler.normal );
        counterText.anchor.set( 0.5 );
        container.addChild( counterText );
        //
        this._questPoints.cTokenCounter.onCountUpdated.add( this.onQuestPointCount_Updated.bind( this, this._questPoints, counterText ) );
        //
        container.visible = false;
        container.position.set( 0, -45 );
        this._go.cContainer.c.addChild( container );

        // Chariot token sockets.
        // Challengers.
        this._challengersSocket = new PIXI.Container();
        this._challengersSocket.position.set( -40, 40 );
        this._go.cContainer.c.addChild( this._challengersSocket );
        // Wainriders.
        this._wainridersSocket = new PIXI.Container();
        this._wainridersSocket.position.set( 40, 40 );
        this._go.cContainer.c.addChild( this._wainridersSocket );

        // Listen to events.
        ServiceLocator.game.cGameWorld.cSauronArea.cQuestHolder.onQuestTokenSet.add( this.onQuestToken_Set, this );
    }

    public end(): void
    {
        this._circuit = null;

        this._frame.end();
        this._frame = null;

        this._challengersCounter.end();
        this._challengersCounter = null;

        this._wainridersCounter.end();
        this._wainridersCounter = null;

        this._poiSocket.end();
        this._poiSocket = null;

        super.end();
    }

    public saveGame(): ISgTrackStage
    {
        return {
            isVisited: this._isVisited,
            challengersCounter: this._challengersCounter.cTokenCounter.saveGame(),
            wainridersCounter: this._wainridersCounter.cTokenCounter.saveGame(),
            questPoints: this._questPoints.cTokenCounter.saveGame() }
    }

    public loadGame( sgTrackStage: ISgTrackStage, pass: number ): void
    {
        this._isVisited = sgTrackStage.isVisited;
        if ( this._isVisited )
        {
            this._frame.cCardPreviewable.cardId = this._questId + "B";
            this._portrait.texture = Utils.img.createCardPortrait( this._questId + "B" );
        }
        this._challengersCounter.cTokenCounter.loadGame( sgTrackStage.challengersCounter, pass );
        this._wainridersCounter.cTokenCounter.loadGame( sgTrackStage.wainridersCounter, pass );
        this._questPoints.cTokenCounter.loadGame( sgTrackStage.questPoints, pass );
    }

    // #endregion //


    // #region IGameObjectDropArea //

    public validateDroppedGameObject( dropped: GameObject ): boolean
    {
        return dropped.cChariotToken != null;
    }

    public processDroppedGameObject( dropped: GameObject, global: PIXI.IPoint, actionScope: action_scope_type ): void
    {
        // Set chariot token.
        if ( dropped.cChariotToken.playerType == player_type.SAURON )
        {
            this._wainridersSocket.addChild( dropped.cContainer.c );

            this._wainridersCounter.cContainer.c.parent.visible = true;
        }
        else 
        {
            this._challengersSocket.addChild( dropped.cContainer.c );

            this._challengersCounter.cContainer.c.parent.visible = true;
        }
        this._questPoints.cContainer.c.parent.visible = true;

        // Listen to events.
        dropped.cDraggable.onDropped.addOnce( this.onChariotToken_Dropped, this );

        // Lap completed.
        if ( this._position == 1 && dropped.cChariotToken.lastTrackPosition == 6 )
        {
            if ( dropped.cChariotToken.isFinalLap() )
            {
                this._circuit.cCircuit.endRace( dropped.cChariotToken.playerType );
            }
            else
            {
                dropped.cChariotToken.setFinalLap();
            }
        }
        dropped.cChariotToken.lastTrackPosition = this._position;

        if ( !this._isVisited )
        {
            this._frame.cCardPreviewable.cardId = this._questId + "B";
            this._portrait.texture = Utils.img.createCardPortrait( this._questId + "B" );

           this._isVisited = true;
        }

        // Set quest token.
        if ( dropped.cChariotToken.playerType == player_type.PLAYER )
        {
            let questCard: GameObject = GameObject.find( "o" + ( this._position - 1 ).toString() + "q" );
            if ( questCard )
            {
                questCard.cCard.isAnimated = false;
                questCard.cCard.setFaceDown( action_scope_type.LOCAL );
                questCard.cCard.isAnimated = true;

                ServiceLocator.game.cGameWorld.cSauronArea.questHolder.cDropArea.forceDrop( questCard, null, action_scope_type.LOCAL );
            }
        }

        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.DROP_AT_DROP_AREA, null, [ dropped.oid, this._frame.oid, null ] );
        }

        if ( ServiceLocator.game.isAnimated )
        {
            // Vfx.
            this.playDroppedVfx( dropped );
            Utils.anim.bump( dropped.cContainer.c );
            // Sfx.
            ServiceLocator.audioManager.playSfx( "token_dropped" );
        }
    }

        private playDroppedVfx( dropped: GameObject ): void
        {
            let droppedEmitter: DroppedEmitter = new DroppedEmitter();
            const kVfxLayer: GameObject = Utils.game.findGameProviderLayer( this._go, layer_type.VFX );
            droppedEmitter.layer = kVfxLayer
            droppedEmitter.init();
            const kPos: PIXI.IPoint = kVfxLayer.cContainer.c.toLocal( dropped.cContainer.c.getGlobalPosition() );
            droppedEmitter.playSmall( kPos.x, kPos.y );
        }

    // #endregion //


    // #region Callbacks //

    private onQuestToken_Set( questToken: GameObject ): void
    {
        if ( questToken.cCardToken.frontSide.cQuestSide.cardId == this._questId + "B" )
        {
            questToken.cCardToken.frontSide.cQuestSide.questPoints.cTokenCounter.setCount( this._questPoints.cTokenCounter.count, action_scope_type.LOCAL )
            questToken.cCardToken.frontSide.cQuestSide.progress.cTokenCounter.setCount( this._challengersCounter.cTokenCounter.count, action_scope_type.LOCAL )
                           
            // Listen to events.
            questToken.cCardToken.frontSide.cQuestSide.questPoints.cTokenCounter.onCountUpdated.add( this.onQuestTokenQuestPointsCount_Updated, this );
            questToken.cCardToken.frontSide.cQuestSide.progress.cTokenCounter.onCountUpdated.add( this.onQuestTokenProgressCount_Updated, this );
        
            if ( this._challengersSocket.children.length == 0 )
            {
                this._frame.cDropArea.forceDrop( this._circuit.cCircuit.challengersToken, null, action_scope_type.LOCAL );
            }
        }
    }

    private onChallengersProgressCount_Updated( tokenCounter: GameObject, counterText: PIXI.Text, count: number, delta: number, isPlayerInput: boolean ): void
    {
        counterText.text = count.toString();

        let questToken: GameObject = ServiceLocator.game.cGameWorld.cSauronArea.cQuestHolder.questToken;
        if ( questToken 
            && questToken.cCardToken.frontSide.cQuestSide
            && questToken.cCardToken.frontSide.cQuestSide.cardId == this._questId + "B"
            && questToken.cCardToken.frontSide.cQuestSide.progress.cTokenCounter.count != count )
        {
            questToken.cCardToken.frontSide.cQuestSide.progress.cTokenCounter.setCount( count, action_scope_type.LOCAL );
        }

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._circuit.cCircuit.challengersToken  ), "progress_token", delta, count, true );
        }
    }

    private onWainridersProgressCount_Updated( tokenCounter: GameObject, counterText: PIXI.Text, count: number, delta: number, isPlayerInput: boolean ): void
    {
        counterText.text = count.toString();

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._circuit.cCircuit.wainridersToken ), "wound_token", delta, count, true );
        }

        if ( count >= this._questPoints.cTokenCounter.count )
        {
            this._circuit.cCircuit.advanceChariotToken( player_type.SAURON, this._position );
        }
    }

    private onQuestPointCount_Updated( tokenCounter: GameObject, counterText: PIXI.Text, count: number, delta: number, isPlayerInput: boolean ): void
    {
        counterText.text = count.toString();

        const kQuestPoints: number = this._kQuestPointsPerPlayer + ( Session.allyId ? this._kQuestPointsPerPlayer : 0 );
        if ( count > kQuestPoints )
        {
            counterText.tint = 0x00ff00;
        }
        else if ( count < kQuestPoints )
        {
            counterText.tint = 0xff0000;
        }
        else
        {
            counterText.tint = 0xffffff;
        }

        let questToken: GameObject = ServiceLocator.game.cGameWorld.cSauronArea.cQuestHolder.questToken;
        if ( questToken 
            && questToken.cCardToken.frontSide.cQuestSide
            && questToken.cCardToken.frontSide.cQuestSide.cardId == this._questId + "B"
            && questToken.cCardToken.frontSide.cQuestSide.questPoints.cTokenCounter.count != count )
        {
            questToken.cCardToken.frontSide.cQuestSide.questPoints.cTokenCounter.setCount( count, action_scope_type.LOCAL );
        }

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( count <= this._wainridersCounter.cTokenCounter.count )
        {
            this._circuit.cCircuit.advanceChariotToken( player_type.SAURON, this._position );
        }

        /*if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._go ), "quest_points", delta, count, true );
        }*/
    }

    private onChariotToken_Dropped( dropped: GameObject ): void
    {
        let progress: GameObject = null;
        if ( dropped.cChariotToken.playerType == player_type.SAURON )
        {
            progress = this._wainridersCounter;
        }
        else
        {
            progress = this._challengersCounter;
        }
        progress.cTokenCounter.setCount( 0, action_scope_type.LOCAL );
        progress.cContainer.c.parent.visible = false;
    }

    private onQuestTokenQuestPointsCount_Updated( count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._questPoints.cTokenCounter.setCount( count, action_scope_type.LOCAL );
    }

    private onQuestTokenProgressCount_Updated( count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._challengersCounter.cTokenCounter.setCount( count, action_scope_type.LOCAL );
    }

    // #endregion //
}