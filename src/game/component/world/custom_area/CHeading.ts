import CCustomArea from "../CCustomArea";

import { player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../../ServiceLocator";
import Utils from "../../../../Utils";
import * as PIXI from "pixi.js";

import { ISgCustomPanel } from "../../../../view/game/SaveGameView";
import GameObject from "../../../GameObject";
import CContainer from "../../pixi/CContainer";
import CButton from "../../input/CButton";
import { IOppActionListener, IOpponentAction } from "../../../AllyActionManager";
import { layer_type } from "../CGameLayerProvider";
import { view_layer_id } from "../../../../service/ViewManager";
import CCompass from "./CCompass";


export default class CHeading extends CCustomArea
{
    // #region Attributes //

    // private:

    private _compass: GameObject = null;

    // Constants.
    private readonly _kPanelWidth: number = 510;
    private readonly _kPanelHeight: number = 510;

    // #endregion //

    
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CHeading";
    }

    public init(): void
    {
        super.init();
        
        this._go.cContainer.c.interactive = true;

        this._go.cGraphics.g.lineStyle( 2, 0x000000, 1 );
        this._go.cGraphics.g.beginFill( 0xefe3af, 0.7 );
        this._go.cGraphics.g.drawRect( 0, 0, this._kPanelWidth, this._kPanelHeight );
        this._go.cGraphics.g.endFill();

        let centre: PIXI.Container = new PIXI.Container();
        centre.position.set( this._kPanelWidth * 0.5, this._kPanelHeight * 0.5 );
        this._go.cContainer.c.addChild( centre );

        // Compass.
        this._compass = new GameObject( [ new CContainer(), new CCompass() ] );
        this._compass.init();
        centre.addChild( this._compass.cContainer.c );

        // Marker.
        let marker: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.find( "compass_marker" ).data );
        marker.anchor.set( 0.5, 1 );
        Utils.game.limitSideSize( 70, marker );
        marker.y = -this._compass.cContainer.c.height * 0.5 + 40;
        centre.addChild( marker );
    }

    public end(): void
    {
        this._compass.end();
        this._compass = null;

        super.end();
    }

    // overrides.
    public saveGame(): ISgCustomPanel
    {
        return { heading: { compass: this._compass.cCompass.saveGame() } };
    }

    // overrides.
    public loadGame( sgCustomPanel: ISgCustomPanel, pass: number ): void
    {
        this._compass.cCompass.loadGame( sgCustomPanel.heading.compass, pass );
    }

    // #endregion //
}