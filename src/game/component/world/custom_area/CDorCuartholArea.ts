import CCustomArea from "../CCustomArea";

import { location_type } from "../CGameWorld";
import ServiceLocator from "../../../../ServiceLocator";
import Utils from "../../../../Utils";
import * as PIXI from "pixi.js";

import { ISgCardToken, ISgCustomPanel, ISgDorCuartholArea, ISgIslandMap } from "../../../../view/game/SaveGameView";
import GameObject from "../../../GameObject";
import CDropArea from "../../input/CDropArea";
import CContainer from "../../pixi/CContainer";
import CIslandLocationHolder from "../../ui/CIslandLocationHolder";
import CLogLocation from "../CLogLocation";
import CardTokenFactory from "../../../CardTokenFactory";
import { action_scope_type } from "../../../../service/socket_io/GameSocketIOController";
import { detail_bar_icon_type } from "../../card/token/CDetailBar";
import CPlayerToken from "../CPlayerToken";
import CDraggable from "../../input/CDraggable";
import CHighlightPoiReceptor from "../poi_receptor/CHighlightPoiReceptor";
import CTooltipReceptor from "../../ui/CTooltipReceptor";
import { player_type } from "../CPlayerArea";
import Session from "../../../../Session";
import CShareableGameElement from "../../CShareableGameElement";
import CDorCuartholLocationHolder from "../../ui/CDorCuartholLocationHolder";
import { layer_type } from "../CGameLayerProvider";


export default class CDorCuartholArea extends CCustomArea
{
    // #region Attributes //

    // private:

    private _playerToken: GameObject = null;

    private _arrLocationHolder: Array<GameObject> = null;
    private _playerLocationIndex: number = null;

    // Constants.
    private readonly _kPanelHeight: number = 300;

    // #endregion //

    
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CDorCuartholArea";
    }

    public init(): void
    {
        super.init();

        this._go.cContainer.c.interactive = true;

        this._go.cGraphics.g.lineStyle( 2, 0x000000, 1 );
        this._go.cGraphics.g.beginFill( 0xefe3af, 0.7 );
        const kPanelWidth: number = 1800;
        this._go.cGraphics.g.drawRect( 0, 0, kPanelWidth, this._kPanelHeight );
        this._go.cGraphics.g.endFill();

        this._arrLocationHolder = new Array<GameObject>();
        const kLocationHolderWidth: number = ( kPanelWidth - 60 ) / 7;
        const kLocationHolderHeight: number = this._kPanelHeight - 60;
        let locationHolderPos: PIXI.Point = new PIXI.Point( 30 + kLocationHolderWidth * 0.5, 30 + kLocationHolderHeight * 0.5 );
        for ( let i: number = 0; i < 7; ++i )
        {
            let locationHolder: GameObject = new GameObject( [ new CContainer(), new CDorCuartholLocationHolder(), new CDropArea(), new CLogLocation() ] );
            locationHolder.oid = "dor-cuarthol_location_holder_" + i.toString();
            locationHolder.init();
            locationHolder.cContainer.c.position.copyFrom( locationHolderPos );
            this._go.cContainer.addChild( locationHolder );
            locationHolder.cDorCuartholLocationHolder.onPlayerTokenDropped.add( this.onPlayerToken_Dropped, this );
            //
            this._arrLocationHolder.push( locationHolder );

            locationHolderPos.x += kLocationHolderWidth;
        }

         // Player token.
         this._playerToken = new GameObject( [ new CContainer(), new CPlayerToken(), new CShareableGameElement(), new CDraggable(), new CHighlightPoiReceptor(), new CTooltipReceptor() ] );
         this._playerToken.oid = "player_token";
         this._playerToken.cDraggable.dragShadowTextureCreator = this._playerToken.cPlayerToken;
         this._playerToken.cTooltipReceptor.text = jQuery.i18n( "PLAYER" );
         this._playerToken.init();
    }

    public end(): void
    {
        for ( let locationHolder of this._arrLocationHolder )
        {
            locationHolder.end();
        }
        this._arrLocationHolder = null;

        this._playerToken.end();
        this._playerToken = null;
    }

    public generate(): void
    {
        let arrDorCuartholCard: Array<GameObject> = [];
        let amonRuhCard: GameObject = null;
        let gateOfTheGarthCard: GameObject = null;

        let arrCard: Array<GameObject> = ServiceLocator.game.cGameWorld.findLocation( location_type.SET_ASIDE ).cDeckIndicator.cViewer.cCardView.findItems();
        arrCard = arrCard.concat( ServiceLocator.game.cGameWorld.findLocation( location_type.SAURON_DECK_0 ).cDeckIndicator.cViewer.cCardView.findItems() );
        for ( let card of arrCard )
        {
            if ( card.cCard.front.cardInfo.type_code == "location" )
            {
                card.cCard.isAnimated = false;
                if ( card.cCard.front.cardInfo.code == "801094A" )
                {
                    card.cCard.setFaceDown( action_scope_type.LOCAL );
                    amonRuhCard = card;
                }
                else if ( card.cCard.front.cardInfo.code == "801096" ) 
                {
                    card.cCard.setFaceUp( action_scope_type.LOCAL );
                    gateOfTheGarthCard = card;
                }
                else
                {
                    card.cCard.setFaceUp( action_scope_type.LOCAL );
                    arrDorCuartholCard.push( card );
                }
                card.cCard.isAnimated = true;
            }
        }

        Utils.game.shuffle( arrDorCuartholCard );
        arrDorCuartholCard.push( gateOfTheGarthCard );
        arrDorCuartholCard.push( amonRuhCard );

        let ctf: CardTokenFactory = new CardTokenFactory();
        for ( let locationHolder of this._arrLocationHolder )
        {
            let locationCard: GameObject = arrDorCuartholCard.pop();
            let locationToken: GameObject = ctf.create( locationCard );
            Utils.game.dropUnderTableAndEndCard( locationCard );
            locationToken.cDropArea.isPropagate = true;
            
            locationToken.cCardToken.isAnimated = false;
            locationHolder.cDropArea.forceDrop( locationToken, null, action_scope_type.LOCAL );
            locationToken.cCardToken.isAnimated = true;
        }

        this._arrLocationHolder[ 0 ].cDropArea.forceDrop( this._playerToken, null, action_scope_type.LOCAL );
    }

    // overrides.
    public saveGame(): ISgCustomPanel
    {
        let arrSgCardToken: Array<ISgCardToken> = new Array<ISgCardToken>();
        for ( let locationHolder of this._arrLocationHolder )
        {
            let sgCardToken: ISgCardToken = null;
            const kLocationToken: GameObject = locationHolder.cDorCuartholLocationHolder.locationToken;
            if ( kLocationToken )
            {
                sgCardToken = kLocationToken.cCardToken.saveGame();
            }
            arrSgCardToken.push( sgCardToken );
        }
        
        return { dorCuartholArea : {
            locations: arrSgCardToken,
            playerLocationIndex: this._playerLocationIndex } };
    }

    // overrides.
    public loadGame( sgCustomPanel: ISgCustomPanel, pass: number ): void
    {
        const KSgDorCuartholArea: ISgDorCuartholArea = sgCustomPanel.dorCuartholArea;
        let ctf: CardTokenFactory = new CardTokenFactory();
        let locationIndex: number = 0;
        for ( let locationHolder of this._arrLocationHolder )
        {
            const kSgCardToken: ISgCardToken = KSgDorCuartholArea.locations[ locationIndex ];
            let locationCard: GameObject = GameObject.find( kSgCardToken.oid.substr( 0, kSgCardToken.oid.length - 1 ) );
            //
            let locationToken: GameObject = ctf.create( locationCard );
            Utils.game.dropUnderTableAndEndCard( locationCard );
            locationToken.cCardToken.loadGame( kSgCardToken, pass );
            locationToken.cDropArea.isPropagate = true;
            locationHolder.cDropArea.forceDrop( locationToken, null, action_scope_type.LOCAL );

            ++locationIndex;
        }

        this._arrLocationHolder[ KSgDorCuartholArea.playerLocationIndex ].cDropArea.forceDrop( this._playerToken, null, action_scope_type.LOCAL );
    }

    // #endregion //


    // #region Callbacks //

    private onPlayerToken_Dropped( dorCuartholLocationHolder: GameObject ): void
    {
        let activeLocationToken: GameObject = ServiceLocator.game.cGameWorld.findLocation( location_type.ACTIVE_LOCATION ).cActiveLocationHolder.activeLocationToken;
        if ( activeLocationToken && this._playerLocationIndex != null )
        {
            this._arrLocationHolder[ this._playerLocationIndex ].cDropArea.forceDrop( activeLocationToken, null, action_scope_type.LOCAL );
        }

        this._playerLocationIndex = this._arrLocationHolder.indexOf( dorCuartholLocationHolder );
    }

    // #endregion //
}