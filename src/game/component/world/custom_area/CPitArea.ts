import Component from "../../Component";

import { location_type } from "../CGameWorld";
import { player_type } from "../CPlayerArea";
import ServiceLocator from "../../../../ServiceLocator";
import Utils from "../../../../Utils";
import * as PIXI from "pixi.js";

import CActorArea from "../CActorArea";
import GameObject from "../../../GameObject";
import CContainer from "../../pixi/CContainer";
import CGraphics from "../../pixi/CGraphics";
import CDropArea from "../../input/CDropArea";
import CLogLocation from "../CLogLocation";
import CQuestHolder from "../../ui/CQuestHolder";
import CCustomArea from "../CCustomArea";
import { ISgCustomPanel, ISgPitArea, ISgRiddleArea } from "../../../../view/game/SaveGameView";
import CShareableGameElement from "../../CShareableGameElement";


export default class CPitArea extends CCustomArea
{
    // #region Attributes //

    // private:

    private _actorArea: GameObject = null; 
    private _questHolder: GameObject = null;

    // Constants.
    private readonly _kPanelWidth: number = 1690;
    private readonly _kPanelHeight: number = 264;

    // #endregion //


    // #region Properties //

    public get actorArea(): GameObject { return this._actorArea; }
    public get cActorArea(): CActorArea { return this._actorArea.cActorArea; }
    public get questHolder(): GameObject { return this._questHolder; }
    public get cQuestHolder(): CQuestHolder { return this._questHolder.cQuestHolder; }

    // #endregion //

    
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CPitArea";
    }

    public init(): void
    {
        super.init();
        
        this._go.cContainer.c.interactive = true;

        this._go.cGraphics.g.lineStyle( 2, 0x000000, 1 );
        this._go.cGraphics.g.beginFill( 0xefe3af, 0.7 );
        this._go.cGraphics.g.drawRect( 0, 0, this._kPanelWidth, this._kPanelHeight );
        this._go.cGraphics.g.endFill();

        this._actorArea = this.createActorArea();
        this._actorArea.cContainer.c.position.set( 30 + 180 + 50, 30 );
        this._go.cContainer.addChild( this._actorArea );

        this._questHolder = this.createQuestHolder();
        this._questHolder.cContainer.c.position.set( 30 + 90, this._actorArea.cContainer.c.y + this._actorArea.cContainer.c.height * 0.5 );
        this._go.cContainer.addChild( this._questHolder );
    }

        private createActorArea(): GameObject
        {
            let actorArea: GameObject = new GameObject( [ new CGraphics(), new CActorArea(), new CDropArea(), new CLogLocation() ] );
            actorArea.oid = this._go.oid + "_actor_area";
            actorArea.cActorArea.location = location_type.PIT_ACTOR_AREA;
            actorArea.cActorArea.width = 1400;
            actorArea.cActorArea.lineColor = ServiceLocator.game.playerColors[ player_type.SAURON ];
            actorArea.cActorArea.padding = new PIXI.Point( 35, 5 );
            actorArea.cActorArea.actorMargin = 30;
            actorArea.cDropArea.target = actorArea.cActorArea;
            actorArea.cDropArea.isSelfDropAllowed = true;
            actorArea.cLogLocation.location = location_type.PIT_ACTOR_AREA;
            actorArea.init();

            let icon: PIXI.Sprite = new PIXI.Sprite( PIXI.Texture.from( ServiceLocator.resourceStack.find( "riddle" ).data ) );
            Utils.game.limitSideSize( 30, icon );
            icon.position.set( 5 );
            actorArea.cContainer.c.addChildAt( icon, 1 );

            return actorArea;
        }

        private createQuestHolder(): GameObject
        {
            let holder: GameObject = new GameObject( [ new CContainer(), new CQuestHolder(), new CDropArea(), new CLogLocation() ] );
            holder.oid = this._go.oid + "_quest_holder";
            holder.cQuestHolder.location = location_type.PIT_QUEST;
            holder.cDropArea.target = holder.cQuestHolder;
            holder.cLogLocation.location = location_type.PIT_QUEST;
            holder.init();

            return holder;
        }

    public end(): void
    {
        this._actorArea.end();
        this._actorArea = null;

        this._questHolder.end();
        this._questHolder = null;
    }

    // overrides.
    public saveGame(): ISgCustomPanel
    {
        return { pitArea: { 
            actorArea: this._actorArea.cActorArea.saveGame(),
            questHolder: this._questHolder.cQuestHolder.saveGame() } };
    }

    // overrides.
    public loadGame( sgCustomPanel: ISgCustomPanel, pass: number ): void
    {
        const kSgPitArea: ISgPitArea = sgCustomPanel.pitArea;
        this._actorArea.cActorArea.loadGame( kSgPitArea.actorArea, pass );
        this._questHolder.cQuestHolder.loadGame( kSgPitArea.questHolder, pass );
    }

    // #endregion //
}