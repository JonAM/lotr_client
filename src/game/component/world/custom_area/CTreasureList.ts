import CCustomArea from "../CCustomArea";

import { player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../../ServiceLocator";
import Utils from "../../../../Utils";
import * as PIXI from "pixi.js";

import { ISgCustomPanel } from "../../../../view/game/SaveGameView";
import GameObject from "../../../GameObject";
import { IOppActionListener, IOpponentAction } from "../../../AllyActionManager";
import CButton from "../../input/CButton";
import SignalBinding from "../../../../lib/signals/SignalBinding";
import CSprite from "../../pixi/CSprite";
import { ICard } from "../../../CardDB";


export default class CTreasureList extends CCustomArea implements IOppActionListener
{
    // #region Attributes //

    // private:

    private _arrCheckedIndex: Array<number> = null;

    private _arrCheckBtn: Array<GameObject> = null;
    private _arrCheckBtnState: Array<boolean> = null;

    // Constants.
    private readonly _kPanelWidth: number = 500;

    // #endregion //


    public set checkedIndexes( value: Array<number> ) { this._arrCheckedIndex = value; }

    
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CTreasureList";
    }

    public init(): void
    {
        super.init();

        this._arrCheckBtn = new Array<GameObject>();
        this._arrCheckBtnState = new Array<boolean>();
        
        this._go.cContainer.c.interactive = true;

        // Title.
        let text: PIXI.Text = new PIXI.Text( jQuery.i18n( "TREASURE_LIST" ), ServiceLocator.game.textStyler.title );
        text.anchor.x = 0.5;
        text.position.set( this._kPanelWidth * 0.5, 30 );
        let nextTextY: number = text.y + text.height;
        this._go.cContainer.c.addChild( text );
        
        let arrTreasureCard: Array<ICard> = ServiceLocator.cardDb.findByTypeCodes( [ "treasure" ] );
        let curTreasureSet: string = null;
        for ( let treasureCard of arrTreasureCard )
        {
            // Header.
            if ( !curTreasureSet || curTreasureSet != treasureCard.treasure_set )
            {
                curTreasureSet = treasureCard.treasure_set;

                text = new PIXI.Text( curTreasureSet, ServiceLocator.game.textStyler.subtitle );
                text.position.set( 30, nextTextY + 40 );
                nextTextY = text.y + text.height;
                this._go.cContainer.c.addChild( text );
            }

            // List item.
            // Checkbox.
            let checkBtn: GameObject = this.createCheckBtn();
            checkBtn.cContainer.c.position.set( 60, nextTextY + 20 );
            nextTextY = checkBtn.cContainer.c.y + checkBtn.cContainer.c.height;
            this._go.cContainer.addChild( checkBtn );
            let sb: SignalBinding = checkBtn.cButton.onClick.add( this.onListItemBtn_Click, this );
            sb.params = [ this._arrCheckBtn.length ];
            //
            this._arrCheckBtn.push( checkBtn );
            this._arrCheckBtnState.push( false );
            // Text.
            text = new PIXI.Text( treasureCard.name, ServiceLocator.game.textStyler.normal );
            text.anchor.y = 0.5;
            text.position.set( 
                checkBtn.cContainer.c.x + checkBtn.cContainer.c.width + 20, 
                checkBtn.cContainer.c.y + checkBtn.cContainer.c.height * 0.5 )
            this._go.cContainer.c.addChild( text );
        }

        // Bg.
        this._go.cGraphics.g.lineStyle( 2, 0x000000, 1 );
        this._go.cGraphics.g.beginFill( 0xefe3af, 0.7 );
        this._go.cGraphics.g.drawRect( 0, 0, this._kPanelWidth, this._go.cContainer.c.height + 60 );
        this._go.cGraphics.g.endFill();

        // Init list values.
        if ( this._arrCheckedIndex )
        {
            for ( let treasureIndex of this._arrCheckedIndex )
            {
                this.toggleListItem( treasureIndex );
            }
        }

        // Listen to events.
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.TOGGLE_TREASURE_LIST_ITEM ] );
    }

        private createCheckBtn(): GameObject
        {
            let result: GameObject = new GameObject( [ new CSprite(), new CButton() ] );
            result.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "unchecked" );
            Utils.game.limitSideSize( 25, result.cSprite.s );
            result.init();

            return result;
        }

    public end(): void
    {
        // Cleanup events.
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.TOGGLE_TREASURE_LIST_ITEM ] );

        for ( let checkBtn of this._arrCheckBtn )
        {
            checkBtn.end();
        }

        super.end();
    }

    public findCheckedIndexes(): Array<number>
    {
        let result: Array<number> = new Array<number>();

        for ( let i: number = 0; i < this._arrCheckBtnState.length; ++i )
        {
            if ( this._arrCheckBtnState[ i ] )
            {
                result.push( i );
            }
        }

        return result;
    }

    // overrides.
    public saveGame(): ISgCustomPanel
    {
        return { treasure_list: { checked_items: this.findCheckedIndexes() } };
    }

    // overrides.
    public loadGame( sgCustomPanel: ISgCustomPanel, pass: number ): void
    {
        for ( let index of sgCustomPanel.treasure_list.checked_items )
        {
            this.toggleListItem( index );
        }
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.TOGGLE_TREASURE_LIST_ITEM )
        {
            this.toggleListItem( action.args[ 0 ] as number );
        }
    }

        private toggleListItem( index: number ): void
        {
            this._arrCheckBtnState[ index ] = !this._arrCheckBtnState[ index ];

            let checkBtn: GameObject = this._arrCheckBtn[ index ];
            if ( this._arrCheckBtnState[ index ] )
            {
                checkBtn.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "checked" );
            }
            else
            {
                checkBtn.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "unchecked" );
            }
            Utils.game.limitSideSize( 25, checkBtn.cSprite.s );
        }

    // #endregion //


    // #region Input Callbacks //

    private onListItemBtn_Click( index: number ): void
    {
        this.toggleListItem( index );

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.TOGGLE_TREASURE_LIST_ITEM, null, [ index ] );
    }

    // #endregion //
}