import Component from "../Component";

import GameObject from "../../GameObject";
import { target_selection_type } from "../input/CTargetSelector";


export default class CTargetReceptor extends Component
{
    // #region Attributes //

    // private:

    private _arrValidTargetSelection: Array<target_selection_type> = null;

    private _targetSocket: GameObject = null;

    // #endregion //


    // #region Properties //
 
    public get targetSocket(): GameObject { return this._targetSocket; }

    public set targetSocket( value: GameObject ) { this._targetSocket = value; }
    public set validTargetSelections( value: Array<target_selection_type> ) { this._arrValidTargetSelection = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CTargetReceptor";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._targetSocket != null, "CTargetReceptor.ts :: init() :: this._targetSocket cannot be null." );
        console.assert( this._arrValidTargetSelection != null, "CTargetReceptor.ts :: init() :: this._arrValidTargetSelection cannot be null." );
    }

    public end(): void
    {
        this._targetSocket = null;
        this._arrValidTargetSelection = null;

        super.end();
    }

    public validate( targetSelectionType: target_selection_type ): boolean
    {
        return this._arrValidTargetSelection.indexOf( targetSelectionType ) != -1;
    }

    public onFocusReceived( from: GameObject ): void
    {
        // TODO:
    }

    public onFocusLost( from: GameObject ): void
    {
        // TODO:
    }

    // #endregion //
}