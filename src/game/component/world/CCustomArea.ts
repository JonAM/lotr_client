import { ISgCustomPanel } from "../../../view/game/SaveGameView";
import Component from "../Component";


export default abstract class CCustomArea extends Component
{
    
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CCustomArea";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CCustomArea.ts :: init() :: CContainer component not found." );
    }

    // virtual.
    public onShown(): void {}

    // virtual.
    public onHidden(): void {}

    // virtual.
    public abstract saveGame(): ISgCustomPanel;

    // virtual.
    public abstract loadGame( sgCustomPanel: ISgCustomPanel, pass: number ): void;

    // #endregion //
}