import Component from "../Component";

import { player_type } from "./CPlayerArea";
import ServiceLocator from "../../../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CContainer from "../pixi/CContainer";
import CPlayerArea from "./CPlayerArea";
import CDraggable from "../input/CDraggable";
import Utils from "../../../Utils";
import CButton from "../input/CButton";
import CGraphics from "../pixi/CGraphics";
import CActionLogger from "../ui/right_menu/CActionLogger";
import { ISgCustomPanel, ISgGameWorld } from "../../../view/game/SaveGameView";
import CTooltipReceptor from "../ui/CTooltipReceptor";
import CSauronArea from "./CSauronArea";
import CSprite from "../pixi/CSprite";
import COptionsMenu from "../ui/COptionsMenu";
import CPhaseDiagramButton from "../ui/CPhaseDiagramButton";
import { action_scope_type } from "../../../service/socket_io/GameSocketIOController";
import CFirstPlayerTokenPoiReceptor from "./poi_receptor/CFirstPlayerTokenPoiReceptor";
import CFirstPlayerToken from "./CFirstPlayerToken";
import Session from "../../../Session";
import { IScenario } from "../../ScenarioDB";
import CGameLayerProvider, { layer_type } from "./CGameLayerProvider";
import CCustomPanelManager from "../ui/CCustomPanelManager";
import CustomPanelFactory, { ICustomPanel } from "../../CustomPanelFactory";


export default class CGameWorld extends Component
{
    // #region Attributes //

    // private:

    private _playerArea: GameObject = null;
    private _allyArea: GameObject = null;
    private _sauronArea: GameObject = null;
    private _actionLogger: GameObject = null;
    private _optionsBtn: GameObject = null;
    private _optionsMenu: GameObject = null;
    private _firstPlayerToken: GameObject = null;
    private _customPanelManager: GameObject = null;

    // #endregion //


    // #region Properties //

    public get playerArea(): GameObject { return this._playerArea; }
    public get cPlayerArea(): CPlayerArea { return this._playerArea.cPlayerArea; }
    public get allyArea(): GameObject { return this._allyArea; }
    public get cAllyArea(): CPlayerArea { return this._allyArea.cPlayerArea; }
    public get sauronArea(): GameObject { return this._sauronArea; }
    public get cSauronArea(): CSauronArea { return this._sauronArea.cSauronArea; }
    public get cActionLogger(): CActionLogger { return this._actionLogger.cActionLogger; }
    public get cOptionsMenu(): COptionsMenu { return this._optionsMenu.cOptionsMenu; }
    public get firstPlayerToken(): GameObject { return this._firstPlayerToken; }
    public get customPanelManager(): GameObject { return this._customPanelManager; }
    public get cCustomPanelManager(): CCustomPanelManager { return this._customPanelManager.cCustomPanelManager; }

    // #endregion //

    
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CGameWorld";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cSprite != null, "CGameWorld.ts :: init() :: CSprite component not found." );
        
        this._go.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "table_bg" ).data );
        
        // Action logger.
        this._actionLogger = new GameObject( [ new CContainer(), new CActionLogger() ] );
        this._actionLogger.init();
        ServiceLocator.game.root.cGameLayerProvider.add( this._actionLogger, layer_type.HUD );
        this._actionLogger.cContainer.c.position.set( 
            ServiceLocator.game.app.screen.width - this._actionLogger.cContainer.c.width,
            ( ServiceLocator.game.app.screen.height - this._actionLogger.cContainer.c.height ) * 0.5 );
        
        // Player area.
        // Ally.
        this._allyArea = new GameObject( [ new CContainer(), new CPlayerArea() ] );
        this._allyArea.oid = "player_area_" + GameObject.findPlayerSufix( player_type.ALLY );
        this._allyArea.cPlayerArea.playerType = player_type.ALLY;
        this._allyArea.init();
        this._go.cContainer.addChild( this._allyArea );
        this._allyArea.cContainer.c.position.set( 0, 0 );
        if ( !Session.allyId )
        {
            this._allyArea.cPlayerArea.setEnabled( false );
        }
        // Player.
        this._playerArea = new GameObject( [ new CContainer(), new CPlayerArea() ] );
        this._playerArea.oid = "player_area_" + GameObject.findPlayerSufix( player_type.PLAYER );
        this._playerArea.cPlayerArea.playerType = player_type.PLAYER;
        this._playerArea.init();
        this._go.cContainer.addChild( this._playerArea );
        this._playerArea.cContainer.c.position.set( 0, 652 );

        // Sauron area.
        this._sauronArea = new GameObject( [ new CContainer(), new CSauronArea() ] );
        this._sauronArea.oid = "sauron_area";
        this._sauronArea.init();
        this._go.cContainer.addChild( this._sauronArea );

        this._playerArea.cPlayerArea.cSideControls.cEndTurnBtn.postInit();
        this._allyArea.cPlayerArea.cSideControls.cEndTurnBtn.postInit();

        // First player token.
        this._firstPlayerToken = new GameObject( [ new CContainer(), new CFirstPlayerToken(), new CDraggable(), new CFirstPlayerTokenPoiReceptor(), new CTooltipReceptor() ] );
        this._firstPlayerToken.oid = "first_player";
        this._firstPlayerToken.cDraggable.dragShadowTextureCreator = this._firstPlayerToken.cFirstPlayerToken;
        this._firstPlayerToken.cTooltipReceptor.text = jQuery.i18n( "FIRST_PLAYER_TOKEN" );
        this._firstPlayerToken.init();
        ServiceLocator.game.root.cGameLayerProvider.add( this._firstPlayerToken, layer_type.UNDER_TABLE );
        // Set first player.
        let firstPlayerPlayArea: GameObject = ServiceLocator.game.firstPlayer == player_type.PLAYER ? this._playerArea : this._allyArea;
        firstPlayerPlayArea.cPlayerArea.sideControls.cPlayerSideControls.go.cDropArea.forceDrop( this._firstPlayerToken, null, action_scope_type.LOCAL );
   
        this._customPanelManager = new GameObject( [ new CContainer(), new CCustomPanelManager() ] );
        const kScenario: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
        if ( kScenario.play_area && kScenario.play_area.custom_panels )
        {
            let cpf: CustomPanelFactory = new CustomPanelFactory();
            let arrCustomPanel: Array<ICustomPanel> = new Array<ICustomPanel>();
            for ( let customPanelId of kScenario.play_area.custom_panels )
            {
                if ( customPanelId != "campaign_log"
                    || customPanelId == "campaign_log" && Session.isCampaignMode )
                {
                    arrCustomPanel.push( cpf.create( customPanelId ) );
                }
            }
            this._customPanelManager.cCustomPanelManager.panels = arrCustomPanel;
        }
        this._customPanelManager.init();
        if ( this._customPanelManager.cCustomPanelManager.find( "isolated" ) )
        {
            this._customPanelManager.cCustomPanelManager.setPanelVisibility( "isolated", false );
        }
        if ( this._customPanelManager.cCustomPanelManager.find( "split_sauron" ) )
        {
            this._customPanelManager.cCustomPanelManager.setPanelVisibility( "split_sauron", false );
        }
        this._customPanelManager.cContainer.c.visible = false;
        ServiceLocator.game.root.cGameLayerProvider.add( this._customPanelManager, layer_type.CUSTOM_PANEL );

        // Options button.
        this._optionsBtn = new GameObject( [ new CSprite(), new CButton(), new CTooltipReceptor() ] );
        this._optionsBtn.cSprite.s.anchor.set( 1, 0 );
        this._optionsBtn.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "options" );
        Utils.game.limitSideSize( 30, this._optionsBtn.cSprite.s );
        this._optionsBtn.cTooltipReceptor.text = jQuery.i18n( "OPTIONS" );
        this._optionsBtn.init();
        this._optionsBtn.cContainer.c.position.set( ServiceLocator.game.app.screen.width - 5, 5 );
        ServiceLocator.game.root.cGameLayerProvider.add( this._optionsBtn, layer_type.HUD );
        this._optionsBtn.cButton.onClick.add( this.onOptionsBtn_Click, this );

        // Options menu.
        this._optionsMenu = new GameObject( [ new CGraphics(), new COptionsMenu() ] );
        this._optionsMenu.init();
        this._optionsMenu.cContainer.c.visible = false;
        this._optionsMenu.cContainer.c.position.set( this._optionsBtn.cContainer.c.x - this._optionsBtn.cContainer.c.width - 10 - this._optionsMenu.cContainer.c.width, 5 );
        ServiceLocator.game.root.cGameLayerProvider.add( this._optionsMenu, layer_type.HUD );
    }

    public end(): void
    {
        this._playerArea.end();
        this._playerArea = null;
        
        this._allyArea.end();
        this._allyArea = null;

        this._sauronArea.end();
        this._sauronArea = null;

        this._actionLogger.end();
        this._actionLogger = null;

        this._optionsBtn.end();
        this._optionsBtn = null;

        this._optionsMenu.end();
        this._optionsMenu = null;

        this._customPanelManager.end();
        this._customPanelManager = null;

        ( this._firstPlayerToken.cContainer.c.getChildByName( "poi_socket" )[ "go" ] as GameObject ).end();
        this._firstPlayerToken.end();
        this._firstPlayerToken = null;
    }

    public findLocation( locationType: location_type ): GameObject
    {
        let result: GameObject = null;

        switch ( locationType )
        {
            case location_type.MY_DECK: { result = this._playerArea.cPlayerArea.cSideControls.deck; break; }
            case location_type.MY_CUSTOM_DECK: { result = this._playerArea.cPlayerArea.cSideControls.customDeck; break; }
            case location_type.MY_DISCARD: { result = this._playerArea.cPlayerArea.cSideControls.discard; break; }
            case location_type.MY_HOME: { result = this._playerArea.cPlayerArea.home; break; }
            case location_type.MY_ENGAGED: { result = this._playerArea.cPlayerArea.engaged; break; }
            case location_type.MY_HAND: { result = this._playerArea.cPlayerArea.cSideControls.hand; break; }
            case location_type.MY_SET_ASIDE: { result = this._playerArea.cPlayerArea.cSideControls.setAside; break; }
            case location_type.MY_THREAT_LEVEL: { result = this._playerArea.cPlayerArea.cSideControls.threatLevel; break; }
            case location_type.ALLY_DECK: { result = this._allyArea.cPlayerArea.cSideControls.deck; break; }
            case location_type.ALLY_CUSTOM_DECK: { result = this._allyArea.cPlayerArea.cSideControls.customDeck; break; }
            case location_type.ALLY_DISCARD: { result = this._allyArea.cPlayerArea.cSideControls.discard; break; }
            case location_type.ALLY_HOME: { result = this._allyArea.cPlayerArea.home; break; }
            case location_type.ALLY_ENGAGED: { result = this._allyArea.cPlayerArea.engaged; break; }
            case location_type.ALLY_HAND: { result = this._allyArea.cPlayerArea.cSideControls.hand; break; }
            case location_type.ALLY_SET_ASIDE: { result = this._allyArea.cPlayerArea.cSideControls.setAside; break; }
            case location_type.ALLY_THREAT_LEVEL: { result = this._allyArea.cPlayerArea.cSideControls.threatLevel; break; }
            case location_type.VICTORY_DISPLAY: { result = this._sauronArea.cSauronArea.cSideControls.victoryDisplay; break; }
            case location_type.REMOVED_FROM_GAME: { result = this._sauronArea.cSauronArea.cSideControls.removedFromGame; break; }
            case location_type.SAURON_DECK_0: { result = this._sauronArea.cSauronArea.cSideControls.deck; break; }
            case location_type.SAURON_DECK_1: { result = this._sauronArea.cSauronArea.cSideControls.secondaryDeck; break; }
            case location_type.SAURON_DISCARD: { result = this._sauronArea.cSauronArea.cSideControls.discard; break; }
            case location_type.SAURON_CUSTOM_DECK_0: { result = this._sauronArea.cSauronArea.cSideControls.customDeck; break; }
            case location_type.SAURON_CUSTOM_DISCARD_0: { result = this._sauronArea.cSauronArea.cSideControls.customDiscard; break; }
            case location_type.SAURON_CUSTOM_DECK_1: { result = this._sauronArea.cSauronArea.cSideControls.secondaryCustomDeck; break; }
            case location_type.SAURON_CUSTOM_DISCARD_1: { result = this._sauronArea.cSauronArea.cSideControls.secondaryCustomDiscard; break; }
            case location_type.SET_ASIDE: { result = this._sauronArea.cSauronArea.cSideControls.setAside; break; }
            case location_type.QUEST_DECK: { result = this._sauronArea.cSauronArea.cSideControls.questDeck; break; }
            case location_type.QUEST_DISCARD: { result = this._sauronArea.cSauronArea.cSideControls.questDiscard; break; }
            case location_type.QUEST: { result = this._sauronArea.cSauronArea.questHolder; break; }
            case location_type.ACTIVE_LOCATION: { result = this._sauronArea.cSauronArea.activeLocationHolder; break; }
            case location_type.STAGING_AREA: { result = this._sauronArea.cSauronArea.staging; break; }
            case location_type.RIDDLE_ACTOR_AREA: { result = this._customPanelManager.cCustomPanelManager.find( "riddle" ).cRiddleArea.actorArea; break; }
            case location_type.RIDDLE_QUEST: { result = this._customPanelManager.cCustomPanelManager.find( "riddle" ).cRiddleArea.questHolder; break; }
            case location_type.PIT_ACTOR_AREA: { result = this._customPanelManager.cCustomPanelManager.find( "pit" ).cPitArea.actorArea; break; }
            case location_type.PIT_QUEST: { result = this._customPanelManager.cCustomPanelManager.find( "pit" ).cPitArea.questHolder; break; }
            case location_type.PURSUIT_STAGING_AREA: { result = this._customPanelManager.cCustomPanelManager.find( "pursuit" ).cPursuitArea.staging; break; }
            case location_type.PURSUIT_QUEST: { result = this._customPanelManager.cCustomPanelManager.find( "pursuit" ).cPursuitArea.questHolder; break; }
            case location_type.ISOLATED_HOME: { result = this._customPanelManager.cCustomPanelManager.find( "isolated" ).cIsolatedArea.home; break; }
            case location_type.ISOLATED_ENGAGED: { result = this._customPanelManager.cCustomPanelManager.find( "isolated" ).cIsolatedArea.engaged; break; }
            case location_type.ISOLATED_STAGING_AREA: { result = this._customPanelManager.cCustomPanelManager.find( "isolated" ).cIsolatedArea.staging; break; }
            case location_type.ISOLATED_QUEST: { result = this._customPanelManager.cCustomPanelManager.find( "isolated" ).cIsolatedArea.questHolder; break; }
            case location_type.ISOLATED_ACTIVE_LOCATION: { result = this._customPanelManager.cCustomPanelManager.find( "isolated" ).cIsolatedArea.activeLocationHolder; break; }
            case location_type.SPLIT_SAURON_STAGING_AREA: { result = this._customPanelManager.cCustomPanelManager.find( "split_sauron" ).cSplitSauronArea.staging; break; }
            case location_type.SPLIT_SAURON_QUEST: { result = this._customPanelManager.cCustomPanelManager.find( "split_sauron" ).cSplitSauronArea.questHolder; break; }
            case location_type.SPLIT_SAURON_ACTIVE_LOCATION: { result = this._customPanelManager.cCustomPanelManager.find( "split_sauron" ).cSplitSauronArea.activeLocationHolder; break; }
        }

        return result;
    }

    // overrides.
    public saveGame(): ISgGameWorld
    {
        let result: ISgGameWorld = {
            playerPlayArea: this._playerArea.cPlayerArea.saveGame(),
            allyPlayArea: this._allyArea.cPlayerArea.saveGame(),
            sauronPlayArea: this._sauronArea.cSauronArea.saveGame(),
            actionLogger: this._actionLogger.cActionLogger.saveGame() };

        result.customPanelManager = this._customPanelManager.cCustomPanelManager.saveGame();
        
        return result;
    }

    // overrides.
    public loadGame( sgGameWorld: ISgGameWorld, pass: number ): void
    {
        this._playerArea.cPlayerArea.loadGame( sgGameWorld.playerPlayArea, pass );
        this._allyArea.cPlayerArea.loadGame( sgGameWorld.allyPlayArea, pass );
        this._sauronArea.cSauronArea.loadGame( sgGameWorld.sauronPlayArea, pass );
        this._actionLogger.cActionLogger.loadGame( sgGameWorld.actionLogger, pass );
        if ( sgGameWorld.customPanelManager )
        {
            this._customPanelManager.cCustomPanelManager.loadGame( sgGameWorld.customPanelManager, pass );
        }
    }

    // #endregion //


    // #region Input Callbacks //

    private onOptionsBtn_Click(): void
    {
        this._optionsMenu.cContainer.c.visible = !this._optionsMenu.cContainer.c.visible;
    }

    // #endregion //
}

export const enum location_type
{
    UNDER_TABLE = 0,
    MY_DECK,
    MY_CUSTOM_DECK,
    MY_DISCARD,
    MY_HOME,
    MY_ENGAGED,
    MY_HAND,
    MY_SET_ASIDE,
    MY_UNDERNEATH,
    MY_THREAT_LEVEL,
    ALLY_DECK,
    ALLY_CUSTOM_DECK,
    ALLY_DISCARD,
    ALLY_HOME,
    ALLY_ENGAGED,
    ALLY_HAND,
    ALLY_SET_ASIDE,
    ALLY_UNDERNEATH,
    ALLY_THREAT_LEVEL,
    VICTORY_DISPLAY,
    REMOVED_FROM_GAME,
    SAURON_DECK_0,
    SAURON_DECK_1,
    SAURON_DISCARD,
    SAURON_CUSTOM_DECK_0,
    SAURON_CUSTOM_DISCARD_0,
    SAURON_CUSTOM_DECK_1,
    SAURON_CUSTOM_DISCARD_1,
    QUEST,
    QUEST_DECK,
    QUEST_DISCARD,
    SET_ASIDE,
    CARD_TRAY,
    ACTIVE_LOCATION,
    ISLAND_LOCATION,
    DOR_CUARTHOL_LOCATION,
    STAGING_AREA,
    RIDDLE_ACTOR_AREA,
    RIDDLE_QUEST,
    PIT_ACTOR_AREA,
    PIT_QUEST,
    PURSUIT_STAGING_AREA,
    PURSUIT_QUEST,
    ISOLATED_HOME,
    ISOLATED_ENGAGED,
    ISOLATED_STAGING_AREA,
    ISOLATED_ACTIVE_LOCATION,
    ISOLATED_QUEST,
    SPLIT_SAURON_STAGING_AREA,
    SPLIT_SAURON_ACTIVE_LOCATION,
    SPLIT_SAURON_QUEST
}