import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CContainer from "../pixi/CContainer";
import IDragShadowTextureCreator from "../../IDragShadowTextureCreator";
import { player_type } from "./CPlayerArea";


export default class CPlayerToken extends Component implements IDragShadowTextureCreator
{
    // #region Attributes //

    // private:

    private _poiSocket: GameObject = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CPlayerToken";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CPlayerToken.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cHighlightPoiReceptor != null, "CPlayerToken.ts :: init() :: CHighlightPoiReceptor component not found." );

        // Shadow.
        /*let shadow: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( "shadow_first_player" ) );
        shadow.anchor.set( 0.45 );
        this._go.cContainer.c.addChild( shadow );*/

        // Frame.
        let frame: PIXI.Graphics = new PIXI.Graphics();
        frame.lineStyle( 2 );
        frame.beginFill( ServiceLocator.game.playerColors[ player_type.PLAYER ] );
        frame.drawCircle( 0, 0, 45 );
        frame.endFill();
        this._go.cContainer.c.addChild( frame );

        // Sprite.
        let sprite: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.find( "player" ).data );
        sprite.name = "sprite";
        Utils.game.limitSideSize( 80, sprite );
        sprite.anchor.set( 0.5 );
        this._go.cContainer.c.addChild( sprite );

        // Border.
        let border: PIXI.Graphics = new PIXI.Graphics();
        border.lineStyle( 2 );
        frame.drawCircle( 0, 0, 40 );
        this._go.cContainer.c.addChild( border );

        // POI socket.
        this._poiSocket = new GameObject( [ new CContainer() ] );
        this._poiSocket.cContainer.c.name = "poi_socket";
        this._poiSocket.init();
        this._poiSocket.cContainer.c.y -= sprite.height * 0.5 - 5;
        this._go.cContainer.addChild( this._poiSocket );
        
        this._go.cHighlightPoiReceptor.poiSocket = this._poiSocket;
    }

    public end(): void
    {
        this._poiSocket.end();
        this._poiSocket = null;

        super.end();
    }

    // #endregion //


    // #region IDragShadowTextureCreator //

    public createDragShadowTexture(): PIXI.Texture
    {
        let dragShadowTexture = PIXI.RenderTexture.create( { width: this._go.cContainer.c.width, height: this._go.cContainer.c.height } );
        let pos: PIXI.Point = new PIXI.Point();
        this._go.cContainer.c.position.copyTo( pos );
        this._go.cContainer.c.position.set( this._go.cContainer.c.width * 0.5, this._go.cContainer.c.height * 0.5 );
        ServiceLocator.game.app.renderer.render( this._go.cContainer.c, dragShadowTexture );    
        pos.copyTo( this._go.cContainer.c.position );

        return dragShadowTexture;
    }

    // #endregion //
}
