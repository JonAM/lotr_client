import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import { gsap } from "gsap";
import * as PIXI from "pixi.js";
import * as particles from "pixi-particles";
import Signal from "../../../lib/signals/Signal";


export default class CFlyingOrb extends Component
{
    // #region Attributes //

    // private:

    private _color: number = null;
    private _target: PIXI.Point = null;

    private _moveDir: PIXI.Point = null;
    private _moveSpeed: number = null;
    private _seekSpeed: number = null;
    private _seekDir: number = null;
    private _attractSpeed: number = null;

    private _emitter: particles.Emitter = null;

    // Signals.
    private _onHit: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get target(): PIXI.Point { return this._target; }

    public set color( value: number ) { this._color = value; }
    public set target( value: PIXI.Point ) { this._target = value; }

    // Signals.
    public get onHit(): Signal { return this._onHit; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CFlyingOrb";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CFlyingOrb.ts :: init() :: CContainer component not found." );
        console.assert( this._color != null, "CFlyingOrb.ts :: init() :: this._color cannot be null." );
        console.assert( this._target != null, "CFlyingOrb.ts :: init() :: this._target cannot be null." );

        this._seekSpeed = 1;

        this._emitter = this.createEmitter();
    }

        private createEmitter(): particles.Emitter
        {
            let emitter: particles.Emitter = new particles.Emitter(
                this._go.cContainer.c,
                [ ServiceLocator.resourceStack.findAsTexture( "particle_default" ) ],
                ServiceLocator.resourceStack.find( "emitter_flying_orb" ).data );
            let arrColor: Array<number> = new Array<number>();
            PIXI.utils.hex2rgb( this._color, arrColor );
            emitter.startColor.value.r = arrColor[ 0 ] * 255;
            emitter.startColor.value.g = arrColor[ 1 ] * 255;
            emitter.startColor.value.b = arrColor[ 2 ] * 255;
            emitter.autoUpdate = true;
            emitter.emit = false;

            return emitter;
        }

    public end(): void
    {
        this._onHit.removeAll();

        this._emitter.emit = false;
        let tryEmitterDestruction: Function = () => {
            if ( this._emitter.particleCount == 0 )
            {
                this._emitter.destroy();
                this._emitter = null;
            }
            else
            {
                window.setTimeout( tryEmitterDestruction, 300 );
            }
        };
        tryEmitterDestruction();

        super.end();
    }

    public update( dt: number ): void
    {
        let pos: PIXI.Point = this._emitter.spawnPos.clone();

        const kMoveDistance: number = dt * this._moveSpeed;
        pos.x += this._moveDir.x * kMoveDistance;
        pos.y += this._moveDir.y * kMoveDistance;

        let targetDir: PIXI.Point = this.calcUnitVector( new PIXI.Point(
            this._target.x - pos.x,
            this._target.y - pos.y ) );
        const kMagneticDistance: number = dt * this._attractSpeed;
        pos.x += targetDir.x * kMagneticDistance;
        pos.y += targetDir.y * kMagneticDistance;

        let mMoveDir: PIXI.Matrix = new PIXI.Matrix();
        mMoveDir.translate( this._moveDir.x, this._moveDir.y );
        mMoveDir.rotate( this._seekSpeed * this._seekDir );
        this._moveDir.x = mMoveDir.tx;
        this._moveDir.y = mMoveDir.ty;

        this._emitter.updateSpawnPos( pos.x, pos.y );

        let newTargetDir: PIXI.Point = this.calcUnitVector( new PIXI.Point( 
            this._target.x - pos.x, 
            this._target.y - pos.y ) );
        if ( targetDir.x * newTargetDir.x + targetDir.y * newTargetDir.y == -1 )
        {
            PIXI.Ticker.shared.remove( this.update, this );

            this._onHit.dispatch();

            this.end();
        }
    }

        private calcUnitVector( vector: PIXI.Point ): PIXI.Point
        {
            let result: PIXI.Point = vector.clone();

            const kTargetDirMod: number = Math.sqrt( result.x * result.x + result.y * result.y );
            result.x /= kTargetDirMod;
            result.y /= kTargetDirMod;

            return result;
        }

    public launch( lp: ILaunchParams ): void
    {
        this._moveDir = lp.moveDir;
        this._moveSpeed = lp.moveSpeed;
        if ( lp.moveTween )
        {
            gsap.to( this, { _moveSpeed: lp.moveTween.value, duration: lp.moveTween.duration, ease: lp.moveTween.ease, delay: lp.moveTween.delay } );
        }
        this._seekSpeed = lp.seekSpeed;
        if ( lp.seekTween )
        {
            gsap.to( this, { _seekSpeed: lp.seekTween.value, duration: lp.seekTween.duration, ease: lp.seekTween.ease, delay: lp.seekTween.delay } );
        }
        this._attractSpeed = lp.attractSpeed;
        if ( lp.attractTween )
        {
            gsap.to( this, { _attractSpeed: lp.attractTween.value, duration: lp.attractTween.duration, ease: lp.attractTween.ease, delay: lp.attractTween.delay } );
        }

        this._emitter.updateSpawnPos( lp.from.x, lp.from.y );
        this._emitter.emit = true;

        let targetDir: PIXI.Point = this.calcUnitVector( new PIXI.Point(
            this._target.x - lp.from.x,
            this._target.y - lp.from.y ) );
        let kLeftPerp: PIXI.Point = new PIXI.Point( -this._moveDir.y, this._moveDir.x );
        if ( kLeftPerp.x * targetDir.x + kLeftPerp.y * targetDir.y < 0 )
        {
            this._seekDir = -1;
        }

        PIXI.Ticker.shared.add( this.update, this );

        // Sfx.
        ServiceLocator.audioManager.playSfx( "flying_orb_launch_" + Math.round( Math.random() * 2 ).toString(), null );
    }

    // #endregion //
}

export interface ILaunchParams
{
    from: PIXI.Point;
    moveDir: PIXI.Point;
    moveSpeed: number;
    moveTween?: IFlyngOrbSpeedTween;
    seekSpeed: number;
    seekTween?: IFlyngOrbSpeedTween;
    attractSpeed: number;
    attractTween?: IFlyngOrbSpeedTween;
}

export interface IFlyngOrbSpeedTween
{
    value: number;
    duration: number;
    ease: gsap.EaseFunction;
    delay?: number;
}