import CActorArea from "../CActorArea";

import { action_scope_type, player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import { location_type } from "../CGameWorld";
import { player_type } from "../CPlayerArea";
import Utils from "../../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../../GameObject";
import CButton from "../../input/CButton";
import CTooltipReceptor from "../../ui/CTooltipReceptor";
import CSprite from "../../pixi/CSprite";
import ServiceLocator from "../../../../ServiceLocator";
import CDropArea from "../../input/CDropArea";
import CardTokenFactory from "../../../CardTokenFactory";
import FloatingMessage from "../../../FloatingMessage";


export default class CEngagedActorArea extends CActorArea 
{
    private _addEnemyBtn: GameObject = null;

    // Global.
    private static _gGenericEnemyCounter: number = 0;


    public static get genericEnemyCounter(): number { return this._gGenericEnemyCounter; }
    
    public static set genericEnemyCounter( value: number ) { this._gGenericEnemyCounter = value; }


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CEngagedActorArea";
    }

    public init(): void
    {
        super.init();

        if ( this._location == location_type.MY_ENGAGED )
        {
            this._addEnemyBtn = this.createAddEnemyBtn();
            this._addEnemyBtn.cContainer.c.position.set( 5, this._go.cContainer.c.height - 5, )
            this._go.cContainer.addChild( this._addEnemyBtn );
            this._addEnemyBtn.cButton.onClick.add( this.onAddEnemyBtn_Click, this );
        }
    }

        private createAddEnemyBtn(): GameObject
        {
            let result: GameObject = new GameObject( [ new CSprite(), new CButton(), new CTooltipReceptor() ] );
            result.cSprite.s.anchor.set( 0, 1 );
            result.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "add_enemy" );
            Utils.game.limitSideSize( 30, result.cSprite.s );
            result.cTooltipReceptor.text = jQuery.i18n( "ADD_ENEMY" );
            result.init();

            return result;
        }

    public end(): void
    {
        if ( this._addEnemyBtn )
        {
            this._addEnemyBtn.end();
            this._addEnemyBtn = null;
        }

        super.end();
    }

    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        if ( this._addEnemyBtn )
        {
            this._addEnemyBtn.cButton.setEnabled( isEnabled );
        }
    }

    public static forceEnemyTokenOidGeneration(): string
    {
        let result: string = null;
        do
        {
            result = "o" + CEngagedActorArea._gGenericEnemyCounter.toString() + "get" + GameObject.findPlayerSufix( player_type.PLAYER );
            CEngagedActorArea._gGenericEnemyCounter += 1;
        }
        while ( GameObject.has( result ) );

        return result;
    }

    // #endregion //


    // #region Multiplayer //

    public addEnemyToken( oid: string ): void
    {
        let ctf: CardTokenFactory = new CardTokenFactory();
        let enemyToken: GameObject = ctf.createGenericEnemy( oid, player_type.ALLY, this._location );
        enemyToken.cCardToken.setEnabled( false );

        this._go.cDropArea.forceDrop( enemyToken, new PIXI.Point( CDropArea.kPredefinedDropPositionCode ), action_scope_type.LOCAL );
    }

    // #endregion //


    // #region IGameObjectDropArea //

    // virtual.
    public validateDroppedGameObject( dropped: GameObject, from: GameObject, global: PIXI.Point ): boolean
    {
        let result: boolean = super.validateDroppedGameObject( dropped, from, global );
        
        if ( result )
        {
            result = 
                dropped.cCardToken
                    && dropped.cCardToken.curSide.cEnemySide != null 
                || dropped.cAttachment 
                    && ( dropped.cAttachment.curSide.cardInfo.type_code == "enemy"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "ship-enemy" )
                || dropped.cShadowCardMini 
                    && ( dropped.cShadowCardMini.card.cCard.curSide.cardInfo.type_code == "enemy"
                        || dropped.cShadowCardMini.card.cCard.curSide.cardInfo.type_code == "ship-enemy" );  
        }

        return result;
    }

    // #endregion //


    // #region Input Callbacks //

    private onAddEnemyBtn_Click(): void
    {
        if ( this.findInsertTabIndex( 130 + this._actorMargin ) != -1 )
        {
            this.showDropTab( action_scope_type.MULTIPLAYER );

            let ctf: CardTokenFactory = new CardTokenFactory();
            let enemyToken: GameObject = ctf.createGenericEnemy( this.generateEnemyTokenOid(), player_type.PLAYER, this._location );
            this._go.cDropArea.forceDrop( enemyToken, new PIXI.Point( CDropArea.kPredefinedDropPositionCode ), action_scope_type.LOCAL );

            // Multiplayer.
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ADD_GENERIC_ENEMY, null, [ enemyToken.oid ] );
        }
        else
        {
            let floatingMessage: FloatingMessage = new FloatingMessage();
            floatingMessage.show( jQuery.i18n( "NOT_ENOUGH_SPACE" ), ServiceLocator.game.app.renderer.plugins.interaction.mouse.global );
        }
    }

        private generateEnemyTokenOid(): string
        {
            let result: string = null;
            do
            {
                result = "o" + CEngagedActorArea._gGenericEnemyCounter.toString() + "get" + GameObject.findPlayerSufix( player_type.PLAYER );
                CEngagedActorArea._gGenericEnemyCounter += 1;
            }
            while ( GameObject.has( result ) );
    
            return result;
        }

    // #endregion //
}