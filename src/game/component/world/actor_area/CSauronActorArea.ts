import CActorArea from "../CActorArea";

import * as PIXI from "pixi.js";

import Signal from "../../../../lib/signals/Signal";
import GameObject from "../../../GameObject";
import CSprite from "../../pixi/CSprite";
import CButton from "../../input/CButton";
import CTooltipReceptor from "../../ui/CTooltipReceptor";
import ServiceLocator from "../../../../ServiceLocator";
import Utils from "../../../../Utils";
import Session from "../../../../Session";
import { player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import { IScenario } from "../../../ScenarioDB";
import { location_type } from "../CGameWorld";
import { ISgActorArea } from "../../../../view/game/SaveGameView";
import { player_type } from "../CPlayerArea";


export default class CSauronActorArea extends CActorArea 
{
    // #region Attributes //

    // private:

    private _splitBtn: GameObject = null;
    private _mergeBtn: GameObject = null;

    // Signals.
    private _onTotalThreatUpdated: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get nightmareSetup(): GameObject { return null; } // TODO:
    public get campaignMode(): GameObject { return null; } // TODO:

    // Signals.
    public get onTotalThreatUpdated(): Signal { return this._onTotalThreatUpdated; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CSauronActorArea";
    }

    public init(): void
    {
        super.init();

        const kScenario: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
        if ( this._location == location_type.STAGING_AREA
            && kScenario.play_area && kScenario.play_area.custom_panels && kScenario.play_area.custom_panels.indexOf( "split_sauron" ) != -1 )
        {
            this._splitBtn = this.createSplitBtn();
            this._splitBtn.cContainer.c.position.set( 5, this._go.cContainer.c.height - 5 );
            this._go.cContainer.addChild( this._splitBtn );
            this._splitBtn.cButton.onClick.add( this.onSplitBtn_Click, this );

            this._mergeBtn = this.createMergeBtn();
            this._mergeBtn.cContainer.c.position.set( 5, this._go.cContainer.c.height - 5 );
            this._go.cContainer.addChild( this._mergeBtn );
            this._mergeBtn.cButton.onClick.add( this.onMergeBtn_Click, this );
            this._mergeBtn.cContainer.c.visible = false;
        }
    }

        private createSplitBtn(): GameObject
        {
            let result: GameObject = new GameObject( [ new CSprite(), new CButton(), new CTooltipReceptor() ] );
            result.cSprite.s.anchor.set( 0, 1 );
            result.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "split" );
            Utils.game.limitSideSize( 30, result.cSprite.s );
            result.cTooltipReceptor.text = jQuery.i18n( "SPLIT" );
            result.init();

            return result;
        }

        private createMergeBtn(): GameObject
        {
            let result: GameObject = new GameObject( [ new CSprite(), new CButton(), new CTooltipReceptor() ] );
            result.cSprite.s.anchor.set( 0, 1 );
            result.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "merge" );
            Utils.game.limitSideSize( 30, result.cSprite.s );
            result.cTooltipReceptor.text = jQuery.i18n( "MERGE" );
            result.init();

            return result;
        }

    public end(): void
    {
        this._onTotalThreatUpdated.removeAll();

        if ( this._splitBtn )
        {
            this._splitBtn.end();
            this._splitBtn = null;
        }

        if ( this._mergeBtn )
        {
            this._mergeBtn.end();
            this._mergeBtn = null;
        }
        
        super.end();
    }

    public findTotalThreat(): number
    {
        let result: number = 0;

        let arrActor: Array<GameObject> = this.findAllActors();
        for ( let actor of arrActor )
        {
            if ( actor.cCardToken.cCurSide.cardInfo.threat_strength )
            {
                result += actor.cCardToken.cCurSide.cardInfo.threat_strength;
            }
        }

        return result;
    }

    public showSplitBtn(): void
    {
        this._splitBtn.cContainer.c.visible = true;
    }

    public hideSplitBtn(): void
    {
        this._splitBtn.cContainer.c.visible = false;
    }

    public onSplit(): void
    {
        this._splitBtn.cContainer.c.visible = false;
        this._mergeBtn.cContainer.c.visible = true;
    }

    public onMerged(): void
    {
        this._splitBtn.cContainer.c.visible = true;
        this._mergeBtn.cContainer.c.visible = false;
    }

    // overrides.
    public saveGame(): ISgActorArea
    {
        let result: ISgActorArea = super.saveGame();

        if ( this._splitBtn )
        {
            result.sauronActorArea = { 
                isPlayerSplit: this._mergeBtn.cContainer.c.visible,
                isAllySplit: Session.allyId 
                    && ( ServiceLocator.game.cGameWorld.cCustomPanelManager.checkPanelVisibility( "split_sauron" )
                        || this._splitBtn.cContainer.c.visible && ServiceLocator.game.cGameWorld.cCustomPanelManager.checkPanelVisibility( "isolated" ) ) };
        }

        return result;
    }

    // overrides.
    public loadGame( sgActorArea: ISgActorArea, pass: number ): void
    {
        super.loadGame( sgActorArea, pass );
        if ( pass == 0 && sgActorArea.sauronActorArea )
        {
            this._splitBtn.cContainer.c.visible = !sgActorArea.sauronActorArea.isPlayerSplit;
            this._mergeBtn.cContainer.c.visible = !this._splitBtn.cContainer.c.visible;
        }
    }

    // #endregion //


    // #region IGameObjectDropArea //

    // virtual.
    public validateDroppedGameObject( dropped: GameObject, from: GameObject, global: PIXI.Point ): boolean
    {
        let result: boolean = super.validateDroppedGameObject( dropped, from, global );

        if ( result )
        {
            result = 
                dropped.cCardToken != null
                    && ( dropped.cCardToken.curSide.cEnemySide != null
                        || dropped.cCardToken.curSide.cLocationSide != null
                        || dropped.cCardToken.curSide.cObjectiveSide != null
                        || dropped.cCardToken.curSide.cSideQuestSide != null
                        || dropped.cCardToken.curSide.cObjectiveSide != null
                        || dropped.cCardToken.curSide.cHeroSide != null )
                || dropped.cAttachment != null 
                    && ( dropped.cAttachment.curSide.cardInfo.type_code == "objective"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "objective-ally"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "objective-hero"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "ship-objective"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "objective-location"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "treachery"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "enemy"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "location"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "encounter-side-quest"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "hero"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "player-side-quest" )
                || dropped.cShadowCardMini != null 
                    && ( dropped.cShadowCardMini.card.cCard.curSide.cardInfo.type_code == "objective"
                        || dropped.cShadowCardMini.card.cCard.curSide.cardInfo.type_code == "objective-ally"
                        || dropped.cShadowCardMini.card.cCard.curSide.cardInfo.type_code == "objective-hero"
                        || dropped.cShadowCardMini.card.cCard.curSide.cardInfo.type_code == "ship-objective"
                        || dropped.cShadowCardMini.card.cCard.curSide.cardInfo.type_code == "objective-location" 
                        || dropped.cShadowCardMini.card.cCard.curSide.cardInfo.type_code == "enemy" 
                        || dropped.cShadowCardMini.card.cCard.curSide.cardInfo.type_code == "location" 
                        || dropped.cShadowCardMini.card.cCard.curSide.cardInfo.type_code == "encounter-side-quest" );
        }

        return result;
    }

    // #endregion //


    // #region Input Callbacks //

    private onSplitBtn_Click(): void
    {
        if ( Session.allyId )
        {
            ServiceLocator.game.cGameWorld.cSauronArea.split( player_type.PLAYER );

            // Multiplayer.
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SPLIT_STAGING_AREA, null, null );
        }
        else
        {
            ServiceLocator.game.cGameWorld.cSauronArea.splitSolo();
        }
    }

    private onMergeBtn_Click(): void
    {
        if ( Session.allyId )
        {
            ServiceLocator.game.cGameWorld.cSauronArea.merge( player_type.PLAYER );

            // Multiplayer.
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.MERGE_STAGING_AREA, null, null );
        }
        else
        {
            ServiceLocator.game.cGameWorld.cSauronArea.mergeSolo();
        }
    }

    // #endregion //
}