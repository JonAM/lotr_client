import CActorArea from "../CActorArea";

import * as PIXI from "pixi.js";

import Signal from "../../../../lib/signals/Signal";
import GameObject from "../../../GameObject";


export default class CPlayerActorArea extends CActorArea 
{
    // #region Attributes //

    // private:

    // Signals.
    private _onTotalWillpowerUpdated: Signal = new Signal();

    // #endregion //


    // #region Properties //

    // Signals.
    public get onTotalWillpowerUpdated(): Signal { return this._onTotalWillpowerUpdated; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CPlayerActorArea";
    }

    public end(): void
    {
        this._onTotalWillpowerUpdated.removeAll();
        
        super.end();
    }

    public findTotalWillpower(): number
    {
        let result: number = 0;

        let arrActor: Array<GameObject> = this.findAllActors();
        for ( let actor of arrActor )
        {
            if ( actor.cCardToken.cCurSide.cardInfo.willpower )
            {
                result += actor.cCardToken.cCurSide.cardInfo.willpower;
            }
        }

        return result;
    }

    // #endregion //


    // #region IGameObjectDropArea //

    // virtual.
    public validateDroppedGameObject( dropped: GameObject, from: GameObject, global: PIXI.Point ): boolean
    {
        let result: boolean = super.validateDroppedGameObject( dropped, from, global );
        
        if ( result )
        {
            result = 
                dropped.cCardToken
                    && ( dropped.cCardToken.curSide.cHeroSide != null 
                        || dropped.cCardToken.curSide.cAllySide != null
                        || dropped.cCardToken.curSide.cLocationSide != null
                        || dropped.cCardToken.curSide.cObjectiveSide != null )
                || dropped.cAttachment 
                    && ( dropped.cAttachment.curSide.cardInfo.type_code == "objective-hero"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "objective-ally"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "ship-objective"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "objective-location"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "objective"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "location"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "hero"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "ally"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "event"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "attachment"
                        || dropped.cAttachment.curSide.cardInfo.type_code == "treasure" )
                || dropped.cShadowCardMini 
                    && ( dropped.cShadowCardMini.card.cCard.curSide.cardInfo.type_code == "objective-hero"
                        || dropped.cShadowCardMini.card.cCard.curSide.cardInfo.type_code == "objective-ally"
                        || dropped.cShadowCardMini.card.cCard.curSide.cardInfo.type_code == "ship-objective"
                        || dropped.cShadowCardMini.card.cCard.curSide.cardInfo.type_code == "objective-location"
                        || dropped.cShadowCardMini.card.cCard.curSide.cardInfo.type_code == "objective"
                        || dropped.cShadowCardMini.card.cCard.curSide.cardInfo.type_code == "location" );
        }

        return result;
    }

    // #endregion //
}