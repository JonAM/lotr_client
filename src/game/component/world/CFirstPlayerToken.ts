import Component from "../Component";

import { location_type } from "./CGameWorld";
import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CContainer from "../pixi/CContainer";
import IDragShadowTextureCreator from "../../IDragShadowTextureCreator";


export default class CFirstPlayerToken extends Component implements IDragShadowTextureCreator
{
    // #region Attributes //

    // private:

    private _poiSocket: GameObject = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CFirstPlayerToken";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CCardToken.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cFirstPlayerTokenPoiReceptor != null, "CCardToken.ts :: init() :: CFirstPlayerTokenPoiReceptor component not found." );

        // Shadow.
        let shadow: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( "shadow_first_player" ) );
        shadow.anchor.set( 0.45 );
        this._go.cContainer.c.addChild( shadow );

        // Sprite.
        let sprite: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.find( "first_player" ).data );
        sprite.name = "sprite";
        Utils.game.limitSideSize( 50, sprite );
        sprite.anchor.set( 0.5 );
        this._go.cContainer.c.addChild( sprite );

        // POI socket.
        this._poiSocket = new GameObject( [ new CContainer() ] );
        this._poiSocket.cContainer.c.name = "poi_socket";
        this._poiSocket.init();
        this._poiSocket.cContainer.c.y -= sprite.height * 0.5 - 5;
        this._go.cContainer.addChild( this._poiSocket );
        
        this._go.cFirstPlayerTokenPoiReceptor.poiSocket = this._poiSocket;
    }

    public end(): void
    {
        this._poiSocket.end();
        this._poiSocket = null;

        super.end();
    }

    // #endregion //


    // #region IDragShadowTextureCreator //

    public createDragShadowTexture(): PIXI.Texture
    {
        let source: PIXI.Sprite = this._go.cContainer.c.getChildByName( "sprite" ) as PIXI.Sprite;
        let dragShadowTexture = PIXI.RenderTexture.create( { width: source.width, height: source.height } );
        let pos: PIXI.Point = new PIXI.Point();
        source.position.copyTo( pos );
        source.position.set( source.width * 0.5, source.height * 0.5 );
        ServiceLocator.game.app.renderer.render( source, dragShadowTexture );    
        pos.copyTo( source.position );

        return dragShadowTexture;
    }

    // #endregion //
}
