import Component from "../Component";

import { action_scope_type } from "../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CardTokenFactory from "../../CardTokenFactory";
import { TweenMax, Sine, gsap } from "gsap";
import Utils from "../../../Utils";
import CDropArea from "../input/CDropArea";
import { ISgActorTab, ISgCardToken } from "../../../view/game/SaveGameView";
import DroppedEmitter from "../../DroppedEmitter";
import { layer_type } from "./CGameLayerProvider";
import Signal from "../../../lib/signals/Signal";


export default class CActorTab extends Component
{
    // #region Attributes //

    // private:

    private _width: number = null;
    private _actorArea: GameObject = null;

    private _isAnimated: boolean = true;

    private _arrActor: Array<GameObject> = null;

    // Signals.
    private _onSizeUpdated: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get actors(): Array<GameObject> { return this._arrActor; }

    public set width( value: number ) { this._width = value; }
    public set actorArea( value: GameObject ) { this._actorArea = value; }
    public set isAnimated( value: boolean ) { this._isAnimated = value; }

    // Signals.
    public get onSizeUpdated(): Signal { return this._onSizeUpdated; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CActorTab";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CActorTab.ts :: init() :: CContainer component not found." );
        console.assert( this._width != null, "CActorTab.ts :: init() :: this._width cannot be null." );
        console.assert( this._actorArea != null, "CActorTab.ts :: init() :: this._actorArea cannot be null." );

        this._arrActor = new Array<GameObject>();
    }

    public end(): void
    {
        // Cleanup events.
        this._onSizeUpdated.removeAll();

        for ( let actor of this._arrActor )
        {
            actor.end();
        }
        this._arrActor = null;

        this._width = null;
        this._actorArea = null;
    }

    public findActorDropPosition( cardToken: GameObject ): PIXI.Point
    {
        let result: PIXI.Point = new PIXI.Point();

        const kUsedSpace: number = this.calcUsedSpace() + this._actorArea.cActorArea.actorMargin + cardToken.cCardToken.calcWidth();
        let curX: number = ( this._width - kUsedSpace ) * 0.5;
        for ( let displayObject of this._go.cContainer.c.children )
        { 
            const kActorAux: GameObject = ( displayObject as any ).go as GameObject;
            curX += this._actorArea.cActorArea.actorMargin + kActorAux.cCardToken.calcWidth();
        }
        const kGameLayerProvider: GameObject = Utils.game.findGameObjectInBranchByComponentName( this._go, "CGameLayerProvider" );
        const kActorTabPos: PIXI.IPoint = kGameLayerProvider.cContainer.c.toLocal( this._go.cContainer.c.getGlobalPosition() );
        result.x = kGameLayerProvider.cContainer.c.x + kActorTabPos.x + curX;
        result.y = kGameLayerProvider.cContainer.c.y + kActorTabPos.y + cardToken.cCardToken.calcHeight() * 0.5;

        return result;
    }

    public findActorInsertIndex( global: PIXI.Point ): number
    {
        let insertIndex: number = this._go.cContainer.c.children.length;
        if ( global.x != CDropArea.kPredefinedDropPositionCode )
        {
            let local: PIXI.IPoint = this._go.cContainer.c.toLocal( global );
            insertIndex = 0;
            let isFound: boolean = false;
            for ( let tokenAux of this._go.cContainer.c.children )
            {
                const kContainer: PIXI.Container = tokenAux as PIXI.Container;
                if ( local.x < kContainer.x + kContainer.getBounds().width * 0.5 )
                {
                    isFound = true;
                    break;
                }

                ++insertIndex;
            }
            //
            if ( !isFound )
            {
                insertIndex = this._go.cContainer.c.children.length;
            }
        }

        return insertIndex;
    }

    public calcWidth(): number
    {
        let result: number = 0;

        for ( let actor of this._arrActor )
        {
            result += actor.cCardToken.calcWidth() + this._actorArea.cActorArea.actorMargin;
        }
        result -= this._actorArea.cActorArea.actorMargin;

        return result;
    }

    public sort( arrDropped: Array<GameObject> ): void
    {
        const kUsedSpace: number = this.calcUsedSpace();

        let curX: number = ( this._width - kUsedSpace ) * 0.5;
        for ( let displayObject of this._go.cContainer.c.children )
        { 
            let cardToken: GameObject = ( displayObject as any ).go as GameObject;

            let isDropped: boolean = arrDropped && arrDropped.indexOf( cardToken ) != -1;
            if ( !isDropped )
            {
                let arrTween: Array<gsap.core.Tween> = gsap.getTweensOf( cardToken.cContainer.c );
                for ( let tween of arrTween )
                {
                    if ( tween.data && tween.data == "x" )
                    {
                        tween.kill();
                    }
                }
            }

            let fromX: number = !isDropped ? cardToken.cContainer.c.x : curX;
            cardToken.cContainer.c.x = curX;

            // Animate.
            if ( this._isAnimated && ServiceLocator.game.isAnimated )
            {
                if ( isDropped )
                {
                    // Vfx.
                    this.playDroppedVfx( cardToken );
                    Utils.anim.dropFromCorner( cardToken.cContainer.c );

                    // Sfx.
                    ServiceLocator.audioManager.playSfx( "token_dropped" );
                }
                else if ( fromX != cardToken.cContainer.c.x )
                {
                    TweenMax.from( cardToken.cContainer.c, Math.abs( cardToken.cContainer.c.x - fromX ) / 500, { x: fromX, ease: Sine.easeOut, data: "x" } );
                }
            }

            curX += this._actorArea.cActorArea.actorMargin + cardToken.cCardToken.calcWidth();
        }
    }
        
        private playDroppedVfx( actor: GameObject ): void
        {
            let droppedEmitter: DroppedEmitter = new DroppedEmitter();
            const kVfxLayer: GameObject = Utils.game.findGameProviderLayer( this._go, layer_type.VFX );
            droppedEmitter.layer = kVfxLayer
            droppedEmitter.init();
            const kSideBg: PIXI.Container = actor.cCardToken.cCurSide.bg.cContainer.c;
            const kSidePos: PIXI.IPoint = kVfxLayer.cContainer.c.toLocal( kSideBg.getGlobalPosition() );
            droppedEmitter.playLarge( kSidePos.x + kSideBg.width * 0.5, kSidePos.y + kSideBg.height * 0.5 );
        }

    public addActorAtIndex( actor: GameObject, insertIndex: number ): void
    {
        if ( insertIndex == null )
        {
            insertIndex = this._go.cContainer.c.children.length;
        }

        this._arrActor.splice( insertIndex, 0, actor );
        this._go.cContainer.addChildAt( actor, insertIndex );

        actor.cCardToken.onSizeUpdated.add( this.onActorSize_Updated, this );
        actor.cDraggable.onDropped.add( this.onActor_Dropped, this );
        actor.cDraggable.onPostDropped.add( this.onActor_PostDropped, this );
    }

    public setActorIndex( actor: GameObject, index: number ): void
    {
        const kCurIndex: number = this._arrActor.indexOf( actor );
        console.assert( kCurIndex >= 0, "CActorTab.ts :: setCharacterIndex() :: actor must be present in the actor tab." );
        this._arrActor.splice( kCurIndex, 1 );
        this._go.cContainer.c.removeChildAt( kCurIndex );
        //
        if ( index > kCurIndex )
        {
            index -= 1;
        }
        //
        this._arrActor.splice( index, 0, actor );
        this._go.cContainer.addChildAt( actor, index );

        this.sort( [ actor ] );
    }

    public prependActors( arrActor: Array<GameObject> ): void
    {
        if ( arrActor.length == 0 ) { return; }

        let requiredSpace: number = 0;
        for ( let actor of arrActor )
        {
            requiredSpace += actor.cCardToken.calcWidth() + this._actorArea.cActorArea.actorMargin;
        }
        console.assert( this.calcRemainingSpace() >= requiredSpace, "CActorTab.ts :: prependActors() :: Not enough space on tab." );

        for ( let i: number = arrActor.length - 1; i >= 0; --i )
        {
            let actor: GameObject = arrActor[ i ];
            gsap.killTweensOf( actor.cContainer.c );

            actor.cDraggable.onDropped.dispatch( actor, this._go, action_scope_type.LOCAL );
            actor.cDraggable.removeListeners();
            
            actor.cCardToken.location = this._actorArea.cActorArea.location;

            this._arrActor.unshift( actor )
            this._go.cContainer.addChildAt( actor, 0 );
        }

        this.sort( arrActor );

        for ( let actor of arrActor )
        {
            actor.cCardToken.onSizeUpdated.add( this.onActorSize_Updated, this );
            actor.cDraggable.onDropped.add( this.onActor_Dropped, this );
            actor.cDraggable.onPostDropped.add( this.onActor_PostDropped, this );
        }

        this._actorArea.cActorArea.onContentChanged.dispatch();
        for ( let actor of arrActor )
        {
            this._actorArea.cActorArea.onTokenSet.dispatch( actor );
        }
    }

    public appendActors( arrActor: Array<GameObject> ): void
    {
        if ( arrActor.length == 0 ) { return; }

        let requiredSpace: number = 0;
        for ( let actor of arrActor )
        {
            requiredSpace += actor.cCardToken.calcWidth() + this._actorArea.cActorArea.actorMargin;
        }
        console.assert( this.calcRemainingSpace() >= requiredSpace, "CActorTab.ts :: appendActors() :: Not enough space on tab." );

        for ( let actor of arrActor )
        {
            gsap.killTweensOf( actor.cContainer.c );

            actor.cDraggable.onDropped.dispatch( actor, this._go, action_scope_type.LOCAL );
            actor.cDraggable.removeListeners();

            actor.cCardToken.location = this._actorArea.cActorArea.location;

            this._arrActor.push( actor )
            this._go.cContainer.addChild( actor );
        }

        this.sort( arrActor );

        for ( let actor of arrActor )
        {
            actor.cCardToken.onSizeUpdated.add( this.onActorSize_Updated, this );
            actor.cDraggable.onDropped.add( this.onActor_Dropped, this );
            actor.cDraggable.onPostDropped.add( this.onActor_PostDropped, this );
        }

        this._actorArea.cActorArea.onContentChanged.dispatch();
        for ( let actor of arrActor )
        {
            this._actorArea.cActorArea.onTokenSet.dispatch( actor );
        }
    }

    public calcRemainingSpace(): number
    {
        return this._width -this.calcUsedSpace();
    }

    public calcUsedSpace(): number
    {
        let occupiedSpace: number = 0;

        for ( let displayObject of this._go.cContainer.c.children )
        {
            occupiedSpace += ( ( displayObject as any ).go as GameObject ).cCardToken.calcWidth();
        }
        if ( this._go.cContainer.c.children.length > 1 )
        {
            occupiedSpace += this._actorArea.cActorArea.actorMargin * ( this._go.cContainer.c.children.length - 1 );
        }

        return occupiedSpace;
    }

    public removeActorListeners( actor: GameObject ): void
    {
        actor.cDraggable.removeListeners();
        actor.cCardToken.onSizeUpdated.remove( this.onActorSize_Updated, this )
    }

    // virtual.
    public saveGame(): ISgActorTab
    {
        let arrSgCardToken: Array<ISgCardToken> = new Array<ISgCardToken>();
        for ( let cardToken of this._arrActor )
        {
            arrSgCardToken.push( cardToken.cCardToken.saveGame() );
        }

        return { actors: arrSgCardToken };
    }

    // virtual.
    public loadGame( sgActorTab: ISgActorTab, pass: number ): void
    {
        if ( pass == 0 )
        {
            if ( sgActorTab.actors.length > 0 )
            {
                let cardTokenFactory: CardTokenFactory = new CardTokenFactory();
                let arrActor: Array<GameObject> = new Array<GameObject>();
                for ( let sgCardToken of sgActorTab.actors )
                {
                    let actor: GameObject = null;
                    if ( sgCardToken.frontSide.cardId == "999001" || sgCardToken.backSide.cardId == "999001" )
                    {
                        actor = cardTokenFactory.createGenericEnemy( sgCardToken.oid, sgCardToken.controller, sgCardToken.location );
                    }
                    else
                    {
                        let card: GameObject = GameObject.find( sgCardToken.oid.substr( 0, sgCardToken.oid.length - 1 ) );
                        actor = cardTokenFactory.create( card );
                        card.end();
                    }
                    actor.cCardToken.loadGame( sgCardToken, pass );
                    
                    arrActor.push( actor );
                }
                this.appendActors( arrActor );
            }
        }
    }

    // #endregion //


    // #region Callbacks //

    private onActorSize_Updated(): void
    {
        this._onSizeUpdated.dispatch( this._go );
    }

    private onActor_Dropped( dropped: GameObject, dropArea: GameObject ): void
    {
        let tokenIndex: number = this._arrActor.indexOf( dropped );
        this._arrActor.splice( tokenIndex, 1 );

        dropped.cCardToken.onSizeUpdated.remove( this.onActorSize_Updated, this );

        if ( dropArea != this._actorArea )
        {
            this._actorArea.cActorArea.onTokenUnset.dispatch( dropped );
            this._actorArea.cActorArea.onContentChanged.dispatch();
        }
    }

    private onActor_PostDropped( dropped: GameObject, dropArea: GameObject ): void
    {
        // If dropped is not already ended (ie. when discarding).
        if ( dropped.cContainer 
            && !Utils.game.isSameGameLayerProvider( this._actorArea, dropped ) )
        {
            ServiceLocator.game.attackBindingManager.removeAllBindings( dropped, action_scope_type.LOCAL );
        }

        this._onSizeUpdated.dispatch( this._go );
    }

    // #endregion //
}