import Signal from "../../../../lib/signals/Signal";
import State from "../../../../lib/state_machine/State";
import { action_scope_type, player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../../ServiceLocator";
import CardPresentationState from "../../../../states/game/CardPresentationState";
import ActorAreaPlacementState from "../../../../states/game/card_placement/ActorAreaPlacementState";
import ActorPlacementState from "../../../../states/game/card_placement/ActorPlacementState";
import DeckIndicatorPlacementState from "../../../../states/game/card_placement/DeckIndicatorPlacementState";
import GameModifierPlacementState from "../../../../states/game/card_placement/GameModifierPlacementState";
import GameModifierSlotPlacementState from "../../../../states/game/card_placement/GameModifierSlotPlacementState";
import QuestPlacementState from "../../../../states/game/card_placement/QuestPlacementState";
import SidePlacementState from "../../../../states/game/card_placement/SidePlacementState";
import { button_type } from "../../../../states/game/setup/CardPresentationUtils";
import Utils from "../../../../Utils";
import GameObject from "../../../GameObject";
import { detail_bar_icon_type, IDetailBarItem } from "../../card/token/CDetailBar";
import LogTargetCard from "../../ui/right_menu/action_log/target/LogTargetCard";
import { location_type } from "../CGameWorld";
import { player_type } from "../CPlayerArea";


export default class CardActivationController
{
    private _stateData: IStateData = null;
    private _presentFrom: location_type = null;

    private _curState: State = null;

    private _onCompleted: Signal = new Signal();


    public set card( value: GameObject ) { this._stateData.card = value; }
    public set presentFrom( value: location_type ) { this._presentFrom = value; }

    public get onCompleted(): Signal { return this._onCompleted; }
 
    
    public constructor() 
    {
        this._stateData = {
            card: null,
            disablerBg: null,
            planeContainer: null,
            buttonRowType: null };
    }

    public init(): void
    {
        console.assert( this._stateData.card != null, "CardActivationController.ts :: init() :: this._stateData.card cannot be null." );
    
        //ServiceLocator.game.cGameWorld.cActionLogger.logSequence( player_type.PLAYER, new LogTargetCard( this._stateData.card ), Utils.img.findLocationTextureId( this._stateData.card.cCard.location ), "play_card", false ); 

        this._curState = this.createPresentationState();
        this._curState.onEnter();
    }

        private createPresentationState(): CardPresentationState
        {
            let cps: CardPresentationState = new CardPresentationState();
            cps.data = this._stateData;
            cps.from = this._presentFrom;
            cps.init();
            cps.onCompleted.add( this.onPresentation_Completed, this );

            return cps;
        }

    public end(): void 
    {
        this._onCompleted.removeAll();

        this._stateData = null;
        this._presentFrom = null;
        this._curState = null;
    }

    public onPresentation_Completed(): void
    {
        this._curState.onLeave();
        this._curState.end();

        // Log.
        if ( this._stateData.buttonRowType == button_type.TARGET )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( this._stateData.card ), "rad_target", false ); 
        }

        switch ( this._stateData.buttonRowType )
        {
            case button_type.DRAW: { this._curState = this.createDeckIndicatorPlacementState( ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.hand ); break; }

            case button_type.PLAY: 
            { 
                switch ( this._stateData.card.cCard.curSide.cardInfo.type_code )
                {
                    case "quest-intro":
                    case "quest": { this._curState = this.createQuestPlacementState(); break; }

                    case "enemy":
                    case "ship-enemy":
                    case "location":
                    case "treachery": 
                    case "objective":
                    case "objective-ally":
                    case "objective-hero":
                    case "ship-objective":
                    case "objective-location":
                    case "encounter-side-quest":
                    case "player-side-quest": 
                        { 
                            ServiceLocator.game.cGameWorld.cCustomPanelManager.hideAll();
                            this._curState = this.createActorAreaPlacementState( ServiceLocator.game.cGameWorld.cSauronArea.staging ); 
                            break; 
                        }

                    case "ally":
                    case "hero": 
                    case "attachment":
                    case "event":
                    case "treasure":
                        { 
                            ServiceLocator.game.cGameWorld.cCustomPanelManager.hideAll();
                            this._curState = this.createActorAreaPlacementState( ServiceLocator.game.cGameWorld.cPlayerArea.home ); 
                            break; 
                        }

                    case "contract":
                        { 
                            ServiceLocator.game.cGameWorld.cCustomPanelManager.hideAll();
                            this._curState = this.createGameModifierSlotPlacementState( ServiceLocator.game.cGameWorld.cPlayerArea.home ); 
                            break; 
                        }

                    case "campaign":
                    case "nightmare-setup":
                    case "gencon-setup":
                        { 
                            ServiceLocator.game.cGameWorld.cCustomPanelManager.hideAll();
                            this._curState = this.createGameModifierSlotPlacementState( ServiceLocator.game.cGameWorld.cSauronArea.staging ); 
                            break; 
                        }
                }
                break;
            }
            
            case button_type.EQUIP:
            case button_type.TARGET:
            case button_type.UNDERNEATH:
            case button_type.SHADOW_CARD: { this._curState = this.createSidePlacementState(); break; }

            case button_type.DISCARD:
            {
                if ( ServiceLocator.game.forcedDiscardLocations.has( this._stateData.card.cCard.front.cardId ) )
                {
                    this._curState = this.createDeckIndicatorPlacementState( ServiceLocator.game.cGameWorld.findLocation( ServiceLocator.game.forcedDiscardLocations.get( this._stateData.card.cCard.front.cardId ) ) );
                }
                else if ( this._stateData.card.cCard.front.cardInfo.side == "999003"
                    || this._stateData.card.cCard.back.cardInfo.side == "999003" )
                {
                    this._curState = this.createDeckIndicatorPlacementState( ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.discard ); 
                }
                else if ( this._stateData.card.cCard.front.cardInfo.side == "999002"
                    || this._stateData.card.cCard.back.cardInfo.side == "999002" )
                {
                    switch ( this._stateData.card.cCard.ownerPlayer )
                    {
                        case player_type.PLAYER: { this._curState = this.createDeckIndicatorPlacementState( ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.discard ); break; }
                        case player_type.ALLY: { this._curState = this.createDeckIndicatorPlacementState( ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.discard ); break; }
                        case player_type.SAURON: { this._curState = this.createDeckIndicatorPlacementState( ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.removedFromGame ); break; }
                    }
                }
                else
                {
                    this._curState = this.createDeckIndicatorPlacementState( ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.removedFromGame ); 
                }
                break;
            }

            case button_type.CANCEL:
            {
                if ( this._stateData.card.cCard.location == location_type.MY_UNDERNEATH || this._stateData.card.cCard.location == location_type.ALLY_UNDERNEATH )
                {
                    let from: GameObject = GameObject.find( this._stateData.card.cCard.locationData );
                    if ( from.cCardToken )
                    {
                        this._curState = this.createActorPlacementState( from );
                    }
                    else if ( from.cGameModifier )
                    {
                        this._curState = this.createGameModifierPlacementState( from );
                    }
                }
                else
                {
                    this._curState = this.createDeckIndicatorPlacementState( ServiceLocator.game.cGameWorld.findLocation( this._stateData.card.cCard.location ) );
                }
                break;
            }
        }

        this._curState.init();
        this._curState.onEnter();

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.CARD_ACTIVATION_PLACE, null, [ this._stateData.buttonRowType ] );
    }

        private createDeckIndicatorPlacementState( deckIndicator: GameObject ): DeckIndicatorPlacementState
        {
            let result: DeckIndicatorPlacementState = new DeckIndicatorPlacementState();
            result.data = this._stateData;
            result.deckIndicator = deckIndicator;
            result.onCompleted.add( this.onPlacement_Completed, this );

            return result;
        }

        private createGameModifierPlacementState( gameModifier: GameObject ): GameModifierPlacementState
        {
            let result: GameModifierPlacementState = new GameModifierPlacementState();
            result.data = this._stateData;
            result.gameModifier = gameModifier;
            result.onCompleted.add( this.onPlacement_Completed, this );

            return result;
        }

        private createActorPlacementState( actor: GameObject ): ActorPlacementState
        {
            let result: ActorPlacementState = new ActorPlacementState();
            result.data = this._stateData;
            result.actor = actor;
            result.onCompleted.add( this.onPlacement_Completed, this );

            return result;
        }

        private createQuestPlacementState(): QuestPlacementState
        {
            let questPlacementState: QuestPlacementState = new QuestPlacementState();
            questPlacementState.data = this._stateData;
            questPlacementState.questHolder = ServiceLocator.game.cGameWorld.cSauronArea.questHolder;
            questPlacementState.onCompleted.add( this.onPlacement_Completed, this );

            return questPlacementState;
        }

        private createActorAreaPlacementState( actorArea: GameObject ): ActorAreaPlacementState
        {
            let result: ActorAreaPlacementState = new ActorAreaPlacementState();
            result.data = this._stateData;
            result.actorArea = actorArea;
            result.onCompleted.add( this.onPlacement_Completed, this );

            return result;
        }

        private createGameModifierSlotPlacementState( actorArea: GameObject ): GameModifierSlotPlacementState
        {
            let result: GameModifierSlotPlacementState = new GameModifierSlotPlacementState();
            result.data = this._stateData;
            result.actorArea = actorArea;
            result.onCompleted.add( this.onPlacement_Completed, this );

            return result;
        }

        private createSidePlacementState(): SidePlacementState
        {
            let sidePlacementState: SidePlacementState = new SidePlacementState();
            sidePlacementState.data = this._stateData;
            sidePlacementState.onCompleted.add( this.onPlacement_Completed, this );

            return sidePlacementState;
        }

    public onPlacement_Completed(): void
    {
        this._curState.onLeave();
        this._curState.end();

        this._onCompleted.dispatch();
    }
}

export interface IStateData
{
    card: GameObject;
    disablerBg: GameObject;
    planeContainer: GameObject;
    buttonRowType: button_type;
}