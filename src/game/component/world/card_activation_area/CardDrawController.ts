import Signal from "../../../../lib/signals/Signal";
import State from "../../../../lib/state_machine/State";
import CardPresentationState from "../../../../states/game/CardPresentationState";
import GameObject from "../../../GameObject";
import ServiceLocator from "../../../../ServiceLocator";
import { action_scope_type, player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import CContainer from "../../pixi/CContainer";
import CLogSequence from "../../ui/right_menu/action_log/CLogSequence";
import CLogActionPreviewable from "../../input/CLogActionPreviewable";
import CHighlightPoiReceptor from "../poi_receptor/CHighlightPoiReceptor";
import { player_type } from "../CPlayerArea";
import LogTargetCard from "../../ui/right_menu/action_log/target/LogTargetCard";
import Utils from "../../../../Utils";
import { location_type } from "../CGameWorld";
import { IStateData } from "./CardActivationController";
import DeckIndicatorPlacementState from "../../../../states/game/card_placement/DeckIndicatorPlacementState";
import { button_type } from "../../../../states/game/setup/CardPresentationUtils";


export default class CardDrawController
{
    private _stateData: IStateData = null;
    private _presentationFrom: location_type = null;

    private _curState: State = null;

    private _onCompleted: Signal = new Signal();


    public set card( value: GameObject ) { this._stateData.card = value; }
    public set presentationFrom( value: location_type ) { this._presentationFrom = value; }

    public get onCompleted(): Signal { return this._onCompleted; }
 
    
    public constructor() 
    {
        this._stateData = {
            card: null,
            disablerBg: null,
            planeContainer: null,
            buttonRowType: null };
    }

    public init(): void
    {
        console.assert( this._stateData.card != null, "CardDrawController.ts :: init() :: this._stateData.card cannot be null." );
    
        if ( ServiceLocator.game.isAnimated )
        {
            ServiceLocator.game.allyActionManager.pause();
        }

        //ServiceLocator.game.cGameWorld.cActionLogger.logSequence( player_type.PLAYER, new LogTargetCard( this._stateData.card ), Utils.img.findLocationTextureId( this._stateData.card.cCard.location ), "play_card", false ); 

        this._curState = this.createPresentationState();
        this._curState.onEnter();
    }

        private createPresentationState(): CardPresentationState
        {
            let cps: CardPresentationState = new CardPresentationState();
            cps.data = this._stateData;
            cps.from = this._presentationFrom;
            cps.buttonTypes = [ button_type.CANCEL ];
            cps.init();
            cps.onCompleted.add( this.onPresentation_Completed, this );

            return cps;
        }

    public end(): void 
    {
        this._onCompleted.removeAll();

        this._stateData = null;
        this._curState = null;

        if ( ServiceLocator.game.isAnimated )
        {
            ServiceLocator.game.allyActionManager.resume();
        }
    }

    public onPresentation_Completed(): void
    {
        this._curState.onLeave();
        this._curState.end();

        if ( this._stateData.buttonRowType == button_type.CANCEL )
        {
            this._curState = this.createDeckIndicatorPlacementState( ServiceLocator.game.cGameWorld.findLocation( this._stateData.card.cCard.location ) );
        }
        else
        {
            this._curState = this.createDeckIndicatorPlacementState( ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.hand ); 
        }

        this._curState.init();
        this._curState.onEnter();
    }

        private createDeckIndicatorPlacementState( deckIndicator: GameObject ): DeckIndicatorPlacementState
        {
            let result: DeckIndicatorPlacementState = new DeckIndicatorPlacementState();
            result.data = this._stateData;
            result.deckIndicator = deckIndicator;
            result.actionScope = action_scope_type.MULTIPLAYER;
            result.onCompleted.add( this.onPlacement_Completed, this );

            return result;
        }

    public onPlacement_Completed(): void
    {
        if ( this._stateData.buttonRowType != button_type.CANCEL )
        {
            let logAction: GameObject = this.createPlacementLogAction();   
            // Multiplayer.
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.LOG_ACTION, null, logAction.cLogAction.serialize() )
            logAction.end();
        }

        this._curState.onLeave();
        this._curState.end();

        this._onCompleted.dispatch();
    }

        private createPlacementLogAction(): GameObject
        {
            let logAction: GameObject = new GameObject( [ new CContainer(), new CLogSequence(), new CLogActionPreviewable(), new CHighlightPoiReceptor() ] );   
            logAction.oid = "temp_log_action";
            logAction.cLogAction.who = player_type.PLAYER;
            let logTargetCard: LogTargetCard = new LogTargetCard( this._stateData.card );
            logTargetCard.id = this._stateData.card.cCard.back.cardId; // show back side.
            logAction.cLogSequence.target = logTargetCard;
            logAction.cLogSequence.fromIconId = "player_deck";
            logAction.cLogSequence.toIconId = "hand";
            logAction.init();
            
            return logAction;
        }
}