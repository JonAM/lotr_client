import Signal from "../../../../lib/signals/Signal";
import State from "../../../../lib/state_machine/State";
import { action_scope_type, player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../../ServiceLocator";
import AllyCardPresentationState from "../../../../states/game/AllyCardPresentationState";
import CardPresentationState from "../../../../states/game/CardPresentationState";
import ActorAreaPlacementState from "../../../../states/game/card_placement/ActorAreaPlacementState";
import ActorPlacementState from "../../../../states/game/card_placement/ActorPlacementState";
import AllySidePlacementState from "../../../../states/game/card_placement/AllySidePlacementState";
import DeckIndicatorPlacementState from "../../../../states/game/card_placement/DeckIndicatorPlacementState";
import GameModifierPlacementState from "../../../../states/game/card_placement/GameModifierPlacementState";
import GameModifierSlotPlacementState from "../../../../states/game/card_placement/GameModifierSlotPlacementState";
import QuestPlacementState from "../../../../states/game/card_placement/QuestPlacementState";
import SidePlacementState from "../../../../states/game/card_placement/SidePlacementState";
import { button_type } from "../../../../states/game/setup/CardPresentationUtils";
import Utils from "../../../../Utils";
import GameObject from "../../../GameObject";
import ProcDummy from "../../../process_sequencer/ProcDummy";
import LogTargetCard from "../../ui/right_menu/action_log/target/LogTargetCard";
import { location_type } from "../CGameWorld";
import { player_type } from "../CPlayerArea";


export default class AllyCardActivationController
{
    private _stateData: IStateData = null;
    private _presentFrom: location_type = null;

    private _curState: State = null;

    private _onCompleted: Signal = new Signal();


    public set card( value: GameObject ) { this._stateData.card = value; }
    public set presentFrom( value: location_type ) { this._presentFrom = value; }

    public get onCompleted(): Signal { return this._onCompleted; }
 
    
    public constructor() 
    {
        this._stateData = {
            card: null,
            disablerBg: null,
            planeContainer: null,
            buttonRowType: null };
    }

    public init(): void
    {
        console.assert( this._stateData.card != null, "AllyCardActivationController.ts :: init() :: this._stateData.card cannot be null." );
        console.assert( this._presentFrom != null, "AllyCardActivationController.ts :: init() :: this._presentFrom cannot be null." );
     
        //ServiceLocator.game.cGameWorld.cActionLogger.logSequence( player_type.ALLY, new LogTargetCard( this._stateData.card ), Utils.img.findLocationTextureId( this._stateData.card.cCard.location ), "play_card", false ); 

        // Sfx.
        ServiceLocator.audioManager.playSfx( "card_played" );

        this._curState = this.createPresentationState();
        this._curState.onEnter();
    }

        private createPresentationState(): AllyCardPresentationState
        {
            let acps: AllyCardPresentationState = new AllyCardPresentationState();
            acps.data = this._stateData;
            acps.from = this._presentFrom;
            acps.init();
            acps.onCompleted.addOnce( this.onPresentation_Completed, this );

            return acps;
        }

    public end(): void 
    {
        this._onCompleted.removeAll();

        this._stateData = null;
        this._presentFrom = null;
        this._curState = null;
    }

    public onPresentation_Completed(): void
    {
        if ( ServiceLocator.game.isAnimated )
        {
            ServiceLocator.game.allyActionManager.pause();
        }

        this._curState.onLeave();
        this._curState.end();

        // Log.
        if ( this._stateData.buttonRowType == button_type.TARGET )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.ALLY, new LogTargetCard( this._stateData.card ), "rad_target", false ); 
        }

        switch ( this._stateData.buttonRowType )
        {
            case button_type.DRAW: { this._curState = this.createDeckIndicatorPlacementState( ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.hand ); break; }

            case button_type.PLAY: 
            { 
                switch ( this._stateData.card.cCard.curSide.cardInfo.type_code )
                {
                    case "quest-intro":
                    case "quest": { this._curState = this.createQuestPlacementState(); break; }

                    case "enemy":
                    case "ship-enemy":
                    case "treachery": 
                    case "objective":
                    case "objective-ally":
                    case "objective-location":
                    case "ship-objective":
                    case "objective-hero":
                    case "player-side-quest":
                    case "encounter-side-quest":
                    case "location": { this._curState = this.createActorAreaPlacementState( ServiceLocator.game.cGameWorld.cSauronArea.staging ); break; }

                    case "ally":
                    case "hero": 
                    case "event":
                    case "attachment":
                    case "treasure": { this._curState = this.createActorAreaPlacementState( ServiceLocator.game.cGameWorld.cAllyArea.home ); break; }
                
                    case "contract":
                        { 
                            this._curState = this.createGameModifierSlotPlacementState( ServiceLocator.game.cGameWorld.cAllyArea.home ); 
                            break; 
                        }

                    case "campaign":
                    case "nightmare-setup":
                    case "gencon-setup":
                        { 
                            this._curState = this.createGameModifierSlotPlacementState( ServiceLocator.game.cGameWorld.cSauronArea.staging ); 
                            break; 
                        }
                }
                break;
            }
            
            case button_type.EQUIP:
            case button_type.TARGET:
            case button_type.UNDERNEATH:
            case button_type.SHADOW_CARD: { this._curState = this.createAllySidePlacementState(); break; }

            case button_type.DISCARD:
            {
                if ( this._stateData.card.cCard.front.cardInfo.side == "999003"
                    || this._stateData.card.cCard.back.cardInfo.side == "999003" )
                {
                    this._curState = this.createDeckIndicatorPlacementState( ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.discard ); 
                }
                else if ( this._stateData.card.cCard.front.cardInfo.side == "999002"
                    || this._stateData.card.cCard.back.cardInfo.side == "999002" )
                {
                    switch ( this._stateData.card.cCard.ownerPlayer )
                    {
                        case player_type.PLAYER: { this._curState = this.createDeckIndicatorPlacementState( ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.discard ); break; }
                        case player_type.ALLY: { this._curState = this.createDeckIndicatorPlacementState( ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.discard ); break; }
                        case player_type.SAURON: { this._curState = this.createDeckIndicatorPlacementState( ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.removedFromGame ); break; }
                    }
                }
                else
                {
                    this._curState = this.createDeckIndicatorPlacementState( ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.removedFromGame ); 
                }
                break;
            }

            case button_type.CANCEL:
            {
                if ( this._stateData.card.cCard.location == location_type.MY_UNDERNEATH || this._stateData.card.cCard.location == location_type.ALLY_UNDERNEATH )
                {
                    let from: GameObject = GameObject.find( this._stateData.card.cCard.locationData );
                    if ( from.cCardToken )
                    {
                        this._curState = this.createActorPlacementState( from );
                    }
                    else if ( from.cGameModifier )
                    {
                        this._curState = this.createGameModifierPlacementState( from );
                    }
                }
                else
                {
                    this._curState = this.createDeckIndicatorPlacementState( ServiceLocator.game.cGameWorld.findLocation( this._stateData.card.cCard.location ) );
                }
                break;
            }
        }

        this._curState.init();
        this._curState.onEnter();
    }

        private createDeckIndicatorPlacementState( deckIndicator: GameObject ): DeckIndicatorPlacementState
        {
            let result: DeckIndicatorPlacementState = new DeckIndicatorPlacementState();
            result.data = this._stateData;
            result.deckIndicator = deckIndicator;
            result.onCompleted.add( this.onPlacement_Completed, this );

            return result;
        }

        private createGameModifierPlacementState( gameModifier: GameObject ): GameModifierPlacementState
        {
            let result: GameModifierPlacementState = new GameModifierPlacementState();
            result.data = this._stateData;
            result.gameModifier = gameModifier;
            result.onCompleted.add( this.onPlacement_Completed, this );

            return result;
        }

        private createActorPlacementState( actor: GameObject ): ActorPlacementState
        {
            let result: ActorPlacementState = new ActorPlacementState();
            result.data = this._stateData;
            result.actor = actor;
            result.onCompleted.add( this.onPlacement_Completed, this );

            return result;
        }

        private createQuestPlacementState(): QuestPlacementState
        {
            let result: QuestPlacementState = new QuestPlacementState();
            result.data = this._stateData;
            if ( Utils.game.isAllyIsolated() )
            {
                ServiceLocator.game.cGameWorld.cCustomPanelManager.show( "isolated" );
                result.questHolder = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "isolated" ).cIsolatedArea.questHolder;
            }
            else
            {
                result.questHolder = ServiceLocator.game.cGameWorld.cSauronArea.questHolder;
            }
            result.onCompleted.add( this.onPlacement_Completed, this );

            return result;
        }

        private createActorAreaPlacementState( actorArea: GameObject ): ActorAreaPlacementState
        {
            let result: ActorAreaPlacementState = new ActorAreaPlacementState();
            result.data = this._stateData;
            if ( Utils.game.isAllyIsolated() )
            {
                ServiceLocator.game.cGameWorld.cCustomPanelManager.show( "isolated" );
                actorArea = this.transformActorAreaForIsolatedArea( actorArea );
            }
            result.actorArea = actorArea;
            result.onCompleted.add( this.onPlacement_Completed, this );

            return result;
        }

            private transformActorAreaForIsolatedArea( actorArea: GameObject ): GameObject
            {
                let result: GameObject = actorArea;

                if ( actorArea.cActorArea.location == location_type.STAGING_AREA )
                {
                    result = ServiceLocator.game.cGameWorld.findLocation( location_type.ISOLATED_STAGING_AREA );
                }
                else if ( actorArea.cActorArea.location == location_type.ALLY_HOME )
                {
                    result = ServiceLocator.game.cGameWorld.findLocation( location_type.ISOLATED_HOME );
                }

                return result;
            }

        private createGameModifierSlotPlacementState( actorArea: GameObject ): GameModifierSlotPlacementState
        {
            let result: GameModifierSlotPlacementState = new GameModifierSlotPlacementState();
            result.data = this._stateData;
            if ( Utils.game.isAllyIsolated() )
            {
                ServiceLocator.game.cGameWorld.cCustomPanelManager.show( "isolated" );
                actorArea = this.transformActorAreaForIsolatedArea( actorArea );
            }
            result.actorArea = actorArea;
            result.onCompleted.add( this.onPlacement_Completed, this );

            return result;
        }

        private createAllySidePlacementState(): AllySidePlacementState
        {
            let result: AllySidePlacementState = new AllySidePlacementState();
            result.data = this._stateData;
            result.onCompleted.add( this.onPlacement_Completed, this );

            return result;
        }

    public onPlacement_Completed(): void
    {
        if ( ServiceLocator.game.isAnimated )
        {
            ServiceLocator.game.allyActionManager.resume();
        }

        this._curState.onLeave();
        this._curState.end();

        this._onCompleted.dispatch();
    }
}

export interface IStateData
{
    card: GameObject;
    disablerBg: GameObject;
    planeContainer: GameObject;
    buttonRowType: button_type;
}