import Component from "../Component";

import IGameObjectDropArea from "../../IGameObjectDropArea";
import GameObject from "../../GameObject";
import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../ServiceLocator";
import { layer_type } from "./CGameLayerProvider";


export default class CGameLayer extends Component implements IGameObjectDropArea
{
    // #region Attributes //

    // private:

    private _layerType: layer_type = null;

    // #endregion //


    // #region Properties //

    public get layerType(): layer_type { return this._layerType; }
    
    public set layerType( value: layer_type ) { this._layerType = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CGameLayer";
    }

    public init(): void
    {
        console.assert( this._go.cContainer != null, "CGameLayer.ts :: init() :: CContainer component not found." );
        console.assert( this._layerType != null, "CGameLayer.ts :: init() :: this._layerType cannot be null." );
              
        super.init();
    }

    // #endregion //


    // #region IGameObjectDropArea //

    public validateDroppedGameObject( dropped: GameObject ): boolean
    {
        return true;
    }

    public processDroppedGameObject( dropped: GameObject, global: PIXI.IPoint, actionScope: action_scope_type ): void
    {
        this._go.cContainer.addChild( dropped );

        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.DROP_AT_DROP_AREA, null, [ dropped.oid, this._go.oid, null ] );
        }
    }

    // #endregion //
}