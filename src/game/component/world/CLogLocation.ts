import Component from "../Component";

import { location_type } from "./CGameWorld";


export default class CLogLocation extends Component 
{
    // #region Attributes //

    // private:

    private _location: location_type = null;

    // #endregion //


    // #region Properties //

    public get location(): location_type { return this._location; }

    public set location( value: location_type ) { this._location = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CLogLocation";
    }

    public init(): void
    {
        console.assert( this._location != null, "CLogLocation.ts :: init() :: this._location cannot be null." );
    }

    // #endregion //
}
