import Component from "../Component";

import GameObject from "../../GameObject";
import CContainer from "../pixi/CContainer";
import CGameLayer from "./CGameLayer";
import CDropArea from "../input/CDropArea";


export default class CGameLayerProvider extends Component
{
    // #region Attributes //

    // private:

    private _arrLayerType: Array<layer_type> = null;

    private _mapGameLayer: Map<layer_type, GameObject> = null;

    // #endregion //


    // #region Properties //
    
    public set layerTypes( value: Array<layer_type> ) { this._arrLayerType = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CGameLayerProvider";
    }

    public init(): void
    {
        super.init();

        console.assert( this._arrLayerType != null, "CGameLayerProvider.ts :: init() :: this._arrLayerType cannot be null." );
    
        this._mapGameLayer = new Map<layer_type, GameObject>();

        this.createLayers();
    }

        private createLayers(): void
        {
            for ( let layerType of this._arrLayerType )
            {
                let layer: GameObject = new GameObject( [ new CContainer(), new CGameLayer() ] );
                layer.cGameLayer.layerType = layerType;
                layer.oid = this._go.oid + "_layer_" + layerType.toString();
                if ( layerType == layer_type.UNDER_TABLE )
                {
                    layer.add( new CDropArea() );
                    layer.cContainer.c.interactiveChildren = false;
                    layer.cDropArea.target = layer.cGameLayer;
                }
                layer.init();
                this._go.cContainer.addChild( layer );
    
                this._mapGameLayer.set( layerType, layer );
            }
        }

    public end(): void
    {
        let entriesIt: IterableIterator<[layer_type, GameObject]> = this._mapGameLayer.entries();
        for ( let entriesItResult: IteratorResult<[layer_type, GameObject]> = entriesIt.next(); !entriesItResult.done; entriesItResult = entriesIt.next() )
        {
            let layer: GameObject = entriesItResult.value[ 1 ] as GameObject;
            if ( layer.cGameLayer.layerType == layer_type.POI )
            {
                for ( let displayObject of layer.cContainer.c.children )
                {
                    gsap.killTweensOf( displayObject );
                }
            }
            layer.end();
        }
        this._mapGameLayer = null;

        this._arrLayerType = null;
    }

    public get( layerType: layer_type ): GameObject
    {
        return this._mapGameLayer.get( layerType );
    }

    public add( gameObject: GameObject, layerType: layer_type ): void
    {
        this._mapGameLayer.get( layerType ).cContainer.addChild( gameObject );
    }

    public reset(): void
    {
        let entriesIt: IterableIterator<[layer_type, GameObject]> = this._mapGameLayer.entries();
        for ( let entriesItResult: IteratorResult<[layer_type, GameObject]> = entriesIt.next(); !entriesItResult.done; entriesItResult = entriesIt.next() )
        {
            let layer: GameObject = entriesItResult.value[ 1 ] as GameObject;
            if ( layer.cGameLayer.layerType == layer_type.POI )
            {
                for ( let displayObject of layer.cContainer.c.children )
                {
                    gsap.killTweensOf( displayObject );
                }
            }
            layer.end();
        }

        this.createLayers();
    }

    // #endregion //
}

export const enum layer_type
{
    UNDER_TABLE = 0,
    GAME,
    ATTACK_BINDING_LINK,
    ATTACK_BINDING_COUNTER,
    POI,
    VFX,
    CUSTOM_PANEL,
    CARD_ACTIVATION_DETECTION,
    ACTIVATED_CARD,
    TARGET_SELECTION,
    HUD,
    CARD_PREVIEW,
    CARD_PRESENTATION,
    POPUP,
    TOOLTIP,
    DISABLED,
    DEBUG
}