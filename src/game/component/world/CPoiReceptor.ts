import Component from "../Component";

import GameObject from "../../GameObject";
import { player_type } from "./CPlayerArea";
import { IPoiMenuItem } from "../ui/CPoiMenu";
import Utils from "../../../Utils";
import { layer_type } from "./CGameLayerProvider";


export default abstract class CPoiReceptor extends Component
{
    // #region Attributes //

    // protected:

    protected _poiSocket: GameObject = null;

    // #endregion //


    // #region Properties //
 
    public get poiSocket(): GameObject { return this._poiSocket; }

    public set poiSocket( value: GameObject ) { this._poiSocket = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CPoiReceptor";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._poiSocket != null, "CPoiReceptor.ts :: init() :: this._poiSocket cannot be null." );
    }

    public end(): void
    {
        this._poiSocket = null;

        super.end();
    }

    // virtual.
    public findPoiContainer(): GameObject
    {
        return Utils.game.findGameProviderLayer( this._go, layer_type.POI );
    }

    public abstract findPoiMenuItems(): Array<IPoiMenuItem>;

    // #endregion //
}