import CPoiReceptor from "../CPoiReceptor";

import { IPoiMenuItem } from "../../ui/CPoiMenu";
import { poi_menu_item_type } from "../../ui/CPoiMenuItem";


export default class CDeckIndicatorPoiReceptor extends CPoiReceptor
{
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CDeckIndicatorPoiReceptor";
    }

    // overrides.
    public findPoiMenuItems(): Array<IPoiMenuItem>
    {
        return [ { type: poi_menu_item_type.HIGHLIGHT } ];
    }

    // #endregion //
}