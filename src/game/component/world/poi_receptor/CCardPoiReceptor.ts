import CPoiReceptor from "../CPoiReceptor";

import { location_type } from "../CGameWorld";
import { player_type } from "../CPlayerArea";

import GameObject from "../../../GameObject";
import { IPoiMenuItem } from "../../ui/CPoiMenu";
import { poi_menu_item_type } from "../../ui/CPoiMenuItem";


export default class CCardPoiReceptor extends CPoiReceptor
{
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CCardPoiReceptor";
    }

    // overrides.
    public findPoiMenuItems(): Array<IPoiMenuItem>
    {
        let result: Array<IPoiMenuItem> = new Array<IPoiMenuItem>();
        
        result.push( { type: poi_menu_item_type.HIGHLIGHT } );
        if ( this._go.cCard.controllerPlayer == player_type.ALLY )
        {
            if ( !this._go.cCard.isFaceUp )
            {
                result.push( { type: poi_menu_item_type.REVEAL } );
            }

            // Common.
            if ( this._go.cCard.location != location_type.SAURON_DISCARD
                && this._go.cCard.location != location_type.ALLY_DISCARD )
            {
                result.push( { type: poi_menu_item_type.DISCARD } );
            }
        }

        return result;
    }

    // overrides.
    public findPoiContainer(): GameObject
    {
        return this._poiSocket;
    }

    // #endregion //
}