import CPoiReceptor from "../CPoiReceptor";

import { poi_menu_item_type } from "../../ui/CPoiMenuItem";
import ServiceLocator from "../../../../ServiceLocator";

import { IPoiMenuItem } from "../../ui/CPoiMenu";
import { player_type } from "../CPlayerArea";


export default class CFirstPlayerTokenPoiReceptor extends CPoiReceptor
{
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CFirstPlayerTokenPoiReceptor";
    }

    // overrides.
    public findPoiMenuItems(): Array<IPoiMenuItem>
    {
        let result: Array<IPoiMenuItem> = new Array<IPoiMenuItem>();
        
        result.push( { type: poi_menu_item_type.HIGHLIGHT } );

        return result;
    }

    // #endregion //
}