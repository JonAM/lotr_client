import CPoiReceptor from "../CPoiReceptor";

import { player_type } from "../CPlayerArea";
import { IPoiMenuItem } from "../../ui/CPoiMenu";
import { poi_menu_item_type } from "../../ui/CPoiMenuItem";
import { location_type } from "../CGameWorld";


export default class CCardTokenPoiReceptor extends CPoiReceptor
{
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CCardTokenPoiReceptor";
    }

    // overrides.
    public findPoiMenuItems(): Array<IPoiMenuItem>
    {
        let result: Array<IPoiMenuItem> = new Array<IPoiMenuItem>();
        
        result.push( { type: poi_menu_item_type.HIGHLIGHT } );

        if ( this._go.cCardToken.controllerPlayer == player_type.ALLY )
        {
            if ( this._go.cCardToken.curSide.cHeroSide || this._go.cCardToken.curSide.cAllySide )
            {
                result.push( { type: this._go.cCardToken.isBowed ? poi_menu_item_type.READY : poi_menu_item_type.BOW } );
                //
                const kProfileValueSignSubmenu: Array<IPoiMenuItem> = this.createValueSignSubmenu();
                let cardTokenProfileSubmenu: Array<IPoiMenuItem> = [];
                cardTokenProfileSubmenu.push( 
                    { type: poi_menu_item_type.ATTACK, submenu: kProfileValueSignSubmenu },
                    { type: poi_menu_item_type.DEFENSE, submenu: kProfileValueSignSubmenu },
                    { type: poi_menu_item_type.WILLPOWER, submenu: kProfileValueSignSubmenu } );
                result.push( { type: poi_menu_item_type.CARD_TOKEN_PROFILE_SUBMENU, submenu: cardTokenProfileSubmenu } );
            }
            
            if ( !this._go.cCardToken.isFaceUp )
            {
                result.push( { type: poi_menu_item_type.REVEAL } );
            }

            result.push( { type: poi_menu_item_type.DISCARD } );
            if ( this._go.cCardToken.location == location_type.ALLY_HOME || this._go.cCardToken.location == location_type.ALLY_ENGAGED )
            {
                result.push({ type: poi_menu_item_type.GIVE_CONTROL } );
            }
        }

        return result;
    }

    // private:

    private createValueSignSubmenu(): Array<IPoiMenuItem>
    {
        const kProfileValueSubmenu: Array<IPoiMenuItem> = [ 
            { type: poi_menu_item_type.ONE },
            { type: poi_menu_item_type.TWO },
            { type: poi_menu_item_type.THREE },
            { type: poi_menu_item_type.FOUR },
            { type: poi_menu_item_type.FIVE } ];
        return [ 
            { type: poi_menu_item_type.ADD, submenu: kProfileValueSubmenu },
            { type: poi_menu_item_type.REMOVE, submenu: kProfileValueSubmenu } ];
    }

    // #endregion //
}