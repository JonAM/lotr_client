import CPoiReceptor from "../CPoiReceptor";

import { player_type } from "../CPlayerArea";
import ServiceLocator from "../../../../ServiceLocator";
import { game_state_id } from "../../../../states/StateId";
import { IPoiMenuItem } from "../../ui/CPoiMenu";
import { poi_menu_item_type } from "../../ui/CPoiMenuItem";


export default class CAttachmentPoiReceptor extends CPoiReceptor
{
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CAttachmentPoiReceptor";
    }

    // virtual.
    public findPoiMenuItems(): Array<IPoiMenuItem>
    {
        let result: Array<IPoiMenuItem> = new Array<IPoiMenuItem>();
        
        result.push( { type: poi_menu_item_type.HIGHLIGHT } );
        if ( ServiceLocator.game.stateMachine.currentStateId > game_state_id.SETUP 
            && this._go.cAttachment.controllerPlayer == player_type.ALLY )
        {
            if ( !this._go.cAttachment.isBowed )
            {
                result.push( { type: poi_menu_item_type.BOW } );
            }
            else
            {
                result.push( { type: poi_menu_item_type.READY } );
            }
            result.push( { type: poi_menu_item_type.DISCARD } );
        }

        return result;
    }

    // #endregion //
}