import CPoiReceptor from "../CPoiReceptor";

import ServiceLocator from "../../../../ServiceLocator";
import { IPoiMenuItem } from "../../ui/CPoiMenu";
import { poi_menu_item_type } from "../../ui/CPoiMenuItem";


export default class CTokenIndicatorPoiReceptor extends CPoiReceptor
{
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CTokenIndicatorPoiReceptor";
    }

    // overrides.
    public findPoiMenuItems(): Array<IPoiMenuItem>
    {
        let result: Array<IPoiMenuItem> = [ { type: poi_menu_item_type.HIGHLIGHT } ];
        if ( this._go == ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.threatLevel )
        {
            const kProfileValueSubmenu: Array<IPoiMenuItem> = [ 
                { type: poi_menu_item_type.ONE },
                { type: poi_menu_item_type.TWO },
                { type: poi_menu_item_type.THREE },
                { type: poi_menu_item_type.FOUR },
                { type: poi_menu_item_type.FIVE } ];
            result.push( { type: poi_menu_item_type.ADD, submenu: kProfileValueSubmenu } );
            result.push( { type: poi_menu_item_type.REMOVE, submenu: kProfileValueSubmenu } );
            result.push( { type: poi_menu_item_type.GIVE, submenu: kProfileValueSubmenu } );
        }

        return result;
    }

    // #endregion //
}