import Component from "../Component";

import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import { player_type } from "./CPlayerArea";
import ServiceLocator from "../../../ServiceLocator";
import { TweenMax, Elastic, Sine, Linear, gsap } from "gsap";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CButton from "../input/CButton";
import CContainer from "../pixi/CContainer";
import CSprite from "../pixi/CSprite";
import CScrollY from "../ui/CScrollY";
import { poi_menu_item_type } from "../ui/CPoiMenuItem";
import LogTargetCard from "../ui/right_menu/action_log/target/LogTargetCard";
import LogTargetPlayer from "../ui/right_menu/action_log/target/LogTargetPlayer";
import CardViewerController from "../ui/viewer/controller/CardViewerController";
import { layer_type } from "./CGameLayerProvider";
import { context_button_type } from "../ui/viewer/CViewContextBtn";


export default class CPoi extends Component
{
    // #region Attributes //

    // private:

    private _poiType: poi_type = null;
    private _creator: player_type = null;
    private _controller: player_type = null;
    private _params: Array<any> = null;

    private _poiPivot: GameObject = null;
    private _poiBtn: GameObject = null;
    private _dismissBtn: GameObject = null;
    private _target: GameObject = null;
    private _poiReceptor: GameObject = null;
    private _scrollY: GameObject = null;
    
    // Binded functions.
    private _bfAutoRemove: any = null;

    // #endregion //


    // #region Properties //

    public get poiType(): poi_type { return this._poiType; }
    public get creator(): player_type { return this._creator; }
    public get target(): GameObject { return this._target; }

    public set poiType( value: poi_type ) { this._poiType = value; }
    public set creator( value: player_type ) { this._creator = value; }
    public set controller( value: player_type ) { this._controller = value; }
    public set target( value: GameObject ) { this._target = value; }
    public set params( value: Array<any> ) { this._params = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CPoi";

        // Binded functions.
        this._bfAutoRemove = this.remove.bind( this );
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CPoi.ts :: init() :: CContainer component not found." );
        console.assert( this._poiType != null, "CPoi.ts :: init() :: this._poiType cannot be null." );
        console.assert( this._controller != null, "CPoi.ts :: init() :: this._controller cannot be null." );

        this._poiPivot = this.createPoiPivot();
        this._go.cContainer.addChild( this._poiPivot );

        this._poiBtn = this.createPoiBtn( this._poiType );
        this._poiPivot.cContainer.addChild( this._poiBtn );
        this._poiBtn.cButton.onClick.add( this.onPoiBtn_Selected, this );

        if ( this._poiType != poi_type.HIGHLIGHT )
        {
            this._dismissBtn = this.createDismissBtn();
            this._go.cContainer.addChild( this._dismissBtn );
            this._dismissBtn.cButton.onClick.add( this.onDismissBtn_Selected, this );
        }

        this._poiPivot.cContainer.c.pivot.set( this._poiPivot.cContainer.c.width * 0.5, this._poiPivot.cContainer.c.height );
        this._go.cContainer.c.visible = false;
    }

    public end(): void
    {
        gsap.killTweensOf( this._bfAutoRemove );
        gsap.killTweensOf( this._go.cContainer.c );
        gsap.killTweensOf( this._go.cContainer.c.scale );

        // Cleanup events.
        if ( this._poiReceptor )
        {
            this._poiReceptor.cPoiReceptor.poiSocket.cContainer.onTransformChanged.remove( this.onPoiSocketTransform_Changed, this );
            this._poiReceptor.cContainer.onVisibleChanged.remove( this.onBranchVisibility_Updated, this );
        }
        
        this._scrollY = null;

        this._poiBtn.end();
        this._poiBtn = null;

        if ( this._dismissBtn )
        {
            this._dismissBtn.end();
            this._dismissBtn = null;
        }

        this._poiType = null;
        this._creator = null;
        this._poiReceptor = null;
        this._target = null;

        this._bfAutoRemove = null;

        super.end();
    }

    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        this._poiBtn.cButton.setEnabled( isEnabled );
        if ( this._dismissBtn )
        {
            this._dismissBtn.cContainer.c.visible = isEnabled;
        }
    }

    public show( poiReceptor: GameObject, playerType: player_type ): void
    {
        // Sfx.
        ServiceLocator.audioManager.playSfx( "poi_created" );

        this._poiReceptor = poiReceptor;
        let poiContainer: GameObject = poiReceptor.cPoiReceptor.findPoiContainer();

        this._go.cContainer.c.visible = true;

        if ( this._poiReceptor.cPoiReceptor.poiSocket != poiContainer )
        {
            this._scrollY = Utils.game.findGameObjectInBranchByComponentName( poiReceptor, CScrollY.name );
            if ( this._scrollY )
            {
                if ( playerType == player_type.ALLY )
                {
                    this._scrollY.cScrollY.scrollToItem( poiReceptor );
                }
            }

            this._poiReceptor.cPoiReceptor.poiSocket.cContainer.onTransformChanged.add( this.onPoiSocketTransform_Changed, this );
            this.onPoiSocketTransform_Changed();
        }
        
        let index: number = poiContainer.cContainer.c.children.length;
        if ( poiContainer.cGameLayer )
        {
            for ( let i: number = poiContainer.cContainer.c.children.length - 1; i >= 0; --i )
            {
                let displayObject: PIXI.DisplayObject = poiContainer.cContainer.c.children[ i ];
                if ( this._go.cContainer.c.y >= displayObject.y 
                    || this._go.cContainer.c.y == displayObject.y && this._go.cContainer.c.x <= displayObject.x )
                {
                    break;
                }

                index -= 1;
            }
        }
        poiContainer.cContainer.addChildAt( this._go, index );

        const kPoiRotation: number = this.findPoiRotation();
        if ( kPoiRotation > 0 )
        {
            this._poiPivot.cContainer.c.rotation = kPoiRotation;
            this._poiBtn.cContainer.c.getChildByName( "poi_icon" ).rotation = -kPoiRotation;
        }
        if ( this._dismissBtn )
        {
            this._dismissBtn.cContainer.c.position.copyFrom( this.findDismissBtnPosition() ); 
        }
        else if ( playerType == player_type.PLAYER || playerType == null )
        {
            gsap.delayedCall( 5, this._bfAutoRemove, [ playerType ] );
        }

        this._go.cContainer.c.scale.set( 0 );
        this._go.cContainer.c.alpha = 0;
        TweenMax.to( this._go.cContainer.c, 0.5, { alpha: 1, ease: Sine.easeOut } );
        let onComplete: ( ...args: any[] ) => void = () => {   
            TweenMax.to( this._go.cContainer.c.scale, 1, { x: 1.05, y: 1.05, ease: Sine.easeInOut, yoyo: true, repeat: -1, delay: 1 } ); };
        TweenMax.to( this._go.cContainer.c.scale, 2, { x: 1, y: 1, ease: Elastic.easeOut, onComplete: onComplete, callbackScope: this } );

        // Listen to events.
        this._poiReceptor.cContainer.onVisibleChanged.add( this.onBranchVisibility_Updated, this );

        // Multiplayer.
        if ( playerType == player_type.PLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SHOW_POI, null, [ poiReceptor.oid, this._go.oid, this._poiType, this._params, this._controller ] );
        }
    }

    public remove( playerType: player_type ): void
    {
        if ( this._go.cContainer )
        {
            if ( playerType == player_type.PLAYER || playerType == null )
            {
                this._poiBtn.cButton.setEnabled( false );
            }
            
            gsap.killTweensOf( this._go.cContainer.c.scale );
            gsap.killTweensOf( this._go.cContainer.c );
            gsap.killTweensOf( this._bfAutoRemove );

            TweenMax.to( this._go.cContainer.c, 0.25, { alpha: 0, ease: Sine.easeIn } );
            TweenMax.to( this._go.cContainer.c.scale, 0.35, { x: 0, y: 0, ease: Linear.easeInOut, callbackScope: this, onComplete: this.onRemoveAnim_Completed } );
        }
        
        // Multiplayer.
        if ( playerType == player_type.PLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.REMOVE_POI, null, [ this._go.oid ] );
        }
    }

    // private: 

    private createPoiPivot(): GameObject
    {
        let result: GameObject = new GameObject( [ new CContainer() ] );
        result.init();

        return result;
    }
    
    private createPoiBtn( poiType: poi_type ): GameObject
    {
        let result: GameObject = new GameObject( [ new CContainer(), new CButton() ] );
        result.init();
        // Bg.
        let poiBg: PIXI.Graphics = new PIXI.Graphics();
        poiBg.beginFill( ServiceLocator.game.playerColors[ this._creator ] );
        poiBg.drawCircle( 40, 40, 40 );
        poiBg.drawPolygon( [ new PIXI.Point( 30, 70 ), new PIXI.Point( 50, 70 ), new PIXI.Point( 40, 110 ) ] );
        poiBg.endFill();
        result.cContainer.c.addChild( poiBg );
        // Icon.
        // Bg.
        let iconBg: PIXI.Graphics = new PIXI.Graphics();
        iconBg.beginFill( 0xffffff );
        iconBg.drawCircle( 40, 40, 30 );
        iconBg.endFill();
        result.cContainer.c.addChild( iconBg );
        //
        let iconContainer: PIXI.Container = null;
        switch ( poiType )
        {
            case poi_type.ADD_VALUE:
            {
                iconContainer = new PIXI.Container();

                const kValue: number = this._params[ 0 ] as number;
                let valueStr: string = kValue.toString();
                if ( kValue > 0 )
                {
                    valueStr = "+" + valueStr;
                }
                let text: PIXI.Text = new PIXI.Text( valueStr, ServiceLocator.game.textStyler.subtitle );
                if ( this._params.length == 1 )
                {
                    text.anchor.set( 0.5, 0.5 );
                    iconContainer.addChild( text );
                }
                else
                {
                    text.anchor.set( 1, 0.5 );
                    iconContainer.addChild( text );
                    //
                    const kValueIconId: string = this.findValueIconId( this._params[ 1 ] as poi_menu_item_type );
                    let valueSprite: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.find( kValueIconId ).data );
                    valueSprite.anchor.set( 0, 0.5 );
                    Utils.game.limitSideSize( 30, valueSprite );
                    iconContainer.addChild( valueSprite );
                }
                break;
            }

            case poi_type.GIVE_VALUE:
            {
                iconContainer = new PIXI.Container();

                const kValue: number = this._params[ 0 ] as number;
                let text: PIXI.Text = new PIXI.Text( kValue.toString(), ServiceLocator.game.textStyler.subtitle );
                text.anchor.set( 0.5, 1 );
                text.y += 5;
                iconContainer.addChild( text );
                //
                let giveSprite: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.find( "give" ).data );
                giveSprite.anchor.set( 0.5, 0.5 );
                Utils.game.limitSideSize( 30, giveSprite );
                giveSprite.y += 10;
                iconContainer.addChild( giveSprite );
                break;
            }

            default:
            {
                const kPoiIconId: string = this.findPoiIconId( poiType );
                let iconSprite: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.find( kPoiIconId ).data );
                iconSprite.anchor.set( 0.5 );
                Utils.game.limitSideSize( 50, iconSprite );
                iconContainer = iconSprite;
            }
        }
        iconContainer.name = "poi_icon";
        iconContainer.position.set( 40, 40 );
        result.cContainer.c.addChild( iconContainer );

        return result;
    }

    private findValueIconId( poiMenuItemType: poi_menu_item_type ): string
    {
        let result: string = null;

        switch ( poiMenuItemType )
        {
            case poi_menu_item_type.ATTACK: { result = "attack"; break; }
            case poi_menu_item_type.DEFENSE: { result = "defense"; break; }
            case poi_menu_item_type.WILLPOWER: { result = "willpower"; break; }
            case poi_menu_item_type.THREAT: { result = "threat"; break; }
        }

        return result;
    }

    private findPoiIconId( poiType: poi_type ): string
    {
        let result: string = null;

        switch ( poiType )
        {
            case poi_type.HIGHLIGHT: { result = "exclamation_mark"; break; }
            case poi_type.BOW: { result = "rad_bow"; break; }
            case poi_type.READY: { result = "rad_ready"; break; }
            case poi_type.DISCARD: { result = "rad_discard"; break; }
            case poi_type.GIVE_CONTROL: { result = "rad_give_control"; break; }
            case poi_type.REVEAL: { result = "rad_put_face_up"; break; }
            case poi_type.RANDOMLY_DISCARD: { result = "randomly_discard"; break; }
        }

        return result;
    }

    private createDismissBtn(): GameObject
    {
        let result: GameObject = new GameObject( [ new CSprite(), new CButton() ] );
        result.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "cancel" ).data );
        Utils.game.limitSideSize( 30, result.cSprite.s );
        result.cSprite.s.anchor.set( 0.5 );
        result.init();

        return result;
    }

    private findPoiRotation(): number
    {
        let result: number = null;

        const kScanDivisionCount: number = 8;
        const kScanRotationStep: number = 2 * Math.PI / kScanDivisionCount;        
        const kScanMatrix: PIXI.Matrix = new PIXI.Matrix();
        kScanMatrix.rotate( kScanRotationStep );
        let gameLayerBounds: PIXI.Rectangle = ServiceLocator.game.root.cGameLayerProvider.get( layer_type.GAME ).cContainer.c.getBounds().clone();
        gameLayerBounds.width = gameLayerBounds.width - 60;
        gameLayerBounds.x = 60;
        gameLayerBounds.height = gameLayerBounds.height - 60;
        gameLayerBounds.y = 60;
        //
        let g: PIXI.Point = this._go.cContainer.c.getGlobalPosition();
        let candidateTLCorner: PIXI.Point = new PIXI.Point( -this._poiPivot.cContainer.c.width * 0.5, -this._poiPivot.cContainer.c.height );
        let candidateTRCorner: PIXI.Point = new PIXI.Point( this._poiPivot.cContainer.c.width * 0.5, -this._poiPivot.cContainer.c.height );
        //
        for ( let i: number = 0; i < kScanDivisionCount; ++i )
        {
            if ( gameLayerBounds.contains( g.x + candidateTLCorner.x, g.y + candidateTLCorner.y )
                && gameLayerBounds.contains( g.x + candidateTRCorner.x, g.y + candidateTRCorner.y ) )
            {
                result = i * kScanRotationStep;
                break;
            }
            else
            {
                candidateTLCorner = kScanMatrix.apply( candidateTLCorner );
                candidateTRCorner = kScanMatrix.apply( candidateTRCorner );
            }
        }

        return result;
    }

    private findDismissBtnPosition(): PIXI.Point
    {
        let result: PIXI.Point = new PIXI.Point();

        let poiBtnIconSprite: PIXI.DisplayObject = this._poiBtn.cContainer.c.getChildByName( "poi_icon" );
        let p: PIXI.Point = poiBtnIconSprite.getGlobalPosition();
        let q: PIXI.Point = this._go.cContainer.c.getGlobalPosition();
        result.x = p.x - q.x + 30;
        result.y = p.y - q.y - 30;

        return result;
    }

    private updateVisibility(): void
    {
        let isVisible: boolean = this._poiReceptor.cContainer.c.worldVisible;
        if ( this._scrollY && isVisible )
        {
            isVisible = isVisible && this._scrollY.cScrollY.checkPointVisibility( this._go.cContainer.c.position );
        }
        this._go.cContainer.c.visible = isVisible;
    }

    // #endregion //


    // #region Callbacks //

    private onPoiBtn_Selected(): void
    {
        switch ( this._poiType )
        {
            case poi_type.BOW:
            {
                if ( this._target.cCardToken )
                {
                    this._target.cCardToken.setBowed( true, action_scope_type.MULTIPLAYER );
                }
                else if ( this._target.cAttachment )
                {
                    this._target.cAttachment.setBowed( true, action_scope_type.MULTIPLAYER );
                }

                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( this._target ), "rad_bow", true );
                break;
            }

            case poi_type.READY:
            {
                if ( this._target.cCardToken )
                {
                    this._target.cCardToken.setBowed( false, action_scope_type.MULTIPLAYER );
                }
                else if ( this._target.cAttachment )
                {
                    this._target.cAttachment.setBowed( false, action_scope_type.MULTIPLAYER );
                }

                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( this._target ), "rad_ready", true );
                break;
            }

            case poi_type.DISCARD:
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( this._target ), "rad_discard", true );

                if ( this._target.cCardToken )
                {
                    this._target.cCardToken.discard( action_scope_type.MULTIPLAYER );
                }
                else if ( this._target.cAttachment )
                {
                    this._target.cAttachment.discard( action_scope_type.MULTIPLAYER );
                }
                else if ( this._target.cShadowCardMini )
                {
                    this._target.cShadowCardMini.discard( action_scope_type.MULTIPLAYER );
                }
                else if ( this._target.cCard )
                {
                    this._target.cCard.discard( action_scope_type.MULTIPLAYER );
                }
                break;
            }

            case poi_type.GIVE_CONTROL:
            {
                this._target.cCardToken.giveControl();
                break;
            }

            case poi_type.REVEAL:
            {
                if ( this._target.cCardToken )
                {
                    this._target.cCardToken.setFaceUp( action_scope_type.MULTIPLAYER );
                }
                else if ( this._target.cCard )
                {
                    this._target.cCard.setFaceUp( action_scope_type.MULTIPLAYER );
                }

                ServiceLocator.game.cGameWorld.cActionLogger.logSingleIcon( player_type.PLAYER, new LogTargetCard( this._target ), "rad_put_face_up", true );
                break;
            }

            case poi_type.ADD_VALUE:
            {
                const kValue: number = this._params[ 0 ] as number;
                let tokenCounter: GameObject = null;
                if ( this._target.cCardToken && this._target.cCardToken.curSide.cHeroSide )
                {
                    switch ( this._params[ 1 ] as poi_menu_item_type )
                    {
                        case poi_menu_item_type.ATTACK: { tokenCounter = this._target.cCardToken.curSide.cHeroSide.attack; break; }
                        case poi_menu_item_type.DEFENSE: { tokenCounter = this._target.cCardToken.curSide.cHeroSide.defense; break; }
                        case poi_menu_item_type.WILLPOWER: { tokenCounter = this._target.cCardToken.curSide.cHeroSide.willpower; break; }
                    
                        // TODO: More attributes.
                    }

                    const kIconId: string = this.findValueIconId( this._params[ 1 ] );
                    ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._target ), kIconId, kValue, null, true );
                }
                else if ( this._target.cCardToken && this._target.cCardToken.curSide.cAllySide )
                {
                    switch ( this._params[ 1 ] as poi_menu_item_type )
                    {
                        case poi_menu_item_type.ATTACK: { tokenCounter = this._target.cCardToken.curSide.cAllySide.attack; break; }
                        case poi_menu_item_type.DEFENSE: { tokenCounter = this._target.cCardToken.curSide.cAllySide.defense; break; }
                        case poi_menu_item_type.WILLPOWER: { tokenCounter = this._target.cCardToken.curSide.cAllySide.willpower; break; }
                    
                        // TODO: More attributes.
                    }

                    const kIconId: string = this.findValueIconId( this._params[ 1 ] );
                    ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._target ), kIconId, kValue, null, true );
                }
                else if ( this._target.cTokenIndicator )
                {
                    tokenCounter = this._target.cTokenIndicator.trigger;

                    ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetPlayer( player_type.PLAYER ), this._target.cTokenIndicator.trigger.cTokenCounter.tokenName, kValue, null, true );
                }
                tokenCounter.cTokenCounter.addCount( kValue, action_scope_type.LOCAL );

                // Multiplayer.
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ADD_TO_TOKEN_COUNTER, null, [ tokenCounter.oid, kValue ] );
                break;
            }

            case poi_type.GIVE_VALUE:
            {
                const kValue: number = this._params[ 0 ] as number;
                this._target.cTokenIndicator.trigger.cTokenCounter.addCount( -kValue, action_scope_type.LOCAL );
                let allyTokenCounter: GameObject = null;
                if ( this._target == ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.threatLevel )
                {
                    allyTokenCounter = ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.threatLevel.cTokenIndicator.trigger;
                }
                allyTokenCounter.cTokenCounter.addCount( kValue, action_scope_type.LOCAL );

                ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetPlayer( player_type.PLAYER ), this._target.cTokenIndicator.trigger.cTokenCounter.tokenName, -kValue, null, true );
                ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetPlayer( player_type.ALLY ), this._target.cTokenIndicator.trigger.cTokenCounter.tokenName, kValue, null, true );

                // Multiplayer.
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ADD_TO_TOKEN_COUNTER, null, [ this._target.cTokenIndicator.trigger.oid, -kValue ] );
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ADD_TO_TOKEN_COUNTER, null, [ allyTokenCounter.oid, kValue ] );
                break;
            }

            case poi_type.RANDOMLY_DISCARD:
            {
                let hand: GameObject = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.hand;
                if ( hand.cDeckIndicator.panel.cViewer.cCardView.findItems().length > 0 )
                {
                    ( hand.cDeckIndicator.panel.cViewer.controller as CardViewerController ).onContextBtnSelected( context_button_type.RANDOMLY_DISCARD, null );
                }
                break;
            }
        }

        this.remove( this._creator == null ? null : player_type.PLAYER );
    }

    private onDismissBtn_Selected(): void
    {
        this.remove( player_type.PLAYER );
    }

    private onRemoveAnim_Completed(): void
    {
        this._go.end();
    }

    private onPoiSocketTransform_Changed(): void
    {
        const kGameLayerProvider: GameObject = Utils.game.findGameObjectInBranchByComponentName( this._poiReceptor, "CGameLayerProvider" );
        this._go.cContainer.c.position.copyFrom( kGameLayerProvider.cContainer.c.toLocal( new PIXI.Point(), this._poiReceptor.cPoiReceptor.poiSocket.cContainer.c ) );
        
        this.updateVisibility();
    }

    private onBranchVisibility_Updated( isVisible: boolean ): void
    {
        this.updateVisibility();
    }

    // #endregion //
}

export const enum poi_type
{
    // IMPORTANT: Same order as in poi_menu_item_type.
    HIGHLIGHT = 0,
    BOW,
    READY,
    DISCARD,
    GIVE_CONTROL,
    REVEAL,
    RANDOMLY_DISCARD,
    // 
    ADD_VALUE,
    GIVE_VALUE
}