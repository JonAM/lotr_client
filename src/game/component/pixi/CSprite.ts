import CContainer from "./CContainer";

import * as PIXI from "pixi.js";


export default class CSprite extends CContainer
{
    // #region Attributes //

    // private:

    private _sprite: PIXI.Sprite = null;

    // #endregion //


    // #region Properties //

    public get s(): PIXI.Sprite { return this._sprite; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CSprite";

        this._sprite = this._container as PIXI.Sprite;
    }

    // protected:

    // virtual.
    protected createPixiObject(): PIXI.Container
    {
        return new PIXI.Sprite();
    }

    // #endregion //
}