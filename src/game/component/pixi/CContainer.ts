import Component from "../Component";

import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import Signal from "../../../lib/signals/Signal";


export default class CContainer extends Component
{
    // #region Attributes //

    // protected:

    protected _container: PIXI.Container = null;

    // Signals.
    private _onTranformChanged: Signal = new Signal();
    private _onVisibleChanged: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get c(): PIXI.Container { return this._container; }

    // Signals.
    public get onTransformChanged(): Signal { return this._onTranformChanged; }
    public get onVisibleChanged(): Signal { return this._onVisibleChanged; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CContainer";

        this._container = this.createPixiObject();
        this._container.transform = new CustomContainerTransform();

        delete this._container.visible;
        Object.defineProperty( this._container, "_visible", {
            value: true,
            writable: true,
            configurable: true,
            enumerable: true } );
        Object.defineProperty( this._container, "visible", {
            get: function() { return this._visible; },
            set: function( value ) { 
                this._visible = value; 
                CContainer.propagateVisibleChange( this, value ); 
            } } );
    }

    public init(): void
    {
        super.init();

        this._container[ "go" ] = this._go;

        // Listen to events.
        ( this._container.transform as CustomContainerTransform ).onChanged.add( this.onTransform_Changed, this );
    }

    public end(): void
    {
        this._onTranformChanged.removeAll();
        this._onVisibleChanged.removeAll();

        if ( this._container.parent )
        {
            this._container.parent.removeChild( this._container );
        }

        // Cleanup events.
        ( this._container.transform as CustomContainerTransform ).onChanged.remove( this.onTransform_Changed, this );
    }

    public addChild( gameObject: GameObject ): void
    {
        this._container.addChild( gameObject.cContainer.c );
    }

    public addChildAt( gameObject: GameObject, index: number ): void
    {
        this._container.addChildAt( gameObject.cContainer.c, index );
    }

    // global.
    public static propagateVisibleChange( displayObject: PIXI.DisplayObject, isVisible: boolean ): void
    {
        let go: GameObject = displayObject[ "go" ];
        if ( go != undefined && go.cContainer )
        {
            go.cContainer.onVisibleChanged.dispatch( isVisible );
        }

        if ( displayObject instanceof PIXI.Container )
        {
            for ( let child of displayObject.children )
            {
                CContainer.propagateVisibleChange( child, isVisible );
            }
        }
    }

    // protected:

    // virtual.
    protected createPixiObject(): PIXI.Container
    {
        return new PIXI.Container;
    }

    // #endregion //


    // #region Callbacks //

    public onTransform_Changed(): void
    {
        this._onTranformChanged.dispatch();

        this.propagateOnTransformEvent( this._go.cContainer.c );
    }

        private propagateOnTransformEvent( container: PIXI.Container ): void
        {
            for ( let displayObject of container.children )
            {
                if ( displayObject instanceof PIXI.Container )
                {
                    let go: GameObject = displayObject[ "go" ];
                    if ( go != undefined && go.cContainer )
                    {
                        go.cContainer.onTransformChanged.dispatch();
                    }

                    this.propagateOnTransformEvent( displayObject );
                }
            }
        }

    // #endregion //
}

class CustomContainerTransform extends PIXI.Transform
{
    private _onChanged: Signal = new Signal();

    public get onChanged(): Signal { return this._onChanged; }

    protected onChange(): void
    {
        super.onChange();

        this._onChanged.dispatch();
    }
}