import CContainer from "./CContainer";

import * as PIXI from "pixi.js";


export default class CGraphics extends CContainer
{
    // #region Attributes //

    // private:

    private _graphics: PIXI.Graphics = null;

    // #endregion //


    // #region Properties //

    public get g(): PIXI.Graphics { return this._graphics; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CGraphics";

        this._graphics = this._container as PIXI.Graphics;
    }

    // protected:

    // virtual.
    protected createPixiObject(): PIXI.Container
    {
        return new PIXI.Graphics();
    }

    // #endregion //
}