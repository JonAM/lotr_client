import CSprite from "./CSprite";

import * as PIXI from "pixi.js";
import ServiceLocator from "../../../ServiceLocator";


export default class CTilingSprite extends CSprite
{
    // #region Attributes //

    // private:

    private _tilingSprite: PIXI.TilingSprite = null;

    // #endregion //


    // #region Properties //

    // public get sprite(): PIXI.Sprite { return this._sprite; }
    // public get anchor(): PIXI.Point { return this._sprite.anchor; }

    // public set texture( value: PIXI.Texture ) { this._sprite.texture = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CTilingSprite";

        this._tilingSprite = this._container as PIXI.TilingSprite;
    }

    // protected:

    // virtual.
    protected createPixiObject(): PIXI.Container
    {
        return new PIXI.TilingSprite( ServiceLocator.resourceStack.find( "transparent" ).data );
    }

    // #endregion //
}