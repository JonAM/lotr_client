import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CSprite from "../pixi/CSprite";
import { IOption, option_type } from "../ui/CRadialMenu";
import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import Signal from "../../../lib/signals/Signal";
import CCardPreviewable from "../input/CCardPreviewable";
import { ISgCard } from "../../../view/game/SaveGameView";
import IDragShadowTextureCreator from "../../IDragShadowTextureCreator";


export default class CShadowCardMini extends Component implements IDragShadowTextureCreator
{
    // #region Attributes //

    // private:

    private _card: GameObject = null;

    private _isAnimated: boolean = null;
    private _front: GameObject = null;
    private _back: GameObject = null;

    // Signals.
    private _onDiscarded: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get card(): GameObject { return this._card; }
    public get isFaceUp(): boolean { return this._front.cContainer.c.visible; }

    public set card( value: GameObject ) { this._card = value; }

    // Signals.
    public get onDiscarded(): Signal { return this._onDiscarded; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CShadowCardMini";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CShadowCardMini.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cDraggable != null, "CShadowCardMini.ts :: init() :: CDraggable component not found." );
        console.assert( this._go.cButton != null, "CShadowCardMini.ts :: init() :: CButton component not found." );
        console.assert( this._card != null, "CShadowCardMini.ts :: init() :: this._card cannot be null." );

        this._isAnimated = true;

        // Portrait.
        let portrait: PIXI.Container = new PIXI.Container();
        this._go.cContainer.c.addChild( portrait );
        // Front.
        this._front = new GameObject( [ new CSprite(), new CCardPreviewable() ] );
        this._front.cSprite.s.texture = Utils.img.createCardPortrait( this._card.cCard.front.cardId );
        Utils.game.limitSideSize( 70, this._front.cSprite.s );
        this._front.cSprite.s.anchor.set( 0.5 );
        this._front.cCardPreviewable.cardId = this._card.cCard.front.cardId;
        this._front.init();
        this._front.cContainer.c.visible = this._card.cCard.isFaceUp;
        portrait.addChild( this._front.cContainer.c );
        // Back.
        this._back = new GameObject( [ new CSprite() ] );
        this._back.cSprite.s.texture = Utils.img.createCardPortrait( this._card.cCard.back.cardId );
        Utils.game.limitSideSize( 70, this._back.cSprite.s );
        this._back.cSprite.s.anchor.set( 0.5 );
        this._back.init();
        this._back.cContainer.c.visible = !this._card.cCard.isFaceUp;
        portrait.addChild( this._back.cContainer.c );
        // Mask.
        let mask: PIXI.Graphics = new PIXI.Graphics();
        mask.beginFill();
        mask.moveTo( -20, 0 );
        mask.lineTo( 0, -20 );
        mask.lineTo( 20, 0 );
        mask.lineTo( 0, 20 );
        mask.lineTo( -20, 0 );
        mask.endFill();
        portrait.addChild( mask );
        portrait.mask = mask;

        // Border.
        let border: PIXI.Graphics = new PIXI.Graphics();
        border.lineStyle( 2, ServiceLocator.game.playerColors[ this._card.cCard.ownerPlayer ] );
        border.moveTo( -20, 0 );
        border.lineTo( 0, -20 );
        border.lineTo( 20, 0 );
        border.lineTo( 0, 20 );
        border.lineTo( -20, 0 );
        this._go.cContainer.c.addChild( border );

        // Listen to events.
        this._go.cButton.onClick.add( this.onRadialMenu_Requested, this );
    }

    public end(): void
    {
        this._onDiscarded.removeAll();

        // Cleanup events.
        this._go.cButton.onClick.remove( this.onRadialMenu_Requested, this );

        this._card = null;
        this._isAnimated = null;

        this._front.end();
        this._front = null;

        this._back.end();
        this._back = null;

        super.end();
    }

    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        this._go.cDraggable.setEnabled( isEnabled );
        this._go.cButton.setEnabled( isEnabled );
    }

    public discard( actionScope: action_scope_type ): void
    {
        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.DISCARD_SHADOW_CARD, null, [ this._go.oid ] );
        }

        Utils.game.discard( this._card, action_scope_type.LOCAL );

        this._onDiscarded.dispatch( this._go );

        this._go.end();
    }

    public setFaceUp( actionScope: action_scope_type ): void
    {
        if ( this._front.cContainer.c.visible ) { return; }

        if ( this._isAnimated && ServiceLocator.game.isAnimated )
        {
            Utils.anim.bump( this._go.cContainer.c, this.onFaceUpAnimRepeatCb.bind( this ) );
        }
        else
        {
            this._front.cContainer.c.visible = true;
            this._back.cContainer.c.visible = false;
        }
        
        this._card.cCard.isAnimated = false;
        this._card.cCard.setFaceUp( actionScope );
        this._card.cCard.isAnimated = true;

        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_SHADOW_CARD_FACE_UP, null, [ this._go.oid ] );
        }
    }

        private onFaceUpAnimRepeatCb(): void
        {
            // Sfx.
            ServiceLocator.audioManager.playSfx( "card_face_up" );

            if ( ServiceLocator.game.cardPreview.cCardPreview.isTarget( this._back ) )
            {  
                ServiceLocator.game.cardPreview.cCardPreview.hide();
            }

            this._front.cContainer.c.visible = true;
            this._back.cContainer.c.visible = false; 

            this._front.cCardPreviewable.refresh();
        }

    public setFaceDown( actionScope: action_scope_type ): void
    {
        if ( this._back.cContainer.c.visible ) { return; }

        if ( this._isAnimated && ServiceLocator.game.isAnimated )
        {
            Utils.anim.bump( this._go.cContainer.c, this.onFaceDownAnimRepeatCb.bind( this ) );
        }
        else
        {
            this._front.cContainer.c.visible = false;
            this._back.cContainer.c.visible = true;
        }

        this._card.cCard.isAnimated = false;
        this._card.cCard.setFaceDown( actionScope );
        this._card.cCard.isAnimated = true;

        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_SHADOW_CARD_FACE_DOWN, null, [ this._go.oid ] );
        }
    }

        private onFaceDownAnimRepeatCb(): void
        {
            // Sfx.
            ServiceLocator.audioManager.playSfx( "card_face_down" );

            if ( ServiceLocator.game.cardPreview.cCardPreview.isTarget( this._back ) )
            {  
                ServiceLocator.game.cardPreview.cCardPreview.hide();
            }

            this._front.cContainer.c.visible = false;
            this._back.cContainer.c.visible = true; 

            this._front.cCardPreviewable.refresh();
        }

    // overrides.
    public saveGame(): ISgCard
    {
        return this._card.cCard.saveGame();
    }

    // overrides.
    public loadGame( sgCard: ISgCard, pass: number ): void
    {
        this._isAnimated = false;
        if ( sgCard.isFaceUp )
        {
            this.setFaceUp( action_scope_type.LOCAL );
        }
        else
        {
            this.setFaceDown( action_scope_type.LOCAL );
        }
        this._isAnimated = true;
    }

    // #endregion //


    // #region IDragShadowTextureCreator //

    public createDragShadowTexture(): PIXI.Texture
    {
        let dragShadowTexture = PIXI.RenderTexture.create( { width: this._go.cContainer.c.width, height: this._go.cContainer.c.height } );
        let pos: PIXI.Point = new PIXI.Point();
        this._go.cContainer.c.position.copyTo( pos );
        this._go.cContainer.c.position.set( this._go.cContainer.c.width * 0.5, this._go.cContainer.c.height * 0.5 );
        ServiceLocator.game.app.renderer.render( this._go.cContainer.c, dragShadowTexture );    
        pos.copyTo( this._go.cContainer.c.position );

        return dragShadowTexture;
    }

    // #endregion //


    // #region Callbacks //

    private onRadialMenu_Requested( target: GameObject ): void
    {
        if ( ServiceLocator.game.poiMenu.cContainer.c.visible
            || Utils.game.findGameObjectInBranchByComponentName( target, "CPoi" )
            || ServiceLocator.game.dragShadowManager.dragShadow ) 
        { 
            return; 
        }
        
        let arrOption: Array<IOption> = [
            { id: this._card.cCard.isFaceUp ? option_type.PUT_FACE_DOWN : option_type.PUT_FACE_UP, isEnabled: true },
            { id: option_type.DISCARD, isEnabled: true } ];
        ServiceLocator.game.cRadialMenu.tryShow( this._go, this._go.cContainer.c.getGlobalPosition(), arrOption );
    }

    // #endregion //
}