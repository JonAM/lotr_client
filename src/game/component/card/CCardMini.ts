import Component from "../Component";

import Session from "../../../Session";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";
import ServiceLocator from "../../../ServiceLocator";


export default class CCardMini extends Component
{
    private _cardId: string = null;
    private _radius: number = 25;
    private _borderColor: number = ServiceLocator.game.playerColors[ 0 ];

    public get cardId(): string { return this._cardId; }

    public set cardId( value: string ) { this._cardId = value; }
    public set radius( value: number ) { this._radius = value; }
    public set borderColor( value: number ) { this._borderColor = value; }

    public constructor()
    {
        super();

        this._id = "CCardMini";
    }

    public init(): void
    {
        console.assert( this._go.cContainer != null, "CCardMini.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cCardPreviewable != null, "CCardMini.ts :: init() :: CCardPreviewable component not found." );
        console.assert( this._cardId != null, "CCardMini.ts :: init() :: this._cardId cannot be null." );

        this._go.cCardPreviewable.cardId = this._cardId;

        this.createPortrait();
        this.createBorder();
    }

    // private:

    private createPortrait(): PIXI.Sprite
    {
        let s: PIXI.Sprite = new PIXI.Sprite( Utils.img.createCardPortrait( this._cardId ) );
        const kScale = ( this._radius * 2 ) / Math.min( s.texture.width, s.texture.height );
        s.scale.set( kScale, kScale );
        this._go.cContainer.c.addChild( s );
        s.mask = this.createMask();

        return s;
    }

    private createMask(): PIXI.Graphics
    {
        let g: PIXI.Graphics = new PIXI.Graphics();
        g.beginFill();
        g.drawCircle( this._radius, this._radius, this._radius );
        g.endFill();
        this._go.cContainer.c.addChild( g );

        return g;
    }

    private createBorder(): PIXI.Graphics
    {
        let g: PIXI.Graphics = new PIXI.Graphics();
        g.lineStyle( 3, this._borderColor );
        g.drawCircle( this._radius, this._radius, this._radius );
        this._go.cContainer.c.addChild( g );

        return g;
    }
}