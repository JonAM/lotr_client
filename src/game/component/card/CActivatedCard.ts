import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CSprite from "../pixi/CSprite";
import { IOption, option_type } from "../ui/CRadialMenu";
import { target_selection_type } from "../input/CTargetSelector";
import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import { player_type } from "../world/CPlayerArea";
import LogTargetCard from "../ui/right_menu/action_log/target/LogTargetCard";
import CardPresentationUtils, { button_type } from "../../../states/game/setup/CardPresentationUtils";


export default class CActivatedCard extends Component
{
    // #region Attributes //

    // private:

    private _card: GameObject = null;
    private _targetSelectionType: target_selection_type = null;

    private _portrait: GameObject = null;

    // #endregion //


    // #region Properties //

    public get card(): GameObject { return this._card; }

    public set card( value: GameObject ) { this._card = value; }
    public set targetSelectionType( value: target_selection_type ) { this._targetSelectionType = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CActivatedCard";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CActivatedCard.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cButton != null, "CActivatedCard.ts :: init() :: CButton component not found." );
        console.assert( this._card != null, "CActivatedCard.ts :: init() :: this._card cannot be null." );
        console.assert( this._targetSelectionType != null, "CActivatedCard.ts :: init() :: this._targetSelectionType cannot be null." );

        // Portrait.
        this._portrait = this.createPortrait();
        Utils.game.limitSideSize( 300, this._portrait.cSprite.s );
        this._go.cContainer.addChild( this._portrait );

        // Border.
        let border: PIXI.Graphics = new PIXI.Graphics();
        border.lineStyle( 2, ServiceLocator.game.playerColors[ this._card.cCard.ownerPlayer ] );
        border.drawRect( 0, 0, this._go.cContainer.c.width, this._go.cContainer.c.height );
        this._go.cContainer.c.addChild( border );

        // Listen to events.
        this._go.cButton.onClick.add( this.onRadialMenu_Requested, this );
        this._go.cTargetSelector.onTargetSelected.add( this.onTarget_Selected, this );
    }

    public end(): void
    {
        // Cleanup events.
        this._go.cButton.onClick.remove( this.onRadialMenu_Requested, this );
        this._go.cTargetSelector.onTargetSelected.remove( this.onTarget_Selected, this );

        this._card = null;
        this._targetSelectionType = null;

        this._portrait.end();
        this._portrait = null;

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.CARD_ACTIVATION_DISCARD, null, null );

        super.end();
    }

    public discard( actionScopeType: action_scope_type ): void
    {
        Utils.game.discard( this._card, actionScopeType );

        this._go.end();
    }

    // private:

    private createPortrait(): GameObject
    {
        let portrait: GameObject = new GameObject( [ new CSprite() ] );
        portrait.cSprite.s.texture = Utils.img.findCardTexture( this._card.cCard.curSide.cardId );
        portrait.init();

        return portrait;
    }

    // #endregion //


    // #region Callbacks //

    private onRadialMenu_Requested( target: GameObject ): void
    {
        if ( ServiceLocator.game.poiMenu.cContainer.c.visible
            || Utils.game.findGameObjectInBranchByComponentName( target, "CPoi" )
            || ServiceLocator.game.dragShadowManager.dragShadow ) 
        { 
            return; 
        }
        
        let showAtPos: PIXI.Point = this._go.cContainer.c.getGlobalPosition();
        showAtPos.x += this._go.cContainer.c.width * 0.5;
        showAtPos.y += this._go.cContainer.c.height * 0.5;

        let cpu: CardPresentationUtils = new CardPresentationUtils();
        const kArrButtonType: Array<button_type> = cpu.findContextControls( this._card )
        let arrOption: Array<IOption> = new Array<IOption>();
        if ( kArrButtonType.indexOf( button_type.TARGET ) != -1 )
        {
            arrOption.push( { id: option_type.TARGET, isEnabled: true } );
        }
        if ( kArrButtonType.indexOf( button_type.EQUIP ) != -1 )
        {
            arrOption.push( { id: option_type.EQUIP, isEnabled: true } );
        }
        if ( kArrButtonType.indexOf( button_type.SHADOW_CARD ) != -1 )
        {
            arrOption.push( { id: option_type.SHADOW_CARD, isEnabled: true } );
        }
        arrOption.push( 
            { id: option_type.UNDERNEATH, isEnabled: true }, 
            { id: option_type.DISCARD, isEnabled: true } );
        ServiceLocator.game.cRadialMenu.tryShow( this._go, showAtPos, arrOption );
    }

    private onTarget_Selected( target: GameObject, targetSelectionType: target_selection_type ): void
    {
        switch ( targetSelectionType )
        {
            case target_selection_type.TARGET:
            {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "button_click" );

                ServiceLocator.game.cGameWorld.cActionLogger.logTargetSelection( 
                    player_type.PLAYER, new LogTargetCard( this._card ), new LogTargetCard( target ), "rad_target", false, true );
                break;
            }
            
            case target_selection_type.EQUIP:
            {
                // IMPORTANT: Logged in CCardToken.

                // Sfx.
                ServiceLocator.audioManager.playSfx( "equipped" );

                target.cCardTokenSide.cardToken.cDropArea.forceDrop( this._card, new PIXI.Point(), action_scope_type.MULTIPLAYER );

                this._go.end();
                break;
            }

            case target_selection_type.SHADOW_CARD:
            {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "button_click" );

                ServiceLocator.game.cGameWorld.cActionLogger.logTargetSelection( 
                    player_type.PLAYER, new LogTargetCard( this._card ), new LogTargetCard( target ), "rad_shadow_card", false, true );

                target.cEnemySide.addShadowCard(  this._card, action_scope_type.MULTIPLAYER );

                this._go.end();
                break;
            }

            case target_selection_type.UNDERNEATH:
            {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "button_click" );
                
                ServiceLocator.game.cGameWorld.cActionLogger.logTargetSelection( 
                    player_type.PLAYER, new LogTargetCard( this._card ), new LogTargetCard( target ), "rad_underneath", false, true );

                if ( target.cCardTokenSide )
                {
                    target.cCardTokenSide.cardToken.cCardToken.addCardUnderneath( this._card, action_scope_type.MULTIPLAYER );
                }
                else if ( target.cGameModifier )
                {
                    target.cGameModifier.addCardUnderneath( this._card, action_scope_type.MULTIPLAYER );
                }

                this._go.end();
                break;
            }
        }
    }

    // #endregion //
}
