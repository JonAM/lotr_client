import Component from "../Component";

import { location_type } from "../world/CGameWorld";
import { OutlineFilter } from "pixi-filters";
import { player_action_type, action_scope_type } from "../../../service/socket_io/GameSocketIOController";
import { player_type } from "../world/CPlayerArea";
import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import Signal from "../../../lib/signals/Signal";
import CContainer from "../pixi/CContainer";
import CDraggable from "../input/CDraggable";
import cDetailBar, { detail_bar_icon_type, IDetailBarItem } from "./token/CDetailBar";
import LogTargetCard from "../ui/right_menu/action_log/target/LogTargetCard";
import { ISgCardToken, ISgAttachment } from "../../../view/game/SaveGameView";
import DroppedEmitter from "../../DroppedEmitter";
import CScrollY from "../ui/CScrollY";
import CAttachment from "./token/CAttachment";
import CAttachmentPoiReceptor from "../world/poi_receptor/CAttachmentPoiReceptor";
import IGameObjectDropArea from "../../IGameObjectDropArea";
import ProcScrollToTarget from "../../process_sequencer/ProcScrollToTarget";
import ProcBumpFromCorner from "../../process_sequencer/ProcBumpFromCorner";
import ProcFlash from "../../process_sequencer/ProcFlash";
import CTargetSelector, { target_selection_type } from "../input/CTargetSelector";
import CScrollX from "../ui/CScrollX";
import IDragShadowTextureCreator from "../../IDragShadowTextureCreator";
import CDropArea from "../input/CDropArea";
import CShareableGameElement from "../CShareableGameElement";
import CCardTokenSide, { status_type } from "./token/CCardTokenSide";
import CardTokenSideFactory from "../../CardTokenSideFactory";
import { layer_type } from "../world/CGameLayerProvider";
import FloatingMessage from "../../FloatingMessage";
import { ICardSide } from "./CCard";
import CCardView from "../ui/viewer/CCardView";
import CViewer from "../ui/CViewer";
import CardTokenFactory from "../../CardTokenFactory";
import CGraphics from "../pixi/CGraphics";
import CAnimatedFrame from "../ui/CAnimatedFrame";


export default class CCardToken extends Component implements IGameObjectDropArea, IDragShadowTextureCreator
{
    // #region Attributes //

    // private:

    private _frontSideCardId: string = null;
    private _backSideCardId: string = null;
    private _ownerPlayer: player_type = null;
    private _controllerPlayer: player_type = null;
    private _location: location_type = null;

    private _shadow: PIXI.Sprite = null;
    private _sideContainer: PIXI.Container = null;
    private _frontSide: GameObject = null;
    private _backSide: GameObject = null;
    private _curSide: GameObject = null;
    private _frame: PIXI.Graphics = null;
    private _animatedFrame: GameObject = null;
    private _poiSocket: GameObject = null;
    private _detailBar: GameObject = null;
    private _attachmentHolder: GameObject = null;
    
    private _isAnimated: boolean = true;
	private _isBowed: boolean = false;
    private _isFaceUp: boolean = true;
    private _isHighlighted: boolean = false;
    
    // Signals.
    private _onFaceUp: Signal = new Signal();
    private _onFaceDown: Signal = new Signal();
    private _onFlipped: Signal = new Signal();
    private _onBowed: Signal = new Signal();
    private _onReady: Signal = new Signal();
    private _onSizeUpdated: Signal = new Signal();
    private _onCardAttached: Signal = new Signal();
    private _onCardDetached: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get isBowed(): boolean { return this._isBowed; }
    public get isFaceUp(): boolean { return this._isFaceUp; }
    public get ownerPlayer(): player_type { return this._ownerPlayer; }
    public get controllerPlayer(): player_type { return this._controllerPlayer; }
    public get location(): location_type { return this._location; }
    public get frontSide(): GameObject { return this._frontSide; }
    public get cFrontSide(): CCardTokenSide { return this._frontSide.cCardTokenSide; }
    public get backSide(): GameObject { return this._backSide; }
    public get cBackSide(): CCardTokenSide { return this._backSide.cCardTokenSide; }
    public get curSide(): GameObject { return this._curSide; }
    public get cCurSide(): CCardTokenSide { return this._curSide.cCardTokenSide; }
    public get altSide(): GameObject { return this._curSide == this._frontSide ? this._backSide : this._frontSide; }
    public get cAltSide(): CCardTokenSide { return ( this._curSide == this._frontSide ? this._backSide : this._frontSide ).cCardTokenSide; }
    public get frameWidth(): number { return this._frame.width; }
    public get frameHeight(): number { return this._frame.height; }
    public get animatedFrame(): GameObject { return this._animatedFrame; }
    public get isAnimated(): boolean { return this._isAnimated; }
    public get cDetailBar(): cDetailBar { return this._detailBar.cDetailBar; }
    public get attachmentHolder(): GameObject { return this._attachmentHolder; }

    public set frontSideCardId( value: string ) { this._frontSideCardId = value; }
    public set backSideCardId( value: string ) { this._backSideCardId = value; }
    public set ownerPlayer( value: player_type ) { this._ownerPlayer = value; }
    public set controllerPlayer( value: player_type ) { this._controllerPlayer = value; }
    public set location( value: location_type ) 
    { 
        let isChanged: boolean = this._location != null && this._location != value;
        this._location = value; 
        if ( isChanged )
        {
            this.onLocationChanged();
        }
    }
    public set isFaceUp( value: boolean ) { this._isFaceUp = value; }
    public set isBowed( value: boolean ) { this._isBowed = value; }
    public set isAnimated( value: boolean ) { this._isAnimated = value; }

    // Signals.
    public get onFaceUp(): Signal { return this._onFaceUp; }
    public get onFaceDown(): Signal { return this._onFaceDown; }
    public get onFlipped(): Signal { return this._onFlipped; }
    public get onBowed(): Signal { return this._onBowed; }
    public get onReady(): Signal { return this._onReady; }
    public get onSizeUpdated(): Signal { return this._onSizeUpdated; }
    public get onCardAttached(): Signal { return this._onCardAttached; }
    public get onCardDetached(): Signal { return this._onCardDetached; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CCardToken";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CCardToken.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cDropArea != null, "CCardToken.ts :: init() :: CDropArea component not found." );
        console.assert( this._go.cCardTokenPoiReceptor != null, "CCardToken.ts :: init() :: CCardTokenPoiReceptor component not found." );
        console.assert( this._go.cShareableGameElement != null, "CCardToken.ts :: init() :: CShareableGameElement component not found." );
        console.assert( this._frontSideCardId != null, "CCardToken.ts :: init() :: this._frontSideCardId cannot be null." );
        console.assert( this._backSideCardId != null, "CCardToken.ts :: init() :: this._backSideCardId cannot be null." );
        console.assert( this._ownerPlayer != null, "CCardToken.ts :: init() :: this._ownerPlayer cannot be null." );
        console.assert( this._controllerPlayer != null, "CCardToken.ts :: init() :: this._controllerPlayer cannot be null." );
        
        this._go.cContainer.c.interactive = true;
        this._go.cShareableGameElement.setEnabled( Utils.game.isSharedLocation( this._location ) );

        // Side.
        this._sideContainer = new PIXI.Container();
        this._sideContainer.position.set( 1, 1 );
        this._go.cContainer.c.addChild( this._sideContainer );
        // Front side.
        let ctsf: CardTokenSideFactory = new CardTokenSideFactory();
        this._frontSide = ctsf.create( this._frontSideCardId, this._go );
        this._frontSide.cContainer.c.visible = this._isFaceUp;
        this._sideContainer.addChild( this._frontSide.cContainer.c );
        // Back side.
        this._backSide = ctsf.create( this._backSideCardId, this._go );
        this._backSide.cContainer.c.visible = !this._isFaceUp;
        this._sideContainer.addChild( this._backSide.cContainer.c );
        //
        this._curSide = this._isFaceUp ? this._frontSide : this._backSide;

        // Shadow.
        this._shadow = this.createShadow();
        this._go.cContainer.c.addChildAt( this._shadow, 0 );

        // Frame.
        this._frame = new PIXI.Graphics();
        this._frame.lineStyle( 2, ServiceLocator.game.playerColors[ this._ownerPlayer ] );
        this._frame.drawRect( 0, 0, this._curSide.cCardTokenSide.bg.cContainer.c.width + 2, this._curSide.cCardTokenSide.bg.cContainer.c.height + 2 );
        this._go.cContainer.c.addChildAt( this._frame, 1 );

        // Animated frame.
        this._animatedFrame = new GameObject( [ new CGraphics(), new CAnimatedFrame() ] );
        this._animatedFrame.cAnimatedFrame.width = this._curSide.cCardTokenSide.bg.cContainer.c.width + 2;
        this._animatedFrame.cAnimatedFrame.height = this._curSide.cCardTokenSide.bg.cContainer.c.height + 2;
        this._animatedFrame.cAnimatedFrame.lineWidth = 2;
        this._animatedFrame.init();
        this._go.cContainer.addChildAt( this._animatedFrame, 2 );

        // Attachment holder.
        this._attachmentHolder = new GameObject( [ new CContainer(), new CScrollY() ] );
        const kAttachmentHolderItemHeight: number = 60;
        const kAttachmentHolderItemCount: number = Math.floor( this._frame.height / kAttachmentHolderItemHeight );
        this._attachmentHolder.cScrollY.width = 43;
        this._attachmentHolder.cScrollY.height = kAttachmentHolderItemHeight * kAttachmentHolderItemCount;
        this._attachmentHolder.cScrollY.itemHeight = kAttachmentHolderItemHeight;
        this._attachmentHolder.init();
        this._attachmentHolder.cContainer.c.position.set( this._frame.width, 0 );
        this._attachmentHolder.cContainer.c.visible = false;
        this._go.cContainer.addChildAt( this._attachmentHolder, 1 );

        // Detail bar.
        this._detailBar = new GameObject( [ new CContainer(), new cDetailBar(), new CScrollX() ] );
        this._detailBar.oid = this._go.oid + "_status_bar";
        const kdetailBarItemSide: number = 25;
        this._detailBar.cDetailBar.ownerToken = this._go;
        this._detailBar.cDetailBar.itemSide = kdetailBarItemSide;
        const kdetailBarItemCount: number = Math.floor( this._frame.width / kdetailBarItemSide );
        this._detailBar.cScrollX.width = kdetailBarItemSide * kdetailBarItemCount;
        this._detailBar.cScrollX.height = kdetailBarItemSide;
        this._detailBar.cScrollX.itemWidth = kdetailBarItemSide;
        this._detailBar.init();
        this._detailBar.cContainer.c.position.set( 
            ( this._frame.width - kdetailBarItemSide * kdetailBarItemCount ) * 0.5, 
            this._frame.height );
        this._go.cContainer.addChildAt( this._detailBar, 1 );
        this._detailBar.cDetailBar.scanForKeywords( this._curSide.cCardTokenSide.cardInfo );

        // POI socket.
        this._poiSocket = new GameObject( [ new CContainer() ] );
        this._poiSocket.init();
        this._poiSocket.cContainer.c.position.set( this._frame.width * 0.5, 5 );
        this._go.cContainer.addChild( this._poiSocket );
        //
        this._go.cCardTokenPoiReceptor.poiSocket = this._poiSocket;
    }

    public end(): void
    {
        this._onFaceUp.removeAll();
        this._onFaceDown.removeAll();
        this._onFlipped.removeAll();
        this._onBowed.removeAll();
        this._onReady.removeAll();
        this._onSizeUpdated.removeAll();
        this._onCardAttached.removeAll();
        this._onCardDetached.removeAll();

        this._animatedFrame.end();
        this._animatedFrame = null;

        this._detailBar.end();
        this._detailBar = null;

        this._poiSocket.end();
        this._poiSocket = null;

        this._frontSide.end();
        this._frontSide = null;

        this._backSide.end();
        this._backSide = null;

        this._curSide = null;
        
        this._attachmentHolder.end();
        this._attachmentHolder = null;

        super.end();
    }

    public calcWidth(): number
    {
        let result: number = this._frame.width;
        if ( this._attachmentHolder.cContainer.c.visible )
        {
            result += this._attachmentHolder.cContainer.c.width;
        }

        return result;
    }

    public calcHeight(): number
    {
        return this._frame.height;
    }

    public hasAttachments(): boolean
    {
        return this._attachmentHolder.cScrollY.hasItems();
    }

    public setFaceUp( actionScope: action_scope_type ): void
    {
        if ( this._isFaceUp ) { return; }

        if ( this._isAnimated && ServiceLocator.game.isAnimated )
        {
            Utils.anim.bumpFromCorner( this._go.cContainer.c, this.onFaceUpAnimRepeatCb.bind( this, true ) );
        }
        else
        {
            this.onFaceUpAnimRepeatCb.call( this, false );
        }

        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_CARD_TOKEN_FACE_UP, null, [ this._go.oid ] );
        }
    }

        private onFaceUpAnimRepeatCb( isAnimated: boolean ): void
        {
            if ( isAnimated )
            {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "card_face_up" );
            }

            if ( ServiceLocator.game.cardPreview.cCardPreview.isTarget( this._curSide.cCardTokenSide.bg ) )
            {
                ServiceLocator.game.cardPreview.cCardPreview.hide();
            }
            
            this._frontSide.cContainer.c.visible = true;
            this._backSide.cContainer.c.visible = false;
            this._curSide.cCardTokenSide.onHidden();
            this._curSide = this._frontSide;   
            this._curSide.cCardTokenSide.onRevealed();

            this._isFaceUp = true;

            if ( this._curSide.cCardTokenSide.bg.cCardPreviewable )
            {
                this._curSide.cCardTokenSide.bg.cCardPreviewable.refresh();
            }

            this._onFaceUp.dispatch( this._go );
            this._onFlipped.dispatch( this._frontSide, this._backSide );
        }

    public setFaceDown( actionScope: action_scope_type ): void
    {
        if ( !this._isFaceUp ) { return; }

        if ( this._isAnimated && ServiceLocator.game.isAnimated )
        {
            Utils.anim.bumpFromCorner( this._go.cContainer.c, this.onFaceDownAnimRepeatCb.bind( this, true ) );
        }
        else
        {
            this.onFaceDownAnimRepeatCb.call( this, false );
        }

        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_CARD_TOKEN_FACE_DOWN, null, [ this._go.oid ] );
        }
    }

        private onFaceDownAnimRepeatCb( isAnimated: boolean ): void
        {
            if ( isAnimated )
            {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "card_face_down" );
            }

            if ( ServiceLocator.game.cardPreview.cCardPreview.isTarget( this._curSide.cCardTokenSide.bg ) )
            {
                ServiceLocator.game.cardPreview.cCardPreview.hide();
            }

            this._frontSide.cContainer.c.visible = false;
            this._backSide.cContainer.c.visible = true;
            this._curSide.cCardTokenSide.onHidden();
            this._curSide = this._backSide;   
            this._curSide.cCardTokenSide.onRevealed();

            this._isFaceUp = false;

            if ( this._curSide.cCardTokenSide.bg.cCardPreviewable )
            {
                this._curSide.cCardTokenSide.bg.cCardPreviewable.refresh();
            }

            this._onFaceDown.dispatch( this._go );
            this._onFlipped.dispatch( this._backSide, this._frontSide );
        }

    public setBowed( isBowed: boolean, actionScope: action_scope_type ): void
    {
        if ( isBowed == this._isBowed ) { return; }
        
        if ( this._isAnimated && ServiceLocator.game.isAnimated )
        {
            Utils.anim.bumpFromCorner( this._go.cContainer.c );
        }

        if ( isBowed )
        {
            this._frontSide.cCardTokenSide.addBowedFilter();
            this._backSide.cCardTokenSide.addBowedFilter();
        }
        else
        {
            this._frontSide.cCardTokenSide.removeBowedFilter();
            this._backSide.cCardTokenSide.removeBowedFilter();
        }

        this._isBowed = isBowed;
        if ( !this._isBowed )
        {
            this._curSide.cCardTokenSide.removeStatus( status_type.HAS_ATTACKED, action_scope_type.LOCAL );
        }
        
        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( isBowed ? player_action_type.BOW_CARD_TOKEN : player_action_type.READY_CARD_TOKEN, null, [ this._go.oid ] );
        }

        if ( this._isBowed )
        {
            this._onBowed.dispatch( this._go );
        }
        else
        {
            this._onReady.dispatch( this._go );
        }
    }

    public setSelectedVfx( isVisible: boolean ): void
    {
        if ( isVisible )
        {
            const kColor: number = ServiceLocator.game.playerColors[ this._controllerPlayer ];
            let outlineFilter: OutlineFilter = new OutlineFilter( 5, kColor, 0.5 );
            outlineFilter.padding = 5;
            this._go.cContainer.c.filters = [ outlineFilter ];
        }
        else
        {
            this._go.cContainer.c.filters = [];
        }
    }

    public setHighlighted( isHighlighted: boolean, actionScope: action_scope_type ): void
    {
        this._isHighlighted = isHighlighted;

        if ( this._isHighlighted )
        {
            this._frontSide.cCardTokenSide.addHighlightedFilter();
            this._backSide.cCardTokenSide.addHighlightedFilter();
            
            // Multiplayer.
            if ( actionScope == action_scope_type.MULTIPLAYER )
            {
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ADD_CARD_TOKEN_HIGHLIGHT, null, [ this._go.oid ] );
            }
        }
        else
        {
            this._frontSide.cCardTokenSide.removeHighlightedFilter();
            this._backSide.cCardTokenSide.removeHighlightedFilter();

            // Multiplayer.
            if ( actionScope == action_scope_type.MULTIPLAYER )
            {
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.REMOVE_CARD_TOKEN_HIGHLIGHT, null, [ this._go.oid ] );
            }
        }
    }

    // virtual.
    public discard( actionScope: action_scope_type ): void
    {
        this.discardAttachments( actionScope );
        this.discardUnderneath( actionScope );
        ServiceLocator.game.attackBindingManager.removeAllBindings( this._go, actionScope );

        this._frontSide.cCardTokenSide.onDiscarded( actionScope );
        this._backSide.cCardTokenSide.onDiscarded( actionScope );

        if ( this._frontSide.cCardTokenSide.cardInfo.pack_code != "Custom" )
        {
            Utils.game.discard( this._go, actionScope );
        }
        else
        {
            ServiceLocator.game.root.cGameLayerProvider.get( layer_type.UNDER_TABLE ).cDropArea.forceDrop( this._go, null, actionScope );
            if ( actionScope == action_scope_type.MULTIPLAYER )
            {
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.DELETE_GAME_OBJECT, null, [ this._go.oid ] );
            }

            this._go.end();
        }
    }

    public discardAttachments( actionScope: action_scope_type ): void
    {
        let arrAttachment: Array<GameObject> = this._attachmentHolder.cScrollY.findItems();
        for ( let attachment of arrAttachment )
        {
            attachment.cAttachment.discard( actionScope );
        }
    }

    public discardUnderneath( actionScope: action_scope_type ): void
    {
        let underneathItem: IDetailBarItem = this._detailBar.cDetailBar.findItem( detail_bar_icon_type.UNDERNEATH );
        if ( underneathItem )
        {
            underneathItem.root.cUnderneathButton.discard( actionScope );
        }
    }

    // virtual.
    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        this._frontSide.cCardTokenSide.setEnabled( isEnabled );
        this._backSide.cCardTokenSide.setEnabled( isEnabled );

        if ( this._go.cDraggable )
        {
            this._go.cDraggable.setEnabled( isEnabled );
        }

        let arrAttachment: Array<GameObject> = this._attachmentHolder.cScrollY.findItems();
        for ( let attachment of arrAttachment )
        {
            if ( attachment.cAttachment.controllerPlayer == player_type.PLAYER )
            {
                attachment.cAttachment.setEnabled( isEnabled );
            }
        }

        this._detailBar.cDetailBar.setEnabled( isEnabled );
    }

    public addCardUnderneath( added: GameObject, actionScope: action_scope_type ): void
    {
        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ADD_CARD_UNDERNEATH, null, [ this._go.oid, added.oid ] );
        }

        let underneathItem: IDetailBarItem = this._detailBar.cDetailBar.findItem( detail_bar_icon_type.UNDERNEATH );
        if ( !underneathItem )
        {
            underneathItem = this._detailBar.cDetailBar.addItem( detail_bar_icon_type.UNDERNEATH );
        }
        underneathItem.root.cUnderneathButton.cDeckIndicator.cViewer.cCardView.go.cDropArea.forceDrop( added, new PIXI.Point( CDropArea.kPredefinedDropPositionCode, 0 ), action_scope_type.LOCAL );
        let arrCard: Array<GameObject> = underneathItem.root.cUnderneathButton.cDeckIndicator.cViewer.cCardView.findItems();
        arrCard[ arrCard.length - 1 ].cCard.locationData = this._go.oid;
    }

    public giveControl(): void
    {
        if ( this._location == location_type.MY_HOME )
        {
            ServiceLocator.game.cGameWorld.cAllyArea.home.cDropArea.forceDrop( this._go, new PIXI.Point( CDropArea.kPredefinedDropPositionCode, 0 ), action_scope_type.MULTIPLAYER );
        }
        else if ( this._location == location_type.MY_ENGAGED )
        {
            ServiceLocator.game.cGameWorld.cAllyArea.engaged.cDropArea.forceDrop( this._go, new PIXI.Point( CDropArea.kPredefinedDropPositionCode, 0 ), action_scope_type.MULTIPLAYER );
        }
    }

    // virtual.
    public saveGame(): ISgCardToken
    {
        let arrAttachment: Array<ISgAttachment> = new Array<ISgAttachment>();
        for ( let attachment of this._attachmentHolder.cScrollY.findItems() )
        {
            arrAttachment.push( attachment.cAttachment.saveGame() );
        }

        return {
            oid: this._go.oid,
            frontSide: this._frontSide.cCardTokenSide.saveGame(),
            backSide: this._backSide.cCardTokenSide.saveGame(),
            isFaceUp: this._isFaceUp,
            isBowed: this._isBowed,
            isHighlighted: this._isHighlighted,
            location: this._location,
            detailBar: this._detailBar.cDetailBar.saveGame(),
            owner: this._ownerPlayer,
            controller: this._controllerPlayer,
            attachments: arrAttachment };
    }

    // virtual.
    public loadGame( sgCardToken: ISgCardToken, pass: number ): void
    {
        if ( pass == 0 ) 
        {
            this._controllerPlayer = sgCardToken.controller;

            this._frontSide.cCardTokenSide.loadGame( sgCardToken.frontSide, pass );
            this._backSide.cCardTokenSide.loadGame( sgCardToken.backSide, pass );

            this.setEnabled( this._controllerPlayer == player_type.PLAYER );

            if ( sgCardToken.isBowed )
            {
                this.setBowed( true, action_scope_type.LOCAL );
            }
            if ( sgCardToken.isFaceUp )
            {
                this.setFaceUp( action_scope_type.LOCAL );
            }
            else
            {
                this.setFaceDown( action_scope_type.LOCAL );
            }
            if ( sgCardToken.isHighlighted )
            {
                this.setHighlighted( true, action_scope_type.LOCAL );
            }

            this._detailBar.cDetailBar.scanForKeywords( this._curSide.cCardTokenSide.cardInfo );
            if ( sgCardToken.detailBar != null )
            {
                this._detailBar.cDetailBar.loadGame( sgCardToken.detailBar, pass );
            }

            for ( let sgAttachment of sgCardToken.attachments )
            {
                let card: GameObject = GameObject.find( sgAttachment.oid.substr( 0, sgAttachment.oid.indexOf( "_attachment" ) ) );
                this._go.cDropArea.forceDrop( card, null, action_scope_type.LOCAL );
                let attachment: GameObject = this._attachmentHolder.cScrollY.getLastItem();
                attachment.cAttachment.loadGame( sgAttachment, pass );

                // Messenger of the Kings contract.
                if ( sgAttachment.backCardId == "022134B" )
                {
                    if ( this._frontSide.cAllySide && this._frontSide.cAllySide.hasResourceCounter() )
                    {
                        this._frontSide.cAllySide.resource.cTokenCounter.setCount( sgCardToken.frontSide.allySide.resource, action_scope_type.LOCAL );
                    }
                    if ( this._backSide.cAllySide && this._backSide.cAllySide.hasResourceCounter() )
                    {
                        this._backSide.cAllySide.resource.cTokenCounter.setCount( sgCardToken.backSide.allySide.resource, action_scope_type.LOCAL );
                    }
                }
            }
        }
    }

    // private:

    private onLocationChanged(): void
    {
        let isShared: boolean = Utils.game.isSharedLocation( this._location );
        this._go.cShareableGameElement.setEnabled( isShared );
        this._frontSide.cShareableGameElement.setEnabled( isShared );
        this._backSide.cShareableGameElement.setEnabled( isShared );

        const kOldControllerPlayer: player_type = this._controllerPlayer;
        const kControllerPlayer: player_type = Utils.game.findControllerPlayerByLocation( this._location );
        this._controllerPlayer = kControllerPlayer;

        // Remove existing POIs.
        if ( kOldControllerPlayer != this._controllerPlayer )
        {
            let poiContainer: GameObject = Utils.game.findGameProviderLayer( this._go, layer_type.POI );
            if ( poiContainer )
            {
                for ( let i: number = poiContainer.cContainer.c.children.length - 1; i >= 0; --i )
                {
                    let displayObject: PIXI.DisplayObject = poiContainer.cContainer.c.children[ i ];
                    if ( ( ( displayObject as any ).go as GameObject ).cPoi.target == this._go )
                    {
                        ( ( displayObject as any ).go as GameObject ).cPoi.remove( player_type.ALLY );
                    }
                }
            }
        }
        //
        let isEnabled: boolean = this._controllerPlayer == player_type.PLAYER;
        this._frontSide.cCardTokenSide.setEnabled( isEnabled );
        this._backSide.cCardTokenSide.setEnabled( isEnabled );
        this._detailBar.cDetailBar.setEnabled( isEnabled );
        //
        let arrAttachment: Array<GameObject> = this._attachmentHolder.cScrollY.findItems();
        for ( let attachment of arrAttachment )
        {
            attachment.cAttachment.setEnabled( isEnabled );
        }
        //
        this.setEnabled( isEnabled );

        let underneathItem: IDetailBarItem = this._detailBar.cDetailBar.findItem( detail_bar_icon_type.UNDERNEATH );
        if ( underneathItem )
        {
            let cViewer: CViewer = underneathItem.root.cUnderneathButton.cDeckIndicator.cViewer;
            cViewer.location = kControllerPlayer == player_type.PLAYER ? location_type.MY_UNDERNEATH : location_type.ALLY_UNDERNEATH;
            cViewer.cCardView.playerType = kControllerPlayer;
            //
            let arrCard: Array<GameObject> = cViewer.cCardView.findItems();
            for ( let card of arrCard )
            {
                card.cCard.location = cViewer.location;
                card.cCard.setEnabled( isEnabled );
            }

            let anchorY: GameObject = cViewer.go.cContainer.c.parent[ "go" ] as GameObject;
            if ( kControllerPlayer == player_type.PLAYER )
            {
                anchorY.cContainer.c.y = 1070;
                anchorY.cAnchorY.anchorY = 1;
            }
            else
            {
                anchorY.cContainer.c.y = 10;
                anchorY.cAnchorY.anchorY = 0;
            }
            anchorY.cAnchorY.onChildContentUpdated();
        }
    }

    private createShadow(): PIXI.Sprite
    {
        let shadowId: string = null;
        if ( this._backSide.cCardTokenSide.cardInfo.type_code == "quest-intro" )
        {
            shadowId = "shadow_quest_token";
        }
        else
        {
            shadowId = "shadow_card_token";
        }
        let result: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( shadowId ) );
        result.pivot.set( 25 );

        return result;
    }

    // #endregion //


    // #region IDragShadowTextureCreator //

    public createDragShadowTexture(): PIXI.Texture
    {
        let source: PIXI.Container = this._curSide.cCardTokenSide.bg.cContainer.c;
        let dragShadowTexture = PIXI.RenderTexture.create( { width: source.width, height: source.height } );
        let pos: PIXI.Point = new PIXI.Point();
        source.position.copyTo( pos );
        source.position.set( 0 );
        ServiceLocator.game.app.renderer.render( source, dragShadowTexture );      
        pos.copyTo( source.position );

        return dragShadowTexture;
    }

    // #endregion //


    // #region IGameObjectDropArea //

    public validateDroppedGameObject( dropped: GameObject ): boolean
    {
        let result: boolean = dropped != this._go
            && ( dropped.cAttachment != null 
                || dropped.cShadowCardMini != null 
                    && this._curSide.cCardTokenSide.cardInfo.type_code == "enemy" ); 
        if ( result )
        {
            if ( dropped.cAttachment != null )
            {
                let fromActorArea: GameObject = Utils.game.findGameObjectInBranchByComponentName( dropped.cAttachment.cardToken, "cActorArea" )
                let toActorArea: GameObject = Utils.game.findGameObjectInBranchByComponentName( this._go, "cActorArea" );
                if ( fromActorArea && toActorArea && fromActorArea != toActorArea )
                {
                    result = toActorArea.cActorArea.findInsertTabIndex( 50 ) != -1;
                    if ( !result )
                    {
                        let floatinMessage = new FloatingMessage();
                        floatinMessage.show( jQuery.i18n( "NOT_ENOUGH_SPACE" ), ServiceLocator.game.app.renderer.plugins.interaction.mouse.global );
                    }
                }
            }
        }

        return result;
    }

    public processDroppedGameObject( dropped: GameObject, global: PIXI.IPoint, actionScope: action_scope_type ): void
    {
        if ( dropped.cCardToken )
        {
            dropped.cCardToken.discardAttachments( action_scope_type.LOCAL );
            dropped.cCardToken.discardUnderneath( action_scope_type.LOCAL );
            ServiceLocator.game.attackBindingManager.removeAllBindings( dropped, action_scope_type.LOCAL );
        }

        if ( dropped.cAttachment || dropped.cCard || dropped.cCardToken || dropped.cGameModifier )
        {
            let attachment: GameObject = this.createDroppedAttachment( dropped );
            this._attachmentHolder.cScrollY.addItem( attachment );
            if ( this._isAnimated && ServiceLocator.game.isAnimated )
            {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "token_dropped" );

                // Vfx.
                let procScrollToTarget: ProcScrollToTarget = new ProcScrollToTarget();
                procScrollToTarget.target = attachment;
                procScrollToTarget.init();
                ServiceLocator.game.processSequencer.add( [ procScrollToTarget ], "add_attachment-" + this._go.oid.toString() );
                //
                let procBumpFromCorner: ProcBumpFromCorner = new ProcBumpFromCorner();
                procBumpFromCorner.target = attachment;
                procBumpFromCorner.init();
                //
                let procFlash: ProcFlash = new ProcFlash();
                procFlash.target = attachment;
                procFlash.width = attachment.cContainer.c.width;
                procFlash.height = attachment.cContainer.c.height;
                procFlash.scale = 1.5;
                procFlash.init();
                ServiceLocator.game.processSequencer.add( [ procBumpFromCorner, procFlash ], "add_attachment-" + this._go.oid.toString() );
            }

            if ( attachment.cAttachment.curSide.cardInfo.text.indexOf( "Restricted." ) != -1 )
            {
                this._detailBar.cDetailBar.addToItemCount( detail_bar_icon_type.RESTRICTED, 1 );
            }
            else if ( attachment.cAttachment.curSide.cardInfo.text.indexOf( "counts as 2 Restricted attachments" ) != -1 )
            {
                this._detailBar.cDetailBar.addToItemCount( detail_bar_icon_type.RESTRICTED, 2 );
            }

            if ( attachment.cAttachment.curSide.cardId == "022134B" )
            {
                // Messenger of the King contract.
                if ( this._frontSide.cAllySide )
                {
                    this._frontSide.cAllySide.addResourceCounter();
                }
                if ( this._backSide.cAllySide )
                {
                    this._backSide.cAllySide.addResourceCounter();
                }
            }

            const kAddSphereIconRegex: RegExp = /gains a \[(lore|leadership|tactics|spirit)\] (resource )?icon\./;
            const kArrExec: RegExpExecArray = kAddSphereIconRegex.exec( attachment.cAttachment.curSide.cardInfo.text );
            if ( kArrExec )
            {
                // Additional sphere icon.
                const kAragornRegex: RegExp = /If (the )?attached hero is Aragorn, he (also )?gains a \[(lore|leadership|tactics|spirit)\] (resource )?icon\./;
                let isOnlyAragorn: boolean = kAragornRegex.test( attachment.cAttachment.curSide.cardInfo.text );
                if ( this._frontSide.cHeroSide 
                    && ( !isOnlyAragorn || isOnlyAragorn && this._frontSide.cHeroSide.cardInfo.name == "Aragorn" ) )
                {
                    this._frontSide.cHeroSide.addSphereIcon( kArrExec[ 1 ] );
                }
                if ( this._backSide.cHeroSide
                    && ( !isOnlyAragorn || isOnlyAragorn && this._backSide.cHeroSide.cardInfo.name == "Aragorn" ) )
                {
                    this._backSide.cHeroSide.addSphereIcon( kArrExec[ 1 ] );
                }
            }

            if ( !this._attachmentHolder.cContainer.c.visible )
            {
                this._attachmentHolder.cContainer.c.visible = true;
                this._onSizeUpdated.dispatch( this._go );
            }

            this._onCardAttached.dispatch();

            // Multiplayer.
            if ( actionScope == action_scope_type.MULTIPLAYER )
            {
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.DROP_AT_DROP_AREA, null, [ dropped.oid, this._go.oid, this._go.cContainer.c.toLocal( global ) ] );
                if ( global != null )
                {
                    ServiceLocator.game.cGameWorld.cActionLogger.logTargetSelection( 
                        player_type.PLAYER, new LogTargetCard( attachment ), new LogTargetCard( this._go ), "rad_equip", false, true );
                }
            }

            // IMPORTANT: cCard is "force" dropped during the game loading process!!
            if ( dropped.cCard || dropped.cCardToken || dropped.cGameModifier )
            {
                dropped.end();
            }
        }
        else if ( dropped.cShadowCardMini )
        {
            this._curSide.cEnemySide.addShadowCardMini( dropped );

            // Multiplayer.
            if ( actionScope == action_scope_type.MULTIPLAYER )
            {
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.DROP_AT_DROP_AREA, null, [ dropped.oid, this._go.oid, this._go.cContainer.c.toLocal( global ) ] );
                if ( global != null )
                {
                    ServiceLocator.game.cGameWorld.cActionLogger.logTargetSelection( 
                        player_type.PLAYER, new LogTargetCard( dropped.cShadowCardMini.card ), new LogTargetCard( this._go ), "rad_shadow_card", false, true );
                }
            }
        }
    }

        private createDroppedAttachment( dropped: GameObject ): GameObject
        {
            let attachment: GameObject = null;

            if ( dropped.cCardToken )
            {
                attachment = new GameObject( [ new CContainer(), new CAttachment(), new CDraggable(), new CAttachmentPoiReceptor(), new CTargetSelector(), new CShareableGameElement() ] );
                attachment.oid = dropped.oid.substr( 0, dropped.oid.length - 1 ) + "_attachment";
                attachment.cAttachment.frontCardId = dropped.cCardToken.cFrontSide.cardId;
                attachment.cAttachment.backCardId = dropped.cCardToken.cBackSide.cardId;
                attachment.cAttachment.ownerPlayer = dropped.cCardToken.ownerPlayer;
                attachment.cAttachment.controllerPlayer = this._controllerPlayer;
                attachment.cAttachment.cardToken = this._go;
                attachment.cAttachment.isBowed = dropped.cCardToken.isBowed;
                attachment.cAttachment.isFaceUp = dropped.cCardToken.isFaceUp;
                attachment.cTargetSelector.receptor = attachment;
                attachment.init();
            }
            else if ( dropped.cAttachment )
            {
                attachment = dropped;
                attachment.cAttachment.controllerPlayer = this._controllerPlayer;
                attachment.cAttachment.cardToken = this._go;
            }
            else if ( dropped.cCard )
            {
                attachment = new GameObject( [ new CContainer(), new CAttachment(), new CDraggable(), new CAttachmentPoiReceptor(), new CTargetSelector(), new CShareableGameElement() ] );
                attachment.oid = dropped.oid + "_attachment";
                attachment.cAttachment.frontCardId = dropped.cCard.front.cardId;
                attachment.cAttachment.backCardId = dropped.cCard.back.cardId;
                attachment.cAttachment.ownerPlayer = dropped.cCard.ownerPlayer;
                attachment.cAttachment.controllerPlayer = this._controllerPlayer;
                attachment.cAttachment.cardToken = this._go;
                attachment.cAttachment.isFaceUp = dropped.cCard.isFaceUp;
                attachment.cTargetSelector.receptor = attachment;
                attachment.init();
            }
            else if ( dropped.cGameModifier )
            {
                attachment = new GameObject( [ new CContainer(), new CAttachment(), new CDraggable(), new CAttachmentPoiReceptor(), new CTargetSelector(), new CShareableGameElement() ] );
                attachment.oid = dropped.oid.substr( 0, dropped.oid.indexOf( "_game_modifier" ) ) + "_attachment";
                attachment.cAttachment.frontCardId = dropped.cGameModifier.front.cardId;
                attachment.cAttachment.backCardId = dropped.cGameModifier.back.cardId;
                attachment.cAttachment.ownerPlayer = dropped.cGameModifier.ownerPlayer;
                attachment.cAttachment.controllerPlayer = this._controllerPlayer;
                attachment.cAttachment.cardToken = this._go;
                attachment.cAttachment.isFaceUp = dropped.cGameModifier.isFaceUp;
                attachment.cTargetSelector.receptor = attachment;
                attachment.init();
            }
            attachment.cAttachment.setEnabled( this._controllerPlayer == player_type.PLAYER );

            // Listen to events.
            attachment.cDraggable.onDropped.add( this.onAttachment_Dropped, this );
            attachment.cAttachment.onFlipped.add( this.onAttachment_Flipped, this );

            return attachment;
        }

    // #endregion //


    // #region Other Callbacks //
    
    private onAttachment_Dropped( dropped: GameObject, dropArea: GameObject, actionScope: action_scope_type ): void
    {
        // Cleanup events.
        dropped.cAttachment.onFlipped.remove( this.onAttachment_Flipped, this );

        this._attachmentHolder.cScrollY.removeItem( dropped );

        if ( dropped.cAttachment.curSide.cardInfo.text.indexOf( "Restricted." ) != -1 )
        {
            this._detailBar.cDetailBar.addToItemCount( detail_bar_icon_type.RESTRICTED, -1 );
        }
        else if ( dropped.cAttachment.curSide.cardInfo.text.indexOf( "counts as 2 Restricted attachments" ) != -1 )
        {
            this._detailBar.cDetailBar.addToItemCount( detail_bar_icon_type.RESTRICTED, -2 );
        }

        if ( dropped.cAttachment.curSide.cardId == "022134B" )
        {
            // Messenger of the King contract.
            if ( this._frontSide.cAllySide )
            {
                this._frontSide.cAllySide.removeResourceCounter();
            }
            if ( this._backSide.cAllySide )
            {
                this._backSide.cAllySide.removeResourceCounter();
            }
        }

        const kAddSphereIconRegex: RegExp = /gains a \[(lore|leadership|tactics|spirit)\] (resource )?icon\./;
        const kArrExec: RegExpExecArray = kAddSphereIconRegex.exec( dropped.cAttachment.curSide.cardInfo.text );
        if ( kArrExec )
        {
            // Additional sphere icon.
            const kAragornRegex: RegExp = /If (the )?attached hero is Aragorn, he (also )?gains a \[(lore|leadership|tactics|spirit)\] (resource )?icon\./;
            let isOnlyAragorn: boolean = kAragornRegex.test( dropped.cAttachment.curSide.cardInfo.text );
            if ( this._frontSide.cHeroSide 
                && ( !isOnlyAragorn || isOnlyAragorn && this._frontSide.cHeroSide.cardInfo.name == "Aragorn" ) )
            {
                this._frontSide.cHeroSide.removeSphereIcon( kArrExec[ 1 ] );
            }
            if ( this._backSide.cHeroSide
                && ( !isOnlyAragorn || isOnlyAragorn && this._backSide.cHeroSide.cardInfo.name == "Aragorn" ) )
            {
                this._backSide.cHeroSide.removeSphereIcon( kArrExec[ 1 ] );
            }
        }

        if ( !this._attachmentHolder.cScrollY.hasItems() )
        {
            this._attachmentHolder.cContainer.c.visible = false;
            this._onSizeUpdated.dispatch( this._go );
        }

        this._onCardDetached.dispatch();
    }

    private onAttachment_Flipped( curSide: ICardSide, otherSide: ICardSide ): void
    {
        let dRestrictred: number = 0;

        if ( curSide.cardInfo.text.indexOf( "Restricted." ) != -1 )
        {
            if ( curSide.cardInfo.text.indexOf( "Restricted." ) != -1 && otherSide.cardInfo.text.indexOf( "Restricted." ) == -1 )
            {
                dRestrictred = 1;
            }
            else if ( curSide.cardInfo.text.indexOf( "Restricted." ) == -1 && otherSide.cardInfo.text.indexOf( "Restricted." ) != -1 )
            {
                dRestrictred = -1;
            }
        }
        else if ( curSide.cardInfo.text.indexOf( "counts as 2 Restricted attachments" ) != -1 )
        {
            if ( curSide.cardInfo.text.indexOf( "counts as 2 Restricted attachments" ) != -1 && otherSide.cardInfo.text.indexOf( "counts as 2 Restricted attachments" ) == -1 )
            {
                dRestrictred = 2;
            }
            else if ( curSide.cardInfo.text.indexOf( "counts as 2 Restricted attachments" ) == -1 && otherSide.cardInfo.text.indexOf( "counts as 2 Restricted attachments" ) != -1 )
            {
                dRestrictred = -2;
            }
        }

        if ( dRestrictred != 0 )
        {
            this._detailBar.cDetailBar.addToItemCount( detail_bar_icon_type.RESTRICTED, dRestrictred );
        }

        // Messenger of the King contract.
        if ( curSide.cardId == "022134A" )
        {
            if ( this._frontSide.cAllySide )
            {
                this._frontSide.cAllySide.removeResourceCounter();
            }
            if ( this._backSide.cAllySide )
            {
                this._backSide.cAllySide.removeResourceCounter();
            }
        }
        else if ( curSide.cardId == "022134B" )
        {
            if ( this._frontSide.cAllySide )
            {
                this._frontSide.cAllySide.addResourceCounter();
            }
            if ( this._backSide.cAllySide )
            {
                this._backSide.cAllySide.addResourceCounter();
            }
        }

        const kAddSphereIconRegex: RegExp = /gains a \[(lore|leadership|tactics|spirit)\] (resource )?icon\./;
        let arrExec: RegExpExecArray = kAddSphereIconRegex.exec( curSide.cardInfo.text );
        if ( arrExec )
        {
            // Additional sphere icon.
            const kAragornRegex: RegExp = /If (the )?attached hero is Aragorn, he (also )?gains a \[(lore|leadership|tactics|spirit)\] (resource )?icon\./;
            let isOnlyAragorn: boolean = kAragornRegex.test( curSide.cardInfo.text );
            if ( this._frontSide.cHeroSide 
                && ( !isOnlyAragorn || isOnlyAragorn && this._frontSide.cHeroSide.cardInfo.text == "Aragorn" ) )
            {
                this._frontSide.cHeroSide.addSphereIcon( arrExec[ 1 ] );
            }
            if ( this._backSide.cHeroSide
                && ( !isOnlyAragorn || isOnlyAragorn && this._backSide.cHeroSide.cardInfo.text == "Aragorn" ) )
            {
                this._backSide.cHeroSide.addSphereIcon( arrExec[ 1 ] );
            }
        }
        
        arrExec = kAddSphereIconRegex.exec( otherSide.cardInfo.text );
        if ( arrExec )
        {
            // Additional sphere icon.
            const kAragornRegex: RegExp = /If (the )?attached hero is Aragorn, he (also )?gains a \[(lore|leadership|tactics|spirit)\] (resource )?icon\./;
            let isOnlyAragorn: boolean = kAragornRegex.test( otherSide.cardInfo.text );
            if ( this._frontSide.cHeroSide 
                && ( !isOnlyAragorn || isOnlyAragorn && this._frontSide.cHeroSide.cardInfo.text == "Aragorn" ) )
            {
                this._frontSide.cHeroSide.removeSphereIcon( arrExec[ 1 ] );
            }
            if ( this._backSide.cHeroSide
                && ( !isOnlyAragorn || isOnlyAragorn && this._backSide.cHeroSide.cardInfo.text == "Aragorn" ) )
            {
                this._backSide.cHeroSide.removeSphereIcon( arrExec[ 1 ] );
            }
        }
    }

    // #endregion //
}