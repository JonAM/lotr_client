import GameObject from "../../GameObject";


export default interface ICharacterTokenSide
{
    readonly attack: GameObject;
	readonly defense: GameObject;
	readonly health: GameObject;
	readonly wound: GameObject;
}