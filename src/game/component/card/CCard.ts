import Component from "../Component";

import { ICard } from "../../CardDB";
import { ISgCard } from "../../../view/game/SaveGameView";
import { IOption, option_type } from "../ui/CRadialMenu";
import { location_type } from "../world/CGameWorld";
import { OutlineFilter  } from "pixi-filters";
import { player_type } from "../world/CPlayerArea";
import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import Signal from "../../../lib/signals/Signal";
import CContainer from "../pixi/CContainer";
import CCardPreviewable from "../input/CCardPreviewable";
import { player_action_type, action_scope_type } from "../../../service/socket_io/GameSocketIOController";
import CSprite from "../pixi/CSprite";
import { input_event_type } from "../../InputController";


export default class CCard extends Component
{
    // #region Attributes //

    // private:

    private _front: ICardSide = null;
    private _back: ICardSide = null;
    private _ownerPlayer: player_type = null;
    private _controllerPlayer: player_type = null;
    private _location: location_type = null;
    private _locationData: string = null;
    private _isFaceUp: boolean = false;

    private _cardImgContainer: PIXI.Container = null;

    private _curSide: ICardSide = null;
    private _isAnimated: boolean = true;

    private _poiSocket: GameObject = null;
    private _label: PIXI.Text = null;
    
    // Signals.
    private _onFaceUp: Signal = new Signal();
    private _onFaceDown: Signal = new Signal();
    private _onPointerTap: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get front(): ICardSide { return this._front; }
    public get back(): ICardSide { return this._back; }
    public get ownerPlayer(): player_type { return this._ownerPlayer; }
    public get controllerPlayer(): player_type { return this._controllerPlayer; }
    public get location(): location_type { return this._location; }
    public get locationData(): string { return this._locationData; }
    public get isFaceUp(): boolean { return this._isFaceUp; }
    public get curSide(): ICardSide { return this._curSide; }
    public get otherSide(): ICardSide { return this._curSide == this._front ? this._back : this._front; }
    public get isAnimated(): boolean { return this._isAnimated; }

    public set frontCardId( value: string ) { this._front.cardId = value; }
    public set backCardId( value: string ) { this._back.cardId = value; }
    public set ownerPlayer( value: player_type ) { this._ownerPlayer = value; }
    public set controllerPlayer( value: player_type ) { this._controllerPlayer = value; }
    public set location( value: location_type ) { this._location = value; }
    public set locationData( value: string ) { this._locationData = value; }
    public set isFaceUp( value: boolean ) { this._isFaceUp = value; }
    public set isAnimated( value: boolean ) { this._isAnimated = value; }

    // Signals.
    public get onFaceUp(): Signal { return this._onFaceUp; }
    public get onFaceDown(): Signal { return this._onFaceDown; }
    public get onPointerTap(): Signal { return this._onPointerTap; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CCard";

        this._front = { cardId: null, cardInfo: null, sprite: null };
        this._back = { cardId: null, cardInfo: null, sprite: null };
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CCard.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cShareableGameElement != null, "CCard.ts :: init() :: CShareableGameElement component not found." );
        console.assert( this._front.cardId != null, "CCard.ts :: init() :: this._front.cardId cannot be null." );
        console.assert( this._back.cardId != null, "CCard.ts :: init() :: this._back.cardId cannot be null." );
        console.assert( this._ownerPlayer != null, "CCard.ts :: init() :: this._ownerPlayer cannot be null." );
        console.assert( this._controllerPlayer != null, "CCard.ts :: init() :: this._controllerPlayer cannot be null." );
        console.assert( this._location != null, "CCard.ts :: init() :: this._location cannot be null." );

        this._go.cContainer.c.interactive = true;
        this._go.cContainer.c.buttonMode = true;

        this._go.cShareableGameElement.setEnabled( this._ownerPlayer == player_type.SAURON );

        this._cardImgContainer = new PIXI.Container();
        this._cardImgContainer.interactive = true;
        this._go.cContainer.c.addChild( this._cardImgContainer );

        this._front.cardInfo = ServiceLocator.cardDb.find( this._front.cardId );
        this._back.cardInfo = ServiceLocator.cardDb.find( this._back.cardId );

        // Back.
        this._back.sprite = this.createCardSide( this._back );
        Utils.game.limitSideSize( 300, this._back.sprite.cSprite.s );
        this._back.sprite.cContainer.c.visible = !this._isFaceUp;
        this._cardImgContainer.addChild( this._back.sprite.cContainer.c );

        // Front.
        this._front.sprite = this.createCardSide( this._front );
        Utils.game.limitSideSize( 300, this._front.sprite.cSprite.s );
        this._front.sprite.cContainer.c.visible = this._isFaceUp;
        this._cardImgContainer.addChild( this._front.sprite.cContainer.c );

        this._curSide = this._isFaceUp ? this._front : this._back;

        // Border.
        let border: PIXI.Graphics = new PIXI.Graphics();
        border.lineStyle( 2, ServiceLocator.game.playerColors[ this._ownerPlayer ] );
        border.drawRect( 0, 0, this._go.cContainer.c.width, this._go.cContainer.c.height );
        this._cardImgContainer.addChild( border );

        // POI socket.
        this._poiSocket = new GameObject( [ new CContainer() ] );
        this._poiSocket.init();
        this._cardImgContainer.addChild( this._poiSocket.cContainer.c );
        this._poiSocket.cContainer.c.position.set( this._go.cContainer.c.width - 20, 20 );
        this._go.cCardPoiReceptor.poiSocket = this._poiSocket;

        // Listen to events.
        ServiceLocator.game.inputController.on( this._cardImgContainer, input_event_type.TAP, this.onCard_PointerTap, this );
    }

        private createCardSide( cardSide: ICardSide ): GameObject
        {
            let result: GameObject = new GameObject( [ new CSprite(), new CCardPreviewable() ] );
            result.cSprite.s.texture = Utils.img.findCardTexture( cardSide.cardId );
            if ( cardSide.cardInfo.type_code == "quest-intro"
                || cardSide.cardInfo.type_code == "quest"
                || cardSide.cardInfo.type_code == "player-side-quest"
                || cardSide.cardInfo.type_code == "encounter-side-quest" )
            {
                result.cSprite.s.anchor.y = 1;
                result.cSprite.s.rotation = Math.PI * 0.5;
            }
            result.cCardPreviewable.cardId = cardSide.cardId;
            result.init();

            return result;
        }

    public end(): void
    {
        // Cleanup events.
        ServiceLocator.game.inputController.off( this._cardImgContainer, input_event_type.TAP, this.onCard_PointerTap, this );

        this._onFaceUp.removeAll();
        this._onFaceUp.removeAll();
        this._onPointerTap.removeAll();

        this._poiSocket.end();
        this._poiSocket = null;

        this._front.sprite.end();
        this._front = null;

        this._back.sprite.end();
        this._back = null;
        
        this._curSide = null;

        super.end();
    }

    public setFaceUp( actionScopeType: action_scope_type ): void
    {
        if ( this._isFaceUp ) { return; }

        if ( this._isAnimated && ServiceLocator.game.isAnimated )
        {
            this._back.sprite.cCardPreviewable.setEnabled( false );
            this.bump( () => {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "card_face_up" );

                if ( ServiceLocator.game.cardPreview.cCardPreview.isTarget( this._curSide.sprite ) )
                {  
                    ServiceLocator.game.cardPreview.cCardPreview.hide();
                }

                this._front.sprite.cContainer.c.visible = true;
                this._back.sprite.cContainer.c.visible = false;
                this._curSide = this._front;

                if ( this._label ) { this._label.visible = false; }

                this._back.sprite.cCardPreviewable.setEnabled( true );
                this._curSide.sprite.cCardPreviewable.refresh();
            } );
        }
        else
        {
            this._front.sprite.cContainer.c.visible = true;
            this._back.sprite.cContainer.c.visible = false;
            this._curSide = this._front;

            if ( this._label ) { this._label.visible = false; }
        }

        this._isFaceUp = true;

        this._onFaceUp.dispatch( this._go );

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_CARD_FACE_UP, null, [ this._go.oid ] );  
        }
    }

    public setFaceDown( actionScopeType: action_scope_type ): void
    {
        if ( !this._isFaceUp ) { return; }

        if ( this._isAnimated && ServiceLocator.game.isAnimated )
        {
            this._front.sprite.cCardPreviewable.setEnabled( false );
            this.bump( () => {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "card_face_down" );

                if ( ServiceLocator.game.cardPreview.cCardPreview.isTarget( this._curSide.sprite ) )
                {  
                    ServiceLocator.game.cardPreview.cCardPreview.hide();
                }
                
                this._front.sprite.cContainer.c.visible = false;
                this._back.sprite.cContainer.c.visible = true; 
                this._curSide = this._back;

                if ( this._label ) { this._label.visible = true; }
                
                this._front.sprite.cCardPreviewable.setEnabled( true );
                this._curSide.sprite.cCardPreviewable.refresh();
            } );
        }
        else
        {
            this._front.sprite.cContainer.c.visible = false;
            this._back.sprite.cContainer.c.visible = true; 
            this._curSide = this._back;

            if ( this._label ) { this._label.visible = true; }
        }
        
        this._isFaceUp = false;

        this._onFaceDown.dispatch( this._go );

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_CARD_FACE_DOWN, null, [ this._go.oid ] );  
        }
    }

    public setSelectedVfx( isVisible: boolean ): void
    {
        if ( isVisible )
        {
            const kColor: number = ServiceLocator.game.playerColors[ this._controllerPlayer ];
            let outlineFilter: OutlineFilter = new OutlineFilter( 5, kColor, 0.5 );
            outlineFilter.padding = 5;
            this._cardImgContainer.filters = [ outlineFilter ];
        }
        else
        {
            this._cardImgContainer.filters = [];
        }
    }

    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        this._go.cContainer.c.buttonMode = isEnabled;
        this._go.cDraggable.setEnabled( isEnabled );

        ServiceLocator.game.inputController.off( this._cardImgContainer, input_event_type.TAP, this.onCard_PointerTap, this );
        if ( isEnabled )
        {
            ServiceLocator.game.inputController.on( this._cardImgContainer, input_event_type.TAP, this.onCard_PointerTap, this );
        }
    }

    public createLabel( text: string ): void
    {
        if ( this._label ) { return; }

        this._label = new PIXI.Text( text, ServiceLocator.game.textStyler.normal );
        this._label.anchor.set( 1, 0 );
        this._label.rotation = -Math.PI * 0.5;
        this._label.position.set( this._cardImgContainer.width - this._label.height + 3, 2 );
        this._cardImgContainer.addChild( this._label );
        
        this._label.visible = !this._isFaceUp;
    }

    // virtual.
    public discard( actionScopeType: action_scope_type ): void
    {
        Utils.game.discard( this._go, actionScopeType );
    }

    public bump( onRepeat: ( ...args: any[] ) => void = null ): gsap.core.Tween
    {
        return Utils.anim.bumpFromCorner( this._cardImgContainer, onRepeat );
    }

    // virtual.
    public saveGame(): ISgCard
    {
        let result: ISgCard = {
            oid: this._go.oid,
            frontCardId: this._front.cardId,
            backCardId: this._back.cardId,
            isFaceUp: this._isFaceUp,
            owner: this._ownerPlayer,
            controller: this._controllerPlayer };
        if ( this._locationData )
        {
            result.locationData = this._locationData;
        }
        if ( this._label )
        {
            result.label = this._label.text;
        }

        return result;
    }

    // virtual.
    public loadGame( sgCard: ISgCard, pass: number ): void
    {
        if ( pass == 0 )
        {
            this._controllerPlayer = sgCard.controller;
            this._ownerPlayer = sgCard.owner;
            if ( this._ownerPlayer != this._controllerPlayer )
            {
                this._go.cCard.setEnabled( this._controllerPlayer == player_type.PLAYER );
            }
            this._isFaceUp = sgCard.isFaceUp;
            this._front.sprite.cContainer.c.visible = this._isFaceUp;
            this._back.sprite.cContainer.c.visible = !this._isFaceUp;
            this._curSide = this._isFaceUp ? this._front : this._back;
            this._locationData = sgCard.locationData;
            if ( sgCard.label )
            {
                this.createLabel( sgCard.label );
            }
        }
    }

    // protected:

    // virtual.
    protected createRadialMenuOptions(): Array<IOption>
    {
        let arrOption: Array<IOption> = null;
        switch ( this._location )
        {
            case location_type.MY_HAND:
                {
                    arrOption = [ { id: option_type.DISCARD, isEnabled: true } ];
                    break;
                }

            case location_type.MY_DECK:
            case location_type.MY_HAND:
            case location_type.MY_SET_ASIDE:
            case location_type.MY_UNDERNEATH:
            case location_type.MY_CUSTOM_DECK:
            case location_type.SAURON_DECK_0:
            case location_type.SAURON_DECK_1:
            case location_type.SAURON_CUSTOM_DECK_0:
            case location_type.SAURON_CUSTOM_DECK_1:
            case location_type.QUEST_DECK:
            case location_type.SET_ASIDE:
            case location_type.CARD_TRAY:
                {
                    arrOption = [ 
                        { id: this._isFaceUp ? option_type.PUT_FACE_DOWN : option_type.PUT_FACE_UP, isEnabled: true },
                        { id: option_type.DISCARD, isEnabled: true } ];
                    break;
                }

            case location_type.QUEST_DISCARD:
            case location_type.REMOVED_FROM_GAME:
                {
                    arrOption = [ { id: this._isFaceUp ? option_type.PUT_FACE_DOWN : option_type.PUT_FACE_UP, isEnabled: true } ];
                    break;
                }
        }

        return arrOption;
    }

    // #endregion //


    // #region Input Callbacks //

    private onCard_PointerTap( event: PIXI.InteractionEvent ): void
    {
        if ( ServiceLocator.game.poiMenu.cContainer.c.visible
            || ( event.target[ "go" ] && Utils.game.findGameObjectInBranchByComponentName( event.target[ "go" ], "CPoi" ) )
            || ServiceLocator.game.dragShadowManager.dragShadow ) 
        { 
            return; 
        }
        
        let showAtPos: PIXI.Point = this._go.cContainer.c.getGlobalPosition();
        showAtPos.x += this._go.cContainer.c.width * 0.5;
        showAtPos.y += this._go.cContainer.c.height * 0.5;

        let arrOption: Array<IOption> = this.createRadialMenuOptions();
        if ( arrOption )
        {
            ServiceLocator.game.cRadialMenu.tryShow( this._go, showAtPos, arrOption );
        }

        this._onPointerTap.dispatch( this._go );
    }

    // #endregion //
}

export interface ICardSide
{
    cardId: string;
    cardInfo: ICard;
    sprite: GameObject;
}