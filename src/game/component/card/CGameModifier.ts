import Component from "../Component";

import { player_action_type, action_scope_type } from "../../../service/socket_io/GameSocketIOController";
import { player_type } from "../world/CPlayerArea";
import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import Signal from "../../../lib/signals/Signal";
import CContainer from "../pixi/CContainer";
import CCardPreviewable from "../input/CCardPreviewable";
import { ISgGameModifier } from "../../../view/game/SaveGameView";
import { IOption, option_type } from "../ui/CRadialMenu";
import { ICardSide } from "./CCard";
import CDetailBar, { detail_bar_icon_type, IDetailBarItem } from "./token/CDetailBar";
import CScrollX from "../ui/CScrollX";
import { target_selection_type } from "../input/CTargetSelector";
import LogTargetCard from "../ui/right_menu/action_log/target/LogTargetCard";
import CDropArea from "../input/CDropArea";
import { layer_type } from "../world/CGameLayerProvider";
import CGraphics from "../pixi/CGraphics";
import CAnimatedFrame from "../ui/CAnimatedFrame";
import { GodrayFilter, OutlineFilter } from "pixi-filters";
import { input_event_type } from "../../InputController";
import CardFactory from "../../CardFactory";


export default class CGameModifier extends Component
{
    // #region Attributes //

    // private:

    private _front: ICardSide = null;
    private _back: ICardSide = null;
    private _ownerPlayer: player_type = null;
    private _controllerPlayer: player_type = null;
    private _isBowed: boolean = false;
    private _isFaceUp: boolean = false;
    private _isHighlighted: boolean = false;

    private _animatedFrame: GameObject = null;
    private _cardImgContainer: PIXI.Container = null;
    private _detailBar: GameObject = null;
    private _poiSocket: GameObject = null;
    private _targetSocket: GameObject = null;

    private _curSide: ICardSide = null;
    private _isAnimated: boolean = true;
    
    private _highlightedFilter: GodrayFilter = null;
    private _bowedFilter: PIXI.Filter = null;
    
    // Signals.
    private _onFaceUp: Signal = new Signal();
    private _onFaceDown: Signal = new Signal();
    private _onPointerTap: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get front(): ICardSide { return this._front; }
    public get back(): ICardSide { return this._back; }
    public get ownerPlayer(): player_type { return this._ownerPlayer; }
    public get controllerPlayer(): player_type { return this._controllerPlayer; }
    public get isBowed(): boolean { return this._isBowed; }
    public get isFaceUp(): boolean { return this._isFaceUp; }
    public get cardImgContainer(): PIXI.Container {return this._cardImgContainer; }
    public get cDetailBar(): CDetailBar { return this._detailBar.cDetailBar; }
    public get curSide(): ICardSide { return this._curSide; }
    public get isAnimated(): boolean { return this._isAnimated; }

    public set frontCardId( value: string ) { this._front.cardId = value; }
    public set backCardId( value: string ) { this._back.cardId = value; }
    public set ownerPlayer( value: player_type ) { this._ownerPlayer = value; }
    public set controllerPlayer( value: player_type ) { this._controllerPlayer = value; }
    public set isBowed( value: boolean ) { this._isBowed = value; }
    public set isFaceUp( value: boolean ) { this._isFaceUp = value; }
    public set isAnimated( value: boolean ) { this._isAnimated = value; }

    // Signals.
    public get onFaceUp(): Signal { return this._onFaceUp; }
    public get onFaceDown(): Signal { return this._onFaceDown; }
    public get onPointerTap(): Signal { return this._onPointerTap; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CGameModifier";

        this._front = { cardId: null, cardInfo: null, sprite: null };
        this._back = { cardId: null, cardInfo: null, sprite: null };
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CGameModifier.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cTargetSelector != null, "CGameModifier.ts :: init() :: CTargetSelector component not found." );
        console.assert( this._go.cTargetReceptor != null, "CGameModifier.ts :: init() :: CTargetReceptor component not found." );
        console.assert( this._front.cardId != null, "CGameModifier.ts :: init() :: this._front.cardId cannot be null." );
        console.assert( this._back.cardId != null, "CGameModifier.ts :: init() :: this._back.cardId cannot be null." );
        console.assert( this._ownerPlayer != null, "CGameModifier.ts :: init() :: this._ownerPlayer cannot be null." );
        console.assert( this._controllerPlayer != null, "CGameModifier.ts :: init() :: this._controllerPlayer cannot be null." );

        this._go.cContainer.c.interactive = true;
        this._go.cContainer.c.buttonMode = true;

        this._cardImgContainer = new PIXI.Container();
        this._cardImgContainer.interactive = true;
        this._go.cContainer.c.addChild( this._cardImgContainer );

        this._front.cardInfo = ServiceLocator.cardDb.find( this._front.cardId );
        this._back.cardInfo = ServiceLocator.cardDb.find( this._back.cardId );

        // Back.
        this._back.sprite = this.createCardSide( this._back );
        this._back.sprite.cContainer.c.visible = !this._isFaceUp;
        this._cardImgContainer.addChild( this._back.sprite.cContainer.c );

        // Front.
        this._front.sprite = this.createCardSide( this._front );
        this._front.sprite.cContainer.c.visible = this._isFaceUp;
        this._cardImgContainer.addChild( this._front.sprite.cContainer.c );

        this._curSide = this._isFaceUp ? this._front : this._back;

        // Frame.
        const kContainerW: number = this._go.cContainer.c.width;
        const kContainerH: number = this._go.cContainer.c.height;
        let frame: PIXI.Graphics = new PIXI.Graphics();
        frame.lineStyle( 2, ServiceLocator.game.playerColors[ this._ownerPlayer ] );
        frame.drawRect( 0, 0, kContainerW, kContainerH );
        this._cardImgContainer.addChild( frame );

        // Animated frame.
        this._animatedFrame = new GameObject( [ new CGraphics(), new CAnimatedFrame() ] );
        this._animatedFrame.cAnimatedFrame.width = kContainerW;
        this._animatedFrame.cAnimatedFrame.height = kContainerH;
        this._animatedFrame.cAnimatedFrame.lineWidth = 2;
        this._animatedFrame.init();
        this._cardImgContainer.addChild( this._animatedFrame.cContainer.c );

        // Shadow.
        let shadow: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( "shadow_game_modifier" ) );
        shadow.pivot.set( 15 );
        this._go.cContainer.c.addChildAt( shadow, 0 );

        // Detail bar.
        this._detailBar = new GameObject( [ new CContainer(), new CDetailBar(), new CScrollX() ] );
        this._detailBar.oid = this._go.oid + "_status_bar";
        this._detailBar.cDetailBar.ownerToken = this._go;
        const kdetailBarItemSide: number = 20;
        this._detailBar.cDetailBar.itemSide = kdetailBarItemSide;
        const kdetailBarItemCount: number = Math.floor( frame.width / kdetailBarItemSide );
        this._detailBar.cScrollX.width = kdetailBarItemSide * kdetailBarItemCount;
        this._detailBar.cScrollX.height = kdetailBarItemSide;
        this._detailBar.cScrollX.itemWidth = kdetailBarItemSide;
        this._detailBar.init();
        this._detailBar.cContainer.c.position.set( 
            ( this._curSide.sprite.cContainer.c.width - kdetailBarItemSide * kdetailBarItemCount ) * 0.5, 
            this._curSide.sprite.cContainer.c.height - kdetailBarItemSide - 2 );
        this._go.cContainer.addChild( this._detailBar );

        // POI socket.
        this._poiSocket = new GameObject( [ new CContainer() ] );
        this._poiSocket.init();
        this._cardImgContainer.addChild( this._poiSocket.cContainer.c );
        this._poiSocket.cContainer.c.position.set( this.calcWidth() * 0.5, 5 );
        this._go.cHighlightPoiReceptor.poiSocket = this._poiSocket;

        if ( this._isBowed )
        {
            this.applyBowedFilter();
        }

        // Target socket.
        this._targetSocket = new GameObject( [ new CContainer() ] );
        this._targetSocket.init();
        this._targetSocket.cContainer.c.position.set( this._cardImgContainer.width * 0.5, this._cardImgContainer.height * 0.5 );
        this._go.cContainer.addChild( this._targetSocket );
        //
        this._go.cTargetReceptor.targetSocket = this._targetSocket;

        // Listen to events.
        ServiceLocator.game.inputController.on( this._cardImgContainer, input_event_type.TAP, this.onCard_PointerTap, this );
        this._go.cTargetSelector.onTargetSelected.add( this.onTarget_Selected, this );
    }

        private createCardSide( cardSide: ICardSide ): GameObject
        {
            let result: GameObject = new GameObject( [ new CContainer(), new CCardPreviewable() ] );
            result.cCardPreviewable.cardId = cardSide.cardId;
            result.init();

            let portrait: PIXI.Sprite = PIXI.Sprite.from( Utils.img.createCardPortrait( cardSide.cardId, { isSquare: true }  ) );
            portrait.name = "portrait";
            portrait.filters = [];
            Utils.game.limitSideSize( 46, portrait );
            result.cContainer.c.addChild( portrait );

            let sideChar: PIXI.Text = new PIXI.Text( cardSide == this._front ? "A" : "B", ServiceLocator.game.textStyler.normal );
            sideChar.anchor.set( 1, 0 );
            sideChar.position.x = portrait.width;
            result.cContainer.c.addChild( sideChar );

            return result;
        }

    public end(): void
    {
        // Cleanup events.
        ServiceLocator.game.inputController.off( this._cardImgContainer, input_event_type.TAP, this.onCard_PointerTap, this );
        this._go.cTargetSelector.onTargetSelected.remove( this.onTarget_Selected, this );

        this._onFaceUp.removeAll();
        this._onFaceUp.removeAll();
        this._onPointerTap.removeAll();

        if ( this._highlightedFilter )
        {
            PIXI.Ticker.shared.remove( this.updateHighlightedFilter, this );
            this._highlightedFilter = null;
        }
        this._bowedFilter = null;

        this._targetSocket.end();
        this._targetSocket = null;

        this._animatedFrame.end();
        this._animatedFrame = null;

        this._detailBar.end();
        this._detailBar = null;

        this._poiSocket.end();
        this._poiSocket = null;

        this._front.sprite.end();
        this._front = null;

        this._back.sprite.end();
        this._back = null;

        this._curSide = null;

        super.end();
    }

    public calcWidth(): number { return this._curSide.sprite.cContainer.c.width; }
    public calcHeight(): number { return this._curSide.sprite.cContainer.c.height; }

    public setBowed( isBowed: boolean, actionScopeType: action_scope_type ): void
    {
        if ( isBowed == this._isBowed ) { return; }

        if ( this._isAnimated )
        {
            Utils.anim.bumpFromCorner( this._go.cContainer.c );
        }

        if ( isBowed )
        {
            this.applyBowedFilter();
        }
        else
        {
            this._front.sprite.cContainer.c.getChildByName( "portrait" ).filters.splice( this._front.sprite.cContainer.c.getChildByName( "portrait" ).filters.indexOf( this._bowedFilter ), 1 );
            this._back.sprite.cContainer.c.getChildByName( "portrait" ).filters.splice( this._back.sprite.cContainer.c.getChildByName( "portrait" ).filters.indexOf( this._bowedFilter ), 1 );

            this._bowedFilter = null;
        }

        this._isBowed = isBowed;

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( isBowed ? player_action_type.BOW_GAME_MODIFIER : player_action_type.READY_GAME_MODIFIER, null, [ this._go.oid ] );
        }        
    }

        private applyBowedFilter(): void
        {
            let cmf: PIXI.filters.ColorMatrixFilter = new PIXI.filters.ColorMatrixFilter();
            cmf.blackAndWhite( true );
            cmf.brightness( 0.75, true );
            this._front.sprite.cContainer.c.getChildByName( "portrait" ).filters.push( cmf );
            this._back.sprite.cContainer.c.getChildByName( "portrait" ).filters.push( cmf );

            this._bowedFilter = cmf;
        }

    public setFaceUp( actionScopeType: action_scope_type ): void
    {
        if ( this._isFaceUp ) { return; }

        if ( this._isAnimated && ServiceLocator.game.isAnimated )
        {
            this._back.sprite.cCardPreviewable.setEnabled( false );
            this.bump( () => {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "card_face_up" );

                if ( ServiceLocator.game.cardPreview.cCardPreview.isTarget( this._curSide.sprite ) )
                {  
                    ServiceLocator.game.cardPreview.cCardPreview.hide();
                }

                this._front.sprite.cContainer.c.visible = true;
                this._back.sprite.cContainer.c.visible = false;
                this._curSide = this._front;

                this._back.sprite.cCardPreviewable.setEnabled( true );
                this._curSide.sprite.cCardPreviewable.refresh();
            } );
        }
        else
        {
            this._front.sprite.cContainer.c.visible = true;
            this._back.sprite.cContainer.c.visible = false;
            this._curSide = this._front;
        }

        this._isFaceUp = true;

        this._onFaceUp.dispatch( this._go );

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_GAME_MODIFIER_FACE_UP, null, [ this._go.oid ] );  
        }
    }

    public setFaceDown( actionScopeType: action_scope_type ): void
    {
        if ( !this._isFaceUp ) { return; }

        if ( this._isAnimated && ServiceLocator.game.isAnimated )
        {
            this._front.sprite.cCardPreviewable.setEnabled( false );
            this.bump( () => {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "card_face_down" );

                if ( ServiceLocator.game.cardPreview.cCardPreview.isTarget( this._curSide.sprite ) )
                {  
                    ServiceLocator.game.cardPreview.cCardPreview.hide();
                }
                
                this._front.sprite.cContainer.c.visible = false;
                this._back.sprite.cContainer.c.visible = true; 
                this._curSide = this._back;
                
                this._front.sprite.cCardPreviewable.setEnabled( true );
                this._curSide.sprite.cCardPreviewable.refresh();
            } );
        }
        else
        {
            this._front.sprite.cContainer.c.visible = false;
            this._back.sprite.cContainer.c.visible = true; 
            this._curSide = this._back;
        }
        
        this._isFaceUp = false;

        this._onFaceDown.dispatch( this._go );

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_GAME_MODIFIER_FACE_DOWN, null, [ this._go.oid ] );  
        }
    }

    public discard( actionScope: action_scope_type ): void
    {
        Utils.game.discard( this._go, actionScope ); 
    }

    public discardUnderneath( actionScope: action_scope_type ): void
    {
        let underneathItem: IDetailBarItem = this._detailBar.cDetailBar.findItem( detail_bar_icon_type.UNDERNEATH );
        if ( underneathItem )
        {
            underneathItem.root.cUnderneathButton.discard( actionScope );
        }
    }

    public setSelectedVfx( isVisible: boolean ): void
    {
        if ( isVisible )
        {
            const kColor: number = ServiceLocator.game.playerColors[ this._controllerPlayer ];
            let outlineFilter: OutlineFilter = new OutlineFilter( 5, kColor, 0.5 );
            outlineFilter.padding = 5;
            this._go.cContainer.c.filters = [ outlineFilter ];
        }
        else
        {
            this._go.cContainer.c.filters = [];
        }
    }

    public setHighlighted( isHighlighted: boolean, actionScope: action_scope_type ): void
    {
        this._isHighlighted = isHighlighted;

        if ( this._isHighlighted )
        {
            this._animatedFrame.cAnimatedFrame.start();

            this._highlightedFilter = new GodrayFilter();
            this._front.sprite.cContainer.c.getChildByName( "portrait" ).filters.push( this._highlightedFilter );
            this._back.sprite.cContainer.c.getChildByName( "portrait" ).filters.push( this._highlightedFilter );

            PIXI.Ticker.shared.add( this.updateHighlightedFilter, this );

            // Multiplayer.
            if ( actionScope == action_scope_type.MULTIPLAYER )
            {
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ADD_GAME_MODIFIER_HIGHLIGHT, null, [ this._go.oid ] );
            }
        }
        else
        {
            this._animatedFrame.cAnimatedFrame.stop();

            this._front.sprite.cContainer.c.getChildByName( "portrait" ).filters.splice( this._front.sprite.cContainer.c.getChildByName( "portrait" ).filters.indexOf( this._highlightedFilter ), 1 );
            this._back.sprite.cContainer.c.getChildByName( "portrait" ).filters.splice( this._back.sprite.cContainer.c.getChildByName( "portrait" ).filters.indexOf( this._highlightedFilter ), 1 );
            this._highlightedFilter = null;
    
            PIXI.Ticker.shared.remove( this.updateHighlightedFilter, this );
    
            // Multiplayer.
            if ( actionScope == action_scope_type.MULTIPLAYER )
            {
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.REMOVE_GAME_MODIFIER_HIGHLIGHT, null, [ this._go.oid ] );
            }
        }
    }
    
        private updateHighlightedFilter(): void
        {
            this._highlightedFilter.time += PIXI.Ticker.shared.elapsedMS / 1000;
        }

    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        this._go.cContainer.c.buttonMode = isEnabled;
        
        ServiceLocator.game.inputController.off( this._cardImgContainer, input_event_type.TAP, this.onCard_PointerTap, this );
        if ( isEnabled )
        {
            ServiceLocator.game.inputController.on( this._cardImgContainer, input_event_type.TAP, this.onCard_PointerTap, this );
        }

        this._detailBar.cDetailBar.setEnabled( isEnabled );
    }

    public bump( onRepeat: ( ...args: any[] ) => void = null ): gsap.core.Tween
    {
        return Utils.anim.bumpFromCorner( this._go.cContainer.c, onRepeat );
    }

    public flash(): void
    {
        let vfxLayer: GameObject = Utils.game.findGameProviderLayer( this._go, layer_type.VFX );
        let flashPos: PIXI.Point = new PIXI.Point().copyFrom( vfxLayer.cContainer.c.toLocal( this._cardImgContainer.getGlobalPosition() ) );
        flashPos.x += this._cardImgContainer.width * 0.5;
        flashPos.y += this._cardImgContainer.height * 0.5;
        Utils.anim.flash( flashPos, this._cardImgContainer.width,this._cardImgContainer.height, 1.5, vfxLayer );
    }

    public addCardUnderneath( added: GameObject, actionScope: action_scope_type ): void
    {
        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ADD_CARD_UNDERNEATH, null, [ this._go.oid, added.oid ] );
        }

        let underneathItem: IDetailBarItem = this._detailBar.cDetailBar.findItem( detail_bar_icon_type.UNDERNEATH );
        if ( !underneathItem )
        {
            underneathItem = this._detailBar.cDetailBar.addItem( detail_bar_icon_type.UNDERNEATH );
        }
        underneathItem.root.cUnderneathButton.cDeckIndicator.cViewer.cCardView.go.cDropArea.forceDrop( added, new PIXI.Point( CDropArea.kPredefinedDropPositionCode, 0 ), action_scope_type.LOCAL );
        let arrCard: Array<GameObject> = underneathItem.root.cUnderneathButton.cDeckIndicator.cViewer.cCardView.findItems();
        arrCard[ arrCard.length - 1 ].cCard.locationData = this._go.oid;
    }

    // virtual.
    public saveGame(): ISgGameModifier
    {
        return { 
            oid: this._go.oid,
            isBowed: this._isBowed,
            isFaceUp: this._isFaceUp,
            isHighlighted: this._isHighlighted,
            detailBar: this._detailBar.cDetailBar.saveGame(),
            controller: this._controllerPlayer };
    }

    // virtual.
    public loadGame( sgGameModifier: ISgGameModifier, pass: number ): void
    {
        if ( pass == 0 )
        {
            this._controllerPlayer = sgGameModifier.controller;
            this.setEnabled( this._controllerPlayer == player_type.PLAYER );

            if ( sgGameModifier.isBowed )
            {
                this.setBowed( true, action_scope_type.LOCAL );
            }

            if ( !sgGameModifier.isFaceUp )
            {
                this.setFaceDown( action_scope_type.LOCAL );
            }

            if ( sgGameModifier.isHighlighted )
            {
                this.setHighlighted( true, action_scope_type.LOCAL );
            }

            if ( sgGameModifier.detailBar != null )
            {
                this._detailBar.cDetailBar.loadGame( sgGameModifier.detailBar, pass );
            }
        }
    }

    // #endregion //


    // #region Input Callbacks //

    private onCard_PointerTap( event: PIXI.InteractionEvent ): void
    {
        if ( ServiceLocator.game.poiMenu.cContainer.c.visible
            || ( event.target[ "go" ] && Utils.game.findGameObjectInBranchByComponentName( event.target[ "go" ], "CPoi" ) )
            || ServiceLocator.game.dragShadowManager.dragShadow ) 
        { 
            return; 
        }
        
        let showAtPos: PIXI.Point = this._go.cContainer.c.getGlobalPosition();
        showAtPos.x += this._cardImgContainer.width * 0.5;
        showAtPos.y += this._cardImgContainer.height * 0.5;

        let arrExtrasOption: Array<IOption> = [
            { id: option_type.EQUIP, isEnabled: true },
            { id: option_type.UNDERNEATH, isEnabled: true },  
            { id: !this._isHighlighted ? option_type.ADD_HIGHLIGHTING : option_type.REMOVE_HIGHLIGHTING, isEnabled: true },
            { id: option_type.ADD_COUNTER, isEnabled: !this._detailBar.cDetailBar.isMaxCustomCounter() } ];
        if ( !this._detailBar.cDetailBar.findItem( detail_bar_icon_type.CUSTOM_TEXT ) )
        {
            arrExtrasOption.push( { id: option_type.ADD_CUSTOM_TEXT, isEnabled: true } );
        }
        arrExtrasOption.push( { id: option_type.BACK, isEnabled: true } );
        //
        const kArrOption: Array<IOption> = [ 
            { id: option_type.TARGET, isEnabled: true },
            { id: this._isBowed ? option_type.READY : option_type.BOW, isEnabled: true },
            { id: this._isFaceUp ? option_type.PUT_FACE_DOWN : option_type.PUT_FACE_UP, isEnabled: this._front.cardId.substr( this._front.cardId.length - 1 ) == "A" }, 
            { id: option_type.DISCARD, isEnabled: true }, 
            { id: option_type.EXTRAS, isEnabled: true, subMenu: arrExtrasOption } ]; 
        ServiceLocator.game.cRadialMenu.tryShow( this._go, showAtPos, kArrOption );

        this._onPointerTap.dispatch( this._go );
    }

    // #endregion //


    // #region Other Callbacks //

    private onTarget_Selected( target: GameObject, targetSelectionType: target_selection_type ): void
    {
        if ( targetSelectionType == target_selection_type.TARGET )
        {
            // Sfx.
            ServiceLocator.audioManager.playSfx( "button_click" );

            ServiceLocator.game.cGameWorld.cActionLogger.logTargetSelection( 
                player_type.PLAYER, new LogTargetCard( this._go ), new LogTargetCard( target ), "rad_target", false, true );
        }
        else if ( targetSelectionType == target_selection_type.EQUIP )
        {
            // Sfx.
            ServiceLocator.audioManager.playSfx( "equipped" );

            target.cCardTokenSide.cardToken.cDropArea.forceDrop( this._go, new PIXI.Point(), action_scope_type.MULTIPLAYER );
        }
        else if ( targetSelectionType == target_selection_type.UNDERNEATH )
        {
            // Sfx.
            ServiceLocator.audioManager.playSfx( "button_click" );

            ServiceLocator.game.cGameWorld.cActionLogger.logTargetSelection( 
                player_type.PLAYER, new LogTargetCard( this._go ), new LogTargetCard( target ), "rad_underneath", false, true );
            
            target.cCardTokenSide.cardToken.cCardToken.addCardUnderneath( this._go, action_scope_type.MULTIPLAYER );  
        }
    }

    // #endregion //
}