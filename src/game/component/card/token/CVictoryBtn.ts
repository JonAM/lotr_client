
import Component from "../../Component";

import { view_layer_id } from "../../../../service/ViewManager";
import { player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import { action_request_type } from "../../../AllyActionManager";
import { gsap, Linear } from "gsap";
import ServiceLocator from "../../../../ServiceLocator";
import Session from "../../../../Session";
import * as PIXI from "pixi.js";

import CButton from "../../input/CButton";
import CSprite from "../../pixi/CSprite";
import CTooltipReceptor from "../../ui/CTooltipReceptor";
import GameObject from "../../../GameObject";
import InfoMessageView, { btn_layout_type } from "../../../../view/common/popup/InfoMessageView";
import VictoryView from "../../../../view/game/VictoryView";


export default class CVictoryBtn extends Component
{
    // #region Attributes //

    // private:

    private _button: GameObject = null;
    private _lightRaysTween: gsap.core.Tween = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CVictoryBtn";
    }

    public init(): void
    {
        console.assert( this._go.cContainer != null, "CVictoryBtn.ts :: init() :: CContainer component not found." );

        super.init();

        let lightRays: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( "quest_light_rays" ) );
        lightRays.anchor.set( 0.5 );
        this._lightRaysTween = gsap.to( lightRays, { 
            rotation: 2 * Math.PI,
            duration: 20,
            repeat: -1,
            ease: Linear.easeInOut } );
        this._go.cContainer.c.addChild( lightRays );
        
        this._button = new GameObject( [ new CSprite(), new CButton(), new CTooltipReceptor() ] );
        this._button.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "quest_completed" );
        this._button.cSprite.s.anchor.set( 0.5 );
        this._button.cTooltipReceptor.text = jQuery.i18n( "TO_VICTORY_SCREEN" );
        this._button.init();
        this._go.cContainer.addChild( this._button );
        this._button.cButton.onClick.add( this.onButton_Click, this );
    }

    public end(): void
    {
        this._lightRaysTween.kill();
        this._lightRaysTween = null;

        this._button.end();
        this._button = null;

        super.end();
    }

    public show(): void
    {
        this._go.cContainer.c.visible = true;
        this._lightRaysTween.resume();
    }

    public hide(): void
    {
        this._go.cContainer.c.visible = false;
        this._lightRaysTween.pause();
    }

    // #endregion //


    // #region Input Callbacks //

    private onButton_Click(): void
    {
        if ( !Session.allyId )
        {
            let endGameConfirmationView: InfoMessageView = new InfoMessageView();
            endGameConfirmationView.message = jQuery.i18n( "TO_VICTORY_SCREEN_CONFIRMATION" );
            endGameConfirmationView.btnLayoutType = btn_layout_type.DOUBLE;
            endGameConfirmationView.init();
            endGameConfirmationView.onClosed.addOnce( this.onEndGameConfirmationPopup_Closed, this );
            ServiceLocator.viewManager.fadeIn( endGameConfirmationView, view_layer_id.POPUP );
        }
        else
        {
            let waitForAllyConfirmationView: InfoMessageView = new InfoMessageView();
            waitForAllyConfirmationView.alias = "waitForAllyConfirmationView";
            waitForAllyConfirmationView.message = jQuery.i18n( "WAIT_FOR_ALLY_CONFIRMATION" );
            waitForAllyConfirmationView.btnLayoutType = btn_layout_type.DOUBLE;
            waitForAllyConfirmationView.init();
            waitForAllyConfirmationView.root.find( "#infoMessageAcceptBtn" ).hide();
            waitForAllyConfirmationView.onClosed.addOnce( this.onWaitForAllyConfirmationPopup_Closed, this );
            ServiceLocator.viewManager.fadeIn( waitForAllyConfirmationView, view_layer_id.POPUP );

            // Multiplayer.
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ACTION_REQUESTED, null, [ action_request_type.GO_TO_VICTORY_SCREEN ] );
        }
    }

        private onEndGameConfirmationPopup_Closed( result: boolean ): void
        {
            if ( result )
            {
                let victoryView: VictoryView = new VictoryView();
                victoryView.init();
                ServiceLocator.viewManager.fadeIn( victoryView, view_layer_id.POPUP );
            }   
        }

        private onWaitForAllyConfirmationPopup_Closed(): void
        {
            // Multiplayer.
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ACTION_REQUEST_CANCELLED, null, null );
        }

    // #endregion //
}