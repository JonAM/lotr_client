import CCardTokenSide, { status_type } from "../CCardTokenSide";

import { action_scope_type } from "../../../../../service/socket_io/GameSocketIOController";
import { player_type } from "../../../world/CPlayerArea";
import ServiceLocator from "../../../../../ServiceLocator";
import { detail_bar_icon_type } from "../../token/CDetailBar";
import Utils from "../../../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../../../GameObject";
import CSprite from "../../../pixi/CSprite";
import CTokenCounter from "../../../ui/CTokenCounter";
import CDropArea from "../../../input/CDropArea";
import CDraggable from "../../../input/CDraggable";
import LogTargetCard from "../../../ui/right_menu/action_log/target/LogTargetCard";
import { IOption, option_type } from "../../../ui/CRadialMenu";
import Session from "../../../../../Session";
import { ISgCardTokenSide } from "../../../../../view/game/SaveGameView";
import CContainer from "../../../pixi/CContainer";
import { IScenario } from "../../../../ScenarioDB";
import CVictoryBtn from "../CVictoryBtn";


export default class CQuestSide extends CCardTokenSide
{
    // #region Attributes //

    // private:

    private _questPoints: GameObject = null;
	private _questPointsText: PIXI.Text = null;
	private _progress: GameObject = null;
	private _progressText: PIXI.Text = null;
    private _lockOverlay: PIXI.Container = null;
    private _victoryBtn: GameObject = null;
    private _isFinal: boolean = null;

    // #endregion //


    // #region Properties //

    public get progress(): GameObject { return this._progress; }
    public get questPoints(): GameObject { return this._questPoints; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CQuestSide";
    }

    public init(): void
    {
        super.init();

        if ( this._cardInfo.quest_points == undefined )
        {
            this._cardInfo.quest_points = 0;
        }

        const kScenarioInfo: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
        this._isFinal = kScenarioInfo.win && kScenarioInfo.win.indexOf( this._cardId ) != -1;

        // Quest points.
        let container: PIXI.Container = new PIXI.Container();
        // Counter.
        this._questPoints = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._questPoints.oid = this._go.oid + "_quest_points";
        this._questPoints.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "quest_points" ).data );
        this._questPoints.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 40, this._questPoints.cSprite.s );
        this._questPoints.cTokenCounter.tokenName = "quest_points";
        this._questPoints.cTokenCounter.minCount = 0;
        this._questPoints.cTokenCounter.maxCount = 99;
        this._questPoints.cTokenCounter.count = this.findTokenCounterValue( this._cardInfo.quest_points );
        this._questPoints.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "quest_points" ).data );
        this._questPoints.cDropArea.target = this._questPoints.cTokenCounter;
        this._questPoints.init();
        container.addChild( this._questPoints.cContainer.c );
        this._questPoints.cTokenCounter.onCountUpdated.add( this.onQuestPointCount_Updated.bind( this, this._questPoints ) );
        // Text.
        this._questPointsText = new PIXI.Text( this.findTokenCounterText( this._cardInfo.quest_points ), ServiceLocator.game.textStyler.normal );
        this._questPointsText.anchor.set( 0.5 );
        container.addChild( this._questPointsText );
        //
        if ( this._cardInfo.quest_points == 255 || this._cardInfo.quest_points == 0 )
        {
            this._questPoints.cTokenCounter.setEnabled( false );
            this._questPointsText.text = "-";
            this._colouringLayer.visible = false;
        }
        //
        container.position.set( 10, this._bg.cContainer.c.height * 0.5 + container.height * 0.5 );
        this._go.cContainer.c.addChild( container );

        // Progress.
        container = new PIXI.Container();
        // Counter.
        this._progress = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._progress.oid = this._go.oid + "_progress";
        this._progress.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "progress_token" ).data );
        this._progress.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 50, this._progress.cSprite.s );
        this._progress.cTokenCounter.tokenName = "progress";
        this._progress.cTokenCounter.minCount = 0;
        this._progress.cTokenCounter.maxCount = 99;
        this._progress.cTokenCounter.count = 0; 
        this._progress.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "progress_token" ).data );
        this._progress.cDropArea.target = this._progress.cTokenCounter;
        this._progress.init();
        container.addChild( this._progress.cContainer.c );
        this._progress.cTokenCounter.onCountUpdated.add( this.onProgressCount_Updated.bind( this, this._progress ) );
        // Text.
        this._progressText = new PIXI.Text( this._progress.cTokenCounter.count.toString(), ServiceLocator.game.textStyler.normal );
        this._progressText.anchor.set( 0.5 );
        container.addChild( this._progressText );
        //
        container.position.set( this._bg.cContainer.c.width * 0.5, this._bg.cContainer.c.height - container.height * 0.5 + 10 );
        this._go.cContainer.c.addChild( container );

        // Lock.
        this._lockOverlay = new PIXI.Container();
        this._lockOverlay.visible = false;
        this._go.cContainer.c.addChild( this._lockOverlay );
        //
        let chains: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( "quest_chains" ) );
        this._lockOverlay.addChild( chains );
        //
        let lock: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( "quest_lock" ) );
        Utils.game.limitSideSize( 70, lock );
        lock.anchor.set( 1, 0 );
        lock.position.set( this._bg.cContainer.c.width + 10, -10 );
        this._lockOverlay.addChild( lock );

        // Victory button.
        if ( this._isFinal )
        {
            this._victoryBtn = new GameObject( [ new CContainer(), new CVictoryBtn() ] );
            this._victoryBtn.init();
            this._victoryBtn.cContainer.c.position.set( 15, 15 );
            this._go.cContainer.c.addChild( this._victoryBtn.cContainer.c );
            if ( this._cardInfo.quest_points == 0 || this._cardInfo.quest_points == 255 )
            {
                this._victoryBtn.cVictoryBtn.show();
            }
            else
            {
                this._victoryBtn.cVictoryBtn.hide();
            }
        }
    }

    public end(): void
    {
        this._questPoints.end();
        this._questPoints = null;

        this._progress.end();
        this._progress = null;

        if ( this._victoryBtn )
        {
            this._victoryBtn.end();
            this._victoryBtn = null;
        }

        super.end();
    }

    public setLockOverlayVisibility( isVisible: boolean ): void
    {
        this._lockOverlay.visible = isVisible;
    }

    // overrides.
    public saveGame(): ISgCardTokenSide
    {
        let result: ISgCardTokenSide = super.saveGame();
        
        result.questSide = {
            questPoints: this.findTokenCounterSaveValue( this._questPointsText, this._questPoints ),
            progress: this._progress.cTokenCounter.count };

        return result;
    }

    // overrides.
    public loadGame( sgCardTokenSide: ISgCardTokenSide, pass: number ): void
    {
        if ( pass == 0 )
        {
            super.loadGame( sgCardTokenSide, pass );

            if ( sgCardTokenSide.questSide.questPoints != null )
            {
                this._questPoints.cTokenCounter.setCount( sgCardTokenSide.questSide.questPoints, action_scope_type.LOCAL );
            }
            if ( sgCardTokenSide.questSide.progress > 0 )
            {
                this._progress.cTokenCounter.setCount( sgCardTokenSide.questSide.progress, action_scope_type.LOCAL );
            }
        }
    }

    // protected:

    // overrides.
    protected showRadialMenu(): void
    {
        let showAtPos: PIXI.Point = this._bg.cContainer.c.getGlobalPosition();
        showAtPos.x += this._bg.cContainer.c.width * 0.5;
        showAtPos.y += this._bg.cContainer.c.height * 0.5;

        let arrExtrasOption: Array<IOption> = [
            { id: option_type.EQUIP, isEnabled: true },
            { id: option_type.UNDERNEATH, isEnabled: true },  
            { id: !this._highlightedFilter ? option_type.ADD_HIGHLIGHTING : option_type.REMOVE_HIGHLIGHTING, isEnabled: true },
            { id: option_type.ADD_COUNTER, isEnabled: !this._cardToken.cCardToken.cDetailBar.isMaxCustomCounter() } ];
        if ( !this._cardToken.cCardToken.cDetailBar.findItem( detail_bar_icon_type.CUSTOM_TEXT ) )
        {
            arrExtrasOption.push( { id: option_type.ADD_CUSTOM_TEXT, isEnabled: true } );
        }
        arrExtrasOption.push( { id: option_type.BACK, isEnabled: true } );
        //
        let arrOption: Array<IOption> = [ 
            { id: this._cardToken.cCardToken.isFaceUp ? option_type.PUT_FACE_DOWN : option_type.PUT_FACE_UP, isEnabled: true }, 
            { id: option_type.DISCARD, isEnabled: true },
            { id: option_type.EXTRAS, isEnabled: true, subMenu: arrExtrasOption } ]; 
        ServiceLocator.game.cRadialMenu.tryShow( this._go, showAtPos, arrOption );
    }

    // #endregion //


    // #region Other Callbacks //

    private onQuestPointCount_Updated( tokenCounter: GameObject, count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._questPointsText.text = count.toString();

        if ( this._cardInfo.quest_points < 253 )
        {
            if ( count > this._cardInfo.quest_points )
            {
                this._questPointsText.tint = 0x00ff00;
            }
            else if ( count < this._cardInfo.quest_points )
            {
                this._questPointsText.tint = 0xff0000;
            }
            else
            {
                this._questPointsText.tint = 0xffffff;
            }
        }

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( count <= this._progress.cTokenCounter.count )
        {
            this.addStatus( status_type.EXPLORED, action_scope_type.LOCAL );
            if ( this._isFinal )
            {
                this._victoryBtn.cVictoryBtn.show();
            }
        }
        else
        {
            this.removeStatus( status_type.EXPLORED, action_scope_type.LOCAL );
            if ( this._isFinal )
            {
                this._victoryBtn.cVictoryBtn.hide();
            }
        }

        this.drawColouringLayer( this._progress.cTokenCounter.count, this._questPoints.cTokenCounter.count, 0x00ff00 );

        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._go ), "quest_points", delta, count, true );
        }
    }

    private onProgressCount_Updated( tokenCounter: GameObject, count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._progressText.text = count.toString();

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( this._cardInfo.quest_points > 0 && this._cardInfo.quest_points < 253
            && this.findTokenCounterSaveValue( this._questPointsText, this._questPoints ) != null )
        {
            if ( count >= this._questPoints.cTokenCounter.count )
            {
                this.addStatus( status_type.EXPLORED, action_scope_type.LOCAL );
                if ( this._isFinal )
                {
                    this._victoryBtn.cVictoryBtn.show();
                }
            }
            else
            {
                this.removeStatus( status_type.EXPLORED, action_scope_type.LOCAL );
                if ( this._isFinal )
                {
                    this._victoryBtn.cVictoryBtn.hide();
                }
            }

            this.drawColouringLayer( this._progress.cTokenCounter.count, this._questPoints.cTokenCounter.count, 0x00ff00 );
        }

        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._go ), "progress_token", delta, count, true );
        }
    }

    // #endregion //
}