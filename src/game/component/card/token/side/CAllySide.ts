import CCardTokenSide, { status_type } from "../CCardTokenSide";

import { action_scope_type, player_action_type } from "../../../../../service/socket_io/GameSocketIOController";
import { game_state_id } from "../../../../../states/StateId";
import { option_type, IOption } from "../../../ui/CRadialMenu";
import { player_type } from "../../../world/CPlayerArea";
import ServiceLocator from "../../../../../ServiceLocator";
import { detail_bar_icon_type } from "../CDetailBar";
import Utils from "../../../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../../../GameObject";
import CSprite from "../../../pixi/CSprite";
import Signal from "../../../../../lib/signals/Signal";
import CTokenCounter from "../../../ui/CTokenCounter";
import CDropArea from "../../../input/CDropArea";
import CDraggable from "../../../input/CDraggable";
import LogTargetCard from "../../../ui/right_menu/action_log/target/LogTargetCard";
import { ISgCardToken, ISgCardTokenSide } from "../../../../../view/game/SaveGameView";
import CTooltipReceptor from "../../../ui/CTooltipReceptor";
import ICharacterTokenSide from "../../ICharacterTokenSide";


export default class CAllySide extends CCardTokenSide implements ICharacterTokenSide
{
    // #region Attributes //

    // private:

	private _willpower: GameObject = null;
    private _willpowerText: PIXI.Text = null;
	private _attack: GameObject = null;
    private _attackText: PIXI.Text = null;
	private _defense: GameObject = null;
	private _defenseText: PIXI.Text = null;
	private _health: GameObject = null;
	private _healthText: PIXI.Text = null;
	private _wound: GameObject = null;
	private _woundText: PIXI.Text = null;
	private _sphere: GameObject = null;

    private _resource: GameObject = null;
    private _resourceText: PIXI.Text = null;

    // Signals.
    private _onCommittedToQuest: Signal = new Signal();
    private _onUncommittedFromQuest: Signal = new Signal();
    
    // #endregion //


    // #region Properties //

    public get willpower(): GameObject { return this._willpower; }
    public get attack(): GameObject { return this._attack; }
    public get defense(): GameObject { return this._defense; }
    public get health(): GameObject { return this._health; }
    public get wound(): GameObject { return this._wound; }
    public get resource(): GameObject { return this._resource; }

    // Signals.
    public get onCommittedToQuest(): Signal { return this._onCommittedToQuest; }
    public get onUncommittedFromQuest(): Signal { return this._onUncommittedFromQuest; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CAllySide";
    }

    public init(): void
    {
        super.init();

        // Willpower.
        let container: PIXI.Container = new PIXI.Container(); 
        // Counter.
        this._willpower = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._willpower.oid = this._go.oid + "_willpower";
        this._willpower.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "player_will" ).data );
        this._willpower.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 40, this._willpower.cSprite.s );
        this._willpower.cTokenCounter.tokenName = "willpower";
        this._willpower.cTokenCounter.minCount = -99;
        this._willpower.cTokenCounter.maxCount = 99;
        this._willpower.cTokenCounter.count = this.findTokenCounterValue( this._cardInfo.willpower );
        this._willpower.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "player_will" ).data );
        this._willpower.cDropArea.target = this._willpower.cTokenCounter;
        this._willpower.init();
        container.addChild( this._willpower.cContainer.c );
        this._willpower.cTokenCounter.onCountUpdated.add( this.onWillpowerCount_Updated.bind( this, this._willpower ) );
        if ( this._cardInfo.willpower == 255 )
        {
            this._willpower.cTokenCounter.setEnabled( false );
        }
        // Text.
        this._willpowerText = new PIXI.Text( this.findTokenCounterText( this._cardInfo.willpower ), ServiceLocator.game.textStyler.normal );
        this._willpowerText.anchor.set( 1, 0.5 );
        this._willpowerText.x = -5;
        this._willpowerText.y = 1;
        container.addChild( this._willpowerText );
        //
        container.position.set( 10, container.height + 5 );
        this._go.cContainer.c.addChild( container );

        // Attack.
        container = new PIXI.Container(); 
        // Counter.
        this._attack = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._attack.oid = this._go.oid + "_attack";
        this._attack.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "player_attack" ).data );
        this._attack.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 40, this._attack.cSprite.s );
        this._attack.cTokenCounter.tokenName = "attack";
        this._attack.cTokenCounter.minCount = -99;
        this._attack.cTokenCounter.maxCount = 99;
        this._attack.cTokenCounter.count = this.findTokenCounterValue( this._cardInfo.attack );
        this._attack.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "player_attack" ).data );
        this._attack.cDropArea.target = this._attack.cTokenCounter;
        this._attack.init();
        container.addChild( this._attack.cContainer.c );
        this._attack.cTokenCounter.onCountUpdated.add( this.onAttackCount_Updated.bind( this, this._attack ) );
        if ( this._cardInfo.attack == 255 )
        {
            this._attack.cTokenCounter.setEnabled( false );
        }
        // Text.
        this._attackText = new PIXI.Text( this.findTokenCounterText( this._cardInfo.attack ), ServiceLocator.game.textStyler.normal );
        this._attackText.anchor.set( 1, 0.5 );
        this._attackText.x = -5;
        this._attackText.y = 1;
        container.addChild( this._attackText );
        //
        container.position.set( 10, this._willpower.cContainer.c.parent.y + this._willpower.cContainer.c.parent.height + 5 );
        this._go.cContainer.c.addChild( container );

        // Defense.
        container = new PIXI.Container(); 
        // Counter.
        this._defense = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._defense.oid = this._go.oid + "_defense";
        this._defense.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "player_defense" ).data );
        this._defense.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 40, this._defense.cSprite.s );
        this._defense.cTokenCounter.tokenName = "defense";
        this._defense.cTokenCounter.minCount = -99;
        this._defense.cTokenCounter.maxCount = 99;
        this._defense.cTokenCounter.count = this.findTokenCounterValue( this._cardInfo.defense );
        this._defense.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "player_defense" ).data );
        this._defense.cDropArea.target = this._defense.cTokenCounter;
        this._defense.init();
        container.addChild( this._defense.cContainer.c );
        this._defense.cTokenCounter.onCountUpdated.add( this.onDefenseCount_Updated.bind( this, this._defense ) );
        if ( this._cardInfo.defense == 255 )
        {
            this._defense.cTokenCounter.setEnabled( false );
        }
        // Text.
        this._defenseText = new PIXI.Text( this.findTokenCounterText( this._cardInfo.defense ), ServiceLocator.game.textStyler.normal );
        this._defenseText.anchor.set( 1, 0.5 );
        this._defenseText.x = -5;
        this._defenseText.y = 1;
        container.addChild( this._defenseText );
        //
        container.position.set( 10, this._attack.cContainer.c.parent.y + this._attack.cContainer.c.parent.height + 5 );
        this._go.cContainer.c.addChild( container );

        // Health.
        container = new PIXI.Container();
        // Counter.
        this._health = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._health.oid = this._go.oid + "_health";
        this._health.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "player_health" ).data );
        this._health.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 40, this._health.cSprite.s );
        this._health.cTokenCounter.tokenName = "health";
        this._health.cTokenCounter.minCount = 0;
        this._health.cTokenCounter.maxCount = 99;
        this._health.cTokenCounter.count = this.findTokenCounterValue( this._cardInfo.health ); 
        this._health.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "player_health" ).data );
        this._health.cDropArea.target = this._health.cTokenCounter;
        this._health.init();
        container.addChild( this._health.cContainer.c );
        this._health.cTokenCounter.onCountUpdated.add( this.onHealthCount_Updated.bind( this, this._health ) );
        if ( this._cardInfo.health == 255 )
        {
            this._health.cTokenCounter.setEnabled( false );
        }
        // Text.
        this._healthText = new PIXI.Text( this.findTokenCounterText( this._cardInfo.health ), ServiceLocator.game.textStyler.normal );
        this._healthText.anchor.set( 0.5 );
        container.addChild( this._healthText );
        //
        container.position.set( 10, this._defense.cContainer.c.parent.y + this._defense.cContainer.c.parent.height * 0.5 + 10 + container.height * 0.5 );
        this._go.cContainer.c.addChild( container );

        // Wound.
        container = new PIXI.Container();
        // Counter.
        this._wound = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._wound.oid = this._go.oid + "_wound";
        this._wound.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "wound_token" );
        this._wound.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 50, this._wound.cSprite.s );
        this._wound.cTokenCounter.tokenName = "wound";
        this._wound.cTokenCounter.count = 0;
        this._wound.cTokenCounter.minCount = 0;
        this._wound.cTokenCounter.maxCount = 99;
        this._wound.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "wound_token" ).data );
        this._wound.cDropArea.target = this._wound.cTokenCounter;
        this._wound.init();
        container.addChild( this._wound.cContainer.c );
        this._wound.cTokenCounter.onCountUpdated.add( this.onWoundCount_Updated.bind( this, this._wound ) );
        // Text.
        this._woundText = new PIXI.Text( "0", ServiceLocator.game.textStyler.normal );
        this._woundText.anchor.set( 0.5 );
        container.addChild( this._woundText );
        //
        container.position.set( this._bg.cContainer.c.width * 0.5, this._bg.cContainer.c.height - container.height * 0.5 + 10 );
        this._go.cContainer.c.addChild( container );

        // Sphere.
        if ( this._cardInfo.sphere_code != "neutral" && this._cardInfo.sphere_code != "none" )
        {
            this._sphere = new GameObject( [ new CSprite(), new CTooltipReceptor() ] );
            this._sphere.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( this._cardInfo.sphere_code ).data );
            this._sphere.cSprite.s.anchor.set( 1 );
            Utils.game.limitSideSize( 40, this._sphere.cSprite.s );
            this._sphere.cTooltipReceptor.text = jQuery.i18n( this._cardInfo.sphere_code.toUpperCase() );
            this._sphere.init();
            this._sphere.cContainer.c.position.set( this._bg.cContainer.c.width + 10, this._bg.cContainer.c.height + 10 );
            this._go.cContainer.c.addChild( this._sphere.cContainer.c );
        }
    }

    public end(): void
    {
        this._onCommittedToQuest.removeAll();
        this._onUncommittedFromQuest.removeAll();

        this._willpower.end();
        this._willpower = null;

        this._attack.end();
        this._attack = null;

        this._defense.end();
        this._defense = null;

        this._health.end();
        this._health = null;

        this._wound.end();
        this._wound = null;

        if ( this._sphere )
        {
            this._sphere.end();
            this._sphere = null
        }

        if ( this._resource )
        {
            this._resource.end();
            this._resource = null;
        }

        super.end();
    }

    // overrides.
    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );
        
        if ( this._cardInfo.willpower != 255 )
        {
            this._willpower.cTokenCounter.setEnabled( isEnabled );
        }
        if ( this._cardInfo.attack != 255 )
        {
            this._attack.cTokenCounter.setEnabled( isEnabled );
        }
        if ( this._cardInfo.defense != 255 )
        {
            this._defense.cTokenCounter.setEnabled( isEnabled );
        }
        if ( this._cardInfo.health != 255 )
        {
            this._health.cTokenCounter.setEnabled( isEnabled );
        }
        this._wound.cTokenCounter.setEnabled( isEnabled );
    }

    public commitToQuest( actionScope: action_scope_type ): void
    {
        this._cardToken.cCardToken.setBowed( true, action_scope_type.LOCAL );
        this.addStatus( status_type.COMMITTED_TO_QUEST, action_scope_type.LOCAL );

        this._onCommittedToQuest.dispatch( this._go );

        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.COMMIT_TO_QUEST, null, [ this._go.oid ] );
        }
    }

    public uncommitFromQuest( actionScope: action_scope_type ): void
    {
        this._cardToken.cCardToken.setBowed( false, action_scope_type.LOCAL );
        this.removeStatus( status_type.COMMITTED_TO_QUEST, action_scope_type.LOCAL );
        
        this._onUncommittedFromQuest.dispatch( this._go );

        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.UNCOMMIT_FROM_QUEST, null, [ this._go.oid ] );
        }
    }

    public hasResourceCounter(): boolean
    {
        return this._resource != null;
    }

    public addResourceCounter(): void
    {
        // Resource.
        let container: PIXI.Container = new PIXI.Container();
        // Counter.
        this._resource = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._resource.oid = this._go.oid + "_resource";
        this._resource.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( "resource_token" );
        this._resource.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 50, this._resource.cSprite.s );
        this._resource.cTokenCounter.tokenName = "resource";
        this._resource.cTokenCounter.minCount = 0;
        this._resource.cTokenCounter.maxCount = 99;
        this._resource.cTokenCounter.count = 0;
        this._resource.cTokenCounter.draggedTexture = ServiceLocator.resourceStack.findAsTexture( "resource_token" );
        this._resource.cDropArea.target = this._resource.cTokenCounter;
        this._resource.init();
        container.addChild( this._resource.cContainer.c );
        this._resource.cTokenCounter.onCountUpdated.add( this.onResourceCount_Updated.bind( this, this._resource ) );
        // Text.
        this._resourceText = new PIXI.Text( "0", ServiceLocator.game.textStyler.normal );
        this._resourceText.anchor.set( 0.5 );
        container.addChild( this._resourceText );
        //
        container.position.set( this._bg.cContainer.c.width - container.width * 0.5 + 10, container.height * 0.5 - 10 );
        this._go.cContainer.c.addChild( container );
    }

    public removeResourceCounter(): void
    {
        this._resource.end();
        this._resource = null;
        
        this._go.cContainer.c.removeChild( this._resourceText.parent );
        this._resourceText = null;
    }

    // overrides.
    public onControlChanged( playerType: player_type ): void
    {
        this.setEnabled( playerType == player_type.PLAYER );
    }

    // overrides.
    public onHidden(): void
    {
        if ( this.hasStatus( status_type.COMMITTED_TO_QUEST ) )
        {
            this._cardToken.cCardToken.isAnimated = false;
            this.uncommitFromQuest( action_scope_type.LOCAL );
            this._cardToken.cCardToken.isAnimated = true;
        }
    }

    // virtual.
    public saveGame(): ISgCardTokenSide
    {
        let sgCardTokenSide: ISgCardTokenSide = super.saveGame();
        
        sgCardTokenSide.allySide = {
            willpower: this.findTokenCounterSaveValue( this._willpowerText, this._willpower ),
            attack: this.findTokenCounterSaveValue( this._attackText, this._attack ),
            defense: this.findTokenCounterSaveValue( this._defenseText, this._defense ),
            health: this.findTokenCounterSaveValue( this._healthText, this._health ),
            wound: this._wound.cTokenCounter.count };
        if ( this._resource )
        {
            sgCardTokenSide.allySide.resource = this._resource.cTokenCounter.count;
        }

        return sgCardTokenSide;
    }

    // overrides.
    public loadGame( sgCardTokenSide: ISgCardTokenSide, pass: number ): void
    {
        if ( pass == 0 )
        {
            super.loadGame( sgCardTokenSide, pass );

            if ( sgCardTokenSide.allySide.willpower != null )
            {
                this._willpower.cTokenCounter.setCount( sgCardTokenSide.allySide.willpower, action_scope_type.LOCAL );
            }
            if ( sgCardTokenSide.allySide.attack != null )
            {
                this._attack.cTokenCounter.setCount( sgCardTokenSide.allySide.attack, action_scope_type.LOCAL );
            }
            if ( sgCardTokenSide.allySide.defense != null )
            {
                this._defense.cTokenCounter.setCount( sgCardTokenSide.allySide.defense, action_scope_type.LOCAL );
            }
            if ( sgCardTokenSide.allySide.health != null )
            {
                this._health.cTokenCounter.setCount( sgCardTokenSide.allySide.health, action_scope_type.LOCAL );
            }
            if ( sgCardTokenSide.allySide.wound > 0 )
            {
                this._wound.cTokenCounter.setCount( sgCardTokenSide.allySide.wound, action_scope_type.LOCAL );
            }
        }
    }

    // protected:

    protected showRadialMenu(): void
    {
        let showAtPos: PIXI.Point = this._bg.cContainer.c.getGlobalPosition();
        showAtPos.x += this._bg.cContainer.c.width * 0.5;
        showAtPos.y += this._bg.cContainer.c.height * 0.5;

        let arrOption: Array<IOption> = [ 
            { id: option_type.TARGET, isEnabled: true }, 
            { id: option_type.PLAYER_ATTACK, isEnabled: true } ];
        if ( ServiceLocator.game.stateMachine.currentStateId == game_state_id.QUEST )
        {
            arrOption.push( { id: this.hasStatus( status_type.COMMITTED_TO_QUEST ) ? option_type.UNCOMMIT : option_type.COMMIT, isEnabled: true } );
        }
        //
        let arrExtrasOption: Array<IOption> = [
            { id: option_type.EQUIP, isEnabled: true },
            { id: option_type.UNDERNEATH, isEnabled: true },  
            { id: !this._highlightedFilter ? option_type.ADD_HIGHLIGHTING : option_type.REMOVE_HIGHLIGHTING, isEnabled: true },
            { id: option_type.ADD_COUNTER, isEnabled: !this._cardToken.cCardToken.cDetailBar.isMaxCustomCounter() } ];
        if ( !this._cardToken.cCardToken.cDetailBar.findItem( detail_bar_icon_type.CUSTOM_TEXT ) )
        {
            arrExtrasOption.push( { id: option_type.ADD_CUSTOM_TEXT, isEnabled: true } );
        }
        arrExtrasOption.push( { id: option_type.BACK, isEnabled: true } );
        //
        arrOption.push(
            { id: this._cardToken.cCardToken.isBowed ? option_type.READY : option_type.BOW, isEnabled: true },
            { id: this._cardToken.cCardToken.isFaceUp ? option_type.PUT_FACE_DOWN : option_type.PUT_FACE_UP, isEnabled: true },
            { id: option_type.DISCARD, isEnabled: true },
            { id: option_type.EXTRAS, isEnabled: true, subMenu: arrExtrasOption } ); 
        ServiceLocator.game.cRadialMenu.tryShow( this._go, showAtPos, arrOption );
    }

    // #endregion //


    // #region Other Callbacks //

    private onWillpowerCount_Updated( tokenCounter: GameObject, count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._willpowerText.text = count.toString();

        if ( this._cardInfo.willpower < 253 )
        {
            if ( count > this._cardInfo.willpower )
            {
                this._willpowerText.tint = 0x00ff00;
            }
            else if ( count < this._cardInfo.willpower )
            {
                this._willpowerText.tint = 0xff0000;
            }
            else
            {
                this._willpowerText.tint = 0xffffff;
            }
        }

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._go ), "player_will", delta, count, true );
        }
    }

    private onAttackCount_Updated( tokenCounter: GameObject, count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._attackText.text = count.toString();

        if ( this._cardInfo.attack < 253 )
        {
            if ( count > this._cardInfo.attack )
            {
                this._attackText.tint = 0x00ff00;
            }
            else if ( count < this._cardInfo.attack )
            {
                this._attackText.tint = 0xff0000;
            }
            else
            {
                this._attackText.tint = 0xffffff;
            }
        }

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._go ), "player_attack", delta, count, true );
        }
    }

    private onDefenseCount_Updated( tokenCounter: GameObject, count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._defenseText.text = count.toString();

        if ( this._cardInfo.defense < 253 )
        {
            if ( count > this._cardInfo.defense )
            {
                this._defenseText.tint = 0x00ff00;
            }
            else if ( count < this._cardInfo.defense )
            {
                this._defenseText.tint = 0xff0000;
            }
            else
            {
                this._defenseText.tint = 0xffffff;
            }
        }

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._go ), "player_defense", delta, count, true );
        }
    }

    private onHealthCount_Updated( tokenCounter: GameObject, count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._healthText.text = count.toString();

        if ( this._cardInfo.health < 253 )
        {
            if ( count > this._cardInfo.health )
            {
                this._healthText.tint = 0x00ff00;
            }
            else if ( count < this._cardInfo.health )
            {
                this._healthText.tint = 0xff0000;
            }
            else
            {
                this._healthText.tint = 0xffffff;
            }
        }

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( count <= this._wound.cTokenCounter.count )
        {
            this.addStatus( status_type.DEAD, action_scope_type.LOCAL );
        }
        else
        {
            this.removeStatus( status_type.DEAD, action_scope_type.LOCAL );
        }

        this.drawColouringLayer( this._wound.cTokenCounter.count, this._health.cTokenCounter.count, 0xff0000 );
    
        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._go ), "player_health", delta, count, true );
        }
    }

    private onWoundCount_Updated( tokenCounter: GameObject, count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._woundText.text = count.toString();

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( this.findTokenCounterSaveValue( this._healthText, this._health ) != null )
        {
            if ( count >= this._health.cTokenCounter.count )
            {
                this.addStatus( status_type.DEAD, action_scope_type.LOCAL );
            }
            else
            {
                this.removeStatus( status_type.DEAD, action_scope_type.LOCAL );
            }

            this.drawColouringLayer( this._wound.cTokenCounter.count, this._health.cTokenCounter.count, 0xff0000 );
        }
        
        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._go ), "wound_token", delta, count, true );
        }
    }

    private onResourceCount_Updated( tokenCounter: GameObject, count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._resourceText.text = count.toString();

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._go ), "resource_token", delta, count, true );
        }
    }

    // #endregion //
}