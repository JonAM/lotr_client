import CCardTokenSide from "../CCardTokenSide";

import { option_type, IOption } from "../../../ui/CRadialMenu";
import ServiceLocator from "../../../../../ServiceLocator";
import Utils from "../../../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../../../GameObject";
import CSprite from "../../../pixi/CSprite";


export default class CBackSide extends CCardTokenSide
{
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CBackSide";
    }

    // protected:

    // overrides.
    protected createBg(): GameObject
    {
        let result: GameObject = new GameObject( [ new CSprite() ] );
        result.cContainer.c.filters = [];
        result.cSprite.s.texture = Utils.img.createCardPortrait( this._cardId );
        Utils.game.limitSideSize( 180, result.cSprite.s ); 
        result.cContainer.c.interactive = true;
        result.cContainer.c.buttonMode = true;
        result.init();

        return result;
    }

    // overrides.
    protected showRadialMenu(): void
    {
        let showAtPos: PIXI.Point = this._bg.cContainer.c.getGlobalPosition();
        showAtPos.x += this._bg.cContainer.c.width * 0.5;
        showAtPos.y += this._bg.cContainer.c.height * 0.5;

        const kArrOption: Array<IOption> = [ 
            { id: option_type.PUT_FACE_UP, isEnabled: true },
            { id: option_type.DISCARD, isEnabled: true } ]; 
        ServiceLocator.game.cRadialMenu.tryShow( this._go, showAtPos, kArrOption );
    }

    // #endregion //
}