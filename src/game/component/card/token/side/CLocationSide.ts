import CCardTokenSide, { status_type } from "../CCardTokenSide";

import { location_type } from "../../../world/CGameWorld";
import { option_type, IOption } from "../../../ui/CRadialMenu";
import { player_type } from "../../../world/CPlayerArea";
import ServiceLocator from "../../../../../ServiceLocator";
import Session from "../../../../../Session";
import { detail_bar_icon_type } from "../CDetailBar";
import Utils from "../../../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../../../GameObject";
import CSprite from "../../../pixi/CSprite";
import CTokenCounter from "../../../ui/CTokenCounter";
import CDropArea from "../../../input/CDropArea";
import CDraggable from "../../../input/CDraggable";
import LogTargetCard from "../../../ui/right_menu/action_log/target/LogTargetCard";
import { ISgCardTokenSide } from "../../../../../view/game/SaveGameView";
import { action_scope_type } from "../../../../../service/socket_io/GameSocketIOController";
import { IScenario } from "../../../../ScenarioDB";
import CContainer from "../../../pixi/CContainer";
import CVictoryBtn from "../CVictoryBtn";


export default class CLocationSide extends CCardTokenSide
{
    // #region Attributes //

    // private:

	private _threat: GameObject = null;
    private _threatText: PIXI.Text = null;
	private _questPoints: GameObject = null;
	private _questPointsText: PIXI.Text = null;
	private _progress: GameObject = null;
	private _progressText: PIXI.Text = null;
    private _victoryBtn: GameObject = null;
    private _isFinal: boolean = null;
    
    // #endregion //


    // #region Properties //

    public get threat(): GameObject { return this._threat; }
    public get questPoints(): GameObject { return this._questPoints; }
    public get progress(): GameObject { return this._progress; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CLocationSide";
    }

    public init(): void
    {
        super.init();

        if ( this._cardInfo.quest_points == undefined )
        {
            this._cardInfo.quest_points = 0;
        }

        const kScenarioInfo: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
        this._isFinal = kScenarioInfo.win && kScenarioInfo.win.indexOf( this._cardId ) != -1;

        // Threat.
        let container: PIXI.Container = new PIXI.Container();
        // Counter.
        this._threat = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._threat.oid = this._go.oid + "_threat";
        this._threat.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "sauron_threat" ).data );
        this._threat.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 40, this._threat.cSprite.s );
        this._threat.cTokenCounter.tokenName = "threat";
        this._threat.cTokenCounter.minCount = -99;
        this._threat.cTokenCounter.maxCount = 99;
        this._threat.cTokenCounter.count = this.findTokenCounterValue( this._cardInfo.threat_strength );
        this._threat.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "sauron_threat" ).data );
        this._threat.cDropArea.target = this._threat.cTokenCounter;
        this._threat.init();
        container.addChild( this._threat.cContainer.c );
        this._threat.cTokenCounter.onCountUpdated.add( this.onThreatCount_Updated.bind( this, this._threat ) );
        if ( this._cardInfo.threat_strength == 255 )
        {
            this._threat.cTokenCounter.setEnabled( false );
        }
        // Text.
        this._threatText = new PIXI.Text( this.findTokenCounterText( this._cardInfo.threat_strength ), ServiceLocator.game.textStyler.normal );
        this._threatText.anchor.set( 1, 0.5 );
        this._threatText.x = -5;
        this._threatText.y = 1;
        container.addChild( this._threatText );
        //
        container.position.set( 10, container.height + 5 );
        this._go.cContainer.c.addChild( container );

        // Quest points.
        container = new PIXI.Container();
        // Counter.
        this._questPoints = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._questPoints.oid = this._go.oid + "_quest_points";
        this._questPoints.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "location_quest_points" ).data );
        this._questPoints.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 40, this._questPoints.cSprite.s );
        this._questPoints.cTokenCounter.tokenName = "quest_points";
        this._questPoints.cTokenCounter.minCount = 0;
        this._questPoints.cTokenCounter.maxCount = 99;
        this._questPoints.cTokenCounter.count = this.findTokenCounterValue( this._cardInfo.quest_points );
        this._questPoints.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "location_quest_points" ).data );
        this._questPoints.cDropArea.target = this._questPoints.cTokenCounter;
        this._questPoints.init();
        container.addChild( this._questPoints.cContainer.c );
        this._questPoints.cTokenCounter.onCountUpdated.add( this.onQuestPointCount_Updated.bind( this, this._questPoints ) );
        // Text.
        this._questPointsText = new PIXI.Text( this.findTokenCounterText( this._cardInfo.quest_points ), ServiceLocator.game.textStyler.normal );
        this._questPointsText.anchor.set( 0.5 );
        container.addChild( this._questPointsText );
        //
        if ( this._cardInfo.quest_points == 255 || this._cardInfo.quest_points == 0 )
        {
            this._questPoints.cTokenCounter.setEnabled( false );
            this._questPointsText.text = "-";
            this._colouringLayer.visible = false;
        }
        //
        container.position.set( 10, this._threat.cContainer.c.parent.y + this._threat.cContainer.c.parent.height * 2.5 + 20 + container.height * 0.5 );
        this._go.cContainer.c.addChild( container );

        // Progress.
        container = new PIXI.Container();
        // Counter.
        this._progress = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._progress.oid = this._go.oid + "_progress";
        this._progress.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "progress_token" ).data );
        this._progress.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 50, this._progress.cSprite.s );
        this._progress.cTokenCounter.tokenName = "progress";
        this._progress.cTokenCounter.minCount = 0;
        this._progress.cTokenCounter.maxCount = 99;
        this._progress.cTokenCounter.count = 0;
        this._progress.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "progress_token" ).data );
        this._progress.cDropArea.target = this._progress.cTokenCounter;
        this._progress.init();
        container.addChild( this._progress.cContainer.c );
        this._progress.cTokenCounter.onCountUpdated.add( this.onProgressCount_Updated.bind( this, this._progress ) );
        // Text.
        this._progressText = new PIXI.Text( "0", ServiceLocator.game.textStyler.normal );
        this._progressText.anchor.set( 0.5 );
        container.addChild( this._progressText );
        //
        container.position.set( this._bg.cContainer.c.width * 0.5, this._bg.cContainer.c.height - container.height * 0.5 + 10 );
        this._go.cContainer.c.addChild( container );

        // Victory button.
        if ( this._isFinal )
        {
            this._victoryBtn = new GameObject( [ new CContainer(), new CVictoryBtn() ] );
            this._victoryBtn.init();
            this._victoryBtn.cContainer.c.position.set( this._bg.cContainer.c.width - 15, 15 );
            this._go.cContainer.c.addChild( this._victoryBtn.cContainer.c );
            if ( this._cardInfo.quest_points == 0 || this._cardInfo.quest_points == 255 )
            {
                this._victoryBtn.cVictoryBtn.show();
            }
            else
            {
                this._victoryBtn.cVictoryBtn.hide();
            }
        }
    }

    public end(): void
    {
        this._threat.end();
        this._threat = null;

        this._questPoints.end();
        this._questPoints = null;

        this._progress.end();
        this._progress = null;

        if ( this._victoryBtn )
        {
            this._victoryBtn.end();
            this._victoryBtn = null;
        }

        super.end();
    }

    // overrides.
    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );
        
        if ( this._cardInfo.threat_strength != 255 )
        {
            this._threat.cTokenCounter.setEnabled( isEnabled );
        }
        if ( this._cardInfo.quest_points > 0 && this._cardInfo.quest_points < 255 )
        {
            this._questPoints.cTokenCounter.setEnabled( isEnabled );
        }
        this._progress.cTokenCounter.setEnabled( isEnabled );
    }

    // overrides.
    public saveGame(): ISgCardTokenSide
    {
        let sgCardToken: ISgCardTokenSide = super.saveGame();
        
        sgCardToken.locationSide = {
            threat: this.findTokenCounterSaveValue( this._threatText, this._threat ),
            questPoints: this.findTokenCounterSaveValue( this._questPointsText, this._questPoints ),
            progress: this._progress.cTokenCounter.count };

        return sgCardToken;
    }

    // overrides.
    public loadGame( sgCardTokenSide: ISgCardTokenSide, pass: number ): void
    {
        if ( pass == 0 )
        {
            super.loadGame( sgCardTokenSide, pass );

            if ( sgCardTokenSide.locationSide.threat != null )
            {
                this._threat.cTokenCounter.setCount( sgCardTokenSide.locationSide.threat, action_scope_type.LOCAL );
            }
            if ( sgCardTokenSide.locationSide.questPoints != null )
            {
                this._questPoints.cTokenCounter.setCount( sgCardTokenSide.locationSide.questPoints, action_scope_type.LOCAL );
            }
            if ( sgCardTokenSide.locationSide.progress > 0 )
            {
                this._progress.cTokenCounter.setCount( sgCardTokenSide.locationSide.progress, action_scope_type.LOCAL );
            }
        }
    }

    // protected:

    protected showRadialMenu(): void
    {
        let showAtPos: PIXI.Point = this._bg.cContainer.c.getGlobalPosition();
        showAtPos.x += this._bg.cContainer.c.width * 0.5;
        showAtPos.y += this._bg.cContainer.c.height * 0.5;

        let arrOption: Array<IOption> = [ { id: option_type.TARGET, isEnabled: true } ];
        if ( ( this._cardToken.cCardToken.location != location_type.ACTIVE_LOCATION
                && this._cardToken.cCardToken.location != location_type.ISLAND_LOCATION
                && this._cardToken.cCardToken.location != location_type.DOR_CUARTHOL_LOCATION
                && this._cardToken.cCardToken.location != location_type.ISOLATED_ACTIVE_LOCATION
                && this._cardToken.cCardToken.location != location_type.SPLIT_SAURON_ACTIVE_LOCATION )
            || this._cardToken.cCardToken.altSide.cLocationSide )
        {
            arrOption.push( { id: this._cardToken.cCardToken.isFaceUp ? option_type.PUT_FACE_DOWN : option_type.PUT_FACE_UP, isEnabled: true } );
        }
        //
        let arrExtrasOption: Array<IOption> = [
            { id: option_type.EQUIP, isEnabled: true },
            { id: option_type.UNDERNEATH, isEnabled: true },  
            { id: !this._highlightedFilter ? option_type.ADD_HIGHLIGHTING : option_type.REMOVE_HIGHLIGHTING, isEnabled: true },
            { id: option_type.ADD_COUNTER, isEnabled: !this._cardToken.cCardToken.cDetailBar.isMaxCustomCounter() } ];
        if ( !this._cardToken.cCardToken.cDetailBar.findItem( detail_bar_icon_type.CUSTOM_TEXT ) )
        {
            arrExtrasOption.push( { id: option_type.ADD_CUSTOM_TEXT, isEnabled: true } );
        }
        arrExtrasOption.push( { id: option_type.BACK, isEnabled: true } );
        //
        arrOption.push(
            { id: option_type.DISCARD, isEnabled: true }, 
            { id: option_type.EXTRAS, isEnabled: true, subMenu: arrExtrasOption } ); 
        ServiceLocator.game.cRadialMenu.tryShow( this._go, showAtPos, arrOption );
    }

    // #endregion //


    // #region Other Callbacks //

    private onThreatCount_Updated( tokenCounter: GameObject, count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._threatText.text = count.toString();

        if ( this._cardInfo.threat_strength < 253 )
        {
            if ( count > this._cardInfo.threat_strength )
            {
                this._threatText.tint = 0x00ff00;
            }
            else if ( count < this._cardInfo.threat_strength )
            {
                this._threatText.tint = 0xff0000;
            }
            else
            {
                this._threatText.tint = 0xffffff;
            }
        }

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._go ), "sauron_threat", delta, count, true );
        }
    }

    private onQuestPointCount_Updated( tokenCounter: GameObject, count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._questPointsText.text = count.toString();

        if ( this._cardInfo.quest_points < 253 )
        {
            if ( count > this._cardInfo.quest_points )
            {
                this._questPointsText.tint = 0x00ff00;
            }
            else if ( count < this._cardInfo.quest_points )
            {
                this._questPointsText.tint = 0xff0000;
            }
            else
            {
                this._questPointsText.tint = 0xffffff;
            }
        }

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( count <= this._progress.cTokenCounter.count )
        {
            this.addStatus( status_type.EXPLORED, action_scope_type.LOCAL );
            if ( this._isFinal )
            {
                this._victoryBtn.cVictoryBtn.show();
            }
        }
        else
        {
            this.removeStatus( status_type.EXPLORED, action_scope_type.LOCAL );
            if ( this._isFinal )
            {
                this._victoryBtn.cVictoryBtn.hide();
            }
        }

        this.drawColouringLayer( this._progress.cTokenCounter.count, this._questPoints.cTokenCounter.count, 0x00ff00 );

        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._go ), "location_quest_points", delta, count, true );
        }
    }

    private onProgressCount_Updated( tokenCounter: GameObject, count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._progressText.text = count.toString();

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( this.findTokenCounterSaveValue( this._questPointsText, this._questPoints ) != null )
        {
            if ( count >= this._questPoints.cTokenCounter.count )
            {
                this.addStatus( status_type.EXPLORED, action_scope_type.LOCAL );
                if ( this._isFinal )
                {
                    this._victoryBtn.cVictoryBtn.show();
                }
            }
            else
            {
                this.removeStatus( status_type.EXPLORED, action_scope_type.LOCAL );
                if ( this._isFinal )
                {
                    this._victoryBtn.cVictoryBtn.hide();
                }
            }

            if ( this._cardInfo.quest_points > 0 )
            {
                this.drawColouringLayer( this._progress.cTokenCounter.count, this._questPoints.cTokenCounter.count, 0x00ff00 );
            }
        }

        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._go ), "progress_token", delta, count, true );
        }
    }

    // #endregion //
}