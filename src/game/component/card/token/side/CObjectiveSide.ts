import CCardTokenSide from "../CCardTokenSide";

import { option_type, IOption } from "../../../ui/CRadialMenu";
import ServiceLocator from "../../../../../ServiceLocator";
import { detail_bar_icon_type } from "../CDetailBar";
import * as PIXI from "pixi.js";
import GameObject from "../../../../GameObject";
import Session from "../../../../../Session";
import { IScenario } from "../../../../ScenarioDB";
import CContainer from "../../../pixi/CContainer";
import CVictoryBtn from "../CVictoryBtn";


export default class CObjectiveSide extends CCardTokenSide
{
    // #region Attributes //

    // private:

    private _victoryBtn: GameObject = null;
    private _isFinal: boolean = null;
    
    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CObjectiveSide";
    }

    public init(): void
    {
        super.init();

        const kScenarioInfo: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
        this._isFinal = kScenarioInfo.win && kScenarioInfo.win.indexOf( this._cardId ) != -1;

        // Victory button.
        if ( this._isFinal )
        {
            this._victoryBtn = new GameObject( [ new CContainer(), new CVictoryBtn() ] );
            this._victoryBtn.init();
            this._victoryBtn.cContainer.c.position.set( this._bg.cContainer.c.width - 15, 15 );
            this._go.cContainer.c.addChild( this._victoryBtn.cContainer.c );
            this._victoryBtn.cVictoryBtn.show();
        }
    }

    public end(): void
    {
        if ( this._victoryBtn )
        {
            this._victoryBtn.end();
            this._victoryBtn = null;
        }

        super.end();
    }

    // protected:

    protected showRadialMenu(): void
    {
        let showAtPos: PIXI.Point = this._bg.cContainer.c.getGlobalPosition();
        showAtPos.x += this._bg.cContainer.c.width * 0.5;
        showAtPos.y += this._bg.cContainer.c.height * 0.5;

        let arrExtrasOption: Array<IOption> = [
            { id: option_type.UNDERNEATH, isEnabled: true },  
            { id: option_type.ADD_COUNTER, isEnabled: !this._cardToken.cCardToken.cDetailBar.isMaxCustomCounter() },
            { id: !this._highlightedFilter ? option_type.ADD_HIGHLIGHTING : option_type.REMOVE_HIGHLIGHTING, isEnabled: true } ];
        if ( !this._cardToken.cCardToken.cDetailBar.findItem( detail_bar_icon_type.CUSTOM_TEXT ) )
        {
            arrExtrasOption.push( { id: option_type.ADD_CUSTOM_TEXT, isEnabled: true } );
        }
        arrExtrasOption.push( { id: option_type.BACK, isEnabled: true } );
        //
        let arrOption: Array<IOption> = [ 
            { id: option_type.TARGET, isEnabled: true },
            { id: option_type.EQUIP, isEnabled: true }, 
            { id: this._cardToken.cCardToken.isFaceUp ? option_type.PUT_FACE_DOWN : option_type.PUT_FACE_UP, isEnabled: true }, 
            { id: option_type.DISCARD, isEnabled: true }, 
            { id: option_type.EXTRAS, isEnabled: true, subMenu: arrExtrasOption } ]; 
        ServiceLocator.game.cRadialMenu.tryShow( this._go, showAtPos, arrOption );
    }

    // #endregion //
}