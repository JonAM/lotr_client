import CCardTokenSide, { status_type } from "../CCardTokenSide";

import ServiceLocator from "../../../../../ServiceLocator";
import * as PIXI from "pixi.js";

import { IOption, option_type } from "../../../ui/CRadialMenu";
import { detail_bar_icon_type } from "../CDetailBar";


export default class CQuestIntroSide extends CCardTokenSide
{
    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CQuestIntroSide";
    }

    // overrides.
    protected showRadialMenu(): void
    {
        let showAtPos: PIXI.Point = this._bg.cContainer.c.getGlobalPosition();
        showAtPos.x += this._bg.cContainer.c.width * 0.5;
        showAtPos.y += this._bg.cContainer.c.height * 0.5;

        let arrExtrasOption: Array<IOption> = [
            { id: option_type.EQUIP, isEnabled: true },
            { id: option_type.UNDERNEATH, isEnabled: true },  
            { id: !this._highlightedFilter ? option_type.ADD_HIGHLIGHTING : option_type.REMOVE_HIGHLIGHTING, isEnabled: true },
            { id: option_type.ADD_COUNTER, isEnabled: !this._cardToken.cCardToken.cDetailBar.isMaxCustomCounter() } ];
        if ( !this._cardToken.cCardToken.cDetailBar.findItem( detail_bar_icon_type.CUSTOM_TEXT ) )
        {
            arrExtrasOption.push( { id: option_type.ADD_CUSTOM_TEXT, isEnabled: true } );
        }
        arrExtrasOption.push( { id: option_type.BACK, isEnabled: true } );
        //
        const kArrOption: Array<IOption> = [ 
            { id: option_type.PUT_FACE_UP, isEnabled: true },
            { id: option_type.DISCARD, isEnabled: true },
            { id: option_type.EXTRAS, isEnabled: true, subMenu: arrExtrasOption } ]; 
        ServiceLocator.game.cRadialMenu.tryShow( this._go, showAtPos, kArrOption );
    }

    // #endregion //
}