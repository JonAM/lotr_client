import CCardTokenSide, { status_type } from "../CCardTokenSide";

import { action_scope_type, player_action_type } from "../../../../../service/socket_io/GameSocketIOController";
import { detail_bar_icon_type } from "../CDetailBar";
import { layer_type } from "../../../world/CGameLayerProvider";
import { option_type, IOption } from "../../../ui/CRadialMenu";
import { player_type } from "../../../world/CPlayerArea";
import ServiceLocator from "../../../../../ServiceLocator";
import { target_selection_type } from "../../../input/CTargetSelector";
import Utils from "../../../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../../../GameObject";
import CSprite from "../../../pixi/CSprite";
import CTokenCounter from "../../../ui/CTokenCounter";
import CDropArea from "../../../input/CDropArea";
import CDraggable from "../../../input/CDraggable";
import CContainer from "../../../pixi/CContainer";
import LogTargetCard from "../../../ui/right_menu/action_log/target/LogTargetCard";
import { ISgCard, ISgCardTokenSide } from "../../../../../view/game/SaveGameView";
import CShadowCardMini from "../../CShadowCardMini";
import CButton from "../../../input/CButton";
import CShareableGameElement from "../../../CShareableGameElement";
import ICharacterTokenSide from "../../ICharacterTokenSide";
import Session from "../../../../../Session";
import { IScenario } from "../../../../ScenarioDB";
import CVictoryBtn from "../CVictoryBtn";


export default class CEnemySide extends CCardTokenSide implements ICharacterTokenSide
{
    // #region Attributes //

    // private:

    private _engagementCost: GameObject = null;
	private _engagementCostText: PIXI.Text = null;
	private _threat: GameObject = null;
    private _threatText: PIXI.Text = null;
	private _attack: GameObject = null;
    private _attackText: PIXI.Text = null;
	private _defense: GameObject = null;
	private _defenseText: PIXI.Text = null;
	private _health: GameObject = null;
	private _healthText: PIXI.Text = null;
	private _wound: GameObject = null;
	private _woundText: PIXI.Text = null;
    private _shadowCardContainer: PIXI.Container = null;
    private _victoryBtn: GameObject = null;
    private _isFinal: boolean = null;
    
    // #endregion //


    // #region Properties //

    public get engagementCost(): GameObject { return this._engagementCost; }
    public get threat(): GameObject { return this._threat; }
    public get attack(): GameObject { return this._attack; }
    public get defense(): GameObject { return this._defense; }
    public get health(): GameObject { return this._health; }
    public get wound(): GameObject { return this._wound; }
    public get shadowCardContainer(): PIXI.Container { return this._shadowCardContainer; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id += "_CEnemySide";
    }

    public init(): void
    {
        super.init();

        const kScenarioInfo: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
        this._isFinal = kScenarioInfo.win && kScenarioInfo.win.indexOf( this._cardId ) != -1;

        // Engagement cost.
        let container: PIXI.Container = new PIXI.Container();
        // Counter.
        this._engagementCost = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._engagementCost.oid = this._go.oid + "_engagement_cost";
        this._engagementCost.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "engagement_cost" ).data );
        this._engagementCost.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 50, this._engagementCost.cSprite.s );
        this._engagementCost.cTokenCounter.tokenName = "engagement_cost";
        this._engagementCost.cTokenCounter.minCount = 0;
        this._engagementCost.cTokenCounter.maxCount = 99;
        this._engagementCost.cTokenCounter.count = parseInt( this._cardInfo.engagement_cost );
        this._engagementCost.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "engagement_cost" ).data );
        this._engagementCost.cDropArea.target = this._engagementCost.cTokenCounter;
        this._engagementCost.init();
        container.addChild( this._engagementCost.cContainer.c );
        this._engagementCost.cTokenCounter.onCountUpdated.add( this.onEngagementCostCount_Updated.bind( this, this._engagementCost ) );
        // Text.
        this._engagementCostText = new PIXI.Text( this._cardInfo.engagement_cost, ServiceLocator.game.textStyler.normal );
        this._engagementCostText.anchor.set( 0.5 );
        container.addChild( this._engagementCostText );
        //
        container.position.set( this._bg.cContainer.c.width - container.width * 0.5 + 10, container.height * 0.5 - 10 );
        this._go.cContainer.c.addChild( container );

        // Threat.
        container = new PIXI.Container(); 
        // Counter.
        this._threat = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._threat.oid = this._go.oid + "_threat";
        this._threat.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "sauron_threat" ).data );
        this._threat.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 40, this._threat.cSprite.s );
        this._threat.cTokenCounter.tokenName = "threat";
        this._threat.cTokenCounter.minCount = 0;
        this._threat.cTokenCounter.maxCount = 99;
        this._threat.cTokenCounter.count = this.findTokenCounterValue( this._cardInfo.threat_strength );
        this._threat.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "sauron_threat" ).data );
        this._threat.cDropArea.target = this._threat.cTokenCounter;
        this._threat.init();
        container.addChild( this._threat.cContainer.c );
        this._threat.cTokenCounter.onCountUpdated.add( this.onThreatCount_Updated.bind( this, this._threat ) );
        if ( this._cardInfo.threat_strength == 255 )
        {
            this._threat.cTokenCounter.setEnabled( false );
        }
        // Text.
        this._threatText = new PIXI.Text( this.findTokenCounterText( this._cardInfo.threat_strength ), ServiceLocator.game.textStyler.normal );
        this._threatText.anchor.set( 1, 0.5 );
        this._threatText.x = -5;
        this._threatText.y = 1;
        container.addChild( this._threatText );
        //
        container.position.set( 10, container.height + 5 );
        this._go.cContainer.c.addChild( container );

        // Attack.
        container = new PIXI.Container(); 
        // Counter.
        this._attack = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._attack.oid = this._go.oid + "_attack";
        this._attack.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "sauron_attack" ).data );
        this._attack.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 40, this._attack.cSprite.s );
        this._attack.cTokenCounter.tokenName = "attack";
        this._attack.cTokenCounter.minCount = -99;
        this._attack.cTokenCounter.maxCount = 99;
        this._attack.cTokenCounter.count = this.findTokenCounterValue( this._cardInfo.attack );
        this._attack.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "sauron_attack" ).data );
        this._attack.cDropArea.target = this._attack.cTokenCounter;
        this._attack.init();
        container.addChild( this._attack.cContainer.c );
        this._attack.cTokenCounter.onCountUpdated.add( this.onAttackCount_Updated.bind( this, this._attack ) );
        if ( this._cardInfo.attack == 255 )
        {
            this._attack.cTokenCounter.setEnabled( false );
        }
        // Text.
        this._attackText = new PIXI.Text( this.findTokenCounterText( this._cardInfo.attack ), ServiceLocator.game.textStyler.normal );
        this._attackText.anchor.set( 1, 0.5 );
        this._attackText.x = -5;
        this._attackText.y = 1;
        container.addChild( this._attackText );
        //
        container.position.set( 10, this._threat.cContainer.c.parent.y + this._threat.cContainer.c.parent.height + 5 );
        this._go.cContainer.c.addChild( container );

        // Defense.
        container = new PIXI.Container(); 
        // Counter.
        this._defense = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._defense.oid = this._go.oid + "_defense";
        this._defense.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "sauron_defense" ).data );
        this._defense.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 40, this._defense.cSprite.s );
        this._defense.cTokenCounter.tokenName = "defense";
        this._defense.cTokenCounter.minCount = -99;
        this._defense.cTokenCounter.maxCount = 99;
        this._defense.cTokenCounter.count = this.findTokenCounterValue( this._cardInfo.defense );
        this._defense.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "sauron_defense" ).data );
        this._defense.cDropArea.target = this._defense.cTokenCounter;
        this._defense.init();
        container.addChild( this._defense.cContainer.c );
        this._defense.cTokenCounter.onCountUpdated.add( this.onDefenseCount_Updated.bind( this, this._defense ) );
        if ( this._cardInfo.defense == 255 )
        {
            this._defense.cTokenCounter.setEnabled( false );
        }
        // Text.
        this._defenseText = new PIXI.Text( this.findTokenCounterText( this._cardInfo.defense ), ServiceLocator.game.textStyler.normal );
        this._defenseText.anchor.set( 1, 0.5 );
        this._defenseText.x = -5;
        this._defenseText.y = 1;
        container.addChild( this._defenseText );
        //
        container.position.set( 10, this._attack.cContainer.c.parent.y + this._attack.cContainer.c.parent.height + 5 );
        this._go.cContainer.c.addChild( container );

        // Health.
        container = new PIXI.Container();
        // Counter.
        this._health = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._health.oid = this._go.oid + "_health";
        this._health.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "sauron_health" ).data );
        this._health.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 40, this._health.cSprite.s );
        this._health.cTokenCounter.tokenName = "health";
        this._health.cTokenCounter.minCount = 0;
        this._health.cTokenCounter.maxCount = 99;
        this._health.cTokenCounter.count = this.findTokenCounterValue( this._cardInfo.health ); 
        this._health.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "sauron_health" ).data );
        this._health.cDropArea.target = this._health.cTokenCounter;
        this._health.init();
        container.addChild( this._health.cContainer.c );
        this._health.cTokenCounter.onCountUpdated.add( this.onHealthCount_Updated.bind( this, this._health ) );
        if ( this._cardInfo.health == 255 )
        {
            this._health.cTokenCounter.setEnabled( false );
        }
        // Text.
        this._healthText = new PIXI.Text( this.findTokenCounterText( this._cardInfo.health ), ServiceLocator.game.textStyler.normal );
        this._healthText.anchor.set( 0.5 );
        container.addChild( this._healthText );
        //
        container.position.set( 10, this._defense.cContainer.c.parent.y + this._defense.cContainer.c.parent.height * 0.5 + 10 + container.height * 0.5 );
        this._go.cContainer.c.addChild( container );

        // Wound.
        container = new PIXI.Container();
        // Counter.
        this._wound = new GameObject( [ new CSprite(), new CTokenCounter(), new CDropArea(), new CDraggable() ] );
        this._wound.oid = this._go.oid + "_wound";
        this._wound.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "wound_token" ).data );
        this._wound.cSprite.s.anchor.set( 0.5 );
        Utils.game.limitSideSize( 50, this._wound.cSprite.s );
        this._wound.cTokenCounter.tokenName = "wound";
        this._wound.cTokenCounter.minCount = 0;
        this._wound.cTokenCounter.maxCount = 99;
        this._wound.cTokenCounter.count = 0;
        this._wound.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "wound_token" ).data );
        this._wound.cDropArea.target = this._wound.cTokenCounter;
        this._wound.init();
        container.addChild( this._wound.cContainer.c );
        this._wound.cTokenCounter.onCountUpdated.add( this.onWoundCount_Updated.bind( this, this._wound ) );
        // Text.
        this._woundText = new PIXI.Text( "0", ServiceLocator.game.textStyler.normal );
        this._woundText.anchor.set( 0.5 );
        container.addChild( this._woundText );
        //
        container.position.set( this._bg.cContainer.c.width * 0.5, this._bg.cContainer.c.height - container.height * 0.5 + 10 );
        this._go.cContainer.c.addChild( container );

        // Shadow card container.
        this._shadowCardContainer = new PIXI.Container();
        this._shadowCardContainer.position.set( this._bg.cContainer.c.width - 10, this._health.cContainer.c.parent.y );
        this._go.cContainer.c.addChild( this._shadowCardContainer );

        // Victory button.
        if ( this._isFinal )
        {
            this._victoryBtn = new GameObject( [ new CContainer(), new CVictoryBtn() ] );
            this._victoryBtn.init();
            this._victoryBtn.cContainer.c.position.set( this._bg.cContainer.c.width - 15, 15 );
            this._go.cContainer.c.addChild( this._victoryBtn.cContainer.c );
            this._victoryBtn.cVictoryBtn.hide();
        }
    }

    public end(): void
    {
        this._engagementCost.end();
        this._engagementCost = null;

        this._threat.end();
        this._threat = null;

        this._attack.end();
        this._attack = null;

        this._defense.end();
        this._defense = null;

        this._health.end();
        this._health = null;

        this._wound.end();
        this._wound = null;

        for ( let displayObject of this._shadowCardContainer.children )
        {
            ( ( displayObject as any ).go as GameObject ).cShadowCardMini.end();
        }

        if ( this._victoryBtn )
        {
            this._victoryBtn.end();
            this._victoryBtn = null;
        }

        super.end();
    }

    // overrides.
    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );
        
        this._engagementCost.cTokenCounter.setEnabled( isEnabled );
        if ( this._cardInfo.threat_strength != 255 )
        {
            this._threat.cTokenCounter.setEnabled( isEnabled );
        }
        if ( this._cardInfo.attack != 255 )
        {
            this._attack.cTokenCounter.setEnabled( isEnabled );
        }
        if ( this._cardInfo.defense != 255 )
        {
            this._defense.cTokenCounter.setEnabled( isEnabled );
        }
        if ( this._cardInfo.health != 255 )
        {
            this._health.cTokenCounter.setEnabled( isEnabled );
        }
        this._wound.cTokenCounter.setEnabled( isEnabled );

        for ( let displayObject of this._shadowCardContainer.children )
        {
            ( ( displayObject as any ).go as GameObject ).cShadowCardMini.setEnabled( isEnabled );
        }
    }

    public addShadowCard( card: GameObject, actionScope: action_scope_type ): void
    {
        let shadowCardMini: GameObject = new GameObject( [ new CContainer(), new CShadowCardMini(), new CDraggable(), new CButton(), new CShareableGameElement() ] );
        shadowCardMini.oid = this._go.oid + "_sc_" + card.oid;
        shadowCardMini.cShadowCardMini.card = card;
        shadowCardMini.cDraggable.dragShadowTextureCreator = shadowCardMini.cShadowCardMini;
        shadowCardMini.init();
        shadowCardMini.cContainer.c.y = -shadowCardMini.cContainer.c.height * this._shadowCardContainer.children.length;
        shadowCardMini.cShadowCardMini.setEnabled( this._cardToken.cCardToken.controllerPlayer == player_type.PLAYER );
        this._shadowCardContainer.addChild( shadowCardMini.cContainer.c );
        shadowCardMini.cShadowCardMini.onDiscarded.addOnce( this.onShadowCard_Discarded, this );
        shadowCardMini.cDraggable.onDropped.addOnce( this.onShadowCardMini_Dropped, this );

        if ( ServiceLocator.game.isAnimated && this._cardToken.cCardToken.isAnimated )
        {
            // Vfx.
            const kVfxLayer: GameObject = Utils.game.findGameProviderLayer( this._go, layer_type.VFX );
            const kVfxPos: PIXI.Point = new PIXI.Point().copyFrom( kVfxLayer.cContainer.c.toLocal( shadowCardMini.cContainer.c.getGlobalPosition() ) );
            Utils.anim.flashDiamond( kVfxPos, 40, 2, kVfxLayer );
        }
        
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ADD_SHADOW_CARD, null, [ this._cardToken.oid, card.oid ] );
        }
    }

    public addShadowCardMini( shadowCardMini: GameObject ): void
    {
        shadowCardMini.cContainer.c.y = -shadowCardMini.cContainer.c.height * this._shadowCardContainer.children.length;
        shadowCardMini.cShadowCardMini.setEnabled( this._cardToken.cCardToken.controllerPlayer == player_type.PLAYER );
        this._shadowCardContainer.addChild( shadowCardMini.cContainer.c );
        shadowCardMini.cShadowCardMini.onDiscarded.addOnce( this.onShadowCard_Discarded, this );
        shadowCardMini.cDraggable.onDropped.addOnce( this.onShadowCardMini_Dropped, this );

        if ( ServiceLocator.game.isAnimated && this._cardToken.cCardToken.isAnimated )
        {
            // Vfx.
            const kVfxLayer: GameObject = Utils.game.findGameProviderLayer( this._go, layer_type.VFX );
            const kVfxPos: PIXI.Point = new PIXI.Point().copyFrom( kVfxLayer.cContainer.c.toLocal( shadowCardMini.cContainer.c.getGlobalPosition() ) );
            Utils.anim.flashDiamond( kVfxPos, 40, 2, kVfxLayer );
        }
    }

    public discardShadowCards( actionScope: action_scope_type ): void
    {
        for ( let i: number = this._shadowCardContainer.children.length - 1; i >= 0; --i )
        {
            ( ( this._shadowCardContainer.getChildAt( i ) as any ).go as GameObject ).cShadowCardMini.discard( actionScope );
        }
    }

    // overrides.
    public onDiscarded( actionScope: action_scope_type ): void
    {
        this.discardShadowCards( actionScope );
    }

    // virtual.
    public saveGame(): ISgCardTokenSide
    {
        let sgCardTokenSide: ISgCardTokenSide = super.saveGame();

        let arrShadowCard: Array<ISgCard> = new Array<ISgCard>();
        for ( let i: number = this._shadowCardContainer.children.length - 1; i >= 0; --i )
        {
            arrShadowCard.push( ( ( this._shadowCardContainer.getChildAt( i ) as any ).go as GameObject ).cShadowCardMini.saveGame() );
        }
        
        sgCardTokenSide.enemySide = {
            engagementCost: this._engagementCost.cTokenCounter.count,
            threat: this.findTokenCounterSaveValue( this._threatText, this._threat ),
            attack: this.findTokenCounterSaveValue( this._attackText, this._attack ),
            defense: this.findTokenCounterSaveValue( this._defenseText, this._defense ),
            health: this.findTokenCounterSaveValue( this._healthText, this._health ),
            wound: this._wound.cTokenCounter.count,
            shadowCards: arrShadowCard };

        return sgCardTokenSide;
    }

    // overrides.
    public loadGame( sgCardTokenSide: ISgCardTokenSide, pass: number ): void
    {
        if ( pass == 0 )
        {
            super.loadGame( sgCardTokenSide, pass );

            let shadowCardCount: number = 0;
            for ( let sgShadowCard of sgCardTokenSide.enemySide.shadowCards )
            {
                let card: GameObject = GameObject.find( sgShadowCard.oid );
                card.cCard.loadGame( sgShadowCard, pass );
                this.addShadowCard( card, action_scope_type.LOCAL );
                ( ( this._shadowCardContainer.getChildAt( shadowCardCount ) as any ).go as GameObject ).cShadowCardMini.loadGame( sgShadowCard, pass );

                shadowCardCount += 1;
            }

            this._engagementCost.cTokenCounter.setCount( sgCardTokenSide.enemySide.engagementCost, action_scope_type.LOCAL );
            if ( sgCardTokenSide.enemySide.threat != null )
            {
                this._threat.cTokenCounter.setCount( sgCardTokenSide.enemySide.threat, action_scope_type.LOCAL );
            }
            if ( sgCardTokenSide.enemySide.attack != null )
            {
                this._attack.cTokenCounter.setCount( sgCardTokenSide.enemySide.attack, action_scope_type.LOCAL );
            }
            if ( sgCardTokenSide.enemySide.defense != null )
            {
                this._defense.cTokenCounter.setCount( sgCardTokenSide.enemySide.defense, action_scope_type.LOCAL );
            }
            if ( sgCardTokenSide.enemySide.health != null )
            {
                this._health.cTokenCounter.setCount( sgCardTokenSide.enemySide.health, action_scope_type.LOCAL );
            }
            if ( sgCardTokenSide.enemySide.wound > 0 )
            {
                this._wound.cTokenCounter.setCount( sgCardTokenSide.enemySide.wound, action_scope_type.LOCAL );
            }
        }
    }

    // protected:

    protected showRadialMenu(): void
    {
        let showAtPos: PIXI.Point = this._bg.cContainer.c.getGlobalPosition();
        showAtPos.x += this._bg.cContainer.c.width * 0.5;
        showAtPos.y += this._bg.cContainer.c.height * 0.5;

        let arrExtrasOption: Array<IOption> = [
            { id: option_type.EQUIP, isEnabled: this._cardId != "999001" },
            { id: option_type.UNDERNEATH, isEnabled: this._cardId != "999001" },  
            { id: !this._highlightedFilter ? option_type.ADD_HIGHLIGHTING : option_type.REMOVE_HIGHLIGHTING, isEnabled: true },
            { id: option_type.ADD_COUNTER, isEnabled: !this._cardToken.cCardToken.cDetailBar.isMaxCustomCounter() } ];
        if ( !this._cardToken.cCardToken.cDetailBar.findItem( detail_bar_icon_type.CUSTOM_TEXT ) )
        {
            arrExtrasOption.push( { id: option_type.ADD_CUSTOM_TEXT, isEnabled: true } );
        }
        arrExtrasOption.push( { id: option_type.BACK, isEnabled: true } );
        //
        let arrOption: Array<IOption> = [ 
            { id: option_type.TARGET, isEnabled: true },
            { id: option_type.SAURON_ATTACK, isEnabled: true }, 
            { id: this._cardToken.cCardToken.isFaceUp ? option_type.PUT_FACE_DOWN : option_type.PUT_FACE_UP, isEnabled: true }, 
            { id: option_type.DISCARD, isEnabled: true }, 
            { id: option_type.EXTRAS, isEnabled: true, subMenu: arrExtrasOption } ]; 
        ServiceLocator.game.cRadialMenu.tryShow( this._go, showAtPos, arrOption );
    }

    // #endregion //


    // #region Other Callbacks //

    private onEngagementCostCount_Updated( tokenCounter: GameObject, count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._engagementCostText.text = count.toString();

        const kEngagementCost: number = parseInt( this._cardInfo.engagement_cost );
        if ( !Number.isNaN( kEngagementCost ) )
        {
            if ( kEngagementCost < 253 )
            {
                if ( count > kEngagementCost )
                {
                    this._engagementCostText.tint = 0x00ff00;
                }
                else if ( count < kEngagementCost )
                {
                    this._engagementCostText.tint = 0xff0000;
                }
                else
                {
                    this._engagementCostText.tint = 0xffffff;
                }
            }
        }

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._go ), "engagement_cost", delta, count, true );
        }
    }

    private onThreatCount_Updated( tokenCounter: GameObject, count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._threatText.text = count.toString();

        if ( this._cardInfo.threat_strength < 253 )
        {
            if ( count > this._cardInfo.threat_strength )
            {
                this._threatText.tint = 0x00ff00;
            }
            else if ( count < this._cardInfo.threat_strength )
            {
                this._threatText.tint = 0xff0000;
            }
            else
            {
                this._threatText.tint = 0xffffff;
            }
        }

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._go ), "sauron_threat", delta, count, true );
        }
    }

    private onAttackCount_Updated( tokenCounter: GameObject, count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._attackText.text = count.toString();

        if ( this._cardInfo.attack < 253 )
        {
            if ( count > this._cardInfo.attack )
            {
                this._attackText.tint = 0x00ff00;
            }
            else if ( count < this._cardInfo.attack )
            {
                this._attackText.tint = 0xff0000;
            }
            else
            {
                this._attackText.tint = 0xffffff;
            }
        }

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._go ), "sauron_attack", delta, count, true );
        }
    }

    private onDefenseCount_Updated( tokenCounter: GameObject, count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._defenseText.text = count.toString();

        if ( this._cardInfo.defense < 253 )
        {
            if ( count > this._cardInfo.defense )
            {
                this._defenseText.tint = 0x00ff00;
            }
            else if ( count < this._cardInfo.defense )
            {
                this._defenseText.tint = 0xff0000;
            }
            else
            {
                this._defenseText.tint = 0xffffff;
            }
        }

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._go ), "sauron_defense", delta, count, true );
        }
    }

    private onHealthCount_Updated( tokenCounter: GameObject, count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._healthText.text = count.toString();

        if ( this._cardInfo.health < 253 )
        {
            if ( count > this._cardInfo.health )
            {
                this._healthText.tint = 0x00ff00;
            }
            else if ( count < this._cardInfo.health )
            {
                this._healthText.tint = 0xff0000;
            }
            else
            {
                this._healthText.tint = 0xffffff;
            }
        }

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( count <= this._wound.cTokenCounter.count )
        {
            this.addStatus( status_type.DEAD, action_scope_type.LOCAL );
            if ( this._isFinal )
            {
                this._victoryBtn.cVictoryBtn.show();
            }
        }
        else
        {
            this.removeStatus( status_type.DEAD, action_scope_type.LOCAL );
            if ( this._isFinal )
            {
                this._victoryBtn.cVictoryBtn.hide();
            }
        }

        this.drawColouringLayer( this._wound.cTokenCounter.count, this._health.cTokenCounter.count, 0xff0000 );

        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._go ), "sauron_health", delta, count, true );
        }
    }

    private onWoundCount_Updated( tokenCounter: GameObject, count: number, delta: number, isPlayerInput: boolean ): void
    {
        this._woundText.text = count.toString();

        CTokenCounter.animate( tokenCounter.cContainer.c.parent, delta );

        if ( this.findTokenCounterSaveValue( this._healthText, this._health ) != null )
        {
            if ( count >= this._health.cTokenCounter.count )
            {
                this.addStatus( status_type.DEAD, action_scope_type.LOCAL);
                if ( this._isFinal )
                {
                    this._victoryBtn.cVictoryBtn.show();
                }
            }
            else
            {
                this.removeStatus( status_type.DEAD, action_scope_type.LOCAL );
                if ( this._isFinal )
                {
                    this._victoryBtn.cVictoryBtn.hide();
                }
            }

            this.drawColouringLayer( this._wound.cTokenCounter.count, this._health.cTokenCounter.count, 0xff0000 );
        }

        if ( isPlayerInput )
        {
            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._go ), "wound_token", delta, count, true );
        }
    }

    private onShadowCard_Discarded( shadowCardMini: GameObject ): void
    {
        let index: number = 0;
        for ( let displayObject of this._shadowCardContainer.children )
        {
            displayObject.y = -( displayObject as PIXI.Container ).height * index;

            if ( ( displayObject as any ).go != shadowCardMini )
            {
                ++index;
            }
        }
    }

    private onShadowCardMini_Dropped( dropped: GameObject, dropArea: GameObject, actionScope: action_scope_type ): void
    {
        // Cleanup events.
        dropped.cShadowCardMini.onDiscarded.remove( this.onShadowCard_Discarded, this );

        this.onShadowCard_Discarded( dropped );
    }

    // virtual.
    protected onTarget_Selected( target: GameObject, targetSelectionType: target_selection_type ): void
    {
        if ( targetSelectionType == target_selection_type.UNDERNEATH )
        {
            this.discardShadowCards( action_scope_type.MULTIPLAYER );
        }
        else if ( targetSelectionType == target_selection_type.SAURON_ATTACK )       
        {
            if ( !ServiceLocator.savedData.data.gamePreferences.isManuallyRevealShadowCards )
            {
                for ( let i: number = this._shadowCardContainer.children.length - 1; i >= 0; --i )
                {
                    ( ( this._shadowCardContainer.getChildAt( i ) as any ).go as GameObject ).cShadowCardMini.setFaceUp( action_scope_type.MULTIPLAYER );
                }
            }
        }

        super.onTarget_Selected( target, targetSelectionType );
    }

    // #endregion //
}