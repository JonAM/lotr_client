import Component from "../../Component";

import { ICard } from "../../../CardDB";
import { player_action_type, action_scope_type } from "../../../../service/socket_io/GameSocketIOController";
import { player_type } from "../../world/CPlayerArea";
import ServiceLocator from "../../../../ServiceLocator";
import Utils from "../../../../Utils";
import * as PIXI from "pixi.js";

import { GodrayFilter } from "pixi-filters";
import CSprite from "../../pixi/CSprite";
import GameObject from "../../../GameObject";
import CCardPreviewable from "../../input/CCardPreviewable";
import CContainer from "../../pixi/CContainer";
import LogTargetCard from "../../ui/right_menu/action_log/target/LogTargetCard";
import { ISgCardTokenSide } from "../../../../view/game/SaveGameView";
import CTargetSelector, { target_selection_type } from "../../input/CTargetSelector";
import { detail_bar_icon_type } from "./CDetailBar";
import { input_event_type } from "../../../InputController";


export default abstract class CCardTokenSide extends Component
{
    // #region Attributes //

    // protected:

    protected _cardToken: GameObject = null;
    protected _cardId: string = null;
    protected _cardInfo: ICard = null;
    protected _bg: GameObject = null;
    protected _colouringLayer: PIXI.Graphics = null;
    protected _statusIcon: PIXI.Sprite = null;
    protected _targetSocket: GameObject = null;
    protected _arrStatus: Array<status_type> = null;
    
    protected _highlightedFilter: GodrayFilter = null;
    protected _bowedFilter: PIXI.Filter = null;

    // #endregion //


    // #region Properties //

    public get cardToken(): GameObject { return this._cardToken; }
    public get cardId(): string { return this._cardId; }
    public get cardInfo(): ICard { return this._cardInfo; }
    public get bg(): GameObject { return this._bg; }

    public set cardToken( value: GameObject ) { this._cardToken = value; }
    public set cardId( value: string ) { this._cardId = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CCardTokenSide";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CCardTokenSide.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cTargetReceptor != null, "CCardTokenSide.ts :: init() :: CTargetReceptor component not found." );
        console.assert( this._go.cTargetSelector != null, "CCardTokenSide.ts :: init() :: CTargetSelector component not found." );
        console.assert( this._go.cShareableGameElement != null, "CCardTokenSide.ts :: init() :: CShareableGameElement component not found." );
        console.assert( this._cardToken != null, "CCardTokenSide.ts :: init() :: this._cardToken cannot be null." );
        console.assert( this._cardId != null, "CCardTokenSide.ts :: init() :: this._cardId cannot be null." );
        
        this._arrStatus = new Array<status_type>();

        this._go.cContainer.c.interactive = true;
        this._go.cShareableGameElement.setEnabled( Utils.game.isSharedLocation( this._cardToken.cCardToken.location ) );

        this._cardInfo = ServiceLocator.cardDb.find( this._cardId );

        // Bg.
        this._bg = this.createBg();
        this._go.cContainer.addChild( this._bg );
        ServiceLocator.game.inputController.on( this._bg.cContainer.c, input_event_type.TAP, this.onRadialMenu_Requested, this );

        // Colouring layer.
        this._colouringLayer = new PIXI.Graphics();
        this._colouringLayer.y = this._bg.cContainer.c.height;
        this._go.cContainer.c.addChild( this._colouringLayer );

        // Status icon.
        this._statusIcon = new PIXI.Sprite();
        this._statusIcon.anchor.set( 0.5 );
        this._statusIcon.alpha = 0.3;
        this._statusIcon.position.set( this._bg.cContainer.c.width * 0.5, this._bg.cContainer.c.height * 0.5 );
        this._go.cContainer.c.addChild( this._statusIcon );

        // Target socket.
        this._targetSocket = new GameObject( [ new CContainer() ] );
        this._targetSocket.init();
        this._targetSocket.cContainer.c.position.set( this._bg.cContainer.c.width * 0.5, this._bg.cContainer.c.height * 0.5 );
        this._go.cContainer.addChild( this._targetSocket );
        //
        this._go.cTargetReceptor.targetSocket = this._targetSocket;
        
        // Listen to events.
        this._go.cTargetSelector.onTargetSelected.add( this.onTarget_Selected, this );
    }

        // virtual. default.
        protected createBg(): GameObject
        {
            let result: GameObject = new GameObject( [ new CSprite(), new CCardPreviewable(), new CTargetSelector() ] );
            result.cContainer.c.filters = [];
            result.cSprite.s.texture = Utils.img.createCardPortrait( this._cardId );
            Utils.game.limitSideSize( 180, result.cSprite.s ); 
            result.cContainer.c.interactive = true;
            result.cContainer.c.buttonMode = true;
            result.cCardPreviewable.cardId = this._cardId;
            result.cTargetSelector.receptor = this._go;
            result.init();

            return result;
        }

    public end(): void
    {
        if ( this._highlightedFilter )
        {
            PIXI.Ticker.shared.remove( this.updateHighlightedFilter, this );
        }

        this._highlightedFilter = null;
        this._bowedFilter = null;

        this._bg.end();
        this._bg = null; 

        this._targetSocket.end();
        this._targetSocket = null;

        this._cardToken = null;
        this._cardId = null;
        this._cardInfo = null;
        this._colouringLayer = null;
        this._statusIcon = null;
        this._arrStatus = null;

        super.end();
    }

    public hasStatus( status: status_type ): boolean
    {
        return this._arrStatus.indexOf( status ) >= 0;
    }

    public addStatus( status: status_type, actionScope: action_scope_type ): void
    {
        if ( this._arrStatus.indexOf( status ) != -1 ) { return; }

        // Sfx.
        switch ( status )
        {
            case status_type.EXPLORED : { ServiceLocator.audioManager.playSfx( "explored" ); break; }
            case status_type.DEAD : { ServiceLocator.audioManager.playSfx( "dead" ); break; }

            // TODO:
        }

        let insertIndex: number = 0;
        for ( let statusAux of this._arrStatus )
        {
            if ( statusAux > status )
            {
                break;
            }
            
            insertIndex += 1;
        }
        this._arrStatus.splice( insertIndex, 0, status );
        this.updateStatusIcon();

        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ADD_CARD_TOKEN_STATUS, null, [ this._go.oid, status ] );
        }
    }

        private updateStatusIcon(): void
        {
            if ( this._arrStatus.length == 0 )
            {
                this._statusIcon.visible = false;
            }
            else
            {
                const kArrIconId: Array<string> = [ "status_dead", "status_committed", "status_has_attacked", "status_explored" ];
                this._statusIcon.texture = ServiceLocator.resourceStack.findAsTexture( kArrIconId[ this._arrStatus[ 0 ] ] );
                Utils.game.limitSideSize( this._bg.cContainer.c.height * 0.7, this._statusIcon );
                this._statusIcon.visible = true;
            }
        }

    public removeStatus( status: status_type, actionScope: action_scope_type ): void
    {
        const kStatusIndex: number = this._arrStatus.indexOf( status );
        if ( kStatusIndex >= 0 )
        {
            this._arrStatus.splice( kStatusIndex, 1 );
            this.updateStatusIcon();
        }

        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.REMOVE_CARD_TOKEN_STATUS, null, [ this._go.oid, status ] );
        }
    }

    public addHighlightedFilter(): void
    {
        this._cardToken.cCardToken.animatedFrame.cAnimatedFrame.start();

        this._highlightedFilter = new GodrayFilter();
        this._bg.cContainer.c.filters.push( this._highlightedFilter );

        PIXI.Ticker.shared.add( this.updateHighlightedFilter, this );
    }

        private updateHighlightedFilter(): void
        {
            this._highlightedFilter.time += PIXI.Ticker.shared.elapsedMS / 1000;
        }

    public removeHighlightedFilter(): void
    {
        this._cardToken.cCardToken.animatedFrame.cAnimatedFrame.stop();

        this._bg.cContainer.c.filters.splice( this._bg.cContainer.c.filters.indexOf( this._highlightedFilter ), 1 );
        this._highlightedFilter = null;

        PIXI.Ticker.shared.remove( this.updateHighlightedFilter, this );
    }

    public addBowedFilter(): void
    {
        let cmf: PIXI.filters.ColorMatrixFilter = new PIXI.filters.ColorMatrixFilter();
        cmf.blackAndWhite( true );
        cmf.brightness( 0.75, true );
        this._bg.cContainer.c.filters.push( cmf );

        this._bowedFilter = cmf;
    }

    public removeBowedFilter(): void
    {
        this._bg.cContainer.c.filters.splice( this._bg.cContainer.c.filters.indexOf( this._bowedFilter ), 1 );
        this._bowedFilter = null;
    }

    // virtual.
    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        this._bg.cContainer.c.buttonMode = isEnabled;
        
        ServiceLocator.game.inputController.off( this._bg.cContainer.c, input_event_type.TAP, this.onRadialMenu_Requested, this );
        if ( isEnabled )
        {
            ServiceLocator.game.inputController.on( this._bg.cContainer.c, input_event_type.TAP, this.onRadialMenu_Requested, this );
        }
    }

    // virtual.
    public onDiscarded( actionScope: action_scope_type ): void {}

    // virtual.
    public onHidden(): void 
    {
        ServiceLocator.game.attackBindingManager.removeAllBindings( this._cardToken, action_scope_type.LOCAL );
    }

    // virtual.
    public onRevealed(): void 
    {
        if ( !this._highlightedFilter )
        {
            this._cardToken.cCardToken.animatedFrame.cAnimatedFrame.stop();
        }
        else 
        {
            this._cardToken.cCardToken.animatedFrame.cAnimatedFrame.start();
        }

        this._cardToken.cCardToken.cDetailBar.removeItem( detail_bar_icon_type.ABILITY_USES );
        this._cardToken.cCardToken.cDetailBar.scanForKeywords( this._cardInfo );
    }

    // virtual.
    public saveGame(): ISgCardTokenSide
    {
        return {
            cardId: this._cardId,
            status: this._arrStatus };
    }

    // virtual.
    public loadGame( sgCardTokenSide: ISgCardTokenSide, pass: number ): void
    {
        if ( pass == 0 ) 
        {
            this._arrStatus = sgCardTokenSide.status;
            this.updateStatusIcon();
        }
    }

    // protected:

    protected drawColouringLayer( current: number, maximum: number, color: number ): void
    {
        this._colouringLayer.clear();
        this._colouringLayer.beginFill( color, 0.3 );
        //
        let percentage: number = 1;
        if ( maximum > 0 )
        {
            percentage = current / maximum;
            if ( percentage > 1 )
            {
                percentage = 1;
            }
        }
        //
        const kCurSideBg: PIXI.Container = this._bg.cContainer.c;
        this._colouringLayer.drawRect( 0, 0, kCurSideBg.width, -kCurSideBg.height * percentage );
        this._colouringLayer.endFill();
    }

    protected findTokenCounterValue( value: number ): number
    {
        return value >= 253 ? null : value;
    }

    protected findTokenCounterText( value: number ): string
    {
        let result: string = null;

        switch ( value )
        {
            case 253: { result = "*"; break; }
            case 254: { result = "X"; break; }
            case 255: { result = "-"; break; }

            default: { result = value.toString(); }
        }

        return result;
    }

    protected findTokenCounterSaveValue( text: PIXI.Text, counter: GameObject ): number
    {
        return text.text == "*" || text.text == "X" || text.text == "-" ? null : counter.cTokenCounter.count;
    }

    // virtual.
    protected abstract showRadialMenu(): void;

    // #endregion //


    // #region Input Callbacks //

    private onRadialMenu_Requested( event: PIXI.InteractionEvent ): void
    {
        if ( ServiceLocator.game.poiMenu.cContainer.c.visible || ServiceLocator.game.dragShadowManager.dragShadow ) { return; }
        
        this.showRadialMenu();
    }

    // #endregion //


    // #region Other Callbacks //

    // virtual.
    protected onTarget_Selected( target: GameObject, targetSelectionType: target_selection_type ): void
    {
        switch ( targetSelectionType )
        {
            case target_selection_type.TARGET:
            {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "button_click" );
                
                ServiceLocator.game.cGameWorld.cActionLogger.logTargetSelection( 
                    player_type.PLAYER, new LogTargetCard( this._cardToken ), new LogTargetCard( target.cCardTokenSide.cardToken ), "rad_target", false, true );
                break;
            }
            
            case target_selection_type.PLAYER_ATTACK:
            case target_selection_type.SAURON_ATTACK:
            {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "attack_target_selected" );

                ServiceLocator.game.cGameWorld.cActionLogger.logTargetSelection( 
                    player_type.PLAYER, new LogTargetCard( this._cardToken ), new LogTargetCard( target.cCardTokenSide.cardToken ), targetSelectionType == target_selection_type.PLAYER_ATTACK ? "rad_player_attack" : "rad_sauron_attack", false, true );

                ServiceLocator.game.attackBindingManager.addBinding( this._cardToken, target.cCardTokenSide.cardToken, player_type.PLAYER, action_scope_type.MULTIPLAYER );
                break;
            }
            
            case target_selection_type.EQUIP:
            {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "equipped" );

                target.cCardTokenSide.cardToken.cDropArea.forceDrop( this._cardToken, new PIXI.Point(), action_scope_type.MULTIPLAYER );
                break;
            }

            case target_selection_type.UNDERNEATH:
            {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "button_click" );

                ServiceLocator.game.cGameWorld.cActionLogger.logTargetSelection( 
                    player_type.PLAYER, new LogTargetCard( this._cardToken ), new LogTargetCard( target ), "rad_underneath", false, true );

                this._cardToken.cCardToken.discardAttachments( action_scope_type.MULTIPLAYER );
                this._cardToken.cCardToken.discardUnderneath( action_scope_type.MULTIPLAYER );
                ServiceLocator.game.attackBindingManager.removeAllBindings( this._cardToken, action_scope_type.MULTIPLAYER );

                if ( target.cCardTokenSide )
                {
                    target.cCardTokenSide.cardToken.cCardToken.addCardUnderneath( this._cardToken, action_scope_type.MULTIPLAYER );
                }
                else if ( target.cGameModifier )
                {
                    target.cGameModifier.addCardUnderneath( this._cardToken, action_scope_type.MULTIPLAYER );
                }
                break;
            }
        }
    }

    // #endregion //
}

export interface IFrame
{
    origin: PIXI.Point;
    width: number;
    height: number;
}

export const enum status_type
{
    DEAD = 0,
    COMMITTED_TO_QUEST,
    HAS_ATTACKED,
    EXPLORED
}