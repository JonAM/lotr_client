import Component from "../../Component";

import { action_scope_type, player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import { player_type } from "../../world/CPlayerArea";
import ServiceLocator from "../../../../ServiceLocator";
import { target_selection_type } from "../../input/CTargetSelector";
import Utils from "../../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../../GameObject";
import CSprite from "../../pixi/CSprite";
import CDropArea from "../../input/CDropArea";
import { option_type, IOption } from "../../ui/CRadialMenu";
import CContainer from "../../pixi/CContainer";
import CCardPreviewable from "../../input/CCardPreviewable";
import { GodrayFilter, OutlineFilter } from "pixi-filters";
import LogTargetCard from "../../ui/right_menu/action_log/target/LogTargetCard";
import { ISgAttachment } from "../../../../view/game/SaveGameView";
import CScrollX from "../../ui/CScrollX";
import CDetailBar, { detail_bar_icon_type, IDetailBarItem } from "./CDetailBar";
import { ICardSide } from "../CCard";
import Signal from "../../../../lib/signals/Signal";
import CGraphics from "../../pixi/CGraphics";
import CAnimatedFrame from "../../ui/CAnimatedFrame";
import { input_event_type } from "../../../InputController";


export default class CAttachment extends Component
{
    // #region Attributes //

    // private:

    private _front: ICardSide = null;
    private _back: ICardSide = null;
    private _ownerPlayer: player_type = null;
    private _controllerPlayer: player_type = null;
    private _cardToken: GameObject = null;
    private _isBowed: boolean = false;
    private _isFaceUp: boolean = true;
    private _isHighlighted: boolean = false;
    
    private _animatedFrame: GameObject = null;
    private _cardImgContainer: PIXI.Container = null;
    private _detailBar: GameObject = null;
    private _poiSocket: GameObject = null;

    private _curSide: ICardSide = null;
    private _isAnimated: boolean = true;
    
    private _highlightedFilter: GodrayFilter = null;
    private _bowedFilter: PIXI.Filter = null;


    // Signals.
    private _onFlipped: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get front(): ICardSide { return this._front; }
    public get back(): ICardSide { return this._back; }
    public get ownerPlayer(): player_type { return this._ownerPlayer; }
    public get controllerPlayer(): player_type { return this._controllerPlayer; }
    public get cardToken(): GameObject { return this._cardToken; }
    public get cDetailBar(): CDetailBar { return this._detailBar.cDetailBar; }
    public get isBowed(): boolean { return this._isBowed; }
    public get isFaceUp(): boolean { return this._isFaceUp; }
    public get curSide(): ICardSide { return this._curSide; }

    public set frontCardId( value: string ) { this._front.cardId = value; }
    public set backCardId( value: string ) { this._back.cardId = value; }
    public set ownerPlayer( value: player_type ) { this._ownerPlayer = value; }
    public set controllerPlayer( value: player_type ) { this._controllerPlayer = value; }
    public set cardToken( value: GameObject ) { this._cardToken = value; }
    public set isBowed( value: boolean ) { this._isBowed = value; }
    public set isFaceUp( value: boolean ) { this._isFaceUp = value; }
    public set isAnimated( value: boolean ) { this._isAnimated = value; }

    // Signals.
    public get onFlipped(): Signal { return this._onFlipped; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CAttachment";
        
        this._front = { cardId: null, cardInfo: null, sprite: null };
        this._back = { cardId: null, cardInfo: null, sprite: null };
    }

    public init(): void
    {
        console.assert( this._go.cContainer != null, "CAttachment.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cDraggable != null, "CAttachment.ts :: init() :: CDraggable component not found." );
        console.assert( this._go.cAttachmentPoiReceptor != null, "CAttachment.ts :: init() :: CAttachmentPoiReceptor component not found." );
        console.assert( this._go.cTargetSelector != null, "CAttachment.ts :: init() :: CTargetSelector component not found." );
        console.assert( this._go.cShareableGameElement != null, "CAttachment.ts :: init() :: CSShareableGameElement component not found." );
        console.assert( this._front.cardId != null, "CAttachment.ts :: init() :: this._front.cardId cannot be null." );
        console.assert( this._back.cardId != null, "CAttachment.ts :: init() :: this._back.cardId cannot be null." );
        console.assert( this._ownerPlayer != null, "CAttachment.ts :: init() :: this._ownerPlayer cannot be null." );
        console.assert( this._controllerPlayer != null, "CAttachment.ts :: init() :: this._controllerPlayer cannot be null." );
        console.assert( this._cardToken != null, "CAttachment.ts :: init() :: this._cardToken cannot be null." );

        this._go.cContainer.c.interactive = true;
        this._go.cContainer.c.buttonMode = true;

        this._cardImgContainer = new PIXI.Container();
        this._cardImgContainer.interactive = true;
        this._go.cContainer.c.addChild( this._cardImgContainer );

        this._front.cardInfo = ServiceLocator.cardDb.find( this._front.cardId );
        this._back.cardInfo = ServiceLocator.cardDb.find( this._back.cardId );

        // Back.
        this._back.sprite = this.createCardSide( this._back );
        Utils.game.limitSideSize( 60, this._back.sprite.cSprite.s );
        this._back.sprite.cContainer.c.visible = !this._isFaceUp;
        this._cardImgContainer.addChild( this._back.sprite.cContainer.c );

        // Front.
        this._front.sprite = this.createCardSide( this._front );
        Utils.game.limitSideSize( 60, this._front.sprite.cSprite.s );
        this._front.sprite.cContainer.c.visible = this._isFaceUp;
        this._cardImgContainer.addChild( this._front.sprite.cContainer.c );

        this._curSide = this._isFaceUp ? this._front : this._back;

        // Frame.
        let frame: PIXI.Graphics = new PIXI.Graphics();
        frame.lineStyle( 1, ServiceLocator.game.playerColors[ this._ownerPlayer ] );
        frame.drawRect( 0.5, 0.5, this._go.cContainer.c.width - 1, this._go.cContainer.c.height - 1 );
        this._cardImgContainer.addChild( frame );

        // Animated frame.
        this._animatedFrame = new GameObject( [ new CGraphics(), new CAnimatedFrame() ] );
        this._animatedFrame.cAnimatedFrame.width = this._go.cContainer.c.width - 1;
        this._animatedFrame.cAnimatedFrame.height = this._go.cContainer.c.height - 1;
        this._animatedFrame.init();
        this._cardImgContainer.addChild( this._animatedFrame.cContainer.c );
        //
        this._animatedFrame.cContainer.c.position.set( 0.5, 0.5 );

        // Detail bar.
        this._detailBar = new GameObject( [ new CContainer(), new CDetailBar(), new CScrollX() ] );
        this._detailBar.oid = this._go.oid + "_status_bar";
        this._detailBar.cDetailBar.ownerToken = this._go;
        const kdetailBarItemSide: number = 20;
        this._detailBar.cDetailBar.itemSide = kdetailBarItemSide;
        const kdetailBarItemCount: number = Math.floor( frame.width / kdetailBarItemSide );
        this._detailBar.cScrollX.width = kdetailBarItemSide * kdetailBarItemCount;
        this._detailBar.cScrollX.height = kdetailBarItemSide;
        this._detailBar.cScrollX.itemWidth = kdetailBarItemSide;
        this._detailBar.init();
        this._detailBar.cContainer.c.position.set( 
            ( this._curSide.sprite.cContainer.c.width - kdetailBarItemSide * kdetailBarItemCount ) * 0.5, 
            this._curSide.sprite.cContainer.c.height - kdetailBarItemSide - 2 );
        this._go.cContainer.addChild( this._detailBar );
        this._detailBar.cDetailBar.scanForKeywords( this._curSide.cardInfo );

        // POI socket.
        this._poiSocket = new GameObject( [ new CContainer() ] );
        this._poiSocket.init();
        this._go.cContainer.addChild( this._poiSocket );
        this._poiSocket.cContainer.c.position.set( this._go.cContainer.c.width * 0.5, 5 );
        this._go.cAttachmentPoiReceptor.poiSocket = this._poiSocket;

        if ( this._isBowed )
        {
            this.applyBowedFilter();
        }

        // Listen to events.
        ServiceLocator.game.inputController.on( this._cardImgContainer, input_event_type.TAP, this.onAttachment_PointerTap, this );
        this._go.cTargetSelector.onTargetSelected.add( this.onTarget_Selected, this );

        this.setEnabled( this._controllerPlayer == player_type.PLAYER );
    }

        private createCardSide( cardSide: ICardSide ): GameObject
        {
            let result: GameObject = new GameObject( [ new CSprite(), new CCardPreviewable() ] );
            result.cSprite.s.texture = Utils.img.createCardPortrait( cardSide.cardId );
            result.cSprite.s.filters = [];
            result.cCardPreviewable.cardId = cardSide.cardId;
            result.init();

            return result;
        }

    public end(): void
    {
        // Cleanup events.
        ServiceLocator.game.inputController.off( this._cardImgContainer, input_event_type.TAP, this.onAttachment_PointerTap, this );
        this._go.cTargetSelector.onTargetSelected.remove( this.onTarget_Selected, this );

        this._onFlipped.removeAll();

        if ( this._highlightedFilter )
        {
            PIXI.Ticker.shared.remove( this.updateHighlightedFilter, this );
            this._highlightedFilter = null;
        }
        this._bowedFilter = null;

        this._animatedFrame.end();
        this._animatedFrame = null;

        this._poiSocket.end(),
        this._poiSocket = null;

        this._detailBar.end();
        this._detailBar = null;

        this._front.sprite.end();
        this._front = null;

        this._back.sprite.end();
        this._back = null;
        
        this._curSide = null;

        super.end();
    }

    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        this._go.cContainer.c.buttonMode = isEnabled;
        this._go.cDraggable.setEnabled( isEnabled );

        ServiceLocator.game.inputController.off( this._cardImgContainer, input_event_type.TAP, this.onAttachment_PointerTap, this );
        if ( isEnabled )
        {
            ServiceLocator.game.inputController.on( this._cardImgContainer, input_event_type.TAP, this.onAttachment_PointerTap, this );
        }

        this._detailBar.cDetailBar.setEnabled( isEnabled );
    }

    public setBowed( isBowed: boolean, actionScopeType: action_scope_type ): void
    {
        if ( isBowed == this._isBowed ) { return; }

        if ( this._isAnimated )
        {
            Utils.anim.bumpFromCorner( this._go.cContainer.c );
        }

        if ( isBowed )
        {
            this.applyBowedFilter();
        }
        else
        {
            this._front.sprite.cSprite.s.filters.splice( this._front.sprite.cSprite.s.filters.indexOf( this._bowedFilter ), 1 );
            this._back.sprite.cSprite.s.filters.splice( this._back.sprite.cSprite.s.filters.indexOf( this._bowedFilter ), 1 );

            this._bowedFilter = null;
        }

        this._isBowed = isBowed;

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( isBowed ? player_action_type.BOW_ATTACHMENT_TOKEN : player_action_type.READY_ATTACHMENT_TOKEN, null, [ this._go.oid ] );
        }        
    }

        private applyBowedFilter(): void
        {
            let cmf: PIXI.filters.ColorMatrixFilter = new PIXI.filters.ColorMatrixFilter();
            cmf.blackAndWhite( true );
            cmf.brightness( 0.75, true );
            this._front.sprite.cSprite.s.filters.push( cmf );
            this._back.sprite.cSprite.s.filters.push( cmf );

            this._bowedFilter = cmf;
        }

    public setFaceUp( actionScope: action_scope_type ): void
    {
        if ( this._isFaceUp ) { return; }

        if ( this._isAnimated && ServiceLocator.game.isAnimated )
        {
            Utils.anim.bumpFromCorner( this._go.cContainer.c, this.onFaceUpAnimRepeatCb.bind( this, true ) );
        }
        else
        {
            this.onFaceUpAnimRepeatCb.call( this, false );
        }

        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_ATTACHMENT_FACE_UP, null, [ this._go.oid ] );
        }
    }

        private onFaceUpAnimRepeatCb( isAnimated: boolean ): void
        {
            if ( isAnimated )
            {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "card_face_up" );
            }
            
            this._front.sprite.cContainer.c.visible = true;
            this._back.sprite.cContainer.c.visible = false;
            this._curSide = this._front;   

            this._isFaceUp = true;

            if ( this._curSide.sprite.cCardPreviewable )
            {
                this._curSide.sprite.cCardPreviewable.refresh();
            }

            this._detailBar.cDetailBar.scanForKeywords( this._curSide.cardInfo );

            this._onFlipped.dispatch( this._front, this._back );
        }

    public setFaceDown( actionScope: action_scope_type ): void
    {
        if ( !this._isFaceUp ) { return; }

        if ( this._isAnimated && ServiceLocator.game.isAnimated )
        {
            Utils.anim.bumpFromCorner( this._go.cContainer.c, this.onFaceDownAnimRepeatCb.bind( this, true ) );
        }
        else
        {
            this.onFaceDownAnimRepeatCb.call( this, false );
        }

        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_ATTACHMENT_FACE_DOWN, null, [ this._go.oid ] );
        }
    }

        private onFaceDownAnimRepeatCb( isAnimated: boolean ): void
        {
            if ( isAnimated )
            {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "card_face_down" );
            }

            this._front.sprite.cContainer.c.visible = false;
            this._back.sprite.cContainer.c.visible = true;
            this._curSide = this._back;   

            this._isFaceUp = false;

            if ( this._curSide.sprite.cCardPreviewable )
            {
                this._curSide.sprite.cCardPreviewable.refresh();
            }

            this._detailBar.cDetailBar.scanForKeywords( this._curSide.cardInfo );

            this._onFlipped.dispatch( this._back, this._front );
        }

    public discard( actionScopeType: action_scope_type ): void
    {
        Utils.game.discard( this._go, actionScopeType );
    }

    public setSelectedVfx( isVisible: boolean ): void
    {
        if ( isVisible )
        {
            const kColor: number = ServiceLocator.game.playerColors[ this._controllerPlayer ];
            let outlineFilter: OutlineFilter = new OutlineFilter( 5, kColor, 0.5 );
            outlineFilter.padding = 5;
            this._go.cContainer.c.filters = [ outlineFilter ];
        }
        else
        {
            this._go.cContainer.c.filters = [];
        }
    }

    public setHighlighted( isHighlighted: boolean, actionScope: action_scope_type ): void
    {
        this._isHighlighted = isHighlighted;

        if ( this._isHighlighted )
        {
            this._animatedFrame.cAnimatedFrame.start();

            this._highlightedFilter = new GodrayFilter();
            this._front.sprite.cContainer.c.filters.push( this._highlightedFilter );
            this._back.sprite.cContainer.c.filters.push( this._highlightedFilter );

            PIXI.Ticker.shared.add( this.updateHighlightedFilter, this );

            // Multiplayer.
            if ( actionScope == action_scope_type.MULTIPLAYER )
            {
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ADD_ATTACHMENT_HIGHLIGHT, null, [ this._go.oid ] );
            }
        }
        else
        {
            this._animatedFrame.cAnimatedFrame.stop();

            this._front.sprite.cContainer.c.filters.splice( this._front.sprite.cContainer.c.filters.indexOf( this._highlightedFilter ), 1 );
            this._back.sprite.cContainer.c.filters.splice( this._back.sprite.cContainer.c.filters.indexOf( this._highlightedFilter ), 1 );
            this._highlightedFilter = null;
    
            PIXI.Ticker.shared.remove( this.updateHighlightedFilter, this );
    
            // Multiplayer.
            if ( actionScope == action_scope_type.MULTIPLAYER )
            {
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.REMOVE_ATTACHMENT_HIGHLIGHT, null, [ this._go.oid ] );
            }
        }
    }
    
        private updateHighlightedFilter(): void
        {
            this._highlightedFilter.time += PIXI.Ticker.shared.elapsedMS / 1000;
        }

    public saveGame(): ISgAttachment
    {
        return {
            oid: this._go.oid,
            frontCardId: this._front.cardId,
            backCardId: this._back.cardId,
            isBowed: this._isBowed,
            isFaceUp: this._isFaceUp,
            isHighlighted: this._isHighlighted,
            detailBar: this._detailBar.cDetailBar.saveGame(),
            owner: this._ownerPlayer,
            controller: this._controllerPlayer };
    }

    public loadGame( sgAttachment: ISgAttachment, pass: number ): void
    {
        if ( pass == 0 )
        {
            this._controllerPlayer = sgAttachment.controller;
            this.setEnabled( this._controllerPlayer == player_type.PLAYER );

            if ( sgAttachment.isHighlighted )
            {
                this.setHighlighted( true, action_scope_type.LOCAL );
            }
            if ( sgAttachment.isBowed )
            {
                this.setBowed( true, action_scope_type.LOCAL );
            }
            if ( sgAttachment.isFaceUp )
            {
                this.setFaceUp( action_scope_type.LOCAL );
            }
            else
            {
                this.setFaceDown( action_scope_type.LOCAL );
            }
            this._detailBar.cDetailBar.scanForKeywords( this._curSide.cardInfo );
            if ( sgAttachment.detailBar != null )
            {
                this._detailBar.cDetailBar.loadGame( sgAttachment.detailBar, pass );
            }
        }
    }

    // #endregion //


    // #region Input Callbacks //

    // virtual.
    protected onAttachment_PointerTap(): void
    {
        let showAtPos: PIXI.Point = this._go.cContainer.c.getGlobalPosition();
        showAtPos.x += this._go.cContainer.c.width * 0.5;
        showAtPos.y += this._go.cContainer.c.height * 0.5;

        let arrExtrasOption: Array<IOption> = [
            { id: option_type.UNDERNEATH, isEnabled: true },  
            { id: !this._isHighlighted ? option_type.ADD_HIGHLIGHTING : option_type.REMOVE_HIGHLIGHTING, isEnabled: true },
            { id: option_type.ADD_COUNTER, isEnabled: !this._detailBar.cDetailBar.isMaxCustomCounter() } ];
        if ( !this._detailBar.cDetailBar.findItem( detail_bar_icon_type.CUSTOM_TEXT ) )
        {
            arrExtrasOption.push( { id: option_type.ADD_CUSTOM_TEXT, isEnabled: true } );
        }
        arrExtrasOption.push( { id: option_type.BACK, isEnabled: true } );
        //
        const kArrOption: Array<IOption> = [ 
            { id: option_type.TARGET, isEnabled: true },
            { id: this._isBowed ? option_type.READY : option_type.BOW, isEnabled: true },
            { id: this._isFaceUp ? option_type.PUT_FACE_DOWN : option_type.PUT_FACE_UP, isEnabled: true },
            { id: option_type.DISCARD, isEnabled: true }, 
            { id: option_type.EXTRAS, isEnabled: true, subMenu: arrExtrasOption } ]; 
        ServiceLocator.game.cRadialMenu.tryShow( this._go, showAtPos, kArrOption );
    }

    // #endregion //


    // #region Other Callbacks //

    private onTarget_Selected( target: GameObject, targetSelectionType: target_selection_type ): void
    {
        switch ( targetSelectionType )
        {
            case target_selection_type.TARGET:
            {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "button_click" );

                ServiceLocator.game.cGameWorld.cActionLogger.logTargetSelection( 
                    player_type.PLAYER, new LogTargetCard( this._go ), new LogTargetCard( target ), "rad_target", false, true );
                break;
            }

            case target_selection_type.UNDERNEATH:
            {
                // Sfx.
                ServiceLocator.audioManager.playSfx( "button_click" );

                ServiceLocator.game.cGameWorld.cActionLogger.logTargetSelection( 
                    player_type.PLAYER, new LogTargetCard( this._go ), new LogTargetCard( target ), "rad_underneath", false, true );
                
                target.cCardTokenSide.cardToken.cCardToken.addCardUnderneath( this._go, action_scope_type.MULTIPLAYER );
                break;
            }
        }
    }

    // #endregion //
}