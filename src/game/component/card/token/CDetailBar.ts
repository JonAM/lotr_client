import Component from "../../Component";

import { action_scope_type, player_action_type } from "../../../../service/socket_io/GameSocketIOController";
import { ISgCard, ISgCardTokenDetailBar, ISgCustomCounter, ISgDetailBarItem } from "../../../../view/game/SaveGameView";
import { player_type } from "../../world/CPlayerArea";
import ServiceLocator from "../../../../ServiceLocator";
import Utils from "../../../../Utils";
import { view_layer_id } from "../../../../service/ViewManager";
import * as PIXI from "pixi.js";

import GameObject from "../../../GameObject";
import CSprite from "../../pixi/CSprite";
import CButton from "../../input/CButton";
import CTextPreviewable from "../../input/CTextPreviewable";
import CTokenCounter from "../../ui/CTokenCounter";
import CDraggable from "../../input/CDraggable";
import LogTargetCard from "../../ui/right_menu/action_log/target/LogTargetCard";
import CTooltipReceptor from "../../ui/CTooltipReceptor";
import CGraphics from "../../pixi/CGraphics";
import CustomTextEditionView from "../../../../view/game/card_token/CustomTextEditionView";
import CustomCounterEditionView from "../../../../view/game/card_token/CustomCounterEditionView";
import CUnderneathButton from "../../ui/CUnderneathButton";
import CContainer from "../../pixi/CContainer";
import CDropArea from "../../input/CDropArea";
import SignalBinding from "../../../../lib/signals/SignalBinding";
import { ICard } from "../../../CardDB";
import Session from "../../../../Session";


export default class CDetailBar extends Component
{
    // #region Attributes //

    // private:

    private _ownerToken: GameObject = null;
    private _itemSide: number = null;

    private _arrDetailBarItem: Array<IDetailBarItem> = null;

    // Constants.
    private _arrIconId: Array<string> = [ 
        null, null, null, null, "custom_text", "underneath", "rad_target", "keyword_ranged", "keyword_sentinel", "keyword_unique", 
        "keyword_restricted", "keyword_doomed", "keyword_surge", "keyword_immune", "keyword_immune", "keyword_immune", "keyword_ambush", "keyword_guarded", "keyword_regenerate", "keyword_secrecy", 
        "keyword_battle", "keyword_siege", "keyword_indestructible", "keyword_underworld", "keyword_archery", "keyword_prowl", "keyword_villagers", "keyword_aflame",
        "keyword_assault", "keyword_boarding", "keyword_burn", "keyword_capture", "keyword_deep", "keyword_defense", "keyword_dire", "keyword_discover", "keyword_exploration", "keyword_grapple",
        "keyword_guarded", "keyword_hide", "keyword_hinder", "keyword_investigate", "keyword_loot", "keyword_mire", "keyword_peril", "keyword_permanent", "keyword_phantom", "keyword_race", 
        "keyword_sack", "keyword_safe", "keyword_sailing", "keyword_scour", "keyword_searches", "keyword_spectral", "keyword_time", "keyword_toughness", "keyword_track", "keyword_uncharted", 
        "keyword_venom", "keyword_vast", "keyword_devoted", "keyword_hunt", "keyword_raid", "keyword_ransom", "keyword_relentless", "keyword_sneak", "keyword_steeds", "keyword_frostbite", 
        "victory_display" ];
    private _arrTooltip: Array<string> = [ 
        null, null, null, null, null, "UNDERNEATH", "ABILITY_USES", "RANGED", "SENTINEL", "UNIQUE", 
        "RESTRICTED", "DOOMED_X", "SURGE", "IMMUNE_TO_CARD_EFFECTS", "IMMUNE_TO_PLAYER_CARD_EFFECTS", "IMMUNE_TO_RANGED_DAMAGE", "AMBUSH", "GUARDED", "REGENERATE_X", "SECRECY_X", 
        "BATTLE", "SIEGE", "INDESTRUCTIBLE", "UNDERWORLD_X", "ARCHERY_X", "PROWL_X", "VILLAGERS_X", "AFLAME_X",
        "ASSAULT", "BOARDING_X", "BURN_X", "CAPTURE_X", "DEEP", "DEFENSE", "DIRE", "DISCOVER_X", "EXPLORATION", "GRAPPLE",
        "GUARDED_X", "HIDE_X", "HINDER", "INVESTIGATE_X", "LOOT", "MIRE_X", "PERIL", "PERMANENT", "PHANTOM", "RACE", 
        "SACK_X", "SAFE", "SAILING", "SCOUR", "SEARCHES_X", "SPECTRAL", "TIME_X", "TOUGHNESS_X", "TRACK", "UNCHARTED", 
        "VENOM", "VAST", "DEVOTED", "HUNT_X", "RAID", "RANSOM_X", "RELENTLESS", "SNEAK", "STEEDS_X", "FROSTBITE_X",
        "VICTORY" ];
    private _arrTooltipDesc: Array<string> = [ 
        null, null, null, null, null, null, null, "RANGED_DESC", "SENTINEL_DESC", "UNIQUE_DESC", 
        "RESTRICTED_DESC", "DOOMED_X_DESC", "SURGE_DESC", "IMMUNE_TO_CARD_EFFECTS_DESC", "IMMUNE_TO_PLAYER_CARD_EFFECTS_DESC", "IMMUNE_TO_RANGED_DAMAGE_DESC", "AMBUSH_DESC", "GUARDED_DESC", "REGENERATE_X_DESC", "SECRECY_X_DESC", 
        "BATTLE_DESC", "SIEGE_DESC", "INDESTRUCTIBLE_DESC", "UNDERWORLD_X_DESC", "ARCHERY_X_DESC", "PROWL_X_DESC", "VILLAGERS_X_DESC", "AFLAME_X_DESC",
        "ASSAULT_DESC", "BOARDING_X_DESC", "BURN_X_DESC", "CAPTURE_X_DESC", "DEEP_DESC", "DEFENSE_DESC", "DIRE_DESC", "DISCOVER_X_DESC", "EXPLORATION_DESC", "GRAPPLE_DESC",
        "GUARDED_X_DESC", "HIDE_X_DESC", "HINDER_DESC", "INVESTIGATE_X_DESC", "LOOT_DESC", "MIRE_X_DESC", "PERIL_DESC", "PERMANENT_DESC", "PHANTOM_DESC", "RACE_DESC", 
        "SACK_X_DESC", "SAFE_DESC", "SAILING_DESC", "SCOUR_DESC", "SEARCHES_X_DESC", "SPECTRAL_DESC", "TIME_X_DESC", "TOUGHNESS_X_DESC", "TRACK_DESC", "UNCHARTED_DESC", 
        "VENOM_DESC", "VAST_DESC", "DEVOTED_DESC", "HUNT_X_DESC", "RAID_DESC", "RANSOM_X_DESC", "RELENTLESS_DESC", "SNEAK_DESC", "STEEDS_DESC", "FROSTBITE_X_DESC",
        "VICTORY_DESC" ];
    
    private static readonly _kArrOidSufix: Array<string> = [ "custom_counter_0", "custom_counter_1", "custom_counter_2", "custom_counter_3", "custom_text", "underneath", "ability_uses" ];

    // #endregion //


    // #region Properties //

    public set ownerToken( value: GameObject ) { this._ownerToken = value; }
    public set itemSide( value: number ) { this._itemSide = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CDetailBar";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CDetailBar.ts :: init() :: CContainer component not found." );
        console.assert( this._go.cScrollX != null, "CDetailBar.ts :: init() :: CScrollX component not found." );
        console.assert( this._ownerToken != null, "CDetailBar.ts :: init() :: this._ownerToken cannot be null." );
        console.assert( this._itemSide != null, "CDetailBar.ts :: init() :: this._itemSide cannot be null." );
        
        this._arrDetailBarItem = new Array<IDetailBarItem>();

        this._go.cContainer.c.visible = false;
    }

    public end(): void
    {
        for ( let detailBarItem of this._arrDetailBarItem )
        {
            if ( detailBarItem.counter )
            {
                detailBarItem.counter.end();
            }
            detailBarItem.root.end();
        }
        this._arrDetailBarItem = null;

        this._ownerToken = null;
        this._itemSide = null;

        super.end();
    }

    public findItem( detailIconType: detail_bar_icon_type ): IDetailBarItem
    {
        let result: IDetailBarItem = null;

        for ( let detailBarItem of this._arrDetailBarItem )
        {
            if ( detailBarItem.type == detailIconType )
            {
                result = detailBarItem;
                break;
            }
        }

        return result;
    }

    public addItem( detailIconType: detail_bar_icon_type, ...params: Array<any> ): IDetailBarItem
    {
        let item: IDetailBarItem = null;
        switch ( detailIconType )
        {
            case detail_bar_icon_type.CUSTOM_COUNTER_0: 
            case detail_bar_icon_type.CUSTOM_COUNTER_1: 
            case detail_bar_icon_type.CUSTOM_COUNTER_2: 
            case detail_bar_icon_type.CUSTOM_COUNTER_3: 
            { 
                this._arrIconId[ detailIconType ] = params[ 2 ] as string;

                const kIsEditable: boolean = this.isPlayerControlled();
                item = this.createCounterItem( detailIconType, params[ 0 ] as number, -1, params[ 1 ] as string, kIsEditable, params[ 2 ] as string ); 
                break; 
            }

            case detail_bar_icon_type.CUSTOM_TEXT: { item = this.createCustomTextItem( params[ 0 ] as string, params[ 1 ] as boolean ); break; }
            case detail_bar_icon_type.UNDERNEATH: { item = this.createUnderneathItem(); break; }

            case detail_bar_icon_type.ABILITY_USES: 
            { 
                const kTooltip: string = jQuery.i18n( this._arrTooltip[ detailIconType ] );
                const kIsEditable: boolean = this.isPlayerControlled();
                const kIconId: string = this._arrIconId[ detailIconType ];
                item = this.createCounterItem( detailIconType, params[ 0 ] as number, 0, kTooltip, kIsEditable, kIconId ); 
                break; 
            }

            case detail_bar_icon_type.RANGED:
            case detail_bar_icon_type.SENTINEL:
            case detail_bar_icon_type.UNIQUE:
            case detail_bar_icon_type.SURGE:
            case detail_bar_icon_type.IMMUNE_TO_CARD_EFFECTS:
            case detail_bar_icon_type.IMMUNE_TO_PLAYER_CARD_EFFECTS:
            case detail_bar_icon_type.IMMUNE_TO_RANGED_DAMAGE:
            case detail_bar_icon_type.AMBUSH:
            case detail_bar_icon_type.GUARDED:
            case detail_bar_icon_type.GUARDED_X:
            case detail_bar_icon_type.BATTLE:
            case detail_bar_icon_type.SIEGE:
            case detail_bar_icon_type.INDESTRUCTIBLE:
            case detail_bar_icon_type.ASSAULT:
            case detail_bar_icon_type.DEEP:
            case detail_bar_icon_type.DEFENSE:
            case detail_bar_icon_type.DIRE:
            case detail_bar_icon_type.EXPLORATION:
            case detail_bar_icon_type.GRAPPLE:
            case detail_bar_icon_type.HINDER:
            case detail_bar_icon_type.LOOT:
            case detail_bar_icon_type.PERIL:
            case detail_bar_icon_type.PERMAMENT:
            case detail_bar_icon_type.PHANTOM:
            case detail_bar_icon_type.RACE:
            case detail_bar_icon_type.SAFE:
            case detail_bar_icon_type.SAILING:
            case detail_bar_icon_type.SCOUR:
            case detail_bar_icon_type.SPECTRAL:
            case detail_bar_icon_type.TRACK:
            case detail_bar_icon_type.UNCHARTED:
            case detail_bar_icon_type.VENOM: 
            case detail_bar_icon_type.VAST: 
            case detail_bar_icon_type.DEVOTED:
            case detail_bar_icon_type.SNEAK:
            case detail_bar_icon_type.RELENTLESS:
            case detail_bar_icon_type.RAID: { item = this.createIconItem( detailIconType ); break; }

            case detail_bar_icon_type.RESTRICTED:
            case detail_bar_icon_type.DOOMED_X:
            case detail_bar_icon_type.REGENERATE_X:
            case detail_bar_icon_type.SECRECY_X:
            case detail_bar_icon_type.UNDERWORLD_X:
            case detail_bar_icon_type.ARCHERY_X:
            case detail_bar_icon_type.PROWL_X:
            case detail_bar_icon_type.VILLAGERS_X:
            case detail_bar_icon_type.AFLAME_X:
            case detail_bar_icon_type.BOARDING_X:
            case detail_bar_icon_type.BURN_X:
            case detail_bar_icon_type.CAPTURE_X:
            case detail_bar_icon_type.DISCOVER_X:
            case detail_bar_icon_type.HIDE_X:
            case detail_bar_icon_type.INVESTIGATE_X:
            case detail_bar_icon_type.MIRE_X:
            case detail_bar_icon_type.SACK_X:
            case detail_bar_icon_type.SEARCHES_X:
            case detail_bar_icon_type.TIME_X:
            case detail_bar_icon_type.TOUGHNESS_X:
            case detail_bar_icon_type.HUNT_X:
            case detail_bar_icon_type.RANSOM_X:
            case detail_bar_icon_type.STEEDS_X:
            case detail_bar_icon_type.FROSTBITE_X:
            case detail_bar_icon_type.VICTORY: { item = this.createIconWithTextItem( detailIconType, params[ 0 ] as number ); break; }
        }
        const kInsertIndex: number = this.findItemInsertIndex( detailIconType );
        this._arrDetailBarItem.splice( kInsertIndex, 0, item );
        this._go.cScrollX.addItemAt( item.root, kInsertIndex );

        this._go.cContainer.c.visible = true;

        return item;
    }

        private isPlayerControlled(): boolean
        {
            return this._ownerToken.cCardToken && this._ownerToken.cCardToken.controllerPlayer != player_type.ALLY
                || this._ownerToken.cAttachment && this._ownerToken.cAttachment.controllerPlayer != player_type.ALLY
                || this._ownerToken.cGameModifier != null;
        }

    public removeItem( detailIconType: detail_bar_icon_type ): void
    {
        let itemIndex: number = -1;
        for ( let i: number = 0; i < this._arrDetailBarItem.length; ++i )
        {
            if ( this._arrDetailBarItem[ i ].type == detailIconType )
            {
                itemIndex = i;
                break;
            }
        }
        if ( itemIndex >= 0 )
        {
            let item: IDetailBarItem = this._arrDetailBarItem[ itemIndex ];
            this._arrDetailBarItem.splice( itemIndex, 1 );
            this._go.cScrollX.removeItem( item.root );
            if ( item.counter )
            {
                item.counter.end();
            }
            item.root.end();
        }

        if ( this._arrDetailBarItem.length == 0 )
        {
            this._go.cContainer.c.visible = false;
        }
    }

    public isMaxCustomCounter(): boolean
    {
        let customCounterCount: number = 0;
        for ( let itemAux of this._arrDetailBarItem )
        {
            if ( itemAux.type == detail_bar_icon_type.CUSTOM_COUNTER_0
                || itemAux.type == detail_bar_icon_type.CUSTOM_COUNTER_1
                || itemAux.type == detail_bar_icon_type.CUSTOM_COUNTER_2
                || itemAux.type == detail_bar_icon_type.CUSTOM_COUNTER_3 )
            {
                customCounterCount += 1;
            }
        }

        return customCounterCount >= 4;
    }

    public setItemCount( detailIconType: detail_bar_icon_type, count: number ): void
    {
        let item: IDetailBarItem = null;
        for ( let itemAux of this._arrDetailBarItem )
        {
            if ( itemAux.type == detailIconType )
            {
                item = itemAux;
                break;
            }
        }

        if ( !item )
        {
            this.addItem( detailIconType, count );
        }
        else
        {
            if ( count == 0 && !item.counter 
                || item.counter.cTokenCounter.minCount == count )
            {
                this.removeItem( detailIconType );
            }
            else
            {
                if ( item.counter )
                {
                    item.counter.cTokenCounter.setCount( count, action_scope_type.LOCAL );
                }
                else
                {
                    item.text.text = count.toString();
                }
            }
        }
    }

    public addToItemCount( detailIconType: detail_bar_icon_type, count: number ): void
    {
        let item: IDetailBarItem = null;
        for ( let itemAux of this._arrDetailBarItem )
        {
            if ( itemAux.type == detailIconType )
            {
                item = itemAux;
                break;
            }
        }

        if ( !item )
        {
            this.addItem( detailIconType, count );
        }
        else 
        {
            let newCount: number = -1;
            if ( item.counter )
            {
                newCount = count + item.counter.cTokenCounter.count;
            }
            else
            {
                newCount = parseInt( item.text.text ) + count;
            }

            if ( newCount == 0 && !item.counter
                || item.counter && item.counter.cTokenCounter.minCount == newCount )
            {
                this.removeItem( detailIconType );
            }
            else
            {
                if ( item.counter )
                {
                    item.counter.cTokenCounter.setCount( newCount, action_scope_type.LOCAL );
                }
                else
                {
                    item.text.text = newCount.toString();
                }
            }
        }
    }

    public setEnabled( isEnabled: boolean ): void
    {
        super.setEnabled( isEnabled );

        for ( let item of this._arrDetailBarItem )
        {
            switch ( item.type )
            {
                case detail_bar_icon_type.ABILITY_USES:
                case detail_bar_icon_type.CUSTOM_COUNTER_0:
                case detail_bar_icon_type.CUSTOM_COUNTER_1:
                case detail_bar_icon_type.CUSTOM_COUNTER_2:
                case detail_bar_icon_type.CUSTOM_COUNTER_3:
                {
                    item.counter.cTokenCounter.setEnabled( isEnabled );
                    break;
                }

                case detail_bar_icon_type.CUSTOM_TEXT:
                {
                    item.root.cButton.setEnabled( isEnabled );
                    item.root.cDraggable.setEnabled( isEnabled );
                    break;
                }

                case detail_bar_icon_type.UNDERNEATH:
                {
                    if ( Utils.game.isSharedLocation( this._ownerToken.cCardToken.location ) )
                    {
                        item.root.cUnderneathButton.setEnabled( isEnabled );
                    }
                    break;
                }

                // TODO: More?
            }
        }
    }

    public scanForKeywords( cardInfo: ICard ): void
    {
        this.removeKeywords();

        if ( cardInfo.is_unique )
        {
            this.addItem( detail_bar_icon_type.UNIQUE );
        }
        if ( cardInfo.victory )
        {
            let victory: number = null;
            if ( cardInfo.code == "704015" )
            {
                victory = 1;
                if ( Session.allyId )
                {
                    victory += 1;
                }
            }
            else
            {
                victory = cardInfo.victory;
            }
            this.addItem( detail_bar_icon_type.VICTORY, victory );
        }

        if ( cardInfo.text.indexOf( "Ranged." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.RANGED );
        }
        if ( cardInfo.text.indexOf( "Sentinel." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.SENTINEL );
        }
        if ( cardInfo.text.indexOf( "Restricted." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.RESTRICTED, null );
        }
        if ( cardInfo.text.indexOf( "Surge." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.SURGE );
        }
        if ( cardInfo.text.indexOf( "Immune to card effects." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.IMMUNE_TO_CARD_EFFECTS );
        }
        if ( cardInfo.text.indexOf( "Immune to player card effects." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.IMMUNE_TO_PLAYER_CARD_EFFECTS );
        }
        if ( cardInfo.text.indexOf( "Immune to ranged damage." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.IMMUNE_TO_RANGED_DAMAGE );
        }
        if ( cardInfo.text.indexOf( "Ambush." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.AMBUSH );
        }
        if ( cardInfo.text.indexOf( "Guarded." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.GUARDED );
        }
        if ( cardInfo.text.indexOf( "Guarded (" ) != -1 )
        {
            this.addItem( detail_bar_icon_type.GUARDED_X );
        }
        if ( cardInfo.text.indexOf( "Battle." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.BATTLE );
        }
        if ( cardInfo.text.indexOf( "Siege." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.SIEGE );
        }
        if ( cardInfo.text.indexOf( "Indestructible." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.INDESTRUCTIBLE );
        }
        if ( cardInfo.text.indexOf( "Assault." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.ASSAULT );
        }
        const kDeepRegex: RegExp = /^((\W|\w)+\. )?Deep\./m;
        if ( kDeepRegex.test( cardInfo.text ) )
        {
            this.addItem( detail_bar_icon_type.DEEP );
        }
        const kDefenseRegex: RegExp = /^((\W|\w)+\. )?Defense\./m;
        if ( kDefenseRegex.test( cardInfo.text ) )
        {
            this.addItem( detail_bar_icon_type.DEFENSE );
        }
        if ( cardInfo.text.indexOf( "Dire." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.DIRE );
        }
        if ( cardInfo.text.indexOf( "Exploration." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.EXPLORATION );
        }
        if ( cardInfo.text.indexOf( "Grapple." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.GRAPPLE );
        }
        if ( cardInfo.text.indexOf( "Hinder." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.HINDER );
        }
        if ( cardInfo.text.indexOf( "Loot." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.LOOT );
        }
        if ( cardInfo.text.indexOf( "Peril." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.PERIL );
        }
        if ( cardInfo.text.indexOf( "Permanent." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.PERMAMENT );
        }
        if ( cardInfo.text.indexOf( "Phantom." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.PHANTOM );
        }
        if ( cardInfo.text.indexOf( "Race." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.RACE );
        }
        if ( cardInfo.text.indexOf( "Safe." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.SAFE );
        }
        if ( cardInfo.text.indexOf( "Sailing." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.SAILING );
        }
        if ( cardInfo.text.indexOf( "Scour." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.SCOUR );
        }
        if ( cardInfo.text.indexOf( "Spectral." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.SPECTRAL );
        }
        if ( cardInfo.text.indexOf( "Track." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.TRACK );
        }
        if ( cardInfo.text.indexOf( "Uncharted." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.UNCHARTED );
        }
        if ( cardInfo.text.indexOf( "Venom." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.VENOM );
        }
        if ( cardInfo.text.indexOf( "Vast." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.VAST );
        }
        if ( cardInfo.text.indexOf( "Devoted." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.DEVOTED );
        }
        if ( cardInfo.text.indexOf( "Sneak." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.SNEAK );
        }
        if ( cardInfo.text.indexOf( "Raid." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.RAID );
        }
        if ( cardInfo.text.indexOf( "Relentless." ) != -1 )
        {
            this.addItem( detail_bar_icon_type.RELENTLESS );
        }

        const kDoomedRegex: RegExp = /Doomed (\d|X)\./;
        let arrExec: RegExpExecArray = kDoomedRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.DOOMED_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kRegenerateRegex: RegExp = /Regenerate (\d|X)\./;
        arrExec = kRegenerateRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.REGENERATE_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kSecrecyRegex: RegExp = /Secrecy (\d|X)\./;
        arrExec = kSecrecyRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.SECRECY_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kUnderworldRegex: RegExp = /Underworld (\d|X)\./;
        arrExec = kUnderworldRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.UNDERWORLD_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kArcheryRegex: RegExp = /Archery (\d|X)\./;
        arrExec = kArcheryRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.ARCHERY_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kProwlRegex: RegExp = /Prowl (\d|X)\./;
        arrExec = kProwlRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.PROWL_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kVillagersRegex: RegExp = /Villagers (\d|X)\./;
        arrExec = kVillagersRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.VILLAGERS_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kAflameRegex: RegExp = /Aflame (\d|X)\./;
        arrExec = kAflameRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.AFLAME_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kBoardingRegex: RegExp = /Boarding (\d|X)\./;
        arrExec = kBoardingRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.BOARDING_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kBurnRegex: RegExp = /Burn (\d|X)\./;
        arrExec = kBurnRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.BURN_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kCaptureRegex: RegExp = /Capture (\d|X)\./;
        arrExec = kCaptureRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.CAPTURE_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kDiscoverRegex: RegExp = /Discover (\d|X)\./;
        arrExec = kDiscoverRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.DISCOVER_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kHideRegex: RegExp = /Hide (\d|X)\./;
        arrExec = kHideRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.HIDE_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kInvestigateRegex: RegExp = /Investigate (\d|X)\./;
        arrExec = kInvestigateRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.INVESTIGATE_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kMireRegex: RegExp = /Mire (\d|X)\./;
        arrExec = kMireRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.MIRE_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kSackRegex: RegExp = /Sack (\d|X)\./;
        arrExec = kSackRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.SACK_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kSearchesRegex: RegExp = /Searches (\d|X)\./;
        arrExec = kSearchesRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.SEARCHES_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kTimeRegex: RegExp = /Time (\d|X)\./;
        arrExec = kTimeRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.TIME_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kToughnessRegex: RegExp = /Toughness (\d|X)\./;
        arrExec = kToughnessRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.TOUGHNESS_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kHuntRegex: RegExp = /Hunt (\d|X)\./;
        arrExec = kHuntRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.HUNT_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kRansomRegex: RegExp = /Ransom (\d|X)\./;
        arrExec = kRansomRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.RANSOM_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kSteedsRegex: RegExp = /Steeds (\d|X)\./;
        arrExec = kSteedsRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.STEEDS_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }
        const kFrostbiteRegex: RegExp = /Frostbite (\d|X)\./;
        arrExec = kFrostbiteRegex.exec( cardInfo.text );
        if  ( arrExec != null )
        {
            this.addItem( detail_bar_icon_type.FROSTBITE_X, arrExec[ 1 ] == "X" ? null : parseInt( arrExec[ 1 ] ) );
        }

        if ( this._arrDetailBarItem.length > 0 )
        {
            this._go.cContainer.c.visible = true;
        }
    }

        private removeKeywords(): void
        {
            for ( let i: detail_bar_icon_type = detail_bar_icon_type.RANGED; i <= detail_bar_icon_type.VICTORY; ++i )
            {
                this.removeItem( i );
            }
        }

    // overrides.
    public saveGame(): ISgCardTokenDetailBar
    {
        let result: ISgCardTokenDetailBar = { items: new Array<ISgDetailBarItem>() };

        for ( let item of this._arrDetailBarItem )
        {
            switch ( item.type )
            {
                case detail_bar_icon_type.ABILITY_USES:
                {
                    result.items.push( { type: item.type, counter: item.counter.cTokenCounter.saveGame() } );
                    break;
                }

                case detail_bar_icon_type.CUSTOM_COUNTER_0:
                case detail_bar_icon_type.CUSTOM_COUNTER_1:
                case detail_bar_icon_type.CUSTOM_COUNTER_2:
                case detail_bar_icon_type.CUSTOM_COUNTER_3:
                {
                    let sgCustomCounter: ISgCustomCounter = {
                        counter: item.counter.cTokenCounter.saveGame(),
                        tooltip: item.tooltip,
                        iconId: item.iconId };
                    result.items.push( { type: item.type, customCounter: sgCustomCounter } );
                    break;
                }

                case detail_bar_icon_type.CUSTOM_TEXT:
                {
                    result.items.push( { type: item.type, customText: item.root.cTextPreviewable.text } );
                    break;
                }

                case detail_bar_icon_type.UNDERNEATH:
                {
                    let arrSgCard: Array<ISgCard> = new Array<ISgCard>();
                    let arrCard: Array<GameObject> = item.root.cUnderneathButton.cDeckIndicator.cViewer.cCardView.findItems();
                    for ( let card of arrCard )
                    {
                        arrSgCard.push( card.cCard.saveGame() );
                    }
                    result.items.push( { type: item.type, underneath: arrSgCard } );
                    break;
                }
            }
        }

        return result;
    }

    // overrides.
    public loadGame( sgCardTokenDetailBar: ISgCardTokenDetailBar, pass: number ): void
    {
        for ( let item of sgCardTokenDetailBar.items )
        {
            switch ( item.type )
            {
                case detail_bar_icon_type.ABILITY_USES:
                {
                    this.addItem( item.type, item.counter.count );
                    break;
                }

                case detail_bar_icon_type.CUSTOM_COUNTER_0:
                case detail_bar_icon_type.CUSTOM_COUNTER_1:
                case detail_bar_icon_type.CUSTOM_COUNTER_2:
                case detail_bar_icon_type.CUSTOM_COUNTER_3:
                {
                    this.addItem( item.type, item.customCounter.counter.count, item.customCounter.tooltip, item.customCounter.iconId );
                    break;
                }

                case detail_bar_icon_type.CUSTOM_TEXT:
                {
                    this.addItem( item.type, item.customText );
                    break;
                }

                case detail_bar_icon_type.UNDERNEATH:
                {
                    this.addItem( item.type );
                    let cUnderneathButton: CUnderneathButton = this.findItem( detail_bar_icon_type.UNDERNEATH ).root.cUnderneathButton;
                    for ( let sgCard of item.underneath )
                    {
                        let card: GameObject = GameObject.find( sgCard.oid );
                        card.cCard.loadGame( sgCard, pass );
                        cUnderneathButton.cDeckIndicator.cViewer.cCardView.go.cDropArea.forceDrop( card, new PIXI.Point( CDropArea.kPredefinedDropPositionCode, 0 ), action_scope_type.LOCAL );
                    }
                    break;
                }
            }
        }
    }

    // private:

    private findItemInsertIndex( detailIconType: detail_bar_icon_type ): number
    {
        let result: number = 0;

        for ( let detailBarItem of this._arrDetailBarItem )
        {
            if ( detailBarItem.type < detailIconType )
            {
                result += 1;
            }
            else
            {
                break;
            }
        }
    
        return result;
    }

    private createCounterItem( detailIconType: detail_bar_icon_type, count: number, minCount: number, tooltip: string, isEditable: boolean, iconId: string ): IDetailBarItem
    {
        let result: IDetailBarItem = { 
            type: detailIconType,
            root: null,
            iconId: iconId };

        let arrComponent: Array<Component> = [ new CGraphics() ];
        if ( tooltip )
        {
            result.tooltip = tooltip
            arrComponent.push( new CTooltipReceptor() );
        }
        result.root = new GameObject( arrComponent );
        result.root.cGraphics.g.drawRect( 0, 0, this._itemSide, this._itemSide );
        if ( result.root.cTooltipReceptor )
        {
            result.root.cTooltipReceptor.text = tooltip;
        }
        result.root.init();
        //
        let wrapper: PIXI.Container = new PIXI.Container();
        wrapper.position.set( this._itemSide * 0.5 );
        result.root.cContainer.c.addChild( wrapper );
        // Counter.
        result.counter = new GameObject( [ new CSprite(), new CTokenCounter(), new CDraggable() ] );
        result.counter.oid = this._go.oid + "_" + CDetailBar._kArrOidSufix[ detailIconType ];
        result.counter.cSprite.s.texture = ServiceLocator.resourceStack.findAsTexture( iconId );
        Utils.game.limitSideSize( this._itemSide, result.counter.cSprite.s );
        result.counter.cSprite.s.anchor.set( 0.5 );
        result.counter.cTokenCounter.minCount = minCount;
        result.counter.cTokenCounter.count = count;
        result.counter.cTokenCounter.draggedTexture = PIXI.Texture.from( ServiceLocator.resourceStack.find( this._arrIconId[ detailIconType ] ).data );
        result.counter.init();
        wrapper.addChild( result.counter.cContainer.c );
        result.counter.cTokenCounter.onCountUpdated.add( this.onCounter_Updated.bind( this, result ) );
        // Text.
        result.text = new PIXI.Text( count.toString(), ServiceLocator.game.textStyler.small );
        result.text.anchor.set( 0.9, 0.9 );
        result.text.position.set( result.counter.cContainer.c.width * 0.5, result.counter.cContainer.c.height * 0.5 );
        wrapper.addChild( result.text );

        if ( !isEditable )
        {
            result.counter.cTokenCounter.setEnabled( false );
        }
    
        return result;
    }

    private createCustomTextItem( text: string, isEditable: boolean ): IDetailBarItem
    {
        let result: IDetailBarItem = { 
            type: detail_bar_icon_type.CUSTOM_TEXT,
            root: null };

        result.root = new GameObject( [ new CSprite(), new CButton(), new CDraggable(), new CTextPreviewable() ] );  
        result.root.oid = this._go.oid + "_" + CDetailBar._kArrOidSufix[ detail_bar_icon_type.CUSTOM_TEXT ];
        result.root.cSprite.s.texture = PIXI.Texture.from( ServiceLocator.resourceStack.find( "custom_text" ).data );  
        Utils.game.limitSideSize( this._itemSide, result.root.cSprite.s );
        result.root.init();

        result.root.cButton.onClick.add( this.onCustomTextIcon_Click, this );
        result.root.cButton.setEnabled( isEditable );
        result.root.cDraggable.onDragged.addOnce( this.onCustomTextIcon_Dragged, this );
        result.root.cDraggable.setEnabled( isEditable );

        result.root.cTextPreviewable.text = text;

        return result;
    }

    private createIconItem( detailIconType: detail_bar_icon_type ): IDetailBarItem
    {
        let result: IDetailBarItem = { 
            type: detailIconType,
            root: null };

        result.root = new GameObject( [ new CGraphics(), new CTooltipReceptor() ] );
        result.root.cGraphics.g.drawRect( 0, 0, this._itemSide, this._itemSide );
        result.root.cTooltipReceptor.text = jQuery.i18n( this._arrTooltip[ detailIconType ] );
        if ( this._arrTooltipDesc[ detailIconType ] )
        {
            result.root.cTooltipReceptor.description = jQuery.i18n( this._arrTooltipDesc[ detailIconType ] );
        }
        result.root.init();
        let icon: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( this._arrIconId[ detailIconType ] ) );
        Utils.game.limitSideSize( this._itemSide, icon );
        icon.anchor.set( 0.5 );
        icon.position.set( this._itemSide * 0.5 );
        result.root.cContainer.c.addChild( icon );
        
        return result;
    }

    private createIconWithTextItem( detailIconType: detail_bar_icon_type, count: number ): IDetailBarItem
    {
        let result: IDetailBarItem = { 
            type: detailIconType,
            root: null };

        result.root = new GameObject( [ new CGraphics(), new CTooltipReceptor() ] );
        result.root.cGraphics.g.drawRect( 0, 0, this._itemSide, this._itemSide );
        result.root.cTooltipReceptor.text = jQuery.i18n( this._arrTooltip[ detailIconType ] );
        if ( this._arrTooltipDesc[ detailIconType ] )
        {
            result.root.cTooltipReceptor.description = jQuery.i18n( this._arrTooltipDesc[ detailIconType ] );
        }
        result.root.init();
        // Icon.
        let icon: PIXI.Sprite = PIXI.Sprite.from( ServiceLocator.resourceStack.findAsTexture( this._arrIconId[ detailIconType ] ) );
        Utils.game.limitSideSize( this._itemSide, icon );
        icon.anchor.set( 0.5 );
        icon.position.set( this._itemSide * 0.5 );
        result.root.cContainer.c.addChild( icon );
        if ( count != null )
        {
            // Text.
            result.text = new PIXI.Text( count.toString(), ServiceLocator.game.textStyler.small );
            result.text.anchor.set( 0.9, 0.9 );
            result.text.position.set( this._itemSide, this._itemSide );
            result.root.cContainer.c.addChild( result.text );
        }

        return result;
    }

    private createUnderneathItem(): IDetailBarItem
    {
        let result: IDetailBarItem = { 
            type: detail_bar_icon_type.UNDERNEATH,
            root: null };

        result.root = new GameObject( [ new CContainer(), new CUnderneathButton(), new CTooltipReceptor() ] );
        result.root.cUnderneathButton.ownerToken = this._ownerToken;
        result.root.cUnderneathButton.sideLength = this._itemSide;
        result.root.cTooltipReceptor.text = jQuery.i18n( this._arrTooltip[ detail_bar_icon_type.UNDERNEATH ] );
        result.root.init();
        result.root.cUnderneathButton.onItemCountUpdated.add( this.onUnderneathItemCount_Updated, this );

        return result;
    }

    // #endregion //


    // #region Custom Text //

    public tryEditCustomText(): void
    {
        // Multiplayer.
        if ( Session.allyId && this._ownerToken.cShareableGameElement && this._ownerToken.cShareableGameElement.isEnabled )
        {
            this._ownerToken.cShareableGameElement.onLockOpen.addOnce( this.editCustomText, this );
            this._ownerToken.cShareableGameElement.notifyLock();
        }
        else
        {
            this.editCustomText();
        }
    }

    public editCustomText(): void
    {
        let customTextEditionView: CustomTextEditionView = new CustomTextEditionView();
        const kCustomText: string = this.findCustomText();
        if ( kCustomText )
        {
            customTextEditionView.text = kCustomText;
        }
        customTextEditionView.onAccepted.addOnce( this.onCustomText_Updated, this );
        customTextEditionView.onClosed.addOnce( this.onCustomTextEditionView_Closed, this );
        customTextEditionView.init();
        ServiceLocator.viewManager.fadeIn( customTextEditionView, view_layer_id.POPUP );
    }

        private findCustomText(): string 
        {
            let result: string = null;

            for ( let item of this._arrDetailBarItem )
            {
                if ( item.type == detail_bar_icon_type.CUSTOM_TEXT )
                {
                    result = item.root.cTextPreviewable.text;
                    break;
                }
            }

            return result;
        }

    public setCustomText( text: string, actionScopeType: action_scope_type ): void
    {
        let item: IDetailBarItem = null;
        for ( let itemAux of this._arrDetailBarItem )
        {
            if ( itemAux.type == detail_bar_icon_type.CUSTOM_TEXT )
            {
                item = itemAux;
                break;
            }
        }

        if ( !item )
        {
            const kIsEditable: boolean = this.isPlayerControlled();
            this.addItem( detail_bar_icon_type.CUSTOM_TEXT, text, kIsEditable );
        }
        else
        {
            item.root.cTextPreviewable.text = text;
        }

        // Multiplayer.
        if ( actionScopeType == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_CUSTOM_TEXT, null, [ this._ownerToken.oid, text ] );
        }
    }

    // #endregion //


    // #region Custom Counter //

    public tryAddCustomCounter(): void
    {
        // Multiplayer.
        if ( Session.allyId && this._ownerToken.cShareableGameElement && this._ownerToken.cShareableGameElement.isEnabled )
        {
            this._ownerToken.cShareableGameElement.onLockOpen.addOnce( this.addCustomCounter, this );
            this._ownerToken.cShareableGameElement.notifyLock();
        }
        else
        {
            this.addCustomCounter();
        }
    }

    public addCustomCounter(): void
    {
        let customCounterEditionView: CustomCounterEditionView = new CustomCounterEditionView();
        customCounterEditionView.onAccepted.addOnce( this.onCustomCounter_Created, this );
        customCounterEditionView.onClosed.addOnce( this.onCustomCounterEditionView_Closed, this );
        customCounterEditionView.init();
        ServiceLocator.viewManager.fadeIn( customCounterEditionView, view_layer_id.POPUP );
    }

    public forceCustomCounterCreation( count: number, tooltip: string, iconId: string ): void
    {
        this.onCustomCounter_Created( count, tooltip, iconId );
    }

    // #endregion //


    // #region Other Callbacks //

    private onCounter_Updated( detailBarItem: IDetailBarItem, count: number, delta: number, isPlayerInput: boolean ): void
    {
        if ( count > detailBarItem.counter.cTokenCounter.minCount )
        {
            detailBarItem.text.text = count.toString();

            CTokenCounter.animate( detailBarItem.counter.cContainer.c.parent, delta ); 

            if ( isPlayerInput )
            {
                ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.PLAYER, new LogTargetCard( this._ownerToken ), this._arrIconId[ detailBarItem.type ], delta, count, true );
            }
        }
        else
        {
            window.setTimeout( () => { 
                ServiceLocator.game.dragShadowManager.forceStagePointerUp();
                this.removeItem( detailBarItem.type ); } );
        }
    }

    private onCustomTextIcon_Click( go: GameObject, actionScope: action_scope_type, event: PIXI.InteractionEvent ): void
    {   
        if ( event.data.button == 2 )
        {
            this.removeItem( detail_bar_icon_type.CUSTOM_TEXT );

            // Multiplayer.
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.REMOVE_CUSTOM_TEXT, null, [ this._ownerToken.oid ] );
        }
        else
        {
            this.tryEditCustomText();
        }
    }

    private onCustomTextIcon_Dragged(): void
    {
        window.setTimeout( () => { 
            ServiceLocator.game.dragShadowManager.forceStagePointerUp();
            this.removeItem( detail_bar_icon_type.CUSTOM_TEXT ); 

            // Multiplayer.
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.REMOVE_CUSTOM_TEXT, null, [ this._ownerToken.oid ] );
        } );
    }

    private onCustomText_Updated( text: string ): void
    {   
        this.setCustomText( text, action_scope_type.MULTIPLAYER );
    }

    private onCustomTextEditionView_Closed(): void
    {
        // Multiplayer.
        if ( this._ownerToken.cShareableGameElement )
        {
            this._ownerToken.cShareableGameElement.notifyUnlock();
        }
    }

    private onCustomCounter_Created( count: number, tooltip: string, iconId: string ): void
    {   
        const kDetailIconType: detail_bar_icon_type = this.findAvailableCustomCounter();
        this.addItem( kDetailIconType, count, tooltip, "custom_counter_" + iconId );

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ADD_CUSTOM_COUNTER, null, [ this._ownerToken.oid, kDetailIconType, count, tooltip, "custom_counter_" + iconId ] );
    }

        private findAvailableCustomCounter(): detail_bar_icon_type
        {
            let result: detail_bar_icon_type = null;

            const kArrCustomCounterIconType: Array<detail_bar_icon_type> = [ 
                detail_bar_icon_type.CUSTOM_COUNTER_0,
                detail_bar_icon_type.CUSTOM_COUNTER_1,
                detail_bar_icon_type.CUSTOM_COUNTER_2,
                detail_bar_icon_type.CUSTOM_COUNTER_3 ];
            for ( let customCounterIconType of kArrCustomCounterIconType )
            {
                let isFound: boolean = false;
                for ( let item of this._arrDetailBarItem )
                {
                    if ( item.type == customCounterIconType )
                    {
                        isFound = true;
                        break;
                    }
                }
                if ( !isFound )
                {
                    result = customCounterIconType;
                    break;
                }
            }

            return result;
        }

    private onCustomCounterEditionView_Closed(): void
    {
        // Multiplayer.
        if ( this._ownerToken.cShareableGameElement )
        {
            this._ownerToken.cShareableGameElement.notifyUnlock();
        }
    }

    private onUnderneathItemCount_Updated( count: number ): void
    {
        if ( count == 0 )
        {
            this.removeItem( detail_bar_icon_type.UNDERNEATH );
        }
    }

    // #endregion //
}

export const enum detail_bar_icon_type
{
    CUSTOM_COUNTER_0 = 0,
    CUSTOM_COUNTER_1,
    CUSTOM_COUNTER_2,
    CUSTOM_COUNTER_3,
    CUSTOM_TEXT,
    UNDERNEATH,
    ABILITY_USES,
    //
    RANGED,
    SENTINEL,
    UNIQUE,
    RESTRICTED,
    DOOMED_X,
    SURGE,
    IMMUNE_TO_CARD_EFFECTS,
    IMMUNE_TO_PLAYER_CARD_EFFECTS,
    IMMUNE_TO_RANGED_DAMAGE,
    AMBUSH,
    GUARDED,
    REGENERATE_X,
    SECRECY_X,
    BATTLE,
    SIEGE,
    INDESTRUCTIBLE,
    UNDERWORLD_X,
    ARCHERY_X,
    PROWL_X,
    VILLAGERS_X,
    AFLAME_X,
    ASSAULT,
    BOARDING_X,
    BURN_X,
    CAPTURE_X,
    DEEP,
    DEFENSE,
    DIRE,
    DISCOVER_X,
    EXPLORATION,
    GRAPPLE,
    GUARDED_X,
    HIDE_X,
    HINDER,
    INVESTIGATE_X,
    LOOT,
    MIRE_X,
    PERIL,
    PERMAMENT,
    PHANTOM,
    RACE,
    SACK_X,
    SAFE,
    SAILING,
    SCOUR,
    SEARCHES_X,
    SPECTRAL,
    TIME_X,
    TOUGHNESS_X,
    TRACK,
    UNCHARTED,
    VENOM,
    VAST,
    DEVOTED,
    HUNT_X,
    RAID,
    RANSOM_X,
    RELENTLESS,
    SNEAK,
    STEEDS_X,
    FROSTBITE_X,
    VICTORY
}

export interface IDetailBarItem
{
    type: detail_bar_icon_type;
    root: GameObject;
    counter?: GameObject;
    text?: PIXI.Text;
    iconId?: string;
    tooltip?: string;
    isPrivate?: boolean;
}