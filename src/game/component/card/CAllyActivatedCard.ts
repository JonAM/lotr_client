import Component from "../Component";

import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../GameObject";
import CSprite from "../pixi/CSprite";
import CCard from "./CCard";
import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import { IOppActionListener, IOpponentAction } from "../../AllyActionManager";


export default class CAllyActivatedCard extends Component implements IOppActionListener
{
    // #region Attributes //

    // private:

    private _card: GameObject = null;

    private _portrait: GameObject = null;

    // #endregion //


    // #region Properties //

    public get card(): GameObject { return this._card; }

    public set card( value: GameObject ) { this._card = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CAllyActivatedCard";
    }

    public init(): void
    {
        super.init();
        
        console.assert( this._go.cContainer != null, "CAllyActivatedCard.ts :: init() :: CContainer component not found." );
        console.assert( this._card != null, "CAllyActivatedCard.ts :: init() :: this._card cannot be null." );

        // Portrait.
        this._portrait = this.createPortrait();
        Utils.game.limitSideSize( 300, this._portrait.cSprite.s );
        this._go.cContainer.addChild( this._portrait );

        // Border.
        let border: PIXI.Graphics = new PIXI.Graphics();
        border.lineStyle( 2, ServiceLocator.game.playerColors[ this._card.cCard.ownerPlayer ] );
        border.drawRect( 0, 0, this._go.cContainer.c.width, this._go.cContainer.c.height );
        this._go.cContainer.c.addChild( border );

        // Listen to events.
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.CARD_ACTIVATION_DISCARD ] );
    }

        private createPortrait(): GameObject
        {
            let portrait: GameObject = new GameObject( [ new CSprite() ] );
            portrait.cSprite.s.texture = Utils.img.findCardTexture( this._card.cCard.curSide.cardId );
            portrait.init();

            return portrait;
        }

    public end(): void
    {
        // Cleanup events.
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.CARD_ACTIVATION_DISCARD ] );

        this._card = null;

        this._portrait.end();
        this._portrait = null;

        super.end();
    }

    public discard( actionScopeType: action_scope_type ): void
    {
        Utils.game.discard( this._card, actionScopeType );

        this._go.end();
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.CARD_ACTIVATION_DISCARD )
        {
            this._go.end();
        }
    }

    // #endregion //
}