import GameObject from "../GameObject";


export default abstract class Component
{
    // #region Attributes //

    // protected: 

    protected _id: string = null;
    protected _go: GameObject = null;

    protected _isEnabled: boolean = true;

    // #endregion //


    // #region Properties //

    public get id(): string { return this._id; }
    public get go(): GameObject { return this._go; }
    public get isEnabled(): boolean { return this._isEnabled; }
    
    public set go( value: GameObject ) { this._go = value; }

    // #endregion //


    // #region Methods //

    // protected:

    protected constructor() {}

    // public:

    public init(): void
    {
        console.assert( this._id != null, "Component.ts :: init() :: this._id cannot be null." );
        console.assert( this._go != null, "Component.ts :: init() :: this._go cannot be null." );
    }

    public end(): void {}

    public update( dt: number ): void {};

    // virtual.
    public setEnabled( isEnabled: boolean ): void
    {
        this._isEnabled = isEnabled;
    }

    // #endregion //
}