import Component from "./Component";

import * as PIXI from "pixi.js";


export default class CDampedSpring extends Component
{
    // #region Attributes //

    // private:

    private _springConstant: number = null;
    
    private _velocityFactor: number = null;
    private _target: PIXI.Point = new PIXI.Point();
    private _velocity: PIXI.Point = new PIXI.Point();
    private _isAwake: boolean = false;

    // Constants.
    private readonly _kIdealFrameRate: number = 1/30;

    // #endregion //


    // #region Properties //

    public get isAwake(): boolean { return this._isAwake; }

    public set springConstant( value: number ) { this._springConstant = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        this._id = "CDampedSpring";
    }

    public init(): void
    {
        super.init();

        console.assert( this._go.cContainer != null, "CDampedSpring.ts :: init() :: CContainer component not found." );
        console.assert( this._springConstant != null, "CDampedSpring.ts :: init() :: this._springConstant cannot be null." );
    
        this._velocityFactor = Math.sqrt( this._springConstant ) * 2;
        this._target.copyFrom( this._go.cContainer.c.position );
    }

    public end(): void
    {
        this._springConstant = null;
    }

    public update( dt: number ): void
    {
        if ( this._isAwake )
        {
            do
            {
                let dtAux: number = dt;
                if ( dtAux > this._kIdealFrameRate )
                {
                    dtAux = this._kIdealFrameRate;
                }

                const kTo: PIXI.Point = new PIXI.Point( this._target.x - this._go.cContainer.c.position.x, this._target.y - this._go.cContainer.c.position.y );
                const kSpringForce: PIXI.Point = new PIXI.Point( kTo.x * this._springConstant, kTo.y * this._springConstant );
                const kDampingForce: PIXI.Point = new PIXI.Point( -this._velocity.x * this._velocityFactor, -this._velocity.y * this._velocityFactor );
                
                this._velocity.x += ( kSpringForce.x + kDampingForce.x ) * dtAux;
                this._velocity.y += ( kSpringForce.y + kDampingForce.y ) * dtAux;
        
                this._go.cContainer.c.position.x += this._velocity.x * dtAux;
                this._go.cContainer.c.position.y += this._velocity.y * dtAux;
 
                dt -= dtAux;
            }
            while ( dt > 0 );

            this._isAwake = Math.round( this._target.x ) != Math.round( this._go.cContainer.c.position.x )
                || Math.round( this._target.y ) != Math.round( this._go.cContainer.c.position.y );
        }
    }

    public setTarget( target: PIXI.Point ): void
    {
        this._target.copyFrom( target );

        this._isAwake = true;
    }

    public reset(): void
    {
        this._velocity = new PIXI.Point();
    }

    // #endregion //
}