import { ISgCardDbEntry } from "../view/game/SaveGameView";
import ServiceLocator from "../ServiceLocator";
import Utils from "../Utils";


export default class CardDB
{
    // #region Attributes //

    // private:

    private _db: Map<string, Object> = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        this._db = new Map<string, ICard>( Object.entries( ServiceLocator.resourceStack.find( "game_db" ).data ) );
    }

    public find( cardId: string ): ICard
    {
        return this._db.get( cardId ) as ICard;
    }

    public findEntryByCardTitle( cardTitle: string, packName: string ): [string, ICard]
    {
        let result: [string, ICard] = null;
        let arrReserve: Array<[string, ICard]> = new Array<[string, ICard]>();

        let entriesIt: IterableIterator<[string, Object]> = this._db.entries();
        for ( let entriesItResult: IteratorResult<[string, Object]> = entriesIt.next(); !entriesItResult.done; entriesItResult = entriesIt.next() )
        {
            let card: ICard = entriesItResult.value[ 1 ] as ICard;
            if ( card.pack_name.toLowerCase() == packName.toLowerCase() )
            {
                let cardName: string = card.name.toLowerCase();
                cardTitle = cardTitle.toLowerCase();
                if ( cardName == cardTitle )
                {
                    result = entriesItResult.value;
                }

                if ( !result )
                {
                    cardName = cardName.normalize( "NFD" ).replace( /[\u0300-\u036f]/g, "" ).replace( /-/g, " " ).replace( /’/g, "'" );
                    cardTitle = cardTitle.normalize( "NFD" ).replace( /[\u0300-\u036f]/g, "" ).replace( /-/g, " " ).replace( /’/g, "'" );
                    if ( cardName == cardTitle )
                    {
                        result = entriesItResult.value;
                    }
                }

                if ( !result )
                {
                    let isValid: boolean = true;

                    const kArrCardNameToken: Array<string> = Utils.split( cardName, " " );
                    const kArrCardTitleToken: Array<string> = Utils.split( cardTitle, " " );
                    isValid = kArrCardNameToken.length > 0 && kArrCardNameToken.length == kArrCardTitleToken.length;
                    if ( isValid )
                    {
                        for ( let i: number = 0; i < kArrCardNameToken.length; ++i )
                        {
                            const kCardNameToken: string = kArrCardNameToken[ i ];
                            const kCardTitleToken: string = kArrCardTitleToken[ i ];
                            isValid = kCardNameToken.length == kCardTitleToken.length;
                            if ( isValid )
                            {
                                let failCount: number = 0;
                                for ( let j: number = 0; j < kCardNameToken.length; ++j )
                                {
                                    if ( kCardNameToken.charAt( j ) != kCardTitleToken.charAt( j ) )
                                    {
                                        failCount += 1;
                                    }

                                    if ( failCount > 2 )
                                    {
                                        isValid = false;
                                        break;
                                    }
                                }
                            }

                            if ( !isValid )
                            {
                                break;
                            }
                        }
                    }

                    if ( isValid )
                    {
                        arrReserve.push( entriesItResult.value );
                    }
                }

                if ( result )
                {
                    break;
                }
            }
        }

        if ( !result && arrReserve.length > 0 )
        {
            result = arrReserve[ 0 ];
        }

        return result;
    }

    public findByEncounterSet( encounterSets: Array<string> ): Array<ICard>
    {
        let result: Array<ICard> = new Array<ICard>();

        let entriesIt: IterableIterator<[string, Object]> = this._db.entries();
        for ( let entriesItResult: IteratorResult<[string, Object]> = entriesIt.next(); !entriesItResult.done; entriesItResult = entriesIt.next() )
        {
            let card: ICard = entriesItResult.value[ 1 ] as ICard;
            if ( encounterSets.indexOf( card.encounter_set ) != -1 )
            {
                result.push( card );
            }
        }

        return result;
    }

    public findByTypeCodes( typeCodes: Array<string> ): Array<ICard>
    {
        let result: Array<ICard> = new Array<ICard>();

        let entriesIt: IterableIterator<[string, Object]> = this._db.entries();
        for ( let entriesItResult: IteratorResult<[string, Object]> = entriesIt.next(); !entriesItResult.done; entriesItResult = entriesIt.next() )
        {
            let card: ICard = entriesItResult.value[ 1 ] as ICard;
            if ( typeCodes.indexOf( card.type_code ) != -1 )
            {
                result.push( card );
            }
        }

        return result;
    }

    public findBoons(): Array<ICard>
    {
        let result: Array<ICard> = new Array<ICard>();

        let entriesIt: IterableIterator<[string, Object]> = this._db.entries();
        for ( let entriesItResult: IteratorResult<[string, Object]> = entriesIt.next(); !entriesItResult.done; entriesItResult = entriesIt.next() )
        {
            let card: ICard = entriesItResult.value[ 1 ] as ICard;
            if ( card.is_boon )
            {
                result.push( card );
            }
        }

        return result;
    }

    public findBurdens(): Array<ICard>
    {
        let result: Array<ICard> = new Array<ICard>();

        let entriesIt: IterableIterator<[string, Object]> = this._db.entries();
        for ( let entriesItResult: IteratorResult<[string, Object]> = entriesIt.next(); !entriesItResult.done; entriesItResult = entriesIt.next() )
        {
            let card: ICard = entriesItResult.value[ 1 ] as ICard;
            if ( card.is_burden )
            {
                result.push( card );
            }
        }

        return result;
    }

    public has( cardId: string ): boolean
    {
        return this._db.has( cardId );
    }

    public add( cardId: string, cardInfo: ICard ): void
    {
        this._db.set( cardId, cardInfo );
    }

    public removeCustomCards(): void
    {
        let arrCustomCardId: Array<string> = new Array<string>();

        let entriesIt: IterableIterator<[string, Object]> = this._db.entries();
        for ( let entriesItResult: IteratorResult<[string, Object]> = entriesIt.next(); !entriesItResult.done; entriesItResult = entriesIt.next() )
        {
            const kCardId: string = entriesItResult.value[ 0 ] as string;
            if ( kCardId.indexOf( "custom_" ) == 0 )
            {
                arrCustomCardId.push( kCardId );
            }
        }

        for ( let customCardId of arrCustomCardId )
        {
            this._db.delete( customCardId );
        }
    }

    public saveGame(): Array<ISgCardDbEntry>
    {
        let result: Array<ISgCardDbEntry> = new Array<ISgCardDbEntry>();

        let entriesIt: IterableIterator<[string, Object]> = this._db.entries();
        for ( let entriesItResult: IteratorResult<[string, Object]> = entriesIt.next(); !entriesItResult.done; entriesItResult = entriesIt.next() )
        {
            let cardId: string = entriesItResult.value[ 0 ] as string;
            if ( cardId.indexOf( "custom_" ) == 0 )
            {
                result.push( { id: cardId, info: entriesItResult.value[ 1 ] as ICard } );
            }
        }

        return result;
    }

    public loadGame( customEntries: Array<ISgCardDbEntry>, pass: number ): void
    {
        if ( pass == 0 )
        {
            for ( let customEntry of customEntries )
            {
                this._db.set( customEntry.id, customEntry.info );
            }
        }
    }

    // #endregion //
}

export interface ICard
{
    pack_code: string;
    pack_name: string;
    is_official: boolean;
    type_code: string;
    type_name: string;
    sphere_code: string;
    sphere_name: string;
    encounter_set?: string;
    treasure_set?: string;
    position: number;
    code: string;
    name: string;
    traits: string;
    text: string;
    flavor: string;
    is_unique: boolean;
    victory?: number;
    engagement_cost?: string;
    threat_strength?: number;
    quest_points?: number;
    cost?: string;
    threat?: number;
    willpower?: number;
    attack?: number;
    defense?: number;
    health?: number;
    quantity: number;
    deck_limit: number;
    illustrator: string;
    octgnid: string;
    has_errata: boolean;
    url: string;
    imagesrc: string;
    side: string;
    is_boon?: boolean;
    is_burden?: boolean;
    texture_map_id: string;
}