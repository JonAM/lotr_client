import ServiceLocator from "../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "./GameObject";
import SignalBinding from "../lib/signals/SignalBinding";
import CAttackBindingCounter from "./component/ui/CAttackBindingCounter";
import CContainer from "./component/pixi/CContainer";
import { player_type } from "./component/world/CPlayerArea";
import ICharacterTokenSide from "./component/card/ICharacterTokenSide";
import { action_scope_type, player_action_type } from "../service/socket_io/GameSocketIOController";
import { ISgAttackBinding, ISgAttackBindingCounter, ISgAttackBindingManager } from "../view/game/SaveGameView";
import { status_type } from "./component/card/token/CCardTokenSide";
import Utils from "../Utils";
import { layer_type } from "./component/world/CGameLayerProvider";
import { detail_bar_icon_type } from "./component/card/token/CDetailBar";
import { location_type } from "./component/world/CGameWorld";


export default class AttackBindingManager
{
    // #region Attributes //

    // private:

    private _arrAttackBinding: Array<IAttackBinding> = null;
    private _mapAttackBindingCounter: Map<string, GameObject> = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor() {}

    public init(): void
    {
        this._arrAttackBinding = new Array<IAttackBinding>();
        this._mapAttackBindingCounter = new Map<string, GameObject>();
    }

    public end(): void
    {
        for ( let attackBinding of this._arrAttackBinding )
        {
            attackBinding.attacker.cContainer.onTransformChanged.remove( this.draw, this );
            attackBinding.attacker.cContainer.onVisibleChanged.remove( this.onAttackerVisible_Changed, this );
            attackBinding.defender.cContainer.onTransformChanged.remove( this.onDefenderTransform_Changed, this );
            attackBinding.defender.cContainer.onVisibleChanged.remove( this.onDefenderVisible_Changed, this );

            attackBinding.g.parent.removeChild( attackBinding.g );
        }
        this._arrAttackBinding = null;
        
        let entriesIt: IterableIterator<[string, GameObject]> = this._mapAttackBindingCounter.entries();
        for ( let entriesItResult: IteratorResult<[string, GameObject]> = entriesIt.next(); !entriesItResult.done; entriesItResult = entriesIt.next() )
        {
            let attackBindingCounter: GameObject = entriesItResult.value[ 1 ] as GameObject;
            attackBindingCounter.end();
        }
        this._mapAttackBindingCounter = null;
    }

    public findDefenderBindings( defender: GameObject ): Array<IAttackBinding>
    {
        let result: Array<IAttackBinding> = new Array<IAttackBinding>();

        for ( let attackBinding of this._arrAttackBinding )
        {
            if ( defender == attackBinding.defender )
            {
                result.push( attackBinding );
            }
        }

        return result;
    }

    public findBindingCounter( defender: GameObject ): GameObject
    {
        let result: GameObject = null;

        if ( this._mapAttackBindingCounter.has( defender.oid ) )
        {
            result = this._mapAttackBindingCounter.get( defender.oid );
        }

        return result;
    }

    public addBinding( attacker: GameObject, defender: GameObject, playerType: player_type, actionScope: action_scope_type ): void
    {
        console.assert( attacker.cCardToken != null, "AttackBindingManager.ts :: addBinding() :: attacker.cCardToken cannot be null." );
        console.assert( defender.cCardToken != null, "AttackBindingManager.ts :: addBinding() :: defender.cCardToken cannot be null." );

        let attackBinding: IAttackBinding = {
            attacker: attacker,
            defender: defender,
            g: new PIXI.Graphics() };
        this._arrAttackBinding.push( attackBinding );

        Utils.game.findGameProviderLayer( attacker, layer_type.ATTACK_BINDING_LINK ).cContainer.c.addChild( attackBinding.g );
        let sb: SignalBinding = attacker.cContainer.onTransformChanged.add( this.draw, this );
        sb.params = [ attackBinding ];
        sb = attacker.cContainer.onVisibleChanged.add( this.onAttackerVisible_Changed, this );
        sb.params = [ attackBinding ];
        sb = defender.cContainer.onTransformChanged.add( this.onDefenderTransform_Changed, this );
        sb.params = [ defender ];
        sb = defender.cContainer.onVisibleChanged.add( this.onDefenderVisible_Changed, this );
        sb.params = [ defender ];

        if ( !this._mapAttackBindingCounter.has( defender.oid ) )
        {
            let attackBindingCounter: GameObject = this.createAttackBindingCounter( defender, attacker, playerType );
            Utils.game.findGameProviderLayer( defender, layer_type.ATTACK_BINDING_COUNTER ).cContainer.addChild( attackBindingCounter );
            
            this._mapAttackBindingCounter.set( defender.oid, attackBindingCounter );
        }
        else
        {
            this._mapAttackBindingCounter.get( defender.oid ).cAttackBindingCounter.addAttacker( attacker );
        }

        this.draw( attackBinding );

        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ADD_ATTACK_BINDING, null, [ attacker.oid, defender.oid, playerType ^ 1 ] );
        }
    }

        private createAttackBindingCounter( defender: GameObject, attacker: GameObject, playerType: player_type ): GameObject
        {
            let result: GameObject = new GameObject( [ new CContainer(), new CAttackBindingCounter() ] );
            result.oid = defender.oid + "_attack_binding_counter";
            result.cAttackBindingCounter.defender = defender;
            result.cAttackBindingCounter.attacker = attacker;
            result.cAttackBindingCounter.playerType = playerType;
            result.cAttackBindingCounter.onAttackCompleted.add( this.onAttack_Completed, this );
            result.cAttackBindingCounter.onAttackCanceled.add( this.onAttack_Canceled, this );
            result.init();
            
            return result;
        }

    public removeAllDefenderBindings( defender: GameObject, actionScope: action_scope_type ): void
    {
        let isFound: boolean = false;
        for ( let i: number = this._arrAttackBinding.length - 1; i >= 0; --i )
        {
            let attackBinding: IAttackBinding = this._arrAttackBinding[ i ];
            if ( attackBinding.defender == defender )
            {
                attackBinding.attacker.cContainer.onTransformChanged.remove( this.draw, this );
                attackBinding.attacker.cContainer.onVisibleChanged.remove( this.onAttackerVisible_Changed, this );

                attackBinding.g.parent.removeChild( attackBinding.g );

                this._arrAttackBinding.splice( i, 1 );

                isFound = true;
            }
        }

        if ( isFound )
        {
            defender.cContainer.onTransformChanged.remove( this.onDefenderTransform_Changed, this );
            defender.cContainer.onVisibleChanged.remove( this.onDefenderVisible_Changed, this );

            this._mapAttackBindingCounter.get( defender.oid ).end();
            this._mapAttackBindingCounter.delete( defender.oid );
        }

        // Multiplayer.
        if ( actionScope == action_scope_type.MULTIPLAYER )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.REMOVE_DEFENDER_BINDINGS, null, [ defender.oid ] );
        }
    }

    public removeAllAttackerBindings( attacker: GameObject, actionScope: action_scope_type ): void
    {
        let defender: GameObject = null;
        for ( let i: number = this._arrAttackBinding.length - 1; i >= 0; --i )
        {
            let attackBinding: IAttackBinding = this._arrAttackBinding[ i ];
            if ( attackBinding.attacker == attacker )
            {
                defender = attackBinding.defender;

                attackBinding.attacker.cContainer.onTransformChanged.remove( this.draw, this );
                attackBinding.attacker.cContainer.onVisibleChanged.remove( this.onAttackerVisible_Changed, this );

                attackBinding.g.parent.removeChild( attackBinding.g );

                this._arrAttackBinding.splice( i, 1 );
                break;
            }
        }

        if ( defender != null )
        {
            let isDefender: boolean = false;
            for ( let attackBinding of this._arrAttackBinding )
            {
                if ( attackBinding.defender == defender )
                {
                    isDefender = true;
                    break;
                }
            }
            if ( !isDefender )
            {
                defender.cContainer.onTransformChanged.remove( this.onDefenderTransform_Changed, this );
                defender.cContainer.onVisibleChanged.remove( this.onDefenderVisible_Changed, this );

                this._mapAttackBindingCounter.get( defender.oid ).end();
                this._mapAttackBindingCounter.delete( defender.oid );
            }
            else
            {
                this._mapAttackBindingCounter.get( defender.oid ).cAttackBindingCounter.removeAttacker( attacker );
            }

            // Multiplayer.
            if ( actionScope == action_scope_type.MULTIPLAYER )
            {
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.REMOVE_ATTACKER_BINDINGS, null, [ attacker.oid ] );
            }
        }
    }

    public removeAllBindings( attackerOrDefender: GameObject, actionScope: action_scope_type ): void
    {
        this.removeAllAttackerBindings( attackerOrDefender, actionScope );
        this.removeAllDefenderBindings( attackerOrDefender, actionScope );
    }

    public saveGame(): ISgAttackBindingManager
    {
        let result: ISgAttackBindingManager = {
            attackBindings: new Array<ISgAttackBinding>(),
            mapAttackBindingCounter: new Array<[string, ISgAttackBindingCounter]>() };

        for ( let attackBinding of this._arrAttackBinding )
        {
            result.attackBindings.push( { 
                attackerOid: attackBinding.attacker.oid,
                defenderOid: attackBinding.defender.oid,
                isVisible: attackBinding.g.visible } );
        }

        let entriesIt: IterableIterator<[string, GameObject]> = this._mapAttackBindingCounter.entries();
        for ( let entriesItResult: IteratorResult<[string, GameObject]> = entriesIt.next(); !entriesItResult.done; entriesItResult = entriesIt.next() )
        {
            result.mapAttackBindingCounter.push( [
                entriesItResult.value[ 0 ] as string, 
                ( entriesItResult.value[ 1 ] as GameObject ).cAttackBindingCounter.saveGame() ] );
        }

        return result; 
    }

    public loadGame( sgAttackBindingManager: ISgAttackBindingManager, pass: number ): void
    {
        if ( pass == 0 )
        {
            for ( let attackBinding of sgAttackBindingManager.attackBindings )
            {
                let playerType: player_type = null;
                for ( let mapEntry of sgAttackBindingManager.mapAttackBindingCounter )
                {
                    if ( mapEntry[ 0 ] == attackBinding.defenderOid )
                    {
                        playerType = mapEntry[ 1 ].playerType;
                        break;
                    }
                }
                this.addBinding( GameObject.find( attackBinding.attackerOid ), GameObject.find( attackBinding.defenderOid ), playerType, action_scope_type.LOCAL );
                this._arrAttackBinding[ this._arrAttackBinding.length - 1 ].g.visible = attackBinding.isVisible;
            }

            for ( let mapEntry of sgAttackBindingManager.mapAttackBindingCounter )
            {
                let attackBindingCounter: GameObject = this._mapAttackBindingCounter.get( mapEntry[ 0 ] as string );
                attackBindingCounter.cAttackBindingCounter.loadGame( mapEntry[ 1 ] as ISgAttackBindingCounter, pass );
            }
        }
    }

    // private:

    private draw( attackBinding: IAttackBinding ): void
    {
        attackBinding.g.clear();
        const kGameLayerProvider: GameObject = Utils.game.findGameObjectInBranchByComponentName( attackBinding.attacker, "CGameLayerProvider" );
        const kFrom: PIXI.IPoint = kGameLayerProvider.cContainer.c.toLocal( attackBinding.attacker.cCardToken.curSide.cTargetReceptor.targetSocket.cContainer.c.getGlobalPosition() );
        const kTo: PIXI.IPoint = kGameLayerProvider.cContainer.c.toLocal( attackBinding.defender.cCardToken.curSide.cTargetReceptor.targetSocket.cContainer.c.getGlobalPosition() );
        let color: number = ServiceLocator.game.playerColors[ attackBinding.attacker.cCardToken.controllerPlayer ];
        if ( attackBinding.attacker.cCardToken.ownerPlayer == player_type.SAURON )
        {
            color = ServiceLocator.game.playerColors[ player_type.SAURON ];
        }
        attackBinding.g.lineStyle( 10, color );
        attackBinding.g.moveTo( kFrom.x, kFrom.y );
        attackBinding.g.lineTo( kTo.x, kTo.y );
        attackBinding.g.beginFill( color );
        attackBinding.g.drawCircle( kFrom.x, kFrom.y, 10 );
        attackBinding.g.endFill();

        let attackBindingCounter: GameObject = this._mapAttackBindingCounter.get( attackBinding.defender.oid );
        attackBindingCounter.cContainer.c.position.set( kTo.x, kTo.y );
    }

    // #endregion //


    // #region Callbacks //

    private onDefenderTransform_Changed( defender: GameObject ): void
    {
        for ( let attackBinding of this._arrAttackBinding )
        {
            if ( attackBinding.defender == defender )
            {
                this.draw( attackBinding );
            }
        }
    }

    private onDefenderVisible_Changed( defender: GameObject, isVisible: boolean ): void
    {
        for ( let attackBinding of this._arrAttackBinding )
        {
            if ( attackBinding.defender == defender )
            {
                attackBinding.g.visible = attackBinding.attacker.cContainer.c.visible && isVisible;
            }
        }

        this._mapAttackBindingCounter.get( defender.oid ).cContainer.c.visible = isVisible;
    }

    private onAttackerVisible_Changed( attackBinding: IAttackBinding, isVisible: boolean ): void
    {
        attackBinding.g.visible = isVisible && attackBinding.defender.cContainer.c.visible;
    }

    private onAttack_Completed( woundCount: number, defender: GameObject, arrAttacker: Array<GameObject>, isDefended: boolean ): void
    {
        let defCharacterTokenSide: ICharacterTokenSide = defender.cCardToken.cCurSide as unknown as ICharacterTokenSide;

        // Sfx.
        if ( woundCount <= 0 )
        {
            ServiceLocator.audioManager.playSfx( "attack_blocked" );
        }
        else if ( !defender.cCardToken.cCurSide.hasStatus( status_type.DEAD ) 
            && woundCount + defCharacterTokenSide.wound.cTokenCounter.count < defCharacterTokenSide.health.cTokenCounter.count )
        {
            ServiceLocator.audioManager.playSfx( "wounded" );
        }
        
        if ( arrAttacker.length == 1 && arrAttacker[ 0 ].cCardToken.cDetailBar.findItem( detail_bar_icon_type.PHANTOM ) )
        {
            const kLocationType: location_type = defender.cCardToken.controllerPlayer == player_type.PLAYER ? location_type.MY_THREAT_LEVEL : location_type.ALLY_THREAT_LEVEL;
            ServiceLocator.game.cGameWorld.findLocation( kLocationType ).cTokenIndicator.trigger.cTokenCounter.addCount( woundCount, action_scope_type.MULTIPLAYER );
        }
        else
        {
            defCharacterTokenSide.wound.cTokenCounter.addCount( woundCount, action_scope_type.MULTIPLAYER );
        }

        if ( defender.cCardToken.curSide.cEnemySide )
        {
            for ( let attacker of arrAttacker )
            {
                attacker.cCardToken.setBowed( true, action_scope_type.MULTIPLAYER );
                attacker.cCardToken.cCurSide.addStatus( status_type.HAS_ATTACKED, action_scope_type.MULTIPLAYER );
            }
        }
        else
        {
            for ( let attacker of arrAttacker )
            {
                attacker.cCardToken.cCurSide.addStatus( status_type.HAS_ATTACKED, action_scope_type.MULTIPLAYER );
            }
            if ( isDefended )
            {
                defender.cCardToken.setBowed( true, action_scope_type.MULTIPLAYER );
            }
        }

        this.removeAllDefenderBindings( defender, action_scope_type.MULTIPLAYER );
    }

    private onAttack_Canceled( defender: GameObject ): void
    {
        this.removeAllDefenderBindings( defender, action_scope_type.MULTIPLAYER );
    }

    // #endregion //
}

export interface IAttackBinding
{
    attacker: GameObject;
    defender: GameObject;
    g: PIXI.Graphics;
}