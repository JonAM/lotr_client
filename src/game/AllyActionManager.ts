import { game_state_id, main_state_id } from "../states/StateId";
import { gsap } from "gsap";
import { action_scope_type, player_action_type } from "../service/socket_io/GameSocketIOController";
import { player_type } from "./component/world/CPlayerArea";
import ServiceLocator from "../ServiceLocator";
import Utils from "../Utils";
import * as PIXI from "pixi.js";

import State from "../lib/state_machine/State";
import Signal from "../lib/signals/Signal";
import GameObject from "./GameObject";
import CDropArea from "./component/input/CDropArea";
import CardViewerController from "./component/ui/viewer/controller/CardViewerController";
import { detail_bar_icon_type, IDetailBarItem } from "./component/card/token/CDetailBar";
import ProcScrollToTarget from "./process_sequencer/ProcScrollToTarget";
import ProcBumpFromCorner from "./process_sequencer/ProcBumpFromCorner";
import ProcFlash from "./process_sequencer/ProcFlash";
import { location_type } from "./component/world/CGameWorld";
import CViewer from "./component/ui/CViewer";
import CEndTurnButton, { end_turn_btn_state } from "./component/ui/CEndTurnButton";
import InfoMessageView, { btn_layout_type } from "../view/common/popup/InfoMessageView";
import { view_layer_id } from "../service/ViewManager";
import View from "../view/View";
import SignalBinding from "../lib/signals/SignalBinding";
import PhaseIntroState from "../states/game/PhaseIntroState";
import VictoryView from "../view/game/VictoryView";
import SaveGameView, { ISgPlayerWillpowerCounter } from "../view/game/SaveGameView";
import AllyCardActivationController from "./component/world/card_activation_area/AllyCardActivationController";
import Session from "../Session";
import { status_type } from "./component/card/token/CCardTokenSide";
import { layer_type } from "./component/world/CGameLayerProvider";
import CSauronArea from "./component/world/CSauronArea";
import CIsolatedArea from "./component/world/custom_area/CIsolatedArea";


export default class AllyActionManager
{
    // #region Attributes //

    // private:

    private _isStreamMode: boolean = false;

    private _arrAction: Array<IOpponentAction> = null;
    private _isPaused: boolean = false;
    private _mapListener: Map<player_action_type, Array<IOppActionListener>> = null;

    // Binded functions.
    private _bfOnDocumentKeyUp: ( ev: KeyboardEvent) => any = null;

    // Signals.
    private _onFlushEnded: Signal = new Signal();

    // #endregion //


    // #region Properties //
    
    public get isStreamMode(): boolean { return this._isStreamMode; }
    public get isPaused(): boolean { return this._isPaused; }

    public set isStreamMode( value: boolean ) { this._isStreamMode = value; }

    // Signals.
    public get onFlushEnded(): Signal { return this._onFlushEnded; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        this._bfOnDocumentKeyUp = this.onDocument_KeyUp.bind( this );
    }

    public init(): void 
    {
        this._arrAction = new Array<IOpponentAction>();
        this._mapListener = new Map<player_action_type, Array<IOppActionListener>>();

        if ( this._isStreamMode )
        {
            document.addEventListener( "keyup", this._bfOnDocumentKeyUp );
        }
    }

    public end(): void
    {
        this._onFlushEnded.removeAll();

        this._arrAction = null;
        this._mapListener = null;

        if ( this._isStreamMode )
        {
            document.removeEventListener( "keyup", this._bfOnDocumentKeyUp );
        }
    }

    public update( dt: number ): void
    {
        if ( !this._isPaused 
            && !ServiceLocator.game.processSequencer.isBusy( "main" )
            && this._arrAction.length > 0 
            && ( this._arrAction[ 0 ].gameStateId == null
                || this._arrAction[ 0 ].gameStateId == ServiceLocator.game.stateMachine.currentStateId ) )
        {
            this.processAction( this._arrAction.shift() );
        }
    }

    public addAction( playerActionType: player_action_type, gameStateId: game_state_id, args: Array<IOpponentAction> ): void
    {
        this._arrAction.push( { playerActionType: playerActionType, gameStateId: gameStateId, args: args } );
    }

    public flush(): void
    {
        ServiceLocator.game.isAnimated = false;
        window.setTimeout( this.flushAction.bind( this ) );
    }

        private flushAction(): void
        {
            if ( this._arrAction.length > 0 )
            {
                this.processAction( this._arrAction.shift() );
                while ( this.flushTweensAndTimelines() ) {}

                window.setTimeout( this.flushAction.bind( this ) );
            }
            else
            {
                ServiceLocator.game.isAnimated = true;
                this._onFlushEnded.dispatch();
            }
        }

            private flushTweensAndTimelines(): boolean
            {
                let isFlush: boolean = false;

                let arrTween: Array<gsap.core.Tween | gsap.core.Timeline> = gsap.globalTimeline.getChildren( false, true, false );
                for ( let tween of arrTween )
                {
                    if ( !tween.vars.repeat || tween.vars.repeat != -1 )
                    {
                        isFlush = true;
                        Utils.anim.flushTween( tween as gsap.core.Tween );
                    }
                }
                let arrTimeline: Array<gsap.core.Tween | gsap.core.Timeline> = gsap.globalTimeline.getChildren( false, false, true );
                for ( let timeline of arrTimeline )
                {
                    isFlush = true;
                    timeline.totalProgress( 1, false );
                }

                return isFlush;
            }

    public pause(): void
    {
        this._isPaused = true;
    }

    public resume(): void
    {
        this._isPaused = false;
    }

    public addListener( listener: IOppActionListener, arrPlayerActionType: Array<player_action_type> ): void
    {
        for ( let playerActionType of arrPlayerActionType )
        {
            if ( !this._mapListener.has( playerActionType ) )
            {
                this._mapListener.set( playerActionType, [ listener ] );
            }
            else
            {
                let arrListener: Array<IOppActionListener> = this._mapListener.get( playerActionType );
                console.assert( arrListener.indexOf( listener ) == -1, "AllyActionManager.ts :: addListener() :: duplicated value." );
                arrListener.push( listener );
            }
        }
    }

    public removeListener( listener: IOppActionListener, arrPlayerActionType: Array<player_action_type> ): void
    {
        for ( let playerActionType of arrPlayerActionType )
        {
            if ( this._mapListener.has( playerActionType ) )
            {
                let arrListener: Array<IOppActionListener> = this._mapListener.get( playerActionType );
                const kListenerIndex: number = arrListener.indexOf( listener );
                if ( kListenerIndex != -1 )
                {
                    arrListener.splice( arrListener.indexOf( listener ), 1 );
                }
            }
        }
    }

    // private:

    private processAction( action: IOpponentAction ): void
    {
        switch ( action.playerActionType )
        {
            case player_action_type.SET_CARD_FACE_UP:
                {
                    let card: GameObject = GameObject.find( action.args[ 0 ] as string );
                    card.cCard.setFaceUp( action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.SET_CARD_FACE_DOWN:
                {
                    let card: GameObject = GameObject.find( action.args[ 0 ] as string );
                    card.cCard.setFaceDown( action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.SET_CARD_TOKEN_FACE_UP:
                {
                    let cardToken: GameObject = GameObject.find( action.args[ 0 ] as string );
                    cardToken.cCardToken.setFaceUp( action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.SET_CARD_TOKEN_FACE_DOWN:
                {
                    let cardToken: GameObject = GameObject.find( action.args[ 0 ] as string );
                    cardToken.cCardToken.setFaceDown( action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.BOW_CARD_TOKEN:
                {
                    let cardToken: GameObject = GameObject.find( action.args[ 0 ] as string );
                    cardToken.cCardToken.setBowed( true, action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.READY_CARD_TOKEN:
                {
                    let cardToken: GameObject = GameObject.find( action.args[ 0 ] as string );
                    cardToken.cCardToken.setBowed( false, action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.COMMIT_TO_QUEST:
                {
                    let cardTokenSide: GameObject = GameObject.find( action.args[ 0 ] as string );
                    if ( cardTokenSide.cHeroSide )
                    {
                        cardTokenSide.cHeroSide.commitToQuest( action_scope_type.LOCAL );
                    }
                    else if ( cardTokenSide.cAllySide )
                    {
                        cardTokenSide.cAllySide.commitToQuest( action_scope_type.LOCAL );
                    }
                    break;
                }

            case player_action_type.UNCOMMIT_FROM_QUEST:
                {
                    let cardTokenSide: GameObject = GameObject.find( action.args[ 0 ] as string );
                    if ( cardTokenSide.cHeroSide )
                    {
                        cardTokenSide.cHeroSide.uncommitFromQuest( action_scope_type.LOCAL );
                    }
                    else if ( cardTokenSide.cAllySide )
                    {
                        cardTokenSide.cAllySide.uncommitFromQuest( action_scope_type.LOCAL );
                    }
                    break;
                }

            case player_action_type.TRIGGER_ABILITY:
                {
                    let cardToken: GameObject = GameObject.find( action.args[ 0 ] as string ).cCardTokenSide.cardToken;
                    //Utils.anim.bumpFromCorner( cardToken.cContainer.c );
                    const kVfxLayer: GameObject = Utils.game.findGameProviderLayer( cardToken, layer_type.VFX );
                    let flashPos: PIXI.Point = new PIXI.Point().copyFrom( kVfxLayer.cContainer.c.toLocal( cardToken.cContainer.c.getGlobalPosition() ) );
                    flashPos.x += cardToken.cCardToken.calcWidth() * 0.5;
                    flashPos.y += cardToken.cCardToken.calcHeight() * 0.5;
                    Utils.anim.flash( flashPos, cardToken.cCardToken.calcWidth(), cardToken.cCardToken.calcHeight(), 1.5, Utils.game.findGameProviderLayer( cardToken, layer_type.VFX ) );
                    
                    cardToken.cCardToken.cDetailBar.addToItemCount( detail_bar_icon_type.ABILITY_USES, 1 );
                    break;
                }

            case player_action_type.ADD_CARD_TOKEN_STATUS:
                {
                    let cardTokenSide: GameObject = GameObject.find( action.args[ 0 ] as string );
                    cardTokenSide.cCardTokenSide.addStatus( action.args[ 1 ] as status_type, action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.REMOVE_CARD_TOKEN_STATUS:
                {
                    let cardTokenSide: GameObject = GameObject.find( action.args[ 0 ] as string );
                    cardTokenSide.cCardTokenSide.removeStatus( action.args[ 1 ] as status_type, action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.ADD_CARD_TOKEN_HIGHLIGHT:
                {
                    let cardToken: GameObject = GameObject.find( action.args[ 0 ] as string );
                    cardToken.cCardToken.setHighlighted( true, action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.REMOVE_CARD_TOKEN_HIGHLIGHT:
                {
                    let cardToken: GameObject = GameObject.find( action.args[ 0 ] as string );
                    cardToken.cCardToken.setHighlighted( false, action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.ADD_CARD_UNDERNEATH:
                {
                    let target: GameObject = GameObject.find( action.args[ 0 ] as string );
                    let added: GameObject = GameObject.find( action.args[ 1 ] as string );
                    if ( target.cCardToken )
                    {
                        target.cCardToken.addCardUnderneath( added, action_scope_type.LOCAL );
                    }
                    else if ( target.cGameModifier )
                    {
                        target.cGameModifier.addCardUnderneath( added, action_scope_type.LOCAL );
                    }
                    break;
                }

            case player_action_type.ADD_SHADOW_CARD:
                {
                    let target: GameObject = GameObject.find( action.args[ 0 ] as string );
                    let added: GameObject = GameObject.find( action.args[ 1 ] as string );
                    target.cCardToken.curSide.cEnemySide.addShadowCard( added, action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.SET_SHADOW_CARD_FACE_UP:
                {
                    let shadowCard: GameObject = GameObject.find( action.args[ 0 ] as string );
                    shadowCard.cShadowCardMini.setFaceUp( action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.SET_SHADOW_CARD_FACE_DOWN:
                {
                    let shadowCard: GameObject = GameObject.find( action.args[ 0 ] as string );
                    shadowCard.cShadowCardMini.setFaceDown( action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.DISCARD_SHADOW_CARD:
                {
                    let shadowCard: GameObject = GameObject.find( action.args[ 0 ] as string );
                    shadowCard.cShadowCardMini.discard( action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.BOW_ATTACHMENT_TOKEN:
                {
                    let attachment: GameObject = GameObject.find( action.args[ 0 ] as string );
                    attachment.cAttachment.setBowed( true, action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.READY_ATTACHMENT_TOKEN:
                {
                    let attachment: GameObject = GameObject.find( action.args[ 0 ] as string );
                    attachment.cAttachment.setBowed( false, action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.SET_ATTACHMENT_FACE_UP:
                {
                    let attachment: GameObject = GameObject.find( action.args[ 0 ] as string );
                    attachment.cAttachment.setFaceUp( action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.SET_ATTACHMENT_FACE_DOWN:
                {
                    let attachment: GameObject = GameObject.find( action.args[ 0 ] as string );
                    attachment.cAttachment.setFaceDown( action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.ADD_ATTACHMENT_HIGHLIGHT:
                {
                    let attachment: GameObject = GameObject.find( action.args[ 0 ] as string );
                    attachment.cAttachment.setHighlighted( true, action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.REMOVE_ATTACHMENT_HIGHLIGHT:
                {
                    let attachment: GameObject = GameObject.find( action.args[ 0 ] as string );
                    attachment.cAttachment.setHighlighted( false, action_scope_type.LOCAL );
                    break;
                }
                
            case player_action_type.BOW_GAME_MODIFIER:
                {
                    let gameModifier: GameObject = GameObject.find( action.args[ 0 ] as string );
                    gameModifier.cGameModifier.setBowed( true, action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.READY_GAME_MODIFIER:
                {
                    let gameModifier: GameObject = GameObject.find( action.args[ 0 ] as string );
                    gameModifier.cGameModifier.setBowed( false, action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.SET_GAME_MODIFIER_FACE_UP:
                {
                    let gameModifier: GameObject = GameObject.find( action.args[ 0 ] as string );
                    gameModifier.cGameModifier.setFaceUp( action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.SET_GAME_MODIFIER_FACE_DOWN:
                {
                    let gameModifier: GameObject = GameObject.find( action.args[ 0 ] as string );
                    gameModifier.cGameModifier.setFaceDown( action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.ADD_GAME_MODIFIER_HIGHLIGHT:
                {
                    let gameModifier: GameObject = GameObject.find( action.args[ 0 ] as string );
                    gameModifier.cGameModifier.setHighlighted( true, action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.REMOVE_GAME_MODIFIER_HIGHLIGHT:
                {
                    let gameModifier: GameObject = GameObject.find( action.args[ 0 ] as string );
                    gameModifier.cGameModifier.setHighlighted( false, action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.USE_GAME_MODIFIER:
                {
                    let gameModifier: GameObject = GameObject.find( action.args[ 0 ] as string );
                    if ( ServiceLocator.game.isAnimated )
                    {
                        gameModifier.cGameModifier.flash();
                    }
                    gameModifier.cGameModifier.cDetailBar.addToItemCount( detail_bar_icon_type.ABILITY_USES, 1 );
                    break;
                }

            case player_action_type.SET_CUSTOM_TEXT:
                {
                    let go: GameObject = GameObject.find( action.args[ 0 ] as string );
                    if ( go.cCardToken )
                    {
                        go.cCardToken.cDetailBar.setCustomText( action.args[ 1 ] as string, action_scope_type.LOCAL );
                    }
                    else if ( go.cAttachment )
                    {
                        go.cAttachment.cDetailBar.setCustomText( action.args[ 1 ] as string, action_scope_type.LOCAL );
                    }
                    else if ( go.cGameModifier )
                    {
                        go.cGameModifier.cDetailBar.setCustomText( action.args[ 1 ] as string, action_scope_type.LOCAL );
                    }
                    break;
                }

            case player_action_type.REMOVE_CUSTOM_TEXT:
                {
                    let go: GameObject = GameObject.find( action.args[ 0 ] as string );
                    if ( go.cCardToken )
                    {
                        go.cCardToken.cDetailBar.removeItem( detail_bar_icon_type.CUSTOM_TEXT );
                    }
                    else if ( go.cAttachment )
                    {
                        go.cAttachment.cDetailBar.removeItem( detail_bar_icon_type.CUSTOM_TEXT );
                    }
                    else if ( go.cGameModifier )
                    {
                        go.cGameModifier.cDetailBar.removeItem( detail_bar_icon_type.CUSTOM_TEXT );
                    }
                    break;
                }

            case player_action_type.ADD_CUSTOM_COUNTER:
                {
                    let go: GameObject = GameObject.find( action.args[ 0 ] as string );
                    if ( go.cCardToken )
                    {
                        go.cCardToken.cDetailBar.addItem( action.args[ 1 ], action.args[ 2 ], action.args[ 3 ], action.args[ 4 ] );
                    }
                    else if ( go.cAttachment )
                    {
                        go.cAttachment.cDetailBar.addItem( action.args[ 1 ], action.args[ 2 ], action.args[ 3 ], action.args[ 4 ] );
                    }
                    else if ( go.cGameModifier )
                    {
                        go.cGameModifier.cDetailBar.addItem( action.args[ 1 ], action.args[ 2 ], action.args[ 3 ], action.args[ 4 ] );
                    }
                    break;
                }

            case player_action_type.REMOVE_DETAIL_BAR_ITEM:
                {
                    let go: GameObject = GameObject.find( action.args[ 0 ] as string );
                    if ( go.cCardToken )
                    {
                        go.cCardToken.cDetailBar.removeItem( action.args[ 1 ] );
                    }
                    else if ( go.cAttachment )
                    {
                        go.cAttachment.cDetailBar.removeItem( action.args[ 1 ] );
                    }
                    else if ( go.cGameModifier )
                    {
                        go.cGameModifier.cDetailBar.removeItem( action.args[ 1 ] );
                    }
                    break;
                }

            case player_action_type.SET_DRAG_FILTER:
                {
                    let dragged: GameObject = GameObject.find( action.args[ 0 ] as string );
                    if ( dragged )
                    {
                        dragged.cDraggable.setDragFilter();
                    }
                    break;
                }

            case player_action_type.REMOVE_DRAG_FILTER:
                {
                    let dragged: GameObject = GameObject.find( action.args[ 0 ] as string );
                    if ( dragged )
                    {
                        dragged.cDraggable.removeDragFilter();
                    }
                    break;
                }

            case player_action_type.ADD_ATTACK_BINDING:
                {
                    ServiceLocator.game.attackBindingManager.addBinding( 
                        GameObject.find( action.args[ 0 ] as string ), 
                        GameObject.find( action.args[ 1 ] as string ), 
                        action.args[ 2 ] as player_type, action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.REMOVE_ATTACKER_BINDINGS:
                {
                    ServiceLocator.game.attackBindingManager.removeAllAttackerBindings( 
                        GameObject.find( action.args[ 0 ] as string ), 
                        action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.REMOVE_DEFENDER_BINDINGS:
                {
                    ServiceLocator.game.attackBindingManager.removeAllDefenderBindings( 
                        GameObject.find( action.args[ 0 ] as string ), 
                        action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.ACTION_REQUESTED:
                {
                    let actionRequestView: InfoMessageView = new InfoMessageView();
                    actionRequestView.alias = "actionRequestView";
                    let message: string = null;
                    switch ( action.args[ 0 ] as action_request_type )
                    {
                        case action_request_type.RESTART_GAME: { message = jQuery.i18n( "RESTART_GAME_REQUEST" ); break; }
                        case action_request_type.JUMP_TO_GAME_PHASE: { message = jQuery.i18n( "JUMP_TO_GAME_PHASE_REQUEST" ).replace( "#", jQuery.i18n( action.args[ 2 ] as string ) ); break; }
                        case action_request_type.GO_TO_VICTORY_SCREEN: { message = jQuery.i18n( "GO_TO_VICTORY_SCREEN_REQUEST" ); break; }
                    }
                    actionRequestView.message = message;
                    actionRequestView.btnLayoutType = btn_layout_type.DOUBLE;
                    actionRequestView.init();
                    let sb: SignalBinding = actionRequestView.onClosed.addOnce( this.onActionRequestPopup_Closed, this );
                    sb.params = [ action ];
                    ServiceLocator.viewManager.fadeIn( actionRequestView, view_layer_id.POPUP );
                    break;
                }

            case player_action_type.ACTION_REQUEST_CANCELLED:
                {
                    let arrActionRequestView: Array<View> = ServiceLocator.viewManager.findViewByAlias( "actionRequestView" );
                    if ( arrActionRequestView.length > 0 )
                    {
                        ServiceLocator.viewManager.fadeOut( arrActionRequestView[ 0 ] );
                    }
                    break;
                }

            case player_action_type.ACTION_REQUEST_REJECTED:
                {
                    let arrActionRequestView: Array<View> = ServiceLocator.viewManager.findViewByAlias( "waitForAllyConfirmationView" );
                    if ( arrActionRequestView.length > 0 )
                    {
                        ServiceLocator.viewManager.fadeOut( arrActionRequestView[ 0 ] );
                    }
                    break;
                }

            case player_action_type.ACTION_REQUEST_ACCEPTED:
                {
                    let arrActionRequestView: Array<View> = ServiceLocator.viewManager.findViewByAlias( "waitForAllyConfirmationView" );
                    if ( arrActionRequestView.length > 0 )
                    {
                        ServiceLocator.viewManager.clearLayer( view_layer_id.POPUP );
                        this.processActionRequest( action.args );
                    }
                    break;
                }

            case player_action_type.CARD_ACTIVATION_PRESENT:
                {
                    ServiceLocator.game.onOngoingInteractionCanceled.dispatch();

                    let acac: AllyCardActivationController = new AllyCardActivationController();
                    acac.card = GameObject.find( action.args[ 0 ] as string );
                    acac.presentFrom = Utils.game.transformToAllyLocation( action.args[ 1 ] as location_type );
                    acac.init();
                    acac.onCompleted.addOnce( () => { acac.end(); } );
                    break;
                }

            case player_action_type.LOCK_GAME_ELEMENT:
                {
                    let go: GameObject = GameObject.find( action.args[ 0 ] as string );
                    if ( go ) // IMPORTANT: Tentative fix for bug reported by Cuarzo.
                    {
                        go.cShareableGameElement.lock();
                    }
                    break;
                }

            case player_action_type.UNLOCK_GAME_ELEMENT:
                {
                    let go: GameObject = GameObject.find( action.args[ 0 ] as string );
                    if ( go ) // IMPORTANT: Tentative fix for bug reported by Cuarzo.
                    {
                        go.cShareableGameElement.unlock();
                    }
                    break;
                }

            case player_action_type.USE_ATTACHMENT:
                {
                    let attachment: GameObject = GameObject.find( action.args[ 0 ] as string );
                    if ( ServiceLocator.game.isAnimated )
                    {
                        let procScrollToTarget: ProcScrollToTarget = new ProcScrollToTarget();
                        procScrollToTarget.target = attachment;
                        procScrollToTarget.init();
                        ServiceLocator.game.processSequencer.add( [ procScrollToTarget ], "use_attachment-" + attachment.oid.toString() );
                        //
                        let procBumpFromCorner: ProcBumpFromCorner = new ProcBumpFromCorner();
                        procBumpFromCorner.target = attachment;
                        procBumpFromCorner.init();
                        //
                        let procFlash: ProcFlash = new ProcFlash();
                        procFlash.target = attachment;
                        procFlash.width = attachment.cContainer.c.width;
                        procFlash.height = attachment.cContainer.c.height;
                        procFlash.scale = 1.5;
                        procFlash.init();
                        ServiceLocator.game.processSequencer.add( [ procBumpFromCorner, procFlash ], "use_attachment-" + attachment.oid.toString() );
                    }
                    attachment.cAttachment.cDetailBar.addToItemCount( detail_bar_icon_type.ABILITY_USES, 1 );
                    break;
                }

            case player_action_type.ADD_TO_TOKEN_COUNTER:
                {
                    let tokenCounter: GameObject = GameObject.find( action.args[ 0 ] as string );
                    tokenCounter.cTokenCounter.addCount( action.args[ 1 ] as number, action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.SET_TOKEN_COUNTER:
                {
                    let tokenCounter: GameObject = GameObject.find( action.args[ 0 ] as string );
                    tokenCounter.cTokenCounter.setCount( action.args[ 1 ] as number, action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.DROP_AT_DROP_AREA:
                {
                    let dropped: GameObject = GameObject.find( action.args[ 0 ] as string );
                    dropped.cContainer.c.visible = true;
                    let dropArea: GameObject = GameObject.find( action.args[ 1 ] as string );
                    if ( Utils.game.isAllyIsolated() )
                    {
                        dropArea = this.transformDropAreaForIsolatedArea( dropArea );
                    }
                    
                    let local: PIXI.IPoint = null;
                    if ( action.args[ 2 ] != null )
                    {
                        local = action.args[ 2 ] as PIXI.Point;
                        if ( local.x != CDropArea.kPredefinedDropPositionCode )
                        {
                            local = dropArea.cContainer.c.toGlobal( action.args[ 2 ] );
                        }
                        // DEBUG:
                        //Utils.debug.drawPoint( globalOrLocal );
                    }
                    dropArea.cDropArea.forceDrop( dropped, local, action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.SHUFFLE_DECK:
                {
                    let deckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( Utils.game.transformToAllyLocation( action.args[ 0 ] ) );
                    ( deckIndicator.cDeckIndicator.cViewer.controller as CardViewerController ).shuffle( action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.REVEAL_X:
                {
                    let deckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( Utils.game.transformToAllyLocation( action.args[ 0 ] ) );
                    ( deckIndicator.cDeckIndicator.cViewer.controller as CardViewerController ).revealCards( action.args[ 1 ], action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.DRAW_X_PLAYER_CARDS:
                {
                    let deckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( Utils.game.transformToAllyLocation( action.args[ 0 ] ) );
                    ( deckIndicator.cDeckIndicator.cViewer.controller as CardViewerController ).drawPlayerCards( action.args[ 1 ], action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.DISCARD_X:
                {
                    let deckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( Utils.game.transformToAllyLocation( action.args[ 0 ] ) );
                    ( deckIndicator.cDeckIndicator.cViewer.controller as CardViewerController ).discardCards( action.args[ 1 ], action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.REMOVE_ALL_CARDS:
                {
                    let deckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( Utils.game.transformToAllyLocation( action.args[ 0 ] ) );
                    ( deckIndicator.cDeckIndicator.cViewer.controller as CardViewerController ).removeCards( action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.SWITCH_DECKS:
                {
                    let deckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( Utils.game.transformToAllyLocation( action.args[ 0 ] ) );
                    ( deckIndicator.cDeckIndicator.cViewer.controller as CardViewerController ).switchDecks( action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.RESHUFFLE_DECK:
                {
                    let deckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( Utils.game.transformToAllyLocation( action.args[ 0 ] ) );
                    ( deckIndicator.cDeckIndicator.cViewer.controller as CardViewerController ).reshuffle( action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.SORT_HAND_BY_SPHERE:
                {
                    let allyHandIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( location_type.ALLY_HAND );
                    ( allyHandIndicator.cDeckIndicator.cViewer.controller as CardViewerController ).sortBySphere( action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.SORT_DECK_FROM_A_TO_Z:
                {
                    let deckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( Utils.game.transformToAllyLocation( action.args[ 0 ] ) );
                    ( deckIndicator.cDeckIndicator.cViewer.controller as CardViewerController ).sortFromAtoZ( action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.SET_CHARACTER_AREA_TOKEN_INDEX:
                {
                    let actor: GameObject = GameObject.find( action.args[ 0 ] as string );
                    let actorTab: GameObject = Utils.game.findGameObjectInBranchByComponentName( actor, "CActorTab" );
                    actorTab.cActorTab.setActorIndex( actor, action.args[ 1 ] as number );
                    break;
                }

            case player_action_type.SET_STAGING_THREAT_COUNTER_AUTO:
                {
                    let stagingThreatCounter: GameObject = GameObject.find( action.args[ 0 ] as string );
                    stagingThreatCounter.cStagingThreatCounter.forceAutoBtnClick();
                    break;
                }

            case player_action_type.SHOW_STAGING_THREAT_COUNTER_AUTO:
                {
                    let stagingThreatCounter: GameObject = GameObject.find( action.args[ 0 ] as string );
                    stagingThreatCounter.cStagingThreatCounter.showAutoBtn();
                    break;
                }

            case player_action_type.SET_PLAYER_WILLPOWER_COUNTER_ATTRIBUTE:
                {
                    let playerWillpowerCounter: GameObject = GameObject.find( action.args[ 0 ] as string );
                    playerWillpowerCounter.cPlayerWillpowerCounter.forceCharacterAttributeBtnClick( action.args[ 1 ] );
                    break;
                }

            case player_action_type.SET_ACTIVE_PLAYER:
                {
                    ServiceLocator.game.setActivePlayer( player_type.PLAYER );
                    break;
                }

            case player_action_type.SET_END_TURN_BTN_STATE:
                {
                    let cEndTurnBtn: CEndTurnButton = ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn;
                    switch ( action.args[ 0 ] as end_turn_btn_state )
                    {
                        case end_turn_btn_state.WAITING: { cEndTurnBtn.setWaiting( action_scope_type.LOCAL ); break; }
                        case end_turn_btn_state.COMPLETED: { cEndTurnBtn.toggleCompleted( action_scope_type.LOCAL ); break; }
                    }
                    break;
                }

            case player_action_type.LEAVE_GAME:
                {
                    let infoMessageView: InfoMessageView = new InfoMessageView();
                    infoMessageView.message = jQuery.i18n( "ALLY_LEFT" );
                    infoMessageView.btnLayoutType = btn_layout_type.DOUBLE;
                    infoMessageView.onClosed.addOnce( this.onAllyLeftPopup_Closed, this );
                    infoMessageView.init();
                    if ( ServiceLocator.game.stateMachine.currentStateId != game_state_id.SETUP )
                    {
                        infoMessageView.root.find( "#infoMessageCancelBtn" ).text( jQuery.i18n( "SAVE" ) )
                            .off( "click" )
                            .on( "click", () => {
                                    let saveGameView: SaveGameView = new SaveGameView();
                                    saveGameView.viewLayerId = view_layer_id.TOPMOST;
                                    saveGameView.init();
                                    ServiceLocator.viewManager.fadeIn( saveGameView, view_layer_id.TOPMOST );
                                } );
                    }

                    else
                    {
                        infoMessageView.root.find( "#infoMessageCancelBtn" ).hide();
                    }
                    ServiceLocator.viewManager.fadeIn( infoMessageView, view_layer_id.TOPMOST );
                    break;
                }

            case player_action_type.ADD_GENERIC_ENEMY:
                {
                    if ( Utils.game.isAllyIsolated() )
                    {
                        ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "isolated" ).cIsolatedArea.cEngaged.addEnemyToken( action.args[ 0 ] as string );
                    }
                    else
                    {
                        ServiceLocator.game.cGameWorld.cAllyArea.cEngaged.addEnemyToken( action.args[ 0 ] as string );
                    }
                    break;
                }

            case player_action_type.SET_GAME_PHASE_ENABLED:
                {
                    ServiceLocator.game.phaseNavigator.setSkipGamePhase( action.args[ 0 ], action.args[ 1 ], action.args[ 2 ] );
                    
                    ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn.onGamePhaseDiagramChanged();
                    if ( Session.allyId )
                    {
                        ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.onGamePhaseDiagramChanged();
                    }
                    break;
                }

            case player_action_type.SET_AUTO_SKIP_EMPTY_PHASES:
                {
                    ServiceLocator.game.phaseNavigator.isAutoSkip = action.args[ 0 ] as boolean;

                    ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn.onGamePhaseDiagramChanged();
                    if ( Session.allyId )
                    {  
                        ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.onGamePhaseDiagramChanged();
                    }
                    break;
                }

            case player_action_type.SET_RESOURCE_PHASE_ADD_TOKEN:
                {
                    ServiceLocator.game.phaseNavigator.isResPhaseAddToken = action.args[ 0 ] as boolean;
                    break;
                }

            case player_action_type.SET_RESOURCE_PHASE_DRAW_CARD:
                {
                    ServiceLocator.game.phaseNavigator.isResPhaseDrawCard = action.args[ 0 ] as boolean;
                    break;
                }

            case player_action_type.SET_STAGING_SUBPHASE_DRAW_CARD:
                {
                    ServiceLocator.game.phaseNavigator.isStagingSubphaseDrawCard = action.args[ 0 ] as boolean;
                    break;
                }

            case player_action_type.DELETE_GAME_OBJECT:
                {
                    GameObject.find( action.args[ 0 ] as string ).end();
                    break;
                }

            case player_action_type.SELECT_MULTI_ITEM_SELECTOR:
                {
                    let multiItemSelector: GameObject = GameObject.find( action.args[ 0 ] as string );
                    if ( Utils.game.isAllyIsolated() )
                    {
                        multiItemSelector = this.transformMultiItemSelectorForIsolatedArea( multiItemSelector );
                    }
                    multiItemSelector.cMultiItemSelector.selectItem( action.args[ 1 ] as number, action_scope_type.LOCAL );
                    break;
                }

            case player_action_type.SET_AUTO_COUNTER:
                {
                    let go: GameObject = GameObject.find( action.args[ 0 ] as string );
                    if ( go.cAttackBindingCounter )
                    {
                        go.cAttackBindingCounter.onAllyAutoUpdated( action.args[ 1 ] as boolean );
                    }
                    else if ( go.cPlayerWillpowerCounter )
                    {
                        go.cPlayerWillpowerCounter.isAuto = action.args[ 1 ] as boolean;
                    }
                    break;
                }
        }

        // Have to check this._mapListener since it might be null due to restart request.
        if (  this._mapListener && this._mapListener.has( action.playerActionType ) )
        {
            let arrListener: Array<IOppActionListener> = this._mapListener.get( action.playerActionType );
            for ( let listener of arrListener )
            {
                listener.onOpponentActionReceived( action );
            }
        }
    }

        private transformDropAreaForIsolatedArea( dropArea: GameObject ): GameObject
        {
            let result: GameObject = dropArea;

            if ( dropArea.cQuestHolder )
            {
                if ( dropArea.cQuestHolder.location == location_type.ISOLATED_QUEST )
                {
                    result = ServiceLocator.game.cGameWorld.findLocation( location_type.QUEST );
                }
                else if ( dropArea.cQuestHolder.location == location_type.QUEST )
                {
                    result = ServiceLocator.game.cGameWorld.findLocation( location_type.ISOLATED_QUEST );
                }
            }
            else if ( dropArea.cActiveLocationHolder )
            {
                if ( dropArea.cActiveLocationHolder.location == location_type.ISOLATED_ACTIVE_LOCATION )
                {
                    result = ServiceLocator.game.cGameWorld.findLocation( location_type.ACTIVE_LOCATION );
                }
                else if ( dropArea.cActiveLocationHolder.location == location_type.ACTIVE_LOCATION )
                {
                    result = ServiceLocator.game.cGameWorld.findLocation( location_type.ISOLATED_ACTIVE_LOCATION );
                }
            }
            else if ( dropArea.cActorArea )
            {
                switch ( dropArea.cActorArea.location )
                {
                    case location_type.ISOLATED_HOME: { result = ServiceLocator.game.cGameWorld.findLocation( location_type.MY_HOME ); break; }
                    case location_type.ALLY_HOME: { result = ServiceLocator.game.cGameWorld.findLocation( location_type.ISOLATED_HOME ); break; }
                    case location_type.ISOLATED_ENGAGED: { result = ServiceLocator.game.cGameWorld.findLocation( location_type.MY_ENGAGED ); break; }
                    case location_type.ALLY_ENGAGED: { result = ServiceLocator.game.cGameWorld.findLocation( location_type.ISOLATED_ENGAGED ); break; }
                    case location_type.ISOLATED_STAGING_AREA: { result = ServiceLocator.game.cGameWorld.findLocation( location_type.STAGING_AREA ); break; }
                    case location_type.STAGING_AREA: { result = ServiceLocator.game.cGameWorld.findLocation( location_type.ISOLATED_STAGING_AREA ); break; }
                }
            }

            return result;
        }

        private transformMultiItemSelectorForIsolatedArea( multiItemSelector: GameObject ): GameObject
        {
            let result: GameObject = multiItemSelector;

            if ( multiItemSelector.cMultiItemSelector.owner.cQuestHolder )
            {
                if ( multiItemSelector.cMultiItemSelector.owner.cQuestHolder.location == location_type.ISOLATED_QUEST )
                {
                    result = ServiceLocator.game.cGameWorld.findLocation( location_type.QUEST ).cQuestHolder.selector;
                }
                else if ( multiItemSelector.cMultiItemSelector.owner.cQuestHolder.location == location_type.QUEST )
                {
                    result = ServiceLocator.game.cGameWorld.findLocation( location_type.ISOLATED_QUEST ).cQuestHolder.selector;
                }
            }
            else if ( multiItemSelector.cMultiItemSelector.owner.cActiveLocationHolder )
            {
                if ( multiItemSelector.cMultiItemSelector.owner.cActiveLocationHolder.location == location_type.ISOLATED_ACTIVE_LOCATION )
                {
                    result = ServiceLocator.game.cGameWorld.findLocation( location_type.ACTIVE_LOCATION ).cActiveLocationHolder.selector;
                }
                else if ( multiItemSelector.cMultiItemSelector.owner.cActiveLocationHolder.location == location_type.ACTIVE_LOCATION )
                {
                    result = ServiceLocator.game.cGameWorld.findLocation( location_type.ISOLATED_ACTIVE_LOCATION ).cActiveLocationHolder.selector;
                }
            }

            return result;
        }

    // #endregion //


    // #region Input Callbacks //

    private onDocument_KeyUp( event: KeyboardEvent ): void
    {
        if ( event.code == "KeyM" )
        {
            this._isPaused = false;
        }
    }

    // #endregion //


    // #region Other Callbacks //

    private onActionRequestPopup_Closed( oppAction: IOpponentAction, result: boolean ): void
    {
        if ( result )
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ACTION_REQUEST_ACCEPTED, null, oppAction.args );

            ServiceLocator.viewManager.clearLayer( view_layer_id.POPUP );
            this.processActionRequest( oppAction.args );
        }
        else
        {
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ACTION_REQUEST_REJECTED, null, null );
        }
    }

        private processActionRequest( args: Array<any> ): void
        {
            switch ( args[ 0 ] as action_request_type )
            {
                case action_request_type.RESTART_GAME: { ServiceLocator.game.restart(); break; }

                case action_request_type.JUMP_TO_GAME_PHASE: 
                { 
                    ServiceLocator.game.stateMachine.onStateEntered.addOnce( ( state: State ) => {
                        let phaseIntroState: PhaseIntroState = state as PhaseIntroState;
                        phaseIntroState.nextStateId = args[ 1 ] as game_state_id; 
                    } );
                    ServiceLocator.game.stateMachine.request( game_state_id.PHASE_INTRO ); 
                    break; 
                }

                case action_request_type.GO_TO_VICTORY_SCREEN: 
                { 
                    let victoryView: VictoryView = new VictoryView();
                    victoryView.init();
                    ServiceLocator.viewManager.fadeIn( victoryView, view_layer_id.POPUP );
                    break; 
                }
            }
        }

    private onAllyLeftPopup_Closed(): void
    {
        ServiceLocator.viewManager.clearLayer( view_layer_id.POPUP );
        ServiceLocator.viewManager.clearLayer( view_layer_id.TOPMOST );

        ServiceLocator.stateMachine.request( main_state_id.GATEWAY );
    }

    // #endregion //
}

export interface IOpponentAction
{
    playerActionType: player_action_type;
    gameStateId: game_state_id;
    args: Array<any>;
}

export interface IOppActionListener
{
    onOpponentActionReceived( opponentAction: IOpponentAction ): void;
}

export const enum action_request_type
{
    RESTART_GAME = 0,
    JUMP_TO_GAME_PHASE,
    GO_TO_VICTORY_SCREEN
}