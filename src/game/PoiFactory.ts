import { player_type } from "./component/world/CPlayerArea";

import GameObject from "./GameObject";
import CContainer from "./component/pixi/CContainer";
import CPoi, { poi_type } from "./component/world/CPoi";
import SignalBinding from "../lib/signals/SignalBinding";
import CTooltipReceptor from "./component/ui/CTooltipReceptor";


export default class PoiFactory
{
    // #region Attributes //

    // private:

    // Global.
    private static _gPoiCount: number = 0;

    // #endregion //


    // #region Methods //

    // public:

    public create( poiReceptor: GameObject, oid: string, poiType: poi_type, poiParams: Array<any>, creator: player_type, controller: player_type, target: GameObject ): GameObject
    {
        let poi: GameObject = new GameObject( [ new CContainer(), new CPoi(), new CTooltipReceptor() ] );
        poi.oid = oid;
        poi.cPoi.poiType = poiType;
        poi.cPoi.params = poiParams;
        poi.cPoi.creator = creator;
        poi.cPoi.controller = controller;
        poi.cPoi.target = target;
        const kArrTooltip: Array<string> = this.findPoiTooltip( poiType, poiParams );
        poi.cTooltipReceptor.text = kArrTooltip[ 0 ];
        poi.cTooltipReceptor.description = kArrTooltip[ 1 ];
        poi.init();
        poi.cPoi.setEnabled( controller == player_type.PLAYER );

        let arrSignalBinding: any = [];
        let onEndedSb: SignalBinding = poiReceptor.onEnded.addOnce( this.onPoiReceptor_Ended, this );
        arrSignalBinding.push( onEndedSb );
        if ( poiReceptor.cCardToken )
        {
            let onFlippedSb: SignalBinding = poiReceptor.cCardToken.onFlipped.addOnce( this.onPoiReceptor_Ended, this );
            arrSignalBinding.push( onFlippedSb );
            onFlippedSb.params = [ poi, arrSignalBinding ];
        }
        onEndedSb.params = [ poi, arrSignalBinding ];

        return poi;
    }

    public generateOid( poiReceptor: GameObject ): string
    {
        let result: string = poiReceptor.oid 
            + "_poi" 
            + PoiFactory._gPoiCount.toString()
            + GameObject.findPlayerSufix( player_type.PLAYER );

        PoiFactory._gPoiCount += 1;
        if ( PoiFactory._gPoiCount == Number.MAX_SAFE_INTEGER )
        {
            PoiFactory._gPoiCount = 0;
        }

        return result;
    }

    // private:

    private findPoiTooltip( optionType: poi_type, poiParams: Array<any> ): Array<string>
    {
        let result: Array<string> = [ null, null ];

        switch ( optionType )
        {
            case poi_type.HIGHLIGHT: { result[ 0 ] = jQuery.i18n( "HIGHLIGHT" ); break; }
            case poi_type.BOW: { result[ 0 ] = jQuery.i18n( "BOW" ); break; }
            case poi_type.READY: { result[ 0 ] = jQuery.i18n( "READY" ); break; }
            case poi_type.DISCARD: { result[ 0 ] = jQuery.i18n( "DISCARD" ); break; }
            case poi_type.GIVE_CONTROL: { result[ 0 ] = jQuery.i18n( "GIVE_CONTROL" ); break; }
            case poi_type.REVEAL: { result[ 0 ] = jQuery.i18n( "REVEAL" ); break; }
            case poi_type.RANDOMLY_DISCARD: { result[ 0 ] = jQuery.i18n( "RANDOMLY_DISCARD" ); break; }
            case poi_type.ADD_VALUE: { result[ 0 ] = jQuery.i18n( poiParams[ 0 ] > 0 ? "ADD_VALUE" : "REMOVE_VALUE" ).replace( "#", poiParams[ 0 ] ); break; }
            case poi_type.GIVE_VALUE: { result[ 0 ] = jQuery.i18n( "GIVE_VALUE" ).replace( "#", poiParams[ 0 ] ); break; }
        }

        return result;
    }

    // #endregion //


    // #region Callbacks //

    private onPoiReceptor_Ended( poi: GameObject, arrSignalBinding: Array<SignalBinding> ): void
    {
        for ( let sb of arrSignalBinding )
        {
            sb.detach();
        }

        if ( poi.cContainer )
        {
            poi.end();
        }
    }

    // #endregion //
}