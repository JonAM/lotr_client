import ServiceLocator from "../ServiceLocator";


export default class ScenarioDB
{
    // #region Attributes //

    // private:

    private _db: Map<string, ICardSet> = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        this._db = new Map<string, ICardSet>( Object.entries( ServiceLocator.resourceStack.find( "scenario_db" ).data ) );
    }

    public find( cardSetId: string ): ICardSet
    {
        let result: ICardSet = null;

        if ( this._db.has( cardSetId ) )
        {
            result = this._db.get( cardSetId );
        }

        return result;
    }

    public findScenario( scenarioId: string ): IScenario
    {
        let result: IScenario = null;

        const kArrScenario: Array<IScenario> = this._db.get( scenarioId.substr( 0, 2 ) ).scenarios;
        for ( let scenario of kArrScenario )
        {
            if ( scenario.id == scenarioId )
            {
                result = scenario;
                break;
            }
        }

        return result;
    }

    public list(): Array<ICardSet>
    {
        let result: Array<ICardSet> = new Array<ICardSet>();

        let entriesIt: IterableIterator<[string, Object]> = this._db.entries();
        for ( let entriesItResult: IteratorResult<[string, Object]> = entriesIt.next(); !entriesItResult.done; entriesItResult = entriesIt.next() )
        {
            let cardSet: ICardSet = entriesItResult.value[ 1 ] as ICardSet;
            cardSet.id = entriesItResult.value[ 0 ];
            result.push( cardSet );
        }

        return result;
    }

    // #endregion //
}

export interface ICardSet
{
    id?: string;
    name: string;
    texture_map_count: number;
    scenarios: Array<IScenario>;
}

export interface IScenario
{
    id: string;
    name?: string;
    type: string;
    product_icon: string;
    mec?: string;
    under_construction?: boolean;
    encounter_set: IEncounterSet;
    additional_cards?: Array<string>;
    treasure_sets?: Array<string>;
    boons?: Array<string>;
    removed_sauron_cards?: Array<string>;
    quest: IQuest;
    win?: Array<string>;
    gencon_setup?: string;
    play_area?: IPlayArea;
    setup?: Array<ISetupAction>;
}

    interface IEncounterSet
    {
        main: string;
        additional?: Array<string>;
    }

    interface IQuest
    {
        shuffle?: Array<Array<number>>;
        deck?: Array<string>;
    }

    interface IPlayArea
    {
        sauron?: ISauronPlayArea;
        player_custom_deck?: IDeck;
        custom_panels?: Array<string>; // isolated, riddle, stormcaller, orcs, circuit, island, compass, investigation_list, treasure_list.
    }

        interface ISauronPlayArea
        {
            custom_deck?: IDeck;
            custom_discard?: IDeck;
            secondary_custom_deck?: IDeck;
            secondary_custom_discard?: IDeck;
            secondary_encounter_deck?: IDeck;
        }

        interface IDeck
        {
            titleI18n: string;
            cards?: Array<string>;
        }

    export interface ISetupAction
    {
        type: string;
        params?: Array<any>;
        options?: Array<string>;
        select?: ICardSelect;
    }

        export interface ICardSelect
        {
            filters?: Array<ICardFilter>;
            max_count?: any;
        }

        export interface ICardFilter
        {
            encounter_sets?: Array<string>;
            type_codes?: Array<string>;
            traits?: Array<string>;
            codes?: Array<string>;
            is_unique?: boolean;
            is_burden?: boolean;
        }