import ServiceLocator from "../ServiceLocator";
import { gsap, TimelineMax, Sine, Back } from "gsap";
import * as PIXI from "pixi.js";
import { layer_type } from "./component/world/CGameLayerProvider";


export default class FloatingMessage
{
    // #region Methods //

    // public:

    public constructor() {}

    public show( message: string, global: PIXI.Point ): void
    {
        let tooltip: PIXI.Graphics = new PIXI.Graphics();
        //
        let textStyle: PIXI.TextStyle = ServiceLocator.game.textStyler.normal;
        textStyle.wordWrap = true;
        textStyle.wordWrapWidth = 300;
        let text: PIXI.Text = new PIXI.Text( message, textStyle );
        text.position.set( 10, 10 );
        tooltip.addChild( text );
        //
        tooltip.lineStyle( 1, 0x00000, 1 );
        tooltip.beginFill( 0xefe4b0, 1 );
        tooltip.drawRect( 0, 0, text.width + 20, tooltip.height + 20 );
        tooltip.endFill();
        tooltip.position.copyFrom( this.findSpawnPosition( tooltip, global ) );
        //
        ServiceLocator.game.root.cGameLayerProvider.get( layer_type.POPUP ).cContainer.c.addChild( tooltip );

        this.animate( tooltip);
    }

    // private:

    private findSpawnPosition( tooltip: PIXI.Container, global: PIXI.Point ): PIXI.Point
    {
        const kWorldWidth: number = ServiceLocator.game.app.screen.width;
        const kWorldHeight: number = ServiceLocator.game.app.screen.height;

        let result: PIXI.Point = new PIXI.Point( global.x - tooltip.width, global.y - tooltip.height );
        let isValidPosition: boolean = result.x >= 0 && result.y >= 0;
        if ( !isValidPosition )
        {
            result.x = global.x;
            isValidPosition = result.x + tooltip.width <= kWorldWidth && result.y >= 0;
        }
        if ( !isValidPosition )
        {
            result.set( global.x - tooltip.width, global.y );
            isValidPosition = result.x >= 0 && result.y + tooltip.height <= kWorldHeight;
        }
        if ( !isValidPosition )
        {
            result.x = global.x;
        }

        return result;
    }

    private animate( tooltip: PIXI.Container ): void
    {
        gsap.to( tooltip, { 
            y: "-=50",
            duration: 1,
            ease: Sine.easeOut } );
        gsap.to( tooltip, { 
            alpha: 0,
            duration: 0.25,
            delay: 0.75,
            ease: Sine.easeOut,
            onComplete: ( tooltip: PIXI.Container ) => { tooltip.parent.removeChild( tooltip ); },
            onCompleteParams: [ tooltip ],
            callbackScope: this } );
    }

    // #endregion //
}