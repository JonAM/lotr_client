import ServiceLocator from "../ServiceLocator";
import Utils from "../Utils";
import * as PIXI from "pixi.js";

import GameObject from "./GameObject";
import KeyBindingManager from "./KeyBindingManager";
import { layer_type } from "./component/world/CGameLayerProvider";


export default class ForceDragManager
{
    // #region Attributes //

    // private:

    private _keyBindingManager: KeyBindingManager = null;

    // #endregion //


    // #region Properties //

    public set keyBidingManager( value: KeyBindingManager ) { this._keyBindingManager = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() {}

    public init(): void
    {
        console.assert( this._keyBindingManager != null, "ForceDragManager.ts :: init() :: this._keyBindingManager cannot be null." );

        this._keyBindingManager.onKeyUp.add( this.onBindedKey_Up, this );
    }

    public end(): void 
    {
        this._keyBindingManager.onKeyUp.remove( this.onBindedKey_Up, this );

        this._keyBindingManager = null;
    }

    // #endregion //


    // #region Callbacks //

    private onBindedKey_Up( bindingId: string ): void
    {
        if ( bindingId == "forceDrag" )
        {
            const kGlobal: PIXI.Point = ServiceLocator.game.app.renderer.plugins.interaction.mouse.global;
            let hit: GameObject = Utils.game.hitFirstTest( kGlobal, "cDraggable", ServiceLocator.game.root.cGameLayerProvider.get( layer_type.HUD ).cContainer.c );
            if ( hit )
            {
                if ( hit.cCard )
                {
                    let arrCard: Array<GameObject> = Utils.game.hitTest( kGlobal, "cCard", hit.cContainer.c.parent );
                    if ( arrCard.length > 0 )
                    {
                        hit = arrCard[ arrCard.length - 1 ];
                    }
                }
            }

            if ( !hit )
            {
                hit = Utils.game.hitFirstTest( kGlobal, "cContainer", ServiceLocator.game.root.cGameLayerProvider.get( layer_type.HUD ).cContainer.c );
            }

            if ( !hit )
            {
                let arrHit: Array<GameObject> = Utils.game.hitTest( kGlobal, "cDraggable", ServiceLocator.game.root.cGameLayerProvider.get( layer_type.CUSTOM_PANEL ).cContainer.c );
                if ( arrHit.length > 0 )
                {
                    hit = this.findHitWithHighestPriority( arrHit );
                }
            }

            if ( !hit )
            {
                hit = Utils.game.hitFirstTest( kGlobal, "cContainer", ServiceLocator.game.root.cGameLayerProvider.get( layer_type.CUSTOM_PANEL ).cContainer.c );
            }

            if ( !hit )
            {
                let arrHit: Array<GameObject> = Utils.game.hitTest( kGlobal, "cDraggable", ServiceLocator.game.root.cGameLayerProvider.get( layer_type.GAME ).cContainer.c );
                if ( arrHit.length > 0 )
                {
                    hit = this.findHitWithHighestPriority( arrHit );
                }
            }

            if ( !hit ) 
            {
                hit = Utils.game.hitFirstTest( kGlobal, "cDeckIndicator", ServiceLocator.game.app.stage );
            }

            if ( hit 
                && ( hit.cDraggable
                    || hit.cDeckIndicator && hit.cDeckIndicator.trigger.cDragDetector ) )
            {
                let fakeEvent: PIXI.InteractionEvent = new PIXI.InteractionEvent();
                fakeEvent.data = new PIXI.InteractionData();
                fakeEvent.data.global = kGlobal.clone();
                if ( hit.cDeckIndicator)
                {
                    hit.cDeckIndicator.trigger.cDragDetector.onDragged.dispatch( null, fakeEvent );
                }
                else
                {
                    hit.cDraggable.forceDrag( fakeEvent );
                }
            }
        }
    }

        private findHitWithHighestPriority( arrHit: Array<GameObject> ): GameObject
        {
            let result: GameObject = null;

            for ( let hitAux of arrHit )
            {
                if ( hitAux.cTokenCounter )
                {
                    result = hitAux;
                    break;
                }
                else if ( hitAux.cAttachment || hitAux.cShadowCardMini )
                {
                    result = hitAux;
                }
                else if ( !result || !result.cAttachment && !result.cShadowCardMini )
                {
                    result = hitAux;
                }
            }
            
            return result;
        }

    // #endregion //
}