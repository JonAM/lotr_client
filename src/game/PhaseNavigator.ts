import ServiceLocator from "../ServiceLocator";
import Session from "../Session";
import { combat_state_id, encounter_state_id, game_state_id, quest_state_id } from "../states/StateId";
import { ISgPhaseNavigator } from "../view/game/SaveGameView";
import GameObject from "./GameObject";


export default class PhaseNavigator
{
    // #region Attributes //

    // private:

    private _isAutoSkip: boolean = null;
    private _isResPhaseAddToken: boolean = null;
    private _isResPhaseDrawCard: boolean = null;
    private _isStagingSubphaseDrawCard: boolean = null;

    private _arrGamePhase: Array<IGamePhase> = null;

    // #endregion //


    public get isAutoSkip(): boolean { return this._isAutoSkip; }
    public get isResPhaseAddToken(): boolean { return this._isResPhaseAddToken; }
    public get isResPhaseDrawCard(): boolean { return this._isResPhaseDrawCard; }
    public get isStagingSubphaseDrawCard(): boolean { return this._isStagingSubphaseDrawCard; }

    public set isAutoSkip( value: boolean ) { this._isAutoSkip = value; }
    public set isResPhaseAddToken( value: boolean ) { this._isResPhaseAddToken = value; }
    public set isResPhaseDrawCard( value: boolean ) { this._isResPhaseDrawCard = value; }
    public set isStagingSubphaseDrawCard( value: boolean ) { this._isStagingSubphaseDrawCard = value; }


    // #region Methods //

    // public:

    public constructor() {}

    public init(): void
    {
        this._isAutoSkip = false;
        this._isResPhaseAddToken = true;
        this._isResPhaseDrawCard = true;
        this._isStagingSubphaseDrawCard = true;

        // Main.
        let arrEndTurnButtonI18n: Array<string> = [
            null, // SETUP.
            null, // RESOURCE.
            "TO_PLANNING_PHASE",
            "TO_QUEST_PHASE",
            "TO_TRAVEL_PHASE",
            "TO_ENCOUNTER_PHASE",
            "TO_COMBAT_PHASE",
            "TO_REFRESH_PHASE" ];
        this._arrGamePhase = new Array<IGamePhase>();
        for ( let stateId: game_state_id = game_state_id.SETUP; stateId <= game_state_id.REFRESH; ++stateId )
        {
            this._arrGamePhase.push( { 
                stateId: stateId,
                endTurnButtonI18n: arrEndTurnButtonI18n[ stateId ] } );
        }
        // Children.
        // Quest.
        arrEndTurnButtonI18n = [
            null, // CHARACTER_COMMITMENT.
            "TO_STAGING",
            "TO_QUEST_RESOLUTION" ];
        let arrGameSubphase: Array<IGamePhase> = new Array<IGamePhase>();
        for ( let stateId: quest_state_id = quest_state_id.CHARACTER_COMMITMENT; stateId <= quest_state_id.QUEST_RESOLUTION; ++stateId )
        {
            arrGameSubphase.push( {
                stateId: stateId,
                endTurnButtonI18n: arrEndTurnButtonI18n[ stateId ] } );
        }
        this._arrGamePhase[ game_state_id.QUEST ].children = arrGameSubphase;
        // Encounter.
        arrEndTurnButtonI18n = [
            null, // OPTIONAL_ENGAGEMENT.
            "TO_ENGAGEMENT_CHECKS" ];
        arrGameSubphase = new Array<IGamePhase>();
        for ( let stateId: encounter_state_id = encounter_state_id.OPTIONAL_ENGAGEMENT; stateId <= encounter_state_id.ENGAGEMENT_CHECKS; ++stateId )
        {
            arrGameSubphase.push( {
                stateId: stateId,
                endTurnButtonI18n: arrEndTurnButtonI18n[ stateId ] } );
        }
        this._arrGamePhase[ game_state_id.ENCOUNTER ].children = arrGameSubphase;
        // Combat.
        arrEndTurnButtonI18n = [
            null, // ENEMY_ATTACKS.
            "TO_PLAYER_ATTACKS" ];
        arrGameSubphase = new Array<IGamePhase>();
        for ( let stateId: combat_state_id = combat_state_id.ENEMY_ATTACKS; stateId <= combat_state_id.PLAYER_ATTACKS; ++stateId )
        {
            arrGameSubphase.push( {
                stateId: stateId,
                endTurnButtonI18n: arrEndTurnButtonI18n[ stateId ] } );
        }
        this._arrGamePhase[ game_state_id.COMBAT ].children = arrGameSubphase;
    }

    public end(): void
    {
        this._isAutoSkip = null;
        this._isResPhaseAddToken = null;
        this._isResPhaseDrawCard = null;
        this._isStagingSubphaseDrawCard = null;
        this._arrGamePhase = null;
    }

    public findNextStateId( curStateId: number, parentStateId: game_state_id = null ): number
    {
        let result: number = null;
        let arrGamePhase: Array<IGamePhase> = null;
        if ( parentStateId == null )
        {
            arrGamePhase = this._arrGamePhase;
        }
        else
        {
            arrGamePhase = this._arrGamePhase[ parentStateId ].children;
        }
        for ( let gamePhase of arrGamePhase )
        {
            if ( gamePhase.stateId > curStateId 
                && !gamePhase.isSkip
                && !this.isPhaseEmpty( gamePhase.stateId, parentStateId ) )
            {
                result = gamePhase.stateId;
                break;
            }
        }
        if ( result == null && parentStateId != null )
        {
            result = this.findNextStateId( parentStateId );
        }

        return result;
    }

    public findNexEndTurnButtonI18n( curStateId: number, parentStateId: game_state_id = null ): string
    {
        let result: string = null;
        let arrGamePhase: Array<IGamePhase> = null;
        if ( parentStateId == null )
        {
            arrGamePhase = this._arrGamePhase;
        }
        else
        {
            arrGamePhase = this._arrGamePhase[ parentStateId ].children;
        }
        for ( let gamePhase of arrGamePhase )
        {
            if ( gamePhase.stateId > curStateId 
                && !gamePhase.isSkip
                && !this.isPhaseEmpty( gamePhase.stateId, parentStateId ) )
            {
                result = gamePhase.endTurnButtonI18n;
                break;
            }
        }
        if ( !result && parentStateId != null )
        {
            result = this.findNexEndTurnButtonI18n( parentStateId );
        }

        return result;
    }

    public findFirstPlayableSubstateId( parentStateId: game_state_id ): number
    {
        let result: number = null;

        if ( this._arrGamePhase[ parentStateId ].children )
        {
            const kArrGameSubpase: Array<IGamePhase> = this._arrGamePhase[ parentStateId ].children;
            for ( let gameSubphase of kArrGameSubpase )
            {
                if ( !gameSubphase.isSkip && !this.isPhaseEmpty( gameSubphase.stateId, parentStateId ) )
                {
                    result = gameSubphase.stateId;
                    break;
                }
            }
        }

        return result;
    }

    public findLastPlayableSubstateId( parentStateId: game_state_id ): number
    {
        let result: number = null;

        if ( this._arrGamePhase[ parentStateId ].children )
        {
            const kArrGameSubpase: Array<IGamePhase> = this._arrGamePhase[ parentStateId ].children;
            for ( let gameSubphase of kArrGameSubpase )
            {
                if ( !gameSubphase.isSkip && !this.isPhaseEmpty( gameSubphase.stateId, parentStateId ) )
                {
                    result = gameSubphase.stateId;
                }
            }
        }

        return result;
    }

    public isPhaseEmpty( stateId: number, parentStateId: game_state_id ): boolean
    {
        let result: boolean = false;

        if ( this._isAutoSkip
            && ( !Session.allyId 
                || !ServiceLocator.game.cGameWorld.customPanelManager.cCustomPanelManager.checkPanelVisibility( "isolated" ) ) )
        {
            let gameStateId: game_state_id = parentStateId;
            if ( parentStateId == null )
            {
                gameStateId = stateId;
            }

            switch ( gameStateId )
            {
                case game_state_id.TRAVEL: 
                { 
                    result = true;
                    for ( let actor of ServiceLocator.game.cGameWorld.cSauronArea.cStaging.findAllActors() )
                    {
                        if ( actor.cCardToken.curSide.cLocationSide )
                        {
                            result = false;
                            break;
                        }
                    }
                    break; 
                }

                case game_state_id.ENCOUNTER: 
                { 
                    if ( parentStateId == null
                        || stateId == encounter_state_id.OPTIONAL_ENGAGEMENT )
                    {
                        result = true;
                        let arrActor: Array<GameObject> = ServiceLocator.game.cGameWorld.cSauronArea.cStaging.findAllActors().concat(
                            ServiceLocator.game.cGameWorld.cPlayerArea.cEngaged.findAllActors().concat(
                                ServiceLocator.game.cGameWorld.cAllyArea.cEngaged.findAllActors() ) );
                        for ( let actor of arrActor )
                        {
                            if ( actor.cCardToken.curSide.cEnemySide )
                            {
                                result = false;
                                break;
                            }
                        }
                    }
                    else if ( stateId == encounter_state_id.ENGAGEMENT_CHECKS )
                    {
                        result = true;
                        for ( let actor of ServiceLocator.game.cGameWorld.cSauronArea.cStaging.findAllActors() )
                        {
                            if ( actor.cCardToken.curSide.cEnemySide )
                            {
                                result = false;
                                break;
                            }
                        }
                    }
                    break; 
                }

                case game_state_id.COMBAT:
                {
                    result = true;
                    let arrActor: Array<GameObject> = ServiceLocator.game.cGameWorld.cSauronArea.cStaging.findAllActors().concat(
                        ServiceLocator.game.cGameWorld.cPlayerArea.cEngaged.findAllActors().concat(
                            ServiceLocator.game.cGameWorld.cAllyArea.cEngaged.findAllActors() ) );
                    for ( let actor of arrActor )
                    {
                        if ( actor.cCardToken.curSide.cEnemySide )
                        {
                            result = false;
                            break;
                        }
                    }
                    break;
                }
            }
        }

        return result;
    }

    public isSkipGamePhase( mainStateId: number, childStateId: number = null ): boolean
    {
        let result: boolean = false;

        if ( childStateId == null )
        {
            result = this._arrGamePhase[ mainStateId ].isSkip;
        }
        else
        {
            result = this._arrGamePhase[ mainStateId ].children[ childStateId ].isSkip;
        }

        return result;
    }

    public setSkipGamePhase( mainStateId: number, childStateId: number = null, isSkip: boolean ): void
    {
        console.assert( mainStateId >= game_state_id.QUEST && mainStateId <= game_state_id.COMBAT, "GamePhaseNavigationManager.ts :: setSkipGamePhase() :: Invalid mainStateId." );
        let gamePhase: IGamePhase = this._arrGamePhase[ mainStateId ];
        if ( childStateId == null )
        {
            gamePhase.isSkip = isSkip;
            if ( gamePhase.children )
            {
                for ( let gameSubphase of gamePhase.children )
                {
                    gameSubphase.isSkip = isSkip;
                }
            }
        }
        else
        {
            gamePhase.children[ childStateId ].isSkip = isSkip;
            if ( isSkip )
            {
                let isPhaseSkip: boolean = true;
                for ( let gameSubphase of gamePhase.children )
                {
                    if ( !gameSubphase.isSkip )
                    {
                        isPhaseSkip = false;
                        break;
                    }
                }
                if ( isPhaseSkip )
                {
                    gamePhase.isSkip = true;
                }
            }
            else
            {
                gamePhase.isSkip = false;
            }
        }
    }

    public saveGame(): ISgPhaseNavigator
    {
        return { 
            isAutoSkip: this._isAutoSkip,
            isResPhaseAddToken: this._isResPhaseAddToken,
            isResPhaseDrawCard: this._isResPhaseDrawCard,
            isStagingSubphaseDrawCard: this._isStagingSubphaseDrawCard,
            gamePhases: this._arrGamePhase };
    }

    public loadGame( sgPhaseNavigator: ISgPhaseNavigator, pass: number ): void
    {
        if ( pass == 0 )
        {
            this._isAutoSkip = sgPhaseNavigator.isAutoSkip;
            this._isResPhaseAddToken = sgPhaseNavigator.isResPhaseAddToken == undefined ? true : sgPhaseNavigator.isResPhaseAddToken ;
            this._isResPhaseDrawCard = sgPhaseNavigator.isResPhaseDrawCard == undefined ? true : sgPhaseNavigator.isResPhaseDrawCard;
            this._isStagingSubphaseDrawCard = sgPhaseNavigator.isStagingSubphaseDrawCard == undefined ? true : sgPhaseNavigator.isStagingSubphaseDrawCard;
            this._arrGamePhase = sgPhaseNavigator.gamePhases;
        }
    }

    // #endregion //
}

export interface IGamePhase
{
    stateId: number;
    children?: Array<IGamePhase>;
    isSkip?: boolean;
    endTurnButtonI18n?: string;
}