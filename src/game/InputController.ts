import * as PIXI from "pixi.js";
import Utils from "../Utils";


export default abstract class InputController
{
    protected _stage: PIXI.Container = null;

    protected _mapEventTypeToListener: Map<input_event_type, Map<PIXI.DisplayObject, Array<IInputListenerData>>> = null;
    protected _mapEventTypeToChildrenListener: Map<input_event_type, Map<PIXI.Container, Array<IInputListenerData>>> = null;
    protected _arrEvent: Array<PIXI.InteractionEvent> = null;

    protected _over: PIXI.DisplayObject = null;


    public set stage( value: PIXI.Container ) { this._stage = value; }


    protected constructor() 
    {
        this._mapEventTypeToListener = new Map<input_event_type, Map<PIXI.DisplayObject, Array<IInputListenerData>>>();
        this._mapEventTypeToChildrenListener = new Map<input_event_type, Map<PIXI.Container, Array<IInputListenerData>>>();
        this._arrEvent = new Array<PIXI.InteractionEvent>();
    }

    // virtual.
    public init(): void
    {
        console.assert( this._stage != null, "InputController.ts :: init() :: this._stage cannot be null." );

        this._mapEventTypeToListener = new Map<input_event_type, Map<PIXI.DisplayObject, Array<IInputListenerData>>>( [ 
            [ input_event_type.DOWN, new Map<PIXI.DisplayObject, Array<IInputListenerData>>() ], 
            [ input_event_type.UP, new Map<PIXI.DisplayObject, Array<IInputListenerData>>() ], 
            [ input_event_type.OVER, new Map<PIXI.DisplayObject, Array<IInputListenerData>>() ], 
            [ input_event_type.OUT, new Map<PIXI.DisplayObject, Array<IInputListenerData>>() ], 
            [ input_event_type.MOVE, new Map<PIXI.DisplayObject, Array<IInputListenerData>>() ], 
            [ input_event_type.TAP, new Map<PIXI.DisplayObject, Array<IInputListenerData>>() ] ] );

        this._mapEventTypeToChildrenListener = new Map<input_event_type, Map<PIXI.Container, Array<IInputListenerData>>>( [ 
            [ input_event_type.DOWN, new Map<PIXI.Container, Array<IInputListenerData>>() ],  
            [ input_event_type.UP, new Map<PIXI.Container, Array<IInputListenerData>>() ],  
            [ input_event_type.OVER, new Map<PIXI.Container, Array<IInputListenerData>>() ],  
            [ input_event_type.OUT, new Map<PIXI.Container, Array<IInputListenerData>>() ],  
            [ input_event_type.MOVE, new Map<PIXI.Container, Array<IInputListenerData>>() ],  
            [ input_event_type.TAP, new Map<PIXI.Container, Array<IInputListenerData>>() ] ] );
    }

    // virtual.
    public end(): void
    {
        this._stage = null;

        this._mapEventTypeToListener = null;
        this._mapEventTypeToChildrenListener = null;
        this._arrEvent = null;

        this._over = null;
    }

    public abstract update( dt: number ): void;

    public on( target: PIXI.DisplayObject, eventType: input_event_type, callback: Function, context: any, ...params: any[] ): void
    {
        this.addListener( target, eventType, callback, context, params, false );
    }

        private addListener( target: PIXI.DisplayObject, eventType: input_event_type, callback: Function, context: any, params: Array<any>, isOnce: boolean ): void
        {
            let mapDisplayObjectToInputListener: Map<PIXI.DisplayObject, Array<IInputListenerData>> = this._mapEventTypeToListener.get( eventType );
            if ( !mapDisplayObjectToInputListener.has( target ) )
            {
                mapDisplayObjectToInputListener.set( target, [ { callback: callback, context: context, params: params, isOnce: isOnce } ] );
            }
            else
            {
                let arrInputListenerData: Array<IInputListenerData> = mapDisplayObjectToInputListener.get( target );
                let isDuplicate: boolean = false;
                for ( let inputListenerData of arrInputListenerData )
                {
                    if ( inputListenerData.callback == callback && inputListenerData.context == context )
                    {
                        isDuplicate = true;
                        break;
                    }
                }
                if ( !isDuplicate )
                {
                    arrInputListenerData.push( { callback: callback, context: context, params: params, isOnce: isOnce } );
                }
            }
        }

    public once( target: PIXI.DisplayObject, eventType: input_event_type, callback: Function, context: any, ...params: any[] ): void
    {
        this.addListener( target, eventType, callback, context, params, true );
    }

    public off( target: PIXI.DisplayObject, eventType: input_event_type, callback: Function, context: any ): void
    {
        let mapDisplayObjectToInputListener: Map<PIXI.DisplayObject, Array<IInputListenerData>> = this._mapEventTypeToListener.get( eventType );
        if ( mapDisplayObjectToInputListener.has( target ) )
        {
            let arrInputListenerData: Array<IInputListenerData> = mapDisplayObjectToInputListener.get( target );
            for ( let i: number = 0; i < arrInputListenerData.length; ++i )
            {
                const kInputListenerData: IInputListenerData = arrInputListenerData[ i ];
                if ( kInputListenerData.callback == callback && kInputListenerData.context == context )
                {
                    arrInputListenerData.splice( i, 1 );
                    break;
                }
            }
            if ( arrInputListenerData.length == 0 )
            {
                mapDisplayObjectToInputListener.delete( target );
            }
        }
    }

    public onChildren( target: PIXI.Container, eventType: input_event_type, callback: Function, context: any, ...params: any[] ): void
    {
        this.addChildrenListener( target, eventType, callback, context, params, false );
    }

        private addChildrenListener( target: PIXI.Container, eventType: input_event_type, callback: Function, context: any, params: Array<any>, isOnce: boolean ): void
        {
            let mapContainerToInputListener: Map<PIXI.Container, Array<IInputListenerData>> = this._mapEventTypeToChildrenListener.get( eventType );
            if ( !mapContainerToInputListener.has( target ) )
            {
                mapContainerToInputListener.set( target, [ { callback: callback, context: context, params: params, isOnce: isOnce } ] );
            }
            else
            {
                let arrInputListenerData: Array<IInputListenerData> = mapContainerToInputListener.get( target );
                let isDuplicate: boolean = false;
                for ( let inputListenerData of arrInputListenerData )
                {
                    if ( inputListenerData.callback == callback && inputListenerData.context == context )
                    {
                        isDuplicate = true;
                        break;
                    }
                }
                if ( !isDuplicate )
                {
                    arrInputListenerData.push( { callback: callback, context: context, params: params, isOnce: isOnce } );
                }
            }
        }

    public onceChildren( target: PIXI.Container, eventType: input_event_type, callback: Function, context: any, ...params: any[] ): void
    {
        this.addChildrenListener( target, eventType, callback, context, params, true );
    }

    public offChildren( target: PIXI.Container, eventType: input_event_type, callback: Function, context: any ): void
    {
        let mapContainerToInputListener: Map<PIXI.Container, Array<IInputListenerData>> = this._mapEventTypeToChildrenListener.get( eventType );
        if ( mapContainerToInputListener.has( target ) )
        {
            let arrInputListenerData: Array<IInputListenerData> = mapContainerToInputListener.get( target );
            for ( let i: number = 0; i < arrInputListenerData.length; ++i )
            {
                const kInputListenerData: IInputListenerData = arrInputListenerData[ i ];
                if ( kInputListenerData.callback == callback && kInputListenerData.context == context )
                {
                    arrInputListenerData.splice( i, 1 );
                    break;
                }
            }
            if ( arrInputListenerData.length == 0 )
            {
                mapContainerToInputListener.delete( target );
            }
        }
    }

    // protected:

    protected notifyEvent( eventType: input_event_type, hit: PIXI.DisplayObject, event: PIXI.InteractionEvent ): void
    {
        let mapContainerToInputListenerData: Map<PIXI.Container, Array<IInputListenerData>> = this._mapEventTypeToChildrenListener.get( eventType );
        let entriesIt: IterableIterator<[PIXI.Container, Array<IInputListenerData>]> = mapContainerToInputListenerData.entries();
        for ( let entriesItResult: IteratorResult<[PIXI.Container, Array<IInputListenerData>]> = entriesIt.next(); !entriesItResult.done; entriesItResult = entriesIt.next() )
        {
            let container: PIXI.Container = entriesItResult.value[ 0 ] as PIXI.Container;
            if ( Utils.game.isAncestor( hit, container ) )
            {
                let arrInputListenerData: Array<IInputListenerData> = mapContainerToInputListenerData.get( container );
                for ( let i: number = arrInputListenerData.length - 1; i >= 0; --i )
                {
                    const kInputListenerData: IInputListenerData = arrInputListenerData[ i ];
                    kInputListenerData.callback.call( kInputListenerData.context, ...kInputListenerData.params.concat( event, hit ) );
                    if ( kInputListenerData.isOnce )
                    {
                        arrInputListenerData.splice( i, 1 );
                    }
                }
                if ( arrInputListenerData.length == 0 )
                {
                    mapContainerToInputListenerData.delete( container );
                }
            }
        }
        
        let mapDisplayObjToInputListenerData: Map<PIXI.DisplayObject, Array<IInputListenerData>> = this._mapEventTypeToListener.get( eventType );
        let isDone: boolean = false;
        while ( !isDone )
        {
            if ( hit == this._stage || hit.parent == null )
            {
                isDone = true;
            }

            if ( mapDisplayObjToInputListenerData.has( hit ) )
            {
                let arrInputListenerData: Array<IInputListenerData> = mapDisplayObjToInputListenerData.get( hit );
                for ( let i: number = arrInputListenerData.length - 1; i >= 0; --i )
                {
                    const kInputListenerData: IInputListenerData = arrInputListenerData[ i ];
                    kInputListenerData.callback.call( kInputListenerData.context, ...kInputListenerData.params.concat( event ) );
                    if ( kInputListenerData.isOnce )
                    {
                        arrInputListenerData.splice( i, 1 );
                    }
                }
                if ( arrInputListenerData.length == 0 )
                {
                    mapDisplayObjToInputListenerData.delete( hit );
                }

                isDone = true;
            }
            
            if ( !isDone )
            {
                hit = hit.parent;
            }
        }
    }
}

export interface IInputListenerData
{
    callback: Function;
    context: any;
    params: Array<any>;
    isOnce: boolean;
}

export const enum input_event_type
{
    DOWN = 0,
    UP,
    OVER,
    OUT,
    MOVE,
    TAP
}