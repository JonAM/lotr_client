import { location_type } from "./component/world/CGameWorld";
import { player_type } from "./component/world/CPlayerArea";

import GameObject from "./GameObject";
import CContainer from "./component/pixi/CContainer";
import CDraggable from "./component/input/CDraggable";
import Component from "./component/Component";
import CCardToken from "./component/card/CCardToken";
import CDropArea from "./component/input/CDropArea";
import CCardTokenPoiReceptor from "./component/world/poi_receptor/CCardTokenPoiReceptor";
import CShareableGameElement from "./component/CShareableGameElement";


export default class CardTokenFactory
{
    public create( card: GameObject ): GameObject
    {
        let arrComponent: Array<Component> = [ 
            new CContainer(), 
            new CCardToken(),
            new CDraggable(),
            new CDropArea(), 
            new CCardTokenPoiReceptor(),
            new CShareableGameElement() ];
        let cardToken: GameObject = new GameObject( arrComponent );
        cardToken.oid = card.oid + "t";
        cardToken.cDraggable.dragShadowTextureCreator = cardToken.cCardToken;
        cardToken.cCardToken.frontSideCardId = card.cCard.front.cardId;
        cardToken.cCardToken.backSideCardId = card.cCard.back.cardId;
        cardToken.cCardToken.ownerPlayer = card.cCard.ownerPlayer;
        cardToken.cCardToken.controllerPlayer = card.cCard.controllerPlayer;
        cardToken.cCardToken.location = card.cCard.location;
        cardToken.cCardToken.isFaceUp = card.cCard.isFaceUp;
        cardToken.cDropArea.target = cardToken.cCardToken;
        cardToken.cDropArea.isPropagate = true;
        cardToken.init();

        cardToken.cCardToken.setEnabled( card.cCard.controllerPlayer == player_type.PLAYER );

        return cardToken;
    }

    public createFromAttachment( attachment: GameObject ): GameObject
    {
        let arrComponent: Array<Component> = [ 
            new CContainer(), 
            new CCardToken(),
            new CDraggable(),
            new CDropArea(), 
            new CCardTokenPoiReceptor(),
            new CShareableGameElement() ];
        let cardToken: GameObject = new GameObject( arrComponent );
        cardToken.oid = attachment.oid.substr( 0, attachment.oid.indexOf( "_attachment" ) ) + "t";
        cardToken.cDraggable.dragShadowTextureCreator = cardToken.cCardToken;
        cardToken.cCardToken.frontSideCardId = attachment.cAttachment.front.cardId;
        cardToken.cCardToken.backSideCardId = attachment.cAttachment.back.cardId;
        cardToken.cCardToken.ownerPlayer = attachment.cAttachment.ownerPlayer;
        cardToken.cCardToken.controllerPlayer = attachment.cAttachment.controllerPlayer;
        cardToken.cCardToken.location = attachment.cAttachment.cardToken.cCardToken.location;
        cardToken.cCardToken.isFaceUp = attachment.cAttachment.isFaceUp;
        cardToken.cCardToken.isBowed = attachment.cAttachment.isBowed;
        cardToken.cDropArea.target = cardToken.cCardToken;
        cardToken.cDropArea.isPropagate = true;
        cardToken.init();

        cardToken.cCardToken.setEnabled( attachment.cAttachment.controllerPlayer == player_type.PLAYER );

        return cardToken;
    }

    public createGenericEnemy( oid: string, controllerPlayer: player_type, location: location_type ): GameObject
    {
        let result: GameObject = new GameObject( [ 
            new CContainer(), 
            new CCardToken(),
            new CDraggable(),
            new CDropArea(), 
            new CCardTokenPoiReceptor(),
            new CShareableGameElement() ] );
        result.oid = oid;
        result.cDraggable.dragShadowTextureCreator = result.cCardToken;
        result.cCardToken.frontSideCardId = "999001";
        result.cCardToken.backSideCardId = "999003";
        result.cCardToken.ownerPlayer = player_type.SAURON;
        result.cCardToken.controllerPlayer = controllerPlayer;
        result.cCardToken.location = location;
        result.cCardToken.isFaceUp = true;
        result.cDropArea.target = result.cCardToken;
        result.cDropArea.isPropagate = true;
        result.init();

        return result;
    }
}