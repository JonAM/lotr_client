import GameObject from "./GameObject";
import CContainer from "./component/pixi/CContainer";
import { ICard } from "./CardDB";
import CTargetReceptor from "./component/world/CTargetReceptor";
import CHeroSide from "./component/card/token/side/CHeroSide";
import CAllySide from "./component/card/token/side/CAllySide";
import CEnemySide from "./component/card/token/side/CEnemySide";
import CLocationSide from "./component/card/token/side/CLocationSide";
import CQuestSide from "./component/card/token/side/CQuestSide";
import CObjectiveSide from "./component/card/token/side/CObjectiveSide";
import CBackSide from "./component/card/token/side/CBackSide";
import CQuestIntroSide from "./component/card/token/side/CQuestIntroSide";
import ServiceLocator from "../ServiceLocator";
import CTargetSelector, { target_selection_type } from "./component/input/CTargetSelector";
import CShareableGameElement from "./component/CShareableGameElement";
import CSideQuestSide from "./component/card/token/side/CSideQuestSide";


export default class CardTokenSideFactory
{
    public create( cardId: string, cardToken: GameObject ): GameObject
    {
        let result: GameObject = new GameObject( [ new CContainer() ] );
        result.oid = cardToken.oid + "_" + cardId;
        const kCardInfo: ICard = ServiceLocator.cardDb.find( cardId );
        switch ( kCardInfo.type_code )
        {
            case "hero": 
            case "objective-hero": { result.add( new CHeroSide() ); break; }

            case "ally": 
            case "objective-ally": 
            case "ship-objective": { result.add( new CAllySide() ); break; }

            case "player-side-quest":
            case "encounter-side-quest": { result.add( new CSideQuestSide() ); break; }

            case "enemy": 
            case "ship-enemy": { result.add( new CEnemySide() ); break; }

            case "location": 
            case "objective-location": { result.add( new CLocationSide() ); break; }

            case "objective":
            case "treachery":
            case "treasure":
            case "attachment":
            case "event": { result.add( new CObjectiveSide() ); break; }

            case "quest-intro": { result.add( new CQuestIntroSide() ); break; }
            case "quest": { result.add( new CQuestSide() ); break; }

            case "player-back": 
            case "encounter-back": { result.add( new CBackSide() ); break; }
        }
        result.add( new CTargetSelector() );
        result.add( new CTargetReceptor() );
        result.add( new CShareableGameElement() );
        result.cCardTokenSide.cardToken = cardToken;
        result.cCardTokenSide.cardId = cardId;
        result.cTargetSelector.receptor = result;
        result.cTargetReceptor.validTargetSelections = this.createValidTargetSelections( kCardInfo.type_code );
        result.init();

        return result;
    }

        private createValidTargetSelections( typeCode: string ): Array<target_selection_type>
        {
            let result: Array<target_selection_type> = null;

            switch ( typeCode )
            {
                case "hero": 
                case "ally": 
                case "ship-objective":
                case "objective-hero":
                case "objective-ally": { result = [ target_selection_type.TARGET, target_selection_type.EQUIP, target_selection_type.UNDERNEATH, target_selection_type.SAURON_ATTACK ]; break; }

                case "objective-location":
                case "objective": 
                case "location":
                case "event":
                case "attachment":
                case "treasure":
                case "treachery":
                case "player-side-quest":
                case "encounter-side-quest":
                case "quest-intro":
                case "quest": 
                case "player-back":
                case "encounter-back": { result = [ target_selection_type.TARGET, target_selection_type.EQUIP, target_selection_type.UNDERNEATH ]; break; }

                case "ship-enemy":
                case "enemy": { result = [ target_selection_type.TARGET, target_selection_type.EQUIP, target_selection_type.SHADOW_CARD, target_selection_type.UNDERNEATH, target_selection_type.PLAYER_ATTACK ]; break; }
            }

            return result;
        }
}