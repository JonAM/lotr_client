import Signal from "../lib/signals/Signal";
import ServiceLocator from "../ServiceLocator";
import { view_layer_id } from "../service/ViewManager";


export default class KeyBindingManager
{
    // #region Attributes //

    // private:

    private _bindings: IKeyBindings = null;
    private _isEnabled: boolean = true;

    // Binded functions.
    private _bfOnDocumentKeyDown: ( ev: KeyboardEvent) => any = null;
    private _bfOnDocumentKeyUp: ( ev: KeyboardEvent) => any = null;

    // Signals.
    private _onKeyDown: Signal = new Signal();
    private _onKeyUp: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public set bindings( value: IKeyBindings ) { this._bindings = value; }

    // Signals.
    public get onKeyDown(): Signal { return this._onKeyDown; }
    public get onKeyUp(): Signal { return this._onKeyUp; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() 
    {
        this._bfOnDocumentKeyDown = this.onDocument_KeyDown.bind( this );
        this._bfOnDocumentKeyUp = this.onDocument_KeyUp.bind( this );
    }

    public init(): void
    {
        console.assert( this._bindings != null, "KeyBindingManager.ts :: init() :: this._bindings cannot be null." );

        document.addEventListener( "keydown", this._bfOnDocumentKeyDown );
        document.addEventListener( "keyup", this._bfOnDocumentKeyUp );
    }

    public end(): void 
    {
        this._onKeyUp.removeAll();
        this._onKeyDown.removeAll();

        document.removeEventListener( "keydown", this._bfOnDocumentKeyDown );
        document.removeEventListener( "keyup", this._bfOnDocumentKeyUp );
    }

    public setEnabled( isEnabled: boolean ): void
    {
        this._isEnabled = isEnabled;
    }

    public setBinding( bindingId: string, code: string ): void
    {
        for ( let bid in this._bindings )
        {
            if ( this._bindings[ bid ] == code )
            {
                this._bindings[ bid ] = null;
            }
        }

        this._bindings[ bindingId ] = code;
        localStorage.setItem( "key_bindings", JSON.stringify( this._bindings ) );
    }

    public getBinding( bindingId: string ): string
    {
        let result: string = null;

        if ( !this._bindings[ bindingId ] )
        {
            result = jQuery.i18n( "NONE" );
        }
        else
        {
            result = this._bindings[ bindingId ] as string;
        }

        return result;
    }

    // private:

    private findBindingId( code: string ): string
    {
        let result: string = null;

        for ( let bid in this._bindings )
        {
            if ( this._bindings[ bid ] == code )
            {
                result = bid;
                break;
            }
        }

        return result;
    }

    // #endregion //


    // #region Input Callbacks //

    private onDocument_KeyDown( event: KeyboardEvent ): void
    {
        if ( this._isEnabled 
            && ServiceLocator.viewManager.findViewCount( view_layer_id.POPUP ) == 0
            && ServiceLocator.viewManager.findViewCount( view_layer_id.TOPMOST ) == 0 )
        {
            const kBindingId: string = this.findBindingId( event.code );
            if ( kBindingId )
            {
                this._onKeyDown.dispatch( kBindingId );
            }
        }
    }

    private onDocument_KeyUp( event: KeyboardEvent ): void
    {
        if ( this._isEnabled 
            && ServiceLocator.viewManager.findViewCount( view_layer_id.POPUP ) == 0
            && ServiceLocator.viewManager.findViewCount( view_layer_id.TOPMOST ) == 0 )
        {
            const kBindingId: string = this.findBindingId( event.code );
            if ( kBindingId )
            {
                this._onKeyUp.dispatch( kBindingId );
            }
        }
    }

    // #endregion //
}

export interface IKeyBindings
{
    chat: string;
    hand: string;
    poiMenu: string;
    forceDrag: string;
    endTurn: string;
}