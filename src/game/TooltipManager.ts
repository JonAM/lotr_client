import ServiceLocator from "../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "./GameObject";
import CGraphics from "./component/pixi/CGraphics";
import { view_layer_id } from "../service/ViewManager";
import PointerInputController from "./input/PointerInputController";


export default class TooltipManager
{
    // #region Attributes //

    // private:

    private _stage: PIXI.Container = null;
    private _tooltipLayer: GameObject = null;

    private _tooltip: GameObject = null;
    private _tooltipText: PIXI.Text = null;
    private _tooltipDescription: PIXI.Text = null;
    private _spawnTimeout: number = null;
    private _tooltipReceptor: GameObject = null;
    private _isEnabled: boolean = true;

    // #endregion //


    // #region Properties //
    
    public set stage( value: PIXI.Container ) { this._stage = value; }
    public set tooltipLayer( value: GameObject ) { this._tooltipLayer = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() {}

    public init(): void
    {
        console.assert( this._stage != null, "TooltipManager.ts :: init() :: this._stage cannot be null." );
        console.assert( this._tooltipLayer != null, "TooltipManager.ts :: init() :: this._tooltipLayer cannot be null." );

        this._tooltip = new GameObject( [ new CGraphics() ] );
        //
        let textStyle: PIXI.TextStyle = ServiceLocator.game.textStyler.normal;
        textStyle.wordWrap = true;
        textStyle.wordWrapWidth = 300;
        this._tooltipText = new PIXI.Text( "", textStyle );
        this._tooltipText.position.set( 10, 10 );
        this._tooltip.cContainer.c.addChild( this._tooltipText );
        //
        let descriptionStyle: PIXI.TextStyle = ServiceLocator.game.textStyler.tooltipDescription;
        descriptionStyle.wordWrap = true;
        descriptionStyle.wordWrapWidth = 300;
        this._tooltipDescription = new PIXI.Text( "", descriptionStyle );
        this._tooltipDescription.position.set( 10, 10 );
        this._tooltip.cContainer.c.addChild( this._tooltipDescription );
        //
        this._tooltip.cContainer.c.visible = false;
        this._tooltipLayer.cContainer.addChild( this._tooltip );

        // Listen to events.
        if ( ServiceLocator.game.inputController instanceof PointerInputController )
        {
            this._stage.on( "pointermove", this.onStage_PointerMove, this );
            this._stage.on( "pointerup", this.onStage_PointerMove, this );
        }
        ServiceLocator.viewManager.onViewAdded.add( this.onViewCount_Changed, this );
        ServiceLocator.viewManager.onViewRemoved.add( this.onViewCount_Changed, this );
    }

    public end(): void
    {
        if ( this._tooltipReceptor )
        {
            this.onTooltipReceptor_Invalidated();
        }
        this._tooltipReceptor = null;

        this._tooltip.end();
        this._tooltip = null;
        this._tooltipText = null;
        this._tooltipDescription = null;

        if ( this._spawnTimeout )
        {
            window.clearTimeout( this._spawnTimeout );
            this._spawnTimeout = null;
        }

        this._tooltipLayer = null;

        // Listen to events.
        if ( ServiceLocator.game.inputController instanceof PointerInputController )
        {
            this._stage.off( "pointermove", this.onStage_PointerMove, this );
            this._stage.off( "pointerup", this.onStage_PointerMove, this );
        }
        this._stage = null;
        ServiceLocator.viewManager.onViewAdded.remove( this.onViewCount_Changed, this );
        ServiceLocator.viewManager.onViewRemoved.remove( this.onViewCount_Changed, this );
    }

    public validateCurrentTooltipReceptor( sourceBounds: PIXI.Rectangle ): void
    {
        const kGlobal: PIXI.Point = ServiceLocator.game.app.renderer.plugins.interaction.mouse.global;
        if ( sourceBounds.contains( kGlobal.x, kGlobal.y ) && this._tooltipReceptor )
        {
            this.onTooltipReceptor_Invalidated();
        }
    }

    public triggerTooltip( tooltipReceptor: GameObject, global: PIXI.Point ): void
    {
        this._tooltipReceptor = tooltipReceptor;
        this.showTooltip( tooltipReceptor, global );
    }

    public clearTooltip(): void
    {
        if ( this._spawnTimeout )
        {
            window.clearTimeout( this._spawnTimeout );
            this._spawnTimeout = null;
        }

        if ( this._tooltipReceptor )
        {
            this.onTooltipReceptor_Invalidated();
        }
    }

    // private:

    private showTooltip( tooltipReceptor: GameObject, global: PIXI.Point ): void
    {
        this._tooltipText.text = tooltipReceptor.cTooltipReceptor.text;
        tooltipReceptor.onEnded.addOnce( this.onTooltipReceptor_Invalidated, this );
        tooltipReceptor.cContainer.onTransformChanged.add( this.onTooltipReceptorTransform_Changed, this );
        tooltipReceptor.cContainer.onVisibleChanged.addOnce( this.onTooltipReceptor_Invalidated, this );

        if ( tooltipReceptor.cTooltipReceptor.description )
        {
            this._tooltipDescription.text = tooltipReceptor.cTooltipReceptor.description;
            this._tooltipDescription.y = this._tooltipText.y + this._tooltipText.height + 5;
        }
        else
        {
            this._tooltipDescription.text = "";
            this._tooltipDescription.y = 0;
        }

        this._tooltip.cGraphics.g.clear();
        this._tooltip.cGraphics.g.lineStyle( 1, 0x00000, 1 );
        this._tooltip.cGraphics.g.beginFill( 0xefe4b0, 1 );
        this._tooltip.cGraphics.g.drawRect( 0, 0, Math.max( this._tooltipText.width, this._tooltipDescription.width ) + 20, this._tooltip.cGraphics.g.height + ( this._tooltipDescription.text == "" ? 10 : 20 ) );
        this._tooltip.cGraphics.g.endFill();
        
        this._tooltip.cContainer.c.visible = true;

        const kWorldWidth: number = ServiceLocator.game.world.cContainer.c.width;
        const kWorldHeight: number = ServiceLocator.game.world.cContainer.c.height;
        let tooltipPosition: PIXI.Point = new PIXI.Point( global.x - this._tooltip.cContainer.c.width, global.y - this._tooltip.cContainer.c.height );
        let isValidPosition: boolean = tooltipPosition.x >= 0 && tooltipPosition.y >= 0;
        if ( !isValidPosition )
        {
            tooltipPosition.x = global.x;
            isValidPosition = tooltipPosition.x + this._tooltip.cContainer.c.width <= kWorldWidth 
                && tooltipPosition.y >= 0;
        }
        if ( !isValidPosition )
        {
            tooltipPosition.set( global.x - this._tooltip.cContainer.c.width, global.y );
            isValidPosition = tooltipPosition.x >= 0 
                && tooltipPosition.y + this._tooltip.cContainer.c.height <= kWorldHeight;
        }
        if ( !isValidPosition )
        {
            tooltipPosition.x = global.x;
        }

        this._tooltip.cContainer.c.position.set( tooltipPosition.x, tooltipPosition.y );
    }
    
    private findTooltipReceptor( root: PIXI.Container, global: PIXI.Point ): Object
    {
        let result: Object = { tooltipReceptor: null, isCancel: false };

        for ( let i: number = root.children.length - 1; i >= 0; --i )
        {
            let child: PIXI.DisplayObject = root.getChildAt( i );
            if ( child.visible && child.getBounds().contains( global.x, global.y ) )
            {
                if ( child instanceof PIXI.Container )
                {
                    result = this.findTooltipReceptor( child, global );
                }
            }

            if ( result[ "isCancel" ] || result[ "tooltipReceptor" ] )
            {
                break;
            }
        }

        let go: GameObject = root[ "go" ];
        if ( go && go.cTooltipReceptor && go.cTooltipReceptor.isEnabled && !result[ "tooltipReceptor" ] )
        {
            result[ "tooltipReceptor" ] = go;
        }
        else if ( root.interactive )
        {
            result[ "isCancel" ] = true;
        }

        return result;
    }

    // #endregion //


    // #region Input Callbacks //

    private onStage_PointerMove( event: PIXI.InteractionEvent ): void
    {
        if ( !this._isEnabled ) { return; }

        if ( this._tooltipReceptor )
        {
            this.onTooltipReceptor_Invalidated();
        }

        if ( this._spawnTimeout )
        {
            window.clearTimeout( this._spawnTimeout );
        }
        this._spawnTimeout = window.setTimeout( this.onSpawnTooltip_Timeout.bind( this ), 750 );
    }

    // #endregion //


    // #region Other Callbacks //

    private onSpawnTooltip_Timeout(): void
    {
        const kGlobal: PIXI.Point = ServiceLocator.game.app.renderer.plugins.interaction.mouse.global;
        this._tooltipReceptor = this.findTooltipReceptor( ServiceLocator.game.root.cContainer.c, kGlobal )[ "tooltipReceptor" ];
        if ( this._tooltipReceptor )
        {
            this.showTooltip( this._tooltipReceptor, kGlobal );
        }
    }

    private onTooltipReceptor_Invalidated(): void
    {
        this._tooltipReceptor.onEnded.remove( this.onTooltipReceptor_Invalidated, this );
        this._tooltipReceptor.cContainer.onTransformChanged.remove( this.onTooltipReceptorTransform_Changed, this );
        this._tooltipReceptor.cContainer.onVisibleChanged.remove( this.onTooltipReceptor_Invalidated, this );
        
        this._tooltipReceptor = null;

        this._tooltip.cContainer.c.visible = false;
    }

    private onTooltipReceptorTransform_Changed(): void
    {
        const kGlobal: PIXI.Point = ServiceLocator.game.app.renderer.plugins.interaction.mouse.global;
        if ( !this._tooltipReceptor.cContainer.c.getBounds().contains( kGlobal.x, kGlobal.y ) )
        {
            this.onTooltipReceptor_Invalidated();
        }
    }

    private onViewCount_Changed(): void
    {
        if ( ServiceLocator.viewManager.findViewCount( view_layer_id.TOPMOST ) > 0 
            || ServiceLocator.viewManager.findViewCount( view_layer_id.POPUP ) > 0 )
        {
            if ( this._isEnabled )
            {
                this._isEnabled = false;

                if ( this._tooltipReceptor )
                {
                    this.onTooltipReceptor_Invalidated();
                }
            }
        }
        else 
        {
            if ( !this._isEnabled )
            {
                this._isEnabled = true;
            }
        }
    }

    // #endregion //
}