export default interface IDragShadowTextureCreator
{
    createDragShadowTexture(): PIXI.Texture;
}