import ServiceLocator from "../ServiceLocator";
import * as PIXI from "pixi.js";

import GameObject from "./GameObject";
import CContainer from "./component/pixi/CContainer";
import CDropArea from "./component/input/CDropArea";
import CGameLayer from "./component/world/CGameLayer";
import Component from "./component/Component";
import Signal from "../lib/signals/Signal";


export default class DragShadowManager
{
    // #region Attributes //

    // private:

    private _stage: PIXI.Container = null;
    
    private _dragShadow: GameObject = null;
    private _isDetectingDrag: boolean = null;
    private _isDragging: boolean = null;

    // Signals.
    private _onDragShadowMoved: Signal = new Signal();

    // #endregion //


    // #region Properties //
    
    public get dragShadow(): GameObject { return this._dragShadow; }
    public get isDetectingDrag(): boolean { return this._isDetectingDrag; }
    public get isDragging(): boolean { return this._isDragging; }

    public set stage( value: PIXI.Container ) { this._stage = value; }
    public set isDetectingDrag( value: boolean ) { this._isDetectingDrag = value; }
    public set isDragging( value: boolean ) { this._isDragging = value; }

    // Signals.
    public get onDragShadowMoved(): Signal { return this._onDragShadowMoved; }

    // #endregion //


    // #region Methods //

    // public:

    public init(): void
    {
        console.assert( this._stage != null, "DragShadowManager.ts :: init() :: this._stage cannot be null." );
    
        this._isDragging = false;

        this._stage.interactive = true;

        // Listen to events.
        this._stage.on( "pointerdown", this.onStage_PointerDown.bind( this ) );
        this._stage.on( "pointerup", this.onStage_PointerUp.bind( this ) );
        this._stage.on( "pointermove", this.onStage_PointerMove.bind( this ) );
        ServiceLocator.game.onOngoingInteractionCanceled.add( this.onStage_PointerUp, this );
    }

    public end(): void
    {
        this._onDragShadowMoved.removeAll();

        this._isDragging = null;

        // Cleanup events.
        this._stage.off( "pointerdown", this.onStage_PointerDown.bind( this ) );
        this._stage.off( "pointerup", this.onStage_PointerUp.bind( this ) );
        this._stage.off( "pointermove", this.onStage_PointerMove.bind( this ) );
        ServiceLocator.game.onOngoingInteractionCanceled.remove( this.onStage_PointerUp, this );

        this._stage = null;
    }

    public update( dt: number ): void
    {
        if ( this._dragShadow )
        {
            this._dragShadow.cDampedSpring.update( dt );
        }
    }

    public setDragShadow( dragShadow: GameObject ): void
    {
        this._dragShadow = dragShadow;
        if ( this._dragShadow )
        {
            this._dragShadow.cDampedSpring.reset();
            this._dragShadow.cDampedSpring.setTarget( new PIXI.Point( 
                this._dragShadow.cContainer.c.position.x,
                this._dragShadow.cContainer.c.position.y ) );
        }
    }

    public forceStagePointerUp(): void
    {
        this.onStage_PointerUp( null );
    }

    public forceStagePointerMove(): void
    {
        let fakeEvent: PIXI.InteractionEvent = new PIXI.InteractionEvent();
        fakeEvent.data = new PIXI.InteractionData();
        fakeEvent.data.global = ServiceLocator.game.app.renderer.plugins.interaction.mouse.global;
        this.onStage_PointerMove( fakeEvent );
    }

    // #endregion //


    // #region Input Callbacks //

    private onStage_PointerDown( event: PIXI.InteractionEvent ): void
    {
        if ( ServiceLocator.game.radialMenu.cContainer.c.visible
            && !ServiceLocator.game.radialMenu.cContainer.c.getBounds().contains( event.data.global.x, event.data.global.y )
            /*&& !ServiceLocator.game.radialMenu.cRadialMenu.target.cContainer.c.getBounds().contains( event.data.global.x, event.data.global.y )*/ )
        {
            ServiceLocator.game.cRadialMenu.hide();
        }

        if ( ServiceLocator.game.cContextMenu.go.cContainer.c.visible
            && !ServiceLocator.game.cContextMenu.go.cContainer.c.getBounds().contains( event.data.global.x, event.data.global.y ) )
        {
            ServiceLocator.game.cContextMenu.close();
        }
    }

    private onStage_PointerUp( event: PIXI.InteractionEvent ): void
    {
        if ( this._dragShadow )
        {
            this._dragShadow.cDragShadow.from.cDraggable.cancel();
            this._dragShadow = null
        }
        
        this._isDetectingDrag = false;
        window.setTimeout( () => { this._isDragging = false; } ); // so tap events are not fired if the drag starts and ends on the same button.
    }

    private onStage_PointerMove( event: PIXI.InteractionEvent ): void
    {
        if ( this._dragShadow ) 
        {
            if ( ServiceLocator.game.radialMenu.cContainer.c.visible )
            {
                ServiceLocator.game.cRadialMenu.hide();
            }
            if ( ServiceLocator.game.cardPreview.cCardPreview.isVisible() )
            {
                ServiceLocator.game.cardPreview.cCardPreview.hide();
            }

            this._dragShadow.cDampedSpring.setTarget( new PIXI.Point( 
                event.data.global.x - this._dragShadow.cDragShadow.from.cDraggable.offset.x * this._dragShadow.cContainer.c.width, 
                event.data.global.y - this._dragShadow.cDragShadow.from.cDraggable.offset.y * this._dragShadow.cContainer.c.height ) );
        
            this._onDragShadowMoved.dispatch( this._dragShadow, event.data.global );
        }
    }

    // #endregion //
}