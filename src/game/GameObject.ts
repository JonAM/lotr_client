import ServiceLocator from "../ServiceLocator";

import Component from "./component/Component";
import CCardToken from "./component/card/CCardToken";
import CActorArea from "./component/world/CActorArea";
import CContainer from "./component/pixi/CContainer";
import CDraggable from "./component/input/CDraggable";
import CGameWorld from "./component/world/CGameWorld";
import CSprite from "./component/pixi/CSprite";
import CCardPreview from "./component/input/CCardPreview";
import CButton from "./component/input/CButton";
import CTokenCounter from "./component/ui/CTokenCounter";
import CPlayerArea, { player_type } from "./component/world/CPlayerArea";
import CTilingSprite from "./component/pixi/CTilingSprite";
import CCardPreviewable from "./component/input/CCardPreviewable";
import CGraphics from "./component/pixi/CGraphics";
import CAttachment from "./component/card/token/CAttachment";
import CDropArea from "./component/input/CDropArea";
import CDragDetector from "./component/input/CDragDetector";
import CDeckIndicator from "./component/ui/indicator/CDeckIndicator";
import CActionLogger from "./component/ui/right_menu/CActionLogger";
import CRadialMenu from "./component/ui/CRadialMenu";
import CCardMini from "./component/card/CCardMini";
import CIndicatorLock from "./component/ui/indicator/CIndicatorLock";
import CIndicator from "./component/ui/CIndicator";
import CTokenIndicator from "./component/ui/indicator/CTokenIndicator";
import CViewer from "./component/ui/CViewer";
import CAnchorY from "./component/ui/CAnchorY";
import CMaximizedView from "./component/ui/viewer/CMaximizedView";
import CMinimizedView from "./component/ui/viewer/CMinimizedView";
import CContextMenu from "./component/ui/CContextMenu";
import CScrollY from "./component/ui/CScrollY";
import CCardActivationArea from "./component/world/CCardActivationArea";
import CCard from "./component/card/CCard";
import CCardView from "./component/ui/viewer/CCardView";
import CDeckBottomMarker from "./component/ui/viewer/card/CDeckBottomMarker";
import CTextPreview from "./component/input/CTextPreview";
import CTextPreviewable from "./component/input/CTextPreviewable";
import CDetailBar from "./component/card/token/CDetailBar";
import CDragShadow from "./component/input/CDragShadow";
import CGameLayer from "./component/world/CGameLayer";
import CPoi from "./component/world/CPoi";
import CPoiReceptor from "./component/world/CPoiReceptor";
import CCardPoiReceptor from "./component/world/poi_receptor/CCardPoiReceptor";
import CCardTokenPoiReceptor from "./component/world/poi_receptor/CCardTokenPoiReceptor";
import CPoiMenu from "./component/ui/CPoiMenu";
import CAttachmentPoiReceptor from "./component/world/poi_receptor/CAttachmentPoiReceptor";
import CDeckIndicatorPoiReceptor from "./component/world/poi_receptor/CDeckIndicatorPoiReceptor";
import CDampedSpring from "./component/CDampedSpring";
import Signal from "../lib/signals/Signal";
import CLogActionPreview from "./component/input/CLogActionPreview";
import CLogActivePlayer from "./component/ui/right_menu/action_log/CLogActivePlayer";
import CLogAction from "./component/ui/right_menu/action_log/CLogAction";
import CLogPhaseStart from "./component/ui/right_menu/action_log/CLogPhaseStart";
import CLogActionPreviewable from "./component/input/CLogActionPreviewable";
import CLogCardFlip from "./component/ui/right_menu/action_log/CLogCardFlip";
import CLogCounter from "./component/ui/right_menu/action_log/CLogCounter";
import CLogSequence from "./component/ui/right_menu/action_log/CLogSequence";
import CLogSingleIcon from "./component/ui/right_menu/action_log/CLogSingleIcon";
import CLogLocation from "./component/world/CLogLocation";
import CTokenIndicatorPoiReceptor from "./component/world/poi_receptor/CTokenIndicatorPoiReceptor";
import CPoiMenuItem from "./component/ui/CPoiMenuItem";
import CHighlightPoiReceptor from "./component/world/poi_receptor/CHighlightPoiReceptor";
import CTooltipReceptor from "./component/ui/CTooltipReceptor";
import CPlayerSideControls from "./component/ui/CPlayerSideControls";
import CSauronSideControls from "./component/ui/CSauronSideControls";
import CSauronArea from "./component/world/CSauronArea";
import CEndTurnButton from "./component/ui/CEndTurnButton";
import CActiveLocationHolder from "./component/ui/CActiveLocationHolder";
import CQuestHolder from "./component/ui/CQuestHolder";
import CActivatedCard from "./component/card/CActivatedCard";
import CTargetSelector from "./component/input/CTargetSelector";
import CTargetReceptor from "./component/world/CTargetReceptor";
import CLogTargetSelection from "./component/ui/right_menu/action_log/CLogTargetSelection";
import CScrollX from "./component/ui/CScrollX";
import CStagingThreatCounter from "./component/ui/CStagingThreatCounter";
import CPlayerWillpowerCounter from "./component/ui/CPlayerWillpowerCounter";
import CSauronActorArea from "./component/world/actor_area/CSauronActorArea";
import CPlayerActorArea from "./component/world/actor_area/CPlayerActorArea";
import CEngagedActorArea from "./component/world/actor_area/CEngagedActorArea";
import CUnderneathButton from "./component/ui/CUnderneathButton";
import CCardPresentationControls from "./component/ui/CCardPresentationControls";
import CShadowCardMini from "./component/card/CShadowCardMini";
import CAttackBindingCounter from "./component/ui/CAttackBindingCounter";
import CCardTrayView from "./component/ui/viewer/CCardTrayView";
import COptionsMenu from "./component/ui/COptionsMenu";
import CImagePreview from "./component/input/CImagePreview";
import CImagePreviewable from "./component/input/CImagePreviewable";
import CPhaseDiagramButton from "./component/ui/CPhaseDiagramButton";
import CCardPresentationDefaultAction from "./component/ui/CCardPresentationDefaultAction";
import CFirstPlayerTokenPoiReceptor from "./component/world/poi_receptor/CFirstPlayerTokenPoiReceptor";
import CFirstPlayerToken from "./component/world/CFirstPlayerToken";
import CIndicatorBusy from "./component/ui/indicator/CIndicatorBusy";
import CActivePlayerButton from "./component/ui/CActivePlayerButton";
import CAllyActivatedCard from "./component/card/CAllyActivatedCard";
import CShareableGameElement from "./component/CShareableGameElement";
import CQuestVariation from "./component/ui/CQuestVariation";
import CFlyingOrb from "./component/world/CFlyingOrb";
import CCardTokenSide from "./component/card/token/CCardTokenSide";
import CBackSide from "./component/card/token/side/CBackSide";
import CHeroSide from "./component/card/token/side/CHeroSide";
import CAllySide from "./component/card/token/side/CAllySide";
import CEnemySide from "./component/card/token/side/CEnemySide";
import CLocationSide from "./component/card/token/side/CLocationSide";
import CObjectiveSide from "./component/card/token/side/CObjectiveSide";
import CQuestSide from "./component/card/token/side/CQuestSide";
import CQuestIntroSide from "./component/card/token/side/CQuestIntroSide";
import CSideQuestSide from "./component/card/token/side/CSideQuestSide";
import CIsolatedArea from "./component/world/custom_area/CIsolatedArea";
import CGameLayerProvider from "./component/world/CGameLayerProvider";
import CRiddleArea from "./component/world/custom_area/CRiddleArea";
import CPursuitArea from "./component/world/custom_area/CPursuitArea";
import CCircuit from "./component/world/custom_area/CCircuit";
import CIslandMap from "./component/world/custom_area/CIslandMap";
import CPursuitThreatCounter from "./component/ui/CPursuitThreatCounter";
import CCustomArea from "./component/world/CCustomArea";
import CPursuitBtn from "./component/world/custom_area/pursuit/CPursuitBtn";
import CTrackStage from "./component/world/custom_area/circuit/CTrackStage";
import CChariotToken from "./component/world/custom_area/circuit/CChariotToken";
import CIslandLocationHolder from "./component/ui/CIslandLocationHolder";
import CVictoryBtn from "./component/card/token/CVictoryBtn";
import CPlayerToken from "./component/world/CPlayerToken";
import CMultiItemSelector from "./component/ui/CMultiItemSelector";
import CGameModifier from "./component/card/CGameModifier";
import CHeading from "./component/world/custom_area/CHeading";
import CInvestigationList from "./component/world/custom_area/CInvestigationList";
import CCompass from "./component/world/custom_area/CCompass";
import CTreasureList from "./component/world/custom_area/CTreasureList";
import CSetupInstructions from "./component/world/custom_area/CSetupInstructions";
import CCustomPanelManager from "./component/ui/CCustomPanelManager";
import CCampaignLog from "./component/world/custom_area/CCampaignLog";
import CSplitSauronArea from "./component/world/custom_area/CSplitSauronArea";
import CPitArea from "./component/world/custom_area/CPitArea";
import CDorCuartholArea from "./component/world/custom_area/CDorCuartholArea";
import CDorCuartholLocationHolder from "./component/ui/CDorCuartholLocationHolder";
import CActorTab from "./component/world/CActorTab";
import CViewContextBtn from "./component/ui/viewer/CViewContextBtn";
import CAnimatedFrame from "./component/ui/CAnimatedFrame";


export default class GameObject
{
    // #region Attributes //

    // private:

    private _oid: string = null;
    private _arrComponent: Array<Component> = new Array<Component>();

    // Signals.
    private _onEnded: Signal = new Signal();

    // Global.
    private static _gOidCounter: number = 0;
    private static _gLookupTable: Map<string, GameObject> = new Map<string, GameObject>(); 

    // public:

    // Components.
    public cActionLogger?: CActionLogger = null;
    public cActivatedCard?: CActivatedCard = null;
    public cActiveLocationHolder?: CActiveLocationHolder = null;
    public cActivePlayerButton?: CActivePlayerButton = null;
    public cActorArea?: CActorArea = null;
    public cActorTab?: CActorTab = null;
    public cAllyActivatedCard?: CAllyActivatedCard = null;
    public cAllySide?: CAllySide = null;
    public cAnchorY?: CAnchorY = null;
    public cAnimatedFrame?: CAnimatedFrame = null;
    public cAttachment?: CAttachment = null;
    public cAttachmentPoiReceptor?: CAttachmentPoiReceptor = null;
    public cAttackBindingCounter?: CAttackBindingCounter = null;
    public cBackSide?: CBackSide = null;
    public cButton?: CButton = null;
    public cCampaignLog?: CCampaignLog = null;
    public cCard?: CCard = null;
    public cCardActivationArea?: CCardActivationArea = null;
    public cCardMini?: CCardMini = null;
    public cCardPoiReceptor?: CCardPoiReceptor = null;
    public cCardPresentationControls?: CCardPresentationControls = null;
    public cCardPresentationDefaultAction?: CCardPresentationDefaultAction = null;
    public cCardPreview?: CCardPreview = null;
    public cCardPreviewable?: CCardPreviewable = null;
    public cCardToken?: CCardToken = null;
    public cCardTokenPoiReceptor?: CCardTokenPoiReceptor = null;
    public cCardTokenSide?: CCardTokenSide = null;
    public cCardTrayView?: CCardTrayView = null;
    public cCardView?: CCardView = null;
    public cChariotToken?: CChariotToken = null;
    public cCircuit?: CCircuit = null;
    public cCompass?: CCompass = null;
    public cContainer?: CContainer = null;
    public cContextMenu?: CContextMenu = null;
    public cCustomArea?: CCustomArea = null;
    public cCustomPanelManager?: CCustomPanelManager = null;
    public cDampedSpring?: CDampedSpring = null;
    public cDeckBottomMarker?: CDeckBottomMarker = null;
    public cDeckIndicator?: CDeckIndicator = null;
    public cDeckIndicatorPoiReceptor?: CDeckIndicatorPoiReceptor = null;
    public cDetailBar?: CDetailBar = null;
    public cDorCuartholArea?: CDorCuartholArea = null;
    public cDorCuartholLocationHolder?: CDorCuartholLocationHolder = null;
    public cDragDetector?: CDragDetector = null;
    public cDraggable?: CDraggable = null;
    public cDragShadow?: CDragShadow = null;
    public cDropArea?: CDropArea = null;
    public cEngagedActorArea?: CEngagedActorArea = null;
    public cEndTurnButton?: CEndTurnButton = null;
    public cEnemySide?: CEnemySide = null;
    public cFirstPlayerToken?: CFirstPlayerToken = null;
    public cFirstPlayerTokenPoiReceptor?: CFirstPlayerTokenPoiReceptor = null;
    public cFlyingOrb?: CFlyingOrb = null;
    public cGameLayer?: CGameLayer = null;
    public cGameLayerProvider?: CGameLayerProvider = null;
    public cGameModifier?: CGameModifier = null;
    public cGameWorld?: CGameWorld = null;
    public cGraphics?: CGraphics = null;
    public cHeading?: CHeading = null;
    public cHeroSide?: CHeroSide = null;
    public cHighlightPoiReceptor?: CHighlightPoiReceptor = null;
    public cImagePreview?: CImagePreview = null;
    public cImagePreviewable?: CImagePreviewable = null;
    public cIndicator?: CIndicator = null;
    public cIndicatorLock?: CIndicatorLock = null;
    public cIndicatorBusy?: CIndicatorBusy = null;
    public cInvestigationList?: CInvestigationList = null;
    public cIslandLocationHolder?: CIslandLocationHolder = null;
    public cIslandMap?: CIslandMap = null;
    public cIsolatedArea?: CIsolatedArea = null;
    public cLocationSide?: CLocationSide = null;
    public cLogLocation?: CLogLocation = null;
    public cLogAction?: CLogAction = null;
    public cLogActionPreview?: CLogActionPreview = null;
    public cLogActionPreviewable?: CLogActionPreviewable = null;
    public cLogActivePlayer?: CLogActivePlayer = null;
    public cLogCardFlip?: CLogCardFlip = null;
    public cLogCounter?: CLogCounter = null;
    public cLogSequence?: CLogSequence = null;
    public cLogSingleIcon?: CLogSingleIcon = null;
    public cLogPhaseStart?: CLogPhaseStart = null;
    public cLogTargetSelection?: CLogTargetSelection = null;
    public cMaximizedView?: CMaximizedView = null;
    public cMultiItemSelector?: CMultiItemSelector = null;
    public cMinimizedView?: CMinimizedView = null;
    public cObjectiveSide?: CObjectiveSide = null;
    public cOptionsMenu?: COptionsMenu = null;
    public cPhaseDiagramButton?: CPhaseDiagramButton = null;
    public cPitArea?: CPitArea = null;
    public cPlayerActorArea?: CPlayerActorArea = null;
    public cPlayerArea?: CPlayerArea = null;
    public cPlayerSideControls?: CPlayerSideControls = null;
    public cPlayerToken?: CPlayerToken = null;
    public cPlayerWillpowerCounter?: CPlayerWillpowerCounter = null;
    public cPoi?: CPoi = null;
    public cPoiMenu?: CPoiMenu = null;
    public cPoiMenuItem?: CPoiMenuItem = null;
    public cPoiReceptor?: CPoiReceptor = null;
    public cPursuitArea?: CPursuitArea = null;
    public cPursuitBtn?: CPursuitBtn = null;
    public cPursuitThreatCounter: CPursuitThreatCounter = null;
    public cQuestHolder?: CQuestHolder = null;
    public cQuestSide?: CQuestSide = null;
    public cQuestIntroSide?: CQuestIntroSide = null;
    public cQuestVariation?: CQuestVariation = null;
    public cRadialMenu?: CRadialMenu = null;
    public cRiddleArea?: CRiddleArea = null;
    public cSauronActorArea?: CSauronActorArea = null;
    public cSauronArea?: CSauronArea = null;
    public cSauronSideControls?: CSauronSideControls = null;
    public cScrollX?: CScrollX = null;
    public cScrollY?: CScrollY = null;
    public cSetupInstructions?: CSetupInstructions = null;
    public cShadowCardMini?: CShadowCardMini = null;
    public cShareableGameElement?: CShareableGameElement = null;
    public cSideQuestSide?: CSideQuestSide = null;
    public cSplitSauronArea?: CSplitSauronArea = null;
    public cSprite?: CSprite = null;
    public cStagingThreatCounter?: CStagingThreatCounter = null;
    public cTargetReceptor?: CTargetReceptor = null;
    public cTargetSelector?: CTargetSelector = null;
    public cTextPreview?: CTextPreview = null;
    public cTextPreviewable?: CTextPreviewable = null;
    public cTilingSprite?: CTilingSprite = null;
    public cTokenCounter?: CTokenCounter = null;
    public cTokenIndicator?: CTokenIndicator = null;
    public cTokenIndicatorPoiReceptor?: CTokenIndicatorPoiReceptor = null;
    public cTooltipReceptor?: CTooltipReceptor = null;
    public cTrackStage?: CTrackStage = null;
    public cTreasureList?: CTreasureList = null;
    public cUnderneathButton?: CUnderneathButton = null;
    public cVictoryBtn?: CVictoryBtn = null;
    public cViewContextBtn?: CViewContextBtn = null;
    public cViewer?: CViewer = null;

    // #endregion //


    // #region Properties //

    public static get oidCounter(): number { return this._gOidCounter; }
    public get oid(): string { return this._oid; }
    public get components(): Array<Component> { return this._arrComponent; }
    
    public static set oidCounter( value: number ) { this._gOidCounter = value; }
    public set oid( value: string ) { this._oid = value; }

    // Signals.
    public get onEnded(): Signal { return this._onEnded; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor( arrComponent: Array<Component> = null ) 
    {
        if ( arrComponent != null )
        {
            for ( let component of arrComponent )
            {
                this.add( component );
            }
        }
    }

    public init(): void
    {
        if ( !this._oid )
        {
            do
            {
                this._oid = "o" + GameObject._gOidCounter.toString();
                GameObject._gOidCounter += 1;
            } 
            while ( GameObject._gLookupTable.has( this._oid ) );
        }
        GameObject._gLookupTable.set( this._oid, this );

        for ( let component of this._arrComponent )
        {
            component.init();
        }
    }

    public end(): void
    {
        this._onEnded.dispatch( this );
        this._onEnded.removeAll();

        for ( let i: number = this._arrComponent.length - 1; i >= 0; --i )
        {
            this._arrComponent[ i ].end();
        }
        for ( let component of this._arrComponent )
        {
            this.setComponentShortcut( component.id, null );
        }
        this._arrComponent = new Array<Component>();

        GameObject._gLookupTable.delete( this._oid );
    }

    public update( dt: number ): void
    {
        for ( let component of this._arrComponent )
        {
            component.update( dt);
        }
    }

    public add( component: Component ): void
    {
        component.go = this;
        
        this._arrComponent.push( component );

        this.setComponentShortcut( component.id, component );
    }

    public remove( componentId: string ): void
    {
        for ( let i: number = 0; i < this._arrComponent.length; ++i )
        {
            let component: Component = this._arrComponent[ i ];
            if ( component.id == componentId )
            {
                this.setComponentShortcut( component.id, null );

                component.end();
                this._arrComponent.splice( i, 1 );

                break;
            }
        }
    }

    public static find( oid: string ): GameObject
    {
        return GameObject._gLookupTable.get( oid );
    }

    public static has( oid: string ): boolean
    {
        return GameObject._gLookupTable.has( oid );
    }

    public static replace( oid: string, go: GameObject ): void
    {
        GameObject._gLookupTable.set( oid, go );
    }

    public static findPlayerSufix( playerType: player_type ): string
    {
        return ServiceLocator.game.startingPlayer == playerType && playerType == player_type.PLAYER
            || ServiceLocator.game.startingPlayer == playerType && playerType == player_type.ALLY ? "a" : "b";
    }

    public static reset(): void
    {
        this._gOidCounter = 0;
        this._gLookupTable.clear();
    }

    // private:

    private setComponentShortcut( componentId: string, value: Component ): void
    {
        let arrComponentName: Array<string> = componentId.split( "_" );
        for ( let componentName of arrComponentName )
        {
            this[ componentName.substr( 0, 1 ).toLowerCase() + componentName.substr( 1 ) ] = value;
        }
    }

    // #endregion //
}