import { IOpponentAction, IOppActionListener } from "./AllyActionManager";

import { player_action_type } from "../service/socket_io/GameSocketIOController";
import ServiceLocator from "../ServiceLocator";
import { gsap } from "gsap";
import * as PIXI from "pixi.js";

import GameObject from "./GameObject";
import { view_layer_id } from "../service/ViewManager";
import PoiFactory from "./PoiFactory";
import Utils from "../Utils";
import { poi_type } from "./component/world/CPoi";
import { player_type } from "./component/world/CPlayerArea";
import { layer_type } from "./component/world/CGameLayerProvider";


export default class PoiManager implements IOppActionListener
{
    // #region Methods //

    // public:

    public constructor() {}

    public init(): void
    {
        // Listen to events.
        ServiceLocator.game.allyActionManager.addListener( this, [
            player_action_type.SHOW_POI,
            player_action_type.REMOVE_POI ] );
        ServiceLocator.game.keyBindingManager.onKeyUp.add( this.onBindedKey_Up, this );
        ServiceLocator.game.keyBindingManager.onKeyDown.add( this.onBindedKey_Down, this );
    }

    public end(): void
    {
        // Listen to events.
        ServiceLocator.game.allyActionManager.removeListener( this, [
            player_action_type.SHOW_POI, 
            player_action_type.REMOVE_POI ] );
        ServiceLocator.game.keyBindingManager.onKeyUp.remove( this.onBindedKey_Up, this );
        ServiceLocator.game.keyBindingManager.onKeyDown.remove( this.onBindedKey_Down, this );
    }

    // private:
    
    private findPoiReceptor( root: PIXI.Container, global: PIXI.Point ): GameObject
    {
        let result: GameObject = null;

        for ( let child of root.children )
        {
            if ( child.getBounds().contains( global.x, global.y ) && child.visible )
            {
                let go: GameObject = ( child as any ).go as GameObject;
                if ( go && go.cPoiReceptor )
                {
                    result = go;
                }
                
                if ( child instanceof PIXI.Container )
                {
                    let other: GameObject = this.findPoiReceptor( child, global );
                    if ( other )
                    {
                        result = other;
                    }
                }
            }
        }

        return result;
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.SHOW_POI )
        {
            let poiFactory: PoiFactory = new PoiFactory();
            const kPoiReceptor: GameObject = GameObject.find( action.args[ 0 ] as string );
            let poi: GameObject = poiFactory.create( kPoiReceptor, action.args[ 1 ] as string, action.args[ 2 ] as poi_type, action.args[ 3 ], player_type.ALLY, action.args[ 4 ] ^ 1, kPoiReceptor );
            poi.cPoi.show( kPoiReceptor, player_type.ALLY );
        }
        else if ( action.playerActionType == player_action_type.REMOVE_POI )
        {
            let poi: GameObject = GameObject.find( action.args[ 0 ] );
            poi.cPoi.remove( player_type.ALLY );
        }
    }

    // #endregion //


    // #region Callbacks //

    private onBindedKey_Down( bindingId: string ): void
    {
        if ( ServiceLocator.viewManager.findViewCount( view_layer_id.POPUP ) > 0
            || ServiceLocator.game.root.cGameLayerProvider.get( layer_type.DISABLED ).cContainer.c.children.length > 0 )
        {
            return;
        }

        if ( bindingId == "poiMenu" )
        {
            ServiceLocator.game.cRadialMenu.hide();

            const kGlobal: PIXI.Point = ServiceLocator.game.app.renderer.plugins.interaction.mouse.global;
            let poiReceptor: GameObject = this.findPoiReceptor( ServiceLocator.game.root.cGameLayerProvider.get( layer_type.HUD ).cContainer.c, kGlobal );
            if ( !poiReceptor )
            {
                poiReceptor = this.findPoiReceptor( ServiceLocator.game.root.cGameLayerProvider.get( layer_type.GAME ).cContainer.c, kGlobal );
            }

            if ( poiReceptor )
            {
                ServiceLocator.game.poiMenu.cPoiMenu.show( poiReceptor, kGlobal );
            }
        }
    }

    private onBindedKey_Up( bindingId: string ): void
    {
        if ( bindingId == "poiMenu" )
        {
            ServiceLocator.game.poiMenu.cPoiMenu.hide();
        }
    }

    // #endregion //
}