import { location_type } from "./component/world/CGameWorld";
import { player_type } from "./component/world/CPlayerArea";

import GameObject from "./GameObject";
import CContainer from "./component/pixi/CContainer";
import CAnchorY from "./component/ui/CAnchorY";
import ViewerController from "./component/ui/viewer/ViewerController";
import CViewer from "./component/ui/CViewer";
import CCardView, { face_policy_type } from "./component/ui/viewer/CCardView";
import CardViewerController from "./component/ui/viewer/controller/CardViewerController";
import { context_button_type } from "./component/ui/viewer/CViewContextBtn";


export default class CardViewerFactory
{
    public create( creationParams: ICardViewerCreationParams ): GameObject
    {
        let cardViewer: GameObject = new GameObject( [ new CContainer(), new CViewer() ] );
        cardViewer.oid = creationParams.oid;
        let cCardView: CCardView = this.createCardView( creationParams );
        cardViewer.cViewer.view = cCardView;
        cardViewer.cViewer.title = creationParams.title;
        cardViewer.cViewer.headerIconId = creationParams.headerIconId;
        cardViewer.cViewer.location = creationParams.location;
        if ( !creationParams.customController )
        {
            cardViewer.cViewer.controller = new CardViewerController();
        }
        else
        {
            cardViewer.cViewer.controller = creationParams.customController;
        }
        cardViewer.cViewer.isSelfDropAllowed = true;
        cardViewer.init();

        let anchorY: GameObject = new GameObject( [ new CContainer(), new CAnchorY() ] );
        anchorY.cAnchorY.child = cardViewer.cViewer;
        anchorY.cContainer.c.x = 220;
        if ( creationParams.playerType == player_type.PLAYER
            || creationParams.playerType == player_type.SAURON )
        {
            anchorY.cContainer.c.y = 1070;
            anchorY.cAnchorY.anchorY = 1;
        }
        else
        {
            anchorY.cContainer.c.y = 10;
        }
        anchorY.init();

        return cardViewer;
    }

    // private:

    private createCardView( creationParams: ICardViewerCreationParams ): CCardView
    {
        let cCardView = new CCardView();
        cCardView.playerType = creationParams.playerType;
        cCardView.facePolicy = creationParams.facePolicy;
        if ( creationParams.contextBtnTypes )
        {
            cCardView.contextBtnTypes = creationParams.contextBtnTypes;
        }

        return cCardView;
    }
}

export interface ICardViewerCreationParams
{
    oid: string;
    title: string;
    headerIconId?: string; 
    location: location_type;
    playerType: player_type;
    facePolicy: face_policy_type;
    contextBtnTypes?: Array<context_button_type>;
    customController?: ViewerController;
}