import Process, { state_type } from "./Process";


export default class ProcDummy extends Process
{
    public constructor() 
    {
        super();
    }

    // overrides.
    public complete(): void
    {
        this._state = state_type.COMPLETED;

        this._onEvent.dispatch( "completed" );
    }
}