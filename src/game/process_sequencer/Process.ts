import Signal from "../../lib/signals/Signal";


export default abstract class Process
{
    private _id: string = null;

    protected _state: state_type = state_type.IDLE;

    // Signals.
    protected _onEvent: Signal = new Signal();


    public get state(): state_type { return this._state; }

    public set id( value: string ) { this._id = value; }

    public get id(): string { return this._id; }

    // Signals.
    public get onEvent(): Signal { return this._onEvent; }

    
    protected constructor() {}

    // virtual.
    public init(): void {}

    // virtual.
    public end(): void 
    {
        this._onEvent.removeAll();
    }

    // virtual.
    public start(): void
    {
        this._state = state_type.STARTED;

        this._onEvent.dispatch( "started" );
    }

    // virtual.
    public complete(): void {}

    // virtual.
    public update( dt: number ): void {}
}

export const enum state_type
{
    IDLE = 0,
    STARTED,
    COMPLETED
}