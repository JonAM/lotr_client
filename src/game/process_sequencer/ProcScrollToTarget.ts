import Process, { state_type } from "./Process";

import GameObject from "../GameObject";
import Utils from "../../Utils";


export default class ProcScrollToTarget extends Process
{
    private _target: GameObject = null;

    private _scrollY: GameObject = null;

    public set target( value: GameObject ) { this._target = value; }

    public constructor() 
    {
        super();
    }

    // overrides.
    public init(): void 
    {
        console.assert( this._target != null, "ProcScrollToTarget.ts :: init() :: this._target cannot be null." );
    }

    // overrides.
    public start(): void
    {
        super.start();

        this._scrollY = Utils.game.findGameObjectInBranchByComponentName( this._target, "CScrollY" );
        this._scrollY.cScrollY.onAutoScrollCompleted.addOnce( this.onComplete, this );
        this._scrollY.cScrollY.scrollToItem( this._target );
    }

    private onComplete(): void
    {
        this._state = state_type.COMPLETED;

        this._onEvent.dispatch( "completed" );
    }
}