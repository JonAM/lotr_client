import Process, { state_type } from "./Process";

import GameObject from "../GameObject";
import CGraphics from "../component/pixi/CGraphics";
import { TweenMax, Circ, Sine } from "gsap";
import Utils from "../../Utils";
import { layer_type } from "../component/world/CGameLayerProvider";
import * as PIXI from "pixi.js";


export default class ProcFlash extends Process
{
    private _target: GameObject = null;
    private _position: PIXI.Point = null;
    private _width: number = null;
    private _height: number = null;
    private _scale: number = null;

    private _mask: GameObject = null;
    private _flash: GameObject = null;

    public set target( value: GameObject ) { this._target = value; }
    public set position( value: PIXI.Point ) { this._position = value; }
    public set width( value: number ) { this._width = value; }
    public set height( value: number ) { this._height = value; }
    public set scale( value: number ) { this._scale = value; }

    public constructor() 
    {
        super();
    }

    // overrides.
    public init(): void 
    {
        console.assert( this._position != null || this._target != null, "ProcFlash.ts :: init() :: this._position and this._target cannot both be null." );
        console.assert( this._width != null, "ProcFlash.ts :: init() :: this._width cannot be null." );
        console.assert( this._height != null, "ProcFlash.ts :: init() :: this._height cannot be null." );
        console.assert( this._scale != null, "ProcFlash.ts :: init() :: this._scale cannot be null." );
    }

    // overrides.
    public start(): void
    {
        super.start();

        let layer: GameObject = Utils.game.findGameProviderLayer( this._target, layer_type.VFX );
            
        if ( this._target )
        {
            if ( !this._target.cContainer.c.worldVisible )
            {
                this._state = state_type.COMPLETED;

                this._onEvent.dispatch( "completed" );
            }
            else
            {
                this._position = new PIXI.Point().copyFrom( layer.cContainer.c.toLocal( this._target.cContainer.c.getGlobalPosition() ) );
                this._position.x += this._target.cContainer.c.width * 0.5;
                this._position.y += this._target.cContainer.c.height * 0.5;
            }
        }

        if ( this._state != state_type.COMPLETED )
        {
            const kHalfWidth: number = this._width * 0.5;
            const kHalfHeight: number = this._height * 0.5;
            this._mask = new GameObject( [ new CGraphics() ] );
            this._mask.cGraphics.g.beginFill();
            this._mask.cGraphics.g.drawRoundedRect( -kHalfWidth * this._scale, -kHalfHeight * this._scale, this._width * this._scale, this._height * this._scale, 2 );
            this._mask.cGraphics.g.endFill();
            this._mask.cGraphics.g.beginHole();
            this._mask.cGraphics.g.drawRoundedRect( -kHalfWidth, -kHalfHeight, this._width, this._height, 2 );
            this._mask.cGraphics.g.endHole();
            this._mask.cGraphics.g.renderable = false;
            this._mask.init();
            this._mask.cContainer.c.position.copyFrom( this._position );
            layer.cContainer.addChild( this._mask );

            this._flash = new GameObject( [ new CGraphics() ] );
            this._flash.cGraphics.g.beginFill( 0xffffff );
            this._flash.cGraphics.g.drawRoundedRect( -kHalfWidth, -kHalfHeight, this._width, this._height, 2 );
            this._flash.cGraphics.g.endFill();
            this._flash.cGraphics.g.mask = this._mask.cGraphics.g;
            this._flash.init();
            this._flash.cContainer.c.position.copyFrom( this._position );
            layer.cContainer.addChild( this._flash );

            TweenMax.to( this._flash.cGraphics.g.scale, 1, { x: this._scale, y: this._scale, ease: Circ.easeOut } );
            TweenMax.to( this._flash.cGraphics.g, 1, { alpha: 0, ease: Sine.easeOut, callbackScope: this, onComplete: this.onComplete } );
        }
    }

    private onComplete(): void
    {
        this._flash.end();
        this._flash = null;

        this._mask.end();
        this._mask = null;

        this._state = state_type.COMPLETED;

        this._onEvent.dispatch( "completed" );
    }
}