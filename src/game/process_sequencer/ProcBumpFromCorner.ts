import Process, { state_type } from "./Process";

import GameObject from "../GameObject";
import { TweenMax } from "gsap";


export default class ProcBumpFromCorner extends Process
{
    private _target: GameObject = null;

    public set target( value: GameObject ) { this._target = value; }

    public constructor() 
    {
        super();
    }

    // overrides.
    public init(): void 
    {
        console.assert( this._target != null, "ProcBumpFromCorner.ts :: init() :: this._target cannot be null." );
    }

    // overrides.
    public start(): void
    {
        super.start();

        const kScaleTranslation: number = this._target.cContainer.c.width * 0.15 * 0.5;
        TweenMax.to( this._target.cContainer.c, 0.15, { x: "-=" + kScaleTranslation.toString(), y: "-=" + kScaleTranslation.toString(), yoyo: true, repeat: 1 } );
        TweenMax.to( this._target.cContainer.c.scale, 0.15, { x: "+=0.15", y: "+=0.15", yoyo: true, repeat: 1, callbackScope: this, onRepeat: this.onRepeat, onComplete: this.onComplete } );
    }

    private onRepeat(): void
    {
        this.onEvent.dispatch( "effect" );
    }

    private onComplete(): void
    {
        this._state = state_type.COMPLETED;

        this._onEvent.dispatch( "completed" );
    }
}