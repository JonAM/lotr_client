import Process, { state_type } from "./process_sequencer/Process";


export default class ProcessSequencer
{
    // #region Attributes //

    // private:

    private _mapTimeline: Map<string, ITimeline> = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor() {}

    public init(): void
    {
        this._mapTimeline = new Map<string, ITimeline>();
        this._mapTimeline.set( "main", { 
            runningProcesses: new Array<Process>(), 
            waitingProcesses: new Array<Array<Process>>() } );
    }

    public end(): void 
    {
        this._mapTimeline.forEach( ( timeline: ITimeline, id: string ) => { 
            for ( let process of timeline.runningProcesses )
            {
                process.end();
            }
    
            for ( let arrProcess of timeline.waitingProcesses )
            {
                for ( let process of arrProcess )
                {
                    process.end();
                }
            }
        } );

        this._mapTimeline = null;
    }

    public hasTimeline( timelineId: string ): boolean
    {
        return this._mapTimeline.has( timelineId );
    }

    public findTimelines( searchString: string ): Array<ITimeline>
    {
        let result: Array<ITimeline> = new Array<ITimeline>();

        let it: IterableIterator<[ string, ITimeline ]> = this._mapTimeline.entries();
        for ( let itRes: IteratorResult<[ string, ITimeline ]> = it.next(); !itRes.done; itRes = it.next() )
        {
            if ( ( itRes.value[ 0 ] as string ).indexOf( searchString ) != -1 )
            {
                result.push( itRes.value[ 1 ] as ITimeline );
            }
        }

        return result;
    }

    public isBusy( timelineId: string ): boolean
    {
        const kTimeline: ITimeline = this._mapTimeline.get( timelineId );
        return kTimeline.runningProcesses.length > 0 || kTimeline.waitingProcesses.length > 0;
    }

    public update( dt: number ): void
    {
        this._mapTimeline.forEach( ( timeline: ITimeline, id: string ) => { 
            for ( let process of timeline.runningProcesses )
            {
                process.update( dt );
            }

            for ( let i: number = timeline.runningProcesses.length - 1; i >= 0; --i )
            {
                let process: Process = timeline.runningProcesses[ i ];
                if ( process.state == state_type.COMPLETED )
                {
                    process.end();
                    timeline.runningProcesses.splice( i, 1 );
                }
            }

            if ( timeline.runningProcesses.length == 0 )
            {
                if ( timeline.waitingProcesses.length > 0 )
                {
                    let arrProcess: Array<Process> = timeline.waitingProcesses.shift();
                    for ( let process of arrProcess )
                    {
                        process.start();
                        timeline.runningProcesses.push( process );
                    }
                }
                else if ( id != "main" )
                {
                    this._mapTimeline.delete( id ); // WATCTH: Can it be done here ???
                }
            }
        } );
    }

    public add( arrProcess: Array<Process>, timelineId: string = "main" ): void
    {
        if ( !this._mapTimeline.has( timelineId ) )
        {
            this._mapTimeline.set( timelineId, {
                runningProcesses: new Array<Process>(),
                waitingProcesses: new Array<Array<Process>>() } );
        }
        this._mapTimeline.get( timelineId ).waitingProcesses.push( arrProcess );
    }

    public remove( processId: string, timelineId: string = "main" ): void
    {
        if ( this._mapTimeline.has( timelineId ) )
        {
            let isFound: boolean = false;
            let timeline: ITimeline = this._mapTimeline.get( timelineId );
            for ( let process of timeline.runningProcesses )
            {
                if ( process.id == processId )
                {
                    process.complete();
                    isFound = true;
                    break;
                }
            }

            if ( !isFound )
            {
                for ( let arrProcess of timeline.waitingProcesses )
                {
                    for ( let process of arrProcess )
                    {
                        if ( process.id == processId )
                        {
                            arrProcess.splice( arrProcess.indexOf( process ), 1 );
                            isFound = true;
                            break;
                        }
                    }

                    if ( isFound )
                    {
                        break;
                    }
                }
            }
        }
    }

    // #endregion //
}

interface ITimeline
{
    runningProcesses: Array<Process>;
    waitingProcesses: Array<Array<Process>>;
}