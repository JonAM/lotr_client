import GameObject from "./GameObject";
import { player_type } from "./component/world/CPlayerArea";
import { action_scope_type } from "../service/socket_io/GameSocketIOController";


export default interface IGameObjectDropArea
{
    validateDroppedGameObject( go: GameObject, from: GameObject, global: PIXI.IPoint ): boolean;
    processDroppedGameObject( go: GameObject, global: PIXI.IPoint, actionScopeType: action_scope_type ): void;
}