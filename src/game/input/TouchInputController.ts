import InputController, { input_event_type } from "../InputController";

import * as PIXI from "pixi.js";
import ServiceLocator from "../../ServiceLocator";


export default class TouchInputController extends InputController
{
    private _touchId: number = null;

    private _tapCandidate: PIXI.DisplayObject = null;
    private _overCandidate: PIXI.DisplayObject = null;
    private _overGlobal: PIXI.Point = null;
    private _elapsedMsForOver: number = null; // number


    public constructor() { super(); }

    // overrides.
    public init(): void
    {
        super.init();

        this._elapsedMsForOver = 0;

        this._stage.on( "touchstart", this.onTouchEvent_Triggered, this );
        this._stage.on( "touchend", this.onTouchEvent_Triggered, this );
        this._stage.on( "touchendoutside", this.onTouchEvent_Triggered, this );
        this._stage.on( "touchcancel", this.onTouchEvent_Triggered, this );
        this._stage.on( "touchmove", this.onTouchEvent_Triggered, this );
    }

        private onTouchEvent_Triggered( event: PIXI.InteractionEvent ): void
        {
            // IMPORTANT: It looks like PIXI doesn't create a new instance for each event.
            const kClonedEvent: PIXI.InteractionEvent = Object.assign( {}, event );
            this._arrEvent.push( kClonedEvent );
        }

    // overrides.
    public end(): void
    {
        this._stage.off( "touchstart", this.onTouchEvent_Triggered, this );
        this._stage.off( "touchend", this.onTouchEvent_Triggered, this );
        this._stage.off( "touchendoutside", this.onTouchEvent_Triggered, this );
        this._stage.off( "touchcancel", this.onTouchEvent_Triggered, this );
        this._stage.off( "touchmove", this.onTouchEvent_Triggered, this );

        this._tapCandidate = null;
        this._overCandidate = null;
        this._overGlobal = null;
        this._elapsedMsForOver = null;
        this._touchId = null;

        super.end();
    }

    // overrides.
    public update( dt: number ): void
    {
        while ( this._arrEvent.length > 0 )
        {
            let event: PIXI.InteractionEvent = this._arrEvent.shift();
            if ( !this._touchId )
            {
                this._touchId = event.data.identifier;
            }

            if ( this._touchId == event.data.identifier )
            {
                if ( event.type == "touchmove" )
                {
                    this.notifyEvent( input_event_type.MOVE, this._stage, event );

                    const kHit: PIXI.DisplayObject = ServiceLocator.game.app.renderer.plugins.interaction.hitTest( event.data.global, this._stage );
                    if ( this._over && this._over != kHit )
                    {
                        this.notifyEvent( input_event_type.OUT, this._over, event );

                        this._tapCandidate = null;
                        this._elapsedMsForOver = 0;
                        this._over = null;
                    }

                    if ( this._overCandidate && this._overCandidate != kHit )
                    {
                        this._overCandidate = null;
                    }
                }
                else
                {
                    const kHit: PIXI.DisplayObject = ServiceLocator.game.app.renderer.plugins.interaction.hitTest( event.data.global, this._stage );
                    if ( kHit )
                    {
                        this.notifyEvent( this.findInputEventType( event.type ), kHit, event );

                        // Touch over.
                        if ( event.type == "touchstart" )
                        {
                            console.assert( this._over == null, "TouchInputController.ts :: update() :: this._over must be null." );
                            this._overCandidate = kHit;
                            this._overGlobal = event.data.global.clone();
                            this._tapCandidate = kHit;
                            this._elapsedMsForOver = 0;
                        }
                        else if ( event.type == "touchend" || event.type == "touchendoutside" || event.type == "touchcancel" )
                        {
                            if ( kHit == this._over )
                            {
                                this.notifyEvent( input_event_type.OUT, this._over, event );
                            }
                            
                            if ( !this._over && ( event.type == "touchend" || event.type == "touchendoutside" ) &&  kHit == this._tapCandidate )
                            {
                                this.notifyEvent( input_event_type.TAP, kHit, event );
                            }

                            this._tapCandidate = null;
                            this._overCandidate = null;
                            this._overGlobal = null;
                            this._elapsedMsForOver = 0;
                            this._over = null;

                            this._touchId = null;
                        }
                    }
                }
            }
        }

        // Touch over.
        if ( this._overCandidate )
        {
            this._elapsedMsForOver += ServiceLocator.game.app.ticker.elapsedMS;
            if ( this._elapsedMsForOver >= ServiceLocator.savedData.data.gamePreferences.touchOverDelayInMs )
            {
                let event: PIXI.InteractionEvent = new PIXI.InteractionEvent();
                event.data = new PIXI.InteractionData();
                event.data.global = this._overGlobal;
                this.notifyEvent( input_event_type.OVER, this._overCandidate, event );

                this._over = this._overCandidate;
                this._overCandidate = null;
                this._elapsedMsForOver = 0;
            }
        }
    }

        private findInputEventType( mouseEventType: string ): input_event_type
        {
            let result: input_event_type = null;

            switch ( mouseEventType )
            {
                case "touchstart": { result = input_event_type.DOWN; break; }

                case "touchend": 
                case "touchendoutside": 
                case "touchcancel": { result = input_event_type.UP; break; }

                case "touchmove": { result = input_event_type.MOVE; break; }
            }

            return result;
        }
}