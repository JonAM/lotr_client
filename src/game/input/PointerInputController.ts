import InputController, { input_event_type } from "../InputController";

import * as PIXI from "pixi.js";
import ServiceLocator from "../../ServiceLocator";


export default class PointerInputController extends InputController
{
    private _clickCandidate: PIXI.DisplayObject = null;
    private _lastButton: number = null;


    public constructor() { super(); }

    // overrides.
    public init(): void
    {
        super.init();
        
        this._stage.on( "pointerdown", this.onPointerEvent_Triggered, this );
        this._stage.on( "pointerup", this.onPointerEvent_Triggered, this );
        this._stage.on( "pointermove", this.onPointerEvent_Triggered, this );
    }

        private onPointerEvent_Triggered( event: PIXI.InteractionEvent ): void
        {
            // IMPORTANT: It looks like PIXI doesn't create a new instance for each event.
            const kClonedEvent: PIXI.InteractionEvent = Object.assign( {}, event );
            this._arrEvent.push( kClonedEvent );
        }

    // overrides.
    public end(): void
    {
        this._stage.off( "pointerdown", this.onPointerEvent_Triggered, this );
        this._stage.off( "pointerup", this.onPointerEvent_Triggered, this );
        this._stage.off( "pointermove", this.onPointerEvent_Triggered, this );

        this._clickCandidate = null;
        this._lastButton = null;

        super.end();
    }

    // overrides.
    public update( dt: number ): void
    {
        while ( this._arrEvent.length > 0 )
        {
            let event: PIXI.InteractionEvent = this._arrEvent.shift();
            if ( event.data.button == -1 )
            {
                event.data.button = this._lastButton;
            }
            else
            {
                this._lastButton = event.data.button;
            }
            if ( event.type == "pointermove" || event.type == "pointerover" || event.type == "pointerout" )
            {
                this.notifyEvent( input_event_type.MOVE, this._stage, event );

                const kHit: PIXI.DisplayObject = ServiceLocator.game.app.renderer.plugins.interaction.hitTest( event.data.global, this._stage );
                if ( this._over && this._over != kHit )
                {
                    this.notifyEvent( input_event_type.OUT, this._over, event );
                    this._over = null;
                    this._clickCandidate = null;
                }

                if ( kHit )
                {
                    if ( !this._over )
                    {
                        this.notifyEvent( input_event_type.OVER, kHit, event );
                        this._over = kHit;
                    }
                    this.notifyEvent( input_event_type.MOVE, kHit, event );
                }
            }
            else if ( event.type == "pointerdown" || event.type == "pointerup" )
            {
                const kHit: PIXI.DisplayObject = ServiceLocator.game.app.renderer.plugins.interaction.hitTest( event.data.global, this._stage );
                if ( kHit )
                {
                    this.notifyEvent( this.findInputEventType( event.type ), kHit, event );

                    if ( event.type == "pointerdown" )
                    {
                        this._clickCandidate = kHit;
                    }
                    else if ( event.type == "pointerup" && this._clickCandidate == kHit )
                    {
                        this.notifyEvent( input_event_type.TAP, kHit, event );
                        this._clickCandidate = null;
                    }
                }
            }
        }
    }

        private findInputEventType( pointerEventType: string ): input_event_type
        {
            let result: input_event_type = null;

            switch ( pointerEventType )
            {
                case "pointerdown": { result = input_event_type.DOWN; break; }
                case "pointerup": { result = input_event_type.UP; break; }

                case "pointermove": 
                case "pointerover": 
                case "pointerout":  { result = input_event_type.MOVE; break; }
            }

            return result;
        }
}