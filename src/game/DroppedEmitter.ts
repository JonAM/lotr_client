import * as PIXI from "pixi.js";
import * as particles from "pixi-particles";
import ServiceLocator from "../ServiceLocator";
import GameObject from "./GameObject";


export default class DroppedEmitter
{
    // #region Attributes //

    // private:

    private _layer: GameObject = null;

    private _particleContainer: PIXI.Container = null;
    private _emitter: particles.Emitter = null;

    // #endregion //


    // #region Properties //

    public set layer( value: GameObject ) { this._layer = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() {}

    public init(): void
    {
        console.assert( this._layer != null, "DroppedEmitter.ts :: init() :: this._layer cannot be null." )

        this._particleContainer = new PIXI.Container();
        this._layer.cContainer.c.addChild( this._particleContainer );

        this._emitter = this.createEmitter();
    }

    public end(): void
    {
        this._emitter.destroy();
        this._emitter = null;

        this._layer.cContainer.c.removeChild( this._particleContainer );
        this._particleContainer = null;
        this._layer = null;
    }

    public playSmall( x: number, y: number ): void
    {
        this._emitter.startScale.value = 0.2;
        this._emitter.startScale.next.value = 0.6;
        this._emitter.startSpeed.value = 150;
        this._emitter.startSpeed.next.value = 50;

        this._particleContainer.position.set( x, y );
        this._emitter.playOnce( this.end.bind( this ) );
    }

    public playLarge( x: number, y: number ): void
    {
        this._emitter.startScale.value = 0.4;
        this._emitter.startScale.next.value = 1.2;
        this._emitter.startSpeed.value = 300;
        this._emitter.startSpeed.next.value = 100;

        this._particleContainer.position.set( x, y );
        this._emitter.playOnce( this.end.bind( this ) );
    }

    // private:

    private createEmitter(): particles.Emitter
    {
        let emitter: particles.Emitter = new particles.Emitter(
            this._particleContainer,
            [ PIXI.Texture.from( ServiceLocator.resourceStack.find( "particle_dust" ).data ) ],
            ServiceLocator.resourceStack.find( "emitter_token_dropped" ).data );
        emitter.autoUpdate = true;
        emitter.emit = false;

        return emitter;
    }

    // #endregion //
}