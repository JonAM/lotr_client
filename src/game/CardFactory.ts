import { player_type } from "./component/world/CPlayerArea";
import { location_type } from "./component/world/CGameWorld";

import GameObject from "./GameObject";
import CContainer from "./component/pixi/CContainer";
import CDraggable from "./component/input/CDraggable";
import CCard from "./component/card/CCard";
import CCardPoiReceptor from "./component/world/poi_receptor/CCardPoiReceptor";
import CShareableGameElement from "./component/CShareableGameElement";
import Utils from "../Utils";


export default class CardFactory
{
    public create( params: ICardCreationParams ): GameObject
    {
        let card: GameObject = new GameObject( [ 
            new CContainer(), 
            new CCard(), 
            new CDraggable(),
            new CCardPoiReceptor(),
            new CShareableGameElement() ] );
        card.oid = params.oid;
        card.cCard.frontCardId = params.frontCardId;
        card.cCard.backCardId = params.backCardId;
        card.cCard.ownerPlayer = params.ownerPlayer;
        card.cCard.controllerPlayer = params.controllerPlayer;
        card.cCard.location = params.location;
        card.cCard.isFaceUp = params.isFaceUp;
        card.init();

        return card;
    }

    public createFromCardToken( cardToken: GameObject ): GameObject
    {
        let card: GameObject = new GameObject( [ 
            new CContainer(), 
            new CCard(), 
            new CDraggable(),
            new CCardPoiReceptor(),
            new CShareableGameElement() ] );
        card.oid = cardToken.oid.substr( 0, cardToken.oid.length - 1 );
        card.cCard.frontCardId = cardToken.cCardToken.cFrontSide.cardId;
        card.cCard.backCardId = cardToken.cCardToken.cBackSide.cardId;
        card.cCard.ownerPlayer = cardToken.cCardToken.ownerPlayer;
        card.cCard.controllerPlayer = cardToken.cCardToken.ownerPlayer == player_type.SAURON ? player_type.PLAYER : cardToken.cCardToken.ownerPlayer;
        card.cCard.location = cardToken.cCardToken.location;
        card.cCard.isFaceUp = cardToken.cCardToken.isFaceUp;
        card.init();

        return card;
    }

    public createFromAttachment( attachment: GameObject ): GameObject
    {
        let card: GameObject = new GameObject( [ 
            new CContainer(), 
            new CCard(),
            new CDraggable(),
            new CCardPoiReceptor(),
            new CShareableGameElement() ] );
        card.oid = attachment.oid.substr( 0, attachment.oid.indexOf( "_attachment" ) );
        card.cCard.frontCardId = attachment.cAttachment.front.cardId;
        card.cCard.backCardId = attachment.cAttachment.back.cardId;
        card.cCard.ownerPlayer = attachment.cAttachment.ownerPlayer;
        card.cCard.controllerPlayer = attachment.cAttachment.ownerPlayer == player_type.SAURON ? player_type.PLAYER : attachment.cAttachment.ownerPlayer;
        card.cCard.location = location_type.UNDER_TABLE;
        card.cCard.isFaceUp = attachment.cAttachment.isFaceUp;
        card.init();

        return card;
    }

    public createFromGameModifier( gameModifier: GameObject ): GameObject
    {
        let card: GameObject = new GameObject( [ 
            new CContainer(), 
            new CCard(),
            new CDraggable(),
            new CCardPoiReceptor(),
            new CShareableGameElement() ] );
        card.oid = gameModifier.oid.substr( 0, gameModifier.oid.indexOf( "_game_modifier" ) );
        card.cCard.frontCardId = gameModifier.cGameModifier.front.cardId;
        card.cCard.backCardId = gameModifier.cGameModifier.back.cardId;
        card.cCard.ownerPlayer = gameModifier.cGameModifier.ownerPlayer;
        card.cCard.controllerPlayer = gameModifier.cGameModifier.ownerPlayer == player_type.SAURON ? player_type.PLAYER : gameModifier.cGameModifier.ownerPlayer;
        card.cCard.location = location_type.UNDER_TABLE;
        card.cCard.isFaceUp = gameModifier.cGameModifier.isFaceUp;
        card.init();

        return card;
    }
}

export interface ICardCreationParams
{
    oid: string;
    frontCardId: string;
    backCardId: string;
    ownerPlayer: player_type;
    controllerPlayer: player_type;
    location: location_type;
    isFaceUp: boolean;
}