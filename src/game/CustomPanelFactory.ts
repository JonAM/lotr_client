import GameObject from "./GameObject";
import CContainer from "./component/pixi/CContainer";
import CGameLayerProvider, { layer_type } from "./component/world/CGameLayerProvider";
import CGraphics from "./component/pixi/CGraphics";
import CIsolatedArea from "./component/world/custom_area/CIsolatedArea";
import CPursuitArea from "./component/world/custom_area/CPursuitArea";
import CRiddleArea from "./component/world/custom_area/CRiddleArea";
import CCircuit from "./component/world/custom_area/CCircuit";
import CIslandMap from "./component/world/custom_area/CIslandMap";
import CHeading from "./component/world/custom_area/CHeading";
import CInvestigationList from "./component/world/custom_area/CInvestigationList";
import CTreasureList from "./component/world/custom_area/CTreasureList";
import CSetupInstructions from "./component/world/custom_area/CSetupInstructions";
import Session from "../Session";
import CCampaignLog from "./component/world/custom_area/CCampaignLog";
import ServiceLocator from "../ServiceLocator";
import CampaignLogVO, { ISerializedCampaignLog } from "../vo/CampaignLogVO";
import CSplitSauronArea from "./component/world/custom_area/CSplitSauronArea";
import { player_type } from "./component/world/CPlayerArea";
import Utils from "../Utils";
import CPitArea from "./component/world/custom_area/CPitArea";
import CDorCuartholArea from "./component/world/custom_area/CDorCuartholArea";


export default class CustomPanelFactory
{
    // #region Methods //

    // public:

    public create( panelId: string ): ICustomPanel
    {
        let result: ICustomPanel = { buttonTooltip: null, buttonIconId: null, content: null };
        
        switch ( panelId )
        {
            case "setup_instructions":
                {
                    result.buttonTooltip = jQuery.i18n( "SETUP_INSTRUCTIONS" );
                    result.buttonIconId = "setup_instructions";
                    result.content = this.createSetupInstructions();
                    break;
                }

            case "isolated":
                {
                    result.buttonTooltip = jQuery.i18n( "ALLYS_STAGING_AREA" );
                    result.buttonIconId = "isolated_staging_area";
                    result.content = this.createIsolatedArea();
                    break;
                }

            case "split_sauron":
                {
                    result.buttonTooltip = jQuery.i18n( "MAIN_STAGING_AREA" );
                    result.buttonIconId = "main_staging_area";
                    result.content = this.createSplitSauronArea();
                    break;
                }

            case "stormcaller": 
                { 
                    result.buttonTooltip = jQuery.i18n( "STORMCALLERS_AREA" );
                    result.buttonIconId = "galleon";
                    result.content = this.createPursuitArea();
                    break; 
                }

            case "orcs": 
                { 
                    result.buttonTooltip = jQuery.i18n( "ORCS_AREA" );
                    result.buttonIconId = "orc";
                    result.content = this.createPursuitArea();
                    break; 
                }

            case "riddle": 
                { 
                    result.buttonTooltip = jQuery.i18n( "RIDDLE_AREA" );
                    result.buttonIconId = "riddle"; 
                    result.content = this.createRiddleArea();
                    break; 
                }

            case "circuit": 
                { 
                    result.buttonTooltip = jQuery.i18n( "CIRCUIT" );
                    result.buttonIconId = "circuit";
                    result.content = this.createCircuit();
                    break; 
                }

            case "island": 
                { 
                    result.buttonTooltip = jQuery.i18n( "ISLAND_MAP" );
                    result.buttonIconId = "island_map";
                    result.content = this.createIslandMap();
                    break; 
                }

            case "heading": 
                { 
                    result.buttonTooltip = jQuery.i18n( "HEADING" );
                    result.buttonIconId = "compass";
                    result.content = this.createHeading();
                    break; 
                }

            case "investigation_list": 
                { 
                    result.buttonTooltip = jQuery.i18n( "INVESTIGATION_LIST" );
                    result.buttonIconId = "investigation_list";
                    result.content = this.createInvestigationList();
                    break; 
                }

            case "treasure_list": 
                { 
                    result.buttonTooltip = jQuery.i18n( "TREASURE_LIST" );
                    result.buttonIconId = "treasure_list";
                    result.content = this.createTreasureList();
                    break; 
                }

            case "campaign_log": 
                { 
                    result.buttonTooltip = jQuery.i18n( "CAMPAIGN_LOG" );
                    result.buttonIconId = "campaign_log";
                    result.content = this.createCampaignLog();
                    break; 
                }

            case "pit": 
                { 
                    result.buttonTooltip = jQuery.i18n( "PIT_AREA" );
                    result.buttonIconId = "pit"; 
                    result.content = this.createPitArea();
                    break; 
                }

            case "dor-cuarthol": 
                { 
                    result.buttonTooltip = jQuery.i18n( "DOR-CUARTHOL_AREA" );
                    result.buttonIconId = "mountains"; 
                    result.content = this.createDorCuartholArea();
                    break; 
                }
        }

        return result;
    }

        private createSetupInstructions(): GameObject
        {
            let result: GameObject = new GameObject( [ new CGraphics(), new CSetupInstructions(), new CGameLayerProvider() ] );
            result.oid = "setup_instructions";
            result.cGameLayerProvider.layerTypes = [ 
                layer_type.POI, 
                layer_type.VFX ];
            result.init();

            return result;
        }

        private createIsolatedArea(): GameObject
        {
            let result: GameObject = new GameObject( [ new CGraphics(), new CIsolatedArea(), new CGameLayerProvider() ] );
            result.oid = "isolated";
            result.cGameLayerProvider.layerTypes = [ 
                layer_type.ATTACK_BINDING_LINK, 
                layer_type.ATTACK_BINDING_COUNTER, 
                layer_type.POI, 
                layer_type.VFX ];
            result.init();

            return result;
        }

        private createSplitSauronArea(): GameObject
        {
            let result: GameObject = new GameObject( [ new CGraphics(), new CSplitSauronArea(), new CGameLayerProvider() ] );
            result.oid = "split_sauron";
            result.cGameLayerProvider.layerTypes = [ 
                layer_type.ATTACK_BINDING_LINK, 
                layer_type.ATTACK_BINDING_COUNTER, 
                layer_type.POI, 
                layer_type.VFX ];
            result.init();

            return result;
        }

        private createPursuitArea(): GameObject
        {
            let result: GameObject = new GameObject( [ new CGraphics(), new CPursuitArea(), new CGameLayerProvider() ] );
            result.oid = "pursuit";
            result.cGameLayerProvider.layerTypes = [ 
                layer_type.ATTACK_BINDING_LINK, 
                layer_type.ATTACK_BINDING_COUNTER, 
                layer_type.POI, 
                layer_type.VFX ];
            result.init();

            return result;
        }

        private createRiddleArea(): GameObject
        {
            let result: GameObject = new GameObject( [ new CGraphics(), new CRiddleArea(), new CGameLayerProvider() ] );
            result.oid = "riddle";
            result.cGameLayerProvider.layerTypes = [ 
                layer_type.ATTACK_BINDING_LINK, 
                layer_type.ATTACK_BINDING_COUNTER, 
                layer_type.POI, 
                layer_type.VFX ];
            result.init();

            return result;
        }

        private createCircuit(): GameObject
        {
            let result: GameObject = new GameObject( [ new CGraphics(), new CCircuit(), new CGameLayerProvider() ] );
            result.oid = "circuit";
            result.cGameLayerProvider.layerTypes = [ 
                layer_type.ATTACK_BINDING_LINK, 
                layer_type.ATTACK_BINDING_COUNTER, 
                layer_type.POI, 
                layer_type.VFX ];
            result.init();

            return result;
        }

        private createIslandMap(): GameObject
        {
            let result: GameObject = new GameObject( [ new CGraphics(), new CIslandMap(), new CGameLayerProvider() ] );
            result.oid = "island";
            result.cGameLayerProvider.layerTypes = [ 
                layer_type.ATTACK_BINDING_LINK, 
                layer_type.ATTACK_BINDING_COUNTER, 
                layer_type.POI, 
                layer_type.VFX ];
            result.init();

            return result;
        }

        private createHeading(): GameObject
        {
            let result: GameObject = new GameObject( [ new CGraphics(), new CHeading(), new CGameLayerProvider() ] );
            result.oid = "heading";
            result.cGameLayerProvider.layerTypes = [ 
                layer_type.POI, 
                layer_type.VFX ];
            result.init();

            return result;
        }

        private createInvestigationList(): GameObject
        {
            let result: GameObject = new GameObject( [ new CGraphics(), new CInvestigationList(), new CGameLayerProvider() ] );
            result.oid = "investigation_list";
            result.cGameLayerProvider.layerTypes = [ 
                layer_type.POI, 
                layer_type.VFX ];
            result.init();

            return result;
        }

        private createTreasureList(): GameObject
        {
            let result: GameObject = new GameObject( [ new CGraphics(), new CTreasureList(), new CGameLayerProvider() ] );
            result.oid = "treasure_list";
            if ( ServiceLocator.game.treasureList )
            {
                result.cTreasureList.checkedIndexes = ServiceLocator.game.treasureList.list;
            }
            result.cGameLayerProvider.layerTypes = [ 
                layer_type.POI, 
                layer_type.VFX ];
            result.init();

            return result;
        }

        private createCampaignLog(): GameObject
        {
            let result: GameObject = new GameObject( [ new CContainer(), new CCampaignLog() ] );
            result.oid = "campaign_log";
            let campaignLog: CampaignLogVO = ServiceLocator.game.campaignLog;
            if ( !campaignLog )
            {
                campaignLog = new CampaignLogVO();
                let serializedCampaignLog: ISerializedCampaignLog = {
                    title: jQuery.i18n( "NEW_CAMPAIGN_LOG" ),
                    dateTimestamp: "",
                    username: ServiceLocator.game.hostPlayer == player_type.PLAYER ? Session.playerId : Session.allyId,
                    playerHeroes: ServiceLocator.game.hostPlayer == player_type.PLAYER ? Utils.strArrayToStr( Session.playerDeck.hero ) : Utils.strArrayToStr( Session.allyDeck.hero ),
                    ally: "",
                    allyHeroes: "",
                    fallenHeroes: "",
                    threatPenalty: "0",
                    notes: "",
                    completedScenarios: "",
                    boons: "",
                    burdens: "",
                    whenTimestamp: "" };
                if ( Session.allyId )
                {
                    serializedCampaignLog.allyHeroes = ServiceLocator.game.hostPlayer == player_type.PLAYER ? Utils.strArrayToStr( Session.allyDeck.hero ) : Utils.strArrayToStr( Session.playerDeck.hero );
                }
                campaignLog.parse( serializedCampaignLog );
            }
            result.cCampaignLog.campaignLogVO = campaignLog;
            result.init();

            return result;
        }

        private createPitArea(): GameObject
        {
            let result: GameObject = new GameObject( [ new CGraphics(), new CPitArea(), new CGameLayerProvider() ] );
            result.oid = "pit";
            result.cGameLayerProvider.layerTypes = [ 
                layer_type.ATTACK_BINDING_LINK, 
                layer_type.ATTACK_BINDING_COUNTER, 
                layer_type.POI, 
                layer_type.VFX ];
            result.init();

            return result;
        }

        private createDorCuartholArea(): GameObject
        {
            let result: GameObject = new GameObject( [ new CGraphics(), new CDorCuartholArea(), new CGameLayerProvider() ] );
            result.oid = "dor-cuarthol";
            result.cGameLayerProvider.layerTypes = [ 
                layer_type.ATTACK_BINDING_LINK, 
                layer_type.ATTACK_BINDING_COUNTER, 
                layer_type.POI, 
                layer_type.VFX ];
            result.init();

            return result;
        }

    // #endregion //
}

export interface ICustomPanel
{
    buttonTooltip: string;
    buttonIconId: string;
    content: GameObject;
}