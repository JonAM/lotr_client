export default class Path
{
    // #region Attributes //
    
    // public:
    
    public static readonly kCss: string = "css/";
    public static readonly kResHtml: string = "resources/html/";
    public static readonly kResPdf: string = "resources/pdf/";
    public static readonly kResImages: string = "resources/images/";
    public static readonly kResCommon: string = Path.kResImages + "common/";
    public static readonly kResGame: string = Path.kResImages + "game/";
    public static readonly kResGateway: string = Path.kResImages + "gateway/";
    public static readonly kResPlayerProfile: string = Path.kResGateway + "player_profile/";

    // #endregion //


    // #region Methods //

    // private:

    private constructor() {}

    // #endregion //
}