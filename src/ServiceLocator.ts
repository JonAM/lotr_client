import AudioManager from "./service/AudioManager";
import BrowserDependency from "./service/BrowserDependency";
import GameState from "./states/GameState";
import LoadingScreen from "./service/LoadingScreen";
import ResourceLoader from "./service/ResourceLoader";
import ResourceStack from "./service/ResourceStack";
import SavedData from "./service/SavedData";
import StateMachine from "./lib/StateMachine";
import ViewManager from "./service/ViewManager";
import SocketIOManager from "./service/SocketIOManager";
import PRNG from "./lib/PRNG";
import DbConnector from "./DbConnector";
import CardDB from "./game/CardDB";
import ScenarioDB from "./game/ScenarioDB";
import ResourceCache from "./service/ResourceCache";


export default class ServiceLocator
{
    // #region Attributes //

    // private:

    private static _gAudioManager: AudioManager = null;
    private static _gBrowserDependency: BrowserDependency = null;
    private static _gCardDb: CardDB = null;
    private static _gScenarioDb: ScenarioDB = null;
    private static _gDbConnector: DbConnector = null;
    private static _gGameState: GameState = null;
    private static _gResourceCache: ResourceCache = null;
    private static _gLoadingScreen: LoadingScreen = null;
    private static _gPrng: PRNG = null;
    private static _gResourceLoader: ResourceLoader = null;
    private static _gResourceStack: ResourceStack = null;
    private static _gSavedData: SavedData = null;
    private static _gSocketIOManager: SocketIOManager = null;
    private static _gStateMachine: StateMachine = null;
    private static _gViewManager: ViewManager = null;
    
    // #endregion //


    // #region Properties //

    public static get audioManager(): AudioManager { return this._gAudioManager; }
    public static get browserDependency(): BrowserDependency { return this._gBrowserDependency; }
    public static get cardDb(): CardDB { return this._gCardDb; }
    public static get scenarioDb(): ScenarioDB { return this._gScenarioDb; }
    public static get dbConnector(): DbConnector { return this._gDbConnector; }
    public static get game(): GameState { return this._gGameState; }
    public static get loadingScreen(): LoadingScreen { return this._gLoadingScreen; }
    public static get prng(): PRNG { return this._gPrng; }
    public static get resourceCache(): ResourceCache { return this._gResourceCache; }
    public static get resourceLoader(): ResourceLoader { return this._gResourceLoader; }
    public static get resourceStack(): ResourceStack { return this._gResourceStack; }
    public static get savedData(): SavedData { return this._gSavedData; }
    public static get socketIOManager(): SocketIOManager { return this._gSocketIOManager; }
    public static get stateMachine(): StateMachine { return this._gStateMachine; }
    public static get viewManager(): ViewManager { return this._gViewManager; }

    public static set audioManager( value: AudioManager ) { this._gAudioManager = value; }
    public static set browserDependency(  value: BrowserDependency ) { this._gBrowserDependency = value; }
    public static set cardDb(  value: CardDB ) { this._gCardDb = value; }
    public static set scenarioDb(  value: ScenarioDB ) { this._gScenarioDb = value; }
    public static set dbConnector(  value: DbConnector ) { this._gDbConnector = value; }
    public static set game( value: GameState ) { this._gGameState = value; }
    public static set loadingScreen( value: LoadingScreen ) { this._gLoadingScreen = value; }
    public static set prng( value: PRNG ) { this._gPrng = value; }
    public static set resourceCache( value: ResourceCache ) { this._gResourceCache = value; }
    public static set resourceLoader( value: ResourceLoader ) { this._gResourceLoader = value; }
    public static set resourceStack( value: ResourceStack ) { this._gResourceStack = value; }
    public static set savedData( value: SavedData ) { this._gSavedData = value; }
    public static set socketIOManager( value: SocketIOManager ) { this._gSocketIOManager = value; }
    public static set stateMachine( value: StateMachine ) {  this._gStateMachine = value; }
    public static set viewManager( value: ViewManager ) {  this._gViewManager = value; }

    // #endregion


    // #region Methods //

    // private:

    private constructor() {}

    // #endregion //
}