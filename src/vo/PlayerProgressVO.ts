import Utils from "../Utils";
import { IDeck } from "../view/gateway/DeckParser";


export default class PlayerProgressVO
{
    // #region Attributes //

    // private:

    private _playerOne: string;
    private _playerOneDeck: IDeck;
    private _playerTwo: string;
    private _playerTwoDeck: IDeck;
    private _dateTimestamp: number;
    private _scenarioId: string;
    private _score: number;

    private _whenTimestamp: number;

    // #endregion //
    

    // #region Properties //

    public get playerOne(): string { return this._playerOne; }
    public get playerOneDeck(): IDeck { return this._playerOneDeck; }
    public get playerTwo(): string { return this._playerTwo; }
    public get playerTwoDeck(): IDeck { return this._playerTwoDeck; }
    public get dateTimestamp(): number { return this._dateTimestamp; }
    public get scenarioId(): string { return this._scenarioId; }
    public get score(): number { return this._score; }
    public get whenTimestamp(): number { return this._whenTimestamp; }


    // #endregion //


    // #region Methods //

    // public:

    public parse( jsonData: Object ): boolean
	{
		let result: boolean = true;

		try
		{
            this._playerOne = jsonData[ "playerOne" ];
            this._playerOneDeck = JSON.parse( jsonData[ "playerOneDeck" ] );
            this._playerTwo = jsonData[ "playerTwo" ] == "" ? null : jsonData[ "playerTwo" ];
            this._playerTwoDeck = jsonData[ "playerTwoDeck" ] == "" ? null : JSON.parse( jsonData[ "playerTwoDeck" ] );
            this._dateTimestamp = parseInt( jsonData[ "dateTimestamp" ] );
            this._scenarioId = jsonData[ "scenarioId" ];
            this._score = parseInt( jsonData[ "score" ] );
            this._whenTimestamp = parseInt( jsonData[ "whenTimestamp" ] );
		}
		catch ( error )
		{
			result = false;

			Utils.log( "PlayerProgressVO :: parse() :: error parsing jsonData :: " + error.message );
		}

		return result;
    }
    
    public static parseArray( arrJsonData: Array<Object> ): Array<PlayerProgressVO>
	{
		let result: Array<PlayerProgressVO> = new Array<PlayerProgressVO>();

        for ( let jsonData of arrJsonData )
        {
            let vo: PlayerProgressVO = new PlayerProgressVO();
            if ( vo.parse( jsonData ) )
            {
                result.push( vo );
            }
            else
            {
                result = null;
                break;
            }
        }

		return result;
    }

    // #endregion //
}