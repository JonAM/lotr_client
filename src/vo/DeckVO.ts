import Utils from "../Utils";


export default class DeckVO
{
    // #region Attributes //

    // private:

    private _title: string;
    private _username: string;
    private _content: string;

    // #endregion //
    

    // #region Properties //

    public get title(): string { return this._title; }
    public get username(): string { return this._username; }
    public get content(): string { return this._content; }

    // #endregion //


    // #region Methods //

    // public:

    public parse( jsonData: Object ): boolean
	{
		let result: boolean = true;

		try
		{
            this._title = jsonData[ "title" ];
            this._username = jsonData[ "username" ];
            this._content = jsonData[ "content" ];
		}
		catch ( error )
		{
			result = false;

			Utils.log( "DeckVO :: parse() :: error parsing jsonData :: " + error.message );
		}

		return result;
    }
    
    public static parseArray( arrJsonData: Array<Object> ): Array<DeckVO>
	{
		let result: Array<DeckVO> = new Array<DeckVO>();

        for ( let jsonData of arrJsonData )
        {
            let vo: DeckVO = new DeckVO();
            if ( vo.parse( jsonData ) )
            {
                result.push( vo );
            }
            else
            {
                result = null;
                break;
            }
        }

		return result;
    }

    // #endregion //
}