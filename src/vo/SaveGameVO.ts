import Utils from "../Utils";


export default class SaveGameVO
{
    // #region Attributes //

    // private:

    private _title: string;
    private _username: string;
    private _ally: string;
    private _msSinceCreation: number;
    private _content: string;

    // #endregion //
    

    // #region Properties //

    public get title(): string { return this._title; }
    public get username(): string { return this._username; }
    public get ally(): string { return this._ally; }
    public get msSinceCreation(): number { return this._msSinceCreation; }
    public get content(): string { return this._content; }

    // #endregion //


    // #region Methods //

    // public:

    public parse( jsonData: Object ): boolean
	{
		let result: boolean = true;

		try
		{
            this._title = jsonData[ "title" ];
            this._username = jsonData[ "username" ];
            this._ally = jsonData[ "ally" ];
            this._msSinceCreation = parseInt( jsonData[ "msSinceCreation" ] );
            this._content = jsonData[ "content" ];
		}
		catch ( error )
		{
			result = false;

			Utils.log( "SaveGameVO :: parse() :: error parsing jsonData :: " + error.message );
		}

		return result;
    }
    
    public static parseArray( arrJsonData: Array<Object> ): Array<SaveGameVO>
	{
		let result: Array<SaveGameVO> = new Array<SaveGameVO>();

        for ( let jsonData of arrJsonData )
        {
            let vo: SaveGameVO = new SaveGameVO();
            if ( vo.parse( jsonData ) )
            {
                result.push( vo );
            }
            else
            {
                result = null;
                break;
            }
        }

		return result;
    }

    // #endregion //
}