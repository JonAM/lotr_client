import Utils from "../Utils";


export default class UserVO
{
    // #region Attributes //

    // private:

    private _username: string;
    private _karma: number;
    private _joinDate: string;
    private _lastConnTimestamp: number;

    // #endregion //
    

    // #region Properties //

    public get username(): string { return this._username; }
    public get karma(): number { return this._karma; }
    public get joinDate(): string { return this._joinDate; }
    public get lastConnTimestamp(): number { return this._lastConnTimestamp; }

    // #endregion //


    // #region Methods //

    // public:

    public parse( jsonData: Object ): boolean
	{
		let result: boolean = true;

		try
		{
            this._username = jsonData[ "username" ];
            this._karma = jsonData[ "karma" ];
            this._joinDate = jsonData[ "joinDate" ];
            this._lastConnTimestamp = parseInt( jsonData[ "lastConnTimestamp" ] );
		}
		catch ( error )
		{
			result = false;

			Utils.log( "UserVO :: parse() :: error parsing jsonData :: " + error.message );
		}

		return result;
    }
    
    public static parseArray( arrJsonData: Array<Object> ): Array<UserVO>
	{
		let result: Array<UserVO> = new Array<UserVO>();

        for ( let jsonData of arrJsonData )
        {
            let vo: UserVO = new UserVO();
            if ( vo.parse( jsonData ) )
            {
                result.push( vo );
            }
            else
            {
                result = null;
                break;
            }
        }

		return result;
    }

    // #endregion //
}