import Utils from "../Utils";


export default class NewsVO
{
    // #region Attributes //

    // private:

    private _creationDate: string;
    private _title: string;
    private _message: string;

    // #endregion //
    

    // #region Properties //

    public get creationDate(): string { return this._creationDate; }
    public get title(): string { return this._title; }
    public get message(): string { return this._message; }

    // #endregion //


    // #region Methods //

    // public:

    public parse( jsonData: Object ): boolean
	{
		let result: boolean = true;

		try
		{
            this._creationDate = jsonData[ "creationDate" ];
            this._title = jsonData[ "title" ];
            this._message = jsonData[ "message" ];
		}
		catch ( error )
		{
			result = false;

			Utils.log( "NewsVO :: parse() :: error parsing jsonData :: " + error.message );
		}

		return result;
    }
    
    public static parseArray( arrJsonData: Array<Object> ): Array<NewsVO>
	{
		let result: Array<NewsVO> = new Array<NewsVO>();

        for ( let jsonData of arrJsonData )
        {
            let vo: NewsVO = new NewsVO();
            if ( vo.parse( jsonData ) )
            {
                result.push( vo );
            }
            else
            {
                result = null;
                break;
            }
        }

		return result;
    }

    // #endregion //
}