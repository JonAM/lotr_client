import Utils from "../Utils";


export default class TreasureListVO
{
    // #region Attributes //

    // private:

    private _title: string = null;
    private _dateTimestamp: number = null;
    private _playerId: string = null;
    private _arrPlayerHeroId: Array<string> = null;
    private _allyId: string = null;
    private _arrAllyHeroId: Array<string> = null;
    private _scenarioId: string = null;
    private _arrTreasureCardIndex: Array<number> = null;

    private _whenTimestamp: number = null;

    // #endregion //
    

    // #region Properties //

    public get title(): string { return this._title; }
    public get dateTimestamp(): number { return this._dateTimestamp; }
    public get playerId(): string { return this._playerId; }
    public get playerHeroes(): Array<string> { return this._arrPlayerHeroId; }
    public get allyId(): string { return this._allyId; }
    public get allyHeroes(): Array<string> { return this._arrAllyHeroId; }
    public get scenarioId(): string { return this._scenarioId; }
    public get list(): Array<number> { return this._arrTreasureCardIndex; }
    public get whenTimestamp(): number { return this._whenTimestamp; }

    // #endregion //


    // #region Methods //

    // public:

    public parse( jsonData: Object ): boolean
	{
		let result: boolean = true;

		try
		{
            this._title = jsonData[ "title" ];
            this._dateTimestamp = parseInt( jsonData[ "dateTimestamp" ] );
            this._playerId = jsonData[ "username" ];
            this._arrPlayerHeroId = Utils.split( jsonData[ "playerHeroes" ], "-" );
            this._allyId = jsonData[ "ally" ];
            this._arrAllyHeroId = Utils.split( jsonData[ "allyHeroes" ], "-" );
            this._scenarioId = jsonData[ "scenarioId" ];   
            this._arrTreasureCardIndex = new Array<number>();
            const kListStr: string = jsonData[ "list" ];
            if ( kListStr != "" )
            {
                let arrTreasureIndexStr: Array<string> = kListStr.split( "-" );
                for ( let treasureIndexStr of arrTreasureIndexStr )
                {
                    this._arrTreasureCardIndex.push( parseInt( treasureIndexStr ) );
                }
            }
            this._whenTimestamp = parseInt( jsonData[ "whenTimestamp" ] );
		}
		catch ( error )
		{
			result = false;

			Utils.log( "TrasureListVO :: parse() :: error parsing jsonData :: " + error.message );
		}

		return result;
    }

    public serialize(): ISerializedTreasureList
    {
        return {
            title: this._title,
            dateTimestamp: this._dateTimestamp.toString(),
            username: this._playerId,
            playerHeroes: Utils.strArrayToStr( this._arrPlayerHeroId ),
            ally: this._allyId,
            allyHeroes: Utils.strArrayToStr( this._arrAllyHeroId ),
            scenarioId: this._scenarioId,
            list: Utils.intArrayToStr( this._arrTreasureCardIndex ),
            whenTimestamp: this._whenTimestamp.toString()
        }
    }
    
    public static parseArray( arrJsonData: Array<Object> ): Array<TreasureListVO>
	{
		let result: Array<TreasureListVO> = new Array<TreasureListVO>();

        for ( let jsonData of arrJsonData )
        {
            let vo: TreasureListVO = new TreasureListVO();
            if ( vo.parse( jsonData ) )
            {
                result.push( vo );
            }
            else
            {
                result = null;
                break;
            }
        }

		return result;
    }

    // #endregion //
}

export interface ISerializedTreasureList
{
    title: string;
    dateTimestamp: string;
    username: string;
    playerHeroes: string;
    ally: string;
    allyHeroes: string;
    scenarioId: string;
    list: string;
    whenTimestamp: string;
}