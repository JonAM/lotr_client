import Utils from "../Utils";
import { IDeck } from "../view/gateway/DeckParser";


export default class CampaignLogVO
{
    // #region Attributes //

    // private:

    private _title: string = null;
    private _dateTimestamp: number = null;
    private _playerId: string = null;
    private _arrPlayerHeroId: Array<string> = null;
    private _allyId: string = null;
    private _arrAllyHeroId: Array<string> = null;
    private _arrFallenHeroId: Array<string> = null;
    private _threatPenalty: number = null;
    private _notes: string = null;
    private _arrCompletedScenario: Array<ICompletedScenario> = null;
    private _arrBoon: Array<ICampaignPoolCard> = null;
    private _arrBurden: Array<ICampaignPoolCard> = null;

    private _whenTimestamp: number = null;

    // #endregion //
    

    // #region Properties //

    public get title(): string { return this._title; }
    public get dateTimestamp(): number { return this._dateTimestamp; }
    public get playerId(): string { return this._playerId; }
    public get playerHeroes(): Array<string> { return this._arrPlayerHeroId; }
    public get allyId(): string { return this._allyId; }
    public get allyHeroes(): Array<string> { return this._arrAllyHeroId; }
    public get fallenHeroes(): Array<string> { return this._arrFallenHeroId; }
    public get threatPenalty(): number { return this._threatPenalty; }
    public get notes(): string { return this._notes; }
    public get completedScenarios(): Array<ICompletedScenario> { return this._arrCompletedScenario; }
    public get boons(): Array<ICampaignPoolCard> { return this._arrBoon; }
    public get burdens(): Array<ICampaignPoolCard> { return this._arrBurden; }

    public get whenTimestamp(): number { return this._whenTimestamp; }

    public set threatPenalty( value: number ) { this._threatPenalty = value; }
    public set notes( value: string ) { this._notes = value; }


    // #endregion //


    // #region Methods //

    // public:

    public parse( jsonData: Object ): boolean
	{
		let result: boolean = true;

		try
		{
            const kCampaignLogJson: ISerializedCampaignLog = jsonData as ISerializedCampaignLog;
            this._title = kCampaignLogJson.title
            this._dateTimestamp = parseInt( kCampaignLogJson.dateTimestamp );
            this._playerId = kCampaignLogJson.username;
            this._arrPlayerHeroId = Utils.split( kCampaignLogJson.playerHeroes, "-" );
            this._allyId = kCampaignLogJson.ally;
            this._arrAllyHeroId = Utils.split( kCampaignLogJson.allyHeroes, "-" );
            this._arrFallenHeroId = Utils.split( kCampaignLogJson.fallenHeroes, "-" );
            this._threatPenalty = parseInt( kCampaignLogJson.threatPenalty );
            this._notes = kCampaignLogJson.notes;
            this._arrCompletedScenario = this.parseCompletedScenarios( kCampaignLogJson.completedScenarios );
            this._arrBoon = this.parseCampaignPoolCards( kCampaignLogJson.boons );
            this._arrBurden = this.parseCampaignPoolCards( kCampaignLogJson.burdens );

            this._whenTimestamp = parseInt( kCampaignLogJson.whenTimestamp );
		}
		catch ( error )
		{
			result = false;

			Utils.log( "CampaignLogVO :: parse() :: error parsing jsonData :: " + error.message );
		}

		return result;
    }

        private parseCompletedScenarios( completedScenarioListStr: string ): Array<ICompletedScenario>
        {
            let result: Array<ICompletedScenario> = new Array<ICompletedScenario>();

            const kArrCompletedScenario: Array<string> = Utils.split( completedScenarioListStr, "-" );
            for ( let completedScenarioStr of kArrCompletedScenario )
            {
                let arrScenarioIdScore: Array<string> = Utils.split( completedScenarioStr, "#" );
                result.push( { id: arrScenarioIdScore[ 0 ], score: parseInt( arrScenarioIdScore[ 1 ] ) } );
            }

            return result;
        }

        private parseCampaignPoolCards( campaignPoolCardsStr: string ): Array<ICampaignPoolCard>
        {
            let result: Array<ICampaignPoolCard> = new Array<ICampaignPoolCard>();

            const kArrCampaignPoolCard: Array<string> = Utils.split( campaignPoolCardsStr, "-" );
            for ( let campaignPoolCard of kArrCampaignPoolCard )
            {
                let arrCardIdPermanent: Array<string> = Utils.split( campaignPoolCard, "#" );
                result.push( { id: arrCardIdPermanent[ 0 ], permanentHeroId: arrCardIdPermanent[ 1 ] } );
            }

            return result;
        }

    public serialize(): ISerializedCampaignLog
    {
        let arrCompletedScenarioId: Array<string> = new Array<string>();
        let arrCompletedScenarioScore: Array<number> = new Array<number>();
        for ( let completedScenario of this._arrCompletedScenario )
        {
            arrCompletedScenarioId.push( completedScenario.id );
            arrCompletedScenarioScore.push( completedScenario.score );
        }

        return {
            title: this._title,
            dateTimestamp: this._dateTimestamp.toString(),
            username: this._playerId,
            playerHeroes: Utils.strArrayToStr( this._arrPlayerHeroId ),
            ally: this._allyId,
            allyHeroes: Utils.strArrayToStr( this._arrAllyHeroId ),
            fallenHeroes: Utils.strArrayToStr( this._arrFallenHeroId ),
            threatPenalty: this._threatPenalty.toString(),
            notes: this._notes,
            completedScenarios: this.serializeCompletedScenarios( this._arrCompletedScenario ),
            boons: this.serializeCampaignPoolCards( this._arrBoon ),
            burdens: this.serializeCampaignPoolCards( this._arrBurden ),
            whenTimestamp: this._whenTimestamp.toString()
        }
    }

        private serializeCompletedScenarios( arrCompletedScenario: Array<ICompletedScenario> ): string
        {
            let result: string = "";

            for ( let completedScenario of arrCompletedScenario )
            {
                result = completedScenario.id + "#" + completedScenario.score.toString() + "-";
            }
            result = result.substr( 0, result.length - 1 );

            return result;
        }

        private serializeCampaignPoolCards( arrCampaignPoolCard: Array<ICampaignPoolCard> ): string
        {
            let result: string = "";

            for ( let campaignPoolCard of arrCampaignPoolCard )
            {
                result = campaignPoolCard.id + "#" + campaignPoolCard.permanentHeroId + "-";
            }
            result = result.substr( 0, result.length - 1 );

            return result;
        }
    
    public static parseArray( arrJsonData: Array<Object> ): Array<CampaignLogVO>
	{
		let result: Array<CampaignLogVO> = new Array<CampaignLogVO>();

        for ( let jsonData of arrJsonData )
        {
            let vo: CampaignLogVO = new CampaignLogVO();
            if ( vo.parse( jsonData ) )
            {
                result.push( vo );
            }
            else
            {
                result = null;
                break;
            }
        }

		return result;
    }

    // #endregion //
}

export interface ISerializedCampaignLog
{
    title: string;
    dateTimestamp: string;
    username: string;
    playerHeroes: string;
    ally: string;
    allyHeroes: string;
    fallenHeroes: string;
    threatPenalty: string;
    notes: string;
    completedScenarios: string;
    boons: string;
    burdens: string;
    whenTimestamp: string;
}

interface ICompletedScenario
{
    id: string;
    score: number;
}

export interface ICampaignPoolCard
{
    id: string;
    permanentHeroId: string;
}