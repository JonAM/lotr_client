import Utils from "../Utils";
import { IDeck } from "../view/gateway/DeckParser";


export default class GameHistoryVO
{
    // #region Attributes //

    // private:

    private _playerId: string;
    private _dateTimestamp: number;
    private _scenarioId: string;
    private _allyId: string;

    private _whenTimestamp: number;

    // #endregion //
    

    // #region Properties //

    public get playerId(): string { return this._playerId; }
    public get dateTimestamp(): number { return this._dateTimestamp; }
    public get scenarioId(): string { return this._scenarioId; }
    public get allyId(): string { return this._allyId; }
    public get whenTimestamp(): number { return this._whenTimestamp; }


    // #endregion //


    // #region Methods //

    // public:

    public parse( jsonData: Object ): boolean
	{
		let result: boolean = true;

		try
		{
            this._playerId = jsonData[ "username" ];
            this._dateTimestamp = parseInt( jsonData[ "dateTimestamp" ] );
            this._scenarioId = jsonData[ "scenarioId" ];
            this._allyId = jsonData[ "ally" ];
            this._whenTimestamp = parseInt( jsonData[ "whenTimestamp" ] );
		}
		catch ( error )
		{
			result = false;

			Utils.log( "GameHistoryVO :: parse() :: error parsing jsonData :: " + error.message );
		}

		return result;
    }
    
    public static parseArray( arrJsonData: Array<Object> ): Array<GameHistoryVO>
	{
		let result: Array<GameHistoryVO> = new Array<GameHistoryVO>();

        for ( let jsonData of arrJsonData )
        {
            let vo: GameHistoryVO = new GameHistoryVO();
            if ( vo.parse( jsonData ) )
            {
                result.push( vo );
            }
            else
            {
                result = null;
                break;
            }
        }

		return result;
    }

    // #endregion //
}