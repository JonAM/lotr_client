import { game_state_id } from "../../states/StateId";

import Signal from "../../lib/signals/Signal";
import Session from "../../Session";


export default class GameSocketIOController
{
    // #region Atributes //

    private _socket: SocketIOClient.Socket = null;

    // Signals.
    private _onOpponentActionNotified: Signal = new Signal();
    private _onGameInvalidated: Signal = new Signal();
    private _onGameResumed: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public set socket( value: SocketIOClient.Socket ) { this._socket = value; }

    // Signals.
    public get onOpponentActionNotified(): Signal { return this._onOpponentActionNotified; }
    public get onGameInvalidated(): Signal { return this._onGameInvalidated; }
    public get onGameResumed(): Signal { return this._onGameResumed; }

    // #endregion //


    // #region Methods //

    // public:

    public init(): void 
    {
        console.assert( this._socket != null, "GameSocketIOController.ts :: init() :: this._socket cannot be null." );

        // Callbacks.
        this._socket.on( "oppActionNotified", ( actionType: player_action_type, gameStateId: game_state_id, args: any ) => { this._onOpponentActionNotified.dispatch( actionType, gameStateId, args ); } ) ;
        this._socket.on( "gameResumed", () => { this._onGameResumed.dispatch(); } ) ;
        this._socket.on( "gameInvalidated", () => { this._onGameInvalidated.dispatch(); } );
    }

    public end(): void
    {
        this._onOpponentActionNotified.removeAll();
        this._onGameInvalidated.removeAll();
        this._onGameResumed.removeAll();

        this._socket = null;
    }

    // #region Common //

    public tryGameReconnection( gameId: string ): void
    {
        this._socket.emit( "tryGameReconnection", gameId );
    }

    public notifyAction( actionType: player_action_type, gameStateId: game_state_id, args: Array<any>, isEcho: boolean = false ): void
    {
        if ( Session.allyId ) 
        {
            this._socket.emit( "notifyAction", actionType, gameStateId, args, isEcho );
        }
    }

    public restart(): void
    {
        this._socket.emit( "restart" );
    }

    public gameOver(): void
    {
        this._socket.emit( "gameOver" );
    }

    // #endregion //

    // #endregion //
}

export const enum action_scope_type
{
    LOCAL = 0,
    MULTIPLAYER
}

// IMPORTANT: GameEventManager.js uses LOCK_GAME_ELEMENT, UNLOCK_GAME_ELEMENT and LOCK_STATUS_RECEIVED raw values!!!
export const enum player_action_type
{
    // Setup.
    NOTIFY_SETUP_COMPLETED = 0,
    // Game element sharing.
    LOCK_GAME_ELEMENT, 
    UNLOCK_GAME_ELEMENT,
    LOCK_STATUS_RECEIVED,
    // Card.
    SET_CARD_FACE_UP,
    SET_CARD_FACE_DOWN,
    // Card token.
    SET_CARD_TOKEN_FACE_UP,
    SET_CARD_TOKEN_FACE_DOWN,
    BOW_CARD_TOKEN,
    READY_CARD_TOKEN,
    TRIGGER_ABILITY,
    ADD_CARD_TOKEN_STATUS,
    REMOVE_CARD_TOKEN_STATUS,
    ADD_CARD_UNDERNEATH,
    ADD_SHADOW_CARD,
    ADD_SHADOW_CARD_MINI,
    COMMIT_TO_QUEST,
    UNCOMMIT_FROM_QUEST,
    ADD_CARD_TOKEN_HIGHLIGHT,
    REMOVE_CARD_TOKEN_HIGHLIGHT,
    // // // Shadow card token.
    SET_SHADOW_CARD_FACE_UP,
    SET_SHADOW_CARD_FACE_DOWN,
    DISCARD_SHADOW_CARD,
    // // // Attachment token.
    BOW_ATTACHMENT_TOKEN,
    READY_ATTACHMENT_TOKEN,
    SET_ATTACHMENT_FACE_UP,
    SET_ATTACHMENT_FACE_DOWN,
    ADD_ATTACHMENT_HIGHLIGHT,
    REMOVE_ATTACHMENT_HIGHLIGHT,
    // Game modifier.
    BOW_GAME_MODIFIER,
    READY_GAME_MODIFIER,
    SET_GAME_MODIFIER_FACE_UP,
    SET_GAME_MODIFIER_FACE_DOWN,
    ADD_GAME_MODIFIER_HIGHLIGHT,
    REMOVE_GAME_MODIFIER_HIGHLIGHT,
    USE_GAME_MODIFIER,
    // Detail bar.
    SET_CUSTOM_TEXT,
    REMOVE_CUSTOM_TEXT,
    ADD_CUSTOM_COUNTER,
    REMOVE_DETAIL_BAR_ITEM,
    // POI.
    SHOW_POI,
    REMOVE_POI,
    // Unique token drag.
    SET_DRAG_FILTER,
    REMOVE_DRAG_FILTER,
    // Attack binding.
    ADD_ATTACK_BINDING,
    REMOVE_ATTACKER_BINDINGS,
    REMOVE_DEFENDER_BINDINGS,
    // Action request.
    ACTION_REQUESTED,
    ACTION_REQUEST_CANCELLED,
    ACTION_REQUEST_REJECTED,
    ACTION_REQUEST_ACCEPTED,
    // Card activation.
    CARD_ACTIVATION_PRESENT,
    CARD_ACTIVATION_FLIP,
    CARD_ACTIVATION_PLACE,
    CARD_ACTIVATION_DISCARD,
    // Campaign log.
    CAMPAIGN_LOG_ADD_ROW,
    CAMPAIGN_LOG_REMOVE_ROW,
    CAMPAIGN_LOG_SET_THREAT_PENALTY,
    CAMPAIGN_LOG_SET_NOTES,
    // // 
    USE_ATTACHMENT,
    ADD_TO_TOKEN_COUNTER,
    SET_TOKEN_COUNTER,
    DROP_AT_DROP_AREA,
    VIEWER_CLOSED,
    SHUFFLE_DECK,
    REVEAL_X,
    DRAW_X_PLAYER_CARDS,
    DISCARD_X,
    REMOVE_ALL_CARDS,
    SWITCH_DECKS,
    RESHUFFLE_DECK,
    SORT_HAND_BY_SPHERE,
    SORT_DECK_FROM_A_TO_Z,
    LOG_ACTION,
    SET_CHARACTER_AREA_TOKEN_INDEX,
    SET_STAGING_THREAT_COUNTER_AUTO,
    SHOW_STAGING_THREAT_COUNTER_AUTO,
    SET_PURSUIT_THREAT_COUNTER_AUTO,
    SHOW_PURSUIT_THREAT_COUNTER_AUTO,
    SET_PLAYER_WILLPOWER_COUNTER_ATTRIBUTE,
    SET_ACTIVE_PLAYER,
    SET_END_TURN_BTN_STATE,
    LEAVE_GAME,
    ADD_GENERIC_ENEMY,
    SET_GAME_PHASE_ENABLED,
    SET_AUTO_SKIP_EMPTY_PHASES,
    SET_RESOURCE_PHASE_ADD_TOKEN,
    SET_RESOURCE_PHASE_DRAW_CARD,
    SET_STAGING_SUBPHASE_DRAW_CARD,
    DELETE_GAME_OBJECT,
    TOGGLE_PURSUIT_BTN,
    SELECT_MULTI_ITEM_SELECTOR,
    ROTATE_HEADING,
    TOGGLE_INVESTIGATION_LIST_ITEM,
    TOGGLE_TREASURE_LIST_ITEM,
    TOGGLE_SETUP_LIST_ITEM,
    SET_AUTO_COUNTER,
    TOGGLE_DEFENDED_ATTACK,
    SPLIT_STAGING_AREA,
    MERGE_STAGING_AREA
}

export interface IRemoteCard
{
    cid: string;
    oid: string;
    isFaceUp: boolean;
}