import { IDeck } from "../../view/gateway/DeckParser";
import Signal from "../../lib/signals/Signal";
import { ISaveGame } from "../../view/game/SaveGameView";
import { ISerializedCampaignLog } from "../../vo/CampaignLogVO";
import { ISerializedTreasureList } from "../../vo/TreasureListVO";


export default class GatewaySocketIOController
{
    // #region Atributes //

    private _socket: SocketIOClient.Socket = null;

    // Signals.
    private _onConnectionAccepted: Signal = new Signal();
    private _onConnectionRejected: Signal = new Signal();
    private _onSongPlayed: Signal = new Signal();
    // Player list.
    private _onUserConnected: Signal = new Signal();
    private _onUserDisconnected: Signal = new Signal();
    private _onUserListRetrieved: Signal = new Signal();
    private _onUserStatusUpdated: Signal = new Signal();
    private _onGameInvitationReceived: Signal = new Signal();
    private _onGameInvitationCanceled: Signal = new Signal();
    private _onGameInvitationAccepted: Signal = new Signal();
    private _onGameInvitationRejected: Signal = new Signal();
    // Game room.
    private _onGameRoomRetrieved: Signal = new Signal();
    private _onUserReadyReceived: Signal = new Signal();
    private _onScenarioReceived: Signal = new Signal();
    private _onIsCampaignModeReceived: Signal = new Signal();
    private _onCampaignLogReceived: Signal = new Signal();
    private _onTreasureListReceived: Signal = new Signal();
    private _onUserLeftRoom: Signal = new Signal();
    private _onGameCreated: Signal = new Signal();
    private _onGameLoaded: Signal = new Signal();
    private _onOpponentGameCreated: Signal = new Signal();
    // Chat.
    private _onGlobalMessageReceived: Signal = new Signal();
    private _onGameMessageReceived: Signal = new Signal();
    private _onPrivateMessageReceived: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public set socket( value: SocketIOClient.Socket ) { this._socket = value; }

    // Signals.
    public get onConnectionAccepted(): Signal { return this._onConnectionAccepted; }
    public get onConnectionRejected(): Signal { return this._onConnectionRejected; }
    public get onSongPlayed(): Signal { return this._onSongPlayed; }
    // Player list.
    public get onUserConnected(): Signal { return this._onUserConnected; }
    public get onUserDisconnected(): Signal { return this._onUserDisconnected; }
    public get onUserListRetrieved(): Signal { return this._onUserListRetrieved; }
    public get onUserStatusUpdated(): Signal { return this._onUserStatusUpdated; }
    public get onGameInvitationReceived(): Signal { return this._onGameInvitationReceived; }
    public get onGameInvitationCanceled(): Signal { return this._onGameInvitationCanceled; }
    public get onGameInvitationAccepted(): Signal { return this._onGameInvitationAccepted; }
    public get onGameInvitationRejected(): Signal { return this._onGameInvitationRejected; }
    // Game room.
    public get onGameRoomRetrieved(): Signal { return this._onGameRoomRetrieved; }
    public get onUserReadyReceived(): Signal { return this._onUserReadyReceived; }
    public get onScenarioReceived(): Signal { return this._onScenarioReceived; }
    public get onIsCampaignModeReceived(): Signal { return this._onIsCampaignModeReceived; }
    public get onCampaignLogReceived(): Signal { return this._onCampaignLogReceived; }
    public get onTreasureListReceived(): Signal { return this._onTreasureListReceived; }
    public get onUserLeftRoom(): Signal { return this._onUserLeftRoom; }
    public get onGameCreated(): Signal { return this._onGameCreated; }
    public get onGameLoaded(): Signal { return this._onGameLoaded; }
    public get onOpponentGameCreated(): Signal { return this._onOpponentGameCreated; }
    // Chat.
    public get onGlobalMessageReceived(): Signal { return this._onGlobalMessageReceived; }
    public get onGameMessageReceived(): Signal { return this._onGameMessageReceived; }
    public get onPrivateMessageReceived(): Signal { return this._onPrivateMessageReceived; }
    

    // #endregion //


    // #region Methods //

    // public:

    public init(): void 
    {
        console.assert( this._socket != null, "GatewaySocketIOController.ts :: init() :: this._socket cannot be null." );

        // Callbacks.
        this._socket.on( "connectionAccepted", () => { this._onConnectionAccepted.dispatch(); } ) ;
        this._socket.on( "connectionRejected", ( reasonCode: number ) => { this._onConnectionRejected.dispatch( reasonCode ); } ) ;
        this._socket.on( "songPlayed", ( songIndex: number ) => { this._onSongPlayed.dispatch( songIndex ); } ) ;
        // Player list.
        this._socket.on( "userConnected", ( userId: string ) => { this._onUserConnected.dispatch( userId ); } ) ;
        this._socket.on( "userDisconnected", ( userId: string ) => { this._onUserDisconnected.dispatch( userId ); } ) ;
        this._socket.on( "userListRetrieved", ( arrUser: Array<IUser> ) => { this._onUserListRetrieved.dispatch( arrUser ); } ) ;
        this._socket.on( "userStatusUpdated", ( userId: string, userStatus: user_status_type ) => { this._onUserStatusUpdated.dispatch( userId, userStatus ); } ) ;
        this._socket.on( "gameInvitationReceived", ( userId: string ) => { this._onGameInvitationReceived.dispatch( userId ); } ) ;
        this._socket.on( "gameInvitationCanceled", () => { this._onGameInvitationCanceled.dispatch(); } ) ;
        this._socket.on( "gameInvitationAccepted", () => { this._onGameInvitationAccepted.dispatch(); } ) ;
        this._socket.on( "gameInvitationRejected", () => { this._onGameInvitationRejected.dispatch(); } ) ;
        // Game room.
        this._socket.on( "gameRoomRetrieved", ( allyDeck: IDeck, scenarioId: string, isCampaignMode: boolean, campaignLog: ISerializedCampaignLog, treasureList: ISerializedTreasureList ) => { this._onGameRoomRetrieved.dispatch( allyDeck, scenarioId, isCampaignMode, campaignLog, treasureList ); } ) ;
        this._socket.on( "userReadyReceived", ( allyDeck: IDeck ) => { this._onUserReadyReceived.dispatch( allyDeck ); } ) ;
        this._socket.on( "scenarioReceived", ( scenarioId: string ) => { this._onScenarioReceived.dispatch( scenarioId ); } ) ;
        this._socket.on( "isCampaignModeReceived", ( isCampaignMode: boolean ) => { this._onIsCampaignModeReceived.dispatch( isCampaignMode ); } ) ;
        this._socket.on( "campaignLogReceived", ( campaignLog: ISerializedCampaignLog ) => { this._onCampaignLogReceived.dispatch( campaignLog ); } ) ;
        this._socket.on( "treasureListReceived", ( treasureList: ISerializedTreasureList ) => { this._onTreasureListReceived.dispatch( treasureList ); } ) ;
        this._socket.on( "userLeftRoom", ( userId: string ) => { this._onUserLeftRoom.dispatch( userId ); } ) ;
        this._socket.on( "gameCreated", ( oppDeck: IDeck, prngSeed: number, firstPlayerId: string, gameId: string ) => { this._onGameCreated.dispatch( oppDeck, prngSeed, firstPlayerId, gameId ); } ) ;
        this._socket.on( "gameLoaded", ( prngSeed: number, gameId: string, saveGame: ISaveGame ) => { this._onGameLoaded.dispatch( prngSeed, gameId, saveGame ); } ) ;
        this._socket.on( "opponentGameCreated", () => { this._onOpponentGameCreated.dispatch(); } ) ;
        // Chat.
        this._socket.on( "globalMessageReceived", ( from: string, message: string, nowStr: string ) => { this._onGlobalMessageReceived.dispatch( from, message, new Date( nowStr ) ); } ) ;
        this._socket.on( "gameMessageReceived", ( from: string, message: string, nowStr: string ) => { this._onGameMessageReceived.dispatch( from, message, new Date( nowStr ) ); } ) ;
        this._socket.on( "privateMessageReceived", ( from: string, message: string, nowStr: string ) => { this._onPrivateMessageReceived.dispatch( from, message, new Date( nowStr ) ); } ) ;
    }

    public end(): void
    {
        this._socket = null;
    }

    // #region Player list //

    public retrieveUserList(): void
    {
        this._socket.emit( "retrieveUserList" );
    }

    public inviteToGame( userId: string ): void
    {
        this._socket.emit( "inviteToGame", userId );
    }

    public cancelGameInvitation( userId: string ): void
    {
        this._socket.emit( "cancelGameInvitation", userId );
    }

    public acceptGameInvitation( userId: string ): void
    {
        this._socket.emit( "acceptGameInvitation", userId );
    }

    public rejectGameInvitation( userId: string ): void
    {
        this._socket.emit( "rejectGameInvitation", userId );
    }

    public setUserStatus( userStatus: number ): void
    {
        this._socket.emit( "setUserStatus", userStatus );
    }

    // #endregion //

    
    // #region Game room //

    public clearLastGameRoom(): void
    {
        this._socket.emit( "clearLastGameRoom" );
    }

    public retrieveGameRoom(): void
    {
        this._socket.emit( "retrieveGameRoom" );
    }

    public setReadyStatus( deck: IDeck ): void
    {
        this._socket.emit( "setReadyStatus", deck );
    }

    public setScenario( scenarioId: string ): void
    {
        this._socket.emit( "setScenario", scenarioId );
    }

    public setIsCampaignMode( isCampaignMode: boolean ): void
    {
        this._socket.emit( "setIsCampaignMode", isCampaignMode );
    }

    public setCampaignLog( campaignLog: ISerializedCampaignLog ): void
    {
        this._socket.emit( "setCampaignLog", campaignLog );
    }

    public setTreasureList( treasureList: ISerializedTreasureList ): void
    {
        this._socket.emit( "setTreasureList", treasureList );
    }

    public startGame(): void
    {
        this._socket.emit( "startGame" );
    }

    public loadGame( saveGame: ISaveGame ): void
    {
        this._socket.emit( "loadGame", saveGame );
    }

    public leaveGameRoom( isHost: boolean ): void
    {
        this._socket.emit( "leaveGameRoom", isHost );
    }

    public setOpponentGameCreated(): void
    {
        this._socket.emit( "opponentGameCreated" );
    }

    // #endregion //

    // #region Chat //

    public sendGlobalMessage( message: string ): void
    {
        this._socket.emit( "sendGlobalMessage", message );
    }

    public sendGameMessage( message: string ): void
    {
        this._socket.emit( "sendGameMessage", message );
    }

    public sendPrivateMessage( to: string, message: string ): void
    {
        this._socket.emit( "sendPrivateMessage", to, message );
    }

    // #endregion //

    // #endregion //
}

export const enum user_status_type
{
    DISCONNECTED = -1,
    IDLE = 0,
    NEGOTIATING_GAME,
    IN_GAME_ROOM,
    PLAYING
}

export interface IUser
{
    id: string;
    status: user_status_type;
}