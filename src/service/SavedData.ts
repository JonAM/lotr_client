import { IKeyBindings } from "../game/KeyBindingManager";


export default class SavedData
{
    // #region Attributes //

    // private:

    private _playerId: string = null;

    private _data: IData = null;
    
    // Constants.
	private readonly _kSaveName: string = "tpp_player_";
    
    // #endregion //


    // #region Properties //
    
    public get data(): IData { return this._data; }

    public set playerId( value: string ) { this._playerId = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        this._data = { 
            isHideNightmareScenarios: false,
            scenarioSelectionCardSet: "01",
            isTavernAmbient: true,
            isMusic: true,
            isSfx: true,
            musicVolume: 0.2,
            sfxVolume: 0.5,
            ambientVolume: 0.6,
            chatShowDisconnected: true,
            keyBindings: {
                chat: "KeyQ",
                hand: "Space",
                poiMenu: "ShiftLeft",
                forceDrag: "KeyD",
                endTurn: "CotrolLeft" },
            gamePreferences: {
                isManuallyRevealShadowCards: false,
                isSkipPhaseIntro: false,
                isAutoEndResourcePhase: false,
                touchOverDelayInMs: 500,
                cardPreviewPreferredSide: "right" } };
    }
	
    public init(): void
    {
        console.assert( this._playerId != null, "SavedData.ts :: init() :: this._playerId cannot be null." );

        let dataStr: string = window.localStorage.getItem( this._kSaveName + this._playerId );
        if ( dataStr != null )
        {
            this._data = JSON.parse( dataStr );
            // Backwards compatibility.
            if ( !this._data.scenarioSelectionCardSet )
            {
                this._data.scenarioSelectionCardSet = "01";
            }
            if ( !this._data.keyBindings.forceDrag )
            {
                this._data.keyBindings.forceDrag = "KeyD";
            }
            if ( !this._data.keyBindings.endTurn )
            {
                this._data.keyBindings.endTurn = "ControlLeft";
            }
            if ( this._data.gamePreferences == undefined )
            {
                this._data.gamePreferences = { 
                    isManuallyRevealShadowCards: false,
                    isSkipPhaseIntro: false,
                    isAutoEndResourcePhase: false,
                    touchOverDelayInMs: 500,
                    cardPreviewPreferredSide: "right" }
            }
            if ( this._data.gamePreferences.isSkipPhaseIntro == undefined )
            {
                this._data.gamePreferences.isSkipPhaseIntro = false;
            }
            if ( this._data.gamePreferences.isAutoEndResourcePhase == undefined )
            {
                this._data.gamePreferences.isAutoEndResourcePhase = false;
            }
            if ( this._data.gamePreferences.touchOverDelayInMs == undefined )
            {
                this._data.gamePreferences.touchOverDelayInMs = 500;
            }
            if ( this._data.gamePreferences.cardPreviewPreferredSide == undefined )
            {
                this._data.gamePreferences.cardPreviewPreferredSide = "right";
            }
        }
    }

    public save(): void
    {
        let dataStr: string = JSON.stringify( this._data );
        window.localStorage.setItem( this._kSaveName + this._playerId, dataStr );
    }
}

interface IData
{
    isHideNightmareScenarios: boolean;
    scenarioSelectionCardSet: string;
    isTavernAmbient: boolean;
    isMusic: boolean;
    isSfx: boolean;
    musicVolume: number;
    sfxVolume: number;
    ambientVolume: number;
    chatShowDisconnected: boolean;
    keyBindings: IKeyBindings;
    gamePreferences: IGamePreferences;
}

interface IGamePreferences
{
    isManuallyRevealShadowCards: boolean;
    isSkipPhaseIntro: boolean;
    isAutoEndResourcePhase: boolean;
    touchOverDelayInMs: number;
    cardPreviewPreferredSide: string;
}