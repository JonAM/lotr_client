import Url from "../Url";
import * as io from 'socket.io-client';

import GameSocketIOController from "./socket_io/GameSocketIOController";
import GatewaySocketIOController from "./socket_io/GatewaySocketIOController";
import Signal from "../lib/signals/Signal";


export default class SocketIOManager
{
    // #region Atributes //

    // private:

    private _socket: SocketIOClient.Socket = null;;

    private _gateway: GatewaySocketIOController = null;
    private _game: GameSocketIOController = null;

    // Signals.
    private _onDisconnect: Signal = new Signal();
    private _onReconnect: Signal = new Signal();
    private _onConnectError: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get gateway(): GatewaySocketIOController { return this._gateway; }
    public get game(): GameSocketIOController { return this._game; }

    // Signals.
    public get onDisconnect(): Signal { return this._onDisconnect; }
    public get onReconnect(): Signal { return this._onReconnect; }
    public get onConnectError(): Signal { return this._onConnectError; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        this._gateway = new GatewaySocketIOController();
        this._game = new GameSocketIOController();
    }

    public connect( userId: string ): void 
    {
        this._socket = io.connect( Url.socketIOServer, { query: { userId: userId } } );

        // Callbacks.
        this._socket.on( "connect_error", () => { this.onConnectError.dispatch(); } );
        this._socket.on( "error", this.onError.bind( this ) );
        this._socket.on( "disconnect", () => { this._onDisconnect.dispatch(); } );
        this._socket.on( "reconnect", () => { this._onReconnect.dispatch(); } );
        // Gateway.
        this._gateway.socket = this._socket;
        this._gateway.init();
        // Game.
        this._game.socket = this._socket;
        this._game.init();
    }

    public disconnect(): void
    {
        this._gateway.end();

        this._game.end();

        this._socket.disconnect();
        this._socket = null;
    }

    public emit( eventId: string, ...args: any[] ): void
    {
        this._socket.emit( eventId, ...args );
    }

    // #endregion //


    // #region Other Callbacks //

    private onError(): void
    {
        console.log( "error" );
    }

    // #endregion //
}