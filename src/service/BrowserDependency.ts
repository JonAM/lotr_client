import Signal from "../lib/signals/Signal";


export default class BrowserDependency
{
    // #region Attributes //

    // private:

    private _visibilityApi: IVisibilityParams
    private _isTouchDevice: boolean = false;

    // Binded functions.
    private _bfOnFirstTouchStart: any = null;

    // Signals.
    private _onTouchDeviceDetected: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get visibilityApi(): IVisibilityParams { return this._visibilityApi; }
    public get isTouchDevice(): boolean { return this._isTouchDevice; }

    // Signals.
    public get onTouchDeviceDetected(): Signal { return this._onTouchDeviceDetected; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() 
    {
        this._visibilityApi = this.findVisibilityParameters();

        this._bfOnFirstTouchStart = this.onFirst_TouchStart.bind( this );
        window.addEventListener( "touchstart", this._bfOnFirstTouchStart, false );
    }

    // private: 

    private findVisibilityParameters(): IVisibilityParams
    {
        let result: IVisibilityParams = { hidden: null, visibilityChange: null };

        if ( typeof document.hidden !== "undefined" ) 
        { 
            // Opera 12.10 and Firefox 18 and later support 
            result.hidden = "hidden";
            result.visibilityChange = "visibilitychange";
        } 
        else if ( typeof document[ "msHidden" ] !== "undefined" ) 
        {
            result.hidden = "msHidden";
            result.visibilityChange = "msvisibilitychange";
        } 
        else if ( typeof document[ "webkitHidden" ] !== "undefined" ) 
        {
            result.hidden = "webkitHidden";
            result.visibilityChange = "webkitvisibilitychange";
        }

        return result;
    }

    // #endregion //


    // #region Input Callbacks //

    private onFirst_TouchStart() 
    {
        this._isTouchDevice = true;
        window.removeEventListener( "touchstart", this._bfOnFirstTouchStart, false ); 

        this._bfOnFirstTouchStart = null;

        this._onTouchDeviceDetected.dispatch();
    }

    // #endregion //
}

export interface IVisibilityParams
{
    hidden: string;
    visibilityChange: string;
}