import * as PIXI from "pixi.js";


export default class ResourceStack
{
    // #region Attributes //

    // private:

    private _arrResources: Array<PIXI.IResourceDictionary> = new Array<PIXI.IResourceDictionary>();

    // #endregion //


    // #region Methods //

    // public:

    public constructor() {}

    public push(): void
    {
        this._arrResources.push( {} );
    }

    public pop(): void
    {
        this._arrResources.splice( this._arrResources.length - 1, 1 );
    }

    public find( id: string ): PIXI.LoaderResource
    {
        let result: PIXI.LoaderResource = null;

        for ( let resources of this._arrResources )
        {
            let resAux: PIXI.LoaderResource = resources[ id ];
            if ( resAux )
            {
                result = resAux;
                break;
            }
        }

        return result;
    }

    public findAsTexture( id: string ): PIXI.Texture
    {
        return PIXI.Texture.from( this.find( id ).data );
    }

    public set( id: string, resource: PIXI.LoaderResource, stackIndex: number = null ): void
    {
        if ( stackIndex == null )
        {
            stackIndex = this._arrResources.length - 1;
        }

        this._arrResources[ stackIndex ][ id ] = resource;
    }

    // #endregion //
}