import Config from "../Config";
import ServiceLocator from "../ServiceLocator";
import * as PIXI from "pixi.js";
import Signal from "../lib/signals/Signal";


export default class ResourceLoader
{
    // #region Attributes //

    // private:

    private _loader: PIXI.Loader = null;

    private _arrResourceRequest: Array<IResourceRequest> = new Array<IResourceRequest>();

    // Signals.
    private _onProgress: Signal = new Signal();

    // #endregion //


    // #region Properties //

    // Signals.
    public get onProgress(): Signal { return this._onProgress; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        this._loader = new PIXI.Loader();
        this._loader.onProgress.add( () => { this._onProgress.dispatch( this._loader.progress ); } );
        this._loader.pre( this.beforeResourceLoaded );
        //this._loader.defaultQueryString = "ws=" + Config.kBuildVersion;
    }

        private beforeResourceLoaded( r: PIXI.LoaderResource, next: any ): void
        {
            if ( Config.kIsResourceLoaderQueryString )
            {
                r.url += Config.kQueryString;
            }

            next();
        }

    public isLoading(): boolean
    {
        return this._loader.loading;
    }

    public add( id: string, path: string, options: PIXI.ILoaderOptions = null ): void
    {
        if ( !this._loader.resources[ id ] )
        {
            this._loader.add( id, path, options );
            this._arrResourceRequest.push( { id: id, isOnce: false } );
        }
    }

    public addOnce( id: string, path: string, options: PIXI.ILoaderOptions = null ): void
    {
        if ( !this._loader.resources[ id ] )
        {
            // Don't add to PIXI's texture cache.
            // options = options || { metadata: { noTexture: true } };
            // if ( !options.metadata.noTexture )
            // {
            //     options.metadata.noTexture = true;
            // }

            this._loader.add( id, path, options );
            this._arrResourceRequest.push( { id: id, isOnce: true } );
        }
    }

    public load( onLoadedCb: ( resources: PIXI.IResourceDictionary ) => void ): void
    {
        this._loader.load( this.onResources_Loaded.bind( this, this._arrResourceRequest, onLoadedCb ) );
        this._arrResourceRequest = [];
    }

    public reset(): void
    {
        let isLoading: boolean = this._loader.loading;

        this._loader.reset(); 
        if ( isLoading )
        {
            // IMPORTANT: Calling reset() is not enough, since the loader seems to be left in an unpredicted state. 
            this._loader = new PIXI.Loader();
        }

        this._arrResourceRequest = [];
    }

    // private:

    private onResources_Loaded( arrResourceRequest: Array<IResourceRequest>, onLoadedCb: ( resources: PIXI.IResourceDictionary ) => void, loader: PIXI.Loader, resources: PIXI.IResourceDictionary ): void
    {
        loader.reset();

        // Save persistent resources.
        for ( let resourceRequest of arrResourceRequest )
        {
            if ( !resourceRequest.isOnce )
            {
                ServiceLocator.resourceStack.set( resourceRequest.id, resources[ resourceRequest.id ] );
            }
        }
        
        onLoadedCb( resources );
    }

    // #endregion //
}

interface IResourceRequest
{
    id: string;
    isOnce: boolean;
}