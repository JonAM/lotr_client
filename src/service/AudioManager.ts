import { Howl } from "howler";
import ServiceLocator from "../ServiceLocator";
import { main_state_id } from "../states/StateId";


export default class AudioManager
{
    // #region Attributes //

    // private:

    private _arrVolume: Array<number> = [ 1, 1, 1 ];
    private _arrMute: Array<boolean> = [ false, false, false ];

    private _arrSoundCollection: Array<Map<string, Howl>> = [ new Map<string, Howl>(), new Map<string, Howl>(), new Map<string, Howl>() ];

    private _curMusicName: string = null; // only one music track is played at a time.

    // Constants.
    private readonly _kFadeDuration: number = 500;
    
    // #endregion //


    // #region Properties //

    public get isMuteSfx(): boolean { return this._arrMute[ sound_group_id.SFX ]; }
    public get isMuteMusic(): boolean { return this._arrMute[ sound_group_id.MUSIC ]; }
    public get isMuteAmbient(): boolean { return this._arrMute[ sound_group_id.AMBIENT ]; }

    // #endregion //


    // #region Methods //

    // public:

    /**
     * Adds the sounds. Creates a Howl that can be played later.
     * @param {!Array<ISoundLoadGroup>} arrSoundLoadGroup - array of sound files names excluding their extensions.
     */
    public load( arrSoundLoadGroup: Array<ISoundLoadGroup> ): void
    {
        for ( let addSoundGroup of arrSoundLoadGroup ) 
        {		
            for ( let soundName of addSoundGroup.arrName )
            {
                // Skip repeated sounds.
                let mapSound: Map<string, Howl> = this._arrSoundCollection[ addSoundGroup.soundGroupId ];
                if ( !mapSound.has( soundName ) )
                {
                    // create array with full paths including the extensions.
                    const kUrl: string = addSoundGroup.basePath + soundName;
                    let arrUrlWithExtension: Array<string> = new Array<string>();
                    for ( let extension of addSoundGroup.arrExtension ) 
                    {
                        arrUrlWithExtension.push( kUrl + "." + extension );
                    }

                    // create HOWL.
                    let howl: Howl = new Howl( {
                        src : arrUrlWithExtension,
                        autoplay : false,

                        onloaderror : function( soundId: number, error: any ) {
                            if ( this._kIsDebugMode )
                            {
                                console.warn( "[AudioManager] Error loading sound:" + soundId + ", with error: " + error );
                            }
                        }
                    } );

                    mapSound.set( soundName, howl );
                }
            }
        }
    }

    /**
     * Remove the sounds.
     * @param {!Array<string>} arrSoundName - array of sounds to be removed. Identified by their names.
     */
    public removeSounds( arrSoundName: Array<string>, soundGroupId: sound_group_id ): void 
    {
        let mapSound: Map<string, Howl> = this._arrSoundCollection[ soundGroupId ];
        for ( let soundName of arrSoundName ) 
        {
            let howl: Howl = mapSound.get( soundName );
            if ( howl != null )
            {
                // Cleanup the howl.
                if ( howl.playing() )
                {
                    howl.stop();
                }
                howl.unload();
                howl = null;

                // Remove the entry from the map.
                mapSound.delete( soundName );
            }
        }
    }

    public hasSound( soundName: string, soundGroupId: sound_group_id ): boolean 
    {
        return this._arrSoundCollection[ soundGroupId ].has( soundName );
    }

    public isPlayingSound( soundName: string, soundGroupId: sound_group_id ): boolean 
    {
        return this._findPlayingSound( soundName, soundGroupId ) != null;
    }

    /**
     * Plays a sound.
     * @param {string}      name - the name of a sound.
     * @param {IPlayParams} params - The params for the sound.
     * @returns {Howl}
     */
    public playSfx( name: string, params: IPlayParams = null ) : Howl
    {
        if ( ServiceLocator.stateMachine.currentStateId == main_state_id.GAME 
            && !ServiceLocator.game.isAnimated ) { return; }

        let result: Howl = null;

        if ( !this.hasSound( name, sound_group_id.SFX ) )
        {
            console.warn( "AudioManager.ts :: playSfx() :: cannot find Howl for name: " + name );
        }
        else
        {
            // Check for existing sound.
            result = this._findPlayingSound( name, sound_group_id.SFX );
            if ( result ) 
            {
                if ( result.duration() * ( result.seek() as number ) > 1 )
                {
                    result.play(); // Simple way for allowing playing multiple instances of the same sound.
                }
            }
            else
            {
                // Play the howl.
                result = this._play( name, params, sound_group_id.SFX );
            }
        }

        return result;
    }

    /**
     * Plays a sound.
     * @param {string}      name - the name of a sound.
     * @param {IPlayParams} params - The params for the sound.
     * @returns {Howl}
     */
    public playMusic( name: string, params: IPlayParams ) : Howl
    {
        let result: Howl = null;

        // Check for existing sound.
        result = this._findPlayingSound( name, sound_group_id.MUSIC );
        if ( !result ) 
        {
            this.stopMusic();

            // Play the howl.
            result = this._play( name, params, sound_group_id.MUSIC );
            this._curMusicName = name;
        }

        return result;
    }

    /**
     * Plays a sound.
     * @param {string}      name - the name of a sound.
     * @param {IPlayParams} params - The params for the sound.
     * @returns {Howl}
     */
     public playAmbient( name: string, params: IPlayParams ) : Howl
     {
         let result: Howl = null;
 
         // Check for existing sound.
         result = this._findPlayingSound( name, sound_group_id.AMBIENT );
         if ( !result ) 
         {
             // Play the howl.
             result = this._play( name, params, sound_group_id.AMBIENT );
         }
 
         return result;
     }

    public changeVolume( volume: number, soundGroupId: sound_group_id ): void
    {
        this._arrVolume[ soundGroupId ] = volume;

        let entriesIt: IterableIterator<[string, Howl]> = this._arrSoundCollection[ soundGroupId ].entries();
        for ( let entriesItResult: IteratorResult<[string, Howl]> = entriesIt.next(); !entriesItResult.done; entriesItResult = entriesIt.next() )
        {
            let howl: Howl = entriesItResult.value[ 1 ];
            if ( howl.playing() )
            {
                howl.volume( volume );
            }
        }
    }

    /**
     * Toggle the Mute of all sounds.
     * @param {boolean} isMute
     */
    public mute( isMute: boolean, soundGroupId: sound_group_id ): void
    {
        this._arrMute[ soundGroupId ] = isMute;

        this._updateSoundMuteStatus( isMute, soundGroupId );
    }

    /**
     * Stops a sound.
     * @param {string} name
     */
    public stopSfx( name: string = null ): void
    {
        if ( name )
        {
            let howl: Howl = this._findPlayingSound( name, sound_group_id.SFX );
            if ( howl != null )
            {
                howl.stop();
            }
        }
        else
        {
            this._arrSoundCollection[ sound_group_id.SFX ].forEach( ( value: Howl, key: string ) => {
                value.stop();
            } );
        }
    }

    public stopAmbient( name: string = null ): void
    {
        if ( name )
        {
            let howl: Howl = this._findPlayingSound( name, sound_group_id.AMBIENT );
            if ( howl != null )
            {
                howl.stop();
            }
        }
        else
        {
            this._arrSoundCollection[ sound_group_id.AMBIENT ].forEach( ( value: Howl, key: string ) => {
                value.stop();
            } );
        }
    }

    public stopMusic(): void
    {
        if ( this._curMusicName != null )
        {
            // Utils.log( "AudioManager.ts :: fading-out " + this._curMusicName );

            let howl: Howl = this._arrSoundCollection[ sound_group_id.MUSIC ].get( this._curMusicName );
            howl.fade( howl.volume(), 0, this._kFadeDuration );
            howl.on( "fade", function( name: string ) { 
                let mapMusic: Map<string, Howl> = this._arrSoundCollection[ sound_group_id.MUSIC ];
                if ( mapMusic.has( name ) )
                {
                    mapMusic.get( name ).stop(); 
                    mapMusic.get( name ).off( "fade" );
                    // Utils.log( "AudioManager.ts :: stopping " + name )
                }
            }.bind( this, this._curMusicName ) );

            this._curMusicName = null;
        }
    }

    // private:

    /**
     * Plays the howl. Find its, updates its paramaters, plays it.
     * @param {string} name
     * @param {IPlayParams} params
     * @param {sound_group_id} soundType
     * @returns {Howl}
     * @private
     */
    private _play( name: string, params: IPlayParams, soundGroupId: sound_group_id ): Howl
    {
        let result: Howl = null;

        // parse the params.
        params = params || {};
        params.randomStart = params.randomStart != undefined ? params.randomStart : false;
        params.loop = params.loop != undefined ? params.loop : false;
        params.onComplete = params.onComplete || null;
        params.onCompleteScope = params.onCompleteScope || window;

        // Find the howl;
        let mapSound: Map<string, Howl> = this._arrSoundCollection[ soundGroupId ];
        result = mapSound.get( name );

        // Setup the howl.
        // Volume.
        result.volume( this._arrVolume[ soundGroupId ] );
        result.mute( this._arrMute[ soundGroupId ] );
        // Random start.
        if ( params.randomStart )
        {
            result.seek( result.duration() * Math.random() );
        }
        // Loop.
        result.loop( params.loop );
        // Mute.
        // Callbacks.
        result.on( "end", function() {
            if ( params.onComplete ) 
            {
                params.onComplete.call( params.onCompleteScope );
            }
        }.bind( this ) );

        // Play the howl.
        result.play();
        if ( soundGroupId == sound_group_id.MUSIC && !this._arrMute[ sound_group_id.MUSIC ] ) 
        {
            // If called when muted, it will be unmuted!!!
            result.fade( 0, this._arrVolume[ sound_group_id.MUSIC ], this._kFadeDuration );

            // Utils.log( "AudioManager.ts :: fading-in " + name );
        }

        return result;
    }

    /**
     * Updates the mute status on all sounds.
     * Would be more efficient to only update the ones that have changed.
     * @param {boolean} isMute
     * @param {Array<Howls>} soundArray
     * @private
     */
    private _updateSoundMuteStatus( isMute: boolean, soundGroupId: sound_group_id ): void
    {
        let mapSound: Map<string, Howl> = this._arrSoundCollection[ soundGroupId ];
        let entriesIt: IterableIterator<[string, Howl]> = mapSound.entries();
        for ( let entriesItResult: IteratorResult<[string, Howl]> = entriesIt.next(); !entriesItResult.done; entriesItResult = entriesIt.next() )
        {
            let howl: Howl = entriesItResult.value[ 1 ];
            if ( howl.playing() )
            {
                howl.mute( isMute );
            }
        }
    }

    /**
     * Find the playing sound for the given name.
     * @param {string} name
     * @param {Array<ITrackedHowl>} soundArray
     * @returns {Howl}
     * @private
     */
    private _findPlayingSound( name: string, soundGroupId: sound_group_id ): Howl 
    {
        let result: Howl = null;

        let mapSound: Map<string, Howl> = this._arrSoundCollection[ soundGroupId ];
        if ( mapSound.has( name ) )
        {
            const kFoundHowl: Howl = mapSound.get( name );
            if ( kFoundHowl.playing() )
            {
                result = kFoundHowl;
            }
        }

        return result;
    }

    // #endregion //
}

export const enum sound_group_id
{
    SFX = 0,
    MUSIC,
    AMBIENT
}

export interface IPlayParams
{
    randomStart?: boolean;
    loop?: boolean;
    onComplete?: () => void;
    onCompleteScope?: any;
}

export interface ISoundLoadGroup
{
    basePath: string;
    arrName: Array<string>;
    soundGroupId: sound_group_id;
    arrExtension: Array<string>;
}