import ServiceLocator from "../ServiceLocator";


export default class StyleManager
{
    // #region Attributes

    // private:

    private _mapDependency: Map<string, Array<string>> = new Map<string, Array<string>>();
    private _mapStyle: Map<string, HTMLStyleElement> = new Map<string, HTMLStyleElement>();

    // #endregion


    // #region Methods

    // public:

    /**
    * Adds to the document head the style elements provided avoiding duplicates.
    *
    * @param viewId The id of the view instance that it is adding its styles.
    * @param arrStyleId The link elements that reference the CSS files to add.
    */
    public add( viewId: string, arrStyleId: Array<string> ): void
    {      
        this._mapDependency.set( viewId, new Array<string>() );

        // Get already added styles.
        let arrBaseStyleId = [];
        let keysIt: IterableIterator<string> = this._mapStyle.keys();
        for ( let keysItResult: IteratorResult<string> = keysIt.next(); !keysItResult.done; keysItResult = keysIt.next() )
        {
            arrBaseStyleId.push( keysItResult.value );
        }

        for ( let i = 0; i < arrStyleId.length; ++i )
        {
            let styleIdToAdd: string =  arrStyleId[ i ];

            // Skip global styles.
            if ( styleIdToAdd.indexOf( "css." ) != 0 )
            {
                // Avoid duplicates.
                if ( arrBaseStyleId.indexOf( styleIdToAdd ) == -1 )
                {
                    let styleAux: HTMLStyleElement = document.createElement( "style" );
                    // To ease identifying styles on the browser while debugging.
                    styleAux.setAttribute( "data-id", styleIdToAdd );
                    let cssResource: PIXI.LoaderResource = ServiceLocator.resourceStack.find( styleIdToAdd );
                    if ( !cssResource )
                    {
                        throw new Error( "Cannot find " + styleIdToAdd + " in ServiceLocator.resourceLoader" );
                    }
                    styleAux.innerHTML = cssResource.data;
                    
                    document.head.appendChild( styleAux );

                    this._mapStyle.set( styleIdToAdd, styleAux );
                }
                
                // Add dependency.
                this._mapDependency.get( viewId ).push( styleIdToAdd );
            }
        }   
    }    
        
    /**
    * Removes from the current document head the style elements provided.
    *
    * @param viewId The id of the view instance that it is removing its styles.
    */
    public remove( viewId: string ): void
    {      
        // Get href values to remove.
        let arrStyleIdToRemove: string[] = this._mapDependency.get( viewId );
        for ( let i = arrStyleIdToRemove.length - 1; i >= 0; --i )
        {
            // Skip those with dependencies.
            let hasDependencies: boolean = false;
            let entriesIt: IterableIterator<[string, string[]]> = this._mapDependency.entries();
            for ( let entriesItResult: IteratorResult<[string, string[]]> = entriesIt.next(); !entriesItResult.done; entriesItResult = entriesIt.next() )
            {
                if ( entriesItResult.value[ 0 ] != viewId
                    && entriesItResult.value[ 1 ].indexOf( arrStyleIdToRemove[ i ] ) != -1 )
                {
                    hasDependencies = true;
                    break;
                }
            }

            if ( hasDependencies )
                arrStyleIdToRemove.splice( i, 1 );
        }

        // Check if each style element present in the head section should be removed.
        let keysIt: IterableIterator<string> = this._mapStyle.keys();
        for ( let keysItResult: IteratorResult<string> = keysIt.next(); !keysItResult.done; keysItResult = keysIt.next() )
        {
            if ( arrStyleIdToRemove.indexOf( keysItResult.value ) != -1 )
            {
                this._mapStyle.get( keysItResult.value ).remove();

                // TODO: Will affect iteration?
                this._mapStyle.delete( keysItResult.value );
            }
        }

        // Remove dependency.
        this._mapDependency.delete( viewId );
    }

    // #endregion
}