import { TweenMax, Sine } from "gsap";

import Signal from "../lib/signals/Signal";
import StyleManager from "./StyleManager";
import View from "../view/View";
import ServiceLocator from "../ServiceLocator";


export default class ViewManager
{
    // #region Attributes //

    // private:

    private _arrView: Array<View> = new Array<View>();

    private _styleManager: StyleManager = null;

    // Signals.
    private _onViewAdded: Signal = new Signal();
    private _onViewFadedIn: Signal = new Signal();
    private _onViewRemoved: Signal = new Signal();
    
    // Constants.
    private readonly _kArrLayerSelector: Array<string> = [ "#mainLayer", "#popupLayer", "#hudLayer", "#topmostLayer" ];

    // #endregion //


    // #region Properties //

    public get styleManager(): StyleManager { return this._styleManager; }
    public get viewCount(): number { return this._arrView.length; }

    // Signals.
    public get onViewAdded(): Signal { return this._onViewAdded; }
    public get onViewFadedIn(): Signal { return this._onViewFadedIn; }
    public get onViewRemoved(): Signal { return this._onViewRemoved; }

    // #endregion


    // #region Methods //

    // public:

    public constructor() 
    {
        this._styleManager = new StyleManager();

        jQuery( window ).resize( this.onWindow_Resized.bind( this ) );  
    }

    public add( view: View, layerId: view_layer_id, isPrepend: boolean = false ): void
    {
        this.addHidden( view, layerId, isPrepend);

        view.root.css( "opacity", "1" );
    }

    public remove( view: View ): void
    {
        view.preend();

        // Remove CSS styles.
        this._styleManager.remove( view.id );

        // Remove DOM elements.
        view.root.detach();

        this._arrView.splice( this._arrView.indexOf( view ), 1 );

        this._onViewRemoved.dispatch( view.id );

        view.end();
    }

    public fadeIn( view: View, layerId: view_layer_id, isPrepend: boolean = false ): void
    {
        this.addHidden( view, layerId, isPrepend );

        // Play sfx.
        ServiceLocator.audioManager.playSfx( "window_open" );

        // Fade In.
        TweenMax.to( view.root[ 0 ], 0.2, { css: { opacity: 1 }, ease: Sine.easeOut, onComplete: this.onView_FadedIn, callbackScope: this, onCompleteParams: [ view ] } );
    }

    public fadeOut( view: View ): void
    {
        // Play sfx.
        ServiceLocator.audioManager.playSfx( "window_close" );

        // Fade Out.
        TweenMax.to( view.root[ 0 ], 0.2, { css: { opacity: 0 }, ease: Sine.easeIn, onComplete: this.onView_FadedOut, callbackScope: this, onCompleteParams: [ view ] } );
    }

    public findViewByIndex( viewIndex: number ): View 
    { 
        let result: View = null;

        if ( viewIndex <= this._arrView.length -1 ) 
        { 
            result = this._arrView[ viewIndex ]; 
        }

        return result;
    }

    public findViewByResId( resId: string ): Array<View>
    { 
        let result: Array<View> = new Array<View>();

        for ( let view of this._arrView )
        {
            // The view id has the form [resId]_[autoIncrementedNumber]
            if ( view.id.substr( 0, view.id.lastIndexOf( "_" ) ) == resId )
            {
                result.push( view );
            }
        }

        return result;
    }

    public findViewByAlias( alias: string ): Array<View>
    { 
        let result: Array<View> = new Array<View>();

        for ( let view of this._arrView )
        {
            if ( view.alias == alias )
            {
                result.push( view );
            }
        }

        return result;
    }

    public hasView( view: View ): boolean
    {
        let result: boolean = false;

        for ( let viewAux of this._arrView )
        {
            if ( viewAux == view )
            {
                result = true;
                break;
            }
        }

        return result;
    }

    public findViewCount( layerId: view_layer_id ): number
    {
        return jQuery( this._kArrLayerSelector[ layerId ] ).children().length;
    }

    public clearLayer( viewLayerId: view_layer_id ): void
    {
        let arrTempView: Array<View> = this._arrView.slice();
        const kLayerSelector: string = this._kArrLayerSelector[ viewLayerId ];
        for ( let i: number = arrTempView.length - 1; i >= 0; --i )
        {
            let view: View = arrTempView[ i ];
            if ( view.id && view.root.parent().is( kLayerSelector ) )
            {
                this.fadeOut( arrTempView[ i ] );
            }
        }
    }

    public showLayer( layerId: view_layer_id ): void
    {
        jQuery( this._kArrLayerSelector[ layerId ] ).show();
    }

    public hideLayer( layerId: view_layer_id ): void
    {
        jQuery( this._kArrLayerSelector[ layerId ] ).hide();
    }

    public reinitLocale(): void
    {
        for ( let view of this._arrView )
        {
            view.root.find( "[data-i18n]" ).i18n();
        }
    }

    // private:

    private addHidden( view: View, layerId: view_layer_id, isPrepend: boolean ): void
    {
        this._arrView.push( view );

        view.root.css( "opacity", "0" );

        // Add CSS styles.
        this._styleManager.add( view.id, view.styleIds );

        // Add DOM elements.
        if ( !isPrepend )
        {
            view.root.appendTo( this._kArrLayerSelector[ layerId ] );
        }
        else
        {
            view.root.prependTo( this._kArrLayerSelector[ layerId ] );
        }

        this._onViewAdded.dispatch( view );

        view.postinit();

        view.scale();
    }

    // #endregion //


    // #region Input Callbacks //

    private onWindow_Resized(): void
    {
        for ( let view of this._arrView )
        {
            view.scale();
        }
    }

    // #endregion //


    // #region Other Callbacks //

    private onView_FadedIn( view: View ): void
    {
        this._onViewFadedIn.dispatch( view );
    }

    private onView_FadedOut( view: View ): void
    {
        if ( view.id )
        {
            this.remove( view );
        }
    }

    // #endregion //
}

export const enum view_layer_id
{
    MAIN = 0,
    POPUP = 1,
    HUD = 2,
    TOPMOST = 3
}