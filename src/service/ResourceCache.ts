import ServiceLocator from "../ServiceLocator";
import * as PIXI from "pixi.js";

import { IDBPDatabase, openDB } from "idb";
import Signal from "../lib/signals/Signal";
import { TransformationContext } from "typescript";


export default class ResourceCache 
{
    private _db: any;


    private _onTextureMapsCached: Signal = new Signal();


    public get onTextureMapsCached(): Signal { return this._onTextureMapsCached; }

    
    public constructor() {}

    public async init()
    {
        try 
        {
            this._db = await openDB( "resource_cache", 1, {
                upgrade( db: IDBPDatabase ) 
                {
                    if ( !db.objectStoreNames.contains( "texture_map" ) ) 
                    {
                        db.createObjectStore( "texture_map", { keyPath: "id" } );
                    }
                },
            } );
        } 
        catch ( error ) 
        {
            return false;
        }
    }

    public async getValue( tableName: string, id: any ) 
    {
        const kTx = this._db.transaction( tableName, "readonly" );
        const kStore = kTx.objectStore( tableName );
        const kResult = await kStore.get( id );

        return kResult;
    }

    public async getAllValue( tableName: string ) 
    {
        const kTx = this._db.transaction( tableName, "readonly" );
        const kStore = kTx.objectStore( tableName );
        const kResult = await kStore.getAll();

        return kResult;
    }

    public async putValue( tableName: string, value: object ) 
    {
        const kTx = this._db.transaction( tableName, "readwrite" );
        let store = kTx.objectStore( tableName );
        const kResult = await store.put( value );

        return kResult;
    }

    public async putBulkValue( tableName: string, values: object[] ) 
    {
        const kTx = this._db.transaction( tableName, "readwrite" );
        let store = kTx.objectStore( tableName );
        for ( const kValue of values ) 
        {
            await store.put( kValue );
        }

        return this.getAllValue( tableName );
    }

    public async deleteValue( tableName: string, id: any ) 
    {
        const kTx = this._db.transaction( tableName, "readwrite" );
        let store = kTx.objectStore( tableName );
        const kResult = await store.get( id );
        if ( kResult ) 
        {
            await store.delete( id );
        }

        return kResult ? id : null;
    }

    public async openCursor( tableName: string ) 
    {
        const kTx = this._db.transaction( tableName, "readonly" );
        let store = kTx.objectStore( tableName );

        return store.openCursor();
    }

    public async loadCachedTextureMaps( arrReqTextureMapId: Array<string> )
    {
        await this.openCursor( "texture_map" )
            .then( this.createSpriteSheet.bind( this, arrReqTextureMapId, arrReqTextureMapId.length ) );
    }

        private async createSpriteSheet( arrReqTextureMapId: Array<string>, reqTextureMapCount: number, cursor )
        {
            if ( !cursor ) { return; }

            const kReqTextureMapIndex: number = arrReqTextureMapId.indexOf( cursor.key );
            if ( kReqTextureMapIndex != -1 )
            {
                arrReqTextureMapId.splice( kReqTextureMapIndex, 1 );

                ServiceLocator.loadingScreen.setMessage( jQuery.i18n( "PRELOADING_TEXTURE_MAPS" ).replace( "#0", ( reqTextureMapCount - arrReqTextureMapId.length ).toString() ).replace( "#1", reqTextureMapCount.toString() ) );

                let spritesheet = new PIXI.Spritesheet( PIXI.Texture.from( cursor.value.img ), cursor.value.data );
                spritesheet.parse( function () {
                    let resource: PIXI.LoaderResource = new PIXI.LoaderResource( /*cursor.key, ""*/ ); // o_O?
                    resource.spritesheet = spritesheet;
                    resource.textures = spritesheet.textures;
                    ServiceLocator.resourceStack.set( cursor.key, resource );
                } );
            }

            return cursor.continue().then( this.createSpriteSheet.bind( this, arrReqTextureMapId, reqTextureMapCount ) );
        }

    public async cacheTextureMaps( arrTextureMapId: Array<string>, textureMapCount: number )
    {
        const kTextureMapId: string = arrTextureMapId.shift();
        ServiceLocator.loadingScreen.setMessage( jQuery.i18n( "CACHING_TEXTURE_MAPS" ).replace( "#0", ( textureMapCount - arrTextureMapId.length ).toString() ).replace( "#1", textureMapCount.toString() ) );

        let res: PIXI.LoaderResource = ServiceLocator.resourceStack.find( kTextureMapId );
        //
        let img: HTMLImageElement = res.spritesheet.baseTexture.resource[ "source" ];
        let canvas = document.createElement( "canvas" );
        canvas.width = img.width;
        canvas.height = img.height;
        let ctx: CanvasRenderingContext2D = canvas.getContext( "2d" );
        ctx.drawImage( img, 0, 0 );
        //
        this.putValue( "texture_map", { id: res.name, data: res.data, img: canvas.toDataURL( "image/png" ) } ).then(
            () => { 
                if ( arrTextureMapId.length > 0 )
                {
                    this.cacheTextureMaps( arrTextureMapId, textureMapCount );
                }
                else
                {
                    this._onTextureMapsCached.dispatch();
                }
            } );
    }
}