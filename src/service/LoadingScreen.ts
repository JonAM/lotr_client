export default class LoadingScreen
{
    // #region Attributes //

    // private:

    private _jqLoadingAnimation: JQuery<HTMLElement> = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        this._jqLoadingAnimation = jQuery( "#gbLoadingAnimation" );
    }

    public show(): void
    {
        if ( this._jqLoadingAnimation.css( "display" ) == "none" )
        {
            this._jqLoadingAnimation.fadeIn( "slow" );
        }
    }

    public hide(): void
    {
        if ( this._jqLoadingAnimation.css( "display" ) != "none" )
        {
            this._jqLoadingAnimation.fadeOut( "slow" );
        }
    }

    public isVisible(): boolean
    {
        return this._jqLoadingAnimation.css( "display" ) != "none";
    }

    public setMessage( message: string ): void
    {
        this._jqLoadingAnimation.find( ".message" ).html( message.replace( "\n", "<br>" ) );
    }

    // #endregion //
}