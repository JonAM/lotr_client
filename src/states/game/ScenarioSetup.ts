import { action_scope_type } from "../../service/socket_io/GameSocketIOController";
import { detail_bar_icon_type } from "../../game/component/card/token/CDetailBar";
import { layer_type } from "../../game/component/world/CGameLayerProvider";
import { location_type } from "../../game/component/world/CGameWorld";
import { player_type } from "../../game/component/world/CPlayerArea";
import ServiceLocator from "../../ServiceLocator";
import Session from "../../Session";
import Utils from "../../Utils";
import * as PIXI from "pixi.js";

import { ICard } from "../../game/CardDB";
import CCardView, { deck_side_type } from "../../game/component/ui/viewer/CCardView";
import CardViewerController from "../../game/component/ui/viewer/controller/CardViewerController";
import GameObject from "../../game/GameObject";
import { ICardFilter, ICardSelect, ISetupAction } from "../../game/ScenarioDB";
import CardTokenFactory from "../../game/CardTokenFactory";
import CDeckIndicator from "../../game/component/ui/indicator/CDeckIndicator";
import CDropArea from "../../game/component/input/CDropArea";
import CEngagedActorArea from "../../game/component/world/actor_area/CEngagedActorArea";


export default class ScenarioSetup
{
    private _curCardToken: GameObject = null;
    private _arrCardToken: Array<GameObject> = new Array<GameObject>();


    public constructor() {}

    public apply( arrSetupAction: Array<ISetupAction> ): void
    {
        let setupAction: ISetupAction = arrSetupAction.shift();
        let isSkipAction: boolean = false;
        if ( setupAction.options )
        {
            isSkipAction = Utils.arrayHasItem( setupAction.options, "if_ally" ) && !Session.allyId
                || Utils.arrayHasItem( setupAction.options, "if_campaign" ) && !Session.isCampaignMode;
        }
        if ( !isSkipAction )
        {
            switch ( setupAction.type )
            {
                case "raise_players_starting_threat":
                    {
                        const kThreatPoints: number = setupAction.params[ 0 ];
                        ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( "player_threat_level" ) )
                            .cTokenIndicator.trigger.cTokenCounter.addCount( kThreatPoints, action_scope_type.LOCAL );
                        if ( Session.allyId )
                        {
                            ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( "ally_threat_level" ) )
                                .cTokenIndicator.trigger.cTokenCounter.addCount( kThreatPoints, action_scope_type.LOCAL );
                        }
                        break;
                    }

                case "set_players_threat":
                    {
                        const kThreatPoints: number = setupAction.params[ 0 ];
                        ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( "player_threat_level" ) )
                            .cTokenIndicator.trigger.cTokenCounter.setCount( kThreatPoints, action_scope_type.LOCAL );
                        if ( Session.allyId )
                        {
                            ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( "ally_threat_level" ) )
                                .cTokenIndicator.trigger.cTokenCounter.setCount( kThreatPoints, action_scope_type.LOCAL );
                        }
                        break;
                    }

                case "move_cards":
                    {
                        ServiceLocator.game.isAnimated = false;
                        
                        let deckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( setupAction.params[ 0 ] ) );
                        let arrCard: Array<GameObject> = deckIndicator.cDeckIndicator.cViewer.cCardView.findItems();
                        let arrSelectedCard: Array<GameObject> = this.selectCards( arrCard, setupAction.select );
                        
                        const kTargetDeckLocation: location_type = this.findLocationType( setupAction.params[ 1 ] );
                        let targetDeckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( kTargetDeckLocation );
                        for ( let card of arrSelectedCard )
                        {
                            if ( kTargetDeckLocation == location_type.SET_ASIDE )
                            {
                                card.cCard.isAnimated = false;
                                card.cCard.setFaceUp( action_scope_type.LOCAL );
                                card.cCard.isAnimated = true;
                            }

                            targetDeckIndicator.cDeckIndicator.cViewer.cCardView.go.cDropArea.forceDrop( card, CCardView.findPredefinedDropPosition( deck_side_type.BOTTOM ), action_scope_type.LOCAL );
                        }

                        ServiceLocator.game.isAnimated = true;
                        break;
                    }

                case "set_cards_face_up":
                    {
                        ServiceLocator.game.isAnimated = false;

                        let deckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( setupAction.params[ 0 ] ) );
                        let arrCard: Array<GameObject> = deckIndicator.cDeckIndicator.cViewer.cCardView.findItems();
                        let arrSelectedCard: Array<GameObject> = this.selectCards( arrCard, setupAction.select );
                        for ( let card of arrSelectedCard )
                        {
                            card.cCard.isAnimated = false;
                            card.cCard.setFaceUp( action_scope_type.LOCAL );
                            card.cCard.isAnimated = true;
                        }

                        ServiceLocator.game.isAnimated = true;
                        break;
                    }

                case "set_cards_face_down":
                    {
                        ServiceLocator.game.isAnimated = false;

                        let deckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( setupAction.params[ 0 ] ) );
                        let arrCard: Array<GameObject> = deckIndicator.cDeckIndicator.cViewer.cCardView.findItems();
                        let arrSelectedCard: Array<GameObject> = this.selectCards( arrCard, setupAction.select );
                        for ( let card of arrSelectedCard )
                        {
                            card.cCard.isAnimated = false;
                            card.cCard.setFaceDown( action_scope_type.LOCAL );
                            card.cCard.isAnimated = true;
                        }

                        ServiceLocator.game.isAnimated = true;
                        break;
                    }

                case "set_cards_label":
                    {
                        ServiceLocator.game.isAnimated = false;

                        let deckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( setupAction.params[ 0 ] ) );
                        let arrCard: Array<GameObject> = deckIndicator.cDeckIndicator.cViewer.cCardView.findItems();
                        let arrSelectedCard: Array<GameObject> = this.selectCards( arrCard, setupAction.select );
                        for ( let card of arrSelectedCard )
                        {
                            card.cCard.createLabel( jQuery.i18n( setupAction.params[ 1 ] as string ) )
                        }

                        ServiceLocator.game.isAnimated = true;
                        break;
                    }

                case "set_cards_forced_discard_location":
                    {
                        let deckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( setupAction.params[ 0 ] ) );
                        let arrCard: Array<GameObject> = deckIndicator.cDeckIndicator.cViewer.cCardView.findItems();
                        for ( let card of arrCard )
                        {
                            if ( !ServiceLocator.game.forcedDiscardLocations.has( card.cCard.front.cardId ) )
                            {
                                ServiceLocator.game.forcedDiscardLocations.set( card.cCard.front.cardId, this.findLocationType( setupAction.params[ 1 ] ) );
                            }
                        }
                        break;
                    }

                case "shuffle":
                    {
                        let deckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( setupAction.params[ 0 ] ) );
                        ( deckIndicator.cDeckIndicator.cViewer.controller as CardViewerController ).shuffle( action_scope_type.LOCAL );
                        break;
                    }

                case "randomly_set_aside":
                    {
                        let deckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( setupAction.params[ 0 ] ) );
                        let arrCard: Array<GameObject> = deckIndicator.cDeckIndicator.cViewer.cCardView.findItems();
                        let arrSelectedCard: Array<GameObject> = this.selectCards( arrCard, setupAction.select );
                        let selCardIndex: number = ServiceLocator.prng.randInterval( 0, arrSelectedCard.length - 1 );
                        ServiceLocator.game.cGameWorld.findLocation( location_type.SET_ASIDE ).cDeckIndicator.cViewer.cCardView.go.cDropArea.forceDrop( arrSelectedCard[ selCardIndex ], CCardView.findPredefinedDropPosition( deck_side_type.BOTTOM ), action_scope_type.LOCAL );
                        break;
                    }

                case "play_cards":
                    {
                        let arrCard: Array<GameObject> = ServiceLocator.game.cGameWorld.findLocation( location_type.SAURON_DECK_0 ).cDeckIndicator.cViewer.cCardView.findItems();
                        let arrPlayCardId: Array<string> = setupAction.params[ 0 ].slice();
                        let ctf: CardTokenFactory = new CardTokenFactory();
                        let arrCardToken: Array<GameObject> = new Array<GameObject>();
                        for ( let card of arrCard )
                        {
                            const kPlayCardIdIndex: number = arrPlayCardId.indexOf( card.cCard.front.cardId );
                            if ( kPlayCardIdIndex != -1 )
                            {
                                card.cCard.isAnimated = false;
                                card.cCard.setFaceUp( action_scope_type.LOCAL );
                                card.cCard.isAnimated = true;

                                arrCardToken.push( ctf.create( card ) );
                                
                                ServiceLocator.game.root.cGameLayerProvider.get( layer_type.UNDER_TABLE ).cDropArea.forceDrop( card, null, action_scope_type.LOCAL );
                                card.end();

                                arrPlayCardId.splice( kPlayCardIdIndex, 1 );
                                if ( arrPlayCardId.length == 0 )
                                {
                                    break;
                                }
                            }
                        }
                        let targetLocationType: location_type = location_type.STAGING_AREA;
                        if ( setupAction.params.length >= 2 )
                        {
                            targetLocationType = this.findLocationType( setupAction.params[ 1 ] );
                        }
                        ServiceLocator.game.cGameWorld.findLocation( targetLocationType ).cActorArea.appendActors( arrCardToken );
                        break;
                    }

                case "discard_until_player_count":
                    {
                        let playerCount: number = 1;
                        if ( Session.allyId )
                        {
                            playerCount = 2;
                        }

                        let conditionCount: number = 0;
                        let cDeckIndicator: CDeckIndicator = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( setupAction.params[ 0 ] ) ).cDeckIndicator;
                        let arrCard: Array<GameObject> = cDeckIndicator.cViewer.cCardView.findItems();
                        let cardIndex: number = 0;
                        while ( conditionCount != playerCount && cardIndex < arrCard.length )
                        {
                            let card: GameObject = arrCard[ cardIndex ];
                            ServiceLocator.game.cGameWorld.findLocation( location_type.SAURON_DISCARD ).cDeckIndicator.cViewer.cCardView.go.cDropArea.forceDrop( card, CCardView.findPredefinedDropPosition( deck_side_type.TOP ), action_scope_type.LOCAL );

                            for ( let cardFilter of setupAction.params[ 1 ] )
                            {
                                if ( this.applyCardFilter( card.cCard.front.cardInfo, cardFilter ) )
                                {
                                    conditionCount += 1;
                                    break;
                                }
                            }

                            cardIndex += 1;
                        }
                        break;
                    }

                case "discard_until_deck_size":
                    {
                        let cDeckIndicator: CDeckIndicator = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( setupAction.params[ 0 ] ) ).cDeckIndicator;
                        let arrCard: Array<GameObject> = cDeckIndicator.cViewer.cCardView.findItems();
                        const kDeckSizeVariation: number = arrCard.length - setupAction.params[ 1 ];
                        let cardIndex: number = 0;
                        while ( cardIndex < kDeckSizeVariation && cardIndex < arrCard.length )
                        {
                            Utils.game.discard( arrCard[ cardIndex ], action_scope_type.LOCAL );
                            
                            cardIndex += 1;
                        }
                        break;
                    }

                case "remove_burdens":
                    {
                        ServiceLocator.game.isAnimated = false;

                        let cEncounterDeckIndicator: CDeckIndicator = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( "encounter_deck" ) ).cDeckIndicator;
                        let cSetAsideIndicator: CDeckIndicator = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( "set_aside" ) ).cDeckIndicator;
                        let cRemovedFromGameIndicator: CDeckIndicator = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( "removed_from_game" ) ).cDeckIndicator;
                        let arrCard: Array<GameObject> = cEncounterDeckIndicator.cViewer.cCardView.findItems();
                        arrCard = arrCard.concat( cSetAsideIndicator.cViewer.cCardView.findItems() );
                        
                        for ( let card of arrCard )
                        {
                            if ( card.cCard.front.cardInfo.is_burden && setupAction.params.indexOf( card.cCard.front.cardInfo.encounter_set ) != -1 )
                            {
                                cRemovedFromGameIndicator.cViewer.cCardView.go.cDropArea.forceDrop( card, CCardView.findPredefinedDropPosition( deck_side_type.TOP ), action_scope_type.LOCAL );
                            }
                        }

                        ServiceLocator.game.isAnimated = true;
                        break;
                    }

                case "ct_create":
                    {
                        if ( this._curCardToken ) 
                        { 
                            this._arrCardToken.push( this._curCardToken ); 
                            this._curCardToken = null;
                        }

                        let arrCard: Array<GameObject> = ServiceLocator.game.cGameWorld.findLocation( location_type.SAURON_DECK_0 ).cDeckIndicator.cViewer.cCardView.findItems();
                        arrCard = arrCard.concat( ServiceLocator.game.cGameWorld.findLocation( location_type.SET_ASIDE ).cDeckIndicator.cViewer.cCardView.findItems() );
                        arrCard = arrCard.concat( ServiceLocator.game.cGameWorld.findLocation( location_type.QUEST_DECK ).cDeckIndicator.cViewer.cCardView.findItems() );
                        const kCreateCardId: string = setupAction.params[ 0 ];
                        for ( let card of arrCard )
                        {
                            if ( card.cCard.front.cardId == kCreateCardId )
                            {
                                card.cCard.isAnimated = false;
                                card.cCard.setFaceUp( action_scope_type.LOCAL );
                                card.cCard.isAnimated = true;

                                let ctf: CardTokenFactory = new CardTokenFactory();
                                this._curCardToken = ctf.create( card );
                                
                                ServiceLocator.game.root.cGameLayerProvider.get( layer_type.UNDER_TABLE ).cDropArea.forceDrop( card, null, action_scope_type.LOCAL );
                                card.end();

                                break;
                            }
                        }
                        break;
                    }

                case "ct_randomly_create":
                    {
                        if ( this._curCardToken ) 
                        { 
                            this._arrCardToken.push( this._curCardToken ); 
                            this._curCardToken = null;
                        }

                        let deckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( setupAction.params[ 0 ] ) );
                        let arrCard: Array<GameObject> = deckIndicator.cDeckIndicator.cViewer.cCardView.findItems();
                        let arrSelectedCard: Array<GameObject> = this.selectCards( arrCard, setupAction.select );
                        
                        const kSelCardIndex: number = ServiceLocator.prng.randInterval( 0, arrSelectedCard.length - 1 );
                        let card: GameObject = arrSelectedCard[ kSelCardIndex ]

                        card.cCard.isAnimated = false;
                        card.cCard.setFaceUp( action_scope_type.LOCAL );
                        card.cCard.isAnimated = true;

                        let ctf: CardTokenFactory = new CardTokenFactory();
                        this._curCardToken = ctf.create( card );
                        
                        ServiceLocator.game.root.cGameLayerProvider.get( layer_type.UNDER_TABLE ).cDropArea.forceDrop( card, null, action_scope_type.LOCAL );
                        card.end();
                        break;
                    }

                case "ct_create_from_selection":
                    {
                        if ( this._curCardToken ) 
                        { 
                            this._arrCardToken.push( this._curCardToken ); 
                            this._curCardToken = null;
                        }

                        let deckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( setupAction.params[ 0 ] ) );
                        let arrCard: Array<GameObject> = deckIndicator.cDeckIndicator.cViewer.cCardView.findItems();
                        let arrSelectedCard: Array<GameObject> = this.selectCards( arrCard, setupAction.select );

                        for ( let card of arrSelectedCard )
                        {
                            card.cCard.isAnimated = false;
                            card.cCard.setFaceUp( action_scope_type.LOCAL );
                            card.cCard.isAnimated = true;

                            let ctf: CardTokenFactory = new CardTokenFactory();
                            this._arrCardToken.push( ctf.create( card ) );
                            
                            ServiceLocator.game.root.cGameLayerProvider.get( layer_type.UNDER_TABLE ).cDropArea.forceDrop( card, null, action_scope_type.LOCAL );
                            card.end();
                        }
                        break;
                    }

                case "ct_discard_and_create":
                    {
                        let cDeckIndicator: CDeckIndicator = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( setupAction.params[ 0 ] ) ).cDeckIndicator;
                        let arrSelectedCard: Array<GameObject> = new Array<GameObject>();
                        let arrCard: Array<GameObject> = cDeckIndicator.cViewer.cCardView.findItems();
                        let targetSelCardCount: number = null;
                        if ( setupAction.params[ 2 ] == "player_count-1" )
                        {
                            targetSelCardCount = Session.allyId ? 1 : 0;
                        }
                        else if ( setupAction.params[ 2 ] == "player_count" )
                        {
                            targetSelCardCount = Session.allyId ? 2 : 1;
                        }
                        else
                        {
                            targetSelCardCount = setupAction.params[ 2 ];
                        }
                        let cardIndex: number = 0;
                        while ( arrSelectedCard.length < targetSelCardCount && cardIndex < arrCard.length )
                        {
                            let isTargetCard: boolean = false;

                            let card: GameObject = arrCard[ cardIndex ];
                            for ( let cardFilter of setupAction.params[ 1 ] )
                            {
                                if ( this.applyCardFilter( card.cCard.front.cardInfo, cardFilter ) )
                                {
                                    isTargetCard = true;
                                    arrSelectedCard.push( card );
                                    break;
                                }
                            }

                            if ( !isTargetCard )
                            {
                                Utils.game.discard( card, action_scope_type.LOCAL );
                            }
                            else
                            {
                                ServiceLocator.game.root.cGameLayerProvider.get( layer_type.UNDER_TABLE ).cDropArea.forceDrop( card, null, action_scope_type.LOCAL );
                            }

                            cardIndex += 1;
                        }

                        for ( let card of arrSelectedCard )
                        {
                            if ( this._curCardToken ) 
                            { 
                                this._arrCardToken.push( this._curCardToken ); 
                                this._curCardToken = null;
                            }

                            card.cCard.isAnimated = false;
                            card.cCard.setFaceUp( action_scope_type.LOCAL );
                            card.cCard.isAnimated = true;

                            let ctf: CardTokenFactory = new CardTokenFactory();
                            this._curCardToken = ctf.create( card );
                            
                            card.end();
                        }
                        break;
                    }

                case "ct_set_face_down":
                    {
                        this._curCardToken.cCardToken.isAnimated = false;
                        this._curCardToken.cCardToken.setFaceDown( action_scope_type.LOCAL );
                        this._curCardToken.cCardToken.isAnimated = true;
                        break;
                    }

                case "ct_set_health":
                    {
                        if ( this._curCardToken.cCardToken.curSide.cEnemySide )
                        {
                            this._curCardToken.cCardToken.curSide.cEnemySide.health.cTokenCounter.setCount( setupAction.params[ 0 ], action_scope_type.LOCAL );
                        }
                        else if ( this._curCardToken.cCardToken.curSide.cAllySide )
                        {
                            this._curCardToken.cCardToken.curSide.cAllySide.health.cTokenCounter.setCount( setupAction.params[ 0 ], action_scope_type.LOCAL );
                        }
                        else if ( this._curCardToken.cCardToken.curSide.cHeroSide )
                        {
                            this._curCardToken.cCardToken.curSide.cHeroSide.health.cTokenCounter.setCount( setupAction.params[ 0 ], action_scope_type.LOCAL );
                        }
                        break;
                    }

                case "ct_add_wound_tokens":
                    {
                        if ( this._curCardToken.cCardToken.curSide.cEnemySide )
                        {
                            this._curCardToken.cCardToken.curSide.cEnemySide.wound.cTokenCounter.addCount( setupAction.params[ 0 ], action_scope_type.LOCAL );
                        }
                        else if ( this._curCardToken.cCardToken.curSide.cAllySide )
                        {
                            this._curCardToken.cCardToken.curSide.cAllySide.wound.cTokenCounter.addCount( setupAction.params[ 0 ], action_scope_type.LOCAL );
                        }
                        else if ( this._curCardToken.cCardToken.curSide.cHeroSide )
                        {
                            this._curCardToken.cCardToken.curSide.cHeroSide.wound.cTokenCounter.addCount( setupAction.params[ 0 ], action_scope_type.LOCAL );
                        }
                        break;
                    }

                case "ct_add_progress_tokens":
                    {
                        if ( this._curCardToken.cCardToken.curSide.cLocationSide )
                        {
                            this._curCardToken.cCardToken.curSide.cLocationSide.progress.cTokenCounter.addCount( setupAction.params[ 0 ], action_scope_type.LOCAL );
                        }
                        else if ( this._curCardToken.cCardToken.curSide.cSideQuestSide )
                        {
                            this._curCardToken.cCardToken.curSide.cSideQuestSide.progress.cTokenCounter.addCount( setupAction.params[ 0 ], action_scope_type.LOCAL );
                        }
                        else if ( this._curCardToken.cCardToken.curSide.cQuestSide )
                        {
                            this._curCardToken.cCardToken.curSide.cQuestSide.progress.cTokenCounter.addCount( setupAction.params[ 0 ], action_scope_type.LOCAL );
                        }
                        break;
                    }

                case "ct_play":
                    {
                        if ( this._curCardToken ) 
                        { 
                            this._arrCardToken.push( this._curCardToken ); 
                            this._curCardToken = null;
                        }

                        let playLocationType: location_type = location_type.STAGING_AREA;
                        if ( setupAction.params && setupAction.params.length >= 1 )
                        {
                            playLocationType = this.findLocationType( setupAction.params[ 0 ] );
                        }
                        ServiceLocator.game.cGameWorld.findLocation( playLocationType ).cActorArea.appendActors( this._arrCardToken );
                        
                        this._arrCardToken.splice( 0 );

                        break;
                    }

                case "ct_set_active_location":
                    {
                        ServiceLocator.game.cGameWorld.cSauronArea.activeLocationHolder.cDropArea.forceDrop( this._curCardToken, null, action_scope_type.LOCAL );
                        this._curCardToken = null;
                        break;
                    }

                case "ct_set_quest":
                    {
                        let playLocationType: location_type = location_type.QUEST;
                        if ( setupAction.params && setupAction.params.length >= 1 )
                        {
                            playLocationType = this.findLocationType( setupAction.params[ 0 ] );
                        }
                        ServiceLocator.game.cGameWorld.findLocation( playLocationType ).cDropArea.forceDrop( this._curCardToken, null, action_scope_type.LOCAL );
                        this._curCardToken = null;
                        break;
                    }

                case "ct_push_quest":
                    {
                        if ( this._curCardToken ) 
                        { 
                            this._arrCardToken.push( this._curCardToken ); 
                            this._curCardToken = null;
                        }
                        this._curCardToken = ServiceLocator.game.cGameWorld.cSauronArea.cQuestHolder.questToken;
                        break;
                    }

                case "ct_pop_quest":
                    {
                        if ( this._arrCardToken.length > 0 ) 
                        { 
                            this._curCardToken = this._arrCardToken.pop();
                        }
                        else
                        {
                            this._curCardToken = null;
                        }
                        break;
                    }

                case "ct_add_custom_counter":
                    {
                        this._curCardToken.cCardToken.cDetailBar.forceCustomCounterCreation( 
                            setupAction.params[ 0 ], jQuery.i18n( setupAction.params[ 1 ] as string ), setupAction.params[ 2 ] );
                        break;
                    }

                case "ct_add_to_custom_counter":
                    {
                        this._curCardToken.cCardToken.cDetailBar.addToItemCount( detail_bar_icon_type.CUSTOM_COUNTER_0, setupAction.params[ 0 ] );
                        break;
                    }

                case "ct_attach":
                    {
                        if ( !setupAction.params )
                        {
                            // Attach _curCardToken to the top of _arrCardToken.`
                            let topCardToken: GameObject = this._arrCardToken.pop();
                            topCardToken.cCardToken.isAnimated = false;
                            topCardToken.cDropArea.forceDrop( this._curCardToken, null, action_scope_type.LOCAL );
                            topCardToken.cCardToken.isAnimated = true;

                            this._curCardToken = topCardToken;
                        }
                        else
                        {
                            let arrCard: Array<GameObject> = ServiceLocator.game.cGameWorld.findLocation( location_type.SAURON_DECK_0 ).cDeckIndicator.cViewer.cCardView.findItems();
                            arrCard = arrCard.concat( ServiceLocator.game.cGameWorld.findLocation( location_type.SET_ASIDE ).cDeckIndicator.cViewer.cCardView.findItems() );
                            const kAttachCardId: string = setupAction.params[ 0 ];
                            for ( let card of arrCard )
                            {
                                if ( card.cCard.front.cardId == kAttachCardId )
                                {
                                    this._curCardToken.cCardToken.isAnimated = false;
                                    this._curCardToken.cDropArea.forceDrop( card, new PIXI.Point(), action_scope_type.LOCAL );
                                    this._curCardToken.cCardToken.isAnimated = true;                     
                                    break;
                                }
                            }
                        }
                        break;
                    }

                case "ct_put_underneath":
                    {
                        if ( !setupAction.params )
                        {
                            // Put _curCardToken underneath of the top of _arrCardToken.`
                            let topCardToken: GameObject = this._arrCardToken.pop();
                            topCardToken.cCardToken.isAnimated = false;
                            topCardToken.cCardToken.addCardUnderneath( this._curCardToken, action_scope_type.LOCAL );
                            topCardToken.cCardToken.isAnimated = true;

                            this._curCardToken = topCardToken;
                        }
                        else
                        {
                            let arrCard: Array<GameObject> = ServiceLocator.game.cGameWorld.findLocation( location_type.SAURON_DECK_0 ).cDeckIndicator.cViewer.cCardView.findItems();
                            arrCard = arrCard.concat( ServiceLocator.game.cGameWorld.findLocation( location_type.SET_ASIDE ).cDeckIndicator.cViewer.cCardView.findItems() );
                            const kPutUnderneathCardId: string = setupAction.params[ 0 ];
                            for ( let card of arrCard )
                            {
                                if ( card.cCard.front.cardId == kPutUnderneathCardId )
                                {
                                    this._curCardToken.cCardToken.isAnimated = false;
                                    this._curCardToken.cCardToken.addCardUnderneath( card, action_scope_type.LOCAL );
                                    this._curCardToken.cCardToken.isAnimated = true;                     
                                    break;
                                }
                            }
                        }
                        break;
                    }

                case "ct_put_selection_underneath":
                    {
                        ServiceLocator.game.isAnimated = false;

                        let deckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( setupAction.params[ 0 ] ) );
                        let arrCard: Array<GameObject> = deckIndicator.cDeckIndicator.cViewer.cCardView.findItems();
                        let arrSelectedCard: Array<GameObject> = this.selectCards( arrCard, setupAction.select );
                        for ( let card of arrSelectedCard )
                        {
                            this._curCardToken.cCardToken.addCardUnderneath( card, action_scope_type.LOCAL );
                        }

                        ServiceLocator.game.isAnimated = true;
                        break;
                    }

                case "set_game_modifiers_face_down":
                    {
                        ServiceLocator.game.isAnimated = false;

                        let actorArea: GameObject = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( setupAction.params[ 0 ] ) );
                        let arrGameModifier: Array<GameObject> = actorArea.cActorArea.findGameModifiers();
                        arrGameModifier = this.selectCards( arrGameModifier, setupAction.select );
                        for ( let gameModifier of arrGameModifier )
                        {
                            gameModifier.cGameModifier.isAnimated = false;
                            gameModifier.cGameModifier.setFaceDown( action_scope_type.LOCAL );
                            gameModifier.cGameModifier.isAnimated = true;
                        }

                        ServiceLocator.game.isAnimated = true;
                        break;
                    }

                case "set_heroes_resource_count":
                    {
                        let arrActor: Array<GameObject> = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( "player_home" ) ).cActorArea.findAllActors();
                        if ( Session.allyId )
                        {
                            arrActor = arrActor.concat( ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( "ally_home" ) ).cActorArea.findAllActors() );
                        }
                        for ( let actor of arrActor )
                        {
                            if ( actor.cCardToken.curSide.cHeroSide )
                            {
                                actor.cCardToken.curSide.cHeroSide.resource.cTokenCounter.setCount( setupAction.params[ 0 ], action_scope_type.LOCAL );
                            }
                        }
                        break;
                    }

                case "start_race": { ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "circuit" ).cCircuit.startRace(); break; }
                case "create_island_map": { ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "island" ).cIslandMap.generate(); break; }
                case "create_dor-cuarthol_area": { ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "dor-cuarthol" ).cDorCuartholArea.generate(); break; }
                
                case "reanimate_top_card":
                    {
                        let arrCard: Array<GameObject> = [ ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( "player_deck" ) )
                            .cDeckIndicator.cViewer.cCardView.findItems()[ 0 ] ];
                        if ( Session.allyId )
                        {
                            arrCard.push( ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( "ally_deck" ) )
                                .cDeckIndicator.cViewer.cCardView.findItems()[ 0 ] )
                        }
                        for ( let card of arrCard )
                        {
                            let ctf: CardTokenFactory = new CardTokenFactory();
                            let enemyToken: GameObject = ctf.createGenericEnemy( CEngagedActorArea.forceEnemyTokenOidGeneration(), player_type.PLAYER, location_type.STAGING_AREA );
                            enemyToken.cCardToken.curSide.cEnemySide.threat.cTokenCounter.setCount( 2, action_scope_type.LOCAL );
                            enemyToken.cCardToken.curSide.cEnemySide.attack.cTokenCounter.setCount( 2, action_scope_type.LOCAL );
                            enemyToken.cCardToken.curSide.cEnemySide.defense.cTokenCounter.setCount( 2, action_scope_type.LOCAL );
                            enemyToken.cCardToken.curSide.cEnemySide.health.cTokenCounter.setCount( 2, action_scope_type.LOCAL );
                            enemyToken.cCardToken.cDetailBar.addItem( detail_bar_icon_type.UNDERNEATH, null );
                            enemyToken.cCardToken.cDetailBar.findItem( detail_bar_icon_type.UNDERNEATH ).root.cDropArea.forceDrop( card, null, action_scope_type.LOCAL );
                            ServiceLocator.game.cGameWorld.findLocation( location_type.STAGING_AREA ).cDropArea.forceDrop( enemyToken, new PIXI.Point( CDropArea.kPredefinedDropPositionCode ), action_scope_type.LOCAL );
                        }
                        break;
                    }

                case "hide_mugash": 
                    { 
                        ServiceLocator.game.isAnimated = false;

                        let arrMugash: Array<GameObject> = new Array<GameObject>();
                        let deckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( location_type.SET_ASIDE );
                        let arrCard: Array<GameObject> = deckIndicator.cDeckIndicator.cViewer.cCardView.findItems();
                        for ( let card of arrCard )
                        {
                            if ( this.applyCardFilter( card.cCard.front.cardInfo, { codes: [ "007024" ] } ) )
                            {
                                arrMugash.push( card );
                                break;
                            }
                        }
                        if ( Session.allyId )
                        {
                            for ( let card of arrCard )
                            {
                                if ( this.applyCardFilter( card.cCard.front.cardInfo, { codes: [ "007025" ] } ) )
                                {
                                    arrMugash.push( card );
                                    break;
                                }
                            }
                        }

                        Utils.game.shuffle( arrMugash );

                        let targetDeckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( "player_custom_deck" ) );
                        targetDeckIndicator.cDeckIndicator.cViewer.cCardView.go.cDropArea.forceDrop( arrMugash.pop(), CCardView.findPredefinedDropPosition( deck_side_type.BOTTOM ), action_scope_type.LOCAL );
                        ( targetDeckIndicator.cDeckIndicator.cViewer.controller as CardViewerController ).shuffle( action_scope_type.LOCAL );

                        if ( arrMugash.length > 0 )
                        {
                            let targetDeckIndicator: GameObject = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( "ally_custom_deck" ) );
                            targetDeckIndicator.cDeckIndicator.cViewer.cCardView.go.cDropArea.forceDrop( arrMugash.pop(), CCardView.findPredefinedDropPosition( deck_side_type.BOTTOM ), action_scope_type.LOCAL );
                            ( targetDeckIndicator.cDeckIndicator.cViewer.controller as CardViewerController ).shuffle( action_scope_type.LOCAL );
                        }

                        ServiceLocator.game.isAnimated = true;
                        break;
                    }

                case "setup_tfon_staging_area":
                    {
                        console.assert( this._arrCardToken.length == 0, "ScenarioSetup.ts :: apply() :: Cannot process \"setup_tfon_staging_area\" if this._arrCardToken.length > 0" );

                        // Create Thane Ulchor.
                        let arrCard: Array<GameObject> = ServiceLocator.game.cGameWorld.findLocation( location_type.SAURON_DECK_0 ).cDeckIndicator.cViewer.cCardView.findItems();
                        let thaneUlchorCard: GameObject = this.selectCards( arrCard, { filters: [ { codes: [ "022148" ] } ], max_count: 1 } )[ 0 ];
                        //
                        thaneUlchorCard.cCard.isAnimated = false;
                        thaneUlchorCard.cCard.setFaceUp( action_scope_type.LOCAL );
                        thaneUlchorCard.cCard.isAnimated = true;
                        //
                        let ctf: CardTokenFactory = new CardTokenFactory();
                        this._arrCardToken.push( ctf.create( thaneUlchorCard ) );
                        //      
                        ServiceLocator.game.root.cGameLayerProvider.get( layer_type.UNDER_TABLE ).cDropArea.forceDrop( thaneUlchorCard, null, action_scope_type.LOCAL );
                        thaneUlchorCard.end();

                        // Create Storm the Castle side-quests.
                        let arrStormCastle: Array<GameObject> = new Array<GameObject>();
                        arrCard = ServiceLocator.game.cGameWorld.findLocation( location_type.SET_ASIDE ).cDeckIndicator.cViewer.cCardView.findItems();
                        let arrStormCastleCard: Array<GameObject> = this.selectCards( arrCard, { filters: [ { type_codes: [ "encounter-side-quest" ], traits: [ "castle" ] } ] } );
                        for ( let card of arrStormCastleCard )
                        {
                            card.cCard.isAnimated = false;
                            card.cCard.setFaceUp( action_scope_type.LOCAL );
                            
                            let ctf: CardTokenFactory = new CardTokenFactory();
                            let stormCastle: GameObject = ctf.create( card );
                            arrStormCastle.push( stormCastle );
                            this._arrCardToken.push( stormCastle );

                            // Power of Mordor deck top card.
                            let topCard: GameObject = ServiceLocator.game.cGameWorld.findLocation( location_type.SAURON_CUSTOM_DECK_0 ).cDeckIndicator.cViewer.cCardView.findItems()[ 0 ];
                            topCard.cCard.isAnimated = false;
                            stormCastle.cCardToken.addCardUnderneath( topCard, action_scope_type.LOCAL );
                            topCard.cCard.isAnimated = true;

                            ServiceLocator.game.root.cGameLayerProvider.get( layer_type.UNDER_TABLE ).cDropArea.forceDrop( card, null, action_scope_type.LOCAL );
                            card.end();
                        }

                        // Place player deck top 8 cards under each Storm the Castle side-quest.
                        for ( let stormCastle of arrStormCastle )
                        {
                            for ( let i: number = 0; i < 8; ++i )
                            {
                                let topCard: GameObject = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( "player_deck" ) ).cDeckIndicator.cViewer.cCardView.findItems()[ 0 ];
                                topCard.cCard.isAnimated = false;
                                stormCastle.cCardToken.addCardUnderneath( topCard, action_scope_type.LOCAL );
                                topCard.cCard.isAnimated = true;
                            }
                        }
                        
                        // Place player deck top 8 cards under the quest.
                        let questToken: GameObject = ServiceLocator.game.cGameWorld.cSauronArea.cQuestHolder.questToken;
                        for ( let i: number = 0; i < 8; ++i )
                        {
                            let topCard: GameObject = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( "player_deck" ) ).cDeckIndicator.cViewer.cCardView.findItems()[ 0 ];
                            topCard.cCard.isAnimated = false;
                            questToken.cCardToken.addCardUnderneath( topCard, action_scope_type.LOCAL );
                            topCard.cCard.isAnimated = true;
                        }

                        // Repeat for ally.
                        if ( Session.allyId )
                        {
                            // Storm the Castle.
                            for ( let stormCastle of arrStormCastle )
                            {
                                for ( let i: number = 0; i < 8; ++i )
                                {
                                    let topCard: GameObject = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( "ally_deck" ) ).cDeckIndicator.cViewer.cCardView.findItems()[ 0 ];
                                    topCard.cCard.isAnimated = false;
                                    stormCastle.cCardToken.addCardUnderneath( topCard, action_scope_type.LOCAL );
                                    topCard.cCard.isAnimated = true;
                                }
                            }

                            // Quest.
                            for ( let i: number = 0; i < 8; ++i )
                            {
                                let topCard: GameObject = ServiceLocator.game.cGameWorld.findLocation( this.findLocationType( "ally_deck" ) ).cDeckIndicator.cViewer.cCardView.findItems()[ 0 ];
                                topCard.cCard.isAnimated = false;
                                questToken.cCardToken.addCardUnderneath( topCard, action_scope_type.LOCAL );
                                topCard.cCard.isAnimated = true;
                            }
                        }

                        ServiceLocator.game.cGameWorld.findLocation( location_type.STAGING_AREA ).cActorArea.appendActors( this._arrCardToken );
                        this._arrCardToken.splice( 0 );

                        break;
                    }
            }
        }

        if ( arrSetupAction.length > 0 )
        {
            window.setTimeout( this.apply.bind( this, arrSetupAction ) );
        }
    }

        private findLocationType( deckId: string ): location_type
        {
            let result: location_type = null;

            switch ( deckId )
            {
                case "encounter_deck": { result = location_type.SAURON_DECK_0; break; }
                case "encounter_discard": { result = location_type.SAURON_DISCARD; break; }
                case "secondary_encounter_deck": { result = location_type.SAURON_DECK_1; break; }
                case "player_threat_level": { result = ServiceLocator.game.startingPlayer == player_type.PLAYER ? location_type.MY_THREAT_LEVEL : location_type.ALLY_THREAT_LEVEL; break; }
                case "ally_threat_level": { result = ServiceLocator.game.startingPlayer == player_type.PLAYER ? location_type.ALLY_THREAT_LEVEL : location_type.MY_THREAT_LEVEL; break; }
                case "player_deck": { result = ServiceLocator.game.startingPlayer == player_type.PLAYER ? location_type.MY_DECK : location_type.ALLY_DECK; break; }
                case "ally_deck": { result = ServiceLocator.game.startingPlayer == player_type.PLAYER ? location_type.ALLY_DECK : location_type.MY_DECK; break; }
                case "player_hand": { result = ServiceLocator.game.startingPlayer == player_type.PLAYER ? location_type.MY_HAND : location_type.ALLY_HAND; break; }
                case "ally_hand": { result = ServiceLocator.game.startingPlayer == player_type.PLAYER ? location_type.ALLY_HAND : location_type.MY_HAND; break; }
                case "player_custom_deck": { result = ServiceLocator.game.startingPlayer == player_type.PLAYER ? location_type.MY_CUSTOM_DECK : location_type.ALLY_CUSTOM_DECK; break; }
                case "ally_custom_deck": { result = ServiceLocator.game.startingPlayer == player_type.PLAYER ? location_type.ALLY_CUSTOM_DECK : location_type.MY_CUSTOM_DECK; break; }
                case "quest_deck": { result = location_type.QUEST_DECK; break; }
                case "quest_discard": { result = location_type.QUEST_DISCARD; break; }
                case "set_aside": { result = location_type.SET_ASIDE; break; }
                case "removed_from_game": { result = location_type.REMOVED_FROM_GAME; break; }
                case "sauron_custom_deck": { result = location_type.SAURON_CUSTOM_DECK_0; break; }
                case "sauron_custom_discard": { result = location_type.SAURON_CUSTOM_DISCARD_0; break; }
                case "secondary_sauron_custom_deck": { result = location_type.SAURON_CUSTOM_DECK_1; break; }
                case "secondary_sauron_custom_discard": { result = location_type.SAURON_CUSTOM_DISCARD_1; break; }
                
                case "player_home": { result = location_type.MY_HOME; break; }
                case "ally_home": { result = location_type.ALLY_HOME; break; }
                case "first_player_home": { result = ServiceLocator.game.firstPlayer == player_type.PLAYER ? location_type.MY_HOME : location_type.ALLY_HOME; break; }
                case "second_player_home": { result = ServiceLocator.game.firstPlayer == player_type.PLAYER ? location_type.ALLY_HOME : location_type.MY_HOME; break; }
                case "first_player_engaged": { result = ServiceLocator.game.firstPlayer == player_type.PLAYER ? location_type.MY_ENGAGED : location_type.ALLY_ENGAGED; break; }
                case "second_player_engaged": { result = ServiceLocator.game.firstPlayer == player_type.PLAYER ? location_type.ALLY_ENGAGED : location_type.MY_ENGAGED; break; }
                case "pursuit_staging_area": { result = location_type.PURSUIT_STAGING_AREA; break; }
                case "pursuit_quest": { result = location_type.PURSUIT_QUEST; break; }
                case "riddle_area": { result = location_type.RIDDLE_ACTOR_AREA; break; }
                case "riddle_quest": { result = location_type.RIDDLE_QUEST; break; }
                case "pit_area": { result = location_type.PIT_ACTOR_AREA; break; }
                case "pit_quest": { result = location_type.PIT_QUEST; break; }
                case "staging_area": { result = location_type.STAGING_AREA; break; }
            }

            return result;
        }

        private selectCards( arrGameObject: Array<GameObject>, cardSelect: ICardSelect ): Array<GameObject>
        {
            let result: Array<GameObject> = new Array<GameObject>();

            if ( !cardSelect )
            {
                result = result.concat( arrGameObject );
            }
            else
            {
                let maxCardCount: number = Number.MAX_SAFE_INTEGER;
                if ( cardSelect.max_count )
                {
                    if ( cardSelect.max_count == "1_per_player" )
                    {
                        maxCardCount = Session.allyId ? 2 : 1;
                    }
                    else 
                    {
                        maxCardCount =  cardSelect.max_count;
                    }
                }
                if ( cardSelect.filters )
                {
                    for ( let go of arrGameObject )
                    {
                        for ( let cardFilter of cardSelect.filters )
                        {
                            let cardInfo: ICard = null;
                            if ( go.cCard )
                            {
                                cardInfo = go.cCard.front.cardInfo;
                            }
                            else if ( go.cGameModifier )
                            {
                                cardInfo = go.cGameModifier.front.cardInfo;
                            }
                            if ( this.applyCardFilter( cardInfo, cardFilter ) )
                            {
                                result.push( go );
                                break;
                            }
                        }

                        if ( result.length >= maxCardCount )
                        {
                            break;
                        }
                    }
                }
            }

            return result;
        }

        private applyCardFilter( cardInfo: ICard, filter: ICardFilter ): boolean
        {
            let result: boolean = true;

            if ( filter.encounter_sets != undefined )
            {
                result = false;
                for ( let encounterSet of filter.encounter_sets )
                {
                    if ( cardInfo.encounter_set == encounterSet )
                    {
                        result = true;
                        break;
                    }
                }
            }

            if ( result && filter.type_codes != undefined )
            {
                result = false;
                for ( let typeCode of filter.type_codes )
                {
                    if ( cardInfo.type_code == typeCode )
                    {
                        result = true;
                        break;
                    }
                }
            }

            if ( result && filter.traits != undefined )
            {
                result = false;
                for ( let trait of filter.traits )
                {
                    if ( cardInfo.traits.toLowerCase().indexOf( trait + "." ) != -1 )
                    {
                        result = true;
                        break;
                    }
                }
            }

            if ( result && filter.codes != undefined )
            {
                result = false;
                for ( let code of filter.codes )
                {
                    if ( cardInfo.code == code )
                    {
                        result = true;
                        break;
                    }
                }
            }

            if ( result && filter.is_unique != undefined )
            {
                result = cardInfo.is_unique == filter.is_unique;
            }

            if ( result && filter.is_burden != undefined )
            {
                result = cardInfo.is_burden != undefined && cardInfo.is_burden == filter.is_burden;
            }

            return result;
        }
}