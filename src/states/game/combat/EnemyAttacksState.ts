import State from "../../../lib/state_machine/State";

import { combat_state_id, game_state_id, quest_state_id } from "../../StateId";
import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";

import { IOpponentAction, IOppActionListener } from "../../../game/AllyActionManager";
import { player_type } from "../../../game/component/world/CPlayerArea";
import GameObject from "../../../game/GameObject";
import Signal from "../../../lib/signals/Signal";
import Session from "../../../Session";
import CEndTurnButton, { end_turn_btn_state } from "../../../game/component/ui/CEndTurnButton";
import { detail_bar_icon_type } from "../../../game/component/card/token/CDetailBar";


export default class EnemyAttacksState extends State implements IOppActionListener
{
    // #region Attributes // 

    // private:

    // Signals.
    private _onCompleted: Signal = new Signal();

    // #endregion //


    // #region Properties //

    // Signals.
    public get onCompleted(): Signal { return this._onCompleted; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        super.onEnter();

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setCombatSubphase( combat_state_id.ENEMY_ATTACKS );
        
        ServiceLocator.game.setActivePlayer( ServiceLocator.game.firstPlayer );

        this.dealShadowCards();

        const kEndTurnButtonText: string = ServiceLocator.game.phaseNavigator.findNexEndTurnButtonI18n( combat_state_id.ENEMY_ATTACKS, game_state_id.COMBAT );
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cEndTurnButton.setText( kEndTurnButtonText );
        if ( Session.allyId )
        {
            ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.setText( kEndTurnButtonText );
        }

        // Listen to events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.add( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );
    }

        private dealShadowCards(): void
        {
            // Sort engaged enemies by threat value.
            let arrEnemyTokenList: Array<Array<GameObject>> = [ new Array<GameObject>(), new Array<GameObject>() ];
            // Player.
            let arrActor: Array<GameObject> = ServiceLocator.game.cGameWorld.cPlayerArea.cEngaged.findAllActors();
            for ( let actor of arrActor )
            {
                if ( actor.cCardToken.cDetailBar.findItem( detail_bar_icon_type.HINDER ) == null )
                {
                    let insertIndex: number = 0;
                    for ( let enemyToken of arrEnemyTokenList[ player_type.PLAYER ] )
                    {
                        if ( enemyToken.cCardToken.curSide.cEnemySide.engagementCost.cTokenCounter.count < actor.cCardToken.curSide.cEnemySide.engagementCost.cTokenCounter.count  )
                        {
                            break;
                        }
                        else
                        {
                            insertIndex += 1;
                        }
                    }
                    arrEnemyTokenList[ player_type.PLAYER ].splice( insertIndex, 0, actor );
                }
            }
            // Ally.
            if ( !Utils.game.isAllyIsolated() )
            {
                arrActor = ServiceLocator.game.cGameWorld.cAllyArea.cEngaged.findAllActors();
            }
            else
            {
                arrActor = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "isolated" ).cIsolatedArea.cEngaged.findAllActors();
            }
            for ( let actor of arrActor )
            {
                if ( actor.cCardToken.cDetailBar.findItem( detail_bar_icon_type.HINDER ) == null )
                {
                    let insertIndex: number = 0;
                    for ( let enemyToken of arrEnemyTokenList[ player_type.ALLY ] )
                    {
                        if ( enemyToken.cCardToken.curSide.cEnemySide.engagementCost.cTokenCounter.count < actor.cCardToken.curSide.cEnemySide.engagementCost.cTokenCounter.count  )
                        {
                            break;
                        }
                        else
                        {
                            insertIndex += 1;
                        }
                    }
                    arrEnemyTokenList[ player_type.ALLY ].splice( insertIndex, 0, actor );
                }
            }

            // Deal shadow cards.
            let listIndex: number = ServiceLocator.game.firstPlayer;
            while ( arrEnemyTokenList[ player_type.PLAYER ].length + arrEnemyTokenList[ player_type.ALLY ].length > 0
                && ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.deck.cDeckIndicator.panel.cViewer.cCardView.itemHolder.cContainer.c.children.length > 0 )
            {
                if ( arrEnemyTokenList[ listIndex ].length > 0 )
                {
                    let enemyToken: GameObject = arrEnemyTokenList[ listIndex ].shift();
                    let drawnCard: GameObject = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.deck.cDeckIndicator.cViewer.cCardView.drawCard();
                    enemyToken.cCardToken.curSide.cEnemySide.addShadowCard( drawnCard, action_scope_type.LOCAL );
                }

                listIndex ^= 1;
            }
        }

    public onLeave(): void
    {
        // Cleanup events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.remove( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );

        super.onLeave();
    }

    // overrides.
    public onLoad( sgState: any, pass: number ): void
    {
        super.onLoad( sgState, pass );

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setCombatSubphase( combat_state_id.ENEMY_ATTACKS );

        // Listen to events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.addOnce( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.SET_END_TURN_BTN_STATE
            && action.args[ 0 ] as end_turn_btn_state == end_turn_btn_state.COMPLETED )
        {
            if ( ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn.state == end_turn_btn_state.COMPLETED )
            {
                this._onCompleted.dispatch();
            }
        }
    }

    // #endregion //


    // #region Callbacks //

    private onEndTurnBtn_Click(): void
    {
        if ( !Session.allyId )
        {
            this._onCompleted.dispatch();
        }
        else
        {
            let cEndTurnBtn: CEndTurnButton = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn;
            cEndTurnBtn.toggleCompleted( action_scope_type.MULTIPLAYER );
            if ( cEndTurnBtn.state == end_turn_btn_state.COMPLETED 
                && ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.state == end_turn_btn_state.COMPLETED )
            {        
                this._onCompleted.dispatch();
            }
        }
    }

    // #endregion //
}