import State from "../../../lib/state_machine/State";

import { combat_state_id, game_state_id, quest_state_id } from "../../StateId";
import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../ServiceLocator";

import StateMachine from "../../../lib/StateMachine";
import { IOpponentAction, IOppActionListener } from "../../../game/AllyActionManager";
import { player_type } from "../../../game/component/world/CPlayerArea";
import PhaseIntroState from "../PhaseIntroState";
import GameObject from "../../../game/GameObject";
import Signal from "../../../lib/signals/Signal";
import Session from "../../../Session";
import CEndTurnButton, { end_turn_btn_state } from "../../../game/component/ui/CEndTurnButton";


export default class PlayerAttacksState extends State implements IOppActionListener
{
    // #region Attributes // 

    // private:

    // Signals.
    private _onCompleted: Signal = new Signal();

    // #endregion //


    // #region Properties //

    // Signals.
    public get onCompleted(): Signal { return this._onCompleted; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        super.onEnter();

        ServiceLocator.audioManager.playMusic( "combat_player_phase", { loop: false } );

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setCombatSubphase( combat_state_id.PLAYER_ATTACKS );
        
        ServiceLocator.game.setActivePlayer( ServiceLocator.game.firstPlayer );

        const kEndTurnButtonText: string = ServiceLocator.game.phaseNavigator.findNexEndTurnButtonI18n( combat_state_id.PLAYER_ATTACKS, game_state_id.COMBAT );
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cEndTurnButton.setText( kEndTurnButtonText );
        if ( Session.allyId )
        {
            ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.setText( kEndTurnButtonText );
        }

        // Listen to events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.add( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );
    }

    public onLeave(): void
    {
        // Cleanup events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.remove( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );
        
        super.onLeave();
    }

    // overrides.
    public onLoad( sgState: any, pass: number ): void
    {
        super.onLoad( sgState, pass );

        ServiceLocator.audioManager.playMusic( "combat_player_phase", { loop: false } );

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setCombatSubphase( combat_state_id.PLAYER_ATTACKS );

        // Listen to events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.addOnce( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.SET_END_TURN_BTN_STATE
            && action.args[ 0 ] as end_turn_btn_state == end_turn_btn_state.COMPLETED )
        {
            if ( ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn.state == end_turn_btn_state.COMPLETED )
            {
                this._onCompleted.dispatch();
            }
        }
    }

    // #endregion //


    // #region Callbacks //

    private onEndTurnBtn_Click(): void
    {
        if ( !Session.allyId )
        {
            this._onCompleted.dispatch();
        }
        else
        {
            let cEndTurnBtn: CEndTurnButton = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn;
            cEndTurnBtn.toggleCompleted( action_scope_type.MULTIPLAYER );
            if ( cEndTurnBtn.state == end_turn_btn_state.COMPLETED 
                && ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.state == end_turn_btn_state.COMPLETED )
            {        
                this._onCompleted.dispatch();
            }
        }
    }

    // #endregion //
}