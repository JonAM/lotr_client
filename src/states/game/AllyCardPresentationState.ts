import State from "../../lib/state_machine/State";

import { action_scope_type, player_action_type } from "../../service/socket_io/GameSocketIOController";
import { gsap, TimelineMax, Sine, Back } from "gsap";
import { location_type } from "../../game/component/world/CGameWorld";
import { player_type } from "../../game/component/world/CPlayerArea";
import ServiceLocator from "../../ServiceLocator";
import Utils from "../../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../../game/GameObject";
import Signal from "../../lib/signals/Signal";
import CGraphics from "../../game/component/pixi/CGraphics";
import CContainer from "../../game/component/pixi/CContainer";
import { IStateData } from "../../game/component/world/card_activation_area/CardActivationController";
import LogTargetCard from "../../game/component/ui/right_menu/action_log/target/LogTargetCard";
import { IOppActionListener, IOpponentAction } from "../../game/AllyActionManager";
import { button_type } from "./setup/CardPresentationUtils";
import { layer_type } from "../../game/component/world/CGameLayerProvider";


export default class AllyCardPresentationState extends State implements IOppActionListener
{
    // #region Attributes //

    // private:
    
    private _data: IStateData = null;
    private _from: location_type = null;

    private _backPlane: PIXI.SimplePlane = null;
    private _frontPlane: PIXI.SimplePlane = null;
    private _curPlane: PIXI.SimplePlane = null;

    // Signals.
    private _onCompleted: Signal = new Signal();

    // #endregion //

    
    // #region Properties //

    public set data( value: IStateData ) { this._data = value; }
    public set from( value: location_type ) { this._from = value; }

    // Signals.
    public get onCompleted(): Signal { return this._onCompleted; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        console.assert( this._data != null, "AllyCardPresentationState.ts :: onEnter() :: this._data cannot be null." );
        console.assert( this._from != null, "AllyCardPresentationState.ts :: onEnter() :: this._from cannot be null." );

        super.onEnter();

        ServiceLocator.game.keyBindingManager.setEnabled( false );

        this._data.disablerBg = new GameObject( [ new CGraphics() ] );
        this._data.disablerBg.init();
        this._data.disablerBg.cGraphics.g.interactive = true;
        this._data.disablerBg.cGraphics.g.beginFill( 0x000000 );
        this._data.disablerBg.cGraphics.g.drawRect( 0, 0, ServiceLocator.game.app.screen.width, ServiceLocator.game.app.screen.height );
        this._data.disablerBg.cGraphics.g.endFill();
        ServiceLocator.game.root.cGameLayerProvider.add( this._data.disablerBg, layer_type.CARD_PRESENTATION );

        this._data.planeContainer = new GameObject( [ new CContainer() ] );
        this._data.planeContainer.init();
        //
        this._backPlane = this.createCardPlane( Utils.img.findCardTexture( this._data.card.cCard.back.cardId ) );
        this._backPlane.visible = !this._data.card.cCard.isFaceUp;
        this._data.planeContainer.cContainer.c.addChild( this._backPlane );
        //
        this._frontPlane = this.createCardPlane( Utils.img.findCardTexture( this._data.card.cCard.front.cardId ) );
        this._frontPlane.visible = this._data.card.cCard.isFaceUp;
        this._data.planeContainer.cContainer.c.addChild( this._frontPlane );
        //
        this._curPlane = this._backPlane.visible ? this._backPlane : this._frontPlane;
        //
        this._data.planeContainer.cContainer.c.position.set( ServiceLocator.game.app.screen.width * 0.5, ServiceLocator.game.app.screen.height * 0.5 );
        ServiceLocator.game.root.cGameLayerProvider.add( this._data.planeContainer, layer_type.CARD_PRESENTATION );

        if ( ServiceLocator.game.isAnimated )
        {
            ServiceLocator.game.allyActionManager.pause();
        }

        this.animateIn();

        // Listen to events.
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.CARD_ACTIVATION_FLIP, player_action_type.CARD_ACTIVATION_PLACE ] );
    }
    
        private createCardPlane( texture: PIXI.Texture ): PIXI.SimplePlane
        {
            let cardPlane: PIXI.SimplePlane = null;

            const kVerticesX: number = 3;
            const kVerticesY: number = 3;
            cardPlane = new PIXI.SimplePlane( texture, kVerticesX, kVerticesY );
            cardPlane.pivot.set( cardPlane.width * 0.5, cardPlane.height * 0.5 );

            return cardPlane;
        }

    public onLeave(): void
    {
        this._onCompleted.removeAll();

        // Cleanup events.
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.CARD_ACTIVATION_FLIP, player_action_type.CARD_ACTIVATION_PLACE ] );

        ServiceLocator.game.keyBindingManager.setEnabled( true );

        super.onLeave();
    }

    // private:
    
    private animateIn(): void
    {
        let tl: TimelineMax = new TimelineMax();
        tl.addLabel( "in", 0 );
        this._data.disablerBg.cContainer.c.alpha = 0;
        tl.to( this._data.disablerBg.cContainer.c, { alpha: 0.6, ease: Sine.easeOut, duration: 0.25 }, "in" );
        if ( this._from == location_type.MY_UNDERNEATH || this._from == location_type.ALLY_UNDERNEATH )
        {
            let actor: GameObject = GameObject.find( this._data.card.cCard.locationData );
            Utils.game.showActor( actor );

            const kScale: number = actor.cCardToken.frameHeight / this._data.planeContainer.cContainer.c.getLocalBounds().height;
            this._data.planeContainer.cContainer.c.scale.set( kScale );
            const kDeckIndicatorGlobal: PIXI.Point = actor.cContainer.c.getGlobalPosition();
            tl.from( this._data.planeContainer.cContainer.c, { 
                    x: kDeckIndicatorGlobal.x + actor.cCardToken.frameWidth * 0.5,
                    y: kDeckIndicatorGlobal.y + actor.cCardToken.frameHeight * 0.5, 
                    ease: Sine.easeOut, duration: 1 }, 
                "in" );
        }
        else
        {
            let deckIndicator: GameObject = null;
            switch ( this._from )
            {
                case location_type.ALLY_DECK: { deckIndicator = ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.deck; break; }
                case location_type.ALLY_SET_ASIDE: { deckIndicator = ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.setAside; break; }
                case location_type.ALLY_HAND: { deckIndicator = ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.hand; break; }
                case location_type.ALLY_DISCARD: { deckIndicator = ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.discard; break; }
                case location_type.ALLY_CUSTOM_DECK: { deckIndicator = ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.customDeck; break; }
                case location_type.SAURON_DECK_0: { deckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.deck; break; }
                case location_type.SAURON_DISCARD: { deckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.discard; break; }
                case location_type.SAURON_DECK_1: { deckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.secondaryDeck; break; }
                case location_type.SAURON_CUSTOM_DECK_0: { deckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.customDeck; break; }
                case location_type.SAURON_CUSTOM_DISCARD_0: { deckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.customDiscard; break; }
                case location_type.SAURON_CUSTOM_DECK_1: { deckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.secondaryCustomDeck; break; }
                case location_type.SAURON_CUSTOM_DISCARD_1: { deckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.secondaryCustomDiscard; break; }
                case location_type.SET_ASIDE: { deckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.setAside; break; }
                case location_type.QUEST_DECK: { deckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.questDeck; break; }
                case location_type.QUEST_DISCARD: { deckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.questDiscard; break; }
                case location_type.REMOVED_FROM_GAME: { deckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.removedFromGame; break; }
                case location_type.VICTORY_DISPLAY: { deckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.victoryDisplay; break; }
                case location_type.CARD_TRAY: { deckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.tray; break; }

                // TODO: Underneath?
            }
            const kScale: number = deckIndicator.cContainer.c.height / this._data.planeContainer.cContainer.c.getLocalBounds().height;
            this._data.planeContainer.cContainer.c.scale.set( kScale );
            const kDeckIndicatorGlobal: PIXI.Point = deckIndicator.cContainer.c.getGlobalPosition();
            tl.from( this._data.planeContainer.cContainer.c, { 
                    x: kDeckIndicatorGlobal.x,
                    y: kDeckIndicatorGlobal.y + deckIndicator.cContainer.c.height * 0.5, 
                    ease: Sine.easeOut, duration: 1 }, 
                "in" );
        }
        //
        tl.to( this._data.planeContainer.cContainer.c.scale, { x: 1.5, y: 1.5, ease: Sine.easeOut, duration: 1 }, "in" );
        tl.call( this.onAnimateIn_Completed.bind( this ), null );
    }

    // private:

    private flipCard(): void
    {
        let buffer: PIXI.Buffer = this._curPlane.geometry.getBuffer( "aVertexPosition" );
        let mapVertexIndex: Map<string, number> = this.createVertexIndexMap( this._curPlane.rotation != 0 );
        let obj: Object = { 
            ltx: buffer.data[ mapVertexIndex.get( "ltx" ) ], lty: buffer.data[ mapVertexIndex.get( "lty" ) ], 
            rtx: buffer.data[ mapVertexIndex.get( "rtx" ) ], rty: buffer.data[ mapVertexIndex.get( "rty" ) ], 
            lmx: buffer.data[ mapVertexIndex.get( "lmx" ) ], lmy: buffer.data[ mapVertexIndex.get( "lmy" ) ], 
            rmx: buffer.data[ mapVertexIndex.get( "rmx" ) ], rmy: buffer.data[ mapVertexIndex.get( "rmy" ) ], 
            lbx: buffer.data[ mapVertexIndex.get( "lbx" ) ], lby: buffer.data[ mapVertexIndex.get( "lby" ) ], 
            rbx: buffer.data[ mapVertexIndex.get( "rbx" ) ], rby: buffer.data[ mapVertexIndex.get( "rby" ) ] };

        const kVerticalDeformation: number = 100;
        let tweenVars: gsap.TweenVars = {
            duration: 0.25,
            ease: Sine.easeIn,
            onUpdate: this.updateBuffer,
            onUpdateParams: [ buffer, obj, mapVertexIndex ],
            onComplete: this.playBackSideAnim, 
            onCompleteParams: [ obj ],
            callbackScope: this }
        if ( this._curPlane.rotation == 0 )
        {
            tweenVars.ltx = buffer.data[ mapVertexIndex.get( "ltx" ) ] + this._curPlane.width * 0.5;
            tweenVars.lty = buffer.data[ mapVertexIndex.get( "lty" ) ] - kVerticalDeformation;
            tweenVars.rtx = buffer.data[ mapVertexIndex.get( "rtx" ) ] - this._curPlane.width * 0.5; 
            tweenVars.rty = buffer.data[ mapVertexIndex.get( "rty" ) ] + kVerticalDeformation;
            tweenVars.lmx = buffer.data[ mapVertexIndex.get( "lmx" ) ] + this._curPlane.width * 0.5; 
            tweenVars.rmx = buffer.data[ mapVertexIndex.get( "rmx" ) ] - this._curPlane.width * 0.5; 
            tweenVars.lbx = buffer.data[ mapVertexIndex.get( "lbx" ) ] + this._curPlane.width * 0.5; 
            tweenVars.lby = buffer.data[ mapVertexIndex.get( "lby" ) ] + kVerticalDeformation;
            tweenVars.rbx = buffer.data[ mapVertexIndex.get( "rbx" ) ] - this._curPlane.width * 0.5;
            tweenVars.rby = buffer.data[ mapVertexIndex.get( "rby" ) ] - kVerticalDeformation;
        }
        else
        {
            tweenVars.lty = buffer.data[ mapVertexIndex.get( "lty" ) ] - this._curPlane.height * 0.5; 
            tweenVars.ltx = buffer.data[ mapVertexIndex.get( "ltx" ) ] - kVerticalDeformation;
            tweenVars.rty = buffer.data[ mapVertexIndex.get( "rty" ) ] + this._curPlane.height * 0.5; 
            tweenVars.rtx = buffer.data[ mapVertexIndex.get( "rtx" ) ] + kVerticalDeformation;
            tweenVars.lmy = buffer.data[ mapVertexIndex.get( "lmy" ) ] - this._curPlane.height * 0.5; 
            tweenVars.rmy = buffer.data[ mapVertexIndex.get( "rmy" ) ] + this._curPlane.height * 0.5; 
            tweenVars.lby = buffer.data[ mapVertexIndex.get( "lby" ) ] - this._curPlane.height * 0.5; 
            tweenVars.lbx = buffer.data[ mapVertexIndex.get( "lbx" ) ] + kVerticalDeformation;
            tweenVars.rby = buffer.data[ mapVertexIndex.get( "rby" ) ] + this._curPlane.height * 0.5; 
            tweenVars.rbx = buffer.data[ mapVertexIndex.get( "rbx" ) ] - kVerticalDeformation;
        }
        gsap.to( obj, tweenVars );
    }

        private createVertexIndexMap( isLandscape: boolean ): Map<string, number>
        {
            let result: Map<string, number> = null;

            if ( !isLandscape )
            {
                result = new Map<string, number>( [ 
                    [ "ltx", 0 ], [ "lty", 1 ], [ "rtx", 4 ], [ "rty", 5 ],
                    [ "lmx", 6 ], [ "lmy", 7 ], [ "rmx", 10 ], [ "rmy", 11 ],
                    [ "lbx", 12 ], [ "lby", 13 ], [ "rbx", 16 ], [ "rby", 17 ] ] );
            }
            else
            {
                result = new Map<string, number>( [ 
                    [ "ltx", 12 ], [ "lty", 13 ], [ "rtx", 0 ], [ "rty", 1 ],
                    [ "lmx", 14 ], [ "lmy", 15 ], [ "rmx", 2 ], [ "rmy", 3 ],
                    [ "lbx", 16 ], [ "lby", 17 ], [ "rbx", 4 ], [ "rby", 5 ] ] );
            }

            return result;
        }

        private updateBuffer( buffer: PIXI.Buffer, obj: Object, mapVertexIndex: Map<string, number> ): void
        {
            buffer.data[ mapVertexIndex.get( "ltx" ) ] = obj[ "ltx" ];
            buffer.data[ mapVertexIndex.get( "lty" ) ] = obj[ "lty" ];
            buffer.data[ mapVertexIndex.get( "rtx" ) ] = obj[ "rtx" ];
            buffer.data[ mapVertexIndex.get( "rty" ) ] = obj[ "rty" ];
            buffer.data[ mapVertexIndex.get( "lmx" ) ] = obj[ "lmx" ];
            buffer.data[ mapVertexIndex.get( "lmy" ) ] = obj[ "lmy" ];
            buffer.data[ mapVertexIndex.get( "rmx" ) ] = obj[ "rmx" ];
            buffer.data[ mapVertexIndex.get( "rmy" ) ] = obj[ "rmy" ];
            buffer.data[ mapVertexIndex.get( "lbx" ) ] = obj[ "lbx" ];
            buffer.data[ mapVertexIndex.get( "lby" ) ] = obj[ "lby" ];
            buffer.data[ mapVertexIndex.get( "rbx" ) ] = obj[ "rbx" ];
            buffer.data[ mapVertexIndex.get( "rby" ) ] = obj[ "rby" ];
            buffer.update();
        }

    private playBackSideAnim( obj: Object ): void 
    {
        this._curPlane.visible = false;
        let isRotated: boolean = this._curPlane.rotation != 0;
        this._curPlane = this._curPlane == this._frontPlane ? this._backPlane : this._frontPlane;
        this._curPlane.visible = true;

        let buffer: PIXI.Buffer = this._curPlane.geometry.getBuffer( "aVertexPosition" );
        let mapVertexIndex: Map<string, number> = this.createVertexIndexMap( this._curPlane.rotation != 0 );
        let obji: Object = { 
            ltx: buffer.data[ mapVertexIndex.get( "ltx" ) ], lty: buffer.data[ mapVertexIndex.get( "lty" ) ], 
            rtx: buffer.data[ mapVertexIndex.get( "rtx" ) ], rty: buffer.data[ mapVertexIndex.get( "rty" ) ], 
            lmx: buffer.data[ mapVertexIndex.get( "lmx" ) ], lmy: buffer.data[ mapVertexIndex.get( "lmy" ) ], 
            rmx: buffer.data[ mapVertexIndex.get( "rmx" ) ], rmy: buffer.data[ mapVertexIndex.get( "rmy" ) ], 
            lbx: buffer.data[ mapVertexIndex.get( "lbx" ) ], lby: buffer.data[ mapVertexIndex.get( "lby" ) ], 
            rbx: buffer.data[ mapVertexIndex.get( "rbx" ) ], rby: buffer.data[ mapVertexIndex.get( "rby" ) ] };
        //
        buffer.data[ 0 ] = obj[ isRotated ? "rty" : "rtx" ];
        buffer.data[ 1 ] = obj[ isRotated ? "rtx" : "rty" ];
        buffer.data[ 4 ] = obj[ isRotated ? "lty" : "ltx" ];
        buffer.data[ 5 ] = obj[ isRotated ? "ltx" : "lty" ];
        buffer.data[ 6 ] = obj[ isRotated ? "rmy" : "rmx" ];
        buffer.data[ 10 ] = obj[ isRotated ? "lmy" : "lmx" ];
        buffer.data[ 12 ] = obj[ isRotated ? "rby" : "rbx" ];
        buffer.data[ 13 ] = obj[ isRotated ? "rbx" : "rby" ];
        buffer.data[ 16 ] = obj[ isRotated ? "lby" : "lbx" ];
        buffer.data[ 17 ] = obj[ isRotated ? "lbx" : "lby" ];
        buffer.update();

        obj = { 
            ltx: buffer.data[ 0 ], lty: buffer.data[ 1 ], 
            rtx: buffer.data[ 4 ], rty: buffer.data[ 5 ], 
            lmx: buffer.data[ 6 ], lmy: buffer.data[ 7 ], 
            rmx: buffer.data[ 10 ], rmy: buffer.data[ 11 ], 
            lbx: buffer.data[ 12 ], lby: buffer.data[ 13 ], 
            rbx: buffer.data[ 16 ], rby: buffer.data[ 17 ] };

        gsap.to( obj, { 
            ltx: obji[ "ltx" ], 
            lty: obji[ "lty" ], 
            rtx: obji[ "rtx" ], 
            rty: obji[ "rty" ], 
            lmx: obji[ "lmx" ], 
            rmx: obji[ "rmx" ], 
            lbx: obji[ "lbx" ], 
            lby: obji[ "lby" ], 
            rbx: obji[ "rbx" ], 
            rby: obji[ "rby" ], 
            duration: 0.25, 
            onUpdate: this.updateBuffer, 
            onUpdateParams: [ buffer, obj, mapVertexIndex ],
            onComplete: this.onFlipCardAnim_Completed, 
            callbackScope: this,
            ease: Sine.easeOut });
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.CARD_ACTIVATION_FLIP )
        {
            if ( ServiceLocator.game.isAnimated )
            {
                ServiceLocator.game.allyActionManager.pause();
            }

            const kArrLandscapeTypeCode: Array<string> = [ "player-side-quest", "encounter-side-quest" ];
            if ( kArrLandscapeTypeCode.indexOf( this._data.card.cCard.curSide.cardInfo.type_code ) != -1 && kArrLandscapeTypeCode.indexOf( this._data.card.cCard.otherSide.cardInfo.type_code ) == -1
                || kArrLandscapeTypeCode.indexOf( this._data.card.cCard.curSide.cardInfo.type_code ) == -1 && kArrLandscapeTypeCode.indexOf( this._data.card.cCard.otherSide.cardInfo.type_code ) != -1 )
            {
                this._curPlane.rotation = Math.PI * 0.5;
            }
            this.flipCard();
        }
        else if ( action.playerActionType == player_action_type.CARD_ACTIVATION_PLACE )
        {
            this._data.buttonRowType = action.args[ 0 ] as button_type;

            this._onCompleted.dispatch();
        }
    }

    // #endregion //


    // #region Callbacks //

    private onAnimateIn_Completed(): void
    {
        if ( ServiceLocator.game.isAnimated )
        {
            ServiceLocator.game.allyActionManager.resume();
        }
    }

    private onFlipCardAnim_Completed(): void
    {
        if ( ServiceLocator.game.isAnimated )
        {
            ServiceLocator.game.allyActionManager.resume();
        }

        // Log.
        ServiceLocator.game.cGameWorld.cActionLogger.logCardFlip( player_type.ALLY, new LogTargetCard( this._data.card ), false );

        this._data.card.cCard.isAnimated = false;
        if ( this._curPlane == this._frontPlane )
        {
            this._data.card.cCard.setFaceUp( action_scope_type.LOCAL );
        }
        else
        {
            this._data.card.cCard.setFaceDown( action_scope_type.LOCAL );
        }
        this._data.card.cCard.isAnimated = true;

        if ( ServiceLocator.game.isAnimated )
        {
            ServiceLocator.game.allyActionManager.resume();
        }
    }

    // #endregion //
}