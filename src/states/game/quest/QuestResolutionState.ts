import State from "../../../lib/state_machine/State";

import { game_state_id, quest_state_id } from "../../StateId";
import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../ServiceLocator";
import { Circ, Sine } from "gsap";
import * as PIXI from "pixi.js";

import { IOpponentAction, IOppActionListener } from "../../../game/AllyActionManager";
import GameObject from "../../../game/GameObject";
import Signal from "../../../lib/signals/Signal";
import Session from "../../../Session";
import CEndTurnButton, { end_turn_btn_state } from "../../../game/component/ui/CEndTurnButton";
import CFlyingOrb from "../../../game/component/world/CFlyingOrb";
import CContainer from "../../../game/component/pixi/CContainer";
import { location_type } from "../../../game/component/world/CGameWorld";
import SignalBinding from "../../../lib/signals/SignalBinding";
import Utils from "../../../Utils";
import { player_type } from "../../../game/component/world/CPlayerArea";
import LogTargetPlayer from "../../../game/component/ui/right_menu/action_log/target/LogTargetPlayer";
import { status_type } from "../../../game/component/card/token/CCardTokenSide";
import { layer_type } from "../../../game/component/world/CGameLayerProvider";
import { detail_bar_icon_type } from "../../../game/component/card/token/CDetailBar";


export default class QuestResolutionState extends State implements IOppActionListener
{
    // #region Attributes // 

    // private:

    private _orbLaunchDelayMs: number = 0;

    // Signals.
    private _onCompleted: Signal = new Signal();

    // #endregion //


    // #region Properties //

    // Signals.
    public get onCompleted(): Signal { return this._onCompleted; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        super.onEnter();

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setQuestSubphase( quest_state_id.QUEST_RESOLUTION );

        const kEndTurnButtonText: string = ServiceLocator.game.phaseNavigator.findNexEndTurnButtonI18n( quest_state_id.QUEST_RESOLUTION, game_state_id.QUEST );
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cEndTurnButton.setText( kEndTurnButtonText );
        if ( Session.allyId )
        {
            ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.setText( kEndTurnButtonText );
        }

        // Compare threat and willpower indicators and add threat/progress.
        let stagingThreat: number = ServiceLocator.game.cGameWorld.cSauronArea.cStagingThreatCounter.counter.cTokenCounter.count 
            - ServiceLocator.game.cGameWorld.cSauronArea.cPlayerWillpowerCounter.counter.cTokenCounter.count
            - ServiceLocator.game.cGameWorld.cSauronArea.cSauronWillpowerCounter.counter.cTokenCounter.count
            - ServiceLocator.game.cGameWorld.cSauronArea.cAllyWillpowerCounter.counter.cTokenCounter.count;
        let isDefense: boolean = ServiceLocator.game.cGameWorld.cSauronArea.cQuestHolder.questToken
            &&  ServiceLocator.game.cGameWorld.cSauronArea.cQuestHolder.questToken.cCardToken.cDetailBar.findItem( detail_bar_icon_type.DEFENSE ) != null;
        if ( stagingThreat > 0 && !isDefense )
        {
            this.launchThreatOrb( stagingThreat, location_type.MY_THREAT_LEVEL );
            if ( Session.allyId && !Utils.game.isAllyIsolated() )
            {
                this.launchThreatOrb( stagingThreat, location_type.ALLY_THREAT_LEVEL );
            }

            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.SAURON, new LogTargetPlayer( player_type.SAURON ), "threat_level", stagingThreat, null );
        }
        else if ( stagingThreat > 0 && isDefense 
            || stagingThreat < 0 )
        {
            const kIsQuest: boolean = ServiceLocator.game.cGameWorld.cSauronArea.cQuestHolder.questToken
                && !ServiceLocator.game.cGameWorld.cSauronArea.cQuestHolder.questToken.cCardToken.cDetailBar.findItem( detail_bar_icon_type.ASSAULT );
            const kIsActiveLocation: boolean = ServiceLocator.game.cGameWorld.cSauronArea.cActiveLocationHolder.hasUnexploredLocation();
            let progress: number = Math.abs( stagingThreat );
            if ( !kIsQuest )
            {
                if ( kIsActiveLocation )
                {
                    this.launchProgressOrb( progress, location_type.ACTIVE_LOCATION );
                }
            }
            else if ( kIsQuest )
            {
                if ( kIsActiveLocation )
                {
                    let delta: number = 0;
                    for ( let locationToken of ServiceLocator.game.cGameWorld.cSauronArea.cActiveLocationHolder.locationTokens )
                    {
                        const kCurProgress: number = locationToken.cCardToken.curSide.cLocationSide.progress.cTokenCounter.count;
                        const kQuestPoints: number = locationToken.cCardToken.curSide.cLocationSide.questPoints.cTokenCounter.count;
                        const kCurDelta: number = kQuestPoints - kCurProgress;
                        if ( kCurDelta > 0 )
                        {
                            delta += kQuestPoints - kCurProgress;
                        }
                    }
                    if ( delta > progress )
                    {
                        delta = progress;
                    }
                    
                    this.launchProgressOrb( delta, location_type.ACTIVE_LOCATION );
                
                    progress -= delta;
                }
                if ( progress > 0 )
                {
                    this.launchProgressOrb( progress, location_type.QUEST );
                }
            }

            ServiceLocator.game.cGameWorld.cActionLogger.logCounter( player_type.SAURON, new LogTargetPlayer( player_type.SAURON ), "progress_token", Math.abs( stagingThreat ), null );
        }

        // Listen to events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.add( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );
    }

        private launchThreatOrb( count: number, location: location_type ): void
        {
            for ( let i: number = 0; i < count; ++i )
            {
                let orb: GameObject = this.createFlyingOrb( 0xff0000, location );
                orb.cFlyingOrb.target.y += 25;
                ServiceLocator.game.root.cGameLayerProvider.add( orb, layer_type.VFX );
                let sb: SignalBinding = orb.cFlyingOrb.onHit.addOnce( this.onThreatOrb_Hit, this );
                sb.params = [ location ];

                window.setTimeout( this.launchFlyingOrb.bind( this, orb ), this._orbLaunchDelayMs );
                this._orbLaunchDelayMs += Math.random() * 250;
            }
        }

            private createFlyingOrb( color: number, location: location_type ): GameObject
            {
                let orb: GameObject = new GameObject( [ new CContainer(), new CFlyingOrb() ] );
                orb.cFlyingOrb.color = color;
                orb.cFlyingOrb.target = ServiceLocator.game.cGameWorld.findLocation( location ).cContainer.c.getGlobalPosition();
                orb.init();

                return orb;
            }

            private launchFlyingOrb( orb: GameObject ): void
            {
                let questVariationContainer: PIXI.Container = ServiceLocator.game.cGameWorld.cSauronArea.questVariation.cContainer.c;
                Utils.anim.flashCircle( questVariationContainer.getGlobalPosition(), questVariationContainer.width * 0.5, 1.5, ServiceLocator.game.world.cGameLayerProvider.get( layer_type.VFX ) );

                let mLaunchDir: PIXI.Matrix = new PIXI.Matrix();
                mLaunchDir.translate( 1, 0 );
                let launcherRot: number = Math.PI / 8 * Math.random();
                const kFrom: PIXI.Point = ServiceLocator.game.cGameWorld.cSauronArea.questVariation.cContainer.c.getGlobalPosition();
                if ( kFrom.y > orb.cFlyingOrb.target.y )
                {
                    launcherRot *= -1;
                }
                mLaunchDir.rotate( launcherRot );
                orb.cFlyingOrb.launch( {
                    from: kFrom,
                    moveDir: new PIXI.Point( mLaunchDir.tx, mLaunchDir.ty ),
                    moveSpeed: 10 + 5 * Math.random(), 
                    moveTween: { value: 2, duration: 2, ease: Sine.easeOut },
                    seekSpeed: Math.PI * 0.0025,
                    attractSpeed: 5 * Math.random(),
                    attractTween: { value: 160, duration: 1, ease: Circ.easeIn, delay: 1 + 1 * Math.random() } } );
            }

        private launchProgressOrb( count: number, location: location_type ): void
        {
            for ( let i: number = 0; i < count; ++i )
            {
                let orb: GameObject = this.createFlyingOrb( 0x00ff00, location );
                ServiceLocator.game.world.cGameLayerProvider.add( orb, layer_type.VFX );
                let sb: SignalBinding = orb.cFlyingOrb.onHit.addOnce( this.onProgressOrb_Hit, this );
                sb.params = [ location ];

                window.setTimeout( this.launchFlyingOrb.bind( this, orb ), this._orbLaunchDelayMs );
                this._orbLaunchDelayMs += Math.random() * 250;
            }
        }

    public onLeave(): void
    {
        // Cleanup events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.remove( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );

        super.onLeave();
    }

    // overrides.
    public onLoad( sgState: any, pass: number ): void
    {
        super.onLoad( sgState, pass );

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setQuestSubphase( quest_state_id.QUEST_RESOLUTION );

        // Listen to events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.addOnce( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.SET_END_TURN_BTN_STATE
            && action.args[ 0 ] as end_turn_btn_state == end_turn_btn_state.COMPLETED )
        {
            if ( ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn.state == end_turn_btn_state.COMPLETED )
            {
                this._onCompleted.dispatch();
            }
        }
    }

    // #endregion //


    // #region Callbacks //

    private onEndTurnBtn_Click(): void
    {
        if ( !Session.allyId )
        {
            this._onCompleted.dispatch();
        }
        else
        {
            let cEndTurnBtn: CEndTurnButton = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn;
            cEndTurnBtn.toggleCompleted( action_scope_type.MULTIPLAYER );
            if ( cEndTurnBtn.state == end_turn_btn_state.COMPLETED 
                && ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.state == end_turn_btn_state.COMPLETED )
            {        
                this._onCompleted.dispatch();
            }
        }
    }

    private onThreatOrb_Hit( location: location_type ): void
    {
        ServiceLocator.game.cGameWorld.findLocation( location ).cTokenIndicator.trigger.cTokenCounter.addCount( 1, Utils.game.isAllyIsolated() ? action_scope_type.MULTIPLAYER : action_scope_type.LOCAL );
    }

    private onProgressOrb_Hit( location: location_type ): void
    {
        if ( location == location_type.QUEST )
        {
            let questToken: GameObject = ServiceLocator.game.cGameWorld.cSauronArea.cQuestHolder.questToken;
            if ( questToken )
            {
                if ( questToken.cCardToken.curSide.cQuestSide )
                {
                    questToken.cCardToken.curSide.cQuestSide.progress.cTokenCounter.addCount( 1, Utils.game.isAllyIsolated() ? action_scope_type.MULTIPLAYER : action_scope_type.LOCAL );
                }
                else if ( questToken.cCardToken.curSide.cSideQuestSide )
                {
                    questToken.cCardToken.curSide.cSideQuestSide.progress.cTokenCounter.addCount( 1, Utils.game.isAllyIsolated() ? action_scope_type.MULTIPLAYER : action_scope_type.LOCAL );
                }
            }
        }
        else if ( location == location_type.ACTIVE_LOCATION )
        {
            let activeLocationToken: GameObject = ServiceLocator.game.cGameWorld.cSauronArea.cActiveLocationHolder.activeLocationToken;
            if ( activeLocationToken )
            {
                activeLocationToken.cCardToken.curSide.cLocationSide.progress.cTokenCounter.addCount( 1, Utils.game.isAllyIsolated() ? action_scope_type.MULTIPLAYER : action_scope_type.LOCAL );
                if ( activeLocationToken.cCardToken.curSide.cLocationSide.hasStatus( status_type.EXPLORED ) )
                {
                    ServiceLocator.game.cGameWorld.cSauronArea.cActiveLocationHolder.autoSelectUnexploredLocation( Utils.game.isAllyIsolated() ? action_scope_type.MULTIPLAYER : action_scope_type.LOCAL );
                }
            }
        }
    }

    // #endregion //
}