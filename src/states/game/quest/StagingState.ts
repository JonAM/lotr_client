import State from "../../../lib/state_machine/State";

import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import { game_state_id, quest_state_id } from "../../StateId";
import { location_type } from "../../../game/component/world/CGameWorld";
import { player_type } from "../../../game/component/world/CPlayerArea";
import ServiceLocator from "../../../ServiceLocator";

import { IOpponentAction, IOppActionListener } from "../../../game/AllyActionManager";
import GameObject from "../../../game/GameObject";
import Signal from "../../../lib/signals/Signal";
import Session from "../../../Session";
import CEndTurnButton, { end_turn_btn_state } from "../../../game/component/ui/CEndTurnButton";


export default class StagingState extends State implements IOppActionListener
{
    // #region Attributes // 

    // private:

    //private _drawCount: number = null;

    // Signals.
    private _onCompleted: Signal = new Signal();

    // #endregion //


    // #region Properties //

    // Signals.
    public get onCompleted(): Signal { return this._onCompleted; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        super.onEnter();

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setQuestSubphase( quest_state_id.STAGING );

        const kEndTurnButtonText: string = ServiceLocator.game.phaseNavigator.findNexEndTurnButtonI18n( quest_state_id.STAGING, game_state_id.QUEST );
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cEndTurnButton.setText( kEndTurnButtonText );
        if ( Session.allyId )
        {
            ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.setText( kEndTurnButtonText );
        }

        // Reveal one encounter card for each non-eliminated player.
        if ( ServiceLocator.game.phaseNavigator.isStagingSubphaseDrawCard
            && ( Session.allyId == null || ServiceLocator.game.firstPlayer == player_type.PLAYER ) )
        {
            let drawnCard: GameObject = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.deck.cDeckIndicator.cViewer.cCardView.drawCard();
            if ( drawnCard )
            {
                ServiceLocator.game.cardActivationArea.cCardActivationArea.forceCardActivation( drawnCard, location_type.SAURON_DECK_0, action_scope_type.MULTIPLAYER );
            }
        }

        // Listen to events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.add( this.onEndTurnBtn_Click, this );
        //ServiceLocator.game.cardActivationArea.cCardActivationArea.onCompleted.addOnce( this.onEncounterCard_Revealed, this );
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );
    }

    public onLeave(): void
    {
        // Cleanup events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.remove( this.onEndTurnBtn_Click, this );
        //ServiceLocator.game.cardActivationArea.cCardActivationArea.onCompleted.remove( this.onEncounterCard_Revealed, this );
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );

        super.onLeave();
    }

    // overrides.
    public onLoad( sgState: any, pass: number ): void
    {
        super.onLoad( sgState, pass );

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setQuestSubphase( quest_state_id.STAGING );

        // Listen to events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.addOnce( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.SET_END_TURN_BTN_STATE
            && action.args[ 0 ] as end_turn_btn_state == end_turn_btn_state.COMPLETED )
        {
            if ( ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn.state == end_turn_btn_state.COMPLETED )
            {
                this._onCompleted.dispatch();
            }
        }
    }

    // #endregion //


    // #region Callbacks //

    private onEndTurnBtn_Click(): void
    {
        if ( !Session.allyId )
        {
            this._onCompleted.dispatch();
        }
        else
        {
            let cEndTurnBtn: CEndTurnButton = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn;
            cEndTurnBtn.toggleCompleted( action_scope_type.MULTIPLAYER );
            if ( cEndTurnBtn.state == end_turn_btn_state.COMPLETED 
                && ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.state == end_turn_btn_state.COMPLETED )
            {        
                this._onCompleted.dispatch();
            }
        }
    }

    /*private onEncounterCard_Revealed(): void
    {
        if ( this._drawCount == 2 )
        {
            let drawnCard: GameObject = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.deck.cDeckIndicator.cViewer.cCardView.drawCard();
            if ( drawnCard )
            {
                ServiceLocator.game.cardActivationArea.cDropArea.forceDrop( drawnCard, null, action_scope_type.LOCAL );
            }
        }
    }*/

    // #endregion //
}