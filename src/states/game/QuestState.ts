import State from "../../lib/state_machine/State";

import { game_state_id, quest_state_id } from "../StateId";
import { action_scope_type, player_action_type } from "../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../ServiceLocator";
import Utils from "../../Utils";

import StateMachine from "../../lib/StateMachine";
import { IOpponentAction, IOppActionListener } from "../../game/AllyActionManager";
import { player_type } from "../../game/component/world/CPlayerArea";
import PhaseIntroState from "./PhaseIntroState";
import GameObject from "../../game/GameObject";
import CharacterCommitmentState from "./quest/CharacterCommitmentState";
import StagingState from "./quest/StagingState";
import QuestResolutionState from "./quest/QuestResolutionState";
import { ISgQuestState } from "../../view/game/SaveGameView";
import Session from "../../Session";
import { end_turn_btn_state } from "../../game/component/ui/CEndTurnButton";
import { status_type } from "../../game/component/card/token/CCardTokenSide";
import CIsolatedArea from "../../game/component/world/custom_area/CIsolatedArea";


export default class QuestState extends State implements IOppActionListener
{
    // #region Attributes //

    // private:

    private _stateMachine: StateMachine = null;

    // #endregion //


    // #region Properties //

    public get stateMachine(): StateMachine { return this._stateMachine; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        super.onEnter();

        ServiceLocator.audioManager.playMusic( "quest_phase", { loop: false } );

        ServiceLocator.game.cGameWorld.cActionLogger.logPhaseStart( game_state_id.QUEST );
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setGamePhase( game_state_id.QUEST );

        ServiceLocator.game.setActivePlayer( ServiceLocator.game.firstPlayer );

        ServiceLocator.game.cGameWorld.cSauronArea.cPlayerWillpowerCounter.show();
        ServiceLocator.game.cGameWorld.cSauronArea.cPlayerWillpowerCounter.forceAutoBtnClick();
        ServiceLocator.game.cGameWorld.cSauronArea.cSauronWillpowerCounter.show();
        ServiceLocator.game.cGameWorld.cSauronArea.cSauronWillpowerCounter.forceAutoBtnClick();
        if ( Session.allyId )
        {
            if ( !Utils.game.isAllyIsolated() )
            {
                ServiceLocator.game.cGameWorld.cSauronArea.cAllyWillpowerCounter.show();
                ServiceLocator.game.cGameWorld.cSauronArea.cAllyWillpowerCounter.forceAutoBtnClick();
            }
            else
            {
                let cIsolatedArea: CIsolatedArea = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "isolated" ).cIsolatedArea;
                cIsolatedArea.cAllyWillpowerCounter.show();
                cIsolatedArea.cAllyWillpowerCounter.forceAutoBtnClick();
                //
                cIsolatedArea.cSauronWillpowerCounter.show();
                cIsolatedArea.cSauronWillpowerCounter.forceAutoBtnClick();
                //
                cIsolatedArea.questVariation.cContainer.c.visible = true;
            }
        }
        ServiceLocator.game.cGameWorld.cSauronArea.questVariation.cContainer.c.visible = true;

        // Create states.
        // Character commitment.
        let characterCommitmentState: CharacterCommitmentState = new CharacterCommitmentState();
        characterCommitmentState.onCompleted.addOnce( this.onPhase_Completed, this );
        // Staging.
        let stagingState: StagingState = new StagingState();
        stagingState.onCompleted.addOnce( this.onPhase_Completed, this );
        // Quest resolution.
        let questResolutionState: QuestResolutionState = new QuestResolutionState();
        questResolutionState.onCompleted.addOnce( this.onPhase_Completed, this );
        //
        this._stateMachine = new StateMachine();
        this._stateMachine.add( quest_state_id.CHARACTER_COMMITMENT, characterCommitmentState );
        this._stateMachine.add( quest_state_id.STAGING, stagingState );
        this._stateMachine.add( quest_state_id.QUEST_RESOLUTION, questResolutionState );
        this._stateMachine.init();
        this._stateMachine.request( ServiceLocator.game.phaseNavigator.findFirstPlayableSubstateId( game_state_id.QUEST ) );
    }

    public onLeave(): void
    {
        this._stateMachine.end();
        this._stateMachine = null;

        ServiceLocator.audioManager.stopMusic();

        ServiceLocator.game.cGameWorld.cSauronArea.cPlayerWillpowerCounter.hide();
        ServiceLocator.game.cGameWorld.cSauronArea.cSauronWillpowerCounter.hide();
        if ( Session.allyId )
        {
            if ( !Utils.game.isAllyIsolated() )
            {
                ServiceLocator.game.cGameWorld.cSauronArea.allyWillpowerCounter.cPlayerWillpowerCounter.hide();
            }
            else
            {
                let cIsolatedArea: CIsolatedArea = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "isolated" ).cIsolatedArea;
                cIsolatedArea.allyWillpowerCounter.cPlayerWillpowerCounter.hide();
                cIsolatedArea.questVariation.cContainer.c.visible = false;
            }
        }
        ServiceLocator.game.cGameWorld.cSauronArea.questVariation.cContainer.c.visible = false;

        let arrActor: Array<GameObject> = ServiceLocator.game.cGameWorld.cPlayerArea.cHome.findAllActors();
        for ( let cardToken of arrActor )
        {
            cardToken.cCardToken.cCurSide.removeStatus( status_type.COMMITTED_TO_QUEST, action_scope_type.LOCAL );
        }
        arrActor = ServiceLocator.game.cGameWorld.cAllyArea.cHome.findAllActors();
        for ( let cardToken of arrActor )
        {
            cardToken.cCardToken.cCurSide.removeStatus( status_type.COMMITTED_TO_QUEST, action_scope_type.LOCAL );
        }
        arrActor = ServiceLocator.game.cGameWorld.cSauronArea.cStaging.findAllActors();
        for ( let cardToken of arrActor )
        {
            cardToken.cCardToken.cCurSide.removeStatus( status_type.COMMITTED_TO_QUEST, action_scope_type.LOCAL );
        }
        if ( Session.allyId && Utils.game.isAllyIsolated() )
        {
            let cIsolatedArea: CIsolatedArea = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "isolated" ).cIsolatedArea;
            arrActor = cIsolatedArea.home.cPlayerActorArea.findAllActors();
            for ( let cardToken of arrActor )
            {
                cardToken.cCardToken.cCurSide.removeStatus( status_type.COMMITTED_TO_QUEST, action_scope_type.LOCAL );
            }
        }
    
        super.onLeave();
    }

    public findCurrentSubstate(): quest_state_id
    {
        return this._stateMachine.currentStateId;
    }

    // overrides.
    public saveGame(): any
    {
        let result: ISgQuestState = { 
            substateId: this._stateMachine.currentStateId };

        return result;
    }

    // overrides.
    public onLoad( sgState: any, pass: number ): void
    {
        super.onLoad( sgState, pass );

        ServiceLocator.audioManager.playMusic( "quest_phase", { loop: false } );

        let sgQuestState: ISgQuestState = sgState as ISgQuestState;

        ServiceLocator.game.cGameWorld.cSauronArea.cPlayerWillpowerCounter.show();
        ServiceLocator.game.cGameWorld.cSauronArea.cSauronWillpowerCounter.show();
        if ( Session.allyId )
        {
            if ( !Utils.game.isAllyIsolated() )
            {
                ServiceLocator.game.cGameWorld.cSauronArea.cAllyWillpowerCounter.show();
            }
            else
            {
                let cIsolatedArea: CIsolatedArea = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "isolated" ).cIsolatedArea;
                cIsolatedArea.cAllyWillpowerCounter.show();
                cIsolatedArea.cSauronWillpowerCounter.show();
                cIsolatedArea.questVariation.cContainer.c.visible = true;
            }
        }
        ServiceLocator.game.cGameWorld.cSauronArea.questVariation.cContainer.c.visible = true;

        // Create states.
        // Character commitment.
        let characterCommitmentState: CharacterCommitmentState = new CharacterCommitmentState();
        characterCommitmentState.onCompleted.addOnce( this.onPhase_Completed, this );
        // Staging.
        let stagingState: StagingState = new StagingState();
        stagingState.onCompleted.addOnce( this.onPhase_Completed, this );
        // Quest resolution.
        let questResolutionState: QuestResolutionState = new QuestResolutionState();
        questResolutionState.onCompleted.addOnce( this.onPhase_Completed, this );
        //
        this._stateMachine = new StateMachine();
        this._stateMachine.add( quest_state_id.CHARACTER_COMMITMENT, characterCommitmentState );
        this._stateMachine.add( quest_state_id.STAGING, stagingState );
        this._stateMachine.add( quest_state_id.QUEST_RESOLUTION, questResolutionState );
        this._stateMachine.init();
        this._stateMachine.load( sgQuestState.substateId, sgState, pass );
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        // TODO:
    }

    // #endregion //


    // #region Callbacks //

    private onPhase_Completed(): void
    {
        const kLastStateId: number = ServiceLocator.game.phaseNavigator.findLastPlayableSubstateId( game_state_id.QUEST );
        if ( this._stateMachine.currentStateId >= kLastStateId || kLastStateId == null )
        {
            ServiceLocator.game.stateMachine.onStateEntered.addOnce( ( state: State ) => {
                let phaseIntroState: PhaseIntroState = state as PhaseIntroState;
                phaseIntroState.nextStateId = ServiceLocator.game.phaseNavigator.findNextStateId( game_state_id.QUEST ) } );
            ServiceLocator.game.stateMachine.request( game_state_id.PHASE_INTRO );
        }
        else
        {
            this._stateMachine.request( ServiceLocator.game.phaseNavigator.findNextStateId( this._stateMachine.currentStateId, game_state_id.QUEST ) );
        }
    }

    // #endregion //
}