import State from "../../../lib/state_machine/State";

import { encounter_state_id, game_state_id, quest_state_id } from "../../StateId";
import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";

import StateMachine from "../../../lib/StateMachine";
import { IOpponentAction, IOppActionListener } from "../../../game/AllyActionManager";
import { player_type } from "../../../game/component/world/CPlayerArea";
import PhaseIntroState from "../PhaseIntroState";
import GameObject from "../../../game/GameObject";
import Signal from "../../../lib/signals/Signal";
import Session from "../../../Session";
import CEndTurnButton, { end_turn_btn_state } from "../../../game/component/ui/CEndTurnButton";
import CIsolatedArea from "../../../game/component/world/custom_area/CIsolatedArea";


export default class EngagementChecksState extends State implements IOppActionListener
{
    // #region Attributes // 

    // private:

    // Signals.
    private _onCompleted: Signal = new Signal();

    // #endregion //


    // #region Properties //

    // Signals.
    public get onCompleted(): Signal { return this._onCompleted; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        super.onEnter();

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setEncounterSubphase( encounter_state_id.ENGAGEMENT_CHECKS );

        const kEndTurnButtonText: string = ServiceLocator.game.phaseNavigator.findNexEndTurnButtonI18n( encounter_state_id.ENGAGEMENT_CHECKS, game_state_id.ENCOUNTER );
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cEndTurnButton.setText( kEndTurnButtonText );
        if ( Session.allyId )
        {
            ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.setText( kEndTurnButtonText );
        }

        // Perform engagement checks.
        let isAllyIsolated: boolean = Utils.game.isAllyIsolated();
        //
        let maxPlayerThreatLevel: number = Math.max( 
            ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.threatLevel.cTokenIndicator.trigger.cTokenCounter.count,
            ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.threatLevel.cTokenIndicator.trigger.cTokenCounter.count );
        let arrEngaged: Array<GameObject> = new Array<GameObject>();
        let arrActor: Array<GameObject> = ServiceLocator.game.cGameWorld.cSauronArea.cStaging.findAllActors();
        for ( let actor of arrActor )
        {
            if ( actor.cCardToken.curSide.cEnemySide && actor.cCardToken.curSide.cEnemySide.engagementCost.cTokenCounter.count <= maxPlayerThreatLevel )
            {
                let insertIndex: number = 0;
                for ( let enemyToken of arrEngaged )
                {
                    if ( actor.cCardToken.curSide.cEnemySide.engagementCost.cTokenCounter.count > enemyToken.cCardToken.curSide.cEnemySide.engagementCost.cTokenCounter.count )
                    {
                        break;
                    }
                    else
                    {
                        ++insertIndex;
                    }
                }
                arrEngaged.splice( insertIndex, 0, actor );
            }
        }
        if ( arrEngaged.length > 0 )
        {
            let arrPlayerEngaged: Array<GameObject> = new Array<GameObject>();
            let arrAllyEngaged: Array<GameObject> = new Array<GameObject>();
            let isFirstPlayer: boolean = ServiceLocator.game.firstPlayer == player_type.PLAYER || isAllyIsolated;
            while ( arrEngaged.length > 0 )
            {
                let enemy: GameObject = null;
                if ( isFirstPlayer )
                {
                    enemy = this.findEngagedEnemy( ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.threatLevel.cTokenIndicator.trigger.cTokenCounter.count, arrEngaged );
                    if ( enemy )
                    {
                        arrPlayerEngaged.push( enemy );
                    }
                }
                else
                {
                    enemy = this.findEngagedEnemy( ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.threatLevel.cTokenIndicator.trigger.cTokenCounter.count, arrEngaged );
                    if ( enemy )
                    {
                        arrAllyEngaged.push( enemy );
                    }
                }

                if ( enemy )
                {
                    arrEngaged.splice( arrEngaged.indexOf( enemy ), 1 );
                }

                if ( Session.allyId && !isAllyIsolated )
                {
                    isFirstPlayer = !isFirstPlayer;
                }
            }

            if ( arrPlayerEngaged.length > 0 )
            {
                ServiceLocator.game.cGameWorld.cPlayerArea.cEngaged.appendActors( arrPlayerEngaged.slice() );
            }
            if ( arrAllyEngaged.length > 0 )
            {
                ServiceLocator.game.cGameWorld.cAllyArea.cEngaged.appendActors( arrAllyEngaged.slice() );
            }
            if ( arrPlayerEngaged.length > 0 || arrAllyEngaged.length > 0 )
            {
                ServiceLocator.game.cGameWorld.cSauronArea.cStaging.sort();
            }
        }

        if ( isAllyIsolated )
        {
            arrEngaged = new Array<GameObject>();
            let cIsolatedArea: CIsolatedArea = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "isolated" ).cIsolatedArea;
            arrActor = cIsolatedArea.cStaging.findAllActors();
            for ( let actor of arrActor )
            {
                if ( actor.cCardToken.curSide.cEnemySide && actor.cCardToken.curSide.cEnemySide.engagementCost.cTokenCounter.count <= ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.threatLevel.cTokenIndicator.trigger.cTokenCounter.count )
                {
                    let insertIndex: number = 0;
                    for ( let enemyToken of arrEngaged )
                    {
                        if ( actor.cCardToken.curSide.cEnemySide.engagementCost.cTokenCounter.count > enemyToken.cCardToken.curSide.cEnemySide.engagementCost.cTokenCounter.count )
                        {
                            break;
                        }
                        else
                        {
                            ++insertIndex;
                        }
                    }
                    arrEngaged.splice( insertIndex, 0, actor );
                }
            }
            if ( arrEngaged.length > 0 )
            {
                cIsolatedArea.cEngaged.appendActors( arrEngaged.slice() );
            }
            if ( arrEngaged.length > 0 )
            {
                cIsolatedArea.cStaging.sort();
            }
        }

        // Listen to events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.add( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );
    }

        private findEngagedEnemy( threatLevel: number, arrEnemy: Array<GameObject> ): GameObject
        {
            let result: GameObject = null;

            for ( let enemy of arrEnemy )
            {
                if ( threatLevel >= enemy.cCardToken.curSide.cEnemySide.engagementCost.cTokenCounter.count )
                {
                    result = enemy;
                    break;
                }
            }

            return result;
        }

    public onLeave(): void
    {
        // Cleanup events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.remove( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );

        super.onLeave();
    }

    // overrides.
    public onLoad( sgState: any, pass: number ): void
    {
        super.onLoad( sgState, pass );

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setEncounterSubphase( encounter_state_id.ENGAGEMENT_CHECKS );

        // Listen to events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.addOnce( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.SET_END_TURN_BTN_STATE
            && action.args[ 0 ] as end_turn_btn_state == end_turn_btn_state.COMPLETED )
        {
            if ( ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn.state == end_turn_btn_state.COMPLETED )
            {
                this._onCompleted.dispatch();
            }
        }
    }

    // #endregion //


    // #region Callbacks //

    private onEndTurnBtn_Click(): void
    {
        if ( !Session.allyId )
        {
            this._onCompleted.dispatch();
        }
        else
        {
            let cEndTurnBtn: CEndTurnButton = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn;
            cEndTurnBtn.toggleCompleted( action_scope_type.MULTIPLAYER );
            if ( cEndTurnBtn.state == end_turn_btn_state.COMPLETED 
                && ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.state == end_turn_btn_state.COMPLETED )
            {        
                this._onCompleted.dispatch();
            }
        }
    }

    // #endregion //
}