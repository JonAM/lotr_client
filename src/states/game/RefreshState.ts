import State from "../../lib/state_machine/State";

import { game_state_id } from "../StateId";
import { action_scope_type, player_action_type } from "../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../ServiceLocator";

import StateMachine from "../../lib/StateMachine";
import { IOpponentAction, IOppActionListener } from "../../game/AllyActionManager";
import { player_type } from "../../game/component/world/CPlayerArea";
import PhaseIntroState from "./PhaseIntroState";
import GameObject from "../../game/GameObject";
import Session from "../../Session";
import CEndTurnButton, { end_turn_btn_state } from "../../game/component/ui/CEndTurnButton";
import { detail_bar_icon_type } from "../../game/component/card/token/CDetailBar";
import { status_type } from "../../game/component/card/token/CCardTokenSide";
import CDropArea from "../../game/component/input/CDropArea";
import Utils from "../../Utils";
import CIsolatedArea from "../../game/component/world/custom_area/CIsolatedArea";
import CSplitSauronArea from "../../game/component/world/custom_area/CSplitSauronArea";


export default class RefreshState extends State implements IOppActionListener
{
    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        super.onEnter();

        ServiceLocator.audioManager.playMusic( "refresh_phase", { loop: false } );

        ServiceLocator.game.cGameWorld.cActionLogger.logPhaseStart( game_state_id.REFRESH );
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setGamePhase( game_state_id.REFRESH );

        if ( Session.allyId )
        {
            ServiceLocator.game.firstPlayer ^= 1;

            let firstPlayerPlayArea: GameObject = ServiceLocator.game.firstPlayer == player_type.PLAYER ? ServiceLocator.game.cGameWorld.playerArea : ServiceLocator.game.cGameWorld.allyArea;
            firstPlayerPlayArea.cPlayerArea.sideControls.cPlayerSideControls.go.cDropArea.forceDrop( ServiceLocator.game.cGameWorld.firstPlayerToken, null, action_scope_type.LOCAL );
        }
        
        ServiceLocator.game.setActivePlayer( ServiceLocator.game.firstPlayer );

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cEndTurnButton.setText( "END_ROUND" );
        if ( Session.allyId )
        {
            ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.setText( "END_ROUND" );
        }

        // Refresh actors.
        let arrActor: Array<GameObject> = ServiceLocator.game.cGameWorld.cPlayerArea.cHome.findAllActors();
        for ( let actor of arrActor )
        {
            this.refreshActor( actor );
        }
        arrActor = ServiceLocator.game.cGameWorld.cAllyArea.cHome.findAllActors();
        for ( let actor of arrActor )
        {
            this.refreshActor( actor );
        }
        arrActor = ServiceLocator.game.cGameWorld.cPlayerArea.cEngaged.findAllActors();
        for ( let actor of arrActor )
        {
            this.refreshActor( actor );
        }
        arrActor = ServiceLocator.game.cGameWorld.cAllyArea.cEngaged.findAllActors();
        for ( let actor of arrActor )
        {
            this.refreshActor( actor );
        }
        arrActor = ServiceLocator.game.cGameWorld.cSauronArea.cStaging.findAllActors();
        for ( let actor of arrActor )
        {
            this.refreshActor( actor );
        }
        if ( Session.allyId && Utils.game.isAllyIsolated() )
        {
            let cIsolatedArea: CIsolatedArea = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "isolated" ).cIsolatedArea;
            arrActor = cIsolatedArea.cHome.findAllActors();
            for ( let actor of arrActor )
            {
                this.refreshActor( actor );
            }
            arrActor = cIsolatedArea.cEngaged.findAllActors();
            for ( let actor of arrActor )
            {
                this.refreshActor( actor );
            }
            arrActor = cIsolatedArea.cStaging.findAllActors();
            for ( let actor of arrActor )
            {
                this.refreshActor( actor );
            }
        }
        if ( Utils.game.isMainStagingAreaIsolated() )
        {
            let cSplitSauronArea: CSplitSauronArea = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "split_sauron" ).cSplitSauronArea;
            arrActor = cSplitSauronArea.cStaging.findAllActors();
            for ( let actor of arrActor )
            {
                this.refreshActor( actor );
            }
        }

        // Refresh game modifiers.
        let arrGameModifier: Array<GameObject> = ServiceLocator.game.cGameWorld.cSauronArea.cStaging.findGameModifiers();
        arrGameModifier = arrGameModifier.concat( ServiceLocator.game.cGameWorld.cPlayerArea.cHome.findGameModifiers() );
        arrGameModifier = arrGameModifier.concat( ServiceLocator.game.cGameWorld.cAllyArea.cHome.findGameModifiers() );
        for ( let gameModifier of arrGameModifier )
        {
            gameModifier.cGameModifier.setBowed( false, action_scope_type.LOCAL );
            gameModifier.cGameModifier.cDetailBar.removeItem( detail_bar_icon_type.ABILITY_USES );
        }

        // Increase players' threat level.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.threatLevel.cTokenIndicator.trigger.cTokenCounter.addCount( 1, action_scope_type.LOCAL );
        if ( Session.allyId )
        {
            ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.threatLevel.cTokenIndicator.trigger.cTokenCounter.addCount( 1, action_scope_type.LOCAL );
        }
        
        // Listen to events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.add( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );
    }

        private refreshActor( cardToken: GameObject ): void
        {
            cardToken.cCardToken.setBowed( false, action_scope_type.LOCAL );
            cardToken.cCardToken.cCurSide.removeStatus( status_type.HAS_ATTACKED, action_scope_type.LOCAL );
            cardToken.cCardToken.cDetailBar.removeItem( detail_bar_icon_type.ABILITY_USES );

            let arrAttachment: Array<GameObject> = cardToken.cCardToken.attachmentHolder.cScrollY.findItems();
            for ( let attachment of arrAttachment )
            {
                attachment.cAttachment.setBowed( false, action_scope_type.LOCAL );
                attachment.cAttachment.cDetailBar.removeItem( detail_bar_icon_type.ABILITY_USES );
            }
        } 

    public onLeave(): void
    {
        ServiceLocator.audioManager.stopMusic();

        // Cleanup events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.remove( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );

        super.onLeave();
    }

    // overrides.
    public onLoad( sgState: any, pass: number ): void
    {
        super.onLoad( sgState, pass );

        ServiceLocator.audioManager.playMusic( "refresh_phase", { loop: false } );

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setGamePhase( game_state_id.REFRESH );

        // Listen to events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.addOnce( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.SET_END_TURN_BTN_STATE
            && action.args[ 0 ] as end_turn_btn_state == end_turn_btn_state.COMPLETED )
        {
            if ( ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn.state == end_turn_btn_state.COMPLETED )
            {
                ServiceLocator.game.stateMachine.request( game_state_id.ROUND_INTRO );
            }
        }
    }

    // #endregion //


    // #region Callbacks //

    private onEndTurnBtn_Click(): void
    {
        if ( !Session.allyId )
        {
            ServiceLocator.game.stateMachine.request( game_state_id.ROUND_INTRO );
        }
        else
        {
            let cEndTurnBtn: CEndTurnButton = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn;
            cEndTurnBtn.toggleCompleted( action_scope_type.MULTIPLAYER );
            if ( cEndTurnBtn.state == end_turn_btn_state.COMPLETED 
                && ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.state == end_turn_btn_state.COMPLETED )
            {       
                ServiceLocator.game.stateMachine.request( game_state_id.ROUND_INTRO );
            }
        }
    }

    // #endregion //
}