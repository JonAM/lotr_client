import State from "../../lib/state_machine/State";

import ServiceLocator from "../../ServiceLocator";
import { gsap, TimelineMax, Sine, Back } from "gsap";
import * as PIXI from "pixi.js";

import GameObject from "../../game/GameObject";
import Signal from "../../lib/signals/Signal";
import CGraphics from "../../game/component/pixi/CGraphics";
import CContainer from "../../game/component/pixi/CContainer";
import Utils from "../../Utils";
import { IStateData } from "../../game/component/world/card_activation_area/CardActivationController";
import { action_scope_type, player_action_type } from "../../service/socket_io/GameSocketIOController";
import CTooltipReceptor from "../../game/component/ui/CTooltipReceptor";
import CCardPresentationControls from "../../game/component/ui/CCardPresentationControls";
import CCardPresentationDefaultAction from "../../game/component/ui/CCardPresentationDefaultAction";
import { player_type } from "../../game/component/world/CPlayerArea";
import LogTargetCard from "../../game/component/ui/right_menu/action_log/target/LogTargetCard";
import { location_type } from "../../game/component/world/CGameWorld";
import CardPresentationUtils, { button_type } from "./setup/CardPresentationUtils";
import { layer_type } from "../../game/component/world/CGameLayerProvider";
import FloatingMessage from "../../game/FloatingMessage";
import { input_event_type } from "../../game/InputController";
import PointerInputController from "../../game/input/PointerInputController";
import TouchInputController from "../../game/input/TouchInputController";


export default class CardPresentationState extends State
{
    // #region Attributes //

    // private:
    
    private _data: IStateData = null;
    private _from: location_type = null;
    private _arrButtonType: Array<button_type> = null;

    private _defaultAction: GameObject = null;
    private _backPlane: PIXI.SimplePlane = null;
    private _frontPlane: PIXI.SimplePlane = null;
    private _curPlane: PIXI.SimplePlane = null;
    private _buttonRow: GameObject = null;

    // Signals.
    private _onCompleted: Signal = new Signal();

    // #endregion //

    
    // #region Properties //

    public set data( value: IStateData ) { this._data = value; }
    public set from( value: location_type ) { this._from = value; }
    public set buttonTypes( value: Array<button_type> ) { this._arrButtonType = value; }

    // Signals.
    public get onCompleted(): Signal { return this._onCompleted; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        console.assert( this._data != null, "CardPresentationState.ts :: onEnter() :: this._data cannot be null." );

        super.onEnter();

        ServiceLocator.game.keyBindingManager.setEnabled( false );

        this._data.disablerBg = new GameObject( [ new CGraphics() ] );
        this._data.disablerBg.init();
        this._data.disablerBg.cGraphics.g.interactive = true;
        this._data.disablerBg.cGraphics.g.beginFill( 0x000000 );
        this._data.disablerBg.cGraphics.g.drawRect( 0, 0, ServiceLocator.game.app.screen.width, ServiceLocator.game.app.screen.height );
        this._data.disablerBg.cGraphics.g.endFill();
        ServiceLocator.game.root.cGameLayerProvider.add( this._data.disablerBg, layer_type.CARD_PRESENTATION );

        this._data.planeContainer = new GameObject( [ new CContainer() ] );
        this._data.planeContainer.cContainer.c.interactive = true;
        this._data.planeContainer.cContainer.c.buttonMode = true;
        this._data.planeContainer.init();
        //
        this._backPlane = this.createCardPlane( Utils.img.findCardTexture( this._data.card.cCard.back.cardId ) );
        this._backPlane.visible = !this._data.card.cCard.isFaceUp;
        this._data.planeContainer.cContainer.c.addChild( this._backPlane );
        //
        this._frontPlane = this.createCardPlane( Utils.img.findCardTexture( this._data.card.cCard.front.cardId ) );
        this._frontPlane.visible = this._data.card.cCard.isFaceUp;
        this._data.planeContainer.cContainer.c.addChild( this._frontPlane );
        //
        this._curPlane = this._backPlane.visible ? this._backPlane : this._frontPlane;
        //
        this._defaultAction = new GameObject( [ new CContainer(), new CCardPresentationDefaultAction(), new CTooltipReceptor() ] );
        this._defaultAction.cTooltipReceptor.text = "";
        this._defaultAction.init();
        this._defaultAction.cContainer.c.visible = false;
        this._data.planeContainer.cContainer.addChild( this._defaultAction );
        //
        this._data.planeContainer.cContainer.c.position.set( ServiceLocator.game.app.screen.width * 0.5, ServiceLocator.game.app.screen.height * 0.5 );
        ServiceLocator.game.root.cGameLayerProvider.add( this._data.planeContainer, layer_type.CARD_PRESENTATION );

        this._buttonRow = new GameObject( [ new CContainer(), new CCardPresentationControls() ] );
        this._buttonRow.init();
        this._buttonRow.cContainer.c.visible = false;
        this._buttonRow.cContainer.c.position.set( 
            this._data.planeContainer.cContainer.c.x + this._data.planeContainer.cContainer.c.width * 1.5 * 0.5 + 10,
            this._data.planeContainer.cContainer.c.y - this._data.planeContainer.cContainer.c.height * 1.5 * 0.5 );
        ServiceLocator.game.root.cGameLayerProvider.add( this._buttonRow, layer_type.CARD_PRESENTATION );
        this._buttonRow.cCardPresentationControls.onButtonClick.add( this.onButtonRow_Click, this );

        if ( this._data.card.cCard.location == location_type.MY_UNDERNEATH || this._data.card.cCard.location == location_type.ALLY_UNDERNEATH )
        {
            Utils.game.showActor( GameObject.find( this._data.card.cCard.locationData ) );
        }

        this.animateIn();
    }
    
        private createCardPlane( texture: PIXI.Texture ): PIXI.SimplePlane
        {
            let cardPlane: PIXI.SimplePlane = null;

            const kVerticesX: number = 3;
            const kVerticesY: number = 3;
            cardPlane = new PIXI.SimplePlane( texture, kVerticesX, kVerticesY );
            cardPlane.pivot.set( cardPlane.width * 0.5, cardPlane.height * 0.5 );

            return cardPlane;
        }

    public onLeave(): void
    {
        this._onCompleted.removeAll();

        ServiceLocator.game.app.stage.off( "pointermove", this.onStage_PointerMove, this );

        this._buttonRow.end();
        this._buttonRow = null;

        this._defaultAction.end();
        this._defaultAction = null;
        
        ServiceLocator.game.keyBindingManager.setEnabled( true );

        super.onLeave();
    }

    // private:
    
    private animateIn(): void
    {
        let tl: TimelineMax = new TimelineMax();
        tl.addLabel( "in", 0 );
        this._data.disablerBg.cContainer.c.alpha = 0;
        tl.to( this._data.disablerBg.cContainer.c, { alpha: 0.6, ease: Sine.easeOut, duration: 0.25 }, "in" );
        if ( this._from == null )
        {
            // Manual activation.
            this._data.planeContainer.cContainer.c.alpha = 0.25;
            tl.to( this._data.planeContainer.cContainer.c, { alpha: 1, ease: Sine.easeIn, duration: 0.5 }, "in" );
            this._data.planeContainer.cContainer.c.scale.set( 2.25 );
            tl.to( this._data.planeContainer.cContainer.c.scale, { x: 1.5, y: 1.5, ease: Back.easeOut, duration: 1 }, "in" );
        }
        else
        {
            // Forced activation.
            let deckIndicator: GameObject = null;
            switch ( this._from )
            {
                case location_type.MY_DECK: { deckIndicator = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.deck; break; }
                case location_type.SAURON_DECK_0: { deckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.deck; break; }
                case location_type.SET_ASIDE: { deckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.setAside; break; }
                case location_type.QUEST_DECK: { deckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.questDeck; break; }

                // TODO:
            }
            const kScale: number = deckIndicator.cContainer.c.height / this._data.planeContainer.cContainer.c.getLocalBounds().height;
            this._data.planeContainer.cContainer.c.scale.set( kScale );
            const kDeckIndicatorGlobal: PIXI.Point = deckIndicator.cContainer.c.getGlobalPosition();
            tl.from( this._data.planeContainer.cContainer.c, { 
                    x: kDeckIndicatorGlobal.x,
                    y: kDeckIndicatorGlobal.y + deckIndicator.cContainer.c.height * 0.5, 
                    ease: Sine.easeOut, duration: 1 }, 
                "in" );
            //
            tl.to( this._data.planeContainer.cContainer.c.scale, { x: 1.5, y: 1.5, ease: Sine.easeOut, duration: 1 }, "in" );
        }
        tl.call( this.onAnimateIn_Completed.bind( this ), null );
    }

    // #endregion //


    // #region Input Callbacks //

    private onStage_PointerMove(): void
    {
        const kGlobal: PIXI.Point = ServiceLocator.game.app.renderer.plugins.interaction.mouse.global;
        if ( this._data.planeContainer.cContainer.c.getBounds().contains( kGlobal.x, kGlobal.y ) )
        {
            this._defaultAction.cContainer.c.visible = true;
        }
        else
        {
            this._defaultAction.cContainer.c.visible = false;
        }
    }

    private onCard_PointerTap(): void
    {
        if ( this._defaultAction.cCardPresentationDefaultAction.buttonType == button_type.FLIP )
        {
            ServiceLocator.game.app.stage.off( "pointermove", this.onStage_PointerMove, this );
            this._defaultAction.cContainer.c.visible = false;

            const kArrLandscapeTypeCode: Array<string> = [ "player-side-quest", "encounter-side-quest" ];
            if ( kArrLandscapeTypeCode.indexOf( this._data.card.cCard.curSide.cardInfo.type_code ) != -1 && kArrLandscapeTypeCode.indexOf( this._data.card.cCard.otherSide.cardInfo.type_code ) == -1
                || kArrLandscapeTypeCode.indexOf( this._data.card.cCard.curSide.cardInfo.type_code ) == -1 && kArrLandscapeTypeCode.indexOf( this._data.card.cCard.otherSide.cardInfo.type_code ) != -1 )
            {
                this._curPlane.rotation = Math.PI * 0.5;

                this._buttonRow.cContainer.c.position.set( 
                    this._data.planeContainer.cContainer.c.x + this._data.planeContainer.cContainer.c.width * 0.5 + 10,
                    this._data.planeContainer.cContainer.c.y - this._data.planeContainer.cContainer.c.height * 0.5 );
    
            }
            this.flipCard();

            // Multiplayer.
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.CARD_ACTIVATION_FLIP, null, null );
        }
        else
        {
            this.onButtonRow_Click( this._defaultAction.cCardPresentationDefaultAction.buttonType );
        }
    }

        private flipCard(): void
        {
            let buffer: PIXI.Buffer = this._curPlane.geometry.getBuffer( "aVertexPosition" );
            let mapVertexIndex: Map<string, number> = this.createVertexIndexMap( this._curPlane.rotation != 0 );
            let obj: Object = { 
                ltx: buffer.data[ mapVertexIndex.get( "ltx" ) ], lty: buffer.data[ mapVertexIndex.get( "lty" ) ], 
                rtx: buffer.data[ mapVertexIndex.get( "rtx" ) ], rty: buffer.data[ mapVertexIndex.get( "rty" ) ], 
                lmx: buffer.data[ mapVertexIndex.get( "lmx" ) ], lmy: buffer.data[ mapVertexIndex.get( "lmy" ) ], 
                rmx: buffer.data[ mapVertexIndex.get( "rmx" ) ], rmy: buffer.data[ mapVertexIndex.get( "rmy" ) ], 
                lbx: buffer.data[ mapVertexIndex.get( "lbx" ) ], lby: buffer.data[ mapVertexIndex.get( "lby" ) ], 
                rbx: buffer.data[ mapVertexIndex.get( "rbx" ) ], rby: buffer.data[ mapVertexIndex.get( "rby" ) ] };

            const kVerticalDeformation: number = 100;
            let tweenVars: gsap.TweenVars = {
                duration: 0.25,
                ease: Sine.easeIn,
                onUpdate: this.updateBuffer,
                onUpdateParams: [ buffer, obj, mapVertexIndex ],
                onComplete: this.playBackSideAnim, 
                onCompleteParams: [ obj ],
                callbackScope: this }
            if ( this._curPlane.rotation == 0 )
            {
                tweenVars.ltx = buffer.data[ mapVertexIndex.get( "ltx" ) ] + this._curPlane.width * 0.5;
                tweenVars.lty = buffer.data[ mapVertexIndex.get( "lty" ) ] - kVerticalDeformation;
                tweenVars.rtx = buffer.data[ mapVertexIndex.get( "rtx" ) ] - this._curPlane.width * 0.5; 
                tweenVars.rty = buffer.data[ mapVertexIndex.get( "rty" ) ] + kVerticalDeformation;
                tweenVars.lmx = buffer.data[ mapVertexIndex.get( "lmx" ) ] + this._curPlane.width * 0.5; 
                tweenVars.rmx = buffer.data[ mapVertexIndex.get( "rmx" ) ] - this._curPlane.width * 0.5; 
                tweenVars.lbx = buffer.data[ mapVertexIndex.get( "lbx" ) ] + this._curPlane.width * 0.5; 
                tweenVars.lby = buffer.data[ mapVertexIndex.get( "lby" ) ] + kVerticalDeformation;
                tweenVars.rbx = buffer.data[ mapVertexIndex.get( "rbx" ) ] - this._curPlane.width * 0.5;
                tweenVars.rby = buffer.data[ mapVertexIndex.get( "rby" ) ] - kVerticalDeformation;
            }
            else
            {
                tweenVars.lty = buffer.data[ mapVertexIndex.get( "lty" ) ] - this._curPlane.height * 0.5; 
                tweenVars.ltx = buffer.data[ mapVertexIndex.get( "ltx" ) ] - kVerticalDeformation;
                tweenVars.rty = buffer.data[ mapVertexIndex.get( "rty" ) ] + this._curPlane.height * 0.5; 
                tweenVars.rtx = buffer.data[ mapVertexIndex.get( "rtx" ) ] + kVerticalDeformation;
                tweenVars.lmy = buffer.data[ mapVertexIndex.get( "lmy" ) ] - this._curPlane.height * 0.5; 
                tweenVars.rmy = buffer.data[ mapVertexIndex.get( "rmy" ) ] + this._curPlane.height * 0.5; 
                tweenVars.lby = buffer.data[ mapVertexIndex.get( "lby" ) ] - this._curPlane.height * 0.5; 
                tweenVars.lbx = buffer.data[ mapVertexIndex.get( "lbx" ) ] + kVerticalDeformation;
                tweenVars.rby = buffer.data[ mapVertexIndex.get( "rby" ) ] + this._curPlane.height * 0.5; 
                tweenVars.rbx = buffer.data[ mapVertexIndex.get( "rbx" ) ] - kVerticalDeformation;
            }
            gsap.to( obj, tweenVars );
        }

            private createVertexIndexMap( isLandscape: boolean ): Map<string, number>
            {
                let result: Map<string, number> = null;

                if ( !isLandscape )
                {
                    result = new Map<string, number>( [ 
                        [ "ltx", 0 ], [ "lty", 1 ], [ "rtx", 4 ], [ "rty", 5 ],
                        [ "lmx", 6 ], [ "lmy", 7 ], [ "rmx", 10 ], [ "rmy", 11 ],
                        [ "lbx", 12 ], [ "lby", 13 ], [ "rbx", 16 ], [ "rby", 17 ] ] );
                }
                else
                {
                    result = new Map<string, number>( [ 
                        [ "ltx", 12 ], [ "lty", 13 ], [ "rtx", 0 ], [ "rty", 1 ],
                        [ "lmx", 14 ], [ "lmy", 15 ], [ "rmx", 2 ], [ "rmy", 3 ],
                        [ "lbx", 16 ], [ "lby", 17 ], [ "rbx", 4 ], [ "rby", 5 ] ] );
                }

                return result;
            }

            private updateBuffer( buffer: PIXI.Buffer, obj: Object, mapVertexIndex: Map<string, number> ): void
            {
                buffer.data[ mapVertexIndex.get( "ltx" ) ] = obj[ "ltx" ];
                buffer.data[ mapVertexIndex.get( "lty" ) ] = obj[ "lty" ];
                buffer.data[ mapVertexIndex.get( "rtx" ) ] = obj[ "rtx" ];
                buffer.data[ mapVertexIndex.get( "rty" ) ] = obj[ "rty" ];
                buffer.data[ mapVertexIndex.get( "lmx" ) ] = obj[ "lmx" ];
                buffer.data[ mapVertexIndex.get( "lmy" ) ] = obj[ "lmy" ];
                buffer.data[ mapVertexIndex.get( "rmx" ) ] = obj[ "rmx" ];
                buffer.data[ mapVertexIndex.get( "rmy" ) ] = obj[ "rmy" ];
                buffer.data[ mapVertexIndex.get( "lbx" ) ] = obj[ "lbx" ];
                buffer.data[ mapVertexIndex.get( "lby" ) ] = obj[ "lby" ];
                buffer.data[ mapVertexIndex.get( "rbx" ) ] = obj[ "rbx" ];
                buffer.data[ mapVertexIndex.get( "rby" ) ] = obj[ "rby" ];
                buffer.update();
            }

        private playBackSideAnim( obj: Object ): void 
        {
            this._curPlane.visible = false;
            let isRotated: boolean = this._curPlane.rotation != 0;
            this._curPlane = this._curPlane == this._frontPlane ? this._backPlane : this._frontPlane;
            this._curPlane.visible = true;

            let buffer: PIXI.Buffer = this._curPlane.geometry.getBuffer( "aVertexPosition" );
            let mapVertexIndex: Map<string, number> = this.createVertexIndexMap( this._curPlane.rotation != 0 );
            let obji: Object = { 
                ltx: buffer.data[ mapVertexIndex.get( "ltx" ) ], lty: buffer.data[ mapVertexIndex.get( "lty" ) ], 
                rtx: buffer.data[ mapVertexIndex.get( "rtx" ) ], rty: buffer.data[ mapVertexIndex.get( "rty" ) ], 
                lmx: buffer.data[ mapVertexIndex.get( "lmx" ) ], lmy: buffer.data[ mapVertexIndex.get( "lmy" ) ], 
                rmx: buffer.data[ mapVertexIndex.get( "rmx" ) ], rmy: buffer.data[ mapVertexIndex.get( "rmy" ) ], 
                lbx: buffer.data[ mapVertexIndex.get( "lbx" ) ], lby: buffer.data[ mapVertexIndex.get( "lby" ) ], 
                rbx: buffer.data[ mapVertexIndex.get( "rbx" ) ], rby: buffer.data[ mapVertexIndex.get( "rby" ) ] };
            //
            buffer.data[ 0 ] = obj[ isRotated ? "rty" : "rtx" ];
            buffer.data[ 1 ] = obj[ isRotated ? "rtx" : "rty" ];
            buffer.data[ 4 ] = obj[ isRotated ? "lty" : "ltx" ];
            buffer.data[ 5 ] = obj[ isRotated ? "ltx" : "lty" ];
            buffer.data[ 6 ] = obj[ isRotated ? "rmy" : "rmx" ];
            buffer.data[ 10 ] = obj[ isRotated ? "lmy" : "lmx" ];
            buffer.data[ 12 ] = obj[ isRotated ? "rby" : "rbx" ];
            buffer.data[ 13 ] = obj[ isRotated ? "rbx" : "rby" ];
            buffer.data[ 16 ] = obj[ isRotated ? "lby" : "lbx" ];
            buffer.data[ 17 ] = obj[ isRotated ? "lbx" : "lby" ];
            buffer.update();

            obj = { 
                ltx: buffer.data[ 0 ], lty: buffer.data[ 1 ], 
                rtx: buffer.data[ 4 ], rty: buffer.data[ 5 ], 
                lmx: buffer.data[ 6 ], lmy: buffer.data[ 7 ], 
                rmx: buffer.data[ 10 ], rmy: buffer.data[ 11 ], 
                lbx: buffer.data[ 12 ], lby: buffer.data[ 13 ], 
                rbx: buffer.data[ 16 ], rby: buffer.data[ 17 ] };

            gsap.to( obj, { 
                ltx: obji[ "ltx" ], 
                lty: obji[ "lty" ], 
                rtx: obji[ "rtx" ], 
                rty: obji[ "rty" ], 
                lmx: obji[ "lmx" ], 
                rmx: obji[ "rmx" ], 
                lbx: obji[ "lbx" ], 
                lby: obji[ "lby" ], 
                rbx: obji[ "rbx" ], 
                rby: obji[ "rby" ], 
                duration: 0.25, 
                onUpdate: this.updateBuffer, 
                onUpdateParams: [ buffer, obj, mapVertexIndex ],
                onComplete: this.onFlipCardAnim_Completed, 
                callbackScope: this,
                ease: Sine.easeOut });
        }

    private onButtonRow_Click( buttonRowType: button_type ): void
    {
        let isValid: boolean = true;
        if ( buttonRowType == button_type.PLAY )
        {
            switch ( this._data.card.cCard.curSide.cardInfo.type_code )
            {
                case "hero":
                case "ally": 
                case "event":
                case "attachment":
                case "treasure": { isValid = ServiceLocator.game.cGameWorld.cPlayerArea.cHome.findInsertTabIndex( 130 + ServiceLocator.game.cGameWorld.cPlayerArea.cHome.actorMargin ) != -1; break; }
                
                case "enemy":
                case "ship-enemy":
                case "location":
                case "treachery": 
                case "objective": 
                case "objective-ally": 
                case "objective-hero": 
                case "objective-location": 
                case "ship-objective": 
                case "player-side-quest": 
                case "encounter-side-quest": { isValid = ServiceLocator.game.cGameWorld.cSauronArea.cStaging.findInsertTabIndex( 130 + ServiceLocator.game.cGameWorld.cSauronArea.cStaging.actorMargin ) != -1; break; }
            }
 
            if ( !isValid )
            {
                let floatingMessage: FloatingMessage = new FloatingMessage();
                floatingMessage.show( jQuery.i18n( "NOT_ENOUGH_SPACE" ), ServiceLocator.game.app.renderer.plugins.interaction.mouse.global );
            }
        }

        if ( isValid )
        {
            this._buttonRow.cContainer.c.visible = false;
            
            this._data.buttonRowType = buttonRowType;

            this._onCompleted.dispatch( buttonRowType );
        }
    }

    // #endregion //


    // #region Other Callbacks //

    private onAnimateIn_Completed(): void
    {
        this.updateCardContextControls();
        this._buttonRow.cContainer.c.visible = true;

        ServiceLocator.game.inputController.once( this._data.planeContainer.cContainer.c, input_event_type.TAP, this.onCard_PointerTap, this );
        if ( ServiceLocator.game.inputController instanceof PointerInputController )
        {
            ServiceLocator.game.app.stage.on( "pointermove", this.onStage_PointerMove, this );
            this.onStage_PointerMove();
        }
        else if ( ServiceLocator.game.inputController instanceof TouchInputController )
        {    
            this._defaultAction.cContainer.c.visible = true;
        }
    }

        private updateCardContextControls(): void
        {
            let cpu: CardPresentationUtils = new CardPresentationUtils();
            let arrButtonType: Array<button_type> = cpu.findContextControls( this._data.card );
            //
            this._defaultAction.cCardPresentationDefaultAction.updateIcon( arrButtonType.shift() );
            //
            if ( this._arrButtonType )
            {
                arrButtonType = this._arrButtonType;
            }
            this._buttonRow.cCardPresentationControls.draw( arrButtonType );
        }

    private onFlipCardAnim_Completed(): void
    {
        // Log.
        ServiceLocator.game.cGameWorld.cActionLogger.logCardFlip( player_type.PLAYER, new LogTargetCard( this._data.card ), false );

        this._data.card.cCard.isAnimated = false;
        if ( this._curPlane == this._frontPlane )
        {
            this._data.card.cCard.setFaceUp( action_scope_type.LOCAL );
        }
        else
        {
            this._data.card.cCard.setFaceDown( action_scope_type.LOCAL );
        }
        this._data.card.cCard.isAnimated = true;

        this.updateCardContextControls();
        
        ServiceLocator.game.inputController.once( this._data.planeContainer.cContainer.c, input_event_type.TAP, this.onCard_PointerTap, this );
        if ( ServiceLocator.game.inputController instanceof PointerInputController )
        {
            ServiceLocator.game.app.stage.on( "pointermove", this.onStage_PointerMove, this );
            this.onStage_PointerMove();
        }
        else if ( ServiceLocator.game.inputController instanceof TouchInputController )
        {    
            this._defaultAction.cContainer.c.visible = true;
        }
    }

    // #endregion //
}