import State from "../../lib/state_machine/State";

import { game_state_id } from "../StateId";
import ServiceLocator from "../../ServiceLocator";
import { TimelineMax, Sine, Back } from "gsap";
import * as PIXI from "pixi.js";

import GameObject from "../../game/GameObject";
import CGraphics from "../../game/component/pixi/CGraphics";
import CContainer from "../../game/component/pixi/CContainer";
import { layer_type } from "../../game/component/world/CGameLayerProvider";


export default class PhaseIntroState extends State
{
    // #region Attributes //

    // private:

    private _nextStateId: game_state_id = null;

    private _disablerBg: GameObject = null;
    private _phaseAnnouncement: GameObject = null;

    // #endregion //


    // #region Properties //

    public set nextStateId( value: game_state_id ) { this._nextStateId = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        console.assert( this._nextStateId != null, "PhaseIntroState.ts :: onEnter() :: this._nextStateId cannot be null." );

        super.onEnter();

        if ( ServiceLocator.savedData.data.gamePreferences.isSkipPhaseIntro )
        {
            this.onAnimation_Completed();
        }
        else
        {
            // Sfx.
            ServiceLocator.audioManager.playSfx( "phase_started" );

            if ( ServiceLocator.game.isAnimated )
            {
                ServiceLocator.game.allyActionManager.pause();
            }

            this._disablerBg = new GameObject( [ new CGraphics() ] );
            this._disablerBg.init();
            this._disablerBg.cGraphics.g.interactive = true;
            this._disablerBg.cGraphics.g.beginFill( 0x000000 );
            this._disablerBg.cGraphics.g.drawRect( 0, 0, ServiceLocator.game.app.screen.width, ServiceLocator.game.app.screen.height );
            this._disablerBg.cGraphics.g.endFill();
            ServiceLocator.game.root.cGameLayerProvider.add( this._disablerBg, layer_type.DISABLED );

            this._phaseAnnouncement = this.createPhaseAnnouncement();
            ServiceLocator.game.root.cGameLayerProvider.add( this._phaseAnnouncement, layer_type.DISABLED );
            this._phaseAnnouncement.cContainer.c.position.set( ServiceLocator.game.app.screen.width * 0.5, ServiceLocator.game.app.screen.height * 0.5 );
            this.animatePhaseAnnouncement( this._phaseAnnouncement );
        }
    }

    public onLeave(): void
    {
        if ( !ServiceLocator.savedData.data.gamePreferences.isSkipPhaseIntro )
        {
            this._nextStateId = null;

            this._disablerBg.end();
            this._disablerBg = null;

            this._phaseAnnouncement.end();
            this._phaseAnnouncement = null;

            if ( ServiceLocator.game.isAnimated )
            {
                ServiceLocator.game.allyActionManager.resume();
            }
        }
    
        super.onLeave();
    }

    // private:

    private createPhaseAnnouncement(): GameObject
    {
        let result: GameObject = new GameObject( [ new CContainer() ] );
        result.init();

        let phaseLocaleId: string = null;
        switch ( this._nextStateId )
        {
            case game_state_id.SETUP: { phaseLocaleId = "SETUP_PHASE"; break; }
            case game_state_id.RESOURCE: { phaseLocaleId = "RESOURCE_PHASE"; break; }
            case game_state_id.PLANNING: { phaseLocaleId = "PLANNING_PHASE"; break; }
            case game_state_id.QUEST: { phaseLocaleId = "QUEST_PHASE"; break; }
            case game_state_id.TRAVEL: { phaseLocaleId = "TRAVEL_PHASE"; break; }
            case game_state_id.ENCOUNTER: { phaseLocaleId = "ENCOUNTER_PHASE"; break; }
            case game_state_id.COMBAT: { phaseLocaleId = "COMBAT_PHASE"; break; }
            case game_state_id.REFRESH: { phaseLocaleId = "REFRESH_PHASE"; break; }
        }
        let phaseName: PIXI.Text = new PIXI.Text( jQuery.i18n( phaseLocaleId ), ServiceLocator.game.textStyler.introBig );
        //
        let bg: PIXI.Graphics = new PIXI.Graphics();
        bg.lineStyle( 1, 0x00000, 1 );
        bg.beginFill( 0xefe4b0, 1 );
        const kBgPadding: number = 20;
        bg.drawRoundedRect( -( phaseName.width * 0.5 + kBgPadding ), -( phaseName.height * 0.5 + kBgPadding ), phaseName.width + kBgPadding * 2, phaseName.height + kBgPadding * 2, 25 );
        bg.endFill();
        result.cContainer.c.addChild( bg );
        //
        phaseName.anchor.set( 0.5 );
        bg.addChild( phaseName );

        let torii: PIXI.Sprite = PIXI.Sprite.from( PIXI.Texture.from( ServiceLocator.resourceStack.find( "intro_phase" ).data ) );
        torii.anchor.set( 0.5, 1 );
        torii.position.y = -result.cContainer.c.height * 0.5;
        result.cContainer.c.addChild( torii );

        return result;
    }

    private animatePhaseAnnouncement( phaseAnnouncement: GameObject ): void
    {
        phaseAnnouncement.cContainer.c.scale.set( 0.25 );
        phaseAnnouncement.cContainer.c.alpha = 0.25;
        this._disablerBg.cContainer.c.alpha = 0;

        let tl: TimelineMax = new TimelineMax();
        tl.addLabel( "in", 0 );
        tl.addLabel( "out", 1.5 );
        tl.to( this._disablerBg.cContainer.c, 0.25, { alpha: 0.6, ease: Sine.easeOut }, "in" );
        tl.to( phaseAnnouncement.cContainer.c.scale, 1, { x: 1, y: 1, ease: Back.easeOut }, "in" );
        tl.to( phaseAnnouncement.cContainer.c, 0.5, { alpha: 1, ease: Sine.easeIn }, "in" );
        tl.to( phaseAnnouncement.cContainer.c, 0.5, { alpha: 0, ease: Sine.easeOut }, "out" );
        tl.to( this._disablerBg.cContainer.c, 0.5, { alpha: 0, ease: Sine.easeOut }, "out" );
        tl.to( phaseAnnouncement.cContainer.c.scale, 0.5, { x: 1.5, y: 1.5, ease: Sine.easeOut }, "out" );
        tl.call( this.onAnimation_Completed.bind( this ), null );
    }

    // #endregion //


    // #region Callbacks //

    private onAnimation_Completed(): void
    {
        ServiceLocator.game.stateMachine.request( this._nextStateId );
    }

    // #endregion //
}