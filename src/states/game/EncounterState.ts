import State from "../../lib/state_machine/State";

import { encounter_state_id, game_state_id } from "../StateId";
import { player_action_type } from "../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../ServiceLocator";

import StateMachine from "../../lib/StateMachine";
import { IOpponentAction, IOppActionListener } from "../../game/AllyActionManager";
import { player_type } from "../../game/component/world/CPlayerArea";
import PhaseIntroState from "./PhaseIntroState";
import GameObject from "../../game/GameObject";
import OptionalEngagementState from "./encounter/OptionalEngagementState";
import EngagementChecksState from "./encounter/EngagementChecksState";
import { ISgEncounterState } from "../../view/game/SaveGameView";
import Session from "../../Session";
import { end_turn_btn_state } from "../../game/component/ui/CEndTurnButton";


export default class EncounterState extends State implements IOppActionListener
{
    // #region Attributes //

    // private:

    private _stateMachine: StateMachine = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        super.onEnter();

        ServiceLocator.audioManager.playMusic( "encounter_phase", { loop: false } );

        ServiceLocator.game.cGameWorld.cActionLogger.logPhaseStart( game_state_id.ENCOUNTER );
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setGamePhase( game_state_id.ENCOUNTER );
        
        ServiceLocator.game.setActivePlayer( ServiceLocator.game.firstPlayer );

        // Create states.
        // Optional engagement.
        let optionalEngagementState: OptionalEngagementState = new OptionalEngagementState();
        optionalEngagementState.onCompleted.addOnce( this.onPhase_Completed, this );
        // Engagement checks.
        let engagementChecksState: EngagementChecksState = new EngagementChecksState();
        engagementChecksState.onCompleted.addOnce( this.onPhase_Completed, this );
        //
        this._stateMachine = new StateMachine();
        this._stateMachine.add( encounter_state_id.OPTIONAL_ENGAGEMENT, optionalEngagementState );
        this._stateMachine.add( encounter_state_id.ENGAGEMENT_CHECKS, engagementChecksState );
        this._stateMachine.init();
        this._stateMachine.request( ServiceLocator.game.phaseNavigator.findFirstPlayableSubstateId( game_state_id.ENCOUNTER ) );
    }

    public onLeave(): void
    {
        this._stateMachine.end();
        this._stateMachine = null;

        ServiceLocator.audioManager.stopMusic();
    
        super.onLeave();
    }

    public findCurrentSubstate(): encounter_state_id
    {
        return this._stateMachine.currentStateId;
    }

    // overrides.
    public saveGame(): any
    {
        let result: ISgEncounterState = { substateId: this._stateMachine.currentStateId };

        return result;
    }

    // overrides.
    public onLoad( sgState: any, pass: number ): void
    {
        super.onLoad( sgState, pass );

        ServiceLocator.audioManager.playMusic( "encounter_phase", { loop: false } );

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setGamePhase( game_state_id.ENCOUNTER );

        // Create states.
        // Optional engagement.
        let optionalEngagementState: OptionalEngagementState = new OptionalEngagementState();
        optionalEngagementState.onCompleted.addOnce( this.onPhase_Completed, this );
        // Engagement checks.
        let engagementChecksState: EngagementChecksState = new EngagementChecksState();
        engagementChecksState.onCompleted.addOnce( this.onPhase_Completed, this );
        //
        this._stateMachine = new StateMachine();
        this._stateMachine.add( encounter_state_id.OPTIONAL_ENGAGEMENT, optionalEngagementState );
        this._stateMachine.add( encounter_state_id.ENGAGEMENT_CHECKS, engagementChecksState );
        this._stateMachine.init();
        this._stateMachine.load( ( sgState as ISgEncounterState ).substateId, null, pass );
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        // TODO:
    }

    // #endregion //


    // #region Callbacks //

    private onPhase_Completed(): void
    {
        const kLastStateId: number = ServiceLocator.game.phaseNavigator.findLastPlayableSubstateId( game_state_id.ENCOUNTER );
        if ( this._stateMachine.currentStateId >= kLastStateId || kLastStateId == null )
        {
            ServiceLocator.game.stateMachine.onStateEntered.addOnce( ( state: State ) => {
                let phaseIntroState: PhaseIntroState = state as PhaseIntroState;
                phaseIntroState.nextStateId = ServiceLocator.game.phaseNavigator.findNextStateId( game_state_id.ENCOUNTER ) } );
            ServiceLocator.game.stateMachine.request( game_state_id.PHASE_INTRO );
        }
        else
        {
            this._stateMachine.request( ServiceLocator.game.phaseNavigator.findNextStateId( this._stateMachine.currentStateId, game_state_id.ENCOUNTER ) );
        }
    }

    // #endregion //
}