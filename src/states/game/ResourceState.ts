import State from "../../lib/state_machine/State";

import { game_state_id } from "../StateId";
import { action_scope_type, player_action_type } from "../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../ServiceLocator";

import StateMachine from "../../lib/StateMachine";
import { IOpponentAction, IOppActionListener } from "../../game/AllyActionManager";
import { player_type } from "../../game/component/world/CPlayerArea";
import PhaseIntroState from "./PhaseIntroState";
import GameObject from "../../game/GameObject";
import CEndTurnButton, { end_turn_btn_state } from "../../game/component/ui/CEndTurnButton";
import Session from "../../Session";
import { location_type } from "../../game/component/world/CGameWorld";


export default class ResourceState extends State implements IOppActionListener
{
    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        super.onEnter();

        ServiceLocator.audioManager.playMusic( "resource_phase", { loop: false } );

        ServiceLocator.game.cGameWorld.cActionLogger.logPhaseStart( game_state_id.RESOURCE );
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setGamePhase( game_state_id.RESOURCE );

        ServiceLocator.game.setActivePlayer( ServiceLocator.game.firstPlayer );

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cEndTurnButton.setText( "TO_PLANNING_PHASE" );
        if ( Session.allyId )
        {
            ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.setText( "TO_PLANNING_PHASE" );
        }

        // Draw card.
        if ( ServiceLocator.game.phaseNavigator.isResPhaseDrawCard )
        {
            let drawnCard: GameObject = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.deck.cDeckIndicator.cViewer.cCardView.drawCard();
            if ( drawnCard )
            {
                ServiceLocator.game.cardActivationArea.cCardActivationArea.forceDrawCard( drawnCard );
            }
        }
        else if ( ServiceLocator.game.phaseNavigator.isResPhaseAddToken )
        {
            this.onCard_Drawn();
        }
        else if ( ServiceLocator.savedData.data.gamePreferences.isAutoEndResourcePhase )
        {
            this.onEndTurnBtn_Click();
        }

        // Listen to events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.add( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.cardActivationArea.cCardActivationArea.onCompleted.addOnce( this.onCard_Drawn, this );
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );
    }

    public onLeave(): void
    {
        ServiceLocator.audioManager.stopMusic();

        // Cleanup events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.remove( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.cardActivationArea.cCardActivationArea.onCompleted.remove( this.onCard_Drawn, this );
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );

        super.onLeave();
    }

    // overrides.
    public onLoad( sgState: any, pass: number ): void
    {
        super.onLoad( sgState, pass );

        ServiceLocator.audioManager.playMusic( "resource_phase", { loop: false } );

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setGamePhase( game_state_id.RESOURCE );

        // Listen to events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.addOnce( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.SET_END_TURN_BTN_STATE
            && action.args[ 0 ] as end_turn_btn_state == end_turn_btn_state.COMPLETED )
        {
            if ( ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn.state == end_turn_btn_state.COMPLETED )
            {
                this.nextState();
            }
        }
    }

        private nextState(): void
        {
            ServiceLocator.game.stateMachine.onStateEntered.addOnce( ( state: State ) => {
                let phaseIntroState: PhaseIntroState = state as PhaseIntroState;
                phaseIntroState.nextStateId = game_state_id.PLANNING; } );
            ServiceLocator.game.stateMachine.request( game_state_id.PHASE_INTRO );
        }

    // #endregion //


    // #region Callbacks //

    private onEndTurnBtn_Click(): void
    {
        if ( !Session.allyId )
        {
            this.nextState();
        }
        else
        {
            let cEndTurnBtn: CEndTurnButton = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn;
            cEndTurnBtn.toggleCompleted( action_scope_type.MULTIPLAYER );
            if ( cEndTurnBtn.state == end_turn_btn_state.COMPLETED 
                && ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.state == end_turn_btn_state.COMPLETED )
            {        
                this.nextState();
            }
        }
    }

    private onCard_Drawn(): void
    {
        // Gain resources.
        if ( ServiceLocator.game.phaseNavigator.isResPhaseAddToken )
        {
            let arrCharacterToken: Array<GameObject> = ServiceLocator.game.cGameWorld.cPlayerArea.home.cActorArea.findAllActors();
            for ( let characterToken of arrCharacterToken )
            {
                if ( characterToken.cCardToken.curSide.cHeroSide )
                {
                    characterToken.cCardToken.curSide.cHeroSide.resource.cTokenCounter.addCount( 1, action_scope_type.MULTIPLAYER );
                }
                else if ( characterToken.cCardToken.curSide.cAllySide && characterToken.cCardToken.curSide.cAllySide.hasResourceCounter() )
                {
                    characterToken.cCardToken.curSide.cAllySide.resource.cTokenCounter.addCount( 1, action_scope_type.MULTIPLAYER );
                }
            }
        }

        if ( ServiceLocator.savedData.data.gamePreferences.isAutoEndResourcePhase )
        {
            this.onEndTurnBtn_Click();
        }
    }

    // #endregion //
}