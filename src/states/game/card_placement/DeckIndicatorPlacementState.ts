import ServiceLocator from "../../../ServiceLocator";
import { gsap, TimelineMax, Sine, Back } from "gsap";
import * as PIXI from "pixi.js";

import GameObject from "../../../game/GameObject";
import { IStateData } from "../../../game/component/world/card_activation_area/CardActivationController";
import Signal from "../../../lib/signals/Signal";
import CCardView, { deck_side_type } from "../../../game/component/ui/viewer/CCardView";
import { action_scope_type } from "../../../service/socket_io/GameSocketIOController";
import State from "../../../lib/state_machine/State";
import { player_type } from "../../../game/component/world/CPlayerArea";
import LogTargetCard from "../../../game/component/ui/right_menu/action_log/target/LogTargetCard";
import Utils from "../../../Utils";


export default class DeckIndicatorPlacementState extends State
{
    private _data: IStateData = null;
    private _deckIndicator: GameObject = null;
    private _actionScope: action_scope_type = action_scope_type.LOCAL;

    private _onCompleted: Signal = new Signal();


    public set data( value: IStateData ) { this._data = value; }
    public set deckIndicator( value: GameObject ) { this._deckIndicator = value; }
    public set actionScope( value: action_scope_type ) { this._actionScope = value; }

    public get onCompleted(): Signal { return this._onCompleted; }

    
    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        console.assert( this._data != null, "DeckIndicatorPlacementState.ts :: onEnter() :: this._data cannot be null." );
        console.assert( this._deckIndicator != null, "DeckIndicatorPlacementState.ts :: onEnter() :: this._deckIndicator cannot be null." );

        this.placeCard();
    }

    public onLeave(): void
    {
        this._onCompleted.removeAll();

        this._data = null;
    }

    // private:

    private placeCard(): void
    {
        let tl: TimelineMax = new TimelineMax();
        tl.addLabel( "out", 0 );
        tl.to( this._data.disablerBg.cContainer.c, { alpha: 0, ease: Sine.easeOut, duration: 0.5 }, "out" );
        //
        const kDeckIndicatorGlobal: PIXI.Point = this._deckIndicator.cContainer.c.getGlobalPosition();
        tl.to( this._data.planeContainer.cContainer.c, { 
                x: kDeckIndicatorGlobal.x, 
                y: kDeckIndicatorGlobal.y + this._deckIndicator.cContainer.c.height * 0.5, 
                ease: Sine.easeOut, duration: 0.5 }, 
            "out" );
        //
        const kScale: number = this._deckIndicator.cContainer.c.height / this._data.planeContainer.cContainer.c.getLocalBounds().height;
        tl.to( this._data.planeContainer.cContainer.c.scale, { x: kScale, y: kScale, ease: Sine.easeOut, duration: 0.5 }, "out" );
        //
        tl.call( this.onPlaceCardAnim_Completed.bind( this ), null );
    }

    // #endregion //


    // #region Callbacks //

    private onPlaceCardAnim_Completed(): void
    {
        // Log.
        ServiceLocator.game.cGameWorld.cActionLogger.logSequence( player_type.PLAYER, new LogTargetCard( this._data.card ), Utils.img.findLocationTextureId( this._data.card.cCard.location ), Utils.img.findLocationTextureId( this._deckIndicator.cDeckIndicator.cViewer.location ), false );

        this._data.disablerBg.end();
        this._data.disablerBg = null;

        this._data.planeContainer.end();
        this._data.planeContainer = null;

        this._deckIndicator.cDeckIndicator.cViewer.cCardView.go.cDropArea.forceDrop( this._data.card, CCardView.findPredefinedDropPosition( deck_side_type.TOP ), this._actionScope ); 

        this._onCompleted.dispatch();
    }

    // #endregion //
}