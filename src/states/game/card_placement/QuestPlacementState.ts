import State from "../../../lib/state_machine/State";

import { action_scope_type } from "../../../service/socket_io/GameSocketIOController";
import { IStateData } from "../../../game/component/world/card_activation_area/CardActivationController";
import ServiceLocator from "../../../ServiceLocator";
import { gsap, TimelineMax, Sine, Back } from "gsap";
import * as PIXI from "pixi.js";

import Signal from "../../../lib/signals/Signal";
import { player_type } from "../../../game/component/world/CPlayerArea";
import LogTargetCard from "../../../game/component/ui/right_menu/action_log/target/LogTargetCard";
import Utils from "../../../Utils";
import { location_type } from "../../../game/component/world/CGameWorld";
import GameObject from "../../../game/GameObject";


export default class QuestPlacementState extends State
{
    private _data: IStateData = null;
    private _questHolder: GameObject = null;

    private _onCompleted: Signal = new Signal();


    public set data( value: IStateData ) { this._data = value; }
    public set questHolder( value: GameObject ) { this._questHolder = value; }

    public get onCompleted(): Signal { return this._onCompleted; }


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        console.assert( this._data != null, "QuestPlacementState.ts :: onEnter() :: this._data cannot be null." );
        console.assert( this._questHolder != null, "QuestPlacementState.ts :: onEnter() :: this._questHolder cannot be null." );

        this.placeCard();
    }

    public onLeave(): void
    {
        this._onCompleted.removeAll();

        this._data = null;
    }

    // private:

    protected placeCard(): void
    {
        let tl: TimelineMax = new TimelineMax();
        tl.addLabel( "out", 0 );
        const kQuestHolderPos: PIXI.Point = this._questHolder.cContainer.c.getGlobalPosition().clone();
        tl.to( this._data.planeContainer.cContainer.c, { x: kQuestHolderPos.x, y: kQuestHolderPos.y, ease: Sine.easeOut, duration: 0.5 }, "out" );
        tl.to( this._data.disablerBg.cContainer.c, { alpha: 0, ease: Sine.easeOut, duration: 0.5 }, "out" );
        const kScale: number = 180 / this._data.planeContainer.cContainer.c.getLocalBounds().width;
        tl.to( this._data.planeContainer.cContainer.c.scale, { x: kScale, y: kScale, ease: Sine.easeOut, duration: 0.5 }, "out" );
        tl.call( this.onPlaceCardAnim_Completed.bind( this ), null );
    }

    // #endregion //


    // #region Callbacks //

    protected onPlaceCardAnim_Completed(): void
    {
        // Log.
        ServiceLocator.game.cGameWorld.cActionLogger.logSequence( player_type.PLAYER, new LogTargetCard( this._data.card ), Utils.img.findLocationTextureId( this._data.card.cCard.location ), Utils.img.findLocationTextureId( this._questHolder.cQuestHolder.location ), false );

        this._data.disablerBg.end();
        this._data.disablerBg = null;

        this._data.planeContainer.end();
        this._data.planeContainer = null;

        this._questHolder.cQuestHolder.go.cDropArea.forceDrop( this._data.card, null, action_scope_type.LOCAL );
        
        this._onCompleted.dispatch();
    }

    // #endregion //
}