import ServiceLocator from "../../../ServiceLocator";
import { gsap, TimelineMax, Sine, Back } from "gsap";

import GameObject from "../../../game/GameObject";
import CCardPreviewable from "../../../game/component/input/CCardPreviewable";
import CContainer from "../../../game/component/pixi/CContainer";
import { IStateData } from "../../../game/component/world/card_activation_area/CardActivationController";
import State from "../../../lib/state_machine/State";
import Signal from "../../../lib/signals/Signal";
import Component from "../../../game/component/Component";
import CAllyActivatedCard from "../../../game/component/card/CAllyActivatedCard";
import { layer_type } from "../../../game/component/world/CGameLayerProvider";


export default class AllySidePlacementState extends State
{
    private _data: IStateData = null;

    private _onCompleted: Signal = new Signal();


    public set data( value: IStateData ) { this._data = value; }

    public get onCompleted(): Signal { return this._onCompleted; }


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        console.assert( this._data != null, "AllySidePlacementState.ts :: onEnter() :: this._data cannot be null." );

        this.placeCard();
    }

    public onLeave(): void
    {
        this._onCompleted.removeAll();

        this._data = null;
    }

    // private:

    private placeCard(): void
    {
        let tl: TimelineMax = new TimelineMax();
        tl.addLabel( "out", 0 );
        tl.to( this._data.disablerBg.cContainer.c, { alpha: 0, ease: Sine.easeOut, duration: 0.5 }, "out" );
        //
        let activatedCard: GameObject = this.createActivatedCard();
        activatedCard.cContainer.c.position.set( 
            ServiceLocator.game.app.screen.width - activatedCard.cContainer.c.width - 20, 
            20 );
        activatedCard.cContainer.c.visible = false;
        ServiceLocator.game.root.cGameLayerProvider.add( activatedCard, layer_type.ACTIVATED_CARD );
        tl.to( this._data.planeContainer.cContainer.c, { 
                x: activatedCard.cContainer.c.x + activatedCard.cContainer.c.width * 0.5, 
                y: activatedCard.cContainer.c.y + activatedCard.cContainer.c.height * 0.5, 
                ease: Sine.easeOut, duration: 0.5 }, 
            "out" );
        //
        const kScale: number = this._data.card.cContainer.c.height / this._data.planeContainer.cContainer.c.getLocalBounds().height;
        tl.to( this._data.planeContainer.cContainer.c.scale, { x: kScale, y: kScale, ease: Sine.easeOut, duration: 0.5 }, "out" );
        //
        tl.call( this.onPlaceCardAnim_Completed.bind( this ), [ activatedCard ] );
    }

        private createActivatedCard(): GameObject
        {
            let arrComponent: Array<Component> = [ new CContainer(), new CAllyActivatedCard() ];
            if ( this._data.card.cCard.isFaceUp )
            {
                arrComponent.push( new CCardPreviewable() );
            }
            let result: GameObject = new GameObject( arrComponent );
            result.cAllyActivatedCard.card = this._data.card;
            if ( result.cCardPreviewable )
            {
                result.cCardPreviewable.cardId = this._data.card.cCard.curSide.cardId;
            }
            result.init();

            return result;
        }

    // #endregion //


    // #region Callbacks //

    protected onPlaceCardAnim_Completed( activatedCard: GameObject ): void
    {
        this._data.disablerBg.end();
        this._data.disablerBg = null;

        this._data.planeContainer.end();
        this._data.planeContainer = null;

        activatedCard.cContainer.c.visible = true;

        this._onCompleted.dispatch();
    }

    // #endregion //
}