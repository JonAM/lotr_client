import ServiceLocator from "../../../ServiceLocator";
import { TimelineMax, Sine, Back } from "gsap";

import GameObject from "../../../game/GameObject";
import CCardPreviewable from "../../../game/component/input/CCardPreviewable";
import CContainer from "../../../game/component/pixi/CContainer";
import CActivatedCard from "../../../game/component/card/CActivatedCard";
import CTargetSelector, { target_selection_type } from "../../../game/component/input/CTargetSelector";
import CButton from "../../../game/component/input/CButton";
import { IStateData } from "../../../game/component/world/card_activation_area/CardActivationController";
import State from "../../../lib/state_machine/State";
import Signal from "../../../lib/signals/Signal";
import Component from "../../../game/component/Component";
import { button_type } from "../setup/CardPresentationUtils";
import { layer_type } from "../../../game/component/world/CGameLayerProvider";


export default class SidePlacementState extends State
{
    private _data: IStateData = null;

    private _onCompleted: Signal = new Signal();


    public set data( value: IStateData ) { this._data = value; }

    public get onCompleted(): Signal { return this._onCompleted; }


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        console.assert( this._data != null, "SidePlacementState.ts :: onEnter() :: this._data cannot be null." );

        this.placeCard();
    }

    public onLeave(): void
    {
        this._onCompleted.removeAll();

        this._data = null;
    }

    // private:

    private placeCard(): void
    {
        let tl: TimelineMax = new TimelineMax();
        tl.addLabel( "out", 0 );
        tl.to( this._data.disablerBg.cContainer.c, { alpha: 0, ease: Sine.easeOut, duration: 0.5 }, "out" );
        //
        let activatedCard: GameObject = this.createActivatedCard();
        activatedCard.cContainer.c.position.set( 
            ServiceLocator.game.app.screen.width - activatedCard.cContainer.c.width - 20, 
            ServiceLocator.game.app.screen.height - activatedCard.cContainer.c.height - 20 );
        activatedCard.cContainer.c.visible = false;
        ServiceLocator.game.root.cGameLayerProvider.add( activatedCard, layer_type.ACTIVATED_CARD );
        tl.to( this._data.planeContainer.cContainer.c, { 
                x: activatedCard.cContainer.c.x + activatedCard.cContainer.c.width * 0.5, 
                y: activatedCard.cContainer.c.y + activatedCard.cContainer.c.height * 0.5, 
                ease: Sine.easeOut, duration: 0.5 }, 
            "out" );
        //
        const kScale: number = this._data.card.cContainer.c.height / this._data.planeContainer.cContainer.c.getLocalBounds().height;
        tl.to( this._data.planeContainer.cContainer.c.scale, { x: kScale, y: kScale, ease: Sine.easeOut, duration: 0.5 }, "out" );
        //
        tl.call( this.onPlaceCardAnim_Completed.bind( this ), [ activatedCard ] );
    }

        private createActivatedCard(): GameObject
        {
            let arrComponent: Array<Component> = [ new CContainer(), new CActivatedCard(), new CButton(), new CTargetSelector() ];
            if ( this._data.card.cCard.isFaceUp )
            {
                arrComponent.push( new CCardPreviewable() );
            }
            let result: GameObject = new GameObject( arrComponent );
            result.cActivatedCard.card = this._data.card;
            result.cActivatedCard.targetSelectionType = this.findTargetSelectionType( this._data.buttonRowType );
            result.cTargetSelector.receptor = result;
            if ( result.cCardPreviewable )
            {
                result.cCardPreviewable.cardId = this._data.card.cCard.curSide.cardId;
            }
            result.init();

            return result;
        }

    private findTargetSelectionType( buttonRowType: button_type ): target_selection_type
    {
        let result: target_selection_type = null;

        switch ( buttonRowType )
        {
            case button_type.EQUIP: { result = target_selection_type.EQUIP; break; }
            case button_type.TARGET: { result = target_selection_type.TARGET; break; }
            case button_type.UNDERNEATH: { result = target_selection_type.UNDERNEATH; break; }
            case button_type.SHADOW_CARD: { result = target_selection_type.SHADOW_CARD; break; }
        }

        return result;
    }

    // #endregion //


    // #region Callbacks //

    protected onPlaceCardAnim_Completed( activatedCard: GameObject ): void
    {
        this._data.disablerBg.end();
        this._data.disablerBg = null;

        this._data.planeContainer.end();
        this._data.planeContainer = null;

        activatedCard.cContainer.c.visible = true;
        activatedCard.cTargetSelector.startSelection( this.findTargetSelectionType( this._data.buttonRowType ) );

        this._onCompleted.dispatch();
    }

    // #endregion //
}