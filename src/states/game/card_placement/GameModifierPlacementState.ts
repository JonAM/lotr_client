import ServiceLocator from "../../../ServiceLocator";
import { gsap, TimelineMax, Sine, Back } from "gsap";
import * as PIXI from "pixi.js";

import GameObject from "../../../game/GameObject";
import { IStateData } from "../../../game/component/world/card_activation_area/CardActivationController";
import Signal from "../../../lib/signals/Signal";
import { action_scope_type } from "../../../service/socket_io/GameSocketIOController";
import State from "../../../lib/state_machine/State";
import { player_type } from "../../../game/component/world/CPlayerArea";
import LogTargetCard from "../../../game/component/ui/right_menu/action_log/target/LogTargetCard";
import Utils from "../../../Utils";


export default class GameModifierPlacementState extends State
{
    private _data: IStateData = null;
    private _gameModifier: GameObject = null;
    private _actionScope: action_scope_type = action_scope_type.LOCAL;

    private _onCompleted: Signal = new Signal();


    public set data( value: IStateData ) { this._data = value; }
    public set gameModifier( value: GameObject ) { this._gameModifier = value; }
    public set actionScope( value: action_scope_type ) { this._actionScope = value; }

    public get onCompleted(): Signal { return this._onCompleted; }

    
    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        console.assert( this._data != null, "GameModifierPlacementState.ts :: onEnter() :: this._data cannot be null." );
        console.assert( this._gameModifier != null, "GameModifierPlacementState.ts :: onEnter() :: this._gameModifier cannot be null." );

        Utils.game.showActor( this._gameModifier );

        this.placeCard();
    }

    public onLeave(): void
    {
        this._onCompleted.removeAll();

        this._data = null;
    }

    // private:

    private placeCard(): void
    {
        let tl: TimelineMax = new TimelineMax();
        tl.addLabel( "out", 0 );
        tl.to( this._data.disablerBg.cContainer.c, { alpha: 0, ease: Sine.easeOut, duration: 0.5 }, "out" );
        //
        const kGameModifierGlobal: PIXI.Point = this._gameModifier.cGameModifier.cardImgContainer.getGlobalPosition();
        tl.to( this._data.planeContainer.cContainer.c, { 
                x: kGameModifierGlobal.x + this._gameModifier.cGameModifier.cardImgContainer.width * 0.5, 
                y: kGameModifierGlobal.y + this._gameModifier.cGameModifier.cardImgContainer.height * 0.5, 
                ease: Sine.easeOut, duration: 0.5 }, 
            "out" );
        //
        const kScale: number = this._gameModifier.cGameModifier.cardImgContainer.height / this._data.planeContainer.cContainer.c.getLocalBounds().height;
        tl.to( this._data.planeContainer.cContainer.c.scale, { x: kScale, y: kScale, ease: Sine.easeOut, duration: 0.5 }, "out" );
        //
        tl.call( this.onPlaceCardAnim_Completed.bind( this ), null );
    }

    // #endregion //


    // #region Callbacks //

    private onPlaceCardAnim_Completed(): void
    {
        // Log.
        ServiceLocator.game.cGameWorld.cActionLogger.logTargetSelection( 
            player_type.PLAYER, new LogTargetCard( this._data.card ), new LogTargetCard( this._gameModifier ), "rad_underneath", false, true );

        this._data.disablerBg.end();
        this._data.disablerBg = null;

        this._data.planeContainer.end();
        this._data.planeContainer = null;

        this._gameModifier.cGameModifier.addCardUnderneath( this._data.card, this._actionScope ); 

        this._onCompleted.dispatch();
    }

    // #endregion //
}