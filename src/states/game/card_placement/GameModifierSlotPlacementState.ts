
import ServiceLocator from "../../../ServiceLocator";
import { TimelineMax, Sine } from "gsap";
import * as PIXI from "pixi.js";

import GameObject from "../../../game/GameObject";
import Utils from "../../../Utils";
import CardTokenFactory from "../../../game/CardTokenFactory";
import { action_scope_type } from "../../../service/socket_io/GameSocketIOController";
import { IStateData } from "../../../game/component/world/card_activation_area/CardActivationController";
import Signal from "../../../lib/signals/Signal";
import CDropArea from "../../../game/component/input/CDropArea";
import State from "../../../lib/state_machine/State";
import { player_type } from "../../../game/component/world/CPlayerArea";
import LogTargetCard from "../../../game/component/ui/right_menu/action_log/target/LogTargetCard";
import { layer_type } from "../../../game/component/world/CGameLayerProvider";
import GameModifierFactory from "../../../game/GameModifierFactory";


export default class GameModifierSlotPlacementState extends State
{
    private _data: IStateData = null;
    private _actorArea: GameObject = null;

    private _onCompleted: Signal = new Signal();


    public set data( value: IStateData ) { this._data = value; }
    public set actorArea( value: GameObject ) { this._actorArea = value; }

    public get onCompleted(): Signal { return this._onCompleted; }


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        console.assert( this._data != null, "GameModifierSlotPlacementState.ts :: onEnter() :: this._data cannot be null." );
        console.assert( this._actorArea != null, "GameModifierSlotPlacementState.ts :: onEnter() :: this._actorArea cannot be null." );

        this.placeCard();
    }

    public onLeave(): void
    {
        this._onCompleted.removeAll();

        this._data = null;
    }

    // private:

    private placeCard(): void
    {
        let tl: TimelineMax = new TimelineMax();
        tl.addLabel( "out", 0 );
        tl.to( this._data.disablerBg.cContainer.c, { alpha: 0, ease: Sine.easeOut, duration: 0.5 }, "out" );
        let gmf: GameModifierFactory = new GameModifierFactory();
        let gameModifier: GameObject = gmf.create( this._data.card );
        ServiceLocator.game.root.cGameLayerProvider.add( gameModifier, layer_type.UNDER_TABLE );
        this._actorArea.cActorArea.makeRoomForNewGameModifier();
        let nextItemPos: PIXI.Point = this._actorArea.cActorArea.findNextGameModifierDropPosition( gameModifier );
        nextItemPos.x += gameModifier.cGameModifier.calcWidth() * 0.5;
        nextItemPos.y += gameModifier.cGameModifier.calcHeight() * 0.5;
        gameModifier.cContainer.c.position.copyFrom( nextItemPos );
        const kCardTokenPos: PIXI.Point = gameModifier.cContainer.c.getGlobalPosition();
        tl.to( this._data.planeContainer.cContainer.c, { x: kCardTokenPos.x, y: kCardTokenPos.y, ease: Sine.easeOut, duration: 0.5 }, "out" );
        const kScale: number = gameModifier.cGameModifier.calcHeight() / this._data.planeContainer.cContainer.c.getLocalBounds().height;
        tl.to( this._data.planeContainer.cContainer.c.scale, { x: kScale, y: kScale, ease: Sine.easeOut, duration: 0.5 }, "out" );
        tl.call( this.onPlaceCardAnim_Completed.bind( this ), [ gameModifier ] );
    }

    // #endregion //


    // #region Callbacks //

    protected onPlaceCardAnim_Completed( gameModifier: GameObject ): void
    {
        // Log.
        ServiceLocator.game.cGameWorld.cActionLogger.logSequence( player_type.PLAYER, new LogTargetCard( this._data.card ), Utils.img.findLocationTextureId( this._data.card.cCard.location ), Utils.img.findLocationTextureId( this._actorArea.cActorArea.location ), false );

        this._data.disablerBg.end();
        this._data.disablerBg = null;

        this._data.planeContainer.end();
        this._data.planeContainer = null;

        this._data.card.end();
        this._data.card = null;

        this._actorArea.cActorArea.addGameModifier( gameModifier );

        this._onCompleted.dispatch();
    }

    // #endregion //
}