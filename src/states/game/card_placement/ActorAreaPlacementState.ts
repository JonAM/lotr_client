
import ServiceLocator from "../../../ServiceLocator";
import { TimelineMax, Sine } from "gsap";
import * as PIXI from "pixi.js";

import GameObject from "../../../game/GameObject";
import Utils from "../../../Utils";
import CardTokenFactory from "../../../game/CardTokenFactory";
import { action_scope_type } from "../../../service/socket_io/GameSocketIOController";
import { IStateData } from "../../../game/component/world/card_activation_area/CardActivationController";
import Signal from "../../../lib/signals/Signal";
import CDropArea from "../../../game/component/input/CDropArea";
import State from "../../../lib/state_machine/State";
import { player_type } from "../../../game/component/world/CPlayerArea";
import LogTargetCard from "../../../game/component/ui/right_menu/action_log/target/LogTargetCard";
import { layer_type } from "../../../game/component/world/CGameLayerProvider";


export default class ActorAreaPlacementState extends State
{
    private _data: IStateData = null;
    private _actorArea: GameObject = null;

    private _onCompleted: Signal = new Signal();


    public set data( value: IStateData ) { this._data = value; }
    public set actorArea( value: GameObject ) { this._actorArea = value; }

    public get onCompleted(): Signal { return this._onCompleted; }


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        console.assert( this._data != null, "StagingPlacementState.ts :: onEnter() :: this._data cannot be null." );
        console.assert( this._actorArea != null, "StagingPlacementState.ts :: onEnter() :: this._actorArea cannot be null." );

        this.placeCard();
    }

    public onLeave(): void
    {
        this._onCompleted.removeAll();

        this._data = null;
    }

    // private:

    private placeCard(): void
    {
        let tl: TimelineMax = new TimelineMax();
        tl.addLabel( "out", 0 );
        tl.to( this._data.disablerBg.cContainer.c, { alpha: 0, ease: Sine.easeOut, duration: 0.5 }, "out" );
        let ctf: CardTokenFactory = new CardTokenFactory();
        let cardToken: GameObject = ctf.create( this._data.card );
        ServiceLocator.game.root.cGameLayerProvider.add( cardToken, layer_type.UNDER_TABLE );
        this._actorArea.cActorArea.showDropTab( action_scope_type.LOCAL );
        const kNextItemPos: PIXI.Point = this._actorArea.cActorArea.selActorTab.cActorTab.findActorDropPosition( cardToken );
        cardToken.cContainer.c.position.copyFrom( kNextItemPos );
        cardToken.cContainer.c.position.x += cardToken.cCardToken.calcWidth() * 0.5;
        const kCardTokenPos: PIXI.Point = cardToken.cContainer.c.getGlobalPosition();
        tl.to( this._data.planeContainer.cContainer.c, { x: kCardTokenPos.x, y: kCardTokenPos.y, ease: Sine.easeOut, duration: 0.5 }, "out" );
        const kScale: number = cardToken.cCardToken.cCurSide.bg.cContainer.c.height / this._data.planeContainer.cContainer.c.getLocalBounds().height;
        tl.to( this._data.planeContainer.cContainer.c.scale, { x: kScale, y: kScale, ease: Sine.easeOut, duration: 0.5 }, "out" );
        tl.call( this.onPlaceCardAnim_Completed.bind( this ), [ cardToken ] );
    }

    // #endregion //


    // #region Callbacks //

    protected onPlaceCardAnim_Completed( cardToken: GameObject ): void
    {
        // Log.
        ServiceLocator.game.cGameWorld.cActionLogger.logSequence( player_type.PLAYER, new LogTargetCard( this._data.card ), Utils.img.findLocationTextureId( this._data.card.cCard.location ), Utils.img.findLocationTextureId( this._actorArea.cActorArea.location ), false );

        this._data.disablerBg.end();
        this._data.disablerBg = null;

        this._data.planeContainer.end();
        this._data.planeContainer = null;

        this._data.card.end();
        this._data.card = null;

        cardToken.cContainer.c.position.set( 0 );
        this._actorArea.cDropArea.forceDrop( cardToken, new PIXI.Point( CDropArea.kPredefinedDropPositionCode, 0 ), action_scope_type.LOCAL );

        this._onCompleted.dispatch();
    }

    // #endregion //
}