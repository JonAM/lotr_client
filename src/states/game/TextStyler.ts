import * as PIXI from "pixi.js";


export default class TextStyler
{
    private _small: PIXI.TextStyle = null;
    private _normal: PIXI.TextStyle = null;
    private _subtitle: PIXI.TextStyle = null;
    private _title: PIXI.TextStyle = null;
    private _intro: PIXI.TextStyle = null;
    private _introBig: PIXI.TextStyle = null;
    private _tokenCounterAnim: PIXI.TextStyle = null;
    private _tooltipDescription: PIXI.TextStyle = null;
    private _inGameRequest: PIXI.TextStyle = null;

    public get small(): PIXI.TextStyle { return this._small.clone(); }
    public get normal(): PIXI.TextStyle { return this._normal.clone(); }
    public get subtitle(): PIXI.TextStyle { return this._subtitle.clone(); }
    public get title(): PIXI.TextStyle { return this._title.clone(); }
    public get intro(): PIXI.TextStyle { return this._intro.clone(); }
    public get introBig(): PIXI.TextStyle { return this._introBig.clone(); }
    public get tokenCounterAnim(): PIXI.TextStyle { return this._tokenCounterAnim.clone(); }
    public get tooltipDescription(): PIXI.TextStyle { return this._tooltipDescription.clone(); }
    public get inGameRequest(): PIXI.TextStyle { return this._inGameRequest.clone(); }

    public constructor()
    {
        this._small = new PIXI.TextStyle( {
            dropShadow: true,
            dropShadowDistance: 2,
            fill: "white",
            fontFamily: "Tahoma",
            fontSize: 14,
            lineJoin: "round",
            strokeThickness: 3 } );

        this._normal = new PIXI.TextStyle( {
            dropShadow: true,
            dropShadowDistance: 2,
            fill: "white",
            fontFamily: "Tahoma",
            fontSize: 18,
            lineJoin: "round",
            strokeThickness: 4 } );

        this._subtitle = new PIXI.TextStyle( {
            dropShadow: true,
            dropShadowDistance: 2,
            fill: "white",
            fontFamily: "Tahoma",
            fontSize: 28,
            lineJoin: "round",
            strokeThickness: 4 } );

        this._title = new PIXI.TextStyle( {
            dropShadow: true,
            dropShadowDistance: 2,
            fill: "white",
            fontFamily: "Tahoma",
            fontSize: 44,
            lineJoin: "round",
            strokeThickness: 4 } );

        this._intro = new PIXI.TextStyle( {
            dropShadow: true,
            dropShadowDistance: 2,
            fill: "white",
            fontFamily: "Tahoma",
            fontSize: 64,
            lineJoin: "round",
            strokeThickness: 4 } );

        this._introBig = new PIXI.TextStyle( {
            dropShadow: true,
            dropShadowDistance: 2,
            fill: "white",
            fontFamily: "Tahoma",
            fontSize: 84,
            lineJoin: "round",
            strokeThickness: 4 } );

        this._tokenCounterAnim = new PIXI.TextStyle( {
            dropShadow: true,
            dropShadowDistance: 2,
            fill: "white",
            fontFamily: "Tahoma",
            fontSize: 28,
            fontStyle: "italic",
            fontWeight: "bold",
            lineJoin: "round",
            strokeThickness: 10 } );

        this._tooltipDescription = new PIXI.TextStyle( {
            fill: "black",
            fontFamily: "Tahoma",
            fontSize: 14 } );

        this._inGameRequest = new PIXI.TextStyle({
            dropShadow: true,
            dropShadowAlpha: 0.7,
            dropShadowColor: "#757575",
            dropShadowDistance: 10,
            fill: [
                "red",
                "white"
            ],
            fillGradientStops: [
                0.25
            ],
            fontFamily: "Tahoma",
            fontSize: 79,
            fontStyle: "italic",
            fontWeight: "bolder",
            letterSpacing: 15,
            lineJoin: "round",
            strokeThickness: 10
        });
    }
}