import { location_type } from "../../../game/component/world/CGameWorld";
import GameObject from "../../../game/GameObject";
import ServiceLocator from "../../../ServiceLocator";
import { game_state_id } from "../../StateId";


export default class CardPresentationUtils
{
    public constructor() {}

    public findContextControls( card: GameObject ): Array<button_type>
    {
        let arrButtonType: Array<button_type> = null;

        switch ( card.cCard.curSide.cardInfo.type_code )
        {
            case "hero": { arrButtonType = [ button_type.PLAY ]; break; }
            case "ally": { arrButtonType = [ button_type.PLAY ]; break; }
            case "attachment": { arrButtonType = [ button_type.EQUIP, button_type.PLAY ]; break; }
            case "treasure": { arrButtonType = [ button_type.EQUIP, button_type.PLAY ]; break; }
            case "event": { arrButtonType = [ button_type.DISCARD, button_type.TARGET, button_type.PLAY ]; break; }
            case "player-side-quest": { arrButtonType = [ button_type.PLAY ]; break; }

            case "enemy": { arrButtonType = [ button_type.PLAY ]; break; }
            case "ship-enemy": { arrButtonType = [ button_type.PLAY ]; break; }
            case "location": { arrButtonType = [ button_type.PLAY ]; break; }
            case "treachery": { arrButtonType = [ button_type.DISCARD, button_type.TARGET, button_type.EQUIP, button_type.PLAY ]; break; }
            case "objective": { arrButtonType = [ button_type.PLAY ]; break; }
            case "objective-ally": { arrButtonType = [ button_type.PLAY ]; break; }
            case "objective-hero": { arrButtonType = [ button_type.PLAY ]; break; }
            case "objective-location": { arrButtonType = [ button_type.PLAY ]; break; }
            case "ship-objective": { arrButtonType = [ button_type.PLAY ]; break; }
            case "encounter-side-quest": { arrButtonType = [ button_type.PLAY ]; break; }

            case "quest-intro": { arrButtonType = [ button_type.FLIP, button_type.PLAY ]; break; }
            case "quest": { arrButtonType = [ button_type.PLAY ]; break; }
            case "player-back": { arrButtonType = [ button_type.FLIP ]; break; }
            case "encounter-back": { arrButtonType = [ button_type.FLIP, button_type.SHADOW_CARD ]; break; }

            case "contract":
            case "campaign":
            case "nightmare-setup":
            case "gencon-setup": { arrButtonType = [ button_type.PLAY, button_type.EQUIP ]; break; }
        }
        arrButtonType.push( button_type.UNDERNEATH );
        if ( arrButtonType.indexOf( button_type.DISCARD ) == -1 )
        {
            arrButtonType.push( button_type.DISCARD );
        }
        arrButtonType.push( button_type.CANCEL );

        // Special cases.
        // Drawing a player card.
        if ( card.cCard.isFaceUp 
            && ( card.cCard.location == location_type.MY_DECK || card.cCard.location == location_type.MY_DISCARD ) )
        {
            arrButtonType.unshift( button_type.DRAW );
        }
        // Setting up quest card.
        if ( ServiceLocator.game.stateMachine.currentStateId == game_state_id.SETUP
            && ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.setAside.cIndicatorLock.isLocked() )
        {
            if ( card.cCard.curSide.cardInfo.type_code == "quest-intro" )
            {
                arrButtonType = [ button_type.PLAY ];
            }
        }

        return arrButtonType;
    }
}

export const enum button_type
{
    DRAW = 0,
    PLAY,
    TARGET,
    EQUIP,
    UNDERNEATH,
    SHADOW_CARD,
    DISCARD,
    CANCEL,
    // For default action.
    FLIP
}