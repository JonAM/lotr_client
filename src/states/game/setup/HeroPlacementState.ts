import State from "../../../lib/state_machine/State";

import { location_type } from "../../../game/component/world/CGameWorld";
import { player_type } from "../../../game/component/world/CPlayerArea";
import ServiceLocator from "../../../ServiceLocator";

import Signal from "../../../lib/signals/Signal";
import GameObject from "../../../game/GameObject";
import Session from "../../../Session";
import CContainer from "../../../game/component/pixi/CContainer";
import CDraggable from "../../../game/component/input/CDraggable";
import CDropArea from "../../../game/component/input/CDropArea";
import CCardTokenPoiReceptor from "../../../game/component/world/poi_receptor/CCardTokenPoiReceptor";
import { action_scope_type, IRemoteCard } from "../../../service/socket_io/GameSocketIOController";
import CCardToken from "../../../game/component/card/CCardToken";
import { ICard } from "../../../game/CardDB";
import CShareableGameElement from "../../../game/component/CShareableGameElement";
import { IRemoteDeck } from "../../RemoteDeckCreator";


export default class HeroPlacementState extends State
{
    // #region Attributes //

    // private:

    private _remoteDeck: IRemoteDeck = null;

    // Signals.
    private _onCompleted: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public set remoteDeck( value: IRemoteDeck ) { this._remoteDeck = value; }

    // Signals.
    public get onCompleted(): Signal { return this._onCompleted; }

    // #endregion //


    // #region Methods //

    public constructor() { super(); }

    public onEnter(): void
    {
        console.assert( this._remoteDeck != null, "HeroPlacementState.ts :: onEnter() :: this._remoteDeck cannot be null." );

        super.onEnter();

        this.setPlayerHeroCards( this._remoteDeck.playerDeck.hero );
        if ( this._remoteDeck.allyDeck )
        {
            this.setAllyHeroCards( this._remoteDeck.allyDeck.hero );
        }

        // Smooth animations.
        window.setTimeout( () => { this._onCompleted.dispatch(); }, 750 );
    }

    public onLeave(): void
    {
        this._onCompleted.removeAll();
    
        super.onLeave();
    }

    // private:

    private setPlayerHeroCards( arrHeroRemoteCard: Array<IRemoteCard> ): void
    {
        let arrCardToken: Array<GameObject> = new Array<GameObject>();
        let threat: number = 0;
        let heroIndex: number = 0;
        for ( let heroRemoteCard of arrHeroRemoteCard )
        {
            let heroToken: GameObject = new GameObject( [ 
                new CContainer(), 
                new CCardToken(),
                new CDraggable(), 
                new CDropArea(), 
                new CCardTokenPoiReceptor(),
                new CShareableGameElement() ] );
            heroToken.oid = heroRemoteCard.oid + "t";
            heroToken.cDraggable.dragShadowTextureCreator = heroToken.cCardToken;
            heroToken.cCardToken.frontSideCardId = heroRemoteCard.cid;
            const kCardInfo: ICard = ServiceLocator.cardDb.find( heroRemoteCard.cid );
            heroToken.cCardToken.backSideCardId = kCardInfo.side;
            heroToken.cCardToken.ownerPlayer = player_type.PLAYER;
            heroToken.cCardToken.controllerPlayer = player_type.PLAYER;
            heroToken.cCardToken.location = location_type.MY_HOME;
            heroToken.cDropArea.target = heroToken.cCardToken;
            heroToken.cDropArea.isPropagate = true;
            heroToken.init();

            threat += heroToken.cCardToken.curSide.cHeroSide.cardInfo.threat;

            arrCardToken.push( heroToken );

            ++heroIndex;
        }
        ServiceLocator.game.cGameWorld.cPlayerArea.home.cActorArea.appendActors( arrCardToken );
    
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.threatLevel.cTokenIndicator.trigger.cTokenCounter.setCount( threat, action_scope_type.LOCAL );
    }

    private setAllyHeroCards( arrHeroRemoteCard: Array<IRemoteCard> ): void
    {
        let arrCardToken: Array<GameObject> = new Array<GameObject>();
        let threat: number = 0;
        let heroIndex: number = 0;
        for ( let heroRemoteCard of arrHeroRemoteCard )
        {
            let heroToken: GameObject = new GameObject( [ 
                new CContainer(), 
                new CCardToken(),
                new CDraggable(), 
                new CDropArea(), 
                new CCardTokenPoiReceptor(),
                new CShareableGameElement() ] );
            heroToken.oid = heroRemoteCard.oid + "t";
            heroToken.cDraggable.dragShadowTextureCreator = heroToken.cCardToken;
            heroToken.cCardToken.frontSideCardId = heroRemoteCard.cid;
            const kCardInfo: ICard = ServiceLocator.cardDb.find( heroRemoteCard.cid );
            heroToken.cCardToken.backSideCardId = kCardInfo.side;
            heroToken.cCardToken.ownerPlayer = player_type.ALLY;
            heroToken.cCardToken.controllerPlayer = player_type.ALLY;
            heroToken.cCardToken.location = location_type.ALLY_HOME;
            heroToken.cDropArea.target = heroToken.cCardToken;
            heroToken.cDropArea.isPropagate = true;
            heroToken.init();
            heroToken.cCardToken.setEnabled( false );

            threat += heroToken.cCardToken.curSide.cHeroSide.cardInfo.threat;

            arrCardToken.push( heroToken );

            ++heroIndex;
        }
        ServiceLocator.game.cGameWorld.cAllyArea.home.cActorArea.appendActors( arrCardToken );
    
        ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.threatLevel.cTokenIndicator.trigger.cTokenCounter.setCount( threat, action_scope_type.LOCAL );
    }

    // #endregion //
}