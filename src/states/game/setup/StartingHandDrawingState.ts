import State from "../../../lib/state_machine/State";

import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import { context_button_type } from "../../../game/component/ui/viewer/CViewContextBtn";
import ServiceLocator from "../../../ServiceLocator";

import CCardView, { deck_side_type } from "../../../game/component/ui/viewer/CCardView";
import GameObject from "../../../game/GameObject";
import Signal from "../../../lib/signals/Signal";
import Session from "../../../Session";
import CEndTurnButton, { end_turn_btn_state } from "../../../game/component/ui/CEndTurnButton";
import { IOppActionListener, IOpponentAction } from "../../../game/AllyActionManager";


export default class StartingHandDrawingState extends State implements IOppActionListener
{
    // #region Attributes //

    // private:

    // Signals.
    private _onCompleted: Signal = new Signal();

    // #endregion //

    
    // #region Properties //

    public get onCompleted(): Signal { return this._onCompleted; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        super.onEnter();

        this.drawStartingHand();

        let playerHand: GameObject = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.hand;
        playerHand.cDeckIndicator.cViewer.cCardView.setContextBtnState( [ 
            { buttonType: context_button_type.RANDOMLY_DISCARD, isVisible: false },
            { buttonType: context_button_type.DRAW_X_PLAYER_CARDS, isVisible: false },
            { buttonType: context_button_type.MULLIGAN, isVisible: true }, ] );
        playerHand.cDeckIndicator.cViewer.show();

        // Listen to events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.add( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );
    }

    public onLeave(): void
    {
        this._onCompleted.removeAll();

        // Cleanup events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.remove( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.hand
            .cDeckIndicator.cViewer.cCardView.setContextBtnState( [ 
                { buttonType: context_button_type.RANDOMLY_DISCARD, isVisible: true },
                { buttonType: context_button_type.DRAW_X_PLAYER_CARDS, isVisible: true },
                { buttonType: context_button_type.MULLIGAN, isVisible: false } ] );

        super.onLeave();
    }

    // private:

    private drawStartingHand(): void
    {
        let hand: GameObject = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.hand;
        for ( let i: number = 0; i < 6; ++i )
        {
            let card: GameObject = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.deck.cDeckIndicator.cViewer.cCardView.drawCard();   
            card.cCard.isAnimated = false;
            hand.cDeckIndicator.cViewer.cCardView.go.cDropArea.forceDrop( card, CCardView.findPredefinedDropPosition( deck_side_type.BOTTOM ), action_scope_type.MULTIPLAYER );
            card.cCard.isAnimated = true;
        }
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.SET_END_TURN_BTN_STATE
            && action.args[ 0 ] as end_turn_btn_state == end_turn_btn_state.COMPLETED )
        {
            if ( ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn.state == end_turn_btn_state.COMPLETED )
            {
                this._onCompleted.dispatch();
            }
        }
    }

    // #endregion //


    // #region Callbacks //

    private onEndTurnBtn_Click(): void
    {
        if ( !Session.allyId )
        {
            this._onCompleted.dispatch();
        }
        else
        {
            let cEndTurnBtn: CEndTurnButton = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn;
            cEndTurnBtn.toggleCompleted( action_scope_type.MULTIPLAYER );
            if ( cEndTurnBtn.state == end_turn_btn_state.COMPLETED 
                && ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.state == end_turn_btn_state.COMPLETED )
            {
                this._onCompleted.dispatch();
            }
        }
    }

    // #endregion //
}