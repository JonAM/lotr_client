import State from "../../../lib/state_machine/State";

import { action_scope_type, player_action_type } from "../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../ServiceLocator";

import GameObject from "../../../game/GameObject";
import Signal from "../../../lib/signals/Signal";
import { IOppActionListener, IOpponentAction } from "../../../game/AllyActionManager";
import Session from "../../../Session";
import CEndTurnButton, { end_turn_btn_state } from "../../../game/component/ui/CEndTurnButton";
import { player_type } from "../../../game/component/world/CPlayerArea";
import { location_type } from "../../../game/component/world/CGameWorld";
import { IScenario } from "../../../game/ScenarioDB";
import ScenarioSetup from "../ScenarioSetup";
import CustomPanelFactory from "../../../game/CustomPanelFactory";


export default class QuestPlacementState extends State implements IOppActionListener
{
    // #region Attributes //

    // private:

    // Signals.
    private _onCompleted: Signal = new Signal();

    // #endregion //

    
    // #region Properties //

    public get onCompleted(): Signal { return this._onCompleted; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        super.onEnter();

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn.setWaiting( action_scope_type.LOCAL );
        if ( Session.allyId )
        {
            ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.setWaiting( action_scope_type.LOCAL );
        }

        if ( !Session.allyId || ServiceLocator.game.firstPlayer == player_type.PLAYER )
        {
            let drawnCard: GameObject = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.questDeck.cDeckIndicator.cViewer.cCardView.drawCard()
            ServiceLocator.game.cardActivationArea.cCardActivationArea.forceCardActivation( drawnCard, location_type.QUEST_DECK, action_scope_type.MULTIPLAYER );
        }
        ServiceLocator.game.cGameWorld.cSauronArea.cQuestHolder.onQuestTokenSet.addOnce( this.onQuestIntro_Completed, this );

        // Listen to events.
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );
    }

    public onLeave(): void
    {
        this._onCompleted.removeAll();

        // Cleanup events.
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.remove( this.onEndTurnBtn_Click, this );
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.SET_END_TURN_BTN_STATE ] );
        
        ServiceLocator.game.cGameWorld.cCustomPanelManager.remove( "setup_instructions" );

        super.onLeave();
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        if ( action.playerActionType == player_action_type.SET_END_TURN_BTN_STATE
            && action.args[ 0 ] as end_turn_btn_state == end_turn_btn_state.COMPLETED )
        {
            if ( ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn.state == end_turn_btn_state.COMPLETED )
            {
                this._onCompleted.dispatch();
            }
        }
    }

    // #endregion //


    // #region Callbacks //

    private onQuestIntro_Completed( questCard: GameObject ): void
    {
        ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.deck.cIndicatorLock.setLocked( false );
        ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.questDeck.cIndicatorLock.setLocked( false );
        ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.setAside.cIndicatorLock.setLocked( false );

        // Setup.
        const kScenario: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
        if ( kScenario.setup.length > 0 )
        {
            let scenarioSetup: ScenarioSetup = new ScenarioSetup();
            scenarioSetup.apply( kScenario.setup.slice() );
        }

        // Custom panel.
        let cpf: CustomPanelFactory = new CustomPanelFactory();
        ServiceLocator.game.cGameWorld.cCustomPanelManager.addPanelAt( cpf.create( "setup_instructions" ), 0 );
        ServiceLocator.game.cGameWorld.customPanelManager.cContainer.c.visible = true;
        ServiceLocator.game.cGameWorld.cCustomPanelManager.show( "setup_instructions" );

        // Listen to events.
        ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "setup_instructions" ).cSetupInstructions.onCompleted.addOnce( this.onSetupInstructions_Completed, this );
    }

        private onSetupInstructions_Completed(): void
        {
            ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn.setText( "START_GAME" );
            if ( Session.allyId )
            {
                ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.setText( "START_GAME" );
            }

            // Listen to events.
            ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.endTurnBtn.cButton.onClick.add( this.onEndTurnBtn_Click, this );
        }

    private onEndTurnBtn_Click(): void
    {
        if ( !Session.allyId )
        {
            this._onCompleted.dispatch();
        }
        else
        {
            let cEndTurnBtn: CEndTurnButton = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn;
            cEndTurnBtn.toggleCompleted( action_scope_type.MULTIPLAYER );
            if ( cEndTurnBtn.state == end_turn_btn_state.COMPLETED 
                && ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.state == end_turn_btn_state.COMPLETED )
            {        
                this._onCompleted.dispatch();
            }
        }
    }

    // #endregion //
}