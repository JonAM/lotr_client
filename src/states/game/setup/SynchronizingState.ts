import State from "../../../lib/state_machine/State";

import { game_state_id } from "../../StateId";
import { player_action_type } from "../../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../../ServiceLocator";

import Signal from "../../../lib/signals/Signal";
import { IOpponentAction, IOppActionListener } from "../../../game/AllyActionManager";
import Session from "../../../Session";


export default class SynchronizingState extends State implements IOppActionListener
{ 
    // #region Attributes //

    // private:
    private _isCompleted: boolean = false;

    // Signals.
    private _onCompleted: Signal = new Signal();

    // #endregion //

    
    // #region Properties //
    
    // Signals.
    public get onCompleted(): Signal { return this._onCompleted; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() { super(); }

    public init(): void
    {
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.NOTIFY_SETUP_COMPLETED ] );
    }

    public onEnter(): void
    {
        super.onEnter();

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.NOTIFY_SETUP_COMPLETED, game_state_id.SETUP, null );
        
        if ( this._isCompleted || !Session.allyId )
        {
            this._onCompleted.dispatch();
        }
        else
        {
            // TODO: Show message waiting for ally.
        }
    }

    public onLeave(): void
    {
        this._onCompleted.removeAll();

        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.NOTIFY_SETUP_COMPLETED ] );

        super.onLeave();
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        this._isCompleted = true;
        if ( this._isCurrent )
        {
            this._onCompleted.dispatch();
        }
    }

    // #endregion //
}