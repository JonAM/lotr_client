import State from "../../lib/state_machine/State";

import { game_state_id } from "../StateId";
import ServiceLocator from "../../ServiceLocator";
import { TimelineMax, Sine, Back } from "gsap";
import * as PIXI from "pixi.js";

import GameObject from "../../game/GameObject";
import CGraphics from "../../game/component/pixi/CGraphics";
import PhaseIntroState from "./PhaseIntroState";
import CContainer from "../../game/component/pixi/CContainer";
import CPlayerSideControls from "../../game/component/ui/CPlayerSideControls";
import { layer_type } from "../../game/component/world/CGameLayerProvider";


export default class RoundIntroState extends State
{
    // #region Attributes //

    // private:

    private _isFirstRound: boolean = false;
    private _roundAnnouncement: GameObject = null;
    private _disablerBg: GameObject = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        super.onEnter();

        // Sfx.
        ServiceLocator.audioManager.playSfx( "round_started" );
        
        ServiceLocator.game.roundCount += 1;
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.onRoundCountUpdated( ServiceLocator.game.roundCount );
        this._isFirstRound = ServiceLocator.game.roundCount == 1;

        if ( ServiceLocator.game.isAnimated )
        {
            ServiceLocator.game.allyActionManager.pause();
        }
        
        this._disablerBg = new GameObject( [ new CGraphics() ] );
        this._disablerBg.init();
        this._disablerBg.cGraphics.g.interactive = true;
        this._disablerBg.cGraphics.g.beginFill( 0x000000 );
        this._disablerBg.cGraphics.g.drawRect( 0, 0, ServiceLocator.game.app.screen.width, ServiceLocator.game.app.screen.height );
        this._disablerBg.cGraphics.g.endFill();
        ServiceLocator.game.root.cGameLayerProvider.add( this._disablerBg, layer_type.DISABLED );

        this._roundAnnouncement = this.createPhaseAnnouncement();
        ServiceLocator.game.root.cGameLayerProvider.add( this._roundAnnouncement, layer_type.DISABLED );
        this._roundAnnouncement.cContainer.c.position.set( ServiceLocator.game.app.screen.width * 0.5, ServiceLocator.game.app.screen.height * 0.5 );
        this.animatePhaseAnnouncement( this._roundAnnouncement );
    }

    public onLeave(): void
    {
        this._disablerBg.end();
        this._disablerBg = null;

        this._roundAnnouncement.end();
        this._roundAnnouncement = null;

        if ( ServiceLocator.game.isAnimated )
        {
            ServiceLocator.game.allyActionManager.resume();
        }

        if ( this._isFirstRound )
        {
            let hand: GameObject = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.hand;
            hand.cDeckIndicator.cViewer.cCardView.showContextButtons();
            this.unlockPlayerSideBar();
            this.unlockOpponentSideBar();
            // TODO: Quick fix to adjust the opponent hand's viewer height. It is wrong due to the animation of the cards when dropped.
            //ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.hand.cDeckIndicator.panel.cViewer.cCardView.sortItems();

            this._isFirstRound = false;
        }
    
        super.onLeave();
    }

    // private:

    private createPhaseAnnouncement(): GameObject
    {
        let result: GameObject = new GameObject( [ new CContainer() ] );
        result.init();

        let title: PIXI.Text = new PIXI.Text( jQuery.i18n( "NEW_ROUND" ), ServiceLocator.game.textStyler.introBig );
        //
        let titleBox: PIXI.Graphics = new PIXI.Graphics();
        titleBox.lineStyle( 1, 0x00000, 1 );
        titleBox.beginFill( 0xefe4b0, 1 );
        let boxPadding: number = 40;
        titleBox.drawRoundedRect( -( title.width * 0.5 + boxPadding ), -( title.height * 0.5 + boxPadding ), title.width + boxPadding * 2, title.height + boxPadding * 2, 25 );
        titleBox.endFill();
        result.cContainer.c.addChild( titleBox );
        //
        title.anchor.set( 0.5 );
        titleBox.addChild( title );

        let round: PIXI.Text = new PIXI.Text( ServiceLocator.game.roundCount.toString(), ServiceLocator.game.textStyler.intro );
        //
        let roundBox: PIXI.Graphics = new PIXI.Graphics();
        roundBox.lineStyle( 1, 0x00000, 1 );
        roundBox.beginFill( 0xbcb280, 1 );
        roundBox.drawCircle( 0, 0, 50 );
        roundBox.endFill();
        roundBox.y = titleBox.height * 0.5 + 25;
        result.cContainer.c.addChild( roundBox );
        //
        round.anchor.set( 0.5 );
        roundBox.addChild( round );

        let banner: PIXI.Sprite = PIXI.Sprite.from( PIXI.Texture.from( ServiceLocator.resourceStack.find( "intro_round" ).data ) );
        banner.anchor.set( 0.5, 1 );
        banner.position.y = -titleBox.height * 0.5;
        result.cContainer.c.addChild( banner );

        return result;
    }

    private animatePhaseAnnouncement( phaseAnnouncement: GameObject ): void
    {
        phaseAnnouncement.cContainer.c.scale.set( 0.25 );
        phaseAnnouncement.cContainer.c.alpha = 0.25;
        this._disablerBg.cContainer.c.alpha = 0;

        let tl: TimelineMax = new TimelineMax();
        tl.addLabel( "in", 0 );
        tl.to( this._disablerBg.cContainer.c, 0.25, { alpha: 0.6, ease: Sine.easeOut }, "in" );
        tl.to( phaseAnnouncement.cContainer.c.scale, 1, { x: 1, y: 1, ease: Back.easeOut }, "in" );
        tl.to( phaseAnnouncement.cContainer.c, 0.5, { alpha: 1, ease: Sine.easeIn }, "in" );
        tl.addLabel( "out", "+=1" );
        tl.to( phaseAnnouncement.cContainer.c, 0.5, { alpha: 0, ease: Sine.easeOut }, "out" );
        tl.to( this._disablerBg.cContainer.c, 0.5, { alpha: 0, ease: Sine.easeOut }, "out" );
        tl.to( phaseAnnouncement.cContainer.c.scale, 0.5, { x: 1.5, y: 1.5, ease: Sine.easeOut }, "out" );
        tl.call( this.onAnimation_Completed.bind( this ), null );
    }

    private unlockPlayerSideBar(): void
    {
        let cPlayerSideControls: CPlayerSideControls = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls;
        cPlayerSideControls.threatLevel.cIndicatorLock.setLocked( false );
        cPlayerSideControls.deck.cIndicatorLock.setLocked( false );
        cPlayerSideControls.discard.cIndicatorLock.setLocked( false );
    }

    private unlockOpponentSideBar(): void
    {
        let cPlayerSideControls: CPlayerSideControls = ServiceLocator.game.cGameWorld.cAllyArea.cSideControls;
        // Threat tlevel.
        cPlayerSideControls.threatLevel.cIndicatorLock.setLocked( false );
        cPlayerSideControls.threatLevel.cIndicator.trigger.cTokenCounter.setEnabled( false );
        cPlayerSideControls.threatLevel.cIndicator.trigger.cDropArea.setEnabled( true );
        cPlayerSideControls.threatLevel.cIndicator.trigger.cContainer.c.interactive = true;
        // Discard.
        cPlayerSideControls.discard.cIndicatorLock.setLocked( false );
    }

    // #endregion //


    // #region Callbacks //

    private onAnimation_Completed(): void
    {
        ServiceLocator.game.stateMachine.onStateEntered.addOnce( ( state: State ) => {
            let phaseIntroState: PhaseIntroState = state as PhaseIntroState;
            phaseIntroState.nextStateId = game_state_id.RESOURCE; } );
        ServiceLocator.game.stateMachine.request( game_state_id.PHASE_INTRO );
    }

    // #endregion //
}