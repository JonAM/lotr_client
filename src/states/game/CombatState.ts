import State from "../../lib/state_machine/State";

import { combat_state_id, game_state_id } from "../StateId";
import { action_scope_type } from "../../service/socket_io/GameSocketIOController";
import ServiceLocator from "../../ServiceLocator";

import StateMachine from "../../lib/StateMachine";
import PhaseIntroState from "./PhaseIntroState";
import EnemyAttacksState from "./combat/EnemyAttacksState";
import PlayerAttacksState from "./combat/PlayerAttacksState";
import { ISgCombatState } from "../../view/game/SaveGameView";
import GameObject from "../../game/GameObject";


export default class CombatState extends State
{
    // #region Attributes //

    // private:

    private _stateMachine: StateMachine = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        super.onEnter();

        ServiceLocator.audioManager.playMusic( "combat_sauron_phase", { loop: false } );

        ServiceLocator.game.cGameWorld.cActionLogger.logPhaseStart( game_state_id.COMBAT );
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setGamePhase( game_state_id.COMBAT );

        ServiceLocator.game.setActivePlayer( ServiceLocator.game.firstPlayer );

        // Create states.
        // Enemy attacks.
        let enemyAttacksState: EnemyAttacksState = new EnemyAttacksState();
        enemyAttacksState.onCompleted.addOnce( this.onPhase_Completed, this );
        // Player attacks.
        let playerAttacks: PlayerAttacksState = new PlayerAttacksState();
        playerAttacks.onCompleted.addOnce( this.onPhase_Completed, this );
        //
        this._stateMachine = new StateMachine();
        this._stateMachine.add( combat_state_id.ENEMY_ATTACKS, enemyAttacksState );
        this._stateMachine.add( combat_state_id.PLAYER_ATTACKS, playerAttacks );
        this._stateMachine.init();
        this._stateMachine.request( ServiceLocator.game.phaseNavigator.findFirstPlayableSubstateId( game_state_id.COMBAT ) );
    }

    public onLeave(): void
    {
        let arrActor: Array<GameObject> = ServiceLocator.game.cGameWorld.cPlayerArea.cEngaged.findAllActors();
        for ( let actor of arrActor )
        {
            actor.cCardToken.curSide.cEnemySide.discardShadowCards( action_scope_type.LOCAL );
        }
        arrActor = ServiceLocator.game.cGameWorld.cAllyArea.cEngaged.findAllActors();
        for ( let actor of arrActor )
        {
            actor.cCardToken.curSide.cEnemySide.discardShadowCards( action_scope_type.LOCAL );
        }
        arrActor = ServiceLocator.game.cGameWorld.cSauronArea.cStaging.findAllActors();
        for ( let actor of arrActor )
        {
            if ( actor.cCardToken.curSide.cEnemySide )
            {
                actor.cCardToken.curSide.cEnemySide.discardShadowCards( action_scope_type.LOCAL );
            }
        }
        if ( ServiceLocator.game.cGameWorld.cCustomPanelManager.checkPanelVisibility( "isolated" ) )
        {
            arrActor = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "isolated" ).cIsolatedArea.cEngaged.findAllActors();
            for ( let actor of arrActor )
            {
                if ( actor.cCardToken.curSide.cEnemySide )
                {
                    actor.cCardToken.curSide.cEnemySide.discardShadowCards( action_scope_type.LOCAL );
                }
            }
        }

        this._stateMachine.end();
        this._stateMachine = null;

        ServiceLocator.audioManager.stopMusic();
    
        super.onLeave();
    }

    public findCurrentSubstate(): combat_state_id
    {
        return this._stateMachine.currentStateId;
    }

    // overrides.
    public saveGame(): any
    {
        let result: ISgCombatState = { substateId: this._stateMachine.currentStateId };

        return result;
    }

    // overrides.
    public onLoad( sgState: any, pass: number ): void
    {
        super.onLoad( sgState, pass );

        ServiceLocator.audioManager.playMusic( "combat_sauron_phase", { loop: false } );

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setGamePhase( game_state_id.COMBAT );

        // Create states.
        // Enemy attacks.
        let enemyAttacksState: EnemyAttacksState = new EnemyAttacksState();
        enemyAttacksState.onCompleted.addOnce( this.onPhase_Completed, this );
        // Player attacks.
        let playerAttacks: PlayerAttacksState = new PlayerAttacksState();
        playerAttacks.onCompleted.addOnce( this.onPhase_Completed, this );
        //
        this._stateMachine = new StateMachine();
        this._stateMachine.add( combat_state_id.ENEMY_ATTACKS, enemyAttacksState );
        this._stateMachine.add( combat_state_id.PLAYER_ATTACKS, playerAttacks );
        this._stateMachine.init();
        this._stateMachine.load( ( sgState as ISgCombatState ).substateId, null, pass );
    }

    // #endregion //


    // #region Callbacks //

    private onPhase_Completed(): void
    {
        const kLastStateId: number = ServiceLocator.game.phaseNavigator.findLastPlayableSubstateId( game_state_id.COMBAT );
        if ( this._stateMachine.currentStateId >= kLastStateId || kLastStateId == null )
        {
            ServiceLocator.game.stateMachine.onStateEntered.addOnce( ( state: State ) => {
                let phaseIntroState: PhaseIntroState = state as PhaseIntroState;
                phaseIntroState.nextStateId = game_state_id.REFRESH; } );
            ServiceLocator.game.stateMachine.request( game_state_id.PHASE_INTRO );
        }
        else
        {
            this._stateMachine.request( ServiceLocator.game.phaseNavigator.findNextStateId( this._stateMachine.currentStateId, game_state_id.COMBAT ) );
        }
    }

    // #endregion //
}