import State from "../../lib/state_machine/State";

import { player_type } from "../../game/component/world/CPlayerArea";
import ServiceLocator from "../../ServiceLocator";
import Session from "../../Session";
import { setup_state_id, game_state_id } from "../StateId";
import Utils from "../../Utils";

import StateMachine from "../../lib/StateMachine";
import SynchronizingState from "./setup/SynchronizingState";
import { IDeck } from "../../view/gateway/DeckParser";
import { action_scope_type, IRemoteCard } from "../../service/socket_io/GameSocketIOController";
import GameObject from "../../game/GameObject";
import CPlayerSideControls from "../../game/component/ui/CPlayerSideControls";
import HeroPlacementState from "./setup/HeroPlacementState";
import StartingHandDrawingState from "./setup/StartingHandDrawingState";
import QuestPlacementState from "./setup/QuestPlacementState";
import CCardView from "../../game/component/ui/viewer/CCardView";
import { IScenario } from "../../game/ScenarioDB";
import CContainer from "../../game/component/pixi/CContainer";
import CGameModifier from "../../game/component/card/CGameModifier";
import CDraggable from "../../game/component/input/CDraggable";
import CTargetSelector, { target_selection_type } from "../../game/component/input/CTargetSelector";
import CTargetReceptor from "../../game/component/world/CTargetReceptor";
import CHighlightPoiReceptor from "../../game/component/world/poi_receptor/CHighlightPoiReceptor";
import { ICard } from "../../game/CardDB";
import CShareableGameElement from "../../game/component/CShareableGameElement";
import RemoteDeckCreator, { IRemoteDeck } from "../RemoteDeckCreator";
import GameModifierFactory from "../../game/GameModifierFactory";


export default class SetupState extends State 
{
    // #region Attributes //

    // private:

    private _stateMachine: StateMachine = null;

    private _arrPlayerEncounterCardId: Array<string> = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor() { super(); }

    public onEnter(): void
    {
        super.onEnter();

        this._arrPlayerEncounterCardId = new Array<string>();

        ServiceLocator.game.cGameWorld.cOptionsMenu.setSaveButtonEnabled( false );
        if ( ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "split_sauron" ) )
        {
            ServiceLocator.game.cGameWorld.cSauronArea.cStaging.hideSplitBtn();
        }

        ServiceLocator.game.cGameWorld.cActionLogger.logPhaseStart( game_state_id.SETUP );
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cPhaseDiagramBtn.setGamePhase( game_state_id.SETUP );

        ServiceLocator.audioManager.playMusic( "setup_phase", null );

        // Shuffle.
        Utils.game.shuffle( Session.encounterDeck );
        if ( ServiceLocator.game.firstPlayer == player_type.PLAYER )
        {
            Utils.game.shuffle( Session.playerDeck.player );
            if ( Session.allyId )
            {
                Utils.game.shuffle( Session.allyDeck.player );
            }
        }
        else
        {
            Utils.game.shuffle( Session.allyDeck.player );
            Utils.game.shuffle( Session.playerDeck.player );
        }

        // Create sauron cards.
        let rdc: RemoteDeckCreator = new RemoteDeckCreator();
        const kRemoteDeck: IRemoteDeck = rdc.create();
        //
        ServiceLocator.game.cGameWorld.cSauronArea.sideControls.cSauronSideControls.deck.cDeckIndicator.cViewer
            .cCardView.setCards( kRemoteDeck.encounter );
        this.setQuestDeck( kRemoteDeck.quest );
        ServiceLocator.game.cGameWorld.cSauronArea.sideControls.cSauronSideControls.setAside.cDeckIndicator.cViewer
            .cCardView.setCards( kRemoteDeck.setAside );
        //
        this.createSauronGameModifiers( kRemoteDeck );

        // Create player cards.
        let arrPlayerRemoteCard: Array<IRemoteCard> = kRemoteDeck.playerDeck.player.slice();
        const kArrPlayerSetAsideCard: Array<IRemoteCard> = kRemoteDeck.playerDeck.sideboard.concat( this.removePlayerEncounterCards( arrPlayerRemoteCard ) );
        this.setPlayerSetAsideDeck( kArrPlayerSetAsideCard, player_type.PLAYER );
        this.setPlayerDeck( arrPlayerRemoteCard, player_type.PLAYER );
        if ( Session.allyId )
        {
            let arrAllyRemoteCard: Array<IRemoteCard> = kRemoteDeck.allyDeck.player.slice();
            const kArrAllySetAsideCard: Array<IRemoteCard> = kRemoteDeck.allyDeck.sideboard.concat( this.removePlayerEncounterCards( arrAllyRemoteCard ) );
            this.setPlayerSetAsideDeck( kArrAllySetAsideCard, player_type.ALLY );
            this.setPlayerDeck( arrAllyRemoteCard, player_type.ALLY );
        }
        //
        this.createPlayerGameModifiers( kRemoteDeck );

        // Lock UI.
        ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.deck.cIndicatorLock.setLocked( true );
        ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.questDeck.cIndicatorLock.setLocked( true );
        ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.setAside.cIndicatorLock.setLocked( true );
        //
        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn.setText( "TO_QUEST_SETUP" );
        if ( Session.allyId )
        {
            ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.setText( "TO_QUEST_SETUP" );
        }

        ServiceLocator.game.setActivePlayer( ServiceLocator.game.firstPlayer );

        ServiceLocator.game.allyActionManager.resume();

        // Create states.
        // Hero placement.
        let heroPlacementState: HeroPlacementState = new HeroPlacementState();
        heroPlacementState.remoteDeck = kRemoteDeck;
        heroPlacementState.onCompleted.addOnce( this.onPhase_Completed, this );
        // Drawing.
        let startingHandDrawingState: StartingHandDrawingState = new StartingHandDrawingState();
        startingHandDrawingState.onCompleted.addOnce( this.onPhase_Completed, this );
        // Quest placement.
        let questPlacementState: QuestPlacementState = new QuestPlacementState();
        questPlacementState.onCompleted.addOnce( this.onPhase_Completed, this );
        // Synchronizing.
        let synchronizingState: SynchronizingState = new SynchronizingState();
        synchronizingState.onCompleted.addOnce( this.onPhase_Completed, this );
        //
        this._stateMachine = new StateMachine();
        this._stateMachine.add( setup_state_id.HERO_PLACEMENT, heroPlacementState );
        this._stateMachine.add( setup_state_id.STARTING_HAND_DRAWING, startingHandDrawingState );
        this._stateMachine.add( setup_state_id.QUEST_PLACEMENT, questPlacementState );
        this._stateMachine.add( setup_state_id.SYNCHRONIZING, synchronizingState );
        this._stateMachine.init();
        
        // Smooth animations.
        window.setTimeout( () => { this._stateMachine.request( setup_state_id.HERO_PLACEMENT ); }, 500 );
    }

        private removePlayerEncounterCards( arrRemoteCard: Array<IRemoteCard> ): Array<IRemoteCard>
        {
            let arrRemoved: Array<IRemoteCard> = new Array<IRemoteCard>();

            for ( let i: number = arrRemoteCard.length - 1; i >= 0; --i )
            {
                let remoteCard: IRemoteCard = arrRemoteCard[ i ];
                if ( ServiceLocator.cardDb.find( remoteCard.cid ).text.indexOf( "Encounter." ) != -1 )
                {
                    remoteCard.isFaceUp = true;
                    arrRemoved.push( remoteCard );
                    arrRemoteCard.splice( i, 1 );
                }
            }

            return arrRemoved;
        }

        private setPlayerDeck( arrRemoteCard: Array<IRemoteCard>, playerType: player_type ): void
        {
            let cPlayerSideControls: CPlayerSideControls = null;
            if ( playerType == player_type.PLAYER )
            {
                cPlayerSideControls = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls;
            }
            else
            {
                cPlayerSideControls = ServiceLocator.game.cGameWorld.cAllyArea.cSideControls;
            }
            cPlayerSideControls.deck.cDeckIndicator.cViewer.cCardView.setCards( arrRemoteCard );
        }

        private setPlayerSetAsideDeck( arrRemoteCard: Array<IRemoteCard>, playerType: player_type ): void
        {
            let cPlayerSideControls: CPlayerSideControls = null;
            if ( playerType == player_type.PLAYER )
            {
                cPlayerSideControls = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls;
            }
            else
            {
                cPlayerSideControls = ServiceLocator.game.cGameWorld.cAllyArea.cSideControls;
            }
            cPlayerSideControls.setAside.cDeckIndicator.cViewer.cCardView.setCards( arrRemoteCard );
        }

        private setQuestDeck( arrRemoteCard: Array<IRemoteCard> ): void
        {
            let scenario: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
            if ( scenario.quest && scenario.quest.shuffle != undefined )
            {
                for ( let shufflePair of scenario.quest.shuffle )
                {
                    const kFrom: number = shufflePair[ 0 ];
                    const kTo: number = shufflePair[ 1 ];
                    let arrRandomCard: Array<IRemoteCard> = arrRemoteCard.slice( kFrom, kTo + 1 );
                    arrRemoteCard.splice( kFrom, kTo - kFrom + 1, ...arrRandomCard );
                }
            }
            let cCardView: CCardView = ServiceLocator.game.cGameWorld.cSauronArea.sideControls.cSauronSideControls.questDeck.cDeckIndicator.cViewer.cCardView;
            cCardView.setCards( arrRemoteCard );
        }

        private createPlayerGameModifiers( remoteDeck: IRemoteDeck ): void
        {
            let gmf: GameModifierFactory = null;

            if ( remoteDeck.playerDeck.contract )
            {
                gmf = new GameModifierFactory();
                let gameModifier: GameObject = gmf.createFromRemoteCard( remoteDeck.playerDeck.contract, player_type.PLAYER, player_type.PLAYER );
                ServiceLocator.game.cGameWorld.cPlayerArea.cHome.addGameModifier( gameModifier );
            }
            
            if ( remoteDeck.allyDeck && remoteDeck.allyDeck.contract )
            {
                if ( !gmf )
                {
                    gmf = new GameModifierFactory();
                }
                let gameModifier: GameObject = gmf.createFromRemoteCard( remoteDeck.allyDeck.contract, player_type.ALLY, player_type.ALLY );
                ServiceLocator.game.cGameWorld.cAllyArea.cHome.addGameModifier( gameModifier );
            }
        }

        public createSauronGameModifiers( remoteDeck: IRemoteDeck ): void
        {
            let gmf: GameModifierFactory = null;

            if ( remoteDeck.nightmareSetup )
            {
                gmf = new GameModifierFactory();
                let gameModifier: GameObject = gmf.createFromRemoteCard( remoteDeck.nightmareSetup, player_type.SAURON, player_type.PLAYER );
                ServiceLocator.game.cGameWorld.cSauronArea.cStaging.addGameModifier( gameModifier );
            }

            if ( remoteDeck.campaign )
            {
                if ( !gmf )
                {
                    gmf = new GameModifierFactory();
                }
                let gameModifier: GameObject = gmf.createFromRemoteCard( remoteDeck.campaign, player_type.SAURON, player_type.PLAYER );
                ServiceLocator.game.cGameWorld.cSauronArea.cStaging.addGameModifier( gameModifier );
            }

            if ( remoteDeck.genconSetup )
            {
                if ( !gmf )
                {
                    gmf = new GameModifierFactory();
                }
                let gameModifier: GameObject = gmf.createFromRemoteCard( remoteDeck.genconSetup, player_type.SAURON, player_type.PLAYER );
                ServiceLocator.game.cGameWorld.cSauronArea.cStaging.addGameModifier( gameModifier );
            }
        }

    public onLeave(): void
    {
        ServiceLocator.game.cGameWorld.cOptionsMenu.setSaveButtonEnabled( true );
        if ( ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "split_sauron" ) )
        {
            ServiceLocator.game.cGameWorld.cSauronArea.cStaging.showSplitBtn();
        }

        this._stateMachine.end();
        this._stateMachine = null;

        super.onLeave();
    }

    public findCurrentSubstate(): setup_state_id
    {
        return this._stateMachine.currentStateId;
    }

    // #endregion //


    // #region Callbacks //

    private onPhase_Completed(): void
    {
        if ( this._stateMachine.currentStateId == setup_state_id.SYNCHRONIZING )
        {
            ServiceLocator.game.stateMachine.request( game_state_id.ROUND_INTRO );
        }
        else
        {
            this._stateMachine.request( this._stateMachine.currentStateId + 1 );
        }
    }

    // #endregion //
}