import { detail_bar_icon_type } from "../game/component/card/token/CDetailBar";
import { player_type } from "../game/component/world/CPlayerArea";
import GameObject from "../game/GameObject";
import { ISaveGame, ISgGameModifier, ISgLogAction, ISgCard, ISgCardToken, ISgLogTarget, ISgPlayerPlayArea, ISgPlayerWillpowerCounter, ISgIsolatedArea, ISgActorArea, ISgQuestHolder, ISgActiveLocationHolder, ISgStagingThreatCounter, ISgGameWorld, ISgActorTab, ISgCustomPanel } from "../view/game/SaveGameView";


export default class OppSaveGameGenerator
{
    // public: 

    public constructor() {}

    public generate( saveGame: ISaveGame ): ISaveGame
    {
        saveGame.session = {
            playerId: saveGame.session.allyId,
            allyId: saveGame.session.playerId,
            playerDeck: saveGame.session.allyDeck,
            allyDeck: saveGame.session.playerDeck,
            scenarioId: saveGame.session.scenarioId,
            encounterDeck: saveGame.session.encounterDeck,
            questDeck: saveGame.session.questDeck,
            setAsideDeck: saveGame.session.setAsideDeck,
            isCampaignMode: saveGame.session.isCampaignMode };

        // TODO: Simple solution!
        //saveGame.global.gameObjectOidCounter += 100;
        //saveGame.global.genericEnemyCounter += 100;

        saveGame.gameState.startingPlayer ^= 1;
        saveGame.gameState.firstPlayer ^= 1;
        saveGame.gameState.activePlayer ^= 1;

        if ( saveGame.gameState.gameWorld.sauronPlayArea.staging.sauronActorArea )
        {
            let isTemp: boolean = saveGame.gameState.gameWorld.sauronPlayArea.staging.sauronActorArea.isPlayerSplit;
            saveGame.gameState.gameWorld.sauronPlayArea.staging.sauronActorArea.isPlayerSplit = saveGame.gameState.gameWorld.sauronPlayArea.staging.sauronActorArea.isAllySplit;
            saveGame.gameState.gameWorld.sauronPlayArea.staging.sauronActorArea.isAllySplit = isTemp;
        }

        for ( let sgAttackBindingCounter of saveGame.gameState.attackBindingManager.mapAttackBindingCounter )
        {
            sgAttackBindingCounter[ 1 ].playerType ^= 1;
        }

        const kPlayerPlayArea: ISgPlayerPlayArea = saveGame.gameState.gameWorld.playerPlayArea;
        saveGame.gameState.gameWorld.playerPlayArea = saveGame.gameState.gameWorld.allyPlayArea;
        saveGame.gameState.gameWorld.allyPlayArea = kPlayerPlayArea;

        let arrSgGameModifier: Array<ISgGameModifier> = saveGame.gameState.gameWorld.playerPlayArea.home.gameModifiers.concat( saveGame.gameState.gameWorld.allyPlayArea.home.gameModifiers );
        for ( let sgGameModifier of arrSgGameModifier )
        {
            sgGameModifier.controller ^= 1;
        }

        const kPlayerWillpowerCounter: ISgPlayerWillpowerCounter = saveGame.gameState.gameWorld.sauronPlayArea.playerWillpowerCounter;
        saveGame.gameState.gameWorld.sauronPlayArea.playerWillpowerCounter = saveGame.gameState.gameWorld.sauronPlayArea.allyWillpowerCounter;
        saveGame.gameState.gameWorld.sauronPlayArea.allyWillpowerCounter = kPlayerWillpowerCounter;

        if ( saveGame.gameState.gameWorld.customPanelManager )
        {
            for ( let i: number = 0; i < saveGame.gameState.gameWorld.customPanelManager.customPanels.length; ++i )
            {
                if ( saveGame.gameState.gameWorld.customPanelManager.toggleBtnVisibility[ i ] )
                {
                    const kSgCustomPanel: ISgCustomPanel = saveGame.gameState.gameWorld.customPanelManager.customPanels[ i ];
                    if ( kSgCustomPanel.isolatedArea )
                    {
                        this.transformGameWorldForIsolatedArea( saveGame.gameState.gameWorld, kSgCustomPanel.isolatedArea );
                        break;
                    }
                }
            }
        }

        this.transformCards( saveGame.gameState.gameWorld.playerPlayArea.sideControls.deck.cards );
        this.transformCards( saveGame.gameState.gameWorld.playerPlayArea.sideControls.discard.cards );
        this.transformCards( saveGame.gameState.gameWorld.playerPlayArea.sideControls.hand.cards );
        if ( saveGame.gameState.gameWorld.playerPlayArea.sideControls.customDeck )
        {
            this.transformCards( saveGame.gameState.gameWorld.playerPlayArea.sideControls.customDeck.cards );
        }
        //
        this.transformCards( saveGame.gameState.gameWorld.allyPlayArea.sideControls.deck.cards );
        this.transformCards( saveGame.gameState.gameWorld.allyPlayArea.sideControls.discard.cards );
        this.transformCards( saveGame.gameState.gameWorld.allyPlayArea.sideControls.hand.cards );
        if ( saveGame.gameState.gameWorld.allyPlayArea.sideControls.customDeck )
        {
            this.transformCards( saveGame.gameState.gameWorld.allyPlayArea.sideControls.customDeck.cards );
        }
        //
        this.transformCards( saveGame.gameState.gameWorld.sauronPlayArea.sideControls.tray.cards );
        
        for ( let actorTab of saveGame.gameState.gameWorld.playerPlayArea.home.tabs )
        {
            this.transformCardTokens( actorTab.actors );
        }
        for ( let actorTab of saveGame.gameState.gameWorld.playerPlayArea.engaged.tabs )
        {
            this.transformCardTokens( actorTab.actors );
        }
        //
        for ( let actorTab of saveGame.gameState.gameWorld.allyPlayArea.home.tabs )
        {
            this.transformCardTokens( actorTab.actors );
        }
        for ( let actorTab of saveGame.gameState.gameWorld.allyPlayArea.engaged.tabs )
        {
            this.transformCardTokens( actorTab.actors );
        }

        this.transformLogActions( saveGame.gameState.gameWorld.actionLogger.history );

        return saveGame;
    }

    // private:

    private transformGameWorldForIsolatedArea( gameWorld: ISgGameWorld, sgIsolatedArea: ISgIsolatedArea ): void
    {
        this.transformIsolatedActorArea( sgIsolatedArea.home, gameWorld.playerPlayArea.home, gameWorld.allyPlayArea.home );
        this.transformIsolatedActorArea( sgIsolatedArea.engaged, gameWorld.playerPlayArea.engaged, gameWorld.allyPlayArea.engaged );
        this.transformIsolatedStaging( sgIsolatedArea.staging, gameWorld.sauronPlayArea.staging );

        let sgQuestHolder: ISgQuestHolder = sgIsolatedArea.questHolder;
        sgIsolatedArea.questHolder = gameWorld.sauronPlayArea.questHolder;
        gameWorld.sauronPlayArea.questHolder = sgQuestHolder;

        let sgActiveLocationHolder: ISgActiveLocationHolder = sgIsolatedArea.activeLocationHolder;
        sgIsolatedArea.activeLocationHolder = gameWorld.sauronPlayArea.activeLocationHolder;
        gameWorld.sauronPlayArea.activeLocationHolder = sgActiveLocationHolder;

        let sgStagingThreatCounter: ISgStagingThreatCounter = sgIsolatedArea.stagingThreatCounter;
        sgIsolatedArea.stagingThreatCounter = gameWorld.sauronPlayArea.stagingThreatCounter;
        gameWorld.sauronPlayArea.stagingThreatCounter = sgStagingThreatCounter;

        let sgAllyWillpowerCounter: ISgPlayerWillpowerCounter = sgIsolatedArea.allyWillpowerCounter;
        sgIsolatedArea.allyWillpowerCounter = gameWorld.sauronPlayArea.allyWillpowerCounter;
        gameWorld.sauronPlayArea.allyWillpowerCounter = sgAllyWillpowerCounter;

        let sgSauronWillpowerCounter: ISgPlayerWillpowerCounter = sgIsolatedArea.sauronWillpowerCounter;
        sgIsolatedArea.sauronWillpowerCounter = gameWorld.sauronPlayArea.sauronWillpowerCounter;
        gameWorld.sauronPlayArea.sauronWillpowerCounter = sgSauronWillpowerCounter;
    }

        private transformIsolatedActorArea( isolatedActorArea: ISgActorArea, playerActorArea: ISgActorArea, allyActorArea: ISgActorArea ): void
        {
            playerActorArea.isVisible = true;
            allyActorArea.isVisible = false;

            playerActorArea.tabs = isolatedActorArea.tabs;
            isolatedActorArea.tabs = allyActorArea.tabs;
            allyActorArea.tabs = [];
        }

        private transformIsolatedStaging( isolatedActorArea: ISgActorArea, staging: ISgActorArea ): void
        {
            let arrSgActorTab: Array<ISgActorTab> = isolatedActorArea.tabs;
            isolatedActorArea.tabs = staging.tabs;
            staging.tabs = arrSgActorTab;
        }

    private transformCards( cards: Array<ISgCard> ): void
    {
        for ( let card of cards )
        {
            if ( card.owner != player_type.SAURON )
            {
                card.owner ^= 1;
                card.controller ^= 1;
            }
        }
    }

    private transformCardTokens( cardTokens: Array<ISgCardToken> ): void
    {
        for ( let cardToken of cardTokens )
        {
            if ( cardToken.owner != player_type.SAURON )
            {
                cardToken.owner ^= 1;
            }
            cardToken.controller ^= 1;

            // Attachments.
            for ( let attachment of cardToken.attachments )
            {
                if ( attachment.owner != player_type.SAURON )
                {
                    attachment.owner ^= 1;
                }
                attachment.controller ^= 1;
            }

            for ( let item of cardToken.detailBar.items )
            {
                if ( item.type == detail_bar_icon_type.UNDERNEATH )
                {
                    // Underneath cards.
                    for ( let card of item.underneath )
                    {
                        if ( card.owner != player_type.SAURON )
                        {
                            card.owner ^= 1;
                        }
                        card.controller ^= 1;
                    }

                    break;
                }
            }
        }
    }

    private transformLogActions( logActions: Array<ISgLogAction> ): void
    {
        for ( let logAction of logActions )
        {
            if ( logAction.who != null && logAction.who != player_type.SAURON )
            {
                logAction.who ^= 1;
            }

            if ( logAction.logActivePlayer != null && logAction.logActivePlayer.playerType != player_type.SAURON )
            {
                logAction.logActivePlayer.playerType ^= 1;
            }
            else if ( logAction.logTargetSelection )
            {
                this.transformLogTarget( logAction.logTargetSelection.from );
                this.transformLogTarget( logAction.logTargetSelection.to );
            }
            else if ( logAction.logCounter )
            {
                this.transformLogTarget( logAction.logCounter.target );
            }
            else if ( logAction.logSequence )
            {
                this.transformLogTarget( logAction.logSequence.target );
            }
            else if ( logAction.logSingleIcon )
            {
                this.transformLogTarget( logAction.logSingleIcon.target );
            }
            else if ( logAction.logCardFlip )
            {
                this.transformLogTarget( logAction.logCardFlip.target );
            }
        }
    }

        private transformLogTarget( logTarget: ISgLogTarget ): void
        {
            if ( logTarget.logTargetPlayer && logTarget.logTargetPlayer.playerType != player_type.SAURON )
            {
                logTarget.logTargetPlayer.playerType ^= 1;
            }
        }
}