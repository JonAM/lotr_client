import State from "../lib/state_machine/State";

import Config from "../Config";
import ServiceLocator from "../ServiceLocator";
import { sound_group_id } from "../service/AudioManager";
import Utils from "../Utils";

import HtmlFileLoader from "../lib/HtmlFileLoader";
import InfoMessageView from "../view/common/popup/InfoMessageView";
import Signal from "../lib/signals/Signal";
import ChatView from "../view/common/ChatView";
import PlayerProfileView from "../view/common/PlayerProfileView";
import PasswordResetView from "../view/common/PasswordResetView";
import PlayerReportView from "../view/common/PlayerReportView";
import FullScreenView from "../view/common/FullScreenView";
import { view_layer_id } from "../service/ViewManager";


export default class AppStartupState extends State
{
    // #region Attributes //

    // private:

    private _locale: string = null;
    private _htmlFileLoader: HtmlFileLoader = null;

    // Signals.
    private _onComplete: Signal = new Signal();

    // #endregion //


    // #region Properties //

    // Signals.
    public get onComplete(): Signal { return this._onComplete; }

    // #endregion //
    

    // #region Methods //

    // public:

    public onEnter(): void
    {
        super.onEnter();

        ServiceLocator.loadingScreen.show();

        this._locale = Utils.web.findLocale();

        document.addEventListener( "contextmenu", ( e ) => { 
            e.preventDefault(); 
            e.target.dispatchEvent( new Event( "rightclick" ) ); 

            return false; 
        } );

        // Global UI SFX.
        ServiceLocator.audioManager.load( [ {
            arrName: [ 
                "button_click", 
                "window_open", 
                "window_close",
                "counter_up",
                "counter_down" ], 
            soundGroupId: sound_group_id.SFX,
            arrExtension: [ "ogg", "mp3", "wav" ],
            basePath: "resources/sfx/common/" } ] );

        // Global stack. We won't pop it when leaving this state!
        ServiceLocator.resourceStack.push();

        this._htmlFileLoader = new HtmlFileLoader();
        this._htmlFileLoader.htmlFileInfoList = [ 
            { id: InfoMessageView.kResId, isPreloadImages: true },
            { id: FullScreenView.kResId, isPreloadImages: true },
            // 2nd level.
            { id: ChatView.kResId, isPreloadImages: false },
            { id: PlayerProfileView.kResId, isPreloadImages: false },
            { id: PlayerReportView.kResId, isPreloadImages: false },
            { id: PasswordResetView.kResId, isPreloadImages: false } ];
        this._htmlFileLoader.onCompleted.add( this.onHtmlFiles_Loaded, this );
        this._htmlFileLoader.init();
        this._htmlFileLoader.load();
    }

    // #endregion //


    // #region Callbacks //

    // private:

    private onHtmlFiles_Loaded(): void
    {
        // DB files.
        ServiceLocator.resourceLoader.add( "game_db", "resources/json/game_db.json" );
        ServiceLocator.resourceLoader.add( "scenario_db", "resources/json/scenario_db.json" );
        // Localisation file.
        ServiceLocator.resourceLoader.addOnce( "localisation", "i18n/" + this._locale + ".json" );
        // TTF fonts.
        const kArrFontId: string[] = [ "Roboto-Bold", "Roboto-Italic", "Roboto-Regular", "Roboto-BlackItalic" ];
        for ( const kFontId of kArrFontId )
        {
            ServiceLocator.resourceLoader.addOnce( kFontId, "resources/fonts/" + kFontId + ".ttf" );
        }
        // Custom cursor.
        ServiceLocator.resourceLoader.add( "custom_cursor_default", "resources/images/cursor/default.png" );
        ServiceLocator.resourceLoader.add( "custom_cursor_pointer", "resources/images/cursor/pointer.png" );
        //
        ServiceLocator.resourceLoader.load( this.onAssets_Loaded.bind( this ) )
    } 

    private onAssets_Loaded( resources: PIXI.IResourceDictionary ): void
    {
        // Init localisation.
        jQuery.i18n( { locale: this._locale } );
        jQuery.i18n().load( resources.localisation.data, this._locale );
        
        let fullScreenView: FullScreenView = new FullScreenView();
        fullScreenView.init();
        ServiceLocator.viewManager.fadeIn( fullScreenView, view_layer_id.HUD, true );

        this._onComplete.dispatch();
    }

    // #endregion //
}