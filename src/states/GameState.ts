import WebState from "./WebState";

import { game_state_id, main_state_id } from "./StateId";
import { action_scope_type, player_action_type } from "../service/socket_io/GameSocketIOController";
import { player_type } from "../game/component/world/CPlayerArea";
import ServiceLocator from "../ServiceLocator";
import Session from "../Session";
import { user_status_type } from "../service/socket_io/GatewaySocketIOController";
import { view_layer_id } from "../service/ViewManager";
import * as PIXI from "pixi.js";

import CGameWorld, { location_type } from "../game/component/world/CGameWorld";
import SetupState from "./game/SetupState";
import StateMachine from "../lib/StateMachine";
import GameObject from "../game/GameObject";
import CCardPreview from "../game/component/input/CCardPreview";
import CSprite from "../game/component/pixi/CSprite";
import TextStyler from "./game/TextStyler";
import CRadialMenu from "../game/component/ui/CRadialMenu";
import CContainer from "../game/component/pixi/CContainer";
import PRNG from "../lib/PRNG";
import AllyActionManager from "../game/AllyActionManager";
import InputController from "../game/InputController";
import TouchInputController from "../game/input/TouchInputController";
import PointerInputController from "../game/input/PointerInputController";
import InfoMessageView, { btn_layout_type } from "../view/common/popup/InfoMessageView";
import PhaseDiagramView from "../view/game/PhaseDiagramView";
import CContextMenu from "../game/component/ui/CContextMenu";
import CCardActivationArea from "../game/component/world/CCardActivationArea";
import CDropArea from "../game/component/input/CDropArea";
import CustomTextEditionView from "../view/game/card_token/CustomTextEditionView";
import CTextPreview from "../game/component/input/CTextPreview";
import PoiManager from "../game/PoiManager";
import CPoiMenu from "../game/component/ui/CPoiMenu";
import CLogActionPreview from "../game/component/input/CLogActionPreview";
import KeyBindingManager from "../game/KeyBindingManager";
import KeyBindingView from "../view/game/KeyBindingView";
import ProcessSequencer from "../game/ProcessSequencer";
import SaveGameView, { ISgGameState, ISgGamePhaseState } from "../view/game/SaveGameView";
import RoundIntroState from "./game/RoundIntroState";
import PhaseIntroState from "./game/PhaseIntroState";
import { ISoundLoadGroup, sound_group_id } from "../service/AudioManager";
import GameAssetLoader from "./GameAssetLoader";
import Signal from "../lib/signals/Signal";
import TooltipManager from "../game/TooltipManager";
import ChatView from "../view/common/ChatView";
import ResourceState from "./game/ResourceState";
import PlanningState from "./game/PlanningState";
import QuestState from "./game/QuestState";
import TravelState from "./game/TravelState";
import EncounterState from "./game/EncounterState";
import CombatState from "./game/CombatState";
import RefreshState from "./game/RefreshState";
import CustomCounterEditionView from "../view/game/card_token/CustomCounterEditionView";
import PhaseNavigator from "../game/PhaseNavigator";
import AttackBindingManager from "../game/AttackBindingManager";
import CImagePreview from "../game/component/input/CImagePreview";
import VictoryView from "../view/game/VictoryView";
import State from "../lib/state_machine/State";
import DragShadowManager from "../game/DragShadowManager";
import CGameLayerProvider, { layer_type } from "../game/component/world/CGameLayerProvider";
import Utils from "../Utils";
import TreasureListVO from "../vo/TreasureListVO";
import CampaignLogVO from "../vo/CampaignLogVO";
import CampaignLogView from "../view/common/CampaignLogView";
import { IScenario } from "../game/ScenarioDB";
import { IFileLoadInfo } from "../lib/HtmlFileLoader";
import CShareableGameElement from "../game/component/CShareableGameElement";
import ForceDragManager from "../game/ForceDragManager";
import GamePreferencesView from "../view/game/GamePreferencesView";
import CIsolatedArea from "../game/component/world/custom_area/CIsolatedArea";


export default class GameState extends WebState
{
    // #region Attributes //

    // private:

    private _hostPlayer: player_type = null;
    private _prngSeed: number = null;
    private _startingPlayer: player_type = null;

    private _app: PIXI.Application = null;
    private _stateMachine: StateMachine = null; 
    private _gameWorld: GameObject = null;
    private _cardPreview: GameObject = null;
    private _imagePreview: GameObject = null;
    private _logActionPreview: GameObject = null;
    private _textPreview: GameObject = null;
    private _cardActivationArea: GameObject = null;
    private _radialMenu: GameObject = null;
    private _poiMenu: GameObject = null;
    private _contextMenu: GameObject = null;
    private _textStyler: TextStyler = null;
    private _firstPlayer: player_type = null;
    private _activePlayer: player_type = null;
    private _mapForcedDiscardLocations: Map<string, location_type> = null;
    private _root: GameObject = null;
    private _dragShadowManager: DragShadowManager = null;
    private _poiManager: PoiManager = null;
    private _tooltipManager: TooltipManager = null;
    private _attackBindingManager: AttackBindingManager = null;
    private _allyActionManager: AllyActionManager = null;
    private _inputController: InputController = null;
    private _phaseNavigator: PhaseNavigator = null;
    private _keyBindingManager: KeyBindingManager = null;
    private _froceDragManager: ForceDragManager = null;
    private _processSequencer: ProcessSequencer = null;
    private _roundCount: number = 0;
    private _startTimestamp: number = null;
    private _arrSoundLoadGroup: Array<ISoundLoadGroup> = null;
    private _isAnimated: boolean = null;
    private _treasureListVO: TreasureListVO = null;
    private _campaignLogVO: CampaignLogVO = null;

    // Binded functions.
    private _bfOnDocumentVisibilityChanged: any = null;
    private _bfOnWindowResized: any = null;

    // Signals.
    private _onGameCreated: Signal = new Signal();
    private _onOngoingInteractionCanceled: Signal = new Signal();

    // Constants.
    private readonly _kArrPlayerColor: Array<number> = [ 0x008b00, 0x00008b, 0x8b0000 ]; // player, ally, sauron.

    // #endregion //


    // #region Properties //

    public get world(): GameObject { return this._gameWorld; }
    public get cGameWorld(): CGameWorld { return this._gameWorld.cGameWorld; }
    public get cardPreview(): GameObject { return this._cardPreview; }
    public get imagePreview(): GameObject { return this._imagePreview; }
    public get actionLogItemPreview(): GameObject { return this._logActionPreview; }
    public get textPreview(): GameObject { return this._textPreview; }
    public get cardActivationArea(): GameObject { return this._cardActivationArea; }
    public get radialMenu(): GameObject { return this._radialMenu; }
    public get cRadialMenu(): CRadialMenu { return this._radialMenu.cRadialMenu; }
    public get poiMenu(): GameObject { return this._poiMenu; }
    public get cContextMenu(): CContextMenu { return this._contextMenu.cContextMenu; }
    public get textStyler(): TextStyler { return this._textStyler; }
    public get stateMachine(): StateMachine { return this._stateMachine; }
    public get hostPlayer(): player_type { return this._hostPlayer; }
    public get startingPlayer(): player_type { return this._startingPlayer; }
    public get firstPlayer(): player_type { return this._firstPlayer; }
    public get activePlayer(): player_type { return this._activePlayer; }
    public get forcedDiscardLocations(): Map<string, location_type> { return this._mapForcedDiscardLocations; }
    public get root(): GameObject { return this._root; }
    public get dragShadowManager(): DragShadowManager { return this._dragShadowManager; }
    public get allyActionManager(): AllyActionManager { return this._allyActionManager}
    public get inputController(): InputController { return this._inputController; }
    public get app(): PIXI.Application { return this._app; }
    public get poiManager(): PoiManager { return this._poiManager; }
    public get tooltipManager(): TooltipManager { return this._tooltipManager; }
    public get attackBindingManager(): AttackBindingManager { return this._attackBindingManager; }
    public get phaseNavigator(): PhaseNavigator { return this._phaseNavigator; }
    public get keyBindingManager(): KeyBindingManager { return this._keyBindingManager; }
    public get processSequencer(): ProcessSequencer { return this._processSequencer; }
    public get roundCount(): number { return this._roundCount; }
    public get startTimestamp(): number { return this._startTimestamp; }
    public get isAnimated(): boolean { return this._isAnimated; }
    public get treasureList(): TreasureListVO { return this._treasureListVO; }
    public get campaignLog(): CampaignLogVO { return this._campaignLogVO; }
    public get playerColors(): Array<number> { return this._kArrPlayerColor; }

    public set hostPlayer( value: player_type ) { this._hostPlayer = value; }
    public set prngSeed( value: number ) { this._prngSeed = value; }
    public set startingPlayer( value: player_type ) { this._startingPlayer = value; }
    public set firstPlayer( value: player_type ) { this._firstPlayer = value; }
    public set roundCount( value: number ) { this._roundCount = value; }
    public set startTimestamp( value: number ) { this._startTimestamp = value; }
    public set isAnimated( value: boolean ) { this._isAnimated = value; }
    public set treasureList( value: TreasureListVO ) { this._treasureListVO = value; }
    public set campaignLog( value: CampaignLogVO ) { this._campaignLogVO = value; }

    // Signals.
    public get onGameCreated(): Signal { return this._onGameCreated; }
    public get onOngoingInteractionCanceled(): Signal { return this._onOngoingInteractionCanceled; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor() 
    { 
        super(); 

        // Binded functions.
        this._bfOnWindowResized = this.onWindow_Resized.bind( this );
        this._bfOnDocumentVisibilityChanged = this.onDocumentVisibility_Changed.bind ( this );
    }

    public onEnter(): void
    {
        super.onEnter();

        console.assert( this._hostPlayer != null, "GameState.ts :: onEnter() :: this._hostPlayer cannot be null." );
        console.assert( this._prngSeed != null, "GameState.ts :: onEnter() :: this._prngSeed cannot be null." );
        console.assert( this._startingPlayer != null, "GameState.ts :: onEnter() :: this._startingPlayer cannot be null." );

        GameObject.oidCounter = 0;
        this._isAnimated = false;

        this._arrSoundLoadGroup = new Array<ISoundLoadGroup>();
        this._firstPlayer = this._startingPlayer;

        this._mapForcedDiscardLocations = new Map<string, location_type>();

        this._phaseNavigator = new PhaseNavigator();
        this._phaseNavigator.init();

        this._keyBindingManager = new KeyBindingManager();
        this._keyBindingManager.bindings = ServiceLocator.savedData.data.keyBindings;
        this._keyBindingManager.init();

        this._froceDragManager = new ForceDragManager();
        this._froceDragManager.keyBidingManager = this._keyBindingManager;
        this._froceDragManager.init();

        // TEST: SKIP:
        let chatView: ChatView = ServiceLocator.viewManager.findViewByResId( ChatView.kResId )[ 0 ] as ChatView;
        chatView.listenToBindedKeys();
        chatView.onMyMessageFocusIn.add( this.onMyChatMessage_FocusIn, this );
        chatView.onMyMessageFocusOut.add( this.onMyChatMessage_FocusOut, this );
        //

        this._processSequencer = new ProcessSequencer();
        this._processSequencer.init();

        this._allyActionManager = new AllyActionManager();
        this._allyActionManager.init();
        this._allyActionManager.pause();

        this._roundCount = 0;

        ServiceLocator.prng = new PRNG( this._prngSeed );

        // Create the game.
        this._app = new PIXI.Application( { 
            width: 1920,
            height: 1080 } );
        let jqGameContainer: JQuery<HTMLElement> = jQuery( "<div id=\"gameContainer\"></div>" );
        jqGameContainer.css( {
            width: "100%",
            height: "100%",
            position: "fixed",
            display: "flex",
            "justify-content": "center",
            "align-items": "center" } );
        jqGameContainer.append( this._app.view );
        //this._app.view.addEventListener( "contextmenu", ( e ) => { e.preventDefault(); return false; } );
        jQuery( "#mainLayer" ).append( jqGameContainer );
        this.onWindow_Resized();

        this._app.renderer.plugins.interaction.cursorStyles.default = "url('../resources/images/cursor/default.png'),auto";
        this._app.renderer.plugins.interaction.cursorStyles.pointer = "url('../resources/images/cursor/pointer.png'),auto";

        this._textStyler = new TextStyler();

        this._root = new GameObject( [ new CContainer(), new CGameLayerProvider() ] );
        this._root.oid = "root";
        this._root.cGameLayerProvider.layerTypes = [ 
            layer_type.UNDER_TABLE, 
            layer_type.GAME, 
            layer_type.CUSTOM_PANEL,
            layer_type.CARD_ACTIVATION_DETECTION,
            layer_type.ACTIVATED_CARD,
            layer_type.TARGET_SELECTION,
            layer_type.HUD,
            layer_type.VFX,
            layer_type.CARD_PREVIEW,
            layer_type.CARD_PRESENTATION,
            layer_type.POPUP,
            layer_type.TOOLTIP,
            layer_type.DISABLED,
            layer_type.DEBUG ];
        this._root.init();
        this._app.stage.addChild( this._root.cContainer.c );

        this._inputController = this._app.renderer.plugins.interaction.supportsTouchEvents ? new TouchInputController() : new PointerInputController();
        this._inputController.stage = this._app.stage;
        this._inputController.init();

        this._dragShadowManager = new DragShadowManager();
        this._dragShadowManager.stage = this._app.stage;
        this._dragShadowManager.init();

        this._poiManager = new PoiManager();
        this._poiManager.init();

        this._attackBindingManager = new AttackBindingManager();
        this._attackBindingManager.init();

        this._tooltipManager = new TooltipManager();
        this._tooltipManager.stage = this._app.stage;
        this._tooltipManager.tooltipLayer = this._root.cGameLayerProvider.get( layer_type.TOOLTIP );
        this._tooltipManager.init();

        this._cardPreview = new GameObject( [ new CContainer(), new CCardPreview() ] );
        this._cardPreview.init();
        ServiceLocator.game.root.cGameLayerProvider.add( this._cardPreview, layer_type.CARD_PREVIEW );

        this._imagePreview = new GameObject( [ new CContainer(), new CImagePreview() ] );
        this._imagePreview.init();
        ServiceLocator.game.root.cGameLayerProvider.add( this._imagePreview, layer_type.CARD_PREVIEW );

        this._logActionPreview = new GameObject( [ new CContainer(), new CLogActionPreview() ] );
        this._logActionPreview.init();
        ServiceLocator.game.root.cGameLayerProvider.add( this._logActionPreview, layer_type.CARD_PREVIEW );

        this._textPreview = new GameObject( [ new CContainer(), new CTextPreview() ] );
        this._textPreview.init();
        ServiceLocator.game.root.cGameLayerProvider.add( this._textPreview, layer_type.CARD_PREVIEW );

        this._stateMachine = new StateMachine();
        // 1st level.
        this._stateMachine.add( game_state_id.SETUP, new SetupState() );
        this._stateMachine.add( game_state_id.RESOURCE, new ResourceState() );
        this._stateMachine.add( game_state_id.PLANNING, new PlanningState() );
        this._stateMachine.add( game_state_id.QUEST, new QuestState() );
        this._stateMachine.add( game_state_id.TRAVEL, new TravelState() );
        this._stateMachine.add( game_state_id.ENCOUNTER, new EncounterState() );
        this._stateMachine.add( game_state_id.COMBAT, new CombatState() );
        this._stateMachine.add( game_state_id.REFRESH, new RefreshState() );
        // Shared.
        this._stateMachine.add( game_state_id.ROUND_INTRO, new RoundIntroState() );
        this._stateMachine.add( game_state_id.PHASE_INTRO, new PhaseIntroState() );
        this._stateMachine.init();

        // Listen to events.
        jQuery( window ).on( "resize", this._bfOnWindowResized );
        ServiceLocator.socketIOManager.onReconnect.add( this.onReconnect, this );
        ServiceLocator.socketIOManager.onDisconnect.add( this.onDisconnect, this );
        ServiceLocator.socketIOManager.game.onOpponentActionNotified.add( this.onAllyAction_Notified, this );
        ServiceLocator.socketIOManager.game.onGameInvalidated.add( this.onGame_Invalidated, this );
        ServiceLocator.socketIOManager.game.onGameResumed.add( this.onGame_Resumed, this );
        ServiceLocator.socketIOManager.gateway.onUserDisconnected.add( this.onUser_Disconnected, this );

        // Load HTML files.
        let arrHtmlFileInfo: Array<IFileLoadInfo> = [
            // 1st level.
            { id: PhaseDiagramView.kResId, isPreloadImages: false },
            { id: SaveGameView.kResId, isPreloadImages: false },
            { id: VictoryView.kResId, isPreloadImages: false },
            { id: CustomTextEditionView.kResId, isPreloadImages: false },
            { id: CustomCounterEditionView.kResId, isPreloadImages: false },
            { id: GamePreferencesView.kResId, isPreloadImages: false },
            { id: KeyBindingView.kResId, isPreloadImages: false } ];
        const kScenarioInfo: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
        if ( kScenarioInfo.play_area && kScenarioInfo.play_area.custom_panels && kScenarioInfo.play_area.custom_panels.indexOf( "campaign_log" ) != -1 )
        {
            arrHtmlFileInfo.push( { id: CampaignLogView.kResId, isPreloadImages: false } );
        }
        this._htmlFileLoader.htmlFileInfoList = arrHtmlFileInfo;
        this._htmlFileLoader.load();
    }

    public onLeave(): void
    {
        this._onGameCreated.removeAll();
        this._onOngoingInteractionCanceled.removeAll();

        // TEST: SKIP:
        let chatView: ChatView = ServiceLocator.viewManager.findViewByResId( ChatView.kResId )[ 0 ] as ChatView;
        chatView.onMyMessageFocusIn.remove( this.onMyChatMessage_FocusIn, this );
        chatView.onMyMessageFocusOut.remove( this.onMyChatMessage_FocusOut, this );
        //

        // Cleanup loaded sounds.
        ServiceLocator.audioManager.stopMusic();
        for ( let soundLoadGroup of this._arrSoundLoadGroup )
        {
            ServiceLocator.audioManager.removeSounds( soundLoadGroup.arrName, soundLoadGroup.soundGroupId );
        }
        this._arrSoundLoadGroup = null;

        // Cleanup events.
        jQuery( window ).off( "resize", this._bfOnWindowResized );
        ServiceLocator.socketIOManager.onReconnect.remove( this.onReconnect, this );
        ServiceLocator.socketIOManager.onDisconnect.remove( this.onDisconnect, this );
        ServiceLocator.socketIOManager.game.onOpponentActionNotified.remove( this.onAllyAction_Notified, this );
        ServiceLocator.socketIOManager.game.onGameInvalidated.remove( this.onGame_Invalidated, this );
        ServiceLocator.socketIOManager.game.onGameResumed.remove( this.onGame_Resumed, this );
        ServiceLocator.socketIOManager.gateway.onUserDisconnected.remove( this.onUser_Disconnected, this );

        document.removeEventListener( ServiceLocator.browserDependency.visibilityApi.visibilityChange, this._bfOnDocumentVisibilityChanged, false );

        ServiceLocator.socketIOManager.game.gameOver();

        this._stateMachine.end();
        this._stateMachine = null;

        this._radialMenu.end();
        this._radialMenu = null;

        this._poiMenu.end();
        this._poiMenu = null;

        this._contextMenu.end();
        this._contextMenu = null;

        this._poiManager.end();
        this._poiManager = null;

        this._tooltipManager.end();
        this._tooltipManager = null;

        this._attackBindingManager.end();
        this._attackBindingManager = null;

        this._cardActivationArea.end();
        this._cardActivationArea = null;

        this._cardPreview.end();
        this._cardPreview = null;

        this._imagePreview.end();
        this._imagePreview = null;

        this._logActionPreview.end();
        this._logActionPreview = null;

        this._textPreview.end();
        this._textPreview = null;

        this._gameWorld.end();
        this._gameWorld = null;

        this._inputController.end();
        this._inputController = null;

        this._dragShadowManager.end();
        this._dragShadowManager = null;

        this._root.end();
        this._root = null;

        this._allyActionManager.end();
        this._allyActionManager = null;

        this._phaseNavigator.end();
        this._phaseNavigator = null;

        this._froceDragManager.end();
        this._froceDragManager = null;

        this._keyBindingManager.end();
        this._keyBindingManager = null;

        this._processSequencer.end();
        this._processSequencer = null;

        this._roundCount = null;

        this._treasureListVO = null;
        this._campaignLogVO = null;

        this._app.destroy( true );
        this._app = null;

        // Offline game.
        if ( !Session.allyId )
        {
            ServiceLocator.socketIOManager.gateway.setUserStatus( user_status_type.IDLE );
        }

        Session.reset();
        ServiceLocator.viewManager.clearLayer( view_layer_id.POPUP ); 

        ServiceLocator.cardDb.removeCustomCards();

        GameObject.reset();
    
        super.onLeave();
    }

    public onUpdate( dt: number ): void
    {
        this._stateMachine.onUpdate( dt );
        this._dragShadowManager.update( dt );
        this._processSequencer.update( dt );
        this._allyActionManager.update( dt );
        this._inputController.update( dt );
    }

    public setActivePlayer( playerType: player_type ): void
    {
        if ( this._activePlayer == playerType ) { return; }

        this._activePlayer = playerType;

        if ( playerType == player_type.PLAYER ) 
        {
            ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cActivePlayerBtn.show();
            ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cActivePlayerBtn.hide();
        }
        else
        {
            ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cActivePlayerBtn.hide();
            ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cActivePlayerBtn.show();
        }

        // Log.
        ServiceLocator.game.cGameWorld.cActionLogger.logActivePlayer( playerType );
    }

    public restart(): void
    {
        this._isAnimated = false;

        ServiceLocator.loadingScreen.show();
        // Let's create a bit of drama...
        window.setTimeout( () => { 
            ServiceLocator.loadingScreen.hide();   

            ServiceLocator.game.stateMachine.onStateEntered.addOnce( ( state: State ) => {
                let phaseIntroState: PhaseIntroState = state as PhaseIntroState;
                phaseIntroState.nextStateId = game_state_id.SETUP; } );
            ServiceLocator.game.stateMachine.request( game_state_id.PHASE_INTRO );
        }, 2000 );
        
        ServiceLocator.audioManager.stopMusic();
        ServiceLocator.viewManager.clearLayer( view_layer_id.POPUP ); 

        this._mapForcedDiscardLocations.clear();

        this._stateMachine.end();
        this._stateMachine = new StateMachine();
        // 1st level.
        this._stateMachine.add( game_state_id.SETUP, new SetupState() );
        this._stateMachine.add( game_state_id.RESOURCE, new ResourceState() );
        this._stateMachine.add( game_state_id.PLANNING, new PlanningState() );
        this._stateMachine.add( game_state_id.QUEST, new QuestState() );
        this._stateMachine.add( game_state_id.TRAVEL, new TravelState() );
        this._stateMachine.add( game_state_id.ENCOUNTER, new EncounterState() );
        this._stateMachine.add( game_state_id.COMBAT, new CombatState() );
        this._stateMachine.add( game_state_id.REFRESH, new RefreshState() );
        // Shared.
        this._stateMachine.add( game_state_id.ROUND_INTRO, new RoundIntroState() );
        this._stateMachine.add( game_state_id.PHASE_INTRO, new PhaseIntroState() );
        this._stateMachine.init();

        this._radialMenu.cRadialMenu.hide();
        this._poiMenu.cPoiMenu.hide();
        this._contextMenu.cContextMenu.close();
        this._cardPreview.cCardPreview.hide();
        this._imagePreview.cImagePreview.hide();
        this._logActionPreview.cLogActionPreview.hide();;
        this._textPreview.cTextPreview.hide();

        this._cardActivationArea.end();
        this._cardActivationArea = null;

        this._dragShadowManager.end();
        this._dragShadowManager = null;

        this._poiManager.end();
        this._poiManager = null;

        this._tooltipManager.end();
        this._tooltipManager = null;

        this._attackBindingManager.end();
        this._attackBindingManager = null;

        this._gameWorld.end();
        this._gameWorld = null;

        this._inputController.end();
        this._inputController = null;

        this._roundCount = 0;

        this._processSequencer.end();
        this._processSequencer = new ProcessSequencer();
        this._processSequencer.init();

        this._allyActionManager.end();
        this._allyActionManager = new AllyActionManager();
        this._allyActionManager.init();
        this._allyActionManager.pause();

        GameObject.reset();

        this._root.cGameLayerProvider.reset();
        //
        ServiceLocator.game.root.cGameLayerProvider.add( this._cardPreview, layer_type.CARD_PREVIEW );
        ServiceLocator.game.root.cGameLayerProvider.add( this._imagePreview, layer_type.CARD_PREVIEW );
        ServiceLocator.game.root.cGameLayerProvider.add( this._textPreview, layer_type.CARD_PREVIEW );
        ServiceLocator.game.root.cGameLayerProvider.add( this._logActionPreview, layer_type.CARD_PREVIEW );
        ServiceLocator.game.root.cGameLayerProvider.add( this._radialMenu, layer_type.POPUP );
        ServiceLocator.game.root.cGameLayerProvider.add( this._poiMenu, layer_type.POPUP );
        ServiceLocator.game.root.cGameLayerProvider.add( this._contextMenu, layer_type.POPUP );

        this._inputController = this._app.renderer.plugins.interaction.supportsTouchEvents ? new TouchInputController() : new PointerInputController();
        this._inputController.stage = this._app.stage;
        this._inputController.init();

        this._dragShadowManager = new DragShadowManager();
        this._dragShadowManager.stage = this._app.stage;
        this._dragShadowManager.init();

        this._poiManager = new PoiManager();
        this._poiManager.init();

        this._tooltipManager = new TooltipManager();
        this._tooltipManager.stage = this._app.stage;
        this._tooltipManager.tooltipLayer = this._root.cGameLayerProvider.get( layer_type.TOOLTIP );
        this._tooltipManager.init();

        this._attackBindingManager = new AttackBindingManager();
        this._attackBindingManager.init();

        this._firstPlayer = this._startingPlayer;

        this._gameWorld = new GameObject( [ new CSprite(), new CGameWorld(), new CGameLayerProvider() ] );
        this._gameWorld.oid = "game_world";
        this._gameWorld.cGameLayerProvider.layerTypes = [ 
            layer_type.ATTACK_BINDING_LINK, 
            layer_type.ATTACK_BINDING_COUNTER,
            layer_type.POI, 
            layer_type.VFX ];
        this._gameWorld.init();
        this._root.cGameLayerProvider.add( this._gameWorld, layer_type.GAME );

        this._cardActivationArea = new GameObject( [ new CSprite(), new CCardActivationArea(), new CDropArea(), new CShareableGameElement() ] );
        this._cardActivationArea.oid = "card_activation_area";
        this._cardActivationArea.cDropArea.target = this._cardActivationArea.cCardActivationArea;
        this._cardActivationArea.init();
        ServiceLocator.game.root.cGameLayerProvider.add( this._cardActivationArea, layer_type.CARD_ACTIVATION_DETECTION );


        this._isAnimated = true;
    }

    // overrides.
    public saveGame(): ISgGameState
    {
        let gamePhaseState: ISgGamePhaseState = { stateId: this._stateMachine.currentStateId };
        switch ( this._stateMachine.currentStateId )
        {
            case game_state_id.RESOURCE: { gamePhaseState.resource = this.stateMachine.currentState.saveGame(); break; }
            case game_state_id.PLANNING: { gamePhaseState.planning = this.stateMachine.currentState.saveGame(); break; }
            case game_state_id.QUEST: { gamePhaseState.quest = this.stateMachine.currentState.saveGame(); break; }
            case game_state_id.TRAVEL: { gamePhaseState.travel = this.stateMachine.currentState.saveGame(); break; }
            case game_state_id.ENCOUNTER: { gamePhaseState.encounter = this.stateMachine.currentState.saveGame(); break; }
            case game_state_id.COMBAT: { gamePhaseState.combat = this.stateMachine.currentState.saveGame(); break; }
            case game_state_id.REFRESH: { gamePhaseState.refresh = this.stateMachine.currentState.saveGame(); break; }
        }

        return {
            startingPlayer: this._startingPlayer,
            firstPlayer: this._firstPlayer,
            activePlayer: this._activePlayer,
            roundCount: this._roundCount,
            forcedDiscardLocations: JSON.stringify( Array.from( this._mapForcedDiscardLocations.entries() ) ),
            gameWorld: this._gameWorld.cGameWorld.saveGame(),
            phaseNavigator: this._phaseNavigator.saveGame(),
            attackBindingManager: this._attackBindingManager.saveGame(),
            gamePhaseState: gamePhaseState, 
            startTimestamp: this._startTimestamp };
    }

    public loadGame( sgGameState: ISgGameState, pass: number ): void
    {
        this._isAnimated = false;

        if ( pass == 0 )
        {
            this._startingPlayer = sgGameState.startingPlayer;
            if ( this._firstPlayer != sgGameState.firstPlayer )
            {
                let firstPlayerPlayArea: GameObject = sgGameState.firstPlayer == player_type.PLAYER ? this._gameWorld.cGameWorld.playerArea : this._gameWorld.cGameWorld.allyArea;
                firstPlayerPlayArea.cPlayerArea.sideControls.cDropArea.forceDrop( this._gameWorld.cGameWorld.firstPlayerToken, null, action_scope_type.LOCAL );
            }
            this._activePlayer = sgGameState.activePlayer; 
            this._roundCount = sgGameState.roundCount; 
            this._startTimestamp = sgGameState.startTimestamp;
            this._mapForcedDiscardLocations = new Map<string, location_type>( JSON.parse( sgGameState.forcedDiscardLocations ) );

            this._gameWorld.cGameWorld.loadGame( sgGameState.gameWorld, pass );
            this._phaseNavigator.loadGame( sgGameState.phaseNavigator, pass );
            this._attackBindingManager.loadGame( sgGameState.attackBindingManager, pass );

            if ( Session.allyId && Utils.game.isAllyIsolated() )
            {
                ServiceLocator.game.cGameWorld.cSauronArea.cStagingThreatCounter.onStagingAreaSplit();
                let cIsolatedArea: CIsolatedArea = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "isolated" ).cIsolatedArea;
                ServiceLocator.game.cGameWorld.cSauronArea.cPlayerWillpowerCounter.onStagingAreaSplit( cIsolatedArea.allyWillpowerCounter );
                ServiceLocator.game.cGameWorld.cSauronArea.cSauronWillpowerCounter.onStagingAreaSplit( cIsolatedArea.sauronWillpowerCounter );
            }
        }
        else if ( pass == 1 )
        {
            let sgState: any = null;
            switch ( sgGameState.gamePhaseState.stateId )
            {
                case game_state_id.RESOURCE: { sgState = sgGameState.gamePhaseState.resource; break; }
                case game_state_id.PLANNING: { sgState = sgGameState.gamePhaseState.planning; break; }
                case game_state_id.QUEST: { sgState = sgGameState.gamePhaseState.quest; break; }
                case game_state_id.TRAVEL: { sgState = sgGameState.gamePhaseState.travel; break; }
                case game_state_id.ENCOUNTER: { sgState = sgGameState.gamePhaseState.encounter; break; }
                case game_state_id.COMBAT: { sgState = sgGameState.gamePhaseState.combat; break; }
                case game_state_id.REFRESH: { sgState = sgGameState.gamePhaseState.refresh; break; }
            }
            this._stateMachine.load( sgGameState.gamePhaseState.stateId, sgState, pass );
        }

        this._isAnimated = true;
    }

    // #endregion //


    // #region Sync Callbacks //

    private onAllyAction_Notified( playerActionType: player_action_type, gameStateId: game_state_id, args: Array<any> ): void
    {
        this._allyActionManager.addAction( playerActionType, gameStateId, args );
    }

    private onReconnect(): void
    {
        if ( Session.allyId && ServiceLocator.viewManager.findViewByResId( VictoryView.kResId ).length == 0 )
        {
            ServiceLocator.socketIOManager.game.tryGameReconnection( Session.gameId );
        }
    }

    private onDisconnect(): void
    {
        if ( Session.allyId && ServiceLocator.viewManager.findViewByResId( VictoryView.kResId ).length == 0 )
        {
            let infoMessageView: InfoMessageView = new InfoMessageView();
            infoMessageView.message = jQuery.i18n( "CONNECTION_LOST" ) + jQuery.i18n( "REMEMBER_SAVE_GAME" );
            infoMessageView.btnLayoutType = btn_layout_type.SINGLE;
            infoMessageView.init();
            if ( this._stateMachine.currentStateId == game_state_id.SETUP )
            {
                infoMessageView.root.find( "#infoMessageControls" ).hide();
            }
            else
            {
                infoMessageView.root.find( "#infoMessageAcceptBtn" ).text( jQuery.i18n( "SAVE" ) )
                    .off( "click" )
                    .on( "click", () => {
                            let saveGameView: SaveGameView = new SaveGameView();
                            saveGameView.viewLayerId = view_layer_id.TOPMOST;
                            saveGameView.init();
                            ServiceLocator.viewManager.fadeIn( saveGameView, view_layer_id.TOPMOST );
                        } );
            }
            ServiceLocator.viewManager.fadeIn( infoMessageView, view_layer_id.TOPMOST );
        }
    }

    private onGame_Invalidated(): void
    {
        if ( Session.allyId && ServiceLocator.viewManager.findViewByResId( VictoryView.kResId ).length == 0 )
        {
            let infoMessageView: InfoMessageView = new InfoMessageView();
            infoMessageView.message = jQuery.i18n( "GAME_INVALIDATED" );
            infoMessageView.btnLayoutType = btn_layout_type.DOUBLE;
            infoMessageView.onClosed.addOnce( this.onGameInvalidatedPopup_Closed, this );
            infoMessageView.init();
            if ( this._stateMachine.currentStateId == game_state_id.SETUP )
            {
                infoMessageView.root.find( "#infoMessageCancelBtn" ).hide();
            }
            else
            {
                infoMessageView.root.find( "#infoMessageCancelBtn" ).text( jQuery.i18n( "SAVE" ) )
                    .off( "click" )
                    .on( "click", () => {
                            let saveGameView: SaveGameView = new SaveGameView();
                            saveGameView.viewLayerId = view_layer_id.TOPMOST;
                            saveGameView.init();
                            ServiceLocator.viewManager.fadeIn( saveGameView, view_layer_id.TOPMOST );
                        } );
            }
            ServiceLocator.viewManager.fadeIn( infoMessageView, view_layer_id.TOPMOST );
        }
    }

    private onGameInvalidatedPopup_Closed(): void
    {
        ServiceLocator.viewManager.clearLayer( view_layer_id.POPUP );
        ServiceLocator.viewManager.clearLayer( view_layer_id.TOPMOST );

        ServiceLocator.stateMachine.request( main_state_id.GATEWAY );
    }

    private onOpponentDisconnectedPopup_Closed( result: boolean ): void
    {
        if ( result )
        {
            this.onGameInvalidatedPopup_Closed();
        }
    }

    private onGame_Resumed(): void
    {
        ServiceLocator.viewManager.clearLayer( view_layer_id.POPUP );
        ServiceLocator.viewManager.clearLayer( view_layer_id.TOPMOST );
    }

    private onUser_Disconnected( userId: string ): void
    {
        if ( userId == Session.allyId )
        {
            let infoMessageView: InfoMessageView = new InfoMessageView();
            infoMessageView.message = jQuery.i18n( "ALLY_DISCONNECTED" );
            infoMessageView.btnLayoutType = btn_layout_type.DOUBLE;
            infoMessageView.onClosed.addOnce( this.onOpponentDisconnectedPopup_Closed, this );
            infoMessageView.init();
            infoMessageView.root.find( "#infoMessageAcceptBtn" ).text( jQuery.i18n( "LEAVE" ) );
            if ( this._stateMachine.currentStateId == game_state_id.SETUP )
            {
                infoMessageView.root.find( "#infoMessageCancelBtn" ).hide();
            }
            else
            {
                infoMessageView.root.find( "#infoMessageCancelBtn" ).text( jQuery.i18n( "SAVE" ) )
                    .off( "click" )
                    .on( "click", () => {
                            let saveGameView: SaveGameView = new SaveGameView();
                            saveGameView.viewLayerId = view_layer_id.TOPMOST;
                            saveGameView.init();
                            ServiceLocator.viewManager.fadeIn( saveGameView, view_layer_id.TOPMOST );
                        } );
            }
            ServiceLocator.viewManager.fadeIn( infoMessageView, view_layer_id.TOPMOST );
        }
    }

    // #endregion //


    // #region Input Callbacks //

    private onWindow_Resized(): void
    {
        const kRatio: number = this._app.renderer.width / this._app.renderer.height;
        let w: number = null;
        let h: number = null;
        if ( window.innerWidth / window.innerHeight >= kRatio ) 
        {
            w = window.innerHeight * kRatio;
            h = window.innerHeight;
        } 
        else 
        {
            w = window.innerWidth;
            h = window.innerWidth / kRatio;
        }
        this._app.view.style.width = w.toString() + "px";
        this._app.view.style.height = h.toString() + "px";
    }

    // #endregion //


    // #region Other Callbacks //

    protected onHtmlFiles_Loaded(): void
    {
        let gameAssetLoader: GameAssetLoader = new GameAssetLoader();
        gameAssetLoader.onCompleted.addOnce( this.onGameAssets_Added, this );
        gameAssetLoader.addResources( ServiceLocator.resourceLoader );
    }

        private onGameAssets_Added( arrRequiredTextureMapId: Array<string> ): void
        {
            ServiceLocator.resourceLoader.onProgress.add( this.onGameAssetLoaderProgress_Updated, this );
            ServiceLocator.resourceLoader.load( this.onGameAssets_Loaded.bind( this, arrRequiredTextureMapId ) );
        }

            private onGameAssetLoaderProgress_Updated( progress: number ): void
            {
                ServiceLocator.loadingScreen.setMessage( jQuery.i18n( "LOADING" ) + " (" + Math.ceil( progress ).toString() + "%)" );
            }

            private onGameAssets_Loaded( arrRequiredTextureMapId: Array<string> ): void
            {
                if ( ServiceLocator.resourceCache && arrRequiredTextureMapId.length > 0 )
                {
                    ServiceLocator.resourceCache.onTextureMapsCached.addOnce( this.createGame, this );
                    ServiceLocator.resourceCache.cacheTextureMaps( arrRequiredTextureMapId, arrRequiredTextureMapId.length );
                }
                else
                {
                    this.createGame();
                }
            }

                private createGame(): void
                {
                    ServiceLocator.resourceLoader.onProgress.remove( this.onGameAssetLoaderProgress_Updated, this );

                    // Create sound load groups.
                    this._arrSoundLoadGroup = [ 
                        { // sfx.
                            arrExtension: [ "ogg", "mp3", "wav" ],
                            soundGroupId: sound_group_id.SFX,
                            basePath: "resources/sfx/game/",
                            arrName: [ 
                                "card_face_down", 
                                "card_face_up", 
                                "card_played",
                                "phase_started",
                                "poi_created",
                                "round_started",
                                "token_dropped",
                                "player_activated",
                                "wounded",
                                "dead",
                                "attack_blocked",
                                "attack_target_selected",
                                "explored",
                                "equipped",
                                "flying_orb_launch_0",
                                "flying_orb_launch_1",
                                "flying_orb_launch_2" ] 
                        },
                        { // music.
                            arrExtension: [ "ogg", "mp3" ],
                            soundGroupId: sound_group_id.MUSIC,
                            basePath: "resources/music/game/",
                            arrName: [ 
                                "setup_phase", 
                                "resource_phase", 
                                "planning_phase", 
                                "quest_phase",
                                "travel_phase",
                                "encounter_phase",
                                "combat_sauron_phase",
                                "combat_player_phase",
                                "refresh_phase",
                                "victory_screen" ]
                        } ];
                    // Load sounds.
                    ServiceLocator.audioManager.load( this._arrSoundLoadGroup );
                    
                    this._gameWorld = new GameObject( [ new CSprite(), new CGameWorld(), new CGameLayerProvider() ] );
                    this._gameWorld.oid = "game_world";
                    this._gameWorld.cGameLayerProvider.layerTypes = [ 
                        layer_type.ATTACK_BINDING_LINK, 
                        layer_type.ATTACK_BINDING_COUNTER, 
                        layer_type.POI, 
                        layer_type.VFX ];
                    this._gameWorld.init();
                    this._root.cGameLayerProvider.add( this._gameWorld, layer_type.GAME );

                    this._cardActivationArea = new GameObject( [ new CSprite(), new CCardActivationArea(), new CDropArea(), new CShareableGameElement() ] );
                    this._cardActivationArea.oid = "card_activation_area";
                    this._cardActivationArea.cDropArea.target = this._cardActivationArea.cCardActivationArea;
                    this._cardActivationArea.init();
                    ServiceLocator.game.root.cGameLayerProvider.add( this._cardActivationArea, layer_type.CARD_ACTIVATION_DETECTION );

                    this._radialMenu = new GameObject( [ new CContainer(), new CRadialMenu() ] );
                    this._radialMenu.init();
                    ServiceLocator.game.root.cGameLayerProvider.add( this._radialMenu, layer_type.POPUP );

                    this._poiMenu = new GameObject( [ new CContainer(), new CPoiMenu() ] );
                    this._poiMenu.init();
                    ServiceLocator.game.root.cGameLayerProvider.add( this._poiMenu, layer_type.POPUP );

                    this._contextMenu = new GameObject( [ new CContainer(), new CContextMenu() ] );
                    this._contextMenu.init();
                    ServiceLocator.game.root.cGameLayerProvider.add( this._contextMenu, layer_type.POPUP );

                    document.addEventListener( ServiceLocator.browserDependency.visibilityApi.visibilityChange, this._bfOnDocumentVisibilityChanged, false );

                    this._isAnimated = true;

                    this._onGameCreated.dispatch();
                }

    private onDocumentVisibility_Changed(): void
    {
        if ( !document[ ServiceLocator.browserDependency.visibilityApi.hidden ] )
        {
            if ( !this._allyActionManager.isPaused )
            {
                this._allyActionManager.pause();
                this._stateMachine.pause();

                if ( this._allyActionManager.isStreamMode )
                {
                    this._stateMachine.resume();
                }
                else
                {
                    let infoMessageView: InfoMessageView = new InfoMessageView();
                    infoMessageView.message = jQuery.i18n( "SYNCHRONIZING_GAME_STATE" );
                    infoMessageView.init();
                    infoMessageView.root.find( ".controls" ).hide();
                    ServiceLocator.viewManager.fadeIn( infoMessageView, view_layer_id.TOPMOST );
                    
                    ServiceLocator.viewManager.onViewFadedIn.addOnce( ( view: InfoMessageView ) => {
                        function onFlushEnded(): void
                        {
                            this._allyActionManager.resume();
                            
                            this._stateMachine.resume();
                
                            ServiceLocator.viewManager.fadeOut( infoMessageView ); 
                        };
                        this._allyActionManager.onFlushEnded.addOnce( onFlushEnded, this );
                        this._allyActionManager.flush();
                    } );
                }
            }
        }
    }

    private onMyChatMessage_FocusIn(): void
    {
        this._keyBindingManager.setEnabled( false );
    }

    private onMyChatMessage_FocusOut(): void
    {
        this._keyBindingManager.setEnabled( true );
    }

    // #endregion //
}