import Session from "../Session";
import ResourceLoader from "../service/ResourceLoader";
import ServiceLocator from "../ServiceLocator";
import { ICardSet, IScenario } from "../game/ScenarioDB";
import Utils from "../Utils";
import { ICard } from "../game/CardDB";
import Signal from "../lib/signals/Signal";


export default class GameAssetLoader
{
    private _onCompleted: Signal = new Signal();


    public get onCompleted(): Signal { return this._onCompleted; }


    public constructor() {}

    public addResources( resourceLoader: ResourceLoader ): void
    {
        let arrReqTextureMapId: Array<string> = this.findRequiredTextureMaps();
        if ( ServiceLocator.resourceCache )
        {
            ServiceLocator.loadingScreen.setMessage( jQuery.i18n( "PRELOADING_TEXTURE_MAPS" ).replace( "#0", "0" ).replace( "#1", arrReqTextureMapId.length.toString() ) );

            ServiceLocator.resourceCache.loadCachedTextureMaps( arrReqTextureMapId ).then( () => {
                this.addTextureMaps( arrReqTextureMapId, resourceLoader );
                this.addRemainingResources( resourceLoader );

                this._onCompleted.dispatch( arrReqTextureMapId );
            } );
        }
        else
        {
            this.addTextureMaps( arrReqTextureMapId, resourceLoader );
            this.addRemainingResources( resourceLoader );

            this._onCompleted.dispatch( [] );
        }
    }

        private findRequiredTextureMaps(): Array<string>
        {
            let result: Array<string> = [ "LOTR999-0" ];

            // Find texture packs for game decks.
            let arrCardId: Array<string> = Session.playerDeck.hero
                .concat( Session.playerDeck.player )
                .concat( Session.playerDeck.sideboard );
            if ( Session.playerDeck.contract )
            {
                arrCardId.push( Session.playerDeck.contract );
                const kCardInfo: ICard = ServiceLocator.cardDb.find( Session.playerDeck.contract );
                if ( kCardInfo.side != "999002" && kCardInfo.side != "999003")
                {
                    arrCardId.push( kCardInfo.side );
                }
            }
            if ( Session.allyId )
            {
                arrCardId = arrCardId.concat( Session.allyDeck.hero )
                    .concat( Session.allyDeck.player )
                    .concat( Session.allyDeck.sideboard );
                if ( Session.allyDeck.contract )
                {
                    arrCardId.push( Session.allyDeck.contract );
                    const kCardInfo: ICard = ServiceLocator.cardDb.find( Session.allyDeck.contract );
                    if ( kCardInfo.side != "999002" && kCardInfo.side != "999003")
                    {
                        arrCardId.push( kCardInfo.side );
                    }
                }
            }

            const kScenarioInfo: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
            let arrEncounterSet: Array<string> = [ kScenarioInfo.encounter_set.main ];
            if ( kScenarioInfo.encounter_set.additional )
            {
                arrEncounterSet = arrEncounterSet.concat( kScenarioInfo.encounter_set.additional );
            }
            const kArrEncounterCard: Array<ICard> = ServiceLocator.cardDb.findByEncounterSet( arrEncounterSet );
            for ( let card of kArrEncounterCard )
            {
                arrCardId.push( card.code );
            }

            if ( kScenarioInfo.additional_cards )
            {
                arrCardId = arrCardId.concat( kScenarioInfo.additional_cards );
            }

            if ( kScenarioInfo.treasure_sets )
            {
                let arrTreasureCard: Array<ICard> = ServiceLocator.cardDb.findByTypeCodes( [ "treasure" ] );
                for ( let card of arrTreasureCard )
                {
                    if ( kScenarioInfo.treasure_sets.indexOf( card.treasure_set ) != -1 )
                    {
                        arrCardId.push( card.code );
                    }
                }
            }

            for ( let cardId of arrCardId )
            {
                const kTextureMapId: string = ServiceLocator.cardDb.find( cardId ).texture_map_id;
                if ( result.indexOf( kTextureMapId ) == -1 )
                {
                    result.push( kTextureMapId );
                }
            }

            return result;
        }

        private addTextureMaps( arrReqTextureMapId: Array<string>, resourceLoader: ResourceLoader ): void
        {
            // Load texture packs.
            for ( let textureMapId of arrReqTextureMapId )
            {
                resourceLoader.add( textureMapId, "resources/texture_packer/" + textureMapId.substr( 0, 7 ) + "/" + textureMapId + ".json" );
            }
        }

        private addRemainingResources( resourceLoader: ResourceLoader ): void
        {
            // Load emitters.
            resourceLoader.add( "emitter_new_info_panel", "resources/json/emitter/new_info_panel.json" );
            resourceLoader.add( "emitter_broken_province", "resources/json/emitter/broken_province.json" );
            resourceLoader.add( "emitter_ability_target_selection", "resources/json/emitter/ability_target_selection.json" );
            resourceLoader.add( "emitter_token_dropped", "resources/json/emitter/token_dropped.json" );
            resourceLoader.add( "emitter_player_permission", "resources/json/emitter/player_permission.json" );
            resourceLoader.add( "emitter_active_player", "resources/json/emitter/active_player.json" );
            resourceLoader.add( "emitter_threat_level_alert", "resources/json/emitter/threat_level_alert.json" );
            resourceLoader.add( "emitter_flying_orb", "resources/json/emitter/flying_orb.json" );
            // Load sprites.
            resourceLoader.add( "transparent", "resources/images/transparent.png" );
            // Particle.
            resourceLoader.add( "particle_sakura", "resources/images/game/particle/sakura.png" );
            resourceLoader.add( "particle_default", "resources/images/game/particle/default.png" );
            resourceLoader.add( "particle_fire", "resources/images/game/particle/fire.png" );
            resourceLoader.add( "particle_dust", "resources/images/game/particle/dust.png" );
            resourceLoader.add( "particle_smoke", "resources/images/game/particle/smoke.png" );
            resourceLoader.add( "particle_spark", "resources/images/game/particle/spark.png" );
            // Play area.
            resourceLoader.add( "table_bg", "resources/images/game/play_area/table_bg.jpg" );
            resourceLoader.add( "player_side_controls", "resources/images/game/play_area/player_side_controls.png" );
            resourceLoader.add( "ally_side_controls", "resources/images/game/play_area/ally_side_controls.png" );
            resourceLoader.add( "quest_holder", "resources/images/game/play_area/quest_holder.png" );
            resourceLoader.add( "active_location_holder", "resources/images/game/play_area/active_location_holder.png" );
            resourceLoader.add( "island_location_holder", "resources/images/game/play_area/island_location_holder.png" );
            resourceLoader.add( "dor-cuarthol_location_holder", "resources/images/game/play_area/dor-cuarthol_location_holder.png" );
            // Card.
            resourceLoader.add( "card_sauron_back", "resources/images/game/card/back/sauron.jpg" );
            resourceLoader.add( "card_player_back", "resources/images/game/card/back/player.jpg" );
            resourceLoader.add( "card_sauron_discard_back", "resources/images/game/card/back/sauron_discard.jpg" );
            resourceLoader.add( "card_quest_back", "resources/images/game/card/back/quest.jpg" );
            resourceLoader.add( "card_player_discard_back", "resources/images/game/card/back/player_discard.jpg" );
            // Card token.
            resourceLoader.add( "sauron_back", "resources/images/game/card_token/back/sauron.jpg" );
            resourceLoader.add( "player_back", "resources/images/game/card_token/back/player.jpg" );
            resourceLoader.add( "wound_token", "resources/images/game/card_token/token/wound.png" );
            resourceLoader.add( "progress_token", "resources/images/game/card_token/token/progress.png" );
            resourceLoader.add( "resource_token", "resources/images/game/card_token/token/resource.png" );
            resourceLoader.add( "player_attack", "resources/images/game/card_token/player/attack.png" );
            resourceLoader.add( "player_defense", "resources/images/game/card_token/player/defense.png" );
            resourceLoader.add( "player_will", "resources/images/game/card_token/player/will.png" );
            resourceLoader.add( "player_health", "resources/images/game/card_token/player/health.png" );
            resourceLoader.add( "hero_threat", "resources/images/game/card_token/player/hero_threat.png" );
            resourceLoader.add( "leadership", "resources/images/game/card_token/player/leadership.png" );
            resourceLoader.add( "tactics", "resources/images/game/card_token/player/tactics.png" );
            resourceLoader.add( "lore", "resources/images/game/card_token/player/lore.png" );
            resourceLoader.add( "spirit", "resources/images/game/card_token/player/spirit.png" );
            resourceLoader.add( "baggins", "resources/images/game/card_token/player/baggins.png" );
            resourceLoader.add( "fellowship", "resources/images/game/card_token/player/fellowship.png" );
            resourceLoader.add( "mastery", "resources/images/game/card_token/player/mastery.png" );
            resourceLoader.add( "sauron_attack", "resources/images/game/card_token/sauron/attack.png" );
            resourceLoader.add( "sauron_defense", "resources/images/game/card_token/sauron/defense.png" );
            resourceLoader.add( "sauron_threat", "resources/images/game/card_token/sauron/threat.png" );
            resourceLoader.add( "sauron_health", "resources/images/game/card_token/sauron/health.png" );
            resourceLoader.add( "engagement_cost", "resources/images/game/card_token/sauron/engagement_cost.png" );
            resourceLoader.add( "location_quest_points", "resources/images/game/card_token/sauron/location_quest_points.png" );
            resourceLoader.add( "quest_points", "resources/images/game/card_token/sauron/quest_points.png" );
            resourceLoader.add( "quest_completed", "resources/images/game/card_token/sauron/scenario_completed.png" );
            resourceLoader.add( "quest_light_rays", "resources/images/game/card_token/sauron/light_rays.png" );
            resourceLoader.add( "quest_lock", "resources/images/game/card_token/sauron/quest_lock.png" );
            resourceLoader.add( "quest_chains", "resources/images/game/card_token/sauron/quest_chains.png" );
            resourceLoader.add( "status_committed", "resources/images/game/card_token/status/committed.png" );
            resourceLoader.add( "status_dead", "resources/images/game/card_token/status/dead.png" );
            resourceLoader.add( "status_explored", "resources/images/game/card_token/status/explored.png" );
            resourceLoader.add( "status_has_attacked", "resources/images/game/card_token/status/has_attacked.png" );
            // Shadow.
            resourceLoader.add( "shadow_first_player", "resources/images/game/shadow/first_player.png" );
            resourceLoader.add( "shadow_card_token", "resources/images/game/shadow/card_token.png" );
            resourceLoader.add( "shadow_quest_token", "resources/images/game/shadow/quest_token.png" );
            resourceLoader.add( "shadow_game_modifier", "resources/images/game/shadow/game_modifier.png" );
            // Side bar.
            resourceLoader.add( "hand", "resources/images/game/side_bar/hand.png" );
            resourceLoader.add( "removed_from_game", "resources/images/game/side_bar/removed_from_game.png" );
            resourceLoader.add( "lock", "resources/images/game/side_bar/lock.png" );
            resourceLoader.add( "threat_level", "resources/images/game/side_bar/threat_level.png" );
            resourceLoader.add( "player_deck", "resources/images/game/side_bar/player_deck.png" );
            resourceLoader.add( "player_discard", "resources/images/game/side_bar/player_discard.png" );
            resourceLoader.add( "player_custom_deck", "resources/images/game/side_bar/player_custom_deck.png" );
            resourceLoader.add( "encounter_deck", "resources/images/game/side_bar/encounter_deck.png" );
            resourceLoader.add( "encounter_discard", "resources/images/game/side_bar/encounter_discard.png" );
            resourceLoader.add( "secondary_encounter_deck", "resources/images/game/side_bar/secondary_encounter_deck.png" );
            resourceLoader.add( "sauron_custom_deck", "resources/images/game/side_bar/sauron_custom_deck.png" );
            resourceLoader.add( "secondary_sauron_custom_deck", "resources/images/game/side_bar/secondary_sauron_custom_deck.png" );
            resourceLoader.add( "sauron_custom_discard", "resources/images/game/side_bar/sauron_custom_discard.png" );
            resourceLoader.add( "quest_deck", "resources/images/game/side_bar/quest_deck.png" );
            resourceLoader.add( "quest_discard", "resources/images/game/side_bar/quest_discard.png" );
            resourceLoader.add( "victory_display", "resources/images/game/side_bar/victory_display.png" );
            resourceLoader.add( "set_aside", "resources/images/game/side_bar/set_aside.png" );
            resourceLoader.add( "tray", "resources/images/game/side_bar/tray.png" );
            resourceLoader.add( "end_turn_btn", "resources/images/game/side_bar/end_turn_btn.png" );
            resourceLoader.add( "player_waiting", "resources/images/game/side_bar/player_waiting.png" );
            resourceLoader.add( "player_completed", "resources/images/game/side_bar/player_completed.png" );
            // Card viewer.
            resourceLoader.add( "shuffle", "resources/images/game/card_viewer/shuffle.png" );
            resourceLoader.add( "switch", "resources/images/game/card_viewer/switch.png" );
            resourceLoader.add( "reveal_x", "resources/images/game/card_viewer/reveal_x.png" );
            resourceLoader.add( "randomly_discard", "resources/images/game/card_viewer/randomly_discard.png" );
            resourceLoader.add( "discard_x", "resources/images/game/card_viewer/discard_x.png" );
            resourceLoader.add( "draw_x", "resources/images/game/card_viewer/draw_x.png" );
            resourceLoader.add( "share_view", "resources/images/game/card_viewer/share_view.png" );
            resourceLoader.add( "stop_sharing_view", "resources/images/game/card_viewer/stop_sharing_view.png" );
            resourceLoader.add( "mulligan", "resources/images/game/card_viewer/mulligan.png" );
            resourceLoader.add( "reshuffle", "resources/images/game/card_viewer/reshuffle.png" );
            resourceLoader.add( "sort_by_sphere", "resources/images/game/card_viewer/sort_by_sphere.png" );
            resourceLoader.add( "sort_from_a_to_z", "resources/images/game/card_viewer/sort_from_a_to_z.png" );
            resourceLoader.add( "close", "resources/images/game/card_viewer/close.png" );
            resourceLoader.add( "minimize", "resources/images/game/card_viewer/minimize.png" );
            resourceLoader.add( "maximize", "resources/images/game/card_viewer/maximize.png" );
            // Options menu.
            resourceLoader.add( "abort", "resources/images/game/options/abort.png" );
            resourceLoader.add( "key_bindings", "resources/images/game/options/key_bindings.png" );
            resourceLoader.add( "music_on", "resources/images/game/options/music_on.png" );
            resourceLoader.add( "music_off", "resources/images/game/options/music_off.png" );
            resourceLoader.add( "sfx_on", "resources/images/game/options/sfx_on.png" );
            resourceLoader.add( "sfx_off", "resources/images/game/options/sfx_off.png" );
            resourceLoader.add( "restart", "resources/images/game/options/restart.png" );
            resourceLoader.add( "help", "resources/images/game/options/help.png" );
            resourceLoader.add( "rules", "resources/images/game/options/rules.png" );
            resourceLoader.add( "special_rules", "resources/images/game/options/special_rules.png" );
            resourceLoader.add( "game_preferences", "resources/images/game/options/preferences.png" );
            // Radial menu.
            resourceLoader.add( "rad_add_counter", "resources/images/game/radial_menu/add_counter.png" );
            resourceLoader.add( "rad_put_face_down", "resources/images/game/radial_menu/put_face_down.png" );
            resourceLoader.add( "rad_put_face_up", "resources/images/game/radial_menu/put_face_up.png" );
            resourceLoader.add( "rad_bow", "resources/images/game/radial_menu/bow.png" );
            resourceLoader.add( "rad_ready", "resources/images/game/radial_menu/ready.png" );
            resourceLoader.add( "rad_commit", "resources/images/game/radial_menu/commit.png" );
            resourceLoader.add( "rad_uncommit", "resources/images/game/radial_menu/uncommit.png" );
            resourceLoader.add( "rad_play", "resources/images/game/radial_menu/play.png" );
            resourceLoader.add( "rad_discard", "resources/images/game/radial_menu/discard.png" );
            resourceLoader.add( "rad_equip", "resources/images/game/radial_menu/equip.png" );
            resourceLoader.add( "rad_target", "resources/images/game/radial_menu/target.png" );
            resourceLoader.add( "rad_player_attack", "resources/images/game/radial_menu/player_attack.png" );
            resourceLoader.add( "rad_sauron_attack", "resources/images/game/radial_menu/sauron_attack.png" );
            resourceLoader.add( "rad_defend", "resources/images/game/radial_menu/defend.png" );
            resourceLoader.add( "rad_back", "resources/images/game/radial_menu/back.png" );
            resourceLoader.add( "rad_extras", "resources/images/game/radial_menu/extras.png" );
            resourceLoader.add( "rad_add_custom_text", "resources/images/game/radial_menu/add_custom_text.png" );
            resourceLoader.add( "rad_remove_custom_text", "resources/images/game/radial_menu/remove_custom_text.png" );
            resourceLoader.add( "rad_underneath", "resources/images/game/radial_menu/underneath.png" );
            resourceLoader.add( "rad_shadow_card", "resources/images/game/radial_menu/shadow_card.png" );
            resourceLoader.add( "rad_rotate", "resources/images/game/radial_menu/rotate.png" );
            resourceLoader.add( "rad_add_highlight", "resources/images/game/radial_menu/add_highlight.png" );
            resourceLoader.add( "rad_remove_highlight", "resources/images/game/radial_menu/remove_highlight.png" );
            resourceLoader.add( "rad_give_control", "resources/images/game/radial_menu/give_control.png" );
            // Phase diagram.
            resourceLoader.add( "phase_resource", "resources/images/game/phase/resource.jpg" );
            resourceLoader.add( "phase_planning", "resources/images/game/phase/planning.jpg" );
            resourceLoader.add( "phase_quest", "resources/images/game/phase/quest.jpg" );
            resourceLoader.add( "phase_travel", "resources/images/game/phase/travel.jpg" );
            resourceLoader.add( "phase_encounter", "resources/images/game/phase/encounter.jpg" );
            resourceLoader.add( "phase_combat", "resources/images/game/phase/combat.jpg" );
            resourceLoader.add( "phase_enemy_attack_resolution", "resources/images/game/phase/enemy_attack_resolution.jpg" );
            resourceLoader.add( "phase_player_attack_resolution", "resources/images/game/phase/player_attack_resolution.jpg" );
            resourceLoader.add( "phase_refresh", "resources/images/game/phase/refresh.jpg" );
            // Icon.
            resourceLoader.add( "black_pearl", "resources/images/game/icon/black_pearl.png" );
            resourceLoader.add( "white_pearl", "resources/images/game/icon/white_pearl.png" );
            resourceLoader.add( "first_player", "resources/images/game/icon/first_player.png" );
            resourceLoader.add( "history", "resources/images/game/icon/history.png" );
            resourceLoader.add( "attachment", "resources/images/game/icon/attachment.png" );
            resourceLoader.add( "custom_text", "resources/images/game/icon/custom_text.png" );
            resourceLoader.add( "exclamation_mark", "resources/images/game/icon/exclamation_mark.png" );
            resourceLoader.add( "move_to_battle", "resources/images/game/icon/move_to_battle.png" );
            resourceLoader.add( "move_home", "resources/images/game/icon/move_home.png" );
            resourceLoader.add( "hourglass", "resources/images/game/icon/hourglass.png" );
            resourceLoader.add( "gears", "resources/images/game/icon/gears.png" );
            resourceLoader.add( "ring", "resources/images/game/icon/ring.png" );
            resourceLoader.add( "arrow", "resources/images/game/icon/arrow.png" );
            resourceLoader.add( "home", "resources/images/game/icon/home.png" );
            resourceLoader.add( "engaged", "resources/images/game/icon/engaged.png" );
            resourceLoader.add( "staging", "resources/images/game/icon/staging.png" );
            resourceLoader.add( "reminder", "resources/images/game/icon/reminder.png" );
            resourceLoader.add( "player", "resources/images/game/icon/player.png" );
            resourceLoader.add( "profile", "resources/images/game/icon/profile.png" );
            resourceLoader.add( "give", "resources/images/game/icon/give.png" );
            resourceLoader.add( "save", "resources/images/game/icon/save.png" );
            resourceLoader.add( "load", "resources/images/game/icon/load.png" );
            resourceLoader.add( "heading", "resources/images/game/icon/heading.png" );
            resourceLoader.add( "compass_marker", "resources/images/game/icon/compass_marker.png" );
            //
            resourceLoader.add( "keyword_immune", "resources/images/game/icon/keyword/immune.jpg" );
            resourceLoader.add( "keyword_ranged", "resources/images/game/icon/keyword/ranged.jpg" );
            resourceLoader.add( "keyword_sentinel", "resources/images/game/icon/keyword/sentinel.jpg" );
            resourceLoader.add( "keyword_ambush", "resources/images/game/icon/keyword/ambush.jpg" );
            resourceLoader.add( "keyword_regenerate", "resources/images/game/icon/keyword/regenerate.jpg" );
            resourceLoader.add( "keyword_guarded", "resources/images/game/icon/keyword/guarded.jpg" );
            resourceLoader.add( "keyword_unique", "resources/images/game/icon/keyword/unique.jpg" );
            resourceLoader.add( "keyword_restricted", "resources/images/game/icon/keyword/restricted.jpg" );
            resourceLoader.add( "keyword_doomed", "resources/images/game/icon/keyword/doomed.jpg" );
            resourceLoader.add( "keyword_surge", "resources/images/game/icon/keyword/surge.jpg" );
            resourceLoader.add( "keyword_secrecy", "resources/images/game/icon/keyword/secrecy.jpg" );
            resourceLoader.add( "keyword_battle", "resources/images/game/icon/keyword/battle.jpg" );
            resourceLoader.add( "keyword_siege", "resources/images/game/icon/keyword/siege.jpg" );
            resourceLoader.add( "keyword_indestructible", "resources/images/game/icon/keyword/indestructible.jpg" );
            resourceLoader.add( "keyword_underworld", "resources/images/game/icon/keyword/underworld.jpg" );
            resourceLoader.add( "keyword_archery", "resources/images/game/icon/keyword/archery.jpg" );
            resourceLoader.add( "keyword_prowl", "resources/images/game/icon/keyword/prowl.jpg" );
            resourceLoader.add( "keyword_villagers", "resources/images/game/icon/keyword/villagers.jpg" );
            resourceLoader.add( "keyword_aflame", "resources/images/game/icon/keyword/aflame.jpg" );
            resourceLoader.add( "keyword_assault", "resources/images/game/icon/keyword/assault.jpg" );
            resourceLoader.add( "keyword_boarding", "resources/images/game/icon/keyword/boarding.jpg" );
            resourceLoader.add( "keyword_burn", "resources/images/game/icon/keyword/burn.jpg" );
            resourceLoader.add( "keyword_capture", "resources/images/game/icon/keyword/capture.jpg" );
            resourceLoader.add( "keyword_deep", "resources/images/game/icon/keyword/deep.jpg" );
            resourceLoader.add( "keyword_defense", "resources/images/game/icon/keyword/defense.jpg" );
            resourceLoader.add( "keyword_dire", "resources/images/game/icon/keyword/dire.jpg" );
            resourceLoader.add( "keyword_discover", "resources/images/game/icon/keyword/discover.jpg" );
            resourceLoader.add( "keyword_exploration", "resources/images/game/icon/keyword/exploration.jpg" );
            resourceLoader.add( "keyword_grapple", "resources/images/game/icon/keyword/grapple.jpg" );
            resourceLoader.add( "keyword_hide", "resources/images/game/icon/keyword/hide.jpg" );
            resourceLoader.add( "keyword_hinder", "resources/images/game/icon/keyword/hinder.jpg" );
            resourceLoader.add( "keyword_investigate", "resources/images/game/icon/keyword/investigate.jpg" );
            resourceLoader.add( "keyword_loot", "resources/images/game/icon/keyword/loot.jpg" );
            resourceLoader.add( "keyword_mire", "resources/images/game/icon/keyword/mire.jpg" );
            resourceLoader.add( "keyword_peril", "resources/images/game/icon/keyword/peril.jpg" );
            resourceLoader.add( "keyword_permanent", "resources/images/game/icon/keyword/permanent.jpg" );
            resourceLoader.add( "keyword_phantom", "resources/images/game/icon/keyword/phantom.jpg" );
            resourceLoader.add( "keyword_race", "resources/images/game/icon/keyword/race.jpg" );
            resourceLoader.add( "keyword_sack", "resources/images/game/icon/keyword/sack.jpg" );
            resourceLoader.add( "keyword_safe", "resources/images/game/icon/keyword/safe.jpg" );
            resourceLoader.add( "keyword_sailing", "resources/images/game/icon/keyword/sailing.jpg" );
            resourceLoader.add( "keyword_scour", "resources/images/game/icon/keyword/scour.jpg" );
            resourceLoader.add( "keyword_searches", "resources/images/game/icon/keyword/searches.jpg" );
            resourceLoader.add( "keyword_spectral", "resources/images/game/icon/keyword/spectral.jpg" );
            resourceLoader.add( "keyword_time", "resources/images/game/icon/keyword/time.jpg" );
            resourceLoader.add( "keyword_toughness", "resources/images/game/icon/keyword/toughness.jpg" );
            resourceLoader.add( "keyword_track", "resources/images/game/icon/keyword/track.jpg" );
            resourceLoader.add( "keyword_uncharted", "resources/images/game/icon/keyword/uncharted.jpg" );
            resourceLoader.add( "keyword_venom", "resources/images/game/icon/keyword/venom.jpg" );
            resourceLoader.add( "keyword_vast", "resources/images/game/icon/keyword/vast.jpg" );
            resourceLoader.add( "keyword_devoted", "resources/images/game/icon/keyword/devoted.jpg" );
            resourceLoader.add( "keyword_hunt", "resources/images/game/icon/keyword/hunt.jpg" );
            resourceLoader.add( "keyword_raid", "resources/images/game/icon/keyword/raid.jpg" );
            resourceLoader.add( "keyword_ransom", "resources/images/game/icon/keyword/ransom.jpg" );
            resourceLoader.add( "keyword_relentless", "resources/images/game/icon/keyword/relentless.jpg" );
            resourceLoader.add( "keyword_sneak", "resources/images/game/icon/keyword/sneak.jpg" );
            resourceLoader.add( "keyword_steeds", "resources/images/game/icon/keyword/steeds.jpg" );
            resourceLoader.add( "keyword_frostbite", "resources/images/game/icon/keyword/frostbite.jpg" );
            //
            resourceLoader.add( "attack", "resources/images/game/icon/attack.png" );
            resourceLoader.add( "defense", "resources/images/game/icon/defense.png" );
            resourceLoader.add( "threat", "resources/images/game/icon/threat.png" );
            resourceLoader.add( "willpower", "resources/images/game/icon/willpower.png" );
            resourceLoader.add( "auto", "resources/images/game/icon/auto.png" );
            resourceLoader.add( "player", "resources/images/game/icon/player.png" );
            resourceLoader.add( "ally", "resources/images/game/icon/ally.png" );
            resourceLoader.add( "sauron", "resources/images/game/icon/sauron.png" );
            resourceLoader.add( "ok", "resources/images/game/icon/ok.png" );
            resourceLoader.add( "cancel", "resources/images/game/icon/cancel.png" );
            resourceLoader.add( "add", "resources/images/game/icon/add.png" );
            resourceLoader.add( "remove", "resources/images/game/icon/remove.png" );
            resourceLoader.add( "add_enemy", "resources/images/game/icon/add_enemy.png" );
            resourceLoader.add( "split", "resources/images/game/icon/split.png" );
            resourceLoader.add( "merge", "resources/images/game/icon/merge.png" );
            resourceLoader.add( "options", "resources/images/game/icon/options.png" );
            resourceLoader.add( "pipe", "resources/images/game/icon/pipe.png" );
            resourceLoader.add( "eye", "resources/images/game/icon/eye.png" );
            resourceLoader.add( "active_player", "resources/images/game/icon/active_player.png" );
            resourceLoader.add( "setup_instructions", "resources/images/game/icon/setup_instructions.png" );
            resourceLoader.add( "isolated_staging_area", "resources/images/game/icon/isolated_staging_area.png" );
            resourceLoader.add( "main_staging_area", "resources/images/game/icon/main_staging_area.png" );
            resourceLoader.add( "treasure_list", "resources/images/game/icon/treasure_list.png" );
            resourceLoader.add( "campaign_log", "resources/images/game/icon/campaign_log.png" );
            resourceLoader.add( "orc", "resources/images/game/icon/orc.png" );
            resourceLoader.add( "circuit", "resources/images/game/icon/circuit.png" );
            resourceLoader.add( "galleon", "resources/images/game/icon/galleon.png" );
            resourceLoader.add( "riddle", "resources/images/game/icon/riddle.png" );
            resourceLoader.add( "island_map", "resources/images/game/icon/island_map.png" );
            resourceLoader.add( "pursuit_far", "resources/images/game/icon/pursuit_far.png" );
            resourceLoader.add( "pursuit_close", "resources/images/game/icon/pursuit_close.png" );
            resourceLoader.add( "final_lap", "resources/images/game/icon/final_lap.png" );
            resourceLoader.add( "laurel", "resources/images/game/icon/laurel.png" );
            resourceLoader.add( "compass", "resources/images/game/icon/compass.png" );
            resourceLoader.add( "investigation_list", "resources/images/game/icon/investigation_list.png" );
            resourceLoader.add( "checked", "resources/images/game/icon/checked.png" );
            resourceLoader.add( "unchecked", "resources/images/game/icon/unchecked.png" );
            resourceLoader.add( "pit", "resources/images/game/icon/pit.png" );
            resourceLoader.add( "mountains", "resources/images/game/icon/mountains.png" );
            //resourceLoader.add( "play_card", "resources/images/game/icon/play_card.png" );
            // Custom counter.
            resourceLoader.add( "custom_counter_gollum", "resources/images/game/card_token/custom_counter/gollum.png" );
            resourceLoader.add( "custom_counter_resource", "resources/images/game/card_token/custom_counter/resource.png" );
            resourceLoader.add( "custom_counter_wound", "resources/images/game/card_token/custom_counter/wound.png" );
            resourceLoader.add( "custom_counter_progress", "resources/images/game/card_token/custom_counter/progress.png" );
            resourceLoader.add( "custom_counter_time", "resources/images/game/card_token/custom_counter/time.png" );
            resourceLoader.add( "custom_counter_white", "resources/images/game/card_token/custom_counter/white.jpg" );
            resourceLoader.add( "custom_counter_yellow", "resources/images/game/card_token/custom_counter/yellow.jpg" );
            resourceLoader.add( "custom_counter_red", "resources/images/game/card_token/custom_counter/red.jpg" );
            resourceLoader.add( "custom_counter_orange", "resources/images/game/card_token/custom_counter/orange.jpg" );
            resourceLoader.add( "custom_counter_pink", "resources/images/game/card_token/custom_counter/pink.jpg" );
            resourceLoader.add( "custom_counter_purple", "resources/images/game/card_token/custom_counter/purple.jpg" );
            resourceLoader.add( "custom_counter_green", "resources/images/game/card_token/custom_counter/green.jpg" );
            resourceLoader.add( "custom_counter_black", "resources/images/game/card_token/custom_counter/black.jpg" );
            // Intro.
            resourceLoader.add( "intro_phase", "resources/images/game/intro/phase.png" );
            resourceLoader.add( "intro_round", "resources/images/game/intro/round.png" );
            // Art.
            resourceLoader.add( "circuit_bg", "resources/images/game/art/circuit.png" );
            // Load fonts.
            // resourceLoader.add( "combat_font", "resources/fonts/combat.fnt" );
        }
}