import { ICard } from "../game/CardDB";
import { player_type } from "../game/component/world/CPlayerArea";
import GameObject from "../game/GameObject";
import { IScenario } from "../game/ScenarioDB";
import { IRemoteCard } from "../service/socket_io/GameSocketIOController";
import ServiceLocator from "../ServiceLocator";
import Session from "../Session";
import { IDeck } from "../view/gateway/DeckParser";


export default class RemoteDeckCreator
{
    private _cardIndex: number = 0;


    public create(): IRemoteDeck
    {
        let result: IRemoteDeck = {
            playerDeck: null,
            encounter: new Array<IRemoteCard>(),
            quest: new Array<IRemoteCard>(),
            setAside: new Array<IRemoteCard>() };

        if ( ServiceLocator.game.firstPlayer == player_type.PLAYER )
        {
            result.playerDeck = this.createPlayerDeck( Session.playerDeck, player_type.PLAYER );
            if ( Session.allyDeck )
            {
                result.allyDeck = this.createPlayerDeck( Session.allyDeck, player_type.ALLY );
            }
        }
        else
        {
            result.allyDeck = this.createPlayerDeck( Session.allyDeck, player_type.ALLY );
            result.playerDeck = this.createPlayerDeck( Session.playerDeck, player_type.PLAYER );
        }

        for ( let cardId of Session.encounterDeck )
        {
            result.encounter.push( { cid: cardId, oid: "o" + this._cardIndex.toString() + "c", isFaceUp: false } );
            ++this._cardIndex;
        }
        for ( let cardId of Session.questDeck )
        {
            // IMPORTANT: Make side B the card front side.
            if ( cardId.indexOf( "A" ) == -1 )
            {
                result.quest.push( { cid: cardId, oid: "o" + this._cardIndex.toString() + "c", isFaceUp: false } );
                ++this._cardIndex;
            }
        }
        for ( let cardId of Session.setAsideDeck )
        {
            if ( cardId.indexOf( "B" ) == -1 )
            {
                result.setAside.push( { cid: cardId, oid: "o" + this._cardIndex.toString() + "c", isFaceUp: true } );
                ++this._cardIndex;
            }
        }

        const kScenarioInfo: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
        if ( kScenarioInfo.type == "nightmare" )
        {
            let arrCard: Array<ICard> = ServiceLocator.cardDb.findByEncounterSet( [ kScenarioInfo.encounter_set.main ] );
            for ( let card of arrCard )
            {
                if ( card.type_code == "nightmare-setup" && card.code.indexOf( "A" ) != -1 )
                {
                    result.nightmareSetup = { cid: card.code, oid: "o" + this._cardIndex.toString() + "c", isFaceUp: true };
                    ++this._cardIndex;
                    break;
                }
            }
        }

        if ( Session.isCampaignMode )
        {
            let campaignEncounterSet: string = null;
            if ( kScenarioInfo.type == "nightmare" )
            {
                campaignEncounterSet = kScenarioInfo.encounter_set.additional[ 0 ];
            }
            else
            {
                campaignEncounterSet = kScenarioInfo.encounter_set.main;
            }
            let arrCard: Array<ICard> = ServiceLocator.cardDb.findByEncounterSet( [ campaignEncounterSet ] );
            for ( let card of arrCard )
            {
                if ( card.type_code == "campaign" && card.code.indexOf( "A" ) != -1 )
                {
                    result.campaign = { cid: card.code, oid: "o" + this._cardIndex.toString() + "c", isFaceUp: true };
                    ++this._cardIndex;
                    break;
                }
            }
        }

        if ( kScenarioInfo.gencon_setup )
        {
            result.genconSetup = { cid: kScenarioInfo.gencon_setup + "A", oid: "o" + this._cardIndex.toString() + "c", isFaceUp: true };
            ++this._cardIndex;
        }

        return result;
    }

        private createPlayerDeck( playerDeck: IDeck, playerType: player_type ): IRemotePlayerDeck
        {
            console.assert( playerType == player_type.PLAYER || playerType == player_type.ALLY, "RemoteDeckCreator.ts :: createPlayerDeck() :: Invalid playerType." );

            let result: IRemotePlayerDeck = {
                hero: new Array<IRemoteCard>(),
                player: new Array<IRemoteCard>(),
                sideboard: new Array<IRemoteCard>() };

            const kPlayerSuffix: string = GameObject.findPlayerSufix( playerType );
            for ( let cardId of playerDeck.hero )
            {
                result.hero.push( { cid: cardId, oid: "o" + this._cardIndex.toString() + "c" + kPlayerSuffix, isFaceUp: false } );
                ++this._cardIndex;
            }
            for ( let cardId of playerDeck.player )
            {
                result.player.push( { cid: cardId, oid: "o" + this._cardIndex.toString() + "c" + kPlayerSuffix, isFaceUp: false } );
                ++this._cardIndex;
            }
            for ( let cardId of playerDeck.sideboard )
            {
                result.sideboard.push( { cid: cardId, oid: "o" + this._cardIndex.toString() + "c" + kPlayerSuffix, isFaceUp: false } );
                ++this._cardIndex;
            }
            if ( playerDeck.contract )
            {
                result.contract = { cid: playerDeck.contract, oid: "o" + this._cardIndex.toString() + "c" + kPlayerSuffix, isFaceUp: false };
                ++this._cardIndex;
            }

            return result;
        }
}


export interface IRemoteDeck
{
    playerDeck: IRemotePlayerDeck;
    allyDeck?: IRemotePlayerDeck;
    encounter: Array<IRemoteCard>;
    quest: Array<IRemoteCard>;
    setAside: Array<IRemoteCard>;
    campaign?: IRemoteCard;
    genconSetup?: IRemoteCard;
    nightmareSetup?: IRemoteCard;
}

export interface IRemotePlayerDeck
{
    hero: Array<IRemoteCard>;
    player: Array<IRemoteCard>;
    sideboard: Array<IRemoteCard>;
    contract?: IRemoteCard;
}