export const enum main_state_id
{
    APP_STARTUP = 0,
    GATEWAY,
    GAME,
    FAST_GATEWAY // DEBUG:
}

export const enum game_state_id
{
    // 1st level.
    SETUP = 0,
    RESOURCE,
    PLANNING,
    QUEST,
    TRAVEL,
    ENCOUNTER,
    COMBAT,
    REFRESH,
    // Shared.
    ROUND_INTRO,
    PHASE_INTRO
}

export const enum setup_state_id
{
    HERO_PLACEMENT = 0,
    STARTING_HAND_DRAWING,
    QUEST_PLACEMENT,
    SYNCHRONIZING
}

export const enum quest_state_id
{
    CHARACTER_COMMITMENT = 0,
    STAGING,
    QUEST_RESOLUTION
}

export const enum encounter_state_id
{
    OPTIONAL_ENGAGEMENT = 0,
    ENGAGEMENT_CHECKS
}

export const enum combat_state_id
{
    ENEMY_ATTACKS = 0,
    PLAYER_ATTACKS
}