import State from "../lib/state_machine/State";

import ServiceLocator from "../ServiceLocator";

import HtmlFileLoader from "../lib/HtmlFileLoader";
import Signal from "../lib/signals/Signal";


export default abstract class WebState extends State
{
    // #region Attributes //

    // protected:

    protected _htmlFileLoader: HtmlFileLoader = null;

    // Signals.
    protected _onReady: Signal = new Signal();

    // #endregion //


    // #region Properties //

    // Signals.
    public get onReady(): Signal { return this._onReady; };

    // #endregion //


    // #region Methods //

    // public:

    public onEnter(): void
    {
        super.onEnter();

        ServiceLocator.resourceStack.push();
                
        this._htmlFileLoader = new HtmlFileLoader();
        this._htmlFileLoader.onCompleted.addOnce( this.onHtmlFiles_Loaded, this );
        this._htmlFileLoader.init();
    }

    public onLeave(): void
    {
        ServiceLocator.resourceStack.pop();

        // HTML file loader.
        this._htmlFileLoader.end();
        this._htmlFileLoader = null;
    
        super.onLeave();
    }

    public onUpdate( dt: number ): void {}

    // protected:

    protected abstract onHtmlFiles_Loaded(): void;

    // #endregion //
}