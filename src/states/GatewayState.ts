import WebState from "./WebState";

import { main_state_id, game_state_id } from "./StateId";
import { player_type } from "../game/component/world/CPlayerArea";
import ServiceLocator from "../ServiceLocator";
import Session from "../Session";
import { view_layer_id } from "../service/ViewManager";

import GameRoomView, { game_room_type } from "../view/gateway/GameRoomView";
import GatewayView from "../view/gateway/GatewayView";
import InfoMessageView from "../view/common/popup/InfoMessageView";
import LoginView from "../view/gateway/LoginView";
import DojoView from "../view/gateway/DojoView";
import View from "../view/View";
import GameState from "./GameState";
import ChatView from "../view/common/ChatView";
import { user_status_type } from "../service/socket_io/GatewaySocketIOController";
import { ISaveGame } from "../view/game/SaveGameView";
import OppSaveGameGenerator from "./OppSaveGameGenerator";
import GameObject from "../game/GameObject";
import SignalBinding from "../lib/signals/SignalBinding";
import { action_scope_type, IRemoteCard } from "../service/socket_io/GameSocketIOController";
import { IDeck } from "../view/gateway/DeckParser";
import CardFactory from "../game/CardFactory";
import { location_type } from "../game/component/world/CGameWorld";
import CPlayerSideControls from "../game/component/ui/CPlayerSideControls";
import { IScenario } from "../game/ScenarioDB";
import { ICard } from "../game/CardDB";
import State from "../lib/state_machine/State";
import PhaseIntroState from "./game/PhaseIntroState";
import CEngagedActorArea from "../game/component/world/actor_area/CEngagedActorArea";
import { ISoundLoadGroup, sound_group_id } from "../service/AudioManager";
import Jukebox from "../Jukebox";
import { layer_type } from "../game/component/world/CGameLayerProvider";
import RemoteDeckCreator, { IRemoteDeck } from "./RemoteDeckCreator";
import Utils from "../Utils";
import TreasureListVO from "../vo/TreasureListVO";
import CampaignLogVO, { ICampaignPoolCard } from "../vo/CampaignLogVO";
import FullScreenView from "../view/common/FullScreenView";


export default class GatewayState extends WebState
{
    // #region Attributes //

    // private:

    private _curView: View = null;
    private _arrSoundLoadGroup: Array<ISoundLoadGroup> = null;
    private _jukebox: Jukebox = null;

    private _gameCreation: IGameCreation = null;

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();
    }

    public onEnter(): void
    {
        super.onEnter();

        this._jukebox = new Jukebox();
        this._jukebox.init();

        // Load HTML files.
        this._htmlFileLoader.htmlFileInfoList = [
            // 1st level.
            { id: GatewayView.kResId, isPreloadImages: true },
            { id: !Session.playerId ? LoginView.kResId : DojoView.kResId, isPreloadImages: true } ];
        this._htmlFileLoader.load();

        // Listen to events.
        ServiceLocator.socketIOManager.onDisconnect.add( this.onSync_Disconnect, this );
        ServiceLocator.socketIOManager.onReconnect.add( this.onSync_Reconnect, this );
        ServiceLocator.socketIOManager.gateway.onUserDisconnected.add( this.onSyncUser_Disconnected, this );
        ServiceLocator.socketIOManager.gateway.onSongPlayed.add( this.onSyncSong_Played, this );
    }

    public onLeave(): void
    {
        this._jukebox.end();
        this._jukebox = null;

        // Cleanup loaded sounds.
        ServiceLocator.audioManager.stopMusic();
        for ( let soundLoadGroup of this._arrSoundLoadGroup )
        {
            ServiceLocator.audioManager.removeSounds( soundLoadGroup.arrName, soundLoadGroup.soundGroupId );
        }
        this._arrSoundLoadGroup = null;

        ServiceLocator.viewManager.clearLayer( view_layer_id.MAIN );
        this._curView = null;

        // Cleanup events.
        ServiceLocator.socketIOManager.onDisconnect.remove( this.onSync_Disconnect, this );
        ServiceLocator.socketIOManager.onReconnect.remove( this.onSync_Reconnect, this );
        ServiceLocator.socketIOManager.gateway.onUserDisconnected.remove( this.onSyncUser_Disconnected, this );
        ServiceLocator.socketIOManager.gateway.onSongPlayed.remove( this.onSyncSong_Played, this );
    
        super.onLeave();
    }

    public closeSession(): void
    {
        this._jukebox.onLeft();
        ( ServiceLocator.viewManager.findViewByResId( GatewayView.kResId )[ 0 ] as GatewayView ).onUserDisconnected();

        let loginView: LoginView = new LoginView();
        loginView.onLoginSuccess.addOnce( this.onLogin_Success, this );
        loginView.init();
        ServiceLocator.viewManager.fadeIn( loginView, view_layer_id.MAIN );

        ServiceLocator.viewManager.fadeOut( ServiceLocator.viewManager.findViewByResId( ChatView.kResId )[ 0 ] );

        this._curView = loginView;
    }

    public forcePlay(): void
    {
        this.onPlay_Selected();
    }

    // private:

    private backToDojo(): void
    {
        this._jukebox.onMainHallEntered();

        let dojoView: DojoView = new DojoView();
        dojoView.onSessionClosed.addOnce( this.onSession_Closed, this );
        dojoView.onPlay.add( this.onPlay_Selected, this );
        dojoView.init();

        ServiceLocator.viewManager.fadeOut( this._curView );
        this._curView = dojoView;
        ServiceLocator.viewManager.fadeIn( dojoView, view_layer_id.MAIN );

        let arrView: Array<View> = ServiceLocator.viewManager.findViewByResId( ChatView.kResId );
        if ( arrView.length > 0 )
        {
            let chatView: ChatView = arrView[ 0 ] as ChatView;
            chatView.setMyUserStatus( user_status_type.IDLE );
        }
    }

    private lockOpponentSideBar(): void
    {
        let cPlayerSideControls: CPlayerSideControls = ServiceLocator.game.cGameWorld.cAllyArea.cSideControls;
        // Threat level.
        cPlayerSideControls.threatLevel.cIndicator.trigger.cTokenCounter.setEnabled( false );
        cPlayerSideControls.threatLevel.cIndicator.trigger.cDropArea.setEnabled( true );
        cPlayerSideControls.threatLevel.cIndicator.trigger.cContainer.c.interactive = true;
        // Deck.
        cPlayerSideControls.deck.cIndicatorLock.setLocked( true );
        // Discard.
        cPlayerSideControls.discard.cIndicatorLock.setLocked( true );
        // Hand.
        cPlayerSideControls.hand.cIndicatorLock.setLocked( true );
    }

    // #endregion //


    // #region Sync Callbacks //

    private onSyncUser_Disconnected( userId: string ): void
    {
        if ( userId == Session.allyId )
        {
            ServiceLocator.viewManager.clearLayer( view_layer_id.POPUP );

            let infoMessageView: InfoMessageView = new InfoMessageView();
            infoMessageView.message = jQuery.i18n( "USER_DISCONNECTED" ).replace( "#", userId );
            infoMessageView.init();
            ServiceLocator.viewManager.fadeIn( infoMessageView, view_layer_id.POPUP );

            ServiceLocator.socketIOManager.gateway.setUserStatus( user_status_type.IDLE );
            Session.reset();
            this.backToDojo();
        }
    }

    private onSync_Reconnect(): void
    {
        ServiceLocator.viewManager.clearLayer( view_layer_id.POPUP );
        ServiceLocator.viewManager.clearLayer( view_layer_id.TOPMOST );

        Session.reset();
        if ( !( this._curView instanceof DojoView ) )
        {
            this.backToDojo();
        }
    }

    private onSync_Disconnect(): void
    {
        if ( Session.playerId )
        {
            let infoMessageView: InfoMessageView = new InfoMessageView();
            infoMessageView.message = jQuery.i18n( "CONNECTION_LOST" );
            infoMessageView.init();
            infoMessageView.root.find( ".popup>.controls" ).hide();
            ServiceLocator.viewManager.fadeIn( infoMessageView, view_layer_id.TOPMOST );
        }
    }

    private onSyncOpponentGame_Created(): void
    {
        this._gameCreation.isAllyGameCreated = true;

        if ( this._gameCreation.isPlayerGameCreated )
        {
            this.onGame_Created( this._gameCreation.saveGame );
        }
    }

    private onSyncSong_Played( songIndex: number ): void
    {
        this._jukebox.play( songIndex );
    }

    // #endregion //


    // #region Other Callbacks //

    // protected:

    protected onHtmlFiles_Loaded(): void
    {
        // Create sound load groups.
        this._arrSoundLoadGroup = [ 
            { // sfx.
                arrExtension: [ "ogg", "mp3", "wav" ],
                soundGroupId: sound_group_id.SFX,
                basePath: "resources/sfx/gateway/",
                arrName: [ 
                    "ally_arrived", 
                    "ally_left", 
                    "door_open", 
                    "door_closed",
                    "tip_jar" ] 
            },
            { // ambient.
                arrExtension: [ "ogg", "mp3" ],
                soundGroupId: sound_group_id.AMBIENT,
                basePath: "resources/ambient/gateway/",
                arrName: [ 
                    "tavern_crowded", 
                    "tavern_crowded_damped",
                    //
                    "concerning_hobbits_shire", 
                    "concerning_hobbits_shire_damped", 
                    "may_it_be", 
                    "may_it_be_damped",
                    "misty_mountains",
                    "misty_mountains_damped",
                    "riders_of_rohan",
                    "riders_of_rohan_damped" ]
            } ];
        // Load sounds.
        ServiceLocator.audioManager.load( this._arrSoundLoadGroup );

        let gatewayView: GatewayView = new GatewayView();
        gatewayView.init();
        ServiceLocator.viewManager.add( gatewayView, view_layer_id.MAIN );

        if ( !Session.playerId )
        {
            let loginView: LoginView = new LoginView();
            loginView.onLoginSuccess.addOnce( this.onLogin_Success, this );
            loginView.init();
            ServiceLocator.viewManager.add( loginView, view_layer_id.MAIN );

            this._curView = loginView;
        }
        else
        {
            this._jukebox.onMainHallEntered();
            ( ServiceLocator.viewManager.findViewByResId( GatewayView.kResId )[ 0 ] as GatewayView ).onUserConnected();

            let dojoView: DojoView = new DojoView();
            dojoView.onSessionClosed.addOnce( this.onSession_Closed, this );
            dojoView.onPlay.add( this.onPlay_Selected, this );
            dojoView.init();
            ServiceLocator.viewManager.add( dojoView, view_layer_id.MAIN );

            this._curView = dojoView;
        }

        // Loading animation.
        ServiceLocator.loadingScreen.hide();
    }

    // private:

    private onLogin_Success(): void
    {
        this._jukebox.onMainHallEntered();
        ( ServiceLocator.viewManager.findViewByResId( GatewayView.kResId )[ 0 ] as GatewayView ).onUserConnected();

        let dojoView: DojoView = new DojoView();
        dojoView.onSessionClosed.addOnce( this.onSession_Closed, this );
        dojoView.onPlay.add( this.onPlay_Selected, this );
        dojoView.init();

        ServiceLocator.viewManager.fadeOut( this._curView );
        this._curView = dojoView;
        ServiceLocator.viewManager.fadeIn( dojoView, view_layer_id.MAIN );

        let chatView: ChatView = new ChatView();
        chatView.alias = "chat";
        chatView.onInvitationAccepted.add( this.onGameInvitation_Accepted, this );
        chatView.init();
        ServiceLocator.viewManager.fadeIn( chatView, view_layer_id.HUD, true );
    }

    private onSession_Closed(): void
    {
        this._jukebox.onLeft();
        ( ServiceLocator.viewManager.findViewByResId( GatewayView.kResId )[ 0 ] as GatewayView ).onUserDisconnected();
        
        let loginView: LoginView = new LoginView();
        loginView.onLoginSuccess.addOnce( this.onLogin_Success, this );
        loginView.init();
        ServiceLocator.viewManager.fadeIn( loginView, view_layer_id.MAIN );

        ServiceLocator.viewManager.fadeOut( ServiceLocator.viewManager.findViewByResId( ChatView.kResId )[ 0 ] );

        this._curView = loginView;
    }

    private onPlay_Selected(): void
    {
        ServiceLocator.viewManager.clearLayer( view_layer_id.POPUP );

        this._jukebox.onRoomEntered();

        let gameRoomView: GameRoomView = new GameRoomView();
        gameRoomView.mode = game_room_type.HOST;
        gameRoomView.init();
        gameRoomView.controller.onRoomLeft.addOnce( this.onGameRoom_Left, this );
        gameRoomView.controller.onGameCreated.addOnce( this.onServerGame_Created, this );
        gameRoomView.controller.onGameLoaded.addOnce( this.onGame_Loaded, this );

        ServiceLocator.viewManager.fadeOut( this._curView );
        this._curView = gameRoomView;
        ServiceLocator.viewManager.fadeIn( gameRoomView, view_layer_id.MAIN );

        ServiceLocator.socketIOManager.gateway.setUserStatus( user_status_type.IN_GAME_ROOM );

        // Sfx.
        ServiceLocator.audioManager.playSfx( "door_open" );
    }

    private onGameInvitation_Accepted(): void
    {
        ServiceLocator.viewManager.clearLayer( view_layer_id.POPUP );

        this._jukebox.onRoomEntered();

        let gameRoomView: GameRoomView = new GameRoomView();
        gameRoomView.mode = game_room_type.GUEST;
        gameRoomView.init();
        gameRoomView.controller.onRoomLeft.addOnce( this.onGameRoom_Left, this );
        gameRoomView.controller.onGameCreated.addOnce( this.onServerGame_Created, this );
        gameRoomView.controller.onGameLoaded.addOnce( this.onGame_Loaded, this );

        ServiceLocator.viewManager.fadeOut( this._curView );
        this._curView = gameRoomView;
        ServiceLocator.viewManager.fadeIn( gameRoomView, view_layer_id.MAIN );

        // Sfx.
        ServiceLocator.audioManager.playSfx( "door_open" );
    }

    private onGameRoom_Left(): void
    {
        ServiceLocator.viewManager.clearLayer( view_layer_id.POPUP );

        this._jukebox.onMainHallEntered();

        let dojoView: DojoView = new DojoView();
        dojoView.onSessionClosed.addOnce( this.onSession_Closed, this );
        dojoView.onPlay.add( this.onPlay_Selected, this );
        dojoView.init();

        ServiceLocator.viewManager.fadeOut( this._curView );
        this._curView = dojoView;
        ServiceLocator.viewManager.fadeIn( dojoView, view_layer_id.MAIN );

        // Sfx.
        ServiceLocator.audioManager.playSfx( "door_closed" );
    }

    private onServerGame_Created( prngSeed: number, firstPlayerId: string, isHost: boolean, treasureListVO: TreasureListVO, campaignLogVO: CampaignLogVO ): void
    {
        ServiceLocator.loadingScreen.show();

        this.addGameHistory();

        if ( Session.allyId )
        {
            this._gameCreation = {
                isPlayerGameCreated: false,
                isAllyGameCreated: false,
                saveGame: null };
        }
        else
        {
            this._gameCreation = {
                isPlayerGameCreated: true,
                isAllyGameCreated: true,
                saveGame: null };
        }

        const kScenarioInfo: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
        let arrEncounter: Array<string> = [ kScenarioInfo.encounter_set.main ];
        if ( kScenarioInfo.encounter_set.additional )
        {
            for ( let encounterSet of kScenarioInfo.encounter_set.additional )
            {
                arrEncounter.push( encounterSet );
            }
        }
        let arrCard: Array<ICard> = ServiceLocator.cardDb.findByEncounterSet( arrEncounter );
        if ( kScenarioInfo.treasure_sets )
        {
            for ( let i: number = arrCard.length - 1; i >= 0; --i )
            {
                if ( arrCard[ i ].type_code == "treasure" )
                {
                    arrCard.splice( i, 1 );
                }
            }
            let arrTreasureCard: Array<ICard> = ServiceLocator.cardDb.findByTypeCodes( [ "treasure" ] );
            for ( let card of arrTreasureCard )
            {
                if ( kScenarioInfo.treasure_sets.indexOf( card.treasure_set ) != -1 )
                {
                    arrCard.push( card );
                }
            }
        }
        if ( kScenarioInfo.additional_cards )
        {
            for ( let cardId of kScenarioInfo.additional_cards )
            {
                let isRepeated: boolean = false;
                for ( let card of arrCard )
                {
                    if ( card.code == cardId )
                    {
                        isRepeated = true;
                        break;
                    }
                }
                if ( !isRepeated )
                {
                    arrCard.push( ServiceLocator.cardDb.find( cardId ) );
                }
            }
        }
        if ( campaignLogVO )
        {
            let arrCampaignPoolCard: Array<ICampaignPoolCard> = campaignLogVO.boons.concat( campaignLogVO.burdens );
            for ( let campaignPoolCard of arrCampaignPoolCard )
            {
                let isRepeated: boolean = false;
                for ( let card of arrCard )
                {
                    if ( card.code == campaignPoolCard.id )
                    {
                        isRepeated = true;
                        break;
                    }
                }
                if ( !isRepeated )
                {
                    arrCard.push( ServiceLocator.cardDb.find( campaignPoolCard.id ) );
                }
            }
        }

        Session.encounterDeck = [];
        Session.questDeck = [];
        Session.setAsideDeck = [];
        const kArrSkipCardTypeCode: Array<string> = [ "contract", "campaign", "nightmare-setup", "gencon-setup" ];
        for ( let card of arrCard )
        {
            let isSkipCard: boolean = Utils.arrayHasItem( kArrSkipCardTypeCode, card.type_code );
            if ( !isSkipCard && ( card.type_code == "quest-intro" || card.type_code == "quest" ) )
            {
                isSkipCard = kScenarioInfo.quest != null && kScenarioInfo.quest.deck != null;
            }
            if ( !isSkipCard && ( card.type_code == "quest-intro" || card.type_code == "quest" ) )
            {
                if ( kScenarioInfo.type == "nightmare" )
                {
                    isSkipCard = card.encounter_set != kScenarioInfo.encounter_set.main && card.encounter_set != kScenarioInfo.encounter_set.additional[ 0 ];
                }
                else
                {
                    isSkipCard = card.encounter_set != kScenarioInfo.encounter_set.main;
                }
            }
            if ( !isSkipCard )
            {
                let sauronDeck: Array<string> = null;
                if ( card.is_burden || card.is_boon )
                {
                    if ( card.is_burden && campaignLogVO && this.isBurdenInCampaignPool( card.code, campaignLogVO.burdens ) )
                    {
                        sauronDeck = Session.encounterDeck;
                    }
                    else
                    {
                        sauronDeck = Session.setAsideDeck;
                    }
                }
                else if ( card.side == "999003" )
                {
                    sauronDeck = Session.encounterDeck;
                } 
                else if ( card.type_code == "quest-intro" || card.type_code == "quest" )
                {
                    sauronDeck = Session.questDeck;
                }
                else 
                {
                    // Default.
                    sauronDeck = Session.setAsideDeck;
                }
                for ( let i: number = 0; i < card.quantity; ++i )
                {
                    sauronDeck.push( card.code );
                }
            }
        }
        if ( kScenarioInfo.removed_sauron_cards )
        {
            for ( let removedSauronCard of kScenarioInfo.removed_sauron_cards )
            {
                let cardIndex: number = Session.encounterDeck.indexOf( removedSauronCard );
                if ( cardIndex >= 0 )
                {
                    Session.encounterDeck.splice( cardIndex, 1 );
                }
                if ( cardIndex == -1 )
                {
                    cardIndex = Session.questDeck.indexOf( removedSauronCard );
                    if ( cardIndex >= 0 )
                    {
                        Session.questDeck.splice( cardIndex, 1 );
                    }
                }
                if ( cardIndex == -1 )
                {
                    cardIndex = Session.setAsideDeck.indexOf( removedSauronCard );
                    if ( cardIndex >= 0 )
                    {
                        Session.setAsideDeck.splice( cardIndex, 1 );
                    }
                }
            }
        }
        if ( kScenarioInfo.quest && kScenarioInfo.quest.deck )
        {
            for ( let questId of kScenarioInfo.quest.deck )
            {
                Session.questDeck.push( questId + "A", questId + "B" );
            }
        }
        else
        {
            // Sort default quest deck.
            let sortedQuestDeck: Array<string> = new Array<string>();
            for ( let cardCode of Session.questDeck )
            {
                let insertIndex: number = 0;
                for ( let sortedCardCode of sortedQuestDeck )
                {
                    if ( parseInt( cardCode.substr( 0, 6 ) ) < parseInt( sortedCardCode.substr( 0, 6 ) ) )
                    {
                        break;
                    }
                    else
                    {
                        insertIndex += 1;
                    }
                }
                sortedQuestDeck.splice( insertIndex, 0, cardCode );
            }
            Session.questDeck = sortedQuestDeck;
        }
            
        ServiceLocator.socketIOManager.gateway.onOpponentGameCreated.addOnce( this.onSyncOpponentGame_Created, this );

        ServiceLocator.viewManager.clearLayer( view_layer_id.POPUP );
        ServiceLocator.viewManager.hideLayer( view_layer_id.MAIN );

        let gameState: GameState = ServiceLocator.stateMachine.states.get( main_state_id.GAME ) as GameState;
        gameState.hostPlayer = isHost ? player_type.PLAYER : player_type.ALLY;
        gameState.prngSeed = prngSeed;
        gameState.startingPlayer = firstPlayerId == Session.playerId ? player_type.PLAYER : player_type.ALLY;
        gameState.treasureList = treasureListVO;
        gameState.campaignLog = campaignLogVO;
        let sb: SignalBinding = gameState.onGameCreated.addOnce( this.onGame_Created, this );
        sb.params = [ null ];
        ServiceLocator.stateMachine.request( main_state_id.GAME );
    }

        private isBurdenInCampaignPool( card: string, arrBurden: Array<ICampaignPoolCard> ): boolean
        {
            let result: boolean = false;

            for ( let burden of arrBurden )
            {
                if ( burden.id == card )
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        private addGameHistory(): void
        {
            ServiceLocator.dbConnector.post( { 
                serviceName: "GameHistoryDAO",
                methodName: "setHistory",
                parameters: [ Session.playerId, Session.scenarioId, Session.allyId ? Session.allyId : "", Session.token ] },
                null );
        }

    private onGame_Created( saveGame: ISaveGame ): void
    {
        if ( Session.allyId && !this._gameCreation.isPlayerGameCreated )
        {
            ServiceLocator.socketIOManager.gateway.setOpponentGameCreated();
        }

        if ( this._gameCreation.isAllyGameCreated )
        {
            ServiceLocator.viewManager.showLayer( view_layer_id.MAIN );
            if ( !saveGame )
            {
                ServiceLocator.game.stateMachine.onStateEntered.addOnce( ( state: State ) => {
                    let phaseIntroState: PhaseIntroState = state as PhaseIntroState;
                    phaseIntroState.nextStateId = game_state_id.SETUP; } );
                ServiceLocator.game.stateMachine.request( game_state_id.PHASE_INTRO );
            }
            else
            {
                ServiceLocator.audioManager.mute( true, sound_group_id.SFX );
                this.lockOpponentSideBar();

                let underTableLayer: GameObject = ServiceLocator.game.root.cGameLayerProvider.get( layer_type.UNDER_TABLE );
                let rdc: RemoteDeckCreator = new RemoteDeckCreator();
                const kRemoteDeck: IRemoteDeck = rdc.create();
                let arrPlayerRemoteCard: Array<IRemoteCard> = kRemoteDeck.playerDeck.hero
                    .concat( kRemoteDeck.playerDeck.player )
                    .concat( kRemoteDeck.playerDeck.sideboard );
                if ( kRemoteDeck.playerDeck.contract )
                {
                    arrPlayerRemoteCard = arrPlayerRemoteCard.concat( kRemoteDeck.playerDeck.contract );
                }
                for ( let remoteCard of arrPlayerRemoteCard )
                {
                    let card: GameObject = this.createGameCard( remoteCard, player_type.PLAYER );
                    underTableLayer.cDropArea.forceDrop( card, null, action_scope_type.LOCAL );
                }
                if ( Session.allyId )
                {
                    let arrAllyRemoteCard: Array<IRemoteCard> = kRemoteDeck.allyDeck.hero
                        .concat( kRemoteDeck.allyDeck.player )
                        .concat( kRemoteDeck.allyDeck.sideboard );
                    if ( kRemoteDeck.allyDeck.contract )
                    {
                        arrAllyRemoteCard = arrAllyRemoteCard.concat( kRemoteDeck.allyDeck.contract );
                    }
                    for ( let remoteCard of arrAllyRemoteCard )
                    {
                        let card: GameObject = this.createGameCard( remoteCard, player_type.ALLY );
                        underTableLayer.cDropArea.forceDrop( card, null, action_scope_type.LOCAL );
                    }
                }
                let arrSauronRemoteCard: Array<IRemoteCard> = kRemoteDeck.encounter
                    .concat( kRemoteDeck.quest )
                    .concat( kRemoteDeck.setAside );
                if ( kRemoteDeck.nightmareSetup )
                {
                    arrSauronRemoteCard = arrSauronRemoteCard.concat( kRemoteDeck.nightmareSetup );
                }
                if ( kRemoteDeck.campaign )
                {
                    arrSauronRemoteCard = arrSauronRemoteCard.concat( kRemoteDeck.campaign );
                }
                if ( kRemoteDeck.genconSetup )
                {
                    arrSauronRemoteCard = arrSauronRemoteCard.concat( kRemoteDeck.genconSetup );
                }
                for ( let remoteCard of arrSauronRemoteCard )
                {
                    let card: GameObject = this.createGameCard( remoteCard, player_type.SAURON );
                    underTableLayer.cDropArea.forceDrop( card, null, action_scope_type.LOCAL );
                }
        
                ServiceLocator.game.loadGame( saveGame.gameState, 0 );
                //
                GameObject.oidCounter = saveGame.global.gameObjectOidCounter;
                CEngagedActorArea.genericEnemyCounter = saveGame.global.genericEnemyCounter;
                //
                ServiceLocator.game.cGameWorld.cActionLogger.setEnabled( false );
                ServiceLocator.game.loadGame( saveGame.gameState, 1 );
                ServiceLocator.game.cGameWorld.cActionLogger.setEnabled( true );

                ServiceLocator.game.allyActionManager.resume();
                ServiceLocator.audioManager.mute( !ServiceLocator.savedData.data.isSfx, sound_group_id.SFX );
            }

            ServiceLocator.loadingScreen.hide();
            ServiceLocator.loadingScreen.setMessage( jQuery.i18n( "LOADING" ) );
        }
        else
        {
            this._gameCreation.isPlayerGameCreated = true;
            this._gameCreation.saveGame = saveGame;

            ServiceLocator.loadingScreen.setMessage( jQuery.i18n( "WAITING_FOR_YOUR_OPPONENT" ) );
        }
    }

        private createGameCard( remoteCardId: IRemoteCard, playerType: player_type ): GameObject
        {
            let cardFactory: CardFactory = new CardFactory();
            let card: GameObject = cardFactory.create( {
                oid: remoteCardId.oid,
                frontCardId: remoteCardId.cid,
                backCardId: ServiceLocator.cardDb.find( remoteCardId.cid ).side,
                ownerPlayer: playerType,
                controllerPlayer: playerType == player_type.SAURON ? player_type.PLAYER : playerType,
                location: location_type.UNDER_TABLE,
                isFaceUp: remoteCardId.isFaceUp } );

            let isPlayer: boolean = playerType == player_type.PLAYER || playerType == player_type.SAURON;
            card.cContainer.c.buttonMode = isPlayer;
            card.cCard.setEnabled( isPlayer );

            return card;
        }

    private onGame_Loaded( prngSeed: number, saveGame: ISaveGame, isHost: boolean ): void
    {
        ServiceLocator.loadingScreen.show();

        ServiceLocator.socketIOManager.gateway.onOpponentGameCreated.addOnce( this.onSyncOpponentGame_Created, this );

        if ( Session.playerId == saveGame.session.allyId )
        {
            let oppSaveGameGenerator = new OppSaveGameGenerator();
            saveGame = oppSaveGameGenerator.generate( saveGame );
        }
        Session.playerId = saveGame.session.playerId;
        Session.allyId = saveGame.session.allyId;
        Session.playerDeck = saveGame.session.playerDeck;
        if ( !Session.playerDeck.sideboard )
        {
            Session.playerDeck.sideboard = new Array<string>();
        }
        Session.allyDeck = saveGame.session.allyDeck;
        if ( Session.allyDeck && !Session.allyDeck.sideboard )
        {
            Session.allyDeck.sideboard = new Array<string>();
        }
        Session.encounterDeck = saveGame.session.encounterDeck;
        Session.questDeck = saveGame.session.questDeck;
        Session.setAsideDeck = saveGame.session.setAsideDeck;
        Session.scenarioId = saveGame.session.scenarioId;
        Session.questDeck = saveGame.session.questDeck;

        this.addGameHistory();

        this._gameCreation = {
            isPlayerGameCreated: false,
            isAllyGameCreated: Session.allyId == null,
            saveGame: null };

        ServiceLocator.cardDb.loadGame( saveGame.cardDbCustomEntries, 0 );

        ServiceLocator.viewManager.clearLayer( view_layer_id.POPUP );
        ServiceLocator.viewManager.hideLayer( view_layer_id.MAIN );

        let gameState: GameState = ServiceLocator.stateMachine.states.get( main_state_id.GAME ) as GameState;
        gameState.hostPlayer = isHost ? player_type.PLAYER : player_type.ALLY;
        gameState.prngSeed = prngSeed;
        gameState.startingPlayer = saveGame.gameState.startingPlayer;
        let sb: SignalBinding = gameState.onGameCreated.addOnce( this.onGame_Created, this );
        sb.params = [ saveGame ];
        ServiceLocator.stateMachine.request( main_state_id.GAME );
    }

    // #endregion //
}

interface IGameCreation
{
    isPlayerGameCreated: boolean;
    isAllyGameCreated: boolean;
    saveGame: ISaveGame;
}