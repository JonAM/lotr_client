import Config from "./Config";
import Url from "./Url";


export default class DbConnector
{
    public post( callData: ICallData, cb: ( ...result: any[] ) => void ): void
    {
        jQuery.post( Url.amfPhp, JSON.stringify( callData ), this.result.bind( this, cb ) );
    }

    private result( cb: ( ...result: any[] ) => void, data: any ): void
    {
        let isError: boolean = false;
        if ( typeof data == typeof "string" 
            && ( ( data as string ).indexOf( "Fatal error" ) != -1
                || ( data as string ).indexOf( "Unrecowerable error" ) != -1
                || ( data as string ).indexOf( "Uncaught exception" ) != -1
                || ( data as string ).indexOf( "Parse error" ) != -1 ) )
        {
            // Server error.
            console.error( data as string );
            if ( Config.kIsDebugMode ) { debugger; }

            isError = true;
        }

        if ( cb && !isError )
        {
            cb( ...data );
        }
    }
}

export interface ICallData
{
    serviceName: string;
    methodName: string;
    parameters?: Array<any>;
}