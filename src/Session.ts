import { IDeck as IPlayerDeck } from "./view/gateway/DeckParser";
import { ISgSession } from "./view/game/SaveGameView";


export default class Session
{
    // #region Attributes //

    public static playerId: string = null;
    public static token: string = null;
    public static allyId: string = null;
    public static playerDeck: IPlayerDeck = null;
    public static allyDeck: IPlayerDeck = null;
    public static scenarioId: string = null;
    public static encounterDeck: Array<string> = null;
    public static questDeck: Array<string> = null;
    public static setAsideDeck: Array<string> = null;
    public static isCampaignMode: boolean = false;
    public static gameId: string = null;

    // #endregion //


    // #region Methods //

    public static reset(): void
    {
        Session.allyId = null;
        Session.playerDeck = null;
        Session.allyDeck = null;
        Session.scenarioId = null;
        Session.encounterDeck = null;
        Session.questDeck = null;
        Session.setAsideDeck = null;
        Session.isCampaignMode = false;
        Session.gameId = null;
    }

    public static saveGame(): ISgSession
    {
        let sgSession: ISgSession = {
            playerId: Session.playerId,
            allyId: Session.allyId,
            playerDeck: Session.playerDeck,
            allyDeck: Session.allyDeck,
            scenarioId: Session.scenarioId,
            encounterDeck: Session.encounterDeck,
            questDeck: Session.questDeck,
            setAsideDeck: Session.setAsideDeck,
            isCampaignMode: Session.isCampaignMode };

        return sgSession;
    }

    // #endregion //
}