import LogTarget, { log_target_type } from "../game/component/ui/right_menu/action_log/LogTarget";
import LogTargetCard from "../game/component/ui/right_menu/action_log/target/LogTargetCard";
import GameObject from "../game/GameObject";
import Component from "../game/component/Component";
import { log_layout_type } from "../game/component/ui/right_menu/CActionLogger";
import CTextPreviewable from "../game/component/input/CTextPreviewable";
import CLogPhaseStart from "../game/component/ui/right_menu/action_log/CLogPhaseStart";
import CLogActivePlayer from "../game/component/ui/right_menu/action_log/CLogActivePlayer";
import CLogSingleIcon from "../game/component/ui/right_menu/action_log/CLogSingleIcon";
import CLogSequence from "../game/component/ui/right_menu/action_log/CLogSequence";
import CLogCounter from "../game/component/ui/right_menu/action_log/CLogCounter";
import CLogActionPreviewable from "../game/component/input/CLogActionPreviewable";
import CContainer from "../game/component/pixi/CContainer";
import CHighlightPoiReceptor from "../game/component/world/poi_receptor/CHighlightPoiReceptor";
import CLogTargetSelection from "../game/component/ui/right_menu/action_log/CLogTargetSelection";
import LogTargetDeck from "../game/component/ui/right_menu/action_log/target/LogTargetDeck";
import LogTargetPlayer from "../game/component/ui/right_menu/action_log/target/LogTargetPlayer";
import LogTargetFirstPlayer from "../game/component/ui/right_menu/action_log/target/LogTargetFirstPlayer";
import CLogCardFlip from "../game/component/ui/right_menu/action_log/CLogCardFlip";


export default class ActionLogUtils
{
    // #region Methods //

    // public:

   public deserializeTarget( serialized: Array<any> ): LogTarget
   {
      let logTarget: LogTarget = null;
      switch ( serialized[ 0 ] as log_target_type )
      {
         case log_target_type.CARD: { logTarget = new LogTargetCard(); break; }
         case log_target_type.FIRST_PLAYER: { logTarget = new LogTargetFirstPlayer(); break; }
         case log_target_type.PLAYER: { logTarget = new LogTargetPlayer(); break; } 
         case log_target_type.DECK: { logTarget = new LogTargetDeck(); break; } 
      }
      logTarget.deserialize( serialized.slice( 1 ) );

      return logTarget;
   }

    public deserializeAction( serialized: Array<any>, oid: string ): GameObject
    {
        let arrComponent: Array<Component> = null;
        switch ( serialized[ 0 ] as log_layout_type )
        {
            case log_layout_type.PHASE_START: { arrComponent = [ new CContainer(), new CLogPhaseStart(), new CTextPreviewable(), new CHighlightPoiReceptor() ]; break; }
            case log_layout_type.ACTIVE_PLAYER: { arrComponent = [ new CContainer(), new CLogActivePlayer(), new CTextPreviewable(), new CHighlightPoiReceptor() ]; break; }
            case log_layout_type.SINGLE_ICON: { arrComponent = [ new CContainer(), new CLogSingleIcon(), new CLogActionPreviewable(), new CHighlightPoiReceptor() ]; break; }
            case log_layout_type.SEQUENCE: { arrComponent = [ new CContainer(), new CLogSequence(), new CLogActionPreviewable(), new CHighlightPoiReceptor() ]; break; }
            case log_layout_type.COUNTER: { arrComponent = [ new CContainer(), new CLogCounter(), new CLogActionPreviewable(), new CHighlightPoiReceptor() ]; break; }
            case log_layout_type.TARGET_SELECTION: { arrComponent = [ new CContainer(), new CLogTargetSelection(), new CLogActionPreviewable(), new CHighlightPoiReceptor() ]; break; }
            case log_layout_type.CARD_FLIP: { arrComponent = [ new CContainer(), new CLogCardFlip(), new CLogActionPreviewable(), new CHighlightPoiReceptor() ]; break; }
        }
        
        let logAction: GameObject = new GameObject( arrComponent );
        logAction.oid = oid;
        logAction.cLogAction.deserialize( serialized.slice( 1 ) );
        logAction.init();

        return logAction;
    }

    // #endregion //
}