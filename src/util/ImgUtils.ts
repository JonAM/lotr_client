import { player_type } from "../game/component/world/CPlayerArea";
import { location_type } from "../game/component/world/CGameWorld";
import ServiceLocator from "../ServiceLocator";
import * as PIXI from "pixi.js";
import { ICard } from "../game/CardDB";


export default class ImgUtils
{
    // #region Methods //

    // public:

    public createCardPortrait( cardId: string, options: any = {} ): PIXI.Texture
    {
        let result: PIXI.Texture = null;

        const kCardInfo: ICard = ServiceLocator.cardDb.find( cardId );
        let frame: IFrame = null;
        if ( kCardInfo.is_boon || kCardInfo.is_burden )
        {
            frame = { origin: new PIXI.Point( 137, 102 ), width: 166, height: 236 }; 
        }
        else
        {
            switch( kCardInfo.type_code )
            {
                case "hero": { frame = { origin: new PIXI.Point( 143, 5 ), width: 236, height: 338 }; break; }

                case "ally": 
                case "treachery":
                case "ship-enemy": 
                case "enemy": { frame = { origin: new PIXI.Point( 163, 5 ), width: 208, height: 298 }; break; }

                case "location": { frame = { origin: new PIXI.Point( 143, 62 ), width: 186, height: 266 }; break; }
                
                case "attachment":
                case "treasure":
                case "event":
                case "ship-objective":
                case "objective-ally":
                case "objective-hero":
                case "objective-location":
                case "objective": { frame = { origin: new PIXI.Point( 143, 98 ), width: 186, height: 266 }; break; }
                
                case "campaign":
                case "scenario":
                case "contract":
                case "gencon-setup":
                case "nightmare-setup": { frame = { origin: new PIXI.Point( 158, 89 ), width: 123, height: 176 }; break; }

                case "quest-intro":
                case "quest": { frame = { origin: new PIXI.Point( 192, 85 ), width: 246, height: 173 }; break; }
            
                case "player-side-quest":
                case "encounter-side-quest": { frame = { origin: new PIXI.Point( 247, 66 ), width: 135, height: 193 }; break; }

                case "player-back":
                case "encounter-back": { frame = { origin: new PIXI.Point( 0, 0 ), width: 440, height: 628 }; break; }
            }
        }

        if ( options.isSquare )
        {
            if ( kCardInfo.type_code == "quest-intro"
                || kCardInfo.type_code == "quest" )
            {
                const kDiff: number = frame.width - frame.height;
                frame.origin.x += kDiff * 0.5;
                frame.width = frame.height;
            }
            else
            {
                const kDiff: number = frame.height - frame.width;
                frame.origin.y += kDiff * 0.5;
                frame.height = frame.width;
            }
        }
        
        // Portrait.
        const kTexturePackId: string = ServiceLocator.cardDb.find( cardId ).texture_map_id;
        let resource: PIXI.LoaderResource = ServiceLocator.resourceStack.find( kTexturePackId );
        if ( resource )
        {
            let spiteSheetTexture: PIXI.Texture = resource.textures[ cardId + ".jpg" ];
            if ( spiteSheetTexture.rotate == 2 )
            {
                // Rotated.
                result = new PIXI.Texture( 
                    spiteSheetTexture.baseTexture, 
                    new PIXI.Rectangle( 
                        spiteSheetTexture.frame.x - frame.origin.y + spiteSheetTexture.orig.height - frame.height, 
                        spiteSheetTexture.frame.y + frame.origin.x, 
                        frame.height, frame.width ),
                    new PIXI.Rectangle( 0, 0, frame.width, frame.height ),
                    spiteSheetTexture.trim,
                    spiteSheetTexture.rotate );
            }
            else if ( spiteSheetTexture.rotate == 0 )
            {
                result = new PIXI.Texture( 
                    spiteSheetTexture.baseTexture, 
                    new PIXI.Rectangle( 
                        spiteSheetTexture.frame.x + frame.origin.x, 
                        spiteSheetTexture.frame.y + frame.origin.y, 
                        frame.width, frame.height ),
                    new PIXI.Rectangle( 0, 0, frame.width, frame.height ),
                    spiteSheetTexture.trim,
                    spiteSheetTexture.rotate );
            }
        }
        else
        {
            result = PIXI.Texture.from( ServiceLocator.resourceStack.find( "temp_portrait" ).data );
        }

        return result;
    }

    public findPlayerTextureId( playerType: player_type ): string
    {
        return [ "player", "ally", "sauron" ][ playerType ];
    }

    public findLocationTextureId( location: location_type ): string
    {
        let result: string = null;

        switch ( location )
        {
            case location_type.STAGING_AREA: 
            case location_type.PURSUIT_STAGING_AREA: 
            case location_type.ISOLATED_STAGING_AREA: 
            case location_type.SPLIT_SAURON_STAGING_AREA: { result = "staging"; break; };

            case location_type.RIDDLE_ACTOR_AREA: { result = "riddle"; break; };

            case location_type.PIT_ACTOR_AREA: { result = "pit"; break; };

            case location_type.MY_ENGAGED:
            case location_type.ALLY_ENGAGED: 
            case location_type.ISOLATED_ENGAGED: { result = "engaged"; break; };

            case location_type.MY_HOME:
            case location_type.ALLY_HOME: 
            case location_type.ISOLATED_HOME: { result = "home"; break; };
          
            case location_type.SAURON_DECK_0: { result = "encounter_deck"; break; };
            case location_type.SAURON_DECK_1: { result = "secondary_encounter_deck"; break; };
            case location_type.SAURON_DISCARD: { result = "encounter_discard"; break; };
            
            case location_type.SAURON_CUSTOM_DECK_0: { result = "sauron_custom_deck"; break; };
            case location_type.SAURON_CUSTOM_DISCARD_0: { result = "sauron_custom_discard"; break; };
            
            case location_type.SAURON_CUSTOM_DECK_1: { result = "secondary_sauron_custom_deck"; break; };
            case location_type.SAURON_CUSTOM_DISCARD_1: { result = "sauron_custom_discard"; break; };

            case location_type.QUEST_DECK: { result = "quest_deck"; break; };
            case location_type.QUEST_DISCARD: { result = "quest_discard"; break; };

            case location_type.SET_ASIDE:
            case location_type.MY_SET_ASIDE:
            case location_type.ALLY_SET_ASIDE: { result = "set_aside"; break; };
        
            case location_type.REMOVED_FROM_GAME: { result = "removed_from_game"; break; };
        
            case location_type.VICTORY_DISPLAY: { result = "victory_display"; break; };

            case location_type.CARD_TRAY: { result = "tray"; break; };

            case location_type.MY_DECK:
            case location_type.ALLY_DECK: { result = "player_deck"; break; };

            case location_type.MY_DISCARD:
            case location_type.ALLY_DISCARD: { result = "player_discard"; break; };

            case location_type.MY_HAND:
            case location_type.ALLY_HAND: { result = "hand"; break; };

            case location_type.MY_CUSTOM_DECK:
            case location_type.ALLY_CUSTOM_DECK: { result = "player_custom_deck"; break; };

            case location_type.ACTIVE_LOCATION: 
            case location_type.ISOLATED_ACTIVE_LOCATION: 
            case location_type.SPLIT_SAURON_ACTIVE_LOCATION: { result = "active_location_holder"; break; };

            case location_type.DOR_CUARTHOL_LOCATION: { result = "dor-cuarthol_location_holder"; break; };
            case location_type.ISLAND_LOCATION: { result = "island_location_holder"; break; };

            case location_type.QUEST:
            case location_type.RIDDLE_QUEST:
            case location_type.PIT_QUEST:
            case location_type.PURSUIT_QUEST:
            case location_type.ISOLATED_QUEST: 
            case location_type.SPLIT_SAURON_QUEST: { result = "quest_holder"; break; };
            
            case location_type.MY_UNDERNEATH: 
            case location_type.ALLY_UNDERNEATH: { result = "rad_underneath"; break; };
        }

        return result;
    }

    public findCardTexture( cardId: string ): PIXI.Texture
    {
        const kResource: PIXI.LoaderResource = ServiceLocator.resourceStack.find( ServiceLocator.cardDb.find( cardId ).texture_map_id );
        return kResource.textures[ cardId + ".jpg" ];
    }

    // #endregion //
}

interface IFrame
{
    origin: PIXI.Point;
    width: number;
    height: number;
}