import { player_type } from "../game/component/world/CPlayerArea";
import { action_scope_type } from "../service/socket_io/GameSocketIOController";
import { location_type } from "../game/component/world/CGameWorld";
import ServiceLocator from "../ServiceLocator";
import { status_type } from "../game/component/card/token/CCardTokenSide";
import Utils from "../Utils";
import * as PIXI from "pixi.js";

import GameObject from "../game/GameObject";
import IGameObjectDropArea from "../game/IGameObjectDropArea";
import CCardView, { deck_side_type } from "../game/component/ui/viewer/CCardView";
import CDeckIndicator from "../game/component/ui/indicator/CDeckIndicator";
import State from "../lib/state_machine/State";
import StateMachine from "../lib/StateMachine";
import { layer_type } from "../game/component/world/CGameLayerProvider";
import CardViewerController from "../game/component/ui/viewer/controller/CardViewerController";


export default class GameUtils
{
    // #region Methods //

    // public:

    public shuffle( array: any ): void
    {
        for ( let i: number = array.length - 1; i > 0; --i )
        {
            const j: number = ServiceLocator.prng.randInterval( 0, i );
            const temp: any = array[ i ];
            array[ i ] = array[ j ];
            array[ j ] = temp;
        }
    }

    public parseCounterValue( value: string ): number
    {
        let result: number = parseInt( value );
        if ( isNaN( result ) )
        {
            result = 0;
        }

        return result;
    }

    public limitSideSize( maxLength: number, container: PIXI.Container ): void
    {
        if ( container instanceof PIXI.Sprite )
        {
            let sprite: PIXI.Sprite = container as PIXI.Sprite;
            const kScale = maxLength / Math.max( sprite.texture.width, sprite.texture.height );
            sprite.scale.set( kScale );
        }
        else
        {
            const kScale = maxLength / Math.max( container.width, container.height );
            container.scale.set( kScale );
        }
    }

    public setSideSize( length: number, sprite: PIXI.Sprite ): void
    {
        const kScale: number = length / Math.max( sprite.width, sprite.height );
        sprite.scale.set( kScale );
    }

    public isAncestor( who: PIXI.DisplayObject, candidate: PIXI.Container ): boolean
    {
        let result: boolean = false;

        let parent: PIXI.Container = who.parent;
        while ( parent && parent != ServiceLocator.game.app.stage )
        {
            if ( parent == candidate )
            {
                result = true;
                break;
            }
            else
            {
                parent = parent.parent;
            }
        }

        return result;
    }

    public checkBranchVisibility( container: PIXI.Container ): boolean
    {
        let result: boolean = container.visible;

        if ( result )
        {
            let parent: PIXI.Container = container.parent;
            while ( parent != ServiceLocator.game.app.stage )
            {
                if ( !parent || !parent.visible )
                {
                    result = false;
                    break;
                }
                else
                {
                    parent = parent.parent;
                }
            }
        }

        return result;
    }

    public findBranchGameLayer( container: PIXI.Container ): GameObject
    {
        let result: GameObject = null;

        let parent: PIXI.Container = container.parent;
        while ( parent != ServiceLocator.game.app.stage && parent != null )
        {
            if ( parent[ "go" ] && ( parent[ "go" ] as GameObject ).cGameLayer )
            {
                result = parent[ "go" ];
                break;
            }
            else
            {
                parent = parent.parent;
            }
        }

        return result;
    }

    public instanceofIGameObjectDropArea( object: any ): object is IGameObjectDropArea 
    {
        return "validateDroppedGameObject" in object && "processDroppedGameObject" in object;
    }

    public discard( discarded: GameObject, actionScopeType: action_scope_type ): void
    {
        let isShuffle: boolean = false;
        let ownerPlayer: player_type = null;
        let backCardId: string = null;
        let frontCardId: string = null;
        if ( discarded.cCard )
        {
            ownerPlayer = discarded.cCard.ownerPlayer;
            backCardId = discarded.cCard.back.cardId;
            frontCardId = discarded.cCard.front.cardId;
        }
        else if ( discarded.cCardToken )
        {
            ownerPlayer = discarded.cCardToken.ownerPlayer;
            backCardId = discarded.cCardToken.cBackSide.cardId;
            frontCardId = discarded.cCardToken.cFrontSide.cardId;
        }
        else if ( discarded.cAttachment )
        {
            ownerPlayer = discarded.cAttachment.ownerPlayer;
            backCardId = discarded.cAttachment.back.cardId;
            frontCardId = discarded.cAttachment.front.cardId;
        }
        else if ( discarded.cGameModifier )
        {
            ownerPlayer = discarded.cGameModifier.ownerPlayer;
            backCardId = discarded.cGameModifier.back.cardId;
            frontCardId = discarded.cGameModifier.front.cardId;
        }

        let cDeckIndicator: CDeckIndicator = null;
        if ( ( discarded.cCardToken || discarded.cAttachment ) && ServiceLocator.cardDb.find( frontCardId ).victory )
        {
            cDeckIndicator = ServiceLocator.game.cGameWorld.findLocation( location_type.VICTORY_DISPLAY ).cDeckIndicator;
        }
        else if ( ServiceLocator.game.forcedDiscardLocations.has( frontCardId ) )
        {
            const kForcedLocation: location_type = ServiceLocator.game.forcedDiscardLocations.get( frontCardId );
            cDeckIndicator = ServiceLocator.game.cGameWorld.findLocation( kForcedLocation ).cDeckIndicator;
            isShuffle = kForcedLocation == location_type.SAURON_DECK_0
                || kForcedLocation == location_type.SAURON_DECK_1
                || kForcedLocation == location_type.SAURON_CUSTOM_DECK_0
                || kForcedLocation == location_type.SAURON_CUSTOM_DECK_1;
        }
        else
        {
            if ( ServiceLocator.cardDb.find( frontCardId ).type_code == "contract" )
            {
                if ( ownerPlayer == player_type.PLAYER )
                {
                    cDeckIndicator = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.setAside.cDeckIndicator;
                }
                else if ( ownerPlayer == player_type.ALLY )
                {
                    cDeckIndicator = ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.setAside.cDeckIndicator;
                }
            }
            else if ( backCardId != "999002" && backCardId != "999003" )
            {
                // Double-sided.
                if ( ServiceLocator.cardDb.find( frontCardId ).type_code == "quest" )
                {
                    cDeckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.questDiscard.cDeckIndicator;
                }
                else
                {
                    cDeckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.removedFromGame.cDeckIndicator;
                }
            }
            else if ( backCardId == "999003" )
            {
                // Encounter back.
                if ( ServiceLocator.cardDb.find( frontCardId ).text.indexOf( "Encounter." ) != -1 )
                {
                    cDeckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.removedFromGame.cDeckIndicator;
                }
                else
                {
                    cDeckIndicator = ServiceLocator.game.cGameWorld.cSauronArea.cSideControls.discard.cDeckIndicator;
                }
            }
            else if ( backCardId == "999002" )
            {
                // Player back.
                if ( ownerPlayer == player_type.PLAYER )
                {
                    cDeckIndicator = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.discard.cDeckIndicator;
                }
                else if ( ownerPlayer == player_type.ALLY )
                {
                    cDeckIndicator = ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.discard.cDeckIndicator;
                }
            }
        }
        cDeckIndicator.cViewer.cCardView.go.cDropArea.forceDrop( discarded, CCardView.findPredefinedDropPosition( deck_side_type.TOP ), actionScopeType );
        if ( isShuffle )
        {
            ( cDeckIndicator.cViewer.controller as CardViewerController ).shuffle( action_scope_type.LOCAL );
        }
    }

    public findGameProviderLayer( from: GameObject, layerType: layer_type ): GameObject
    {
        let result: GameObject = null;

        while ( !result )
        {
            const kGameLayerProvider: GameObject = this.findGameObjectInBranchByComponentName( from, "CGameLayerProvider" );
            if ( kGameLayerProvider )
            {
                result = kGameLayerProvider.cGameLayerProvider.get( layerType );
                if ( !result )
                {
                    from = kGameLayerProvider;
                }
            }
            else
            {
                break;
            }
        }
        
        return result;
    }

    public findGameObjectInBranchByComponentName( from: GameObject, componentName: string ): GameObject
    {
        let result: GameObject = null;

        const kComponentAccesor: string = "c" + componentName.substr( 1 );
        let current: PIXI.Container = from.cContainer.c.parent;
        while ( current && current != ServiceLocator.game.app.stage && !result )
        {
            if ( current[ "go" ] != undefined )
            {
                let curGameObject: GameObject = current[ "go" ] as GameObject ;
                if ( curGameObject[ kComponentAccesor ] != undefined )
                {
                    result = curGameObject;
                }
            }
            
            if ( !result )
            {
                current = current.parent;
            }
        }

        return result;
    }

    public isSameGameLayerProvider( one: GameObject, two: GameObject ): boolean
    {
        return this.findGameObjectInBranchByComponentName( one, "CGameLayerProvider" ) == this.findGameObjectInBranchByComponentName( two, "CGameLayerProvider" );
    }

    public hitTest( global: PIXI.Point, componentName: string, root: PIXI.Container ): Array<GameObject>
    {
        let result: Array<GameObject> = new Array<GameObject>();

        for ( let displayObject of root.children )
        {
            if ( displayObject.worldVisible 
                && displayObject[ "go" ] != undefined
                && ( ( displayObject as any ).go as GameObject )[ componentName ] != undefined 
                && displayObject.getBounds().contains( global.x, global.y ) )
            {
                result.push( ( displayObject as any ).go as GameObject );
            }
            if ( displayObject instanceof PIXI.Container && displayObject.children.length > 0 )
            {
                result = result.concat( this.hitTest( global, componentName, displayObject ) );
            }
        }

        return result;
    }

    public hitFirstTest( global: PIXI.Point, componentName: string, root: PIXI.Container ): GameObject
    {
        let result: GameObject = null

        for ( let displayObject of root.children )
        {
            if ( displayObject.worldVisible 
                && displayObject[ "go" ] != undefined
                && ( ( displayObject as any ).go as GameObject )[ componentName ] != undefined 
                && displayObject.getBounds().contains( global.x, global.y ) )
            {
                result = ( displayObject as any ).go as GameObject;
            }
            else if ( displayObject instanceof PIXI.Container && displayObject.children.length > 0 )
            {
                result = this.hitFirstTest( global, componentName, displayObject );
            }

            if ( result )
            {
                break;
            }
        }

        return result;
    }

    public isPlayerEliminated( playerType: player_type ): boolean
    {
        let result: boolean = false;

        let playArea: GameObject = playerType == player_type.PLAYER ? ServiceLocator.game.cGameWorld.playerArea : ServiceLocator.game.cGameWorld.allyArea;
        result = playArea.cPlayerArea.cSideControls.threatLevel.cTokenIndicator.trigger.cTokenCounter.count >= 50;
        if ( !result )
        {
            let heroCount: number = 0;
            let deadHeroCount: number = 0;
            let arrActor: Array<GameObject> = playArea.cPlayerArea.cHome.findAllActors();
            for ( let actor of arrActor )
            {
                if ( actor.cCardToken.curSide.cHeroSide && actor.cCardToken.curSide.cHeroSide.hasStatus( status_type.DEAD ) )
                {
                    deadHeroCount += 1;
                }

                heroCount += 1;
            }

            result = heroCount == 0 || deadHeroCount == heroCount;
        }

        return result;
    }

    public transformToAllyLocation( playerLocation: location_type ): location_type
    {
        let result: location_type = playerLocation;

        switch ( playerLocation )
        {
            case location_type.MY_DECK: { result = location_type.ALLY_DECK; break; }
            case location_type.MY_SET_ASIDE: { result = location_type.ALLY_SET_ASIDE; break; }
            case location_type.MY_DISCARD: { result = location_type.ALLY_DISCARD; break; }
            case location_type.MY_HOME: { result = location_type.ALLY_HOME; break; }
            case location_type.MY_ENGAGED: { result = location_type.ALLY_ENGAGED; break; }
            case location_type.MY_HAND: { result = location_type.ALLY_HAND; break; }
            case location_type.MY_UNDERNEATH: { result = location_type.ALLY_UNDERNEATH; break; }
            case location_type.MY_CUSTOM_DECK: { result = location_type.ALLY_CUSTOM_DECK; break; }
        }

        return result;
    }

    public findGameSubstateId(): number
    {
        let result: number = null;

        let state: State = ServiceLocator.game.stateMachine.currentState;
        if ( state[ "_stateMachine" ] )
        {
            result = ( state[ "_stateMachine" ] as StateMachine ).currentStateId;
        }

        return result;
    }

    public isCardFrontSide( cardId: string ): boolean
    {
        return cardId.indexOf( "A" ) != -1 
            || cardId != "999002" && cardId != "999003";
    }

    public isAllyIsolated(): boolean
    {
        let result: boolean = ServiceLocator.game.cGameWorld.customPanelManager != null;

        if ( result )
        {
            result = ServiceLocator.game.cGameWorld.cCustomPanelManager.checkPanelVisibility( "isolated" );
        }

        return result;
    }

    public isMainStagingAreaIsolated(): boolean
    {
        let result: boolean = ServiceLocator.game.cGameWorld.customPanelManager != null;

        if ( result )
        {
            result = ServiceLocator.game.cGameWorld.cCustomPanelManager.checkPanelVisibility( "split_sauron" );
        }

        return result;
    }

    public dropUnderTableAndEndCard( card: GameObject ): void
    {
        ServiceLocator.game.root.cGameLayerProvider.get( layer_type.UNDER_TABLE ).cDropArea.forceDrop( card, null, action_scope_type.LOCAL );
        card.end();
    }

    public findControllerPlayerByLocation( locationType: location_type ): player_type 
    {
        let result: player_type = null;

        switch ( locationType )
        {
            case location_type.MY_HOME:
            case location_type.MY_ENGAGED: { result = player_type.PLAYER; break; }

            case location_type.ALLY_HOME:
            case location_type.ALLY_ENGAGED: 
            case location_type.ISOLATED_HOME:
            case location_type.ISOLATED_ENGAGED:
            case location_type.ISOLATED_STAGING_AREA:
            case location_type.ISOLATED_ACTIVE_LOCATION:
            case location_type.ISOLATED_QUEST: { result = player_type.ALLY; break; }

            default: { result = player_type.PLAYER; break; }
        }

        return result;
    }

    public isSharedLocation( locationType: location_type ): boolean 
    {
        return locationType != location_type.MY_HOME && locationType != location_type.MY_ENGAGED
            && locationType != location_type.ALLY_HOME && locationType != location_type.ALLY_ENGAGED
            && locationType != location_type.ISOLATED_HOME && locationType != location_type.ISOLATED_ENGAGED
            && locationType != location_type.ISOLATED_STAGING_AREA && locationType != location_type.ISOLATED_ACTIVE_LOCATION
            && locationType != location_type.ISOLATED_QUEST;
    }

    public showActor( actor: GameObject ): void
    {
        if ( actor.cContainer.c.worldVisible ) { return; }

        let customArea: GameObject = this.findGameObjectInBranchByComponentName( actor, "CCustomArea" );
        if ( customArea && !ServiceLocator.game.cGameWorld.cCustomPanelManager.checkPanelVisibility( customArea.cCustomArea.id ) )
        {
            ServiceLocator.game.cGameWorld.cCustomPanelManager.show( customArea.cCustomArea.id );
        }

        if ( !actor.cContainer.c.worldVisible ) 
        {
            let actorArea: GameObject = this.findGameObjectInBranchByComponentName( actor, "CActorArea" );
            if ( actorArea && actorArea.cActorArea.selActorTab.cActorTab.actors.indexOf( actor ) == -1 )
            {
                actorArea.cActorArea.showActor( actor, action_scope_type.LOCAL );
            }
        }

        if ( !actor.cContainer.c.worldVisible ) 
        {
            let activeLocationHolder: GameObject = this.findGameObjectInBranchByComponentName( actor, "CActiveLocationHolder" );
            if ( activeLocationHolder && activeLocationHolder.cActiveLocationHolder.activeLocationToken != actor )
            {
                activeLocationHolder.cActiveLocationHolder.showActor( actor, action_scope_type.LOCAL );
            }
        }

        if ( !actor.cContainer.c.worldVisible ) 
        {
            let questHolder: GameObject = this.findGameObjectInBranchByComponentName( actor, "CQuestHolder" );
            if ( questHolder && questHolder.cQuestHolder.questToken != actor )
            {
                questHolder.cQuestHolder.showActor( actor, action_scope_type.LOCAL );
            }
        }
    }

    public setPreviewPosition( previewContainer: PIXI.Container, targetGlobalPos: PIXI.Point, targetLocalBounds: PIXI.Rectangle,  ): void
    {
        const kWorldWidth: number = ServiceLocator.game.world.cContainer.c.width;
        const kWorldHeight: number = ServiceLocator.game.world.cContainer.c.height;
        const kMarginFromTarget: number = 20;

        previewContainer.position.set( 
            Math.min( Math.max( targetGlobalPos.x - kMarginFromTarget, 0 ), kWorldWidth - previewContainer.width ), 
            targetGlobalPos.y + targetLocalBounds.y - kMarginFromTarget );
        previewContainer.pivot.set( 0, previewContainer.height );
        let isValidAnchor: boolean = previewContainer.y - previewContainer.height >= 0;
        if ( !isValidAnchor )
        {
            if ( ServiceLocator.savedData.data.gamePreferences.cardPreviewPreferredSide == "right" )
            {
                previewContainer.position.set( 
                    targetGlobalPos.x + targetLocalBounds.x + targetLocalBounds.width + kMarginFromTarget, 
                    Math.min( Math.max( targetGlobalPos.y - kMarginFromTarget, 0 ), kWorldHeight - previewContainer.height ) );
                isValidAnchor = previewContainer.x + previewContainer.width <= kWorldWidth;
                if ( isValidAnchor )
                {
                    previewContainer.pivot.set( 0, 0 );
                }
            }
            else
            {
                previewContainer.position.set( 
                    targetGlobalPos.x + targetLocalBounds.x - kMarginFromTarget, 
                    Math.min( Math.max( targetGlobalPos.y - kMarginFromTarget, 0 ), kWorldHeight - previewContainer.height ) );
                isValidAnchor = previewContainer.x - previewContainer.width >= 0;
                if ( isValidAnchor )
                {
                    previewContainer.pivot.set( previewContainer.width, 0 );
                }
            }
        }
        if ( !isValidAnchor )
        {
            previewContainer.position.set( 
                Math.min( Math.max( targetGlobalPos.x - kMarginFromTarget, 0 ), kWorldWidth - previewContainer.width ), 
                targetGlobalPos.y + targetLocalBounds.y + targetLocalBounds.height + kMarginFromTarget );
            isValidAnchor = previewContainer.y + previewContainer.height <= kWorldHeight;
            if ( isValidAnchor )
            {
                previewContainer.pivot.set( 0, 0 );
            }
        }
        if ( !isValidAnchor )
        {
            if ( ServiceLocator.savedData.data.gamePreferences.cardPreviewPreferredSide == "right" )
            {
                previewContainer.position.set( 
                    targetGlobalPos.x + targetLocalBounds.x - kMarginFromTarget, 
                    Math.min( Math.max( targetGlobalPos.y - kMarginFromTarget, 0 ), kWorldHeight - previewContainer.height ) );
                isValidAnchor = previewContainer.x - previewContainer.width >= 0;
                if ( isValidAnchor )
                {
                    previewContainer.pivot.set( previewContainer.width, 0 );
                }
            }
            else
            {
                previewContainer.position.set( 
                    targetGlobalPos.x + targetLocalBounds.x + targetLocalBounds.width + kMarginFromTarget, 
                    Math.min( Math.max( targetGlobalPos.y - kMarginFromTarget, 0 ), kWorldHeight - previewContainer.height ) );
                isValidAnchor = previewContainer.x + previewContainer.width <= kWorldWidth;
                if ( isValidAnchor )
                {
                    previewContainer.pivot.set( 0, 0 );
                }
            }
        }
    }

    // #endregion //
}