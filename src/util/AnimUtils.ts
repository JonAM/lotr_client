import { TweenMax, gsap, Circ, Sine } from "gsap";
import GameObject from "../game/GameObject";
import CGraphics from "../game/component/pixi/CGraphics";
import ServiceLocator from "../ServiceLocator";
import { layer_type } from "../game/component/world/CGameLayerProvider";


export default class AnimUtils
{
    // #region Methods //

    // public:

    public flushTweensOf( target: any ): void
    {
        let arrTween: Array<gsap.core.Tween> = gsap.getTweensOf( target );
        for ( let tween of arrTween )
        {
            this.flushTween( tween );
        }
    }

    public flushTween( tween: gsap.core.Tween ): void
    {
        if ( tween.totalProgress() == 0 )
        {
            if ( tween.eventCallback( "onStart" ) )
            {
                if ( tween.vars.onStartParams )
                {
                    tween.eventCallback( "onStart" ).call( tween.vars.callbackScope, ...tween.vars.onStartParams );
                }
                else
                {
                    tween.eventCallback( "onStart" ).call( tween.vars.callbackScope );
                }
            }
            if ( tween.eventCallback( "onRepeat" ) )
            {
                if ( tween.vars.onRepeatParams )
                {
                    tween.eventCallback( "onRepeat" ).call( tween.vars.callbackScope, ...tween.vars.onRepeatParams );
                }
                else
                {
                    tween.eventCallback( "onRepeat" ).call( tween.vars.callbackScope );
                }
            }
            if ( tween.eventCallback( "onComplete" ) )
            {
                if ( tween.vars.onCompleteParams )
                {
                    tween.eventCallback( "onComplete" ).call( tween.vars.callbackScope, ...tween.vars.onCompleteParams );
                }
                else
                {
                    tween.eventCallback( "onComplete" ).call( tween.vars.callbackScope );
                }
            }
            tween.totalProgress( 1, true );
        }
        else
        {
            tween.totalProgress( 1, false );
        }
        tween.kill();
    }

    public bump( container: PIXI.Container, onRepeat: ( ...args: any[] ) => void = null ): gsap.core.Tween
    {
        let result: gsap.core.Tween = null;
        
        this.flushTweensOf( container.scale );

        result = gsap.to( container.scale, { x: "+=0.15", y: "+=0.15", duration: 0.15, yoyo: true, repeat: 1, onRepeat: onRepeat } );
    
        return result;
    }

    public drop( container: PIXI.Container ): gsap.core.Tween
    {
        let result: gsap.core.Tween = null;
        
        this.flushTweensOf( container.scale );

        const kScaleFromDelta: number = 0.25;
        result = TweenMax.from( container.scale, 0.25, { x: "+=" + kScaleFromDelta.toString(), y: "+=" + kScaleFromDelta.toString(), ease: Sine.easeOut } );
    
        return result;
    }

    public bumpFromCorner( container: PIXI.Container, onRepeat: ( ...args: any[] ) => void = null ): gsap.core.Tween
    {
        let result: gsap.core.Tween = null;
        
        this.flushTweensOf( container );
        this.flushTweensOf( container.scale );

        result = TweenMax.to( container.scale, 0.15, { x: "+=0.15", y: "+=0.15", yoyo: true, repeat: 1, onRepeat: onRepeat } );
        const kScaleTranslationX: number = container.width * 0.15 * 0.5;
        const kScaleTranslationY: number = container.height * 0.15 * 0.5;
        TweenMax.to( container, 0.15, { x: "-=" + kScaleTranslationX.toString(), y: "-=" + kScaleTranslationY.toString(), yoyo: true, repeat: 1 } );
    
        return result;
    }

    public dropFromCorner( container: PIXI.Container ): gsap.core.Tween
    {
        let result: gsap.core.Tween = null;
        
        this.flushTweensOf( container.scale );

        const kScaleFromDelta: number = 0.25;
        result = TweenMax.from( container.scale, 0.25, { x: "+=" + kScaleFromDelta.toString(), y: "+=" + kScaleFromDelta.toString(), ease: Sine.easeOut } );
        const kScaleTranslationX: number = container.width * kScaleFromDelta * 0.5;
        const kScaleTranslationY: number = container.height * kScaleFromDelta * 0.5;
        TweenMax.from( container, 0.25, { x: "-=" + kScaleTranslationX.toString(), y: "-=" + kScaleTranslationY.toString(), ease: Sine.easeOut } );
    
        return result;
    }

    public flash( position: PIXI.Point, width: number, height: number, scale: number, layer: GameObject = null ): void
    {
        if ( !layer )
        {
            layer = ServiceLocator.game.root.cGameLayerProvider.get( layer_type.VFX );
        }

        const kHalfWidth: number = width * 0.5;
        const kHalfHeight: number = height * 0.5;
        let mask: GameObject = new GameObject( [ new CGraphics() ] );
        mask.cGraphics.g.beginFill();
        mask.cGraphics.g.drawRoundedRect( -kHalfWidth * scale, -kHalfHeight * scale, width * scale, height * scale, 2 );
        mask.cGraphics.g.endFill();
        mask.cGraphics.g.beginHole();
        mask.cGraphics.g.drawRoundedRect( -kHalfWidth, -kHalfHeight, width, height, 2 );
        mask.cGraphics.g.endHole();
        mask.cGraphics.g.renderable = false;
        mask.init();
        mask.cContainer.c.position.copyFrom( position );
        layer.cContainer.addChild( mask );

        let flash: GameObject = new GameObject( [ new CGraphics() ] );
        flash.cGraphics.g.beginFill( 0xffffff );
        flash.cGraphics.g.drawRoundedRect( -kHalfWidth, -kHalfHeight, width, height, 2 );
        flash.cGraphics.g.endFill();
        flash.cGraphics.g.mask = mask.cGraphics.g;
        flash.init();
        flash.cContainer.c.position.copyFrom( position );
        layer.cContainer.addChild( flash );

        TweenMax.to( flash.cGraphics.g.scale, 1, { x: scale, y: scale, ease: Circ.easeOut } );
        TweenMax.to( flash.cGraphics.g, 1, { alpha: 0, ease: Sine.easeOut } )
            .then( () => { flash.end(); mask.end(); } );
    }

    public flashCircle( position: PIXI.Point, radius: number, scale: number, layer: GameObject = null ): void
    {
        if ( !layer )
        {
            layer = ServiceLocator.game.root.cGameLayerProvider.get( layer_type.VFX );
        }

        let mask: GameObject = new GameObject( [ new CGraphics() ] );
        mask.cGraphics.g.beginFill();
        mask.cGraphics.g.drawCircle( 0, 0, radius * scale );
        mask.cGraphics.g.endFill();
        mask.cGraphics.g.beginHole();
        mask.cGraphics.g.drawCircle( 0, 0, radius );
        mask.cGraphics.g.endHole();
        mask.cGraphics.g.renderable = false;
        mask.init();
        mask.cContainer.c.position.copyFrom( position );
        layer.cContainer.addChild( mask );

        let flash: GameObject = new GameObject( [ new CGraphics() ] );
        flash.cGraphics.g.beginFill( 0xffffff );
        flash.cGraphics.g.drawCircle( 0, 0, radius );
        flash.cGraphics.g.endFill();
        flash.cGraphics.g.mask = mask.cGraphics.g;
        flash.init();
        flash.cContainer.c.position.copyFrom( position );
        layer.cContainer.addChild( flash );

        TweenMax.to( flash.cGraphics.g.scale, 1, { x: scale, y: scale, ease: Circ.easeOut } );
        TweenMax.to( flash.cGraphics.g, 1, { alpha: 0, ease: Sine.easeOut } )
            .then( () => { flash.end(); mask.end(); } );
    }

    public flashDiamond( position: PIXI.Point, width: number, scale: number, layer: GameObject = null ): void
    {
        if ( !layer )
        {
            layer = ServiceLocator.game.root.cGameLayerProvider.get( layer_type.VFX );
        }

        const kHalfWidth: number = width * 0.5;
        let mask: GameObject = new GameObject( [ new CGraphics() ] );
        mask.cGraphics.g.beginFill();
        mask.cGraphics.g.moveTo( -kHalfWidth * scale, 0 );
        mask.cGraphics.g.lineTo( 0, -kHalfWidth * scale );
        mask.cGraphics.g.lineTo( kHalfWidth * scale, 0 );
        mask.cGraphics.g.lineTo( 0, kHalfWidth * scale );
        mask.cGraphics.g.closePath();
        mask.cGraphics.g.endFill();
        mask.cGraphics.g.beginHole();
        mask.cGraphics.g.moveTo( -kHalfWidth, 0 );
        mask.cGraphics.g.lineTo( 0, -kHalfWidth );
        mask.cGraphics.g.lineTo( kHalfWidth, 0 );
        mask.cGraphics.g.lineTo( 0, kHalfWidth );
        mask.cGraphics.g.endHole();
        mask.cGraphics.g.renderable = false;
        mask.init();
        mask.cContainer.c.position.copyFrom( position );
        layer.cContainer.addChild( mask );

        let flash: GameObject = new GameObject( [ new CGraphics() ] );
        flash.cGraphics.g.beginFill( 0xffffff );
        flash.cGraphics.g.moveTo( -kHalfWidth, 0 );
        flash.cGraphics.g.lineTo( 0, -kHalfWidth );
        flash.cGraphics.g.lineTo( kHalfWidth, 0 );
        flash.cGraphics.g.lineTo( 0, kHalfWidth );
        flash.cGraphics.g.endFill();
        flash.cGraphics.g.mask = mask.cGraphics.g;
        flash.init();
        flash.cContainer.c.position.copyFrom( position );
        layer.cContainer.addChild( flash );

        TweenMax.to( flash.cGraphics.g.scale, 1, { x: scale, y: scale, ease: Circ.easeOut } );
        TweenMax.to( flash.cGraphics.g, 1, { alpha: 0, ease: Sine.easeOut } )
            .then( () => { flash.end(); mask.end(); } );
    }

    // #endregion //
}