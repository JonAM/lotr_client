import Config from "../Config";
import InfoMessageView from "../view/common/popup/InfoMessageView";
import ServiceLocator from "../ServiceLocator";
import { view_layer_id } from "../service/ViewManager";


export default class WebUtils
{
    // #region Methods //

    // public:
    
    /**
     * Converts a href string taken from a link HTML element to an id.
     * 
     * @param href 
     */
    public hrefToId( href: string ): string
    {
        let result: string = href.substring( 0, href.lastIndexOf( "." ) );

        result = result.substring( result.indexOf( "resources/css/" ) );
        result = result.split( "/" ).join( "." );

        return result;
    }

    public findLocale(): string
    {
        let result: string = window.localStorage.getItem( "tpp_locale" );
        if ( !result )
        {
            result = "en_US"; // default value.

            let clientLocale: string = window.navigator.language;
            const kDashIndex: number = clientLocale.indexOf( "-" );
            if ( kDashIndex != -1 )
            {
                clientLocale = clientLocale.substring( 0, kDashIndex ).toLowerCase() + "_" + clientLocale.substring( kDashIndex + 1 ).toUpperCase(); 
            }

            if ( Config.kArrAvailableLocale.indexOf( clientLocale ) != -1 )
            {
                result = clientLocale;
            }
            else
            {
                const kClientLocaleAux: string = clientLocale.substr( 0, 2 );
                for ( let locale of Config.kArrAvailableLocale )
                {
                    if ( locale.substr( 0, 2 ) == kClientLocaleAux )
                    {
                        result = locale;
                        break;
                    }
                }
            }

            window.localStorage.setItem( "tpp_locale", result );
        }

        return result;
    }

    public findRelativePosition( element: HTMLElement, left: number, top: number, parentElement: HTMLElement ): ICssPosition
    {
        while ( element != parentElement )
        {
            top += element.offsetTop;
            left += element.offsetLeft;

            element = element.offsetParent as HTMLElement;
        }

        return { top: top, left: left };
    }

    public createLoadingAnimHtml(): string
    {
        return "<div class=\"gb-loading-anim-bg\">"
                    + "<div class=\"container gb-centered\">"
                    + "<div id=\"gbLoadingAnimationIcon\" class=\"icon\"></div>"
                    + "<div class=\"message gb-text-header-normal gb-text-shadow\">" + jQuery.i18n( "LOADING" ) + "</div>"
                + "</div>";
    }

    public showInfoMessage( message: string, viewLayerId: view_layer_id ): InfoMessageView
    {
        let infoMessageView: InfoMessageView = new InfoMessageView();
        infoMessageView.message = message;
        infoMessageView.init();
        ServiceLocator.viewManager.fadeIn( infoMessageView, viewLayerId );

        return infoMessageView;
    }

    public createElapsedTimeStr( timestamp: number ): string
    {
        let result: string = null;

        let remanent: number = Math.floor( timestamp / 60 );
        if ( remanent <= 0 )
        {
            result = timestamp.toString() + jQuery.i18n( "SECONDS_SHORTENED" );
        }

        let base: number = null;
        if ( !result )
        {
            base = remanent;
            remanent = Math.floor( remanent / 60 );
            if ( remanent <= 0 )
            {
                result = base.toString() + jQuery.i18n( "MINUTES_SHORTENED" );
            }
        }

        if ( !result )
        {
            base = remanent;
            remanent = Math.floor( remanent / 24 );
            if ( remanent <= 0 )
            {
                result = base.toString() + jQuery.i18n( "HOURS_SHORTENED" );
            }
        }

        if ( !result )
        {
            base = remanent;
            remanent = Math.floor( remanent / 7 );
            if ( remanent <= 0 )
            {
                result = base.toString() + jQuery.i18n( "DAYS_SHORTENED" );
            }
        }

        if ( !result )
        {
            base = remanent;
            remanent = Math.floor( remanent / 4 );
            if ( remanent <= 0 )
            {
                result = base.toString() + jQuery.i18n( "WEEKS_SHORTENED" );
            }
        }

        if ( !result )
        {
            base = remanent;
            remanent = Math.floor( remanent / 12 );
            if ( remanent <= 0 )
            {
                result = base.toString() + jQuery.i18n( "MONTHS_SHORTENED" );
            }
        }

        if ( !result )
        {
            result = remanent.toString() + jQuery.i18n( "YEARS_SHORTENED" );
        }

        return result;
    }

    public shuffle( array: any ): void
    {
        for ( let i: number = array.length - 1; i > 0; --i )
        {
            const j: number = Math.floor( Math.random() * i );
            const temp: any = array[ i ];
            array[ i ] = array[ j ];
            array[ j ] = temp;
        }
    }

    // #endregion //
}

export interface ICssPosition
{
    top: number;
    left: number;
}