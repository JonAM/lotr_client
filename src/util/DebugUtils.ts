import ServiceLocator from "../ServiceLocator";
import * as PIXI from "pixi.js";
import GameObject from "../game/GameObject";
import CGraphics from "../game/component/pixi/CGraphics";
import { layer_type } from "../game/component/world/CGameLayerProvider";


export default class DebugUtils
{
    // #region Methods //

    // public:

    public drawPoint( position: PIXI.IPoint ): void
    {
        let touch: GameObject = new GameObject( [ new CGraphics() ] );
        touch.cContainer.c.position.set( position.x, position.y );
        touch.cGraphics.g.beginFill( 0xff0000 );
        touch.cGraphics.g.drawCircle( 0, 0, 10 );
        touch.cGraphics.g.endFill();
        touch.init();
        ServiceLocator.game.root.cGameLayerProvider.add( touch, layer_type.DEBUG );
    }

    public drawBoundingBox( displayObject: PIXI.DisplayObject ): void
    {
        let touch: GameObject = new GameObject( [ new CGraphics() ] );
        touch.cGraphics.g.beginFill( 0x0000ff, 0.5 );
        const kBoundingBox: PIXI.Rectangle = displayObject.getBounds( false );
        touch.cGraphics.g.drawShape( kBoundingBox );
        touch.cGraphics.g.endFill();
        touch.init();
        ServiceLocator.game.root.cGameLayerProvider.add( touch, layer_type.DEBUG );
    }

    // #endregion //
}