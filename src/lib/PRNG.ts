/**
 * Creates a pseudo-random value generator. The seed must be an integer.
 *
 * Uses an optimized version of the Park-Miller PRNG.
 * http://www.firstpr.com.au/dsp/rand31/
 */
export default class PRNG
{
    private _seed: number = 0;
    private _procSeed: number = 0;

    public get seed(): number { return this._seed; }

    public constructor( seed: number )
    {
        //Utils.log( "PRNG :: PRNG.ts :: constructor() :: seed = " + seed.toString() );

        this._seed = seed;
        this._procSeed = seed % 2147483647;
        if ( this._procSeed <= 0 ) this._procSeed += 2147483646;

        //Utils.log( "PRNG :: PRNG.ts :: constructor() :: this._procSeed = " + this._procSeed.toString() );
    }

    /**
     * Return a pseudo-random value between 1 and 2^32 - 2.
     */
    public next() 
    {
        this._procSeed = this._procSeed * 16807 % 2147483647

        // Utils.log( "PRNG :: PRNG.ts :: next() :: result = " + this._procSeed.toString() );

        return this._procSeed;
    }

    /**
     * Return a pseudo-random value between from and to (both included).
     * 
     * @param from 
     * @param to 
     */
    public randInterval( from: number, to: number ): number
    {
        console.assert( from >= 0 && to >= 0, "PRNG.ts :: randInterval() :: from and to cannot be negative values." );

        let result: number = from;

        let d: number = to - from;
        if ( d > 0 )
        {
           result = this.next() % ( d + 1 ) + from;
        }

        // Utils.log( "PRNG :: PRNG.ts :: randInterval() :: result = " + result.toString() );

        return result;
    }
}