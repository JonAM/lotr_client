import Config from "../Config";
import Path from "../Path";
import ServiceLocator from "../ServiceLocator";
import Utils from "../Utils";
import * as PIXI from "pixi.js";

import Signal from "./signals/Signal";
import ResourceLoader from "../service/ResourceLoader";


export default class HtmlFileLoader
{
    // #region Attributes //

    // private:

    private _arrHtmlFileInfo: Array<IFileLoadInfo> = null; // list of HTML files required in this state.
    
    private _resourceLoader: ResourceLoader = new ResourceLoader(); // IMPORTANT: we can't use the global loader, since more than one view may use it at the same time (ie. in composed views).

    // Signals.
    private _onCompleted: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public set htmlFileInfoList( value: Array<IFileLoadInfo> ) { this._arrHtmlFileInfo = value; }  
    
    // Signals.
    public get onCompleted(): Signal { return this._onCompleted; }

    // #endregion //


    // #region Methods //

    // public:

    public init(): void {}

    public end(): void
    {
        this._resourceLoader.reset();
        this._resourceLoader = null;

        // Owned signals.
        this._onCompleted.removeAll();
        this._onCompleted = null;
    }

    public load(): void
    {
        // Load HTML files.
        for ( let htmlFileInfo of this._arrHtmlFileInfo )
        {
            let htmlResource: PIXI.LoaderResource = ServiceLocator.resourceStack.find( htmlFileInfo.id );
            if ( !htmlResource )
            {
                this._resourceLoader.add( htmlFileInfo.id, Path.kResHtml + htmlFileInfo.id.replace( /-/g, "/" ) + ".html", { xhrType: "text" } );
            }
        }
        this._resourceLoader.load( this.onHtmlFiles_Loaded.bind( this ) );
    }

    // private:

    private findDependentHtmlAssets( htmlResource: PIXI.LoaderResource ): Array<string>
    {
        let result: Array<string> = new Array<string>();
        
        // Look in the HTML text for referenced assets and extract the url value.
        const kImgUrlRegex: RegExp = Config.kIsDebugMode ? /src="([^"]+)"/g : /src="([^"]+)\?ws=\d\.\d{2}\.\d{3}/g;
        let arrExec: RegExpExecArray = null;
        do
        {
            arrExec = kImgUrlRegex.exec( htmlResource.data );
            if  ( arrExec != null )
            {
                let assetPath: string = arrExec[ 1 ];
                if ( result.indexOf( assetPath ) == -1 )
                {
                    result.push( assetPath );
                }
            }
        }
        while ( arrExec != null);

        return result;
    }

    private findDependentCssFiles( htlmlResource: PIXI.LoaderResource ): Array<string>
    {
        let result: Array<string> = new Array<string>();

        const kHtmlLinkRegex: RegExp = /<link[^>]+href=\"([\w|\/]+\.css)\">/g;

        // Look in the HTML text for <link> elements and extract the href value.
        let arrExec: RegExpExecArray = null;
        do
        {
            arrExec = kHtmlLinkRegex.exec( htlmlResource.data );
            if  ( arrExec != null )
            {
                let cssFilePath: string = arrExec[ 1 ];
                if ( result.indexOf( cssFilePath ) == -1 )
                {
                    result.push( cssFilePath );
                }
            }
        }
        while ( arrExec != null);

        return result;
    }

    private findDependentCssAssets( cssResource: PIXI.LoaderResource ): Array<string>
    {
        let result: Array<string> = new Array<string>();

        // Look in the CSS text for referenced assets and extract the url value.
        const kCssAssetRegex: RegExp = Config.kIsDebugMode ? /url\("(\/.+)"\)/g : /url\((\/[^\)]+)\?ws=\d\.\d{2}\.\d{3}\)/g;
        let arrExec: RegExpExecArray = null;
        do
        {
            arrExec = kCssAssetRegex.exec( cssResource.data );
            if  ( arrExec != null )
            {
                let assetPath: string = arrExec[ 1 ];
                if ( result.indexOf( assetPath ) == -1 )
                {
                    result.push( assetPath );
                }
            }
        }
        while ( arrExec != null);

        return result;
    }

    // #endregion //


    // #region Callbacks //

    private onHtmlFiles_Loaded( resources: PIXI.IResourceDictionary ): void
    {
        let arrCssResId: Array<string> = new Array<string>();

        for ( let htmlFileInfo of this._arrHtmlFileInfo )
        {
            const kHtmlResource: PIXI.LoaderResource = ServiceLocator.resourceStack.find( htmlFileInfo.id );

            // Load HTML images.
            if ( htmlFileInfo.isPreloadImages )
            {
                const kArrImgUrl: Array<string> = this.findDependentHtmlAssets( kHtmlResource );
                for ( let imgUrl of kArrImgUrl )
                {
                    this._resourceLoader.addOnce( Utils.web.hrefToId( imgUrl ), imgUrl );
                }
            }

            // Load CSS files.
            const kArrCssUrl: Array<string> = this.findDependentCssFiles( kHtmlResource );
            for ( let cssUrl of kArrCssUrl )
            {
                const kCssResId: string = Utils.web.hrefToId( cssUrl );
                let cssResource: PIXI.LoaderResource = ServiceLocator.resourceStack.find( kCssResId );
                if ( !cssResource )
                {
                    this._resourceLoader.add( kCssResId, cssUrl, { xhrType: "text" } );
                }

                if ( htmlFileInfo.isPreloadImages )
                {
                    arrCssResId.push( kCssResId );
                }
            }
        }

        this._resourceLoader.load( this.onCssFiles_Loaded.bind( this, arrCssResId ) );
    }

    private onCssFiles_Loaded( arrCssResId: Array<string>, resources: PIXI.IResourceDictionary ): void
    {
        for ( let cssResId of arrCssResId )
        {
            // Load CSS images.
            const kCssResource: PIXI.LoaderResource = ServiceLocator.resourceStack.find( cssResId );
            const kArrImgUrl: Array<string> = this.findDependentCssAssets( kCssResource );
            for ( let imgUrl of kArrImgUrl )
            {
                this._resourceLoader.addOnce( Utils.web.hrefToId( imgUrl ), imgUrl );
            }
        }

        this._resourceLoader.load( () => { this._onCompleted.dispatch(); } );
    }

    // #endregion //
}

export interface IFileLoadInfo
{
    id: string;
    isPreloadImages: boolean;
}