import Signal from "./signals/Signal";

export default class Timer
{
    // #region Attributes //

    // private:

    private _delay: number = 0; // ms.
    private _currentCount: number = 0;
    private _repeatCount: number = 0;

    private _intervalHandle: number = null;

    // Signals.
    private _onTimer: Signal = new Signal();
    private _onTimerComplete: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get currentCount(): number { return this._currentCount; }
    public get isRunning(): boolean { return this._intervalHandle != null; }

    public set delay( value: number ) { this._delay = value; }
    public set repeatCount( value: number ) 
    { 
        this._repeatCount = value; 

        if ( this._repeatCount > 0 && this._repeatCount <= this._currentCount )
        {
            this.stop();
        }
    }

    // Signals.
    public get onTimer(): Signal { return this._onTimer; }
    public get onTimerComplete(): Signal { return this._onTimerComplete; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor( delay: number, repeatCount: number = 0 )
    {
        this._delay = delay;
        this._repeatCount = repeatCount;
    }

    public start(): void
    {
        if ( !this._intervalHandle )
        {
            this._intervalHandle = window.setInterval( this.onInterval_Completed.bind( this ), this._delay );
        }
    }

    public stop(): void
    {
        clearInterval( this._intervalHandle );
        
        this._intervalHandle = null;
    }

    public reset(): void
    {
        this._currentCount = 0;

        this.stop();
    }

    // #endregion //


    // #region Callbacks //

    private onInterval_Completed(): void
    {
        this._onTimer.dispatch();

        this._currentCount += 1;
        if ( this._repeatCount > 0 && this._currentCount >= this._repeatCount )
        {
            this.stop();

            this._onTimerComplete.dispatch();
        }
    }

    // #endregion //
}