export default abstract class State
{
    // #region Attributes //

    // protected:
    protected _isCurrent: boolean = false;

    // #endregion //


    // #region Properties //

    public set isCurrent( value: boolean ) { this.isCurrent = value; }

    // #endregion //


    // #region Methods //

    // public:

    // virtual.
    public init(): void {}
    public end(): void {};

    // virtual.
    public onEnter(): void { this._isCurrent = true; }
    public onLeave(): void { this._isCurrent = false; }
    public onUpdate( dt: number ): void {}
    public onPushDown(): void {};
    public onPopUp(): void {};

    // virtual.
    public saveGame(): any { return null; }
    public onLoad( sgState: any, pass: number ): void { this._isCurrent = true; }

    // #endregion //
}