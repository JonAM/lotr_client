import * as PIXI from "pixi.js";
import GameObject from "../game/GameObject";


export default class SpacePartitioner
{
    // #region Attributes //

    // private:

    private _width: number = null;
    private _height: number = null;
    private _hDivCount: number = null;
    private _vDivCount: number = null;

    private _partitionWidth: number = null;
    private _partitionHeight: number = null;

    private _space: Array<Array<Array<ITaggedGO>>> = null;

    // #endregion //


    // #region Properties //

    public set width( value: number ) { this._width = value; }
    public set height( value: number ) { this._height = value; }
    public set hDivCount( value: number ) { this._hDivCount = value; }
    public set vDivCount( value: number ) { this._vDivCount = value; }

    // #endregion //


    // #region Methods //

    public init(): void
    {
        console.assert( this._width != null, "SpacePartitioner.ts :: init() :: this._width cannot be null." );
        console.assert( this._height != null, "SpacePartitioner.ts :: init() :: this._height cannot be null." );
        console.assert( this._hDivCount != null, "SpacePartitioner.ts :: init() :: this._hDivCount cannot be null." );
        console.assert( this._vDivCount != null, "SpacePartitioner.ts :: init() :: this._vDivCount cannot be null." );
    
        this._partitionWidth = this._width / this._hDivCount;
        this._partitionHeight = this._height / this._vDivCount;

        this._space = Array<Array<Array<ITaggedGO>>>();
        for ( let i: number = 0; i < this._vDivCount; ++i )
        {
            let hDiv: Array<Array<ITaggedGO>> = new Array<Array<ITaggedGO>>();
            for ( let j: number = 0; j < this._hDivCount; ++j )
            {
                hDiv.push( new Array<ITaggedGO>() );
            }
            this._space.push( hDiv );
        }
    }

    public end(): void
    {
        this._width = null;
        this._height = null;
        this._hDivCount = null;
        this._vDivCount = null;

        this._space = null;
    }

    public add( gameObject: GameObject, tags: Array<string> = null ): void
    {
        const kBounds: PIXI.Rectangle = gameObject.cContainer.c.getBounds();
        const kArrTopLeftIndex: Array<number> = [ 
            Math.floor( kBounds.y / this._partitionHeight ), 
            Math.floor( kBounds.x / this._partitionWidth ) ]; 
        const kArrBotRightIndex: Array<number> = [ 
            Math.floor( ( kBounds.y + kBounds.height - 1 ) / this._partitionHeight ), 
            Math.floor( ( kBounds.x + kBounds.width - 1 ) / this._partitionWidth ) ]; 

        for ( let i: number = kArrTopLeftIndex[ 0 ]; i <= kArrBotRightIndex[ 0 ]; ++i )
        {
            for ( let j: number = kArrTopLeftIndex[ 1 ]; j <= kArrBotRightIndex[ 1 ]; ++j )
            {
                this.createTaggedElement( gameObject, tags, [ i, j ] );
            }
        }
    }

    public remove( gameObject: GameObject ): void
    {
        for ( let i: number = 0; i < this._space.length; ++i )
        {
            for ( let j: number = 0; j < this._space[ i ].length; ++j )
            {
                let partition: Array<ITaggedGO> = this._space[ i ][ j ];
                for ( let k: number = partition.length - 1; k >= 0; --k )
                {
                    if ( partition[ k ].gameObject == gameObject )
                    {
                        partition.splice( k, 1 );
                        break;
                    }
                }
            }
        }
    }

    public raycast( point: PIXI.Point, tag: string = null ): Array<GameObject>
    {
        let result: Array<GameObject> = new Array<GameObject>();

        if ( point.x >= 0 && point.x <= this._width
            && point.y >= 0 && point.y <= this._height )
        {
            const kArrPartitionIndex: Array<number> = [ 
                Math.floor( point.y / this._partitionHeight ), 
                Math.floor( point.x / this._partitionWidth ) ]; 
            
            for ( let taggedElement of this._space[ kArrPartitionIndex[ 0 ] ][ kArrPartitionIndex[ 1 ] ] )
            {
                const kBounds: PIXI.Rectangle = taggedElement.gameObject.cContainer.c.getBounds();
                if ( ( !tag || taggedElement.tags.indexOf( tag ) >= 0 )
                    && point.x >= kBounds.x && point.x <= kBounds.x + kBounds.width - 1
                    && point.y >= kBounds.y && point.y <= kBounds.y + kBounds.height - 1 )
                {
                    result.push( taggedElement.gameObject );
                }
            }
        }

        return result;
    }

    // private:

    private createTaggedElement( gameObject: GameObject, tags: Array<string>, arrPartitionIndex: Array<number> ): void
    {
        let isRepeated: boolean = false;
        let partition: Array<ITaggedGO> = this._space[ arrPartitionIndex[ 0 ] ][ arrPartitionIndex[ 1 ] ];
        for ( let taggedElement of partition )
        {
            if ( taggedElement.gameObject == gameObject )
            {
                isRepeated = true;
                break;
            }
        }

        if ( !isRepeated )
        {
            partition.push( { gameObject: gameObject, tags: tags } );
        }
    }

    // #endregion //
}

interface ITaggedGO
{
    tags: Array<string>;
    gameObject: GameObject;
}