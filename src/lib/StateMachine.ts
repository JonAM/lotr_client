import State from "./state_machine/State";
import Signal from "./signals/Signal";


export default class StateMachine
{
    // #region Attributes //

    // private:

    private _mapState: Map<number, State> = new Map<number, State>();
    private _currentState: State = null;
    private _currentStateId: number = null;
    private _stateStack: Array<State> = null;
    private _isPaused: boolean = false;

    // Signals.
    private _onStateEntered: Signal = new Signal();
    private _onStatePushed: Signal = new Signal();
    private _onStatePoped: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get currentState(): State { return this._currentState; }
    public get currentStateId(): number { return this._currentStateId; }
    public get states(): Map<number, State> { return this._mapState; }

    // Signals.
    public get onStateEntered(): Signal { return this._onStateEntered; }
    public get onStatePushed(): Signal { return this._onStatePushed; }
    public get onStatePoped(): Signal { return this._onStatePoped; }

    // #endregion //


    // #region Methods //

    // public:

    public init(): void
    {
        this._mapState.forEach( ( value: State, key: number ) => { value.init(); } );

        this._stateStack = new Array<State>();
    }

    public end(): void
    {
        this._onStateEntered.removeAll();
        this._onStatePushed.removeAll();
        this.onStatePoped.removeAll();

        if ( this._currentState != null )
        {
            this._currentState.onLeave();

            this._currentState = null;
            this._currentStateId = null;
        }

        this._mapState.forEach( ( value: State, key: number ) => { value.end(); } );

        this._stateStack = null;
    }

    public isInStack( stateId: number ): boolean
    {
        let result: boolean = false;

        for ( let state of this._stateStack )
        {
            if ( this.findStateId( state ) == stateId )
            {
                result = true;
                break;
            }
        }

        return result;
    }

    /**
     * Adds a new state to the state machine with the given state id, so it can be later requested.
     * 
     * @param stateId The state id bound to this state.
     * @param state The state.
     */
    public add( stateId: number, state: State ): void
    {
        this._mapState.set( stateId, state );
    }

    /**
     * Change from the current state to defined target state.
     * 
     * @param stateId Target state id.
     */
    public request( stateId: number ): void
    {
        if ( this._currentState != null )
        {
            this._currentState.onLeave();
        }

        this._currentStateId = stateId;
        this._currentState = this._mapState.get( stateId );

        if ( this._stateStack.length == 0 )
        {
            this._stateStack.unshift( this._currentState );
        }
        else
        {
            this._stateStack[ 0 ] = this._currentState;
        }

        // Retrieve initialization params (if any).
        this._onStateEntered.dispatch( this._currentState, stateId );

        this._currentState.onEnter();
    }

    public load( stateId: number, sgState: any, pass: number ): void
    {
        console.assert( this._currentState == null, "StateMachine.ts :: load() :: this._currentState cannot be set." );
    
        this._currentStateId = stateId;
        this._currentState = this._mapState.get( stateId );

        if ( this._stateStack.length == 0 )
        {
            this._stateStack.unshift( this._currentState );
        }
        else
        {
            this._stateStack[ 0 ] = this._currentState;
        }

        this._currentState.onLoad( sgState, pass );
    }

    public push( stateId: number ): void
    {
        this._onStatePushed.dispatch();

        if ( this._currentState != null )
        {
            this._currentState.onPushDown();
        }

        this._currentStateId = stateId;
        this._currentState = this._mapState.get( stateId );

        this._stateStack.unshift( this._currentState );

        // Retrieve initialization params (if any).
        this._onStateEntered.dispatch( this._currentState, stateId );

        this._currentState.onEnter();
    }

    public pop(): void
    {
        this._onStatePoped.dispatch();

        this._stateStack.shift().onLeave();

        this._currentState = this._stateStack[ 0 ];
        this._currentStateId = this.findStateId( this._currentState );

        this._currentState.onPopUp();
    }

    public clear(): void
    {
        while ( this._stateStack.length > 0 )
        {
            this._stateStack.shift().onLeave();
        }

        this._currentState = null;
        this._currentStateId = null;
    }

    public pause(): void 
    { 
        this._isPaused = true; 
    }

    public resume(): void 
    { 
        this._isPaused = false; 
    }

    public onUpdate( dt: number ): void
    {
        if ( this._currentState != null && !this._isPaused )
        {
            this._currentState.onUpdate( dt );
        }
    }

    // private:

    private findStateId( state: State ): number
    {
        let result: number = null;

        let entriesIt: IterableIterator<[number, State]> = this._mapState.entries();
        for ( let entriesItResult: IteratorResult<[number, State]> = entriesIt.next(); !entriesItResult.done; entriesItResult = entriesIt.next() )
        {
            if ( entriesItResult.value[ 1 ] == state )
            {
                result = entriesItResult.value[ 0 ];
                break;
            }
        }

        return result;
    }

    // #endregion //
}