import Config from "../Config";
import ServiceLocator from "../ServiceLocator";
import Utils from "../Utils";


export default class WebAssetLoader
{
    public load( arrViewResId: Array<string>, assetsLoadedCb: () => void ): void
    {
        let arrAssetUrl: Array<string> = null;

        for ( let viewResIs of arrViewResId )
        {
            let htmlResource: PIXI.LoaderResource = ServiceLocator.resourceStack.find( viewResIs );
            
            // Load dependent img elements.
            let arrAssetUrlAux: Array<string> = this.findDependentHtmlAssets( htmlResource );
            for ( let assetUrlAux of arrAssetUrlAux )
            {
                if ( arrAssetUrl.indexOf( assetUrlAux ) == -1 )
                {
                    arrAssetUrl.push( assetUrlAux );
                    ServiceLocator.resourceLoader.addOnce( Utils.web.hrefToId( assetUrlAux ), assetUrlAux );
                }
            }

            let arrCssUrl: Array<string> = this.findDependentCssFiles( htmlResource );
            if ( arrCssUrl.length > 0 )
            {
                for ( let cssUrl of arrCssUrl )
                {
                    const kCssResource: PIXI.LoaderResource = ServiceLocator.resourceStack.find( Utils.web.hrefToId( cssUrl ) );
                    arrAssetUrlAux = this.findDependentCssAssets( kCssResource );
                    for ( let assetUrlAux of arrAssetUrlAux )
                    {
                        if ( arrAssetUrl.indexOf( assetUrlAux ) == -1 )
                        {
                            arrAssetUrl.push( assetUrlAux );
                            ServiceLocator.resourceLoader.addOnce( Utils.web.hrefToId( assetUrlAux ), assetUrlAux );
                        }
                    }
                }
            }
        }

        if ( arrAssetUrl.length > 0 )
        {
            ServiceLocator.resourceLoader.load( assetsLoadedCb );
        }
        else
        {
            assetsLoadedCb();
        }
    }

    // private:

    private findDependentHtmlAssets( htmlResource: PIXI.LoaderResource ): Array<string>
    {
        let result: Array<string> = new Array<string>();
        
        // Look in the HTML text for referenced assets and extract the url value.
        const kCssAssetRegex: RegExp = Config.kIsDebugMode ? /src="([^"]+)"/g : /src="([^"]+)\?ws=\d\.\d{2}\.\d{3}/g;
        let arrExec: RegExpExecArray = null;
        do
        {
            arrExec = kCssAssetRegex.exec( htmlResource.data );
            if  ( arrExec != null )
            {
                let assetPath: string = arrExec[ 1 ];
                if ( result.indexOf( assetPath ) == -1 )
                {
                    result.push( assetPath );
                }
            }
        }
        while ( arrExec != null);

        return result;
    }

    private findDependentCssFiles( htlmlResource: PIXI.LoaderResource ): Array<string>
    {
        let result: Array<string> = new Array<string>();

        const kHtmlLinkRegex: RegExp = /<link[^>]+href=\"([\w|\/]+\.css)\">/g;

        // Look in the HTML text for <link> elements and extract the href value.
        let arrExec: RegExpExecArray = null;
        do
        {
            arrExec = kHtmlLinkRegex.exec( htlmlResource.data );
            if  ( arrExec != null )
            {
                let cssFilePath: string = arrExec[ 1 ];
                if ( result.indexOf( cssFilePath ) == -1 )
                {
                    result.push( cssFilePath );
                }
            }
        }
        while ( arrExec != null);

        return result;
    }

    private findDependentCssAssets( cssResource: PIXI.LoaderResource ): Array<string>
    {
        let result: Array<string> = new Array<string>();

        // Look in the CSS text for referenced assets and extract the url value.
        const kCssAssetRegex: RegExp = Config.kIsDebugMode ? /url\("(\/.+)"\)/g : /url\((\/[^\)]+)\?ws=\d\.\d{2}\.\d{3}\)/g;
        let arrExec: RegExpExecArray = null;
        do
        {
            arrExec = kCssAssetRegex.exec( cssResource.data );
            if  ( arrExec != null )
            {
                let assetPath: string = arrExec[ 1 ];
                if ( result.indexOf( assetPath ) == -1 )
                {
                    result.push( assetPath );
                }
            }
        }
        while ( arrExec != null);

        return result;
    }

    // #endregion //
}