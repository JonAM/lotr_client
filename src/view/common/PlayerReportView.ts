import View from "../View";

import ServiceLocator from "../../ServiceLocator";
import Utils from "../../Utils";
import { view_layer_id } from "../../service/ViewManager";
import Session from "../../Session";
import InfoMessageView from "./popup/InfoMessageView";


export default class PlayerReportView extends View
{
    // #region Attributes //

    // private:

    private _reported: string = null;
    
    // Constants.
    private static readonly _kResId: string = "common-player_report";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    public set reported( value: string ) { this._reported = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( PlayerReportView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#playerReportBackBtn" ).on( "click", this.onBackBtn_Click.bind( this ) );
        this._jqRoot.find( "#playerReportSendBtn" ).on( "click", this.onSendBtn_Click.bind( this ) );
    }

    public init(): void
    {
        console.assert( this._reported != null, "PlayerReportView.ts :: init() :: this._reported cannot be null." );

        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ { id: PlayerReportView.kResId, isPreloadImages: true } ] } );
    
        this._jqRoot.find( "#playerReportReported" ).val( this._reported );
    }

    // #endregion //


    // #region Input Callbacks //

    private onBackBtn_Click(): void
    {
        ServiceLocator.viewManager.fadeOut( this );
    }

    private onSendBtn_Click(): void
    {
        const kReported: string = ( this._jqRoot.find( "#playerReportReported" ).val() as string ).trim();
        const kDescription: string = ( this._jqRoot.find( "#playerReportDescription" ).val() as string ).trim();

        if ( kReported.length == 0 || kDescription.length == 0 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "ALL_FIELDS_REQUIRED" ), view_layer_id.POPUP );
        }
        else
        {
            this._jqLoadingAnimation.show();

            ServiceLocator.dbConnector.post( { 
                serviceName: "PlayerReportDAO",
                methodName: "send",
                parameters: [ 
                    Session.playerId, 
                    kReported,
                    kDescription, 
                    Session.token ] },
            this.onDbSendReport_Result.bind( this ) );
        }
    }

    // #endregion //


    // #region Other Callbacks //

    private onDbSendReport_Result( result: number ): void
    {
        if ( !this._id ) { return; }

        switch ( result )
        {
            case 0:
            {
                let infoMessageView: InfoMessageView = Utils.web.showInfoMessage( jQuery.i18n( "REPORT_SENT" ), view_layer_id.POPUP );
                infoMessageView.onClosed.addOnce( () => { this.onBackBtn_Click(); } );
                break;
            }

            case 1:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "REPORT_DESCRIPTION_TOO_LONG" ), view_layer_id.POPUP );
                break;
            }

            case 10:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
                break;
            }

            case 11:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "INVALID_USER_TOKEN" ), view_layer_id.POPUP );

                // TODO: Go back to login screen.
                break;
            }
        }

        this._jqLoadingAnimation.hide();
    }
}