import WindowedView from "../WindowedView";

import ServiceLocator from "../../ServiceLocator";
import Utils from "../../Utils";
import { view_layer_id } from "../../service/ViewManager";

import UserVO from "../../vo/UserVO";
import InfoMessageView from "./popup/InfoMessageView";
import GameHistoryVO from "../../vo/GameHistoryVO";
import { ICardSet, IScenario } from "../../game/ScenarioDB";


export default class PlayerProfileView extends WindowedView
{
    // #region Attributes //

    // private:

    private _username: string = null;
    
    private _arrGameHistoryVO: Array<GameHistoryVO> = null;
    
    // Constants.
    private static readonly _kResId: string = "common-player_profile";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    public set username( value: string ) { this._username = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( PlayerProfileView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#playerProfileBackBtn" ).on( "click", this.onBackBtn_Click.bind( this ) );
    }

    public init(): void
    {
        console.assert( this._username != null, "PlayerProfileView.ts :: init() :: this._username cannot be null." );

        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ { id: PlayerProfileView.kResId, isPreloadImages: true } ],
            isServerDataRequired: true } );

        ServiceLocator.dbConnector.post( { 
            serviceName: "UserDAO",
            methodName: "getProfile",
            parameters: [ this._username ] },
            this.onDbProfile_Retrieved.bind( this ) );
    }

    // private:

    private updateHistory( arrGameHistoryVO: Array<GameHistoryVO> ): void
    {
        let jqDocFragment: JQuery<HTMLElement> = jQuery( document.createDocumentFragment() );
        for ( let gameHistoryVO of arrGameHistoryVO )
        {
            const kCardSet: ICardSet = ServiceLocator.scenarioDb.find( gameHistoryVO.scenarioId.substr( 0, 2 ) );
            const kScenario: IScenario = ServiceLocator.scenarioDb.findScenario( gameHistoryVO.scenarioId );
            let jqRow: JQuery<HTMLElement> = jQuery(
                "<div class=\"row\">"
                    + "<div class=\"when gb-text-ellipsis\">" + Utils.web.createElapsedTimeStr( gameHistoryVO.whenTimestamp ) + "</div>"
                    + "<div class=\"card_set gb-text-ellipsis\">" + kCardSet.name + "</div>"
                    + "<div class=\"scenario gb-text-ellipsis\">" + ( kScenario.name ? kScenario.name : kScenario.encounter_set.main ) + "</div>"
                    + "<div class=\"ally gb-text-ellipsis\">" + gameHistoryVO.allyId + "</div>"
                + "</div>" );
            jqDocFragment.append( jqRow );
        }
        while ( jqDocFragment.children().length < 8 )
        {
            let rowHtml: string = 
            "<div class=\"row\">"
                + "<div class=\"when\"></div>"
                + "<div class=\"card_set\"></div>"
                + "<div class=\"scenario\"></div>"
                + "<div class=\"ally\"></div>"
            + "</div>";
            jqDocFragment.append( rowHtml );
        }
        this._jqRoot.find( "#playerProfileGameHistory" ).empty().append( jqDocFragment );
    }

    // #endregion //


    // #region Input Callbacks //

    private onBackBtn_Click(): void
    {
        ServiceLocator.viewManager.fadeOut( this );
    }

    // #endregion //


    // #region Other Callbacks //

    private onDbProfile_Retrieved( result: number, user: Object, arrGameHistoryVO: Array<Object> ): void
    {
        if ( result == 1 )
        {
            let infoMessage: InfoMessageView = Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
            infoMessage.onClosed.addOnce( () => { this.onBackBtn_Click(); } );
        }
        else
        {
            let userVO: UserVO = new UserVO();
            userVO.parse( user );

            this._jqRoot.find( "#playerProfileTitle" ).text( userVO.username );
            let karmaStr: string = userVO.karma.toString();
            if ( userVO.karma > 0 )
            {
                karmaStr = "+" + karmaStr;
                this._jqRoot.find( "#playerProfileKarma" ).addClass( "good" );
            }
            else if ( userVO.karma < 0 )
            {
                this._jqRoot.find( "#playerProfileKarma" ).addClass( "bad" );
            }
            this._jqRoot.find( "#playerProfileKarma" ).text( karmaStr );

            let lastConnection: string = null;
            if ( userVO.lastConnTimestamp == -1 )
            {
                lastConnection = "-";
            }
            else
            {
                lastConnection = jQuery.i18n( "TIME_AGO" ).replace( "#", Utils.web.createElapsedTimeStr( userVO.lastConnTimestamp ) );
            }
            this._jqRoot.find( "#playerProfileLastConnection" ).text( lastConnection );

            this._arrGameHistoryVO = GameHistoryVO.parseArray( arrGameHistoryVO );
            this.updateHistory( this._arrGameHistoryVO );

            this._viewPreloader.onServerDataLoaded();
        }
    }
    
    // #endregion //
}
