import View from "../View";

import { IFileLoadInfo } from "../../lib/HtmlFileLoader";
import ServiceLocator from "../../ServiceLocator";
import Session from "../../Session";
import { view_layer_id } from "../../service/ViewManager";

import InfoMessageView, { btn_layout_type } from "./popup/InfoMessageView";
import Signal from "../../lib/signals/Signal";
import PlayerContextMenuView from "./PlayerContextMenuView";
import State from "../../lib/state_machine/State";
import { main_state_id } from "../../states/StateId";
import { user_status_type, IUser } from "../../service/socket_io/GatewaySocketIOController";
import PlayerProfileView from "./PlayerProfileView";
import { TweenMax } from "gsap";
import Utils from "../../Utils";
import PlayerReportView from "./PlayerReportView";
import UserVO from "../../vo/UserVO";
import { IDeck } from "../gateway/DeckParser";
import GameRoomView from "../gateway/GameRoomView";
import GatewayState from "../../states/GatewayState";


export default class ChatView extends View
{
    // #region Attributes //

    // private:
    
    private _selUserId: string = null;
    private _playerContextMenuView: PlayerContextMenuView = null;
    private _jqActiveMessages: JQuery<HTMLElement> = null;
    private _guess: string = null;

    // Quick access.
    private _jqGlobalMessages: JQuery<HTMLElement> = null;
    private _jqGameMessages: JQuery<HTMLElement> = null;
    private _jqReminder: JQuery<HTMLElement> = null;
    private _jqConnectedPlayerList: JQuery<HTMLElement> = null;
    private _jqDisconnectedPlayerList: JQuery<HTMLElement> = null;
    private _jqShowDisconnectedCheckbox: JQuery<HTMLElement> = null;

    // Binded functions.
    private _bfOnWindowResized: any = null;
    private _bfOnDocumentClick = null;

    // Signals.
    private _onInvitationAccepted: Signal = new Signal();
    private _onMyMessageFocusIn: Signal = new Signal();
    private _onMyMessageFocusOut: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "common-txat";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }
    public get playerContextMenu(): PlayerContextMenuView { return this._playerContextMenuView; }

    // Signals.
    public get onInvitationAccepted(): Signal { return this._onInvitationAccepted; }
    public get onMyMessageFocusIn(): Signal { return this._onMyMessageFocusIn; }
    public get onMyMessageFocusOut(): Signal { return this._onMyMessageFocusOut; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( ChatView._kResId );

        // Quick access.
        this._jqGlobalMessages = this._jqRoot.find( "#chatGlobalMessages" );
        this._jqGameMessages = this._jqRoot.find( "#chatGameMessages" );
        this._jqReminder = this._jqRoot.find( "#chatReminder" );
        this._jqConnectedPlayerList = this._jqRoot.find( "#chatConnectedPlayerList" );
        this._jqDisconnectedPlayerList = this._jqRoot.find( "#chatDisconnectedPlayerList" );
        this._jqShowDisconnectedCheckbox = this._jqRoot.find( "#chatShowDisconnectedCheckbox" );

        // Binded functions.
        this._bfOnWindowResized = this.onWindow_Resized.bind( this );
        this._bfOnDocumentClick = this.onDocument_Click.bind( this );

        // Attach UI event handlers.
        this._jqRoot.find( "#chatGlobalTab" ).on( "click", this.onGlobalChat_Click.bind( this ) );
        this._jqRoot.find( "#chatGameTab" ).on( "click", this.onGameChat_Click.bind( this ) );
        this._jqRoot.find( "#chatMyMessage" ).on( "keyup", this.onMyMessage_KeyUp.bind( this ) );
        this._jqRoot.find( "#chatMyMessage" ).on( "focusin", this.onMyMessage_FocusIn.bind( this ) );
        this._jqRoot.find( "#chatMyMessage" ).on( "focusout", this.onMyMessage_FocusOut.bind( this ) );
        this._jqRoot.find( "#chatHandle" ).on( "click", this.onHandle_Click.bind( this ) );
        this._jqShowDisconnectedCheckbox.on( "click", this.onShowDisconnectedCheckbox_Click.bind( this ) );
    }

    public init(): void
    {
        super.init();

        let arrHtmlFileInfo: Array<IFileLoadInfo> = [  
            // 1st level.
            { id: ChatView.kResId, isPreloadImages: true },
            { id: PlayerContextMenuView.kResId, isPreloadImages: true } ];
        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: arrHtmlFileInfo,
            isServerDataRequired: true,
            onCompleteCb: this.onView_Preloaded.bind( this ) } );

        this._jqActiveMessages = this._jqGlobalMessages;

        this._jqRoot.find( "#chatGameTab" ).hide();
        this._jqReminder.hide();

        this._jqShowDisconnectedCheckbox.prop( "checked", ServiceLocator.savedData.data.chatShowDisconnected );
        if ( !ServiceLocator.savedData.data.chatShowDisconnected )
        {
            this._jqDisconnectedPlayerList.hide();
        }

        ServiceLocator.dbConnector.post( { 
            serviceName: "UserDAO",
            methodName: "getUserList" },
            this.onDbGetUserList_Result.bind( this ) );
    }

    public end(): void
    {
        // Owned signals.
        this._onInvitationAccepted.removeAll();
        this._onMyMessageFocusIn.removeAll();
        this._onMyMessageFocusOut.removeAll();

        ServiceLocator.viewManager.remove( this._playerContextMenuView );
        this._playerContextMenuView = null;

        // Cleanup events.
        jQuery( window ).off( "resize", this._bfOnWindowResized );
        jQuery( document ).off( "click", this._bfOnDocumentClick );
        ServiceLocator.socketIOManager.onReconnect.remove( this.onSync_Reconnect, this );
        ServiceLocator.socketIOManager.gateway.onUserConnected.remove( this.onSyncUser_Connected, this );
        ServiceLocator.socketIOManager.gateway.onUserDisconnected.remove( this.onSyncUser_Disconnected, this );
        ServiceLocator.socketIOManager.gateway.onUserStatusUpdated.remove( this.onSyncUserStatus_Updated, this );
        ServiceLocator.socketIOManager.gateway.onGlobalMessageReceived.remove( this.onSyncGlobalMessage_Received, this );
        ServiceLocator.socketIOManager.gateway.onGameMessageReceived.remove( this.onSyncGameMessage_Received, this );
        ServiceLocator.socketIOManager.gateway.onPrivateMessageReceived.remove( this.onSyncPrivateMessage_Received, this );
        ServiceLocator.socketIOManager.gateway.onGameInvitationReceived.remove( this.onSyncGameInvitation_Received, this );
        ServiceLocator.socketIOManager.gateway.onGameInvitationAccepted.remove( this.onSyncGameInvitation_Accepted, this );
        ServiceLocator.socketIOManager.gateway.onGameInvitationCanceled.remove( this.onSyncGameInvitation_Canceled, this );
        ServiceLocator.socketIOManager.gateway.onGameInvitationRejected.remove( this.onSyncGameInvitation_Rejected, this );
        ServiceLocator.stateMachine.onStateEntered.remove( this.onMainState_Entered, this );

        super.end();
    }

    public setMyUserStatus( userStatus: user_status_type ): void
    {
        this.onSyncUserStatus_Updated( Session.playerId, userStatus );
    }

    public listenToBindedKeys(): void
    {
        ServiceLocator.game.keyBindingManager.onKeyDown.add( this.onBindedKey_Down, this );
    }

    // private:

    private createPlayerItemHtml( userId: string, userStatus: user_status_type ): JQuery<HTMLElement>
    {
        let result: JQuery<HTMLElement> = null;

        result = jQuery( 
            "<div class=\"item gb-text-body-normal gb-custom_pointer\" data-userId=\"" + userId + "\">"
                + "<div class=\"status " + this.findUserStatusCssClass( userStatus ) + "\"></div>"    
                + "<div class=\"name gb-text-ellipsis\">" + userId + "</div>" 
            + "</div>" );
        result.on( "click", this.onPlayerItem_Click.bind( this, userId ) );
        
        return result;
    }

    private findUserStatusCssClass( userStatus: user_status_type ): string
    {
        let result: string = null;

        switch ( userStatus )
        {
            case user_status_type.DISCONNECTED: { result = "disconnected"; break; }
            case user_status_type.IDLE: { result = "idle"; break; }
            case user_status_type.NEGOTIATING_GAME: { result = "negotiating_game"; break; }
            case user_status_type.IN_GAME_ROOM: { result = "in_game_room"; break; }
            case user_status_type.PLAYING: { result = "playing"; break; }
        }

        return result;
    }

    private findUserStatusType( jqPlayerItem: JQuery<HTMLElement> ): user_status_type
    {
        let result: user_status_type = null;

        let jqStatus: JQuery<HTMLElement> = jqPlayerItem.find( ".status" );
        if ( jqStatus.hasClass( "idle" ) )
        {
            result = user_status_type.IDLE;
        }
        else if ( jqStatus.hasClass( "negotiating_game" ) )
        {
            result = user_status_type.NEGOTIATING_GAME;
        }
        else if ( jqStatus.hasClass( "in_game_room" ) )
        {
            result = user_status_type.IN_GAME_ROOM;
        }
        else if ( jqStatus.hasClass( "playing" ) )
        {
            result = user_status_type.PLAYING;
        }
        else if ( jqStatus.hasClass( "disconnected" ) )
        {
            result = user_status_type.DISCONNECTED;
        }

        return result;
    }

    private appendSystemMessage( message: string, dateStr: string ): void
    {
        this.appendMessage( "<div class=\"system\">" 
                + "<span class=\"date\">" + dateStr + "</span> " + message 
            + "</div>", 
            this._jqGlobalMessages );
    }

    private appendPrivateMessage( from: string, to: string, message: string, dateStr: string ): void
    {
        this.appendMessage( 
            "<div class=\"private\">" 
                + "<span class=\"date\">" + dateStr + "</span> ..." + ( from != null ? "from" : "to" ) + " <span class=\"sender\">" + ( from != null ? from : to ) + ":</span> <span class=\"message\">" + message + "</span>"
            + "</div>",
            this._jqActiveMessages );
    }

    private appendGlobalMessage( from: string, message: string, dateStr: string ): void
    {
        this.appendMessage( 
            "<div class=\"global\">" 
                + "<span class=\"date\">" + dateStr + "</span> <span class=\"sender\">" + from + ":</span> <span class=\"message\">" + message + "</span>"
            + "</div>",
            this._jqGlobalMessages );
    }

    private appendGameMessage( from: string, message: string, dateStr: string ): void
    {
        this.appendMessage( 
            "<div class=\"game\">" 
                + "<span class=\"date\">" + dateStr + "</span> <span class=\"sender\">" + from + ":</span> <span class=\"message\">" + message + "</span>"
            + "</div>",
            this._jqGameMessages );
    }

    private appendMessage( messageHtml: string, jqMessages: JQuery<HTMLElement> ): void
    {
        let isAutoScroll: boolean = Math.abs( ( jqMessages[ 0 ].scrollHeight - jqMessages.scrollTop() ) - jqMessages[ 0 ].clientHeight ) < 10;

        jqMessages.append( messageHtml );

        if ( isAutoScroll )
        {
            jqMessages.scrollTop( jqMessages.prop( "scrollHeight" ) );
        }

        if ( this._jqRoot.hasClass( "hidden" ) && jqMessages == this._jqActiveMessages )
        {
            this._jqReminder.show();
            Utils.anim.flushTweensOf( this._jqReminder[ 0 ] );
            TweenMax.to( this._jqReminder[ 0 ], 0.15, { css: { scale: 1.15 }, yoyo: true, repeat: 1 } );
        }
    }

    private formatDate( now: Date ): string
    {
        return ( "0" + now.getHours().toString() ).slice( -2 ) + ":" + ( "0" + now.getMinutes().toString() ).slice( -2 ) + ":" + ( "0" + now.getSeconds().toString() ).slice( -2 );
    }

    // #endregion //


    // #region Input Callbacks //

    private onGlobalChat_Click(): void
    {
        this._jqGlobalMessages.show();
        this._jqGameMessages.hide();

        this._jqRoot.find( "#chatGlobalTab" ).addClass( "selected" );
        this._jqRoot.find( "#chatGameTab" ).removeClass( "selected" );
        
        this._jqActiveMessages = this._jqGlobalMessages;
    }

    private onGameChat_Click(): void
    {
        this._jqGlobalMessages.hide();
        this._jqGameMessages.show();

        this._jqRoot.find( "#chatGameTab" ).addClass( "selected" );
        this._jqRoot.find( "#chatGlobalTab" ).removeClass( "selected" );

        this._jqActiveMessages = this._jqGameMessages;
    }

    private onPlayerItem_Click( userId: string, jqEvent: JQuery.Event ): void
    {
        let jqPlayerItem: JQuery<HTMLElement> = jQuery( jqEvent.target );
        const kUserStatus: user_status_type = this.findUserStatusType( jqPlayerItem );

        this._selUserId = userId;

        this._jqRoot.find( "#chatPlayerList .item" ).removeClass( "selected" );
        jqPlayerItem.addClass( "selected" ); 

        const kMyUserStatus: user_status_type = this.findUserStatusType( this._jqConnectedPlayerList.eq( 0 ) );
        this._playerContextMenuView.onUserStatusUpdated( kUserStatus, userId, kMyUserStatus );
        this._playerContextMenuView.showAt( jqEvent.clientX, jqEvent.clientY );
    }

    private onMyMessage_KeyUp( event: KeyboardEvent ): void
    {
        let message: string = ( this._jqRoot.find( "#chatMyMessage" ).val() as string ).trim();

        if ( event.which == 13 && message != "" )
        {   
            this._jqRoot.find( "#chatMyMessage" ).val( "" );

            const kDateStr: string = this.formatDate( new Date() );
            if ( message.substr( 0, 4 ) == "\/to:" )
            {
                let toAndMessage: string = message.substr( 4 );
                if ( toAndMessage.indexOf( " " ) > 0 )
                {
                    let to: string = toAndMessage.substr( 0, toAndMessage.indexOf( " " ) );
                    message = toAndMessage.substr( to.length + 1 );
                    if ( to != Session.playerId && message.indexOf( " " ) != 0 )
                    {
                        ServiceLocator.socketIOManager.gateway.sendPrivateMessage( to, message );
                        this.appendPrivateMessage( null, to, message, kDateStr );
                    }
                }
            }
            else if ( this._jqGlobalMessages.css( "display" ) != "none" )
            {
                ServiceLocator.socketIOManager.gateway.sendGlobalMessage( message );
                this.appendGlobalMessage( Session.playerId, message, kDateStr );
            }
            else
            {
                let result: boolean = false;

                // Random command.
                const kRandomRegex: RegExp = /\/random (\d+) (\d+)/g;
                let arrExec: RegExpExecArray = kRandomRegex.exec( message );
                if ( arrExec != null )
                {
                    const kFrom: number = parseInt( arrExec[ 1 ] );
                    const kTo: number = parseInt( arrExec[ 2 ] );
                    if ( kFrom < kTo )
                    {
                        const kMessage: string = jQuery.i18n( "RANDOM_SENT" )
                            .replace( "#0", kFrom.toString() )
                            .replace( "#1", kTo.toString() )
                            .replace( "#2", Utils.randInterval( kFrom, kTo, true ).toString() );
                        this.appendGameMessage( Session.playerId, kMessage, kDateStr );
                        ServiceLocator.socketIOManager.gateway.sendGameMessage( kMessage );
                    }

                    result = true;
                }

                if ( !result )
                {
                    // Guess command.
                    const kGuessRegex: RegExp = /\/guess (.+)/g;
                    let arrExec: RegExpExecArray = kGuessRegex.exec( message );
                    if ( arrExec != null )
                    {
                        this.appendSystemMessage( jQuery.i18n( "GUESS_SENT" ).replace( "#", arrExec[ 1 ] ), kDateStr );
                        ServiceLocator.socketIOManager.gateway.sendGameMessage( message );

                        result = true;
                    }
                }

                if ( !result )
                {
                    this.appendGameMessage( Session.playerId, message, kDateStr );
                    ServiceLocator.socketIOManager.gateway.sendGameMessage( message );
                    
                    if ( this._guess )
                    {
                        this.appendSystemMessage( jQuery.i18n( "GUESS_RESULT" ).replace( "#", this._guess ), kDateStr );
                        this._guess = null;
                    }
                }
            }
        }
    }

    private onMyMessage_FocusIn(): void
    {
        this._onMyMessageFocusIn.dispatch();
    }

    private onMyMessage_FocusOut(): void
    {
        this._onMyMessageFocusOut.dispatch();
    }

    private onHandle_Click(): void
    {
        this._jqRoot.toggleClass( "hidden" );
        this._jqReminder.hide();
    }

    private onShowDisconnectedCheckbox_Click(): void
    {
        ServiceLocator.savedData.data.chatShowDisconnected = this._jqShowDisconnectedCheckbox.prop( "checked" );
        if ( ServiceLocator.savedData.data.chatShowDisconnected )
        {
            this._jqDisconnectedPlayerList.show();
        }
        else
        {
            this._jqDisconnectedPlayerList.hide();
        }
        ServiceLocator.savedData.save();
    }

    private onWindow_Resized(): void
    {
        this._playerContextMenuView.hide();
    }

    private onDocument_Click( event: MouseEvent ): void
    {
        if ( event.target instanceof HTMLElement )
        {
            if ( jQuery( event.target ).closest( this._playerContextMenuView.root ).length == 0
                && !event.target.classList.contains( "item" ) ) 
            {
                this._playerContextMenuView.hide();

                this._selUserId = null;
                this._jqRoot.find( "#chatPlayerList .item" ).removeClass( "selected" );
            }
        }
    }

    // #endregion //


    // #region DB Callbacks //

    private onDbGetUserList_Result( arrUser ): void
    {
        let arrUserVO: Array<UserVO> = UserVO.parseArray( arrUser );
        let jqDocumentFragment: JQuery<HTMLElement> = jQuery( document.createDocumentFragment() );
        for ( let userVO of arrUserVO )
        {
            jqDocumentFragment.append( this.createPlayerItemHtml( userVO.username, user_status_type.DISCONNECTED ) );
        }        
        this._jqDisconnectedPlayerList.append( jqDocumentFragment );

        // Retrieve player list.
        ServiceLocator.socketIOManager.gateway.onUserListRetrieved.addOnce( this.onSyncUserList_Retrieved, this );
        ServiceLocator.socketIOManager.gateway.retrieveUserList();

        // Listen to events.
        jQuery( window ).on( "resize", this._bfOnWindowResized );
        jQuery( document ).on( "click", this._bfOnDocumentClick );
        ServiceLocator.socketIOManager.onReconnect.add( this.onSync_Reconnect, this );
        ServiceLocator.socketIOManager.gateway.onUserConnected.add( this.onSyncUser_Connected, this );
        ServiceLocator.socketIOManager.gateway.onUserDisconnected.add( this.onSyncUser_Disconnected, this );
        ServiceLocator.socketIOManager.gateway.onUserStatusUpdated.add( this.onSyncUserStatus_Updated, this );
        ServiceLocator.socketIOManager.gateway.onGlobalMessageReceived.add( this.onSyncGlobalMessage_Received, this );
        ServiceLocator.socketIOManager.gateway.onGameMessageReceived.add( this.onSyncGameMessage_Received, this );
        ServiceLocator.socketIOManager.gateway.onPrivateMessageReceived.add( this.onSyncPrivateMessage_Received, this );
        ServiceLocator.socketIOManager.gateway.onGameInvitationReceived.add( this.onSyncGameInvitation_Received, this );
        ServiceLocator.socketIOManager.gateway.onGameInvitationAccepted.add( this.onSyncGameInvitation_Accepted, this );
        ServiceLocator.socketIOManager.gateway.onGameInvitationCanceled.add( this.onSyncGameInvitation_Canceled, this );
        ServiceLocator.socketIOManager.gateway.onGameInvitationRejected.add( this.onSyncGameInvitation_Rejected, this );
        ServiceLocator.stateMachine.onStateEntered.add( this.onMainState_Entered, this );

        this._viewPreloader.onServerDataLoaded();
    }

    // #endregion //


    // #region Sync Callbacks //

    private onSync_Reconnect(): void
    {
        this._jqDisconnectedPlayerList.append( this._jqConnectedPlayerList.children() );
        for ( let i: number = 0; i < this._jqDisconnectedPlayerList.length; ++i )
        {
            this._jqDisconnectedPlayerList.eq( i ).find( "div.status" )
                .removeClass( "disconnected idle negotiating_game in_game_room playing" )
                .addClass( "disconnected" );
        }

        ServiceLocator.socketIOManager.gateway.onUserListRetrieved.addOnce( this.onSyncUserList_Retrieved, this );
        ServiceLocator.socketIOManager.gateway.retrieveUserList();
    }

    private onSyncUserList_Retrieved( arrUser: Array<IUser> ): void
    {
        let jqDocumentFragment: JQuery<HTMLElement> = jQuery( document.createDocumentFragment() );
        jqDocumentFragment.append( this.createPlayerItemHtml( Session.playerId, user_status_type.IDLE ) );
        for ( let user of arrUser )
        {
            if ( user.id != Session.playerId )
            {
                jqDocumentFragment.append( this.createPlayerItemHtml( user.id, user.status ) );
            }

            this._jqDisconnectedPlayerList.find( "[data-userId=" + user.id + "]" ).remove();
        }        
        this._jqConnectedPlayerList.append( jqDocumentFragment );
    }

    private onSyncUser_Connected( userId: string ): void
    {
        this._jqDisconnectedPlayerList.find( "[data-userId=" + userId + "]" ).remove();
        this._jqConnectedPlayerList.append( this.createPlayerItemHtml( userId, user_status_type.IDLE ) );
        
        this.appendSystemMessage( jQuery.i18n( "HAS_ENTERED_THE_PRANCING_PONY" ).replace( "#", userId ), this.formatDate( new Date() ) );
    }

    private onSyncUser_Disconnected( userId: string ): void
    {
        if ( this._selUserId == userId )
        {
            this._selUserId = null;
            this._playerContextMenuView.hide();
        }

        this._jqConnectedPlayerList.find( "[data-userId=" + userId + "]" ).remove();
        this._jqDisconnectedPlayerList.append( this.createPlayerItemHtml( userId, user_status_type.DISCONNECTED ) );
        
        this.appendSystemMessage( jQuery.i18n( "HAS_LEFT_THE_PRANCING_PONY" ).replace( "#", userId ), this.formatDate( new Date() ) );
    }

    private onSyncUserStatus_Updated( userId: string, userStatus: user_status_type ): void
    {
        let jqPlayerItem: JQuery<HTMLElement> = this._jqConnectedPlayerList.find( "[data-userId=" + userId + "]" );
        jqPlayerItem.find( "div.status" )
            .removeClass( "disconnected idle negotiating_game in_game_room playing" )
            .addClass( this.findUserStatusCssClass( userStatus ) );

        if ( userId == this._selUserId && this._playerContextMenuView.isVisible() )
        {
            const kMyUserStatus: user_status_type = this.findUserStatusType( this._jqConnectedPlayerList.eq( 0 ) );
            this._playerContextMenuView.onUserStatusUpdated( userStatus, userId, kMyUserStatus );
        }
        
        if ( userId == Session.playerId )
        {
            if ( userStatus == user_status_type.IN_GAME_ROOM )
            {
                this._jqRoot.find( "#chatGameTab" ).show();
                this.onGameChat_Click();
            }
            else if ( userStatus == user_status_type.IDLE )
            {
                this._jqRoot.find( "#chatGameTab" ).hide();
                this.onGlobalChat_Click();
            }
        }
        else if ( userId == Session.allyId )
        {
            if ( userStatus == user_status_type.IDLE )
            {
                this._jqRoot.find( "#chatGameTab" ).hide();
                this.onGlobalChat_Click();
            }
        }
    }

    private onSyncGameInvitation_Received( userId: string ): void
    {
        if ( !Session.allyId )
        {
            Session.allyId = userId;

            let infoMessageView: InfoMessageView = new InfoMessageView();
            infoMessageView.alias = "game_invitation_received";
            infoMessageView.message = jQuery.i18n( "GAME_INVITATION_RECEIVED" ).replace( "#0", userId );
            infoMessageView.btnLayoutType = btn_layout_type.DOUBLE;
            infoMessageView.onClosed.add( this.onGameInvitation_Closed, this );
            infoMessageView.init();
            ServiceLocator.viewManager.fadeIn( infoMessageView, view_layer_id.TOPMOST );
        }
        else
        {
            ServiceLocator.socketIOManager.gateway.rejectGameInvitation( userId );
        }
    }

    private onSyncGameInvitation_Accepted(): void
    {
        ServiceLocator.viewManager.fadeOut( ServiceLocator.viewManager.findViewByAlias( "waiting_game_response" )[ 0 ] );
        if ( ServiceLocator.viewManager.findViewByResId( GameRoomView.kResId ).length == 0 )
        {
            ( ServiceLocator.stateMachine.currentState as GatewayState ).forcePlay();
        }
    }

    private onSyncGameInvitation_Canceled(): void
    {
        ServiceLocator.viewManager.fadeOut( ServiceLocator.viewManager.findViewByAlias( "game_invitation_received" )[ 0 ] );

        Session.reset();
    }

    private onSyncGameInvitation_Rejected(): void
    {
        ServiceLocator.viewManager.fadeOut( ServiceLocator.viewManager.findViewByAlias( "waiting_game_response" )[ 0 ] );

        Session.allyId = null;
    }

    private onSyncGlobalMessage_Received( from: string, message: string, now: Date ): void
    {
        this.appendGlobalMessage( from, message, this.formatDate( now ) );
    }

    private onSyncGameMessage_Received( from: string, message: string, now: Date ): void
    {
        if ( message.substr( 0, 7 ) == "\/guess " )
        {
            this._guess = message.substr( 7 );
            this.appendSystemMessage( jQuery.i18n( "CAN_YOU_GUESS" ), this.formatDate( now ) );
        }
        else
        {
            this.appendGameMessage( from, message, this.formatDate( now ) );
        }
    }

    private onSyncPrivateMessage_Received( from: string, message: string, now: Date ): void
    {
        this.appendPrivateMessage( from, null, message, this.formatDate( now ) );
    }

    // #endregion //


    // #region Other Callbacks //

    private onView_Preloaded(): void
    {
        this._playerContextMenuView = new PlayerContextMenuView();
        this._playerContextMenuView.onInviteSelected.add( this.onInvite_Selected, this );
        this._playerContextMenuView.onSendMessageSelected.add( this.onSendMessage_Selected, this );
        this._playerContextMenuView.onViewProfileSelected.add( this.onViewProfile_Selected, this );
        this._playerContextMenuView.onReportSelected.add( this.onReport_Selected, this );
        this._playerContextMenuView.init();
        ServiceLocator.viewManager.add( this._playerContextMenuView, view_layer_id.HUD );
        this._playerContextMenuView.hide();
    }

    private onInvite_Selected(): void
    {
        Session.allyId = this._selUserId;

        if ( ServiceLocator.viewManager.findViewByResId( GameRoomView.kResId ).length == 0 )
        {
            this.setMyUserStatus( user_status_type.NEGOTIATING_GAME );
        }
        ServiceLocator.socketIOManager.gateway.inviteToGame( this._selUserId );
    
        let infoMessageView: InfoMessageView = new InfoMessageView();
        infoMessageView.alias = "waiting_game_response";
        infoMessageView.message = jQuery.i18n( "WAITING_RESPONSE" );
        infoMessageView.root.find( "#infoMessageAcceptBtn" ).text( jQuery.i18n( "CANCEL" ) );
        infoMessageView.onClosed.add( this.onGameInvitation_Canceled, this );
        infoMessageView.init();
        ServiceLocator.viewManager.fadeIn( infoMessageView, view_layer_id.TOPMOST );
        
        this._playerContextMenuView.hide();
    }

    private onSendMessage_Selected(): void
    {
        let jqMyMessage: JQuery<HTMLElement> = this._jqRoot.find( "#chatMyMessage" );
        jqMyMessage.val( "/to:" + this._selUserId + " " );
        let myMessage: HTMLInputElement = jqMyMessage[ 0 ] as HTMLInputElement;
        myMessage.focus();
        myMessage.selectionStart = myMessage.selectionEnd = myMessage.value.length;

        this._playerContextMenuView.hide();
    }

    private onViewProfile_Selected(): void
    {
        let playerProfileView: PlayerProfileView = new PlayerProfileView();
        playerProfileView.username = this._selUserId;
        playerProfileView.init();
        ServiceLocator.viewManager.clearLayer( view_layer_id.POPUP );
        ServiceLocator.viewManager.fadeIn( playerProfileView, view_layer_id.POPUP );

        this._playerContextMenuView.hide();
    }

    private onReport_Selected(): void
    {
        let playerReportView: PlayerReportView = new PlayerReportView();
        playerReportView.reported = this._selUserId;
        playerReportView.init();
        ServiceLocator.viewManager.clearLayer( view_layer_id.POPUP );
        ServiceLocator.viewManager.fadeIn( playerReportView, view_layer_id.POPUP );

        this._playerContextMenuView.hide();
    }

    private onGameInvitation_Canceled(): void
    {
        ServiceLocator.socketIOManager.gateway.cancelGameInvitation( Session.allyId );

        Session.allyId = null;

        if ( ServiceLocator.viewManager.findViewByResId( GameRoomView.kResId ).length == 0 )
        {
            this.setMyUserStatus( user_status_type.IDLE );
        }
    }

    private onGameInvitation_Closed( result: boolean ): void
    {
        if ( result )
        {
            ServiceLocator.socketIOManager.gateway.acceptGameInvitation( Session.allyId );

            this._onInvitationAccepted.dispatch();
        }
        else
        {
            ServiceLocator.socketIOManager.gateway.rejectGameInvitation( Session.allyId );
        
            Session.reset();
        }
    }

    private onMainState_Entered( state: State, stateId: main_state_id ): void
    {
        if ( stateId == main_state_id.GAME )
        {
            ServiceLocator.socketIOManager.gateway.onGameInvitationReceived.remove( this.onSyncGameInvitation_Received, this );
        }
        else if ( stateId == main_state_id.GATEWAY )
        {
            ServiceLocator.socketIOManager.gateway.onGameInvitationReceived.add( this.onSyncGameInvitation_Received, this );
        }
    }

    private onBindedKey_Down( bindedId: string ): void
    {
        if ( bindedId == "chat" )
        {
            this.onHandle_Click();
        }
    }

    // #endregion //
}