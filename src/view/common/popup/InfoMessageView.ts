import WindowedView from "../../WindowedView";

import ServiceLocator from "../../../ServiceLocator";

import Signal from "../../../lib/signals/Signal";


export default class InfoMessageView extends WindowedView
{
    // #region Attributes //

    // private:

    private _btnLayoutType: btn_layout_type = btn_layout_type.SINGLE;
    private _message: string = "";
    private _countdown: number = null;
    private _countdownInterval: number = null;

    // Signals.
    private _onCountdownCompleted: Signal = new Signal();
    private _onClosed: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "common-popup-info_message";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    public set btnLayoutType( value: btn_layout_type ) { this._btnLayoutType = value; }
    public set message( value: string ) { this._message = value; }
    public set countdown( value: number ) { this._countdown = value; }

    // Signals.
    public get onCountdownCompleted(): Signal { return this._onCountdownCompleted; };
    public get onClosed(): Signal { return this._onClosed; };

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( InfoMessageView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#infoMessageAcceptBtn" ).on( "click", this.onSelf_Closed.bind( this, true ) );
        this._jqRoot.find( "#infoMessageCancelBtn" ).on( "click", this.onSelf_Closed.bind( this, false ) );
    }

    public init(): void
    {
        console.assert( this._message != null, "InfoMessageView.ts :: init() :: this._message cannot be null." );

        // Set message text.
        this._jqRoot.find( "#infoMessageText" ).html( this._message );

        // Set button layout.
        if ( this._btnLayoutType == btn_layout_type.SINGLE )
        {
            this._jqRoot.find( "#infoMessageCancelBtn" ).hide();
        }

        if ( this._countdown )
        {
            this._jqRoot.find( "#infoMessageControls" ).hide();
            this._jqRoot.find( "#infoMessageCountdown" ).text( this._countdown.toString() );
            this._countdownInterval = window.setInterval( this.onCountdown_Interval.bind( this ), 1000 );
        }
        else
        {
            this._jqRoot.find( "#infoMessageCountdown" ).hide();
        }
    }

    public end(): void
    {
        // Owned signals.
        this._onCountdownCompleted.removeAll();
        this._onClosed.removeAll();

        if ( this._countdownInterval )
        {
            clearInterval( this._countdownInterval );
            this._countdownInterval = null;
        }

        super.end();
    }

    // #endregion //


    // #region Callbacks //

    private onSelf_Closed( result: boolean, jqEvent: JQuery.Event ): void
    {
        if ( jqEvent )
        {
            jqEvent.stopImmediatePropagation();
        }

        this._onClosed.dispatch( result );
        
        ServiceLocator.viewManager.fadeOut( this );
    }

    private onCountdown_Interval(): void
    {
        this._countdown -= 1;
        this._jqRoot.find( "#infoMessageCountdown" ).text( this._countdown.toString() );

        if ( this._countdown == 0 )
        {
            clearInterval( this._countdownInterval );
            this._countdownInterval = null;

            this._jqRoot.find( "#infoMessageControls" ).show(); 
            this._jqRoot.find( "#infoMessageCountdown" ).hide(); 

            this._onCountdownCompleted.dispatch( this );
        }
    }

    // #endregion //
}

export const enum btn_layout_type
{
    SINGLE = 0,
    DOUBLE
}