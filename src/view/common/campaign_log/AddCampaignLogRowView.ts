import WindowedView from "../../WindowedView";

import ServiceLocator from "../../../ServiceLocator";
import Signal from "../../../lib/signals/Signal";
import { ICard } from "../../../game/CardDB";


export default class AddCampaignLogRowView extends WindowedView
{
    // #region Attributes //

    // private:

    private _arrField: Array<IAddRowField> = null;

    private _arrHeroCard: Array<ICard> = null;
    private _arrBoonCard: Array<ICard> = null;
    private _arrBurdenCard: Array<ICard> = null;
    private _arrValue: Array<string> = null;
    private _coincidenceRequestTimeout: number = null;

    // Signals.
    private _onRowAdded: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "common-campaign_log-add_row";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    public set fields( value: Array<IAddRowField> ) { this._arrField = value; }

    // Signals.
    public get onRowAdded(): Signal { return this._onRowAdded; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( AddCampaignLogRowView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#campaignLogAddRowFirstValue" )
            .on( "input", this.onFieldValue_Input.bind( this, 0 ) )
            .on( "focusin", this.onFieldValue_FocusIn.bind( this, 0 ) )
            .on( "focusout", this.onFieldValue_FocusOut.bind( this, 0 ) );
        this._jqRoot.find( "#campaignLogAddRowSecondValue" )
            .on( "input", this.onFieldValue_Input.bind( this, 1 ) )
            .on( "focusin", this.onFieldValue_FocusIn.bind( this, 1 ) )
            .on( "focusout", this.onFieldValue_FocusOut.bind( this, 1 ) );
        this._jqRoot.find( "#campaignLogAddRowBtn" ).on( "click", this.onAddBtn_Click.bind( this ) );
    }

    public init(): void
    {
        console.assert( this._arrField != null, "AddCampaignLogRowView.ts :: init() :: this._arrField cannot be null." );
        console.assert( this._arrField.length >= 1, "AddCampaignLogRowView.ts :: init() :: this._arrField.length must be >= l." );

        super.init();

        this._arrHeroCard = ServiceLocator.cardDb.findByTypeCodes( [ "hero" ] );
        this._arrBoonCard = ServiceLocator.cardDb.findBoons();
        this._arrBurdenCard = ServiceLocator.cardDb.findBurdens();

        this._arrValue = new Array<string>();
        for ( let i: number = 0; i < this._arrField.length; ++i )
        {
            this._arrValue.push( "" );
        }

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ { id: AddCampaignLogRowView.kResId, isPreloadImages: true } ] } );

        this._jqRoot.find( "#campaignLogAddRowFirstTitle" ).text( this._arrField[ 0 ].title );
        if ( this._arrField.length == 2 )
        {   
            this._jqRoot.find( ".controls" ).addClass( "double" );
            this._jqRoot.find( "#campaignLogAddRowSecondTitle" ).text( this._arrField[ 1 ].title );
        }

        this._jqRoot.find( "#campaignLogAddRowBtn" ).prop( "disabled", !this.validateValues() );
    }

    public end(): void
    {
        this._onRowAdded.removeAll();

        this._arrField = null;

        super.end();
    }

    // private:

    private clearCoincidenceRequestTimeout(): void
    {
        if ( this._coincidenceRequestTimeout != null )
        {
            window.clearTimeout( this._coincidenceRequestTimeout );
            this._coincidenceRequestTimeout = null;
        }
    }

    private validateValues(): boolean
    {
        let isValid: boolean = true;

        for ( let i: number = 0; i < this._arrValue.length; ++i )
        {
            if ( this._arrValue[ i ] == "" && !this._arrField[ i ].isOptional )
            {
                isValid = false;
                break;
            }
        }

        return isValid;
    }

    // #endregion //


    // #region Input Callbacks //

    private onFieldValue_Input( index: number, jqEvent: JQuery.Event ): void
    {
        this.clearCoincidenceRequestTimeout();

        let jqFieldValue: JQuery = jQuery( jqEvent.currentTarget );

        const kSearchText: string = ( jqFieldValue.val() as string ).trim();
        if ( kSearchText.length >= 3 )
        {
            this._coincidenceRequestTimeout = window.setTimeout( this.onCoincidenceRequest_Timeout.bind( this, index, kSearchText.toLowerCase() ), 1000 );
        }
    }

        private onCoincidenceRequest_Timeout( index: number, searchText: string ): void
        {
            this._coincidenceRequestTimeout = null;

            let arrCard: Array<ICard> = new Array<ICard>();

            switch ( this._arrField[ index ].type )
            {
                case add_row_value_type.HERO: { arrCard = this._arrHeroCard.slice(); break; }
                case add_row_value_type.BOON: { arrCard = this._arrBoonCard.slice(); break; }
                case add_row_value_type.BURDEN: { arrCard = this._arrBurdenCard.slice(); break; }
            }

            for ( let i: number = arrCard.length - 1; i >= 0; --i )
            {
                if ( arrCard[ i ].name.substr( 0, searchText.length ).toLowerCase() != searchText )
                {
                    arrCard.splice( i, 1 );
                }
            }

            let jqOptionList: JQuery<HTMLElement> = this._jqRoot.find( ".option_list" ).eq( index );
            jqOptionList.empty();
            for ( let card of arrCard )
            {
                let jqOption: JQuery<HTMLElement> = jQuery( 
                    "<div class=\"row gb-text-body-normal gb-text-ellipsis gb-custom_pointer sfx_item_click\" data-id=\"" + card.code + "\">" 
                        + card.name + " (" + card.pack_code + ")"
                    + "</div>" );
                jqOption.on( "click", this.onOptionListItem_Click.bind( this, index ) );
                this.registerSfx( jqOption );
                jqOptionList.append( jqOption );
            }
        }  

            private onOptionListItem_Click( index: number, jqEvent: JQuery.Event ): void
            {
                let jqOption: JQuery = jQuery( jqEvent.currentTarget );

                this._arrValue[ index ] = ( jqOption.data( "id" ) as number ).toString();

                const kTitle: string = ServiceLocator.cardDb.find( this._arrValue[ index ] ).name;
                this._jqRoot.find( ".value>input" ).eq( index ).val( kTitle );

                this._jqRoot.find( "#campaignLogAddRowBtn" ).prop( "disabled", !this.validateValues() );
            }

    private onFieldValue_FocusIn( index: number, jqEvent: JQuery.Event ): void
    {
        this.clearCoincidenceRequestTimeout();

        let jqFieldValue: JQuery = jQuery( jqEvent.currentTarget );
        jqFieldValue.select();

        const kSearchText: string = ( jqFieldValue.val() as string ).trim();
        if ( kSearchText.length >= 3 )
        {
            this._coincidenceRequestTimeout = window.setTimeout( this.onCoincidenceRequest_Timeout.bind( this, index, kSearchText.toLowerCase() ), 1000 );
        }
    }

    private onFieldValue_FocusOut( index: number, jqEvent: JQuery.Event ): void
    {
        this.clearCoincidenceRequestTimeout();

        let jqFieldValue: JQuery = jQuery( jqEvent.currentTarget );

        let title: string = "";
        if ( this._arrField[ index ].isOptional && jqFieldValue.val() as string == "" )
        {
            this._arrValue[ index ] = "";
        }
        else if ( this._arrValue[ index ] != "" )
        {
            title = ServiceLocator.cardDb.find( this._arrValue[ index ] ).name;
        }
        jqFieldValue.val( title );
    }

    private onAddBtn_Click(): void
    {
        this._onRowAdded.dispatch( this._arrValue );
    }

    // #endregion //
}

export interface IAddRowField
{
    title: string;
    type: add_row_value_type;
    isOptional?: boolean;
}

export const enum add_row_value_type
{
    HERO = 0,
    BOON,
    BURDEN
}