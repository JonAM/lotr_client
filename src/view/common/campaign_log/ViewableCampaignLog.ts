import CampaignLogController from "./CampaignLogController";


export default class ViewableCampaignLog extends CampaignLogController
{
    // #region Methods //

    // public:

    public constructor()
    {
        super();
    }

    public init(): void
    {
        super.init();

        this._view.root.find( "button.add" ).hide();
        this._view.root.find( "#campaignLogThreatPenalty" ).prop( "disabled", true );
        this._view.root.find( "#campaignLogNotes" ).prop( "disabled", true );
    }

    // #endregion //
}