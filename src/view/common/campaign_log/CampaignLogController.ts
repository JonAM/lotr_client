import { StringLiteralType } from "typescript";
import ServiceLocator from "../../../ServiceLocator";
import CampaignLogVO from "../../../vo/CampaignLogVO";
import View from "../../View";
import CampaignLogView from "../CampaignLogView";

export default class CampaignLogController
{
    // #region Attributes //

    // protected:

    protected _view: View = null;
    protected _campaignLogVO: CampaignLogVO = null;
    
    // #endregion //


    // #region Properties //

    public set view( value: View ) { this._view = value; }
    public set campaignLogVO( value: CampaignLogVO ) { this._campaignLogVO = value; }

    // #endregion //


    // #region Methods //

    // public:

    public init(): void
    {
        console.assert( this._view != null, "CampaignLogController.ts :: init() :: this._view cannot be null." );
    
        // Attach UI event handlers.
        this._view.root.find( "#campaignLogPrevPageBtn" ).on( "click", this.onPrevPageBtn_Click.bind( this ) );
        this._view.root.find( "#campaignLogNextPageBtn" ).on( "click", this.onNextPageBtn_Click.bind( this ) );
        this._view.root.find( "#campaignLogCloseBtn" ).on( "click", this.onCloseBtn_Click.bind( this ) );
    
        // Init components.
        this._view.root.find( "#campaignLogTitle" ).text( this._campaignLogVO.title );
        
        // First page.
        // Player heroes.
        this._view.root.find( "#campaignLogPlayerId" ).text( jQuery.i18n( "PLAYER_(ID)" ).replace( "#", this._campaignLogVO.playerId ) );
        let jqPlayerHeroes: JQuery<HTMLElement> = this._view.root.find( "#campaignLogPlayerHeroes" );
        for ( let heroId of this._campaignLogVO.playerHeroes )
        {
            jqPlayerHeroes.append( this.createRow( ServiceLocator.cardDb.find( heroId ).name, campaing_log_row_type.PLAYER_HEROES ) );
        }
        this.fillEmptyRows( 4, jqPlayerHeroes, campaing_log_row_type.PLAYER_HEROES );
        // Ally heroes.
        this._view.root.find( "#campaignLogAllyId" ).text( this._campaignLogVO.allyId ? jQuery.i18n( "ALLY_(ID)" ).replace( "#", this._campaignLogVO.allyId ) : jQuery.i18n( "ALLY" ) );
        let jqAllyHeroes: JQuery<HTMLElement> = this._view.root.find( "#campaignLogAllyHeroes" );
        for ( let heroId of this._campaignLogVO.allyHeroes )
        {
            jqAllyHeroes.append( this.createRow( ServiceLocator.cardDb.find( heroId ).name, campaing_log_row_type.ALLY_HEROES ) );
        }
        this.fillEmptyRows( 4, jqAllyHeroes, campaing_log_row_type.ALLY_HEROES );
        // Fallen heroes.
        let jqFallenHeroes: JQuery<HTMLElement> = this._view.root.find( "#campaignLogFallenHeroes" );
        for ( let heroId of this._campaignLogVO.fallenHeroes )
        {
            jqFallenHeroes.append( this.createRow( ServiceLocator.cardDb.find( heroId ).name , campaing_log_row_type.FALLEN_HEROES ) );
        }
        this.fillEmptyRows( 4, jqFallenHeroes, campaing_log_row_type.FALLEN_HEROES );
        //
        this._view.root.find( "#campaignLogThreatPenalty" ).val( this._campaignLogVO.threatPenalty.toString() );
        this._view.root.find( "#campaignLogNotes" ).val( this._campaignLogVO.notes );
        
        // Second page.
        // Scenarios completed.
        let totalScore: number = 0;
        let jqScenarios: JQuery<HTMLElement> = this._view.root.find( "#campaignLogScenarios" );
        for ( let completedScenario of this._campaignLogVO.completedScenarios )
        {
            let jqRow: JQuery<HTMLElement> = this.createSplitRow(
                ServiceLocator.scenarioDb.findScenario( completedScenario.id ).encounter_set.main,
                completedScenario.score.toString(),
                campaing_log_row_type.SCENARIOS );
            jqScenarios.append( jqRow );

            totalScore += completedScenario.score;
        }
        this.fillEmptyRows( 9, jqScenarios, campaing_log_row_type.SCENARIOS );
        this._view.root.find( "#campaignLogTotalScore" ).text( totalScore.toString() );
        // Boons.
        let jqBoons: JQuery<HTMLElement> = this._view.root.find( "#campaignLogBoons" );
        for ( let boon of this._campaignLogVO.boons )
        {
            let jqRow: JQuery<HTMLElement> = this.createSplitRow(
                ServiceLocator.cardDb.find( boon.id ).name,
                ( boon.permanentHeroId != "" ? ServiceLocator.cardDb.find( boon.permanentHeroId ).name : "" ),
                campaing_log_row_type.BOONS );
            jqBoons.append( jqRow );
        }
        this.fillEmptyRows( 4, jqBoons, campaing_log_row_type.BOONS );
        // Burdens.
        let jqBurdens: JQuery<HTMLElement> = this._view.root.find( "#campaignLogBurdens" );
        for ( let burden of this._campaignLogVO.burdens )
        {
            let jqRow: JQuery<HTMLElement> = this.createSplitRow(
                ServiceLocator.cardDb.find( burden.id ).name,
                ( burden.permanentHeroId != "" ? ServiceLocator.cardDb.find( burden.permanentHeroId ).name : "" ),
                campaing_log_row_type.BURDENS );
            jqBurdens.append( jqRow );
        }
        this.fillEmptyRows( 4, jqBurdens, campaing_log_row_type.BURDENS );
        // Footer.
        this._view.root.find( "#campaignLogSecondPage" ).hide();
        this._view.root.find( "#campaignLogPrevPageBtn" ).addClass( "invisible" );
        this._view.root.find( "#campaignLogPageNumber" ).text( jQuery.i18n( "PAGE_NUMBER" ).replace( "#", "1" ) );
    }

        // virtual.
        protected createRow( title: string, rowType: campaing_log_row_type ): JQuery<HTMLElement>
        {
            return jQuery(
                "<div class=\"row\">" 
                    + "<div class=\"title gb-text-body-normal gb-text-ellipsis\">" + title + "</div>"
                + "</div>" );
        }

        // virtual.
        protected createSplitRow( first: string, second: string, rowType: campaing_log_row_type ): JQuery<HTMLElement>
        {
            return jQuery(
                "<div class=\"split_row\">"
                    + "<div class=\"left\">" 
                        + "<div class=\"title gb-text-body-normal gb-text-ellipsis\">" + first + "</div>"
                    + "</div>"
                    + "<div class=\"right gb-text-body-normal gb-text-ellipsis\">" + second + "</div>"
                + "</div>" );
        }

        protected fillEmptyRows( minRowCount: number, jqRowContainer: JQuery<HTMLElement>, rowType: campaing_log_row_type ): void
        {
            const kEmptyRowHtml: string = rowType <= campaing_log_row_type.FALLEN_HEROES 
                ? "<div class=\"row empty\"></div>" 
                : "<div class=\"split_row empty\">"
                        + "<div class=\"left\"></div>"
                        + "<div class=\"right\"></div>"
                    + "</div>";

            let emptyRowCount: number = minRowCount - jqRowContainer.children().length;
            if ( emptyRowCount > 0 )
            {
                for ( let i: number = 0; i < emptyRowCount; ++i )
                {
                    jqRowContainer.append( jQuery( kEmptyRowHtml ) );
                }
            }
        }

    public end(): void
    {
        this._view = null;
        this._campaignLogVO = null;
    }

    // #endregion //


    // #region Input Callbacks //

    private onPrevPageBtn_Click(): void
    {
        this._view.root.find( "#campaignLogFirstPage" ).show();
        this._view.root.find( "#campaignLogSecondPage" ).hide();
        
        this._view.root.find( "#campaignLogPrevPageBtn" ).addClass( "invisible" );
        this._view.root.find( "#campaignLogNextPageBtn" ).removeClass( "invisible" );

        this._view.root.find( "#campaignLogPageNumber" ).text( jQuery.i18n( "PAGE_NUMBER" ).replace( "#", "1" ) );
    }

    private onNextPageBtn_Click(): void
    {
        this._view.root.find( "#campaignLogFirstPage" ).hide();
        this._view.root.find( "#campaignLogSecondPage" ).show();
        
        this._view.root.find( "#campaignLogPrevPageBtn" ).removeClass( "invisible" );
        this._view.root.find( "#campaignLogNextPageBtn" ).addClass( "invisible" );
        
        this._view.root.find( "#campaignLogPageNumber" ).text( jQuery.i18n( "PAGE_NUMBER" ).replace( "#", "2" ) );
    }

    // virtual.
    protected onCloseBtn_Click(): void
    {
        ( this._view as CampaignLogView ).onClosed.dispatch();
        
        ServiceLocator.viewManager.fadeOut( this._view );
    }

    // #endregion //
}

export const enum campaing_log_row_type
{
    PLAYER_HEROES = 0,
    ALLY_HEROES,
    FALLEN_HEROES,
    SCENARIOS,
    BOONS,
    BURDENS
}