import { player_action_type } from "../../../service/socket_io/GameSocketIOController";
import { view_layer_id } from "../../../service/ViewManager";
import ServiceLocator from "../../../ServiceLocator";
import AddCampaignLogRowView, { add_row_value_type, IAddRowField } from "./AddCampaignLogRowView";
import CampaignLogController, { campaing_log_row_type } from "./CampaignLogController";
import SignalBinding from "../../../lib/signals/SignalBinding";


export default class EditableCampaignLog extends CampaignLogController
{
    // #region Attributes //

    // private:

    private _addRowView: AddCampaignLogRowView = null;

    // Binded functions.
    private _bfOnDocument_Click: any = null;

    // Constants.
    private readonly _kArrRowContainerId: Array<string> = [ 
        "#campaignLogPlayerHeroes", "#campaignLogAllyHeroes", "#campaignLogFallenHeroes",
        "#campaignLogScenarios", "#campaignLogBoons", "#campaignLogBurdens" ];
    private readonly _kArrVoAttributeId: Array<string> = [ 
        "_arrPlayerHeroId", "_arrAllyHeroId", "_arrFallenHeroId",
        "_arrCompletedScenario", "_arrBoon", "_arrBurden" ];
    
    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();

        // Binded functions.
        this._bfOnDocument_Click = this.onDocument_Click.bind( this );  
    }

    public init(): void
    {
        super.init();

        // Attach UI event handlers.
        this._view.root.find( "#campaignLogAddPlayerHeroBtn" ).on( "click", this.onAddRowBtn_Click.bind( this, campaing_log_row_type.PLAYER_HEROES ) );
        this._view.root.find( "#campaignLogAddAllyHeroBtn" ).on( "click", this.onAddRowBtn_Click.bind( this, campaing_log_row_type.ALLY_HEROES ) );
        this._view.root.find( "#campaignLogAddFallenHeroBtn" ).on( "click", this.onAddRowBtn_Click.bind( this, campaing_log_row_type.FALLEN_HEROES ) );
        this._view.root.find( "#campaignLogAddBoonBtn" ).on( "click", this.onAddRowBtn_Click.bind( this, campaing_log_row_type.BOONS ) );
        this._view.root.find( "#campaignLogAddBurdenBtn" ).on( "click", this.onAddRowBtn_Click.bind( this, campaing_log_row_type.BURDENS ) );
        this._view.root.find( "#campaignLogNotes" ).on( "focusout", this.onNotes_FocusOut.bind( this ) );
        this._view.root.find( "#campaignLogThreatPenalty" ).on( "focusout", this.onThreatPenalty_FocusOut.bind( this ) );
    }

    // overrides.
    protected createRow( title: string, rowType: campaing_log_row_type ): JQuery<HTMLElement>
    {
        let jqRow: JQuery<HTMLElement> = super.createRow( title, rowType );

        let jqRemoveBtn: JQuery<HTMLElement> = jQuery( "<button class=\"remove sfx_btn_click\"></button>" );
        jqRow.append( jqRemoveBtn );
        jqRemoveBtn.on( "click", this.onRemoveRowBtn_Click.bind( this, jqRow, rowType ) );

        return jqRow;
    }

    // overrides.
    protected createSplitRow( first: string, second: string, rowType: campaing_log_row_type ): JQuery<HTMLElement>
    {
        let jqSplitRow: JQuery<HTMLElement> = super.createSplitRow( first, second, rowType );

        if ( rowType != campaing_log_row_type.SCENARIOS )
        {
            let jqRemoveBtn: JQuery<HTMLElement> = jQuery( "<button class=\"remove sfx_btn_click\"></button>" );
            jqSplitRow.find( ".left" ).append( jqRemoveBtn );
            jqRemoveBtn.on( "click", this.onRemoveRowBtn_Click.bind( this, jqSplitRow, rowType ) );
        }

        return jqSplitRow;
    }

    public addRow( arrValue: Array<string>, rowType: campaing_log_row_type ): void
    {
        if ( rowType == campaing_log_row_type.SCENARIOS ) { return; }

        let jqRowContainer: JQuery<HTMLElement> = this._view.root.find( this._kArrRowContainerId[ rowType ] );
        if ( rowType <= campaing_log_row_type.FALLEN_HEROES )
        {
            this._campaignLogVO[ this._kArrVoAttributeId[ rowType ] ].push( arrValue[ 0 ] );

            jqRowContainer.find( ".row.empty" ).remove();
            jqRowContainer.append( 
                this.createRow( ServiceLocator.cardDb.find( arrValue[ 0 ] ).name, rowType ) );
        }
        else
        {
            this._campaignLogVO[ this._kArrVoAttributeId[ rowType ] ].push( { id: arrValue[ 0 ], permanentHeroId: arrValue[ 1 ] } );

            jqRowContainer.find( ".split_row.empty" ).remove();
            jqRowContainer.append( this.createSplitRow(
                ServiceLocator.cardDb.find( arrValue[ 0 ] ).name,
                ( arrValue[ 1 ] != "" ? ServiceLocator.cardDb.find( arrValue[ 1 ] ).name : "" ),
                rowType ) );
        }
        this.fillEmptyRows( 4, jqRowContainer, rowType );
    }

    public removeRow( rowIndex: number, rowType: campaing_log_row_type ): void
    {
        if ( rowType == campaing_log_row_type.SCENARIOS ) { return; }

        let jqRowContainer: JQuery<HTMLElement> = this._view.root.find( this._kArrRowContainerId[ rowType ] );
        jqRowContainer.eq( rowIndex ).remove();
        this.fillEmptyRows( 4, jqRowContainer, rowType );
    }

    public setNotes( notes: string ): void
    {
        this._view.root.find( "#campaignLogNotes" ).val( notes );
    }

    public setThreatPenalty( threatPenalty: number ): void
    {
        this._view.root.find( "#campaignLogThreatPenalty" ).val( threatPenalty );
    }

    // #endregion //


    // #region Input Callbacks //

    private onDocument_Click( event: Event ): void
    {
        let jqTarget: JQuery<HTMLElement> = jQuery( event.target );
        if ( jqTarget.closest( this._addRowView.root ).length == 0 ) 
        {
           this.clearAddRowPopup();
        }
        else 
        {
            this._addRowView.root.find( ".option_list" ).empty();
        }
    }

        private clearAddRowPopup(): void
        {
            ServiceLocator.viewManager.remove( this._addRowView );
            this._addRowView = null;
            
            // Cleanup events.
            document.removeEventListener( "click", this._bfOnDocument_Click, false );
        }

    private onAddRowBtn_Click( rowType: campaing_log_row_type, jqEvent: JQuery.Event ): void
    {
        if ( this._addRowView )
        {
            this.clearAddRowPopup();
        }

        const kArrAddRowCssClass: Array<string> = [ 
            "player_hero", "ally_hero", "fallen_hero",
            null, "boon", "burden" ];
        const kArrAddRowLocaleId: Array<string> = [ 
            "HERO", "HERO", "HERO",
            null, "BOON", "BURDEN" ];
        const kArrAddRowValueType: Array<add_row_value_type> = [ 
            add_row_value_type.HERO, add_row_value_type.HERO, add_row_value_type.HERO,
            null, add_row_value_type.BOON, add_row_value_type.BURDEN ];

        this._addRowView = new AddCampaignLogRowView();
        let arrAddRowField: Array<IAddRowField> = [ { title: jQuery.i18n( kArrAddRowLocaleId[ rowType ] ), type: kArrAddRowValueType[ rowType ] } ];
        if ( rowType == campaing_log_row_type.BOONS || rowType == campaing_log_row_type.BURDENS )
        {
            arrAddRowField.push( { title: jQuery.i18n( "HERO" ), type: add_row_value_type.HERO, isOptional: true } );
        }
        this._addRowView.fields = arrAddRowField;
        let sb: SignalBinding = this._addRowView.onRowAdded.addOnce( this.onRow_Added, this );
        sb.params = [ rowType ];
        this._addRowView.init();
        ServiceLocator.viewManager.add( this._addRowView, view_layer_id.POPUP );
        this._view.root.find( "#campaignLogAddRow" )
            .removeClass()
            .addClass( "add_row " + kArrAddRowCssClass[ rowType ] )
            .append( this._addRowView.root );

        // Listen to events.
        // IMPORTANT: We cannot use jQuery since the event propagation becomes unpredictable ???
        window.setTimeout( () => { document.addEventListener( "click", this._bfOnDocument_Click, false ); } );
    }

        private onRow_Added( rowType: campaing_log_row_type, arrValue: Array<string> ): void
        {
            this.addRow( arrValue, rowType );

            this.clearAddRowPopup();
        
            // Multiplayer.
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.CAMPAIGN_LOG_ADD_ROW, null, [ arrValue, rowType ] );
        }

    private onRemoveRowBtn_Click( jqRow: JQuery<HTMLElement>, rowType: campaing_log_row_type): void
    {
        const kRowIndex: number = jqRow.parent().children().index( jqRow );
        this._campaignLogVO[ this._kArrVoAttributeId[ rowType ] ].splice( kRowIndex, 1 );
        jqRow.remove();
        let jqRowContainer: JQuery<HTMLElement> = this._view.root.find( this._kArrRowContainerId[ rowType ] );
        this.fillEmptyRows( 4, jqRowContainer, rowType );

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.CAMPAIGN_LOG_REMOVE_ROW, null, [ kRowIndex, rowType ] );
    }

    // overrides.
    protected onCloseBtn_Click(): void
    {
        if ( this._addRowView )
        {
            this.clearAddRowPopup();
        }

        super.onCloseBtn_Click();
    }

    private onNotes_FocusOut(): void
    {
        // Multiplayer.
        const kNotes: string = this._view.root.find( "#campaignLogNotes" ).val() as string;
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.CAMPAIGN_LOG_SET_NOTES, null, [ kNotes ] );
    }

    private onThreatPenalty_FocusOut(): void
    {
        // Multiplayer.
        const kThreatPenalty: number = this._view.root.find( "#campaignLogThreatPenalty" ).val() as number;
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.CAMPAIGN_LOG_SET_THREAT_PENALTY, null, [ kThreatPenalty ] );
    }

    // #endregion //
}