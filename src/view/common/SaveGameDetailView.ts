import WindowedView from "../WindowedView";

import ServiceLocator from "../../ServiceLocator";
import Session from "../../Session";
import Utils from "../../Utils";
import { view_layer_id } from "../../service/ViewManager";

import SaveGameVO from "../../vo/SaveGameVO";
import Signal from "../../lib/signals/Signal";
import Path from "../../Path";
import Config from "../../Config";
import { ISaveGame } from "../../view/game/SaveGameView";
import { IScenario } from "../../game/ScenarioDB";


export default class SaveGameDetailView extends WindowedView
{
    // #region Attributes //

    // private:

    private _saveGameVO: SaveGameVO = null;

    // Constants.
    private static readonly _kResId: string = "common-save_game_detail";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    public set saveGameVO( value: SaveGameVO ) { this._saveGameVO = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( SaveGameDetailView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#saveGameDetailBackBtn" ).on( "click", this.onBackBtn_Click.bind( this ) );
    }

    public init(): void
    {
        console.assert( this._saveGameVO != null, "SaveGameDetailView.ts :: init() :: this._saveGameVO cannot be null." );

        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ { id: SaveGameDetailView.kResId, isPreloadImages: true } ] } );

        this._jqRoot.find( "#saveGameDetailTitle" ).text( this._saveGameVO.title );

        const kSaveGame: ISaveGame = JSON.parse( this._saveGameVO.content );
        this.initHeroes( kSaveGame.session.playerDeck.hero, this._jqRoot.find( "#saveGameDetailPlayerHeroes" ) );
        if ( kSaveGame.session.allyId )
        {
            this.initHeroes( kSaveGame.session.allyDeck.hero, this._jqRoot.find( "#saveGameDetailAllyHeroes" ) );
        }
        else
        {
            this._jqRoot.find( "#saveGameDetailAlly" ).hide();
        }

        const kScenarioInfo: IScenario = ServiceLocator.scenarioDb.findScenario( kSaveGame.session.scenarioId );
        this._jqRoot.find( ".scenario>img" ).prop( "src", Path.kResImages + "common/scenario/" + kScenarioInfo.product_icon + ".png" );
        this._jqRoot.find( ".scenario>.detail>.title" ).text( kScenarioInfo.name ? kScenarioInfo.name : kScenarioInfo.encounter_set.main );
        this._jqRoot.find( ".scenario>.detail>.set" ).text( ServiceLocator.scenarioDb.find( kSaveGame.session.scenarioId.substr( 0, 2 ) ).name );

        if ( kSaveGame.session.allyId )
        {
            this._jqRoot.find( ".detail>.multiplayer>.ally" ).text( kSaveGame.session.allyId );
            this._jqRoot.find( ".detail>.single_player" ).hide();
        }
        else
        {
            this._jqRoot.find( ".detail>.multiplayer" ).hide();
        }

        if ( kScenarioInfo.type == "nightmare" )
        {
            this._jqRoot.find( ".scenario" ).addClass( "nightmare" );
        }

        this._jqRoot.find( "#saveGameDetailRound" ).text( jQuery.i18n( "ROUND_N" ).replace( "#", kSaveGame.gameState.roundCount.toString() ) );
    }

        private initHeroes( arrHeroId: Array<string>, jqTarget: JQuery<HTMLElement> ): void
        {
            let heroesHtml: string = "";
            for ( let heroId of arrHeroId )
            {
                heroesHtml += "<div class=\"gb-text-body-normal gb-text-ellipsis\">" + ServiceLocator.cardDb.find( heroId ).name + "</div>";
            }

            jqTarget.append( heroesHtml );
        }

    // #endregion //


    // #region Input Callbacks //

    private onBackBtn_Click(): void
    {
        ServiceLocator.viewManager.fadeOut( this );
    }

    // #endregion //
}