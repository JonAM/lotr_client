import WindowedView from "../WindowedView";

import ServiceLocator from "../../ServiceLocator";
import Utils from "../../Utils";
import { view_layer_id } from "../../service/ViewManager";

import InfoMessageView from "./popup/InfoMessageView";


export default class PasswordResetView extends WindowedView
{
    // #region Attributes //

    // private:

    private _username: string = null;
    private _passwordHash: string = null;
    
    // Constants.
    private static readonly _kResId: string = "common-password_reset";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    public set username( value: string ) { this._username = value; }
    public set passwordHash( value: string ) { this._passwordHash = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( PasswordResetView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#passwordResetAcceptBtn" ).on( "click", this.onAcceptBtn_Click.bind( this ) );
        this._jqRoot.find( "#passwordResetCancelBtn" ).on( "click", this.onCancelBtn_Click.bind( this ) );
    }

    public init(): void
    {
        console.assert( this._username != null, "PasswordResetView.ts :: init() :: this._username cannot be null." );
        console.assert( this._passwordHash != null, "PasswordResetView.ts :: init() :: this._passwordHash cannot be null." );

        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ { id: PasswordResetView.kResId, isPreloadImages: true } ] } );
    }

    // #endregion //


    // #region Input Callbacks //

    private onAcceptBtn_Click(): void
    {
        const kPassword: string = ( this._jqRoot.find( "#passwordResetPassword" ).val() as string ).substr( 0, 20 ).trim();
        const kRePassword: string = ( this._jqRoot.find( "#passwordResetRePassword" ).val() as string ).substr( 0, 20 ).trim();
        
        let result: boolean =  kPassword.length > 0 && kRePassword.length > 0;
        if ( !result )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "ALL_FIELDS_REQUIRED" ), view_layer_id.POPUP );
        }

        // Format validations.
        if ( result )
        {
            // Validate password.
            const kPasswordRegEx: RegExp = /^[^ ]+$/;
            result = kPasswordRegEx.test( kPassword );
            if ( !result )
            {
                Utils.web.showInfoMessage( jQuery.i18n( "INVALID_PASSWORD_FORMAT" ), view_layer_id.POPUP );
            }

            if ( result )
            {
                // Validate repeated password.
                result = kPassword == kRePassword;
                if ( !result )
                {
                    Utils.web.showInfoMessage( jQuery.i18n( "NO_PASSWORD_MATCH" ), view_layer_id.POPUP );
                }
            }
        }

        if ( result )
        {
            this._jqLoadingAnimation.show();

            ServiceLocator.dbConnector.post( { 
                serviceName: "UserDAO",
                methodName: "resetPassword",
                parameters: [ kPassword, this._username, this._passwordHash ] },
                this.onDbResetPassword_Result.bind( this ) );
        }
    }

    private onCancelBtn_Click(): void
    {
        ServiceLocator.viewManager.fadeOut( this );
    }

    // #endregion //


    // #region DB Callbacks /

    private onDbResetPassword_Result( result: number ): void
    {
        if ( this._id == null ) { return; }

        switch ( result )
        {
            case 0:
            {
                let im: InfoMessageView = Utils.web.showInfoMessage( jQuery.i18n( "PASSWORD_CHANGED" ), view_layer_id.POPUP );
                im.onClosed.addOnce( () => { this.onCancelBtn_Click(); } );
                break;
            }

            case 1:
            {
                let im: InfoMessageView = Utils.web.showInfoMessage( jQuery.i18n( "INVALID_PASSWORD_RESET_LINK" ), view_layer_id.POPUP );
                im.onClosed.addOnce( () => { this.onCancelBtn_Click(); } );
                break;
            }

            case 10:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
                break;
            }
        }

        this._jqLoadingAnimation.hide();
    }

    // #endregion //
}