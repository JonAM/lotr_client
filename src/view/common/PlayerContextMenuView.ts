import View from "../View";

import { IFileLoadInfo } from "../../lib/HtmlFileLoader";

import Signal from "../../lib/signals/Signal";
import { user_status_type } from "../../service/socket_io/GatewaySocketIOController";
import Session from "../../Session";


export default class PlayerContextMenuView extends View
{
    // #region Attributes //

    // private:
    
    // Signals.
    private _onInviteSelected: Signal = new Signal();
    private _onSendMessageSelected: Signal = new Signal();
    private _onViewProfileSelected: Signal = new Signal();
    private _onReportSelected: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "common-player_context_menu";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    // Signals.
    public get onInviteSelected(): Signal { return this._onInviteSelected; }
    public get onSendMessageSelected(): Signal { return this._onSendMessageSelected; }
    public get onViewProfileSelected(): Signal { return this._onViewProfileSelected; }
    public get onReportSelected(): Signal { return this._onReportSelected; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( PlayerContextMenuView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#pcmInviteBtn" ).on( "click", this.onInviteBtn_Click.bind( this ) );
        this._jqRoot.find( "#pcmSendMessageBtn" ).on( "click", this.onSendMessageBtn_Click.bind( this ) );
        this._jqRoot.find( "#pcmViewProfileBtn" ).on( "click", this.onViewProfileBtn_Click.bind( this ) );
        this._jqRoot.find( "#pcmReportBtn" ).on( "click", this.onReportBtn_Click.bind( this ) );
    }

    public init(): void
    {
        super.init();

        let arrHtmlFileInfo: Array<IFileLoadInfo> = [  
            // 1st level.
            { id: PlayerContextMenuView.kResId, isPreloadImages: true } ];
        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: arrHtmlFileInfo } );
    }

    public end(): void
    {
        // Owned signals.
        this._onInviteSelected.removeAll();
        this._onSendMessageSelected.removeAll();
        this._onViewProfileSelected.removeAll();
        this._onReportSelected.removeAll();

        super.end();
    }

    public showAt( clientX: number, clientY: number ): void
    {
        let left: number = clientX;
        if ( clientX + this._jqRoot.width() >= jQuery( window ).width() )
        {
            left -= this._jqRoot.width();
        }
        let top: number = clientY;
        if ( clientY - this._jqRoot.height() > 0 )
        {
            top -= this._jqRoot.height();
        }
        this._jqRoot.css( { left: left, top: top } );
        this._jqRoot.show();
    }

    public onUserStatusUpdated( userStatus: user_status_type, userId: string, myUserStatus: user_status_type ): void
    {
        if ( userId == Session.playerId )
        {
            this._jqRoot.find( "#pcmInviteBtn" ).prop( "disabled", true );
            this._jqRoot.find( "#pcmSendMessageBtn" ).prop( "disabled", true );
            this._jqRoot.find( "#pcmReportBtn" ).prop( "disabled", true );  
        }
        else
        {
            let isChallengeDisabled: boolean = userStatus != user_status_type.IDLE || myUserStatus != user_status_type.IDLE;
            this._jqRoot.find( "#pcmInviteBtn" ).prop( "disabled", isChallengeDisabled );
            this._jqRoot.find( "#pcmSendMessageBtn" ).prop( "disabled", userStatus == user_status_type.DISCONNECTED );
            this._jqRoot.find( "#pcmReportBtn" ).prop( "disabled", false );  
        }
    }

    // #endregion //


    // #region Input Callbacks //

    private onInviteBtn_Click(): void
    {
        this._onInviteSelected.dispatch();
    }

    private onSendMessageBtn_Click(): void
    {
        this._onSendMessageSelected.dispatch();
    }

    private onViewProfileBtn_Click(): void
    {
        this._onViewProfileSelected.dispatch();
    }

    private onReportBtn_Click(): void
    {
        this._onReportSelected.dispatch();
    }

    // #endregion //
}