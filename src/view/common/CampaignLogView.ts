import WindowedView from "../WindowedView";

import CampaignLogVO from "../../vo/CampaignLogVO";
import CampaignLogController from "./campaign_log/CampaignLogController";
import EditableCampaignLog from "./campaign_log/EditableCampaignLog";
import ViewableCampaignLog from "./campaign_log/ViewableCampaignLog";
import { IFileLoadInfo } from "../../lib/HtmlFileLoader";
import AddCampaignLogRowView from "./campaign_log/AddCampaignLogRowView";
import Signal from "../../lib/signals/Signal";


export default class CampaignLogView extends WindowedView
{
    // #region Attributes //

    // private:

    private _isEditable: boolean = false;
    private _campaignLogVO: CampaignLogVO = null;

    private _controller: CampaignLogController = null;

    // Signals.
    private _onClosed: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "common-campaign_log";
    
    // #endregion //


    // #region Properties //

    public get controller(): CampaignLogController { return this._controller; }
    public static get kResId(): string { return this._kResId; }

    public set isEditable( value: boolean ) { this._isEditable = value; }
    public set campaignLogVO( value: CampaignLogVO ) { this._campaignLogVO = value; }

    // Signals.
    public get onClosed(): Signal { return this._onClosed; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( CampaignLogView._kResId );
    }

    public init(): void
    {
        super.init();

        let arrHtmlFileInfo: Array<IFileLoadInfo> = [ { id: CampaignLogView.kResId, isPreloadImages: true } ];
        if ( this._isEditable )
        {
            arrHtmlFileInfo.push( { id: AddCampaignLogRowView.kResId, isPreloadImages: false } );
        }
        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: arrHtmlFileInfo } );

        this._controller = this._isEditable ? new EditableCampaignLog() : new ViewableCampaignLog();
        this._controller.view = this;
        this._controller.campaignLogVO = this._campaignLogVO;
        this._controller.init();
    }

    public end(): void
    {
        this._onClosed.removeAll();

        this._controller.end();
        this._controller = null;

        super.end();
    }

    // #endregion //
}