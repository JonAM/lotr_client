import View from "../View";

import { IFileLoadInfo } from "../../lib/HtmlFileLoader";
import Path from "../../Path";


export default class FullScreenView extends View
{
    // #region Attributes //

    // Constants.
    private static readonly _kResId: string = "common-full_screen";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( FullScreenView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#fsToggleBtn" ).on( "click", this.onToggleBtn_Click.bind( this ) );
        
    }

    public init(): void
    {
        super.init();

        let arrHtmlFileInfo: Array<IFileLoadInfo> = [  
            // 1st level.
            { id: FullScreenView.kResId, isPreloadImages: true } ];
        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: arrHtmlFileInfo } );
    }

    // #endregion //


    // #region Input Callbacks //

    private onToggleBtn_Click(): void
    {
        let jqToggleBtn: JQuery<HTMLElement> = this._jqRoot.find( "#fsToggleBtn" );

        if ( jqToggleBtn.hasClass( "on" ) )
        {   
            let docElement: any = document.documentElement;
            if ( docElement.requestFullscreen ) 
            {
                docElement.requestFullscreen();
            }
        }
        else
        {
            document.exitFullscreen();
        }
        jqToggleBtn.toggleClass( "on" );

        jqToggleBtn.find( ".icon" ).prop( "src", Path.kResImages + "common/" + ( jqToggleBtn.hasClass( "on" ) ? "enter_full_screen" : "exit_full_screen" ) + ".png" );
    }

    // #endregion //
}