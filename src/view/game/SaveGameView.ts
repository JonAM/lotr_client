import WindowedView from "../WindowedView";

import ServiceLocator from "../../ServiceLocator";
import Session from "../../Session";
import Utils from "../../Utils";
import { view_layer_id } from "../../service/ViewManager";

import SaveGameVO from "../../vo/SaveGameVO";
import InfoMessageView from "../common/popup/InfoMessageView";
import { player_type } from "../../game/component/world/CPlayerArea";
import { combat_state_id, encounter_state_id, game_state_id, quest_state_id } from "../../states/StateId";
import { deck_type } from "../../game/component/ui/right_menu/action_log/target/LogTargetDeck";
import { log_target_type } from "../../game/component/ui/right_menu/action_log/LogTarget";
import { IDeck } from "../gateway/DeckParser";
import { ICard as IDbCard } from "../../game/CardDB";
import GameObject from "../../game/GameObject";
import { lock_type } from "../../game/component/ui/indicator/CIndicatorLock";
import SaveGameDetailView from "../common/SaveGameDetailView";
import { end_turn_btn_state } from "../../game/component/ui/CEndTurnButton";
import { IGamePhase } from "../../game/PhaseNavigator";
import { detail_bar_icon_type } from "../../game/component/card/token/CDetailBar";
import { character_attribute_type } from "../../game/component/ui/CPlayerWillpowerCounter";
import CEngagedActorArea from "../../game/component/world/actor_area/CEngagedActorArea";
import { location_type } from "../../game/component/world/CGameWorld";
import { status_type } from "../../game/component/card/token/CCardTokenSide";
import { ISerializedCampaignLog } from "../../vo/CampaignLogVO";
import { ISerializedTreasureList } from "../../vo/TreasureListVO";


export default class SaveGameView extends WindowedView
{
    // #region Attributes //

    // private:

    private _viewLayerId: view_layer_id = null;

    private _arrSaveGameVO: Array<SaveGameVO> = null;

    // Constants.
    private static readonly _kResId: string = "game-save_game";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    public set viewLayerId( value: view_layer_id ) { this._viewLayerId = value; }
    public set saveGames( value: Array<SaveGameVO> ) { this._arrSaveGameVO = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( SaveGameView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#saveGameBackBtn" ).on( "click", this.onBackBtn_Click.bind( this ) );
        this._jqRoot.find( "#saveGameSaveBtn" ).on( "click", this.onSaveBtn_Click.bind( this ) );
        this._jqRoot.find( "#saveGameTitle" ).on( "keyup", this.onEnterKey_Pressed.bind( this ) );
    }

    public init(): void
    {
        console.assert( this._viewLayerId != null, "SaveGameView.ts :: init() :: this._viewLayerId cannot be null." );

        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ 
                // 1st level.
                { id: SaveGameView.kResId, isPreloadImages: true },
                // 2nd level.
                { id: SaveGameDetailView.kResId, isPreloadImages: false } ],
            isServerDataRequired: true } );

        ServiceLocator.dbConnector.post( { 
            serviceName: "SaveGameDAO",
            methodName: "retrieve",
            parameters: [ Session.playerId ] },
            this.onDbSaveGameRetrieve_Result.bind( this ) );
    }

    // private:

    private createItemHtml( saveGameVO: SaveGameVO ): JQuery<HTMLElement>
    {
        let result: JQuery<HTMLElement> = null;

        result = jQuery( 
            "<div class=\"item gb-custom_pointer sfx_item_click\">" 
                + "<div class=\"title gb-text-body-normal gb-text-ellipsis\">" + saveGameVO.title + "</div>"
                + "<div class=\"created gb-text-body-normal gb-text-ellipsis\">" + jQuery.i18n( "TIME_AGO" ).replace( "#", Utils.web.createElapsedTimeStr( saveGameVO.msSinceCreation ) ) + "</div>"
                + "<button class=\"detail sfx_btn_click\"></button>"
                + "<button class=\"remove sfx_btn_click\"></button>"
            + "</div>" );
        result.find( "button.detail" ).on( "click", this.onDetailBtn_Click.bind( this, result ) );
        result.find( "button" ).on( "click", this.onRemoveBtn_Click.bind( this, result ) );
        result.on( "click", this.onItem_Click.bind( this, result ) );
        
        return result;
    }

    // #endregion //


    // #region Input Callbacks //

    private onItem_Click( jqItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        let jqItems: JQuery<HTMLElement> = this._jqRoot.find( "#saveGameList>.item" );
        const kSelSaveGameVO: SaveGameVO = this._arrSaveGameVO[ jqItems.index( jqItem ) ];

        jqItems.removeClass( "selected" );
        jqItem.addClass( "selected" ); 

        this._jqRoot.find( "#saveGameTitle" ).val( kSelSaveGameVO.title );
    }

    private onRemoveBtn_Click( jqItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        jqEvent.stopPropagation();

        this._jqLoadingAnimation.show();

        const kItemIndex: number = this._jqRoot.find( "#saveGameList>.item" ).index( jqItem );
        const kSaveGameVO: SaveGameVO = this._arrSaveGameVO[ kItemIndex ];

        ServiceLocator.dbConnector.post( { 
            serviceName: "SaveGameDAO",
            methodName: "remove",
            parameters: [ kSaveGameVO.title, Session.playerId, Session.token ] },
            this.onDbSaveGameRemove_Result.bind( this, kItemIndex ) );
    }

    private onDetailBtn_Click( jqItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        jqEvent.stopImmediatePropagation();

        let saveGameDetailView: SaveGameDetailView = new SaveGameDetailView();
        const kItemIndex: number = this._jqRoot.find( "#saveGameList>.item" ).index( jqItem );
        const kSaveGameVO: SaveGameVO = this._arrSaveGameVO[ kItemIndex ];
        saveGameDetailView.saveGameVO = kSaveGameVO;
        saveGameDetailView.init();
        ServiceLocator.viewManager.fadeIn( saveGameDetailView, this._viewLayerId );
    }

    private onBackBtn_Click(): void
    {
        ServiceLocator.viewManager.fadeOut( this );
    }

    private onSaveBtn_Click(): void
    {
        const kTitle: string = ( this._jqRoot.find( "#saveGameTitle" ).val() as string ).trim();
        if ( kTitle.length == 0 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "INVALID_TITLE" ), this._viewLayerId );
        }
        else
        {
            this._jqLoadingAnimation.show();

            const kSaveGame: ISaveGame = {
                session: Session.saveGame(),
                gameState: ServiceLocator.game.saveGame(),
                cardDbCustomEntries: ServiceLocator.cardDb.saveGame(),
                global: {
                    gameObjectOidCounter: GameObject.oidCounter,
                    genericEnemyCounter: CEngagedActorArea.genericEnemyCounter } };

            const kContent: string = JSON.stringify( kSaveGame );
                ServiceLocator.dbConnector.post( { 
                    serviceName: "SaveGameDAO",
                    methodName: "add",
                    parameters: [ 
                        kTitle, 
                        Session.playerId, 
                        Session.allyId, 
                        kContent, 
                        Session.token ] },
                    this.onDbSaveGameAdd_Result.bind( this ) );
        }
    }

    private onEnterKey_Pressed( jqEvent: JQuery.Event ): void
    {
        if ( jqEvent.key == "Enter" )
        {
            jqEvent.preventDefault();

            this.onSaveBtn_Click();
        }
    }

    // #endregion //


    // #region Other Callbacks //

    private onDbSaveGameRetrieve_Result( arrSaveGameVO: Array<SaveGameVO> ): void
    {
        this._arrSaveGameVO = SaveGameVO.parseArray( arrSaveGameVO );
        let jqSaveGameList: JQuery<HTMLElement> = this._jqRoot.find( "#saveGameList" );
        for ( let saveGameVO of this._arrSaveGameVO )
        {
            jqSaveGameList.append( this.createItemHtml( saveGameVO ) );
        }
        this.registerSfx( jqSaveGameList );

        this._viewPreloader.onServerDataLoaded();
    }

    private onDbSaveGameAdd_Result( result: number ): void
    {
        if ( !this._id ) { return; }

        if ( result == 0 )
        {
            let infoMessageView: InfoMessageView = Utils.web.showInfoMessage( jQuery.i18n( "GAME_SAVED" ), this._viewLayerId );
            infoMessageView.onClosed.addOnce( () => { ServiceLocator.viewManager.fadeOut( this ); } );
        }
        else if ( result == 1 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "MAX_CAPACITY_REACHED" ), this._viewLayerId );
        }
        else if ( result == 10 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), this._viewLayerId );
        }
        else if ( result == 11 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "INVALID_USER_TOKEN" ), this._viewLayerId );

            // TODO: Go back to login screen.
        }

        this._jqLoadingAnimation.hide();
    }

    private onDbSaveGameRemove_Result( itemIndex: number, result: number ): void
    {
        if ( !this._id ) { return; }

        if ( result == 0 )
        {
            this._arrSaveGameVO.splice( itemIndex, 1 );
            this._jqRoot.find( "#saveGameList>.item" ).eq( itemIndex ).remove();
        }
        else if ( result == 1 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "SAVE_GAME_NOT_FOUND" ), this._viewLayerId );
        }
        else if ( result == 10 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), this._viewLayerId );
        }
        else if ( result == 11 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "INVALID_USER_TOKEN" ), this._viewLayerId );

            // TODO: Go back to login screen.
        }
        
        this._jqLoadingAnimation.hide();
    }

    // #endregion //
}

export interface ISaveGame
{
    session: ISgSession;
    gameState: ISgGameState;
    cardDbCustomEntries: Array<ISgCardDbEntry>;
    global: ISgGlobal;
}

    export interface ISgSession
    {
        playerId: string;
        allyId: string;
        playerDeck: IDeck;
        allyDeck: IDeck;
        scenarioId: string;
        encounterDeck: Array<string>;
        questDeck: Array<string>;
        setAsideDeck: Array<string>;
        isCampaignMode: boolean;
    }

    export interface ISgGameState
    {
        startingPlayer: player_type;
        firstPlayer: player_type;
        activePlayer: player_type;
        roundCount: number;
        forcedDiscardLocations: string;
        gameWorld: ISgGameWorld;
        phaseNavigator: ISgPhaseNavigator;
        attackBindingManager: ISgAttackBindingManager;
        gamePhaseState: ISgGamePhaseState;
        startTimestamp: number;
    }

        export interface ISgPhaseNavigator
        {
            isAutoSkip: boolean;
            isResPhaseAddToken: boolean;
            isResPhaseDrawCard: boolean;
            isStagingSubphaseDrawCard: boolean;
            gamePhases: Array<IGamePhase>;
        }

        export interface ISgAttackBindingManager
        {
            attackBindings: Array<ISgAttackBinding>;
            mapAttackBindingCounter: Array<[string, ISgAttackBindingCounter]>;
        }

            export interface ISgAttackBinding
            {
                attackerOid: string;
                defenderOid: string;
                isVisible: boolean;
            }

            export interface ISgAttackBindingCounter 
            {
                playerType: player_type;
                defenderOid: string;
                attackerOids: Array<string>;
                count: number;
                isAuto: boolean;
                isDefended: boolean;
                isVisible: boolean;
            }

        export interface ISgGamePhaseState
        {
            stateId: game_state_id;
            resource?: ISgResourceState;
            planning?: ISgPlanningState;
            quest?: ISgQuestState;
            travel?: ISgTravelState;
            encounter?: ISgEncounterState;
            combat?: ISgCombatState;
            refresh?: ISgRefreshState;
        }

            export interface ISgResourceState
            {
                // TODO:
            }

            export interface ISgPlanningState
            {
                // TODO:
            }

            export interface ISgQuestState
            {
                substateId: quest_state_id;
            }

            export interface ISgTravelState
            {
                // TODO:
            }

            export interface ISgEncounterState
            {
                substateId: encounter_state_id;
            }

            export interface ISgCombatState
            {
                substateId: combat_state_id;
            }

            export interface ISgRefreshState
            {
                // TODO:
            }

    export interface ISgCardDbEntry
    {
        id: string;
        info: IDbCard;
    }

    export interface ISgGlobal
    {
        gameObjectOidCounter: number;
        genericEnemyCounter: number;
    }

export interface ISgTokenIndicator
{
    count: number;
    lock?: ISgIndicatorLock;
}

    export interface ISgIndicatorLock
    {
        lockType: lock_type;
        isLocked: boolean;
    }

// From ISgGameState.
export interface ISgGameWorld
{
    playerPlayArea: ISgPlayerPlayArea;
    allyPlayArea: ISgPlayerPlayArea;
    sauronPlayArea: ISgSauronPlayArea;
    actionLogger: ISgActionLogger;
    customPanelManager?: ISgCustomPanelManager;
}

    export interface ISgPlayerPlayArea
    {
        sideControls: ISgPlayerSideControls;
        home: ISgActorArea;
        engaged: ISgActorArea;
    }

        export interface ISgPlayerSideControls
        {
            threat: number;
            deck: ISgCardView;
            discard: ISgCardView;
            hand: ISgCardView;
            setAside: ISgCardView;
            customDeck?: ISgCardView;
            endTurnBtn: ISgEndTurnBtn;
            phaseDiagramButton?: ISgPhaseDiagramButton;
        }

            export interface ISgCardView
            {
                cards: Array<ISgCard>;
            }

                export interface ISgViewContextBtn
                {
                    counter: ISgTokenCounter;
                }

            export interface ISgEndTurnBtn
            {
                state: end_turn_btn_state;
                textLocaleId?: string;
            }

            export interface ISgPhaseDiagramButton
            {
                gameStateId: game_state_id;
                gameSubstateId: number;
            }

    export interface ISgSauronPlayArea
    {
        sideControls: ISgSauronSideControls;
        staging: ISgActorArea;
        questHolder: ISgQuestHolder;
        activeLocationHolder: ISgActiveLocationHolder;
        stagingThreatCounter: ISgStagingThreatCounter;
        playerWillpowerCounter: ISgPlayerWillpowerCounter;
        allyWillpowerCounter: ISgPlayerWillpowerCounter;
        sauronWillpowerCounter: ISgPlayerWillpowerCounter;
        questVariation: ISgQuestVariation;
    }

        export interface ISgSauronSideControls
        {
            deck: ISgCardView;
            secondaryDeck: ISgCardView;
            discard: ISgCardView;
            customDeck: ISgCardView;
            customDiscard: ISgCardView;
            secondaryCustomDeck: ISgCardView;
            secondaryCustomDiscard: ISgCardView;
            questDeck: ISgCardView;
            questDiscard: ISgCardView;
            setAside: ISgCardView;
            removedFromGame: ISgCardView;
            victoryDisplay: ISgCardView;
            tray: ISgCardView;
        }

            export interface ISgActorArea
            {
                isVisible: boolean;
                selTabIndex: number;
                tabs: Array<ISgActorTab>;
                gameModifiers: Array<ISgGameModifier>;
                sauronActorArea?: ISgSauronActorArea;
            }

                export interface ISgActorTab
                {
                    actors: Array<ISgCardToken>;
                }

                export interface ISgGameModifier
                {
                    oid: string;
                    isBowed: boolean;
                    isFaceUp: boolean;
                    isHighlighted: boolean;
                    controller: player_type;
                    detailBar: ISgCardTokenDetailBar;
                }

            export interface ISgSauronActorArea
            {
                isPlayerSplit: boolean;
                isAllySplit: boolean;
            }

        export interface ISgQuestHolder
        {
            questTokens: Array<ISgCardToken>;
            selQuestIndex: number;
        }

        export interface ISgActiveLocationHolder
        {
            locationTokens: Array<ISgCardToken>;
            selLocationIndex: number;
        }

        export interface ISgStagingThreatCounter
        {
            count: number;
            isAuto: boolean;
        }
    
        export interface ISgPlayerWillpowerCounter
        {
            count: number;
            characterAttribute: character_attribute_type;
            isAuto: boolean;
        }

        export interface ISgQuestVariation
        {
            isPlayerWillpowerCounter: boolean;
            isAllyWillpowerCounter: boolean;
            isSauronWillpowerCounter: boolean;
        }

export interface ISgCard
{
    oid: string;
    frontCardId: string;
    backCardId: string;
    isFaceUp: boolean;
    owner: player_type;
    controller: player_type;
    locationData?: string;
    label?: string;
    customCard?: ICustomCard;
    x?: number;
    y?: number;
}

export interface ICustomCard
{
    replacedCardOid: string;
    dbEntry: IDbCard;
}

export interface ISgCardToken
{
    oid: string;
    frontSide: ISgCardTokenSide;
    backSide: ISgCardTokenSide;
    isFaceUp: boolean;
    isBowed: boolean;
    isHighlighted: boolean;
    location: location_type;
    detailBar: ISgCardTokenDetailBar;
    owner: player_type;
    controller: player_type;
    attachments: Array<ISgAttachment>;
}

    export interface ISgCardTokenDetailBar
    {
        items: Array<ISgDetailBarItem>;
    }

        export interface ISgDetailBarItem
        {
            type: detail_bar_icon_type;
            counter?: ISgTokenCounter;
            customCounter?: ISgCustomCounter;
            customText?: string;
            underneath?: Array<ISgCard>;
        }

            export interface ISgTokenCounter
            {
                count: number;
                minCount: number;
                maxCount: number;
            }

            export interface ISgCustomCounter
            {
                counter: ISgTokenCounter;
                tooltip: string;
                iconId: string;
            }

    export interface ISgAttachment
    {
        oid: string;
        frontCardId: string;
        backCardId: string;
        isBowed: boolean;
        isFaceUp: boolean;
        isHighlighted: boolean;
        detailBar: ISgCardTokenDetailBar;
        owner: player_type;
        controller: player_type;
    }

    export interface ISgCardTokenSide
    {
        cardId: string;
        status: Array<status_type>;
        heroSide?: ISgHeroSide;
        allySide?: ISgAllySide;
        enemySide?: ISgEnemySide;
        locationSide?: ISgLocationSide;
        questSide?: ISgQuestSide;
    }

    export interface ISgAllySide
    {
        willpower: number;
        attack: number;
        defense: number;
        health: number;
        wound: number;
        resource?: number;
    }

    export interface ISgEnemySide
    {
        engagementCost: number;
        threat: number;
        attack: number;
        defense: number;
        health: number;
        wound: number;
        shadowCards: Array<ISgCard>;
    }

    export interface ISgHeroSide
    {
        resource: number;
        willpower: number;
        attack: number;
        defense: number;
        health: number;
        wound: number;
    }

    export interface ISgLocationSide
    {
        threat: number;
        questPoints: number;
        progress: number;
    }

    export interface ISgQuestSide
    {
        questPoints: number;
        progress: number;
    }

// From ISgGameWorld.
export interface ISgActionLogger
{
    isLast: boolean;
    history: Array<ISgLogAction>;
    isVisible: boolean;
}

    export interface ISgLogAction
    {
        oid: string;
        who: player_type;
        logActivePlayer?: ISgLogActivePlayer;
        logTargetSelection?: ISgLogTargetSelection;
        logCounter?: ISgLogCounter;
        logPhaseStart?: ISgLogPhaseStart;
        logSequence?: ISgLogSequence;
        logSingleIcon?: ISgLogSingleIcon;
        logCardFlip?: ISgLogCardFlip;
    }

    export interface ISgLogActivePlayer
    {
        playerType: player_type;
    }

    export interface ISgLogTargetSelection
    {
        from: ISgLogTarget;
        to: ISgLogTarget;
        contextIconId: string;
        isReverse: boolean;
    }

        export interface ISgLogTarget
        {
            type: log_target_type;
            logTargetCard?: ISgLogTargetCard;
            logTargetDeck?: ISgLogTargetDeck;
            logTargetPlayer?: ISgLogTargetPlayer;
        }

            export interface ISgLogTargetCard
            {
                oid: string;
                id: string;
                flipSideId: string;
            }

            export interface ISgLogTargetDeck
            {
                deckType: deck_type;
            }

            export interface ISgLogTargetPlayer
            {
                playerType: player_type;
            }

    export interface ISgLogCounter
    {
        target: ISgLogTarget;
        iconId: string;
        count: number;
        total: number;
    }

    export interface ISgLogPhaseStart
    {
        gameStateId: game_state_id;
    }

    export interface ISgLogSequence
    {
        target: ISgLogTarget;
        fromIconId: string;
        toIconId: string;
    }

    export interface ISgLogSingleIcon
    {
        target: ISgLogTarget;
        iconId: string;
    }

    export interface ISgLogCardFlip
    {
        target: ISgLogTarget;
    }

export interface ISgCustomPanelManager
{
    toggleBtnVisibility: Array<boolean>;
    toggleBtnIconIds: Array<string>;
    customPanels: Array<ISgCustomPanel>;
}

    export interface ISgCustomPanel
    {
        pursuitArea?: ISgPursuitArea;
        riddleArea?: ISgRiddleArea;
        isolatedArea?: ISgIsolatedArea;
        splitSauronArea?: ISgSplitSauronArea;
        islandMap?: ISgIslandMap;
        circuit?: ISgCircuit;
        heading?: ISgHeading;
        investigation_list?: ISgInvestigationList;
        treasure_list?: ISgTreasureList;
        campaign_log?: ISerializedCampaignLog;
        pitArea?: ISgPitArea;
        dorCuartholArea?: ISgDorCuartholArea;
    }

        export interface ISgPursuitArea
        {
            staging: ISgActorArea;
            questHolder: ISgQuestHolder;
            stagingThreatCounter: ISgStagingThreatCounter;
            pursuitBtn: ISgPursuitBtn;
        }

            export interface ISgPursuitBtn
            {
                isPursuitFar: boolean;
            }

        export interface ISgRiddleArea
        {
            actorArea: ISgActorArea;
            questHolder: ISgQuestHolder;
        }

        export interface ISgIsolatedArea
        {
            home: ISgActorArea;
            engaged: ISgActorArea;
            staging: ISgActorArea;
            stagingThreatCounter: ISgStagingThreatCounter;
            allyWillpowerCounter: ISgPlayerWillpowerCounter;
            sauronWillpowerCounter: ISgPlayerWillpowerCounter;
            questVariation: ISgQuestVariation;
            questHolder: ISgQuestHolder
            activeLocationHolder: ISgActiveLocationHolder;
        }

        export interface ISgSplitSauronArea
        {
            staging: ISgActorArea;
            stagingThreatCounter: ISgStagingThreatCounter;
            questHolder: ISgQuestHolder
            activeLocationHolder: ISgActiveLocationHolder;
        }

        export interface ISgIslandMap
        {
            locations: Array<ISgCardToken>;
            playerLocationIndex: number;
        }

        export interface ISgCircuit
        {
            trackStages: Array<ISgTrackStage>;
            challengersToken: ISgChariotToken;
            wainridersToken: ISgChariotToken;
        }

            export interface ISgChariotToken
            {
                lastTrackPosition: number;
                isFinalLap: boolean;
            }

            export interface ISgTrackStage
            {
                isVisited: boolean;
                challengersCounter: ISgTokenCounter;
                wainridersCounter: ISgTokenCounter;
                questPoints: ISgTokenCounter;
            }

        export interface ISgHeading
        {
            compass: ISgCompass;
        }

            export interface ISgCompass
            {
                rotation: number;
            }

        export interface ISgInvestigationList
        {
            checked_items: Array<number>;
        }

        export interface ISgTreasureList
        {
            checked_items: Array<number>;
        }

        export interface ISgPitArea
        {
            actorArea: ISgActorArea;
            questHolder: ISgQuestHolder;
        }

        export interface ISgDorCuartholArea
        {
            locations: Array<ISgCardToken>;
            playerLocationIndex: number;
        }
