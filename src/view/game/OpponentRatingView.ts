import WindowedView from "../WindowedView";

import ServiceLocator from "../../ServiceLocator";
import { main_state_id } from "../../states/StateId";
import Session from "../../Session";
import { player_type } from "../../game/component/world/CPlayerArea";
import { view_layer_id } from "../../service/ViewManager";
import Signal from "../../lib/signals/Signal";
import Utils from "../../Utils";


export default class OpponentRatingView extends WindowedView
{
    // #region Attributes //

    // private:

    // Signals.
    private _onClosed: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "game-opponent_rating";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    // Signals.
    public get onClosed(): Signal { return this._onClosed; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( OpponentRatingView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#opponentRatingGoodBtn" ).on( "click", this.onRateBtn_Click.bind( this, true ) );
        this._jqRoot.find( "#opponentRatingBadBtn" ).on( "click", this.onRateBtn_Click.bind( this, false ) );
        this._jqRoot.find( "#opponentRatingSkipBtn" ).on( "click", this.onSkipBtn_Click.bind( this ) );
    }

    public init(): void
    {
        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ { id: OpponentRatingView.kResId, isPreloadImages: true } ] } );
    }

    // #endregion //


    // #region Input Callbacks //

    private onRateBtn_Click( isGood: boolean ): void
    {
        this._jqLoadingAnimation.show();

        ServiceLocator.dbConnector.post( { 
            serviceName: "UserDAO",
            methodName: "rate",
            parameters: [ 
                Session.playerId, 
                Session.allyId,
                isGood,
                Session.token ] },
            this.onDbOpponent_Rated.bind( this ) );
    }

    private onSkipBtn_Click(): void
    {
        this._onClosed.dispatch();

        ServiceLocator.viewManager.fadeOut( this );
    }

    // #endregion //


    // #region Other Callbacks //

    private onDbOpponent_Rated( result: number ): void
    {
        if ( !this._id ) { return; }

        switch ( result )
        {
            case 0:
            {
                this.onSkipBtn_Click();
                break;
            }

            case 10:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
                break;
            }

            case 11:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "INVALID_USER_TOKEN" ), view_layer_id.POPUP );

                // TODO: Go back to login screen.
                break;
            }
        }

        this._jqLoadingAnimation.hide();
    }

    // #endregion //
}