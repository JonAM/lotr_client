import WindowedView from "../WindowedView";

import ServiceLocator from "../../ServiceLocator";
import Session from "../../Session";
import CPlayerArea, { player_type } from "../../game/component/world/CPlayerArea";
import GameObject from "../../game/GameObject";
import { main_state_id } from "../../states/StateId";
import Path from "../../Path";
import Config from "../../Config";
import { IScenario } from "../../game/ScenarioDB";
import PlayerProgressVO from "../../vo/PlayerProgressVO";
import Utils from "../../Utils";
import { status_type } from "../../game/component/card/token/CCardTokenSide";
import { view_layer_id } from "../../service/ViewManager";
import SaveTreasureListView from "./victory/SaveTreasureListView";
import SaveCampaignLogView from "./victory/SaveCampaignLogView";
import { IFileLoadInfo } from "../../lib/HtmlFileLoader";


export default class VictoryView extends WindowedView
{
    // #region Attributes //

    // private:

    private _isTreasureList: boolean = null;
    private _isCampaignLog: boolean = null;

    // Constants.
    private static readonly _kResId: string = "game-victory";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( VictoryView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#victoryCloseBtn" ).on( "click", this.onCloseBtn_Click.bind( this ) );
    }

    public init(): void
    {
        super.init();

        ServiceLocator.audioManager.playMusic( "victory_screen", { loop: false } );

        this._isTreasureList = false;
        this._isCampaignLog = false;
        if ( ServiceLocator.game.hostPlayer == player_type.PLAYER )
        {
            if ( ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "treasure_list" ) )
            {
                this._isTreasureList = true;
            }
            else if ( ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "campaign_log" ) )
            {
                this._isCampaignLog = true;
            }
        }

        let arrHtmlFileInfo: Array<IFileLoadInfo> = [ { id: VictoryView.kResId, isPreloadImages: true } ];
        if ( this._isTreasureList )
        {
            arrHtmlFileInfo.push( { id: SaveTreasureListView.kResId, isPreloadImages: false } );
        }
        else if ( this._isCampaignLog )
        {
            arrHtmlFileInfo.push( { id: SaveCampaignLogView.kResId, isPreloadImages: false } );
        }
        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: arrHtmlFileInfo,
            isServerDataRequired: true } );

        this._jqRoot.find( "#victoryRound" ).text( jQuery.i18n( "ROUND_N" ).replace( "#", ServiceLocator.game.roundCount.toString() ) );

        const kScenarioInfo: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
        if ( kScenarioInfo.type == "nightmare" )
        {
            this._jqRoot.find( ".content>.scenario" ).addClass( "nightmare" );
        }

        ServiceLocator.dbConnector.post( { 
            serviceName: "PlayerProgressDAO",
            methodName: "getProgress",
            parameters: [ Session.playerId ] },
            this.onPlayerProgress_Retrieved.bind( this ) );
    }

    public end(): void
    {
        this._isTreasureList = null;
        this._isCampaignLog = null;

        super.end();
    }

    // private:

    private fillScoreTable(): void
    {
        let finalScore: number = 0;
        // Score.
        let jqScoreTableRows: JQuery<HTMLElement> = this._jqRoot.find( ".score>.table>.body>.row" );
        // Player.
        this.fillPlayerRow( jqScoreTableRows.eq( 0 ) );
        finalScore += parseInt( jqScoreTableRows.eq( 0 ).find( ".subtotal" ).text() );
        // Ally.
        if ( Session.allyId )
        {
            this.fillAllyRow( jqScoreTableRows.eq( 1 ) );
            finalScore += parseInt( jqScoreTableRows.eq( 1 ).find( ".subtotal" ).text() );
        }
        else
        {
            this._jqRoot.find( ".score>.table>.body>.row.ally" ).hide();
        }
        // Victory points.
        const kVictoryPoints: number = this.calcVictoryPoints();
        this._jqRoot.find( ".victory>.value" ).text( kVictoryPoints > 0 ? "-" + kVictoryPoints.toString() : "-" );
        finalScore -= kVictoryPoints;
        // Game length penalization.
        const kGameLengthPenalization: number = this.calcGameLengthPenalization();
        this._jqRoot.find( ".points_x_round>.value" ).text( kGameLengthPenalization > 0 ? "+" + kGameLengthPenalization.toString() : "-" );
        finalScore += kGameLengthPenalization;
        // Final score.
        this._jqRoot.find( ".final_score>.wrapper>.player_count" ).prop( "src", Path.kResGateway + ( Session.allyId ? "multiplayer" : "single_player" ) + ".png" );
        this._jqRoot.find( ".final_score>.wrapper>.value" ).text( finalScore.toString() );
    }

        private fillPlayerRow( jqPlayerRow: JQuery<HTMLElement> ): void
        {
            jqPlayerRow.find( ".player" ).text( Session.playerId );
            const kThreatLevel: number = ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.threatLevel.cTokenIndicator.trigger.cTokenCounter.count;
            jqPlayerRow.find( ".threat_level" ).text( kThreatLevel.toString() );
            const kThreatCost: number = this.calcDeadHeroesThreatCost( ServiceLocator.game.cGameWorld.cPlayerArea, Session.playerDeck.hero );
            jqPlayerRow.find( ".threat_cost" ).text( kThreatCost.toString() );
            const kDamageCount: number = this.calcSurvivingHeroesDamage( ServiceLocator.game.cGameWorld.cPlayerArea );
            jqPlayerRow.find( ".damage_tokens" ).text( kDamageCount.toString() );
            jqPlayerRow.find( ".subtotal" ).text( ( kThreatLevel + kThreatCost + kDamageCount ).toString() );
        }

            private calcDeadHeroesThreatCost( cPlayerArea: CPlayerArea, arrHeroId: Array<string> ): number
            {
                let result: number = 0;

                let arrPlayerAreaHeroId: Array<string> = new Array<string>();
                let arrActor: Array<GameObject> = cPlayerArea.cHome.findAllActors();
                for ( let actor of arrActor )
                {
                    if ( actor.cCardToken.curSide.cHeroSide ) 
                    {
                        arrPlayerAreaHeroId.push( actor.cCardToken.cCurSide.cardId );
                        if ( actor.cCardToken.curSide.cHeroSide.hasStatus( status_type.DEAD ) )
                        {
                            result += actor.cCardToken.curSide.cHeroSide.cardInfo.threat;
                        }
                    }
                }

                if ( arrPlayerAreaHeroId.length < arrHeroId.length )
                {
                    for ( let heroId of arrHeroId )
                    {
                        if ( arrPlayerAreaHeroId.indexOf( heroId ) == -1 )
                        {
                            result += ServiceLocator.cardDb.find( heroId ).threat;
                        }
                    }
                }

                return result;
            }

            private calcSurvivingHeroesDamage( cPlayerArea: CPlayerArea ): number
            {
                let result: number = 0;

                let arrActor: Array<GameObject> = cPlayerArea.cHome.findAllActors();
                for ( let actor of arrActor )
                {
                    if ( actor.cCardToken.curSide.cHeroSide && !actor.cCardToken.curSide.cHeroSide.hasStatus( status_type.DEAD ) )
                    {
                        result += actor.cCardToken.curSide.cHeroSide.wound.cTokenCounter.count;
                    }
                }

                return result;
            }

        private fillAllyRow( jqAllyRow: JQuery<HTMLElement> ): void
        {
            jqAllyRow.find( ".player" ).text( Session.allyId );
            const kThreatLevel: number = ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.threatLevel.cTokenIndicator.trigger.cTokenCounter.count;
            jqAllyRow.find( ".threat_level" ).text( kThreatLevel.toString() );
            const kThreatCost: number = this.calcDeadHeroesThreatCost( ServiceLocator.game.cGameWorld.cAllyArea, Session.allyDeck.hero );
            jqAllyRow.find( ".threat_cost" ).text( kThreatCost.toString() );
            const kDamageCount: number = this.calcSurvivingHeroesDamage( ServiceLocator.game.cGameWorld.cAllyArea );
            jqAllyRow.find( ".damage_tokens" ).text( kDamageCount.toString() );
            jqAllyRow.find( ".subtotal" ).text( ( kThreatLevel + kThreatCost + kDamageCount ).toString() );
        }

        private calcVictoryPoints(): number
        {
            let result: number = 0;

            let arrCard: Array<GameObject> = ServiceLocator.game.cGameWorld.sauronArea.cSauronArea
                .cSideControls.victoryDisplay.cDeckIndicator.cViewer.cCardView.findItems();
            for ( let card of arrCard )
            {
                if ( card.cCard.curSide.cardInfo.victory )
                {
                    result += card.cCard.curSide.cardInfo.victory;
                }
            }

            return result;
        }

        private calcGameLengthPenalization(): number
        {
            return ServiceLocator.game.roundCount * 10;
        }

    // #endregion //


    // #region Input Callbacks //

    private onCloseBtn_Click(): void
    {
        if ( this._isTreasureList )
        {
            let saveTreasureListView: SaveTreasureListView = new SaveTreasureListView();
            saveTreasureListView.onClosed.add( this.onSaveTreasureListView_Closed, this );
            saveTreasureListView.init();
            ServiceLocator.viewManager.fadeIn( saveTreasureListView, view_layer_id.POPUP );
        }
        else if ( this._isCampaignLog )
        {
            let saveCampaignLogView: SaveCampaignLogView = new SaveCampaignLogView();
            saveCampaignLogView.onClosed.add( this.onSaveCampaignLogView_Closed, this );
            saveCampaignLogView.init();
            ServiceLocator.viewManager.fadeIn( saveCampaignLogView, view_layer_id.POPUP );
        }
        else 
        {
            ServiceLocator.viewManager.fadeOut( this );
            ServiceLocator.stateMachine.request( main_state_id.GATEWAY );
        }
    }

        private onSaveTreasureListView_Closed(): void
        {
            ServiceLocator.viewManager.fadeOut( this );
            ServiceLocator.stateMachine.request( main_state_id.GATEWAY );
        }

        private onSaveCampaignLogView_Closed(): void
        {
            ServiceLocator.viewManager.fadeOut( this );
            ServiceLocator.stateMachine.request( main_state_id.GATEWAY );
        }

    // #endregion //


    // #region Other Callbacks //

    private onPlayerProgress_Retrieved( arrPlayerProgress: Array<Object> ): void
    {
        const kScenarioInfo: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
        this._jqRoot.find( ".scenario>img" ).prop( "src", Path.kResImages + "common/scenario/" + kScenarioInfo.product_icon + ".png" );
        this._jqRoot.find( ".scenario>.detail>.title" ).text( kScenarioInfo.name ? kScenarioInfo.name : kScenarioInfo.encounter_set.main );
        this._jqRoot.find( ".scenario>.detail>.set" ).text( ServiceLocator.scenarioDb.find( Session.scenarioId.substr( 0, 2 ) ).name );

        let arrPlayerProgressVO: Array<PlayerProgressVO> = PlayerProgressVO.parseArray( arrPlayerProgress );
        let bestScore: IBestScore = { single: null, multiplayer: null };
        for ( let playerProgressVO of arrPlayerProgressVO )
        {
            if ( playerProgressVO.scenarioId == Session.scenarioId )
            {
                if ( playerProgressVO.playerTwo == null )
                {
                    if ( bestScore.single == null || playerProgressVO.score > bestScore.single.score )
                    {
                        bestScore.single = { score: playerProgressVO.score, when: playerProgressVO.whenTimestamp };
                    }
                }
                else
                {
                    if ( bestScore.multiplayer == null || playerProgressVO.score > bestScore.multiplayer.score )
                    {
                        const kAlly: string = playerProgressVO.playerTwo == Session.playerId ? playerProgressVO.playerOne : playerProgressVO.playerTwo;
                        bestScore.multiplayer = { 
                            score: playerProgressVO.score, 
                            ally: kAlly,
                            when: playerProgressVO.whenTimestamp };
                    }
                }
            }
        }
        if ( bestScore.single == null && bestScore.multiplayer == null )
        {
            bestScore = null;
        }
        if ( bestScore )
        {
            let bestScoreHtml: string = "";
            if ( bestScore.single )
            {
                bestScoreHtml += 
                    "<div class=\"single_player\">"
                        + "<img class=\"player_count\" src=\"./resources/images/gateway/single_player.png\">"
                        + "<div class=\"value black gb-text-body-normal gb-text-ellipsis\">" + bestScore.single.score.toString() + "</div>"
                        + "<div class=\"separator black\">|</div>"
                        + "<div class=\"when black gb-text-body-small\">" + jQuery.i18n( "TIME_AGO" ).replace( "#", Utils.web.createElapsedTimeStr( bestScore.single.when ) ) + "</div>"
                    + "</div>";
            }
            if ( bestScore.multiplayer )
            {
                bestScoreHtml += 
                    "<div class=\"multiplayer\">"
                        + "<img class=\"player_count\" src=\"./resources/images/gateway/multiplayer.png\">"
                        + "<div class=\"value black gb-text-body-normal gb-text-ellipsis\">" + bestScore.multiplayer.score.toString() + "</div>"
                        + "<div class=\"separator black\">|</div>"
                        + "<div class=\"ally black gb-text-body-small\">" + bestScore.multiplayer.ally + "</div>"
                        + "<div class=\"separator black\">|</div>"
                        + "<div class=\"when black gb-text-body-small\">" + jQuery.i18n( "TIME_AGO" ).replace( "#", Utils.web.createElapsedTimeStr( bestScore.multiplayer.when ) ) + "</div>"
                    + "</div>";
            }
            this._jqRoot.find( ".scenario>.detail>.best_score" ).append( bestScoreHtml );
        }
        else
        {
            this._jqRoot.find( ".scenario>.detail>.best_score" ).hide();
        }

        this.fillScoreTable();

        if ( ServiceLocator.game.hostPlayer == player_type.PLAYER )
        {
            const kFinalScore: number = parseInt( this._jqRoot.find( ".final_score>.wrapper>.value" ).text() );

            let campaignLog: GameObject = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "campaign_log" );
            if ( campaignLog )
            {
                campaignLog.cCampaignLog.campaignLogVO.completedScenarios.push( { id: Session.scenarioId, score: kFinalScore } );
            }

            ServiceLocator.dbConnector.post( { 
                serviceName: "PlayerProgressDAO",
                methodName: "setProgress",
                parameters: [ 
                    Session.playerId, 
                    JSON.stringify( Session.playerDeck ), 
                    Session.allyId ? Session.allyId : "", 
                    Session.allyDeck ? JSON.stringify( Session.allyDeck ) : "", 
                    Session.scenarioId, 
                    kFinalScore,
                    Session.token ] },
                null );
        }
        
        this._viewPreloader.onServerDataLoaded();
    }

    // #endregion //
}

interface IBestScore
{
    single: ISingleBestScore;
    multiplayer: IMultiplayerBestScore;
}

interface ISingleBestScore
{
    score: number;
    when: number;
}

interface IMultiplayerBestScore
{
    score: number;
    ally: string;
    when: number;
}
