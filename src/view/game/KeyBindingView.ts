import WindowedView from "../WindowedView";

import ServiceLocator from "../../ServiceLocator";


export default class KeyBindingView extends WindowedView
{
    // #region Attributes //

    // private:

    private _jqSelKeyBindingBtn: JQuery<HTMLElement> = null;

    // Binded functions.
    private _bfOnDocumentKeyDown: ( ev: KeyboardEvent) => any = null;
    
    // Constants.
    private static readonly _kResId: string = "game-key_binding";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( KeyBindingView._kResId );

        this._bfOnDocumentKeyDown = this.onDocument_KeyDown.bind( this );

        // Attach UI event handlers.
        this._jqRoot.find( "#keyBindingBackBtn" ).on( "click", this.onBackBtn_Click.bind( this ) );
        this._jqRoot.find( ".section>button" ).on( "click", this.onKeyBindingBtn_Click.bind( this ) );
    }

    public init(): void
    {
        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ { id: KeyBindingView.kResId, isPreloadImages: true } ] } );
    
        ServiceLocator.game.keyBindingManager.setEnabled( false );

        this._jqRoot.find( "#keyBindingChatBtn" ).text( ServiceLocator.game.keyBindingManager.getBinding( "chat" ) );
        this._jqRoot.find( "#keyBindingHandBtn" ).text( ServiceLocator.game.keyBindingManager.getBinding( "hand" ) );
        this._jqRoot.find( "#keyBindingPoiMenuBtn" ).text( ServiceLocator.game.keyBindingManager.getBinding( "poiMenu" ) );
        this._jqRoot.find( "#keyBindingForceDragBtn" ).text( ServiceLocator.game.keyBindingManager.getBinding( "forceDrag" ) );
        this._jqRoot.find( "#keyBindingEndTurnBtn" ).text( ServiceLocator.game.keyBindingManager.getBinding( "endTurn" ) );
    }

    public end(): void
    {
        document.removeEventListener( "keydown", this._bfOnDocumentKeyDown );

        ServiceLocator.game.keyBindingManager.setEnabled( true );

        super.end();
    }

    // #endregion //


    // #region Input Callbacks //

    private onBackBtn_Click(): void
    {
        ServiceLocator.savedData.save();

        ServiceLocator.viewManager.fadeOut( this );
    }

    private onKeyBindingBtn_Click( jqEvent: JQuery.Event ): void
    {
        let jqKeyBindignBtn: JQuery<HTMLElement> = jQuery( jqEvent.target );
        jqKeyBindignBtn.text( "Press key" );
        this._jqSelKeyBindingBtn = jqKeyBindignBtn;

        this._jqRoot.find( ".section>button" ).prop( "disabled", true );
        jqKeyBindignBtn.prop( "disabled", false );
        document.addEventListener( "keydown", this._bfOnDocumentKeyDown );
    }

    private onDocument_KeyDown( event: KeyboardEvent )
    {
        ServiceLocator.game.keyBindingManager.setBinding( this._jqSelKeyBindingBtn.data( "key" ), event.code );
        let jqKeyBindignBtnList: JQuery<HTMLElement> = this._jqRoot.find( ".section>button" );
        for ( let i: number = 0; i < jqKeyBindignBtnList.length; ++i )
        {
            let jqKeyBindignBtn: JQuery<HTMLElement> = jqKeyBindignBtnList.eq( i );
            const kBinding: string = ServiceLocator.game.keyBindingManager.getBinding( jqKeyBindignBtn.data( "key" ) );
            jqKeyBindignBtn.text( kBinding );
        }
        this._jqSelKeyBindingBtn.text( event.code );
        this._jqSelKeyBindingBtn = null;
        this._jqRoot.find( ".section>button" ).prop( "disabled", false );
        document.removeEventListener( "keydown", this._bfOnDocumentKeyDown );
    }

    // #endregion //
}