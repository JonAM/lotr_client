import WindowedView from "../WindowedView";

import { IFileLoadInfo } from "../../lib/HtmlFileLoader";
import ServiceLocator from "../../ServiceLocator";

import Signal from "../../lib/signals/Signal";


export default class CardTrayCreationView extends WindowedView
{
    // #region Attributes //

    // private:
    
    // Signals.
    private _onCreated: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "game-card_tray_creation";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    // Signals.
    public get onCreated(): Signal { return this._onCreated; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( CardTrayCreationView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#loginConnectBtn" ).on( "click", this.onCreateBtn_Click.bind( this ) );
    }

    public init(): void
    {
        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ { id: CardTrayCreationView.kResId, isPreloadImages: true } ] } );
    }

    public end(): void
    {
        // Owned signals.
        this._onCreated.removeAll();

        super.end();
    }

    // #endregion //


    // #region Input Callbacks //

    private onCreateBtn_Click(): void
    {
        this._onCreated.dispatch();
    }

    // #endregion //
}