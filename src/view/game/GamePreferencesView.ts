import WindowedView from "../WindowedView";

import ServiceLocator from "../../ServiceLocator";

import Signal from "../../lib/signals/Signal";


export default class GamePreferencesView extends WindowedView
{
    // #region Attributes //

    // private:

    // Quick access.
    private _jqTimeToCardPreview: JQuery<HTMLElement> = null;
    private _jqManuallyRevealShadowCardsCheckbox: JQuery<HTMLElement> = null;
    private _jqSkipPhaseIntroCheckbox: JQuery<HTMLElement> = null;
    private _jqAutoEndResourcePhaseCheckbox: JQuery<HTMLElement> = null;

    // Constants.
    private static readonly _kResId: string = "game-game_preferences";

    // Signals.
    private _onClosed: Signal = new Signal();
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    // Signals.
    public get onClosed(): Signal { return this._onClosed; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( GamePreferencesView._kResId );

        // Quick access.
        this._jqTimeToCardPreview = this._jqRoot.find( "#gamePreferencesTimeToCardPreview" );
        this._jqManuallyRevealShadowCardsCheckbox = this._jqRoot.find( "#gamePreferencesManuallyRevealShadowCardsCheckbox" );
        this._jqSkipPhaseIntroCheckbox = this._jqRoot.find( "#gamePreferencesSkipPhaseIntroCheckbox" );
        this._jqAutoEndResourcePhaseCheckbox = this._jqRoot.find( "#gamePreferencesAutoEndResourcePhaseCheckbox" );

        // Attach UI event handlers.
        this._jqManuallyRevealShadowCardsCheckbox.on( "click", this.onManuallyRevealShadowCardsCheckbox_Click.bind( this ) );
        this._jqSkipPhaseIntroCheckbox.on( "click", this.onSkipPhaseIntroCheckbox_Click.bind( this ) );
        this._jqAutoEndResourcePhaseCheckbox.on( "click", this.onAutoEndResourcePhaseCheckbox_Click.bind( this ) );
        this._jqRoot.find( "#gamePreferencesBackBtn" ).on( "click", this.onBackBtn_Click.bind( this ) );
    }

    public init(): void
    {
        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ { id: GamePreferencesView.kResId, isPreloadImages: true } ] } );

        this._jqRoot.find( "#gamePreferencesLeftCardPreviewBtn" ).prop( "checked", ServiceLocator.savedData.data.gamePreferences.cardPreviewPreferredSide == "left" );
        this._jqRoot.find( "#gamePreferencesRightCardPreviewBtn" ).prop( "checked", ServiceLocator.savedData.data.gamePreferences.cardPreviewPreferredSide == "right" );    
        this._jqTimeToCardPreview.val( ServiceLocator.savedData.data.gamePreferences.touchOverDelayInMs );
        this._jqManuallyRevealShadowCardsCheckbox.prop( "checked", ServiceLocator.savedData.data.gamePreferences.isManuallyRevealShadowCards );
        this._jqSkipPhaseIntroCheckbox.prop( "checked", ServiceLocator.savedData.data.gamePreferences.isSkipPhaseIntro );
        this._jqAutoEndResourcePhaseCheckbox.prop( "checked", ServiceLocator.savedData.data.gamePreferences.isAutoEndResourcePhase );

        if ( !ServiceLocator.game.app.renderer.plugins.interaction.supportsTouchEvents )
        {
            this._jqRoot.find( "#gameParametersTimeToCardPreviewRow" ).hide();
        }
    }
    
    public end(): void
    {
        this._onClosed.removeAll();

        super.end();
    }

    // #endregion //


    // #region Input Callbacks //

    private onManuallyRevealShadowCardsCheckbox_Click(): void
    {
        ServiceLocator.savedData.data.gamePreferences.isManuallyRevealShadowCards = this._jqManuallyRevealShadowCardsCheckbox.prop( "checked" );
    }

    private onSkipPhaseIntroCheckbox_Click(): void
    {
        ServiceLocator.savedData.data.gamePreferences.isSkipPhaseIntro = this._jqSkipPhaseIntroCheckbox.prop( "checked" );
    }

    private onAutoEndResourcePhaseCheckbox_Click(): void
    {
        ServiceLocator.savedData.data.gamePreferences.isAutoEndResourcePhase = this._jqAutoEndResourcePhaseCheckbox.prop( "checked" );
    }

    private onBackBtn_Click(): void
    {
        ServiceLocator.savedData.data.gamePreferences.touchOverDelayInMs = this._jqTimeToCardPreview.val() as number;
        if ( ServiceLocator.savedData.data.gamePreferences.touchOverDelayInMs < 0 )
        {
            ServiceLocator.savedData.data.gamePreferences.touchOverDelayInMs = 0;
        }
        ServiceLocator.savedData.data.gamePreferences.cardPreviewPreferredSide = this._jqRoot.find( "#gamePreferencesLeftCardPreviewBtn" ).prop( "checked" ) ? "left" : "right";
        ServiceLocator.savedData.save();

        ServiceLocator.viewManager.fadeOut( this );
    }

    // #endregion //
}