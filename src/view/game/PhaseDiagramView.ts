import WindowedView from "../WindowedView";

import { game_state_id, main_state_id, setup_state_id } from "../../states/StateId";
import ServiceLocator from "../../ServiceLocator";

import Signal from "../../lib/signals/Signal";
import SetupState from "../../states/game/SetupState";
import QuestState from "../../states/game/QuestState";
import EncounterState from "../../states/game/EncounterState";
import CombatState from "../../states/game/CombatState";
import InfoMessageView, { btn_layout_type } from "../common/popup/InfoMessageView";
import { view_layer_id } from "../../service/ViewManager";
import SignalBinding from "../../lib/signals/SignalBinding";
import PhaseIntroState from "../../states/game/PhaseIntroState";
import State from "../../lib/state_machine/State";
import Session from "../../Session";
import { action_scope_type, player_action_type } from "../../service/socket_io/GameSocketIOController";
import { action_request_type, IOppActionListener, IOpponentAction } from "../../game/AllyActionManager";
import { end_turn_btn_state } from "../../game/component/ui/CEndTurnButton";


export default class PhaseDiagramView extends WindowedView implements IOppActionListener
{
    // #region Attributes //

    // private:
    private _curPhaseIndex: number = null;
    
    // Binded functions.
    private _bfOnWindowResized: any = null;
    
    // Signals.
    private _onStateRequested: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "game-phase_diagram";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    // Signals.
    public get onStateRequested(): Signal { return this._onStateRequested; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( PhaseDiagramView._kResId );

        // Binded functions.
        this._bfOnWindowResized = this.onWindow_Resized.bind( this );

        // Attach UI event handlers.
        this._jqRoot.find( "#bg" ).on( "click", this.onSelf_Closed.bind( this ) );
        this._jqRoot.find( "#phaseDiagramAutoSkipCheckbox" ).on( "click", this.onAutoSkipCheckBox_Click.bind( this ) );
        let jqDetailBtnList: JQuery<HTMLElement> = this._jqRoot.find( "button.detail" );
        for ( let i: number = 0; i < jqDetailBtnList.length; ++i )
        {
            let jqDetailBtn: JQuery<HTMLElement> = jqDetailBtnList.eq( i );
            jqDetailBtn.on( "click", this.onPhaseDetailBtn_Click.bind( this, jqDetailBtn ) );
        }
        let jqJumpBtnList: JQuery<HTMLElement> = this._jqRoot.find( "button.jump" );
        for ( let i: number = 0; i < jqJumpBtnList.length; ++i )
        {
            let jqJumpBtn: JQuery<HTMLElement> = jqJumpBtnList.eq( i );
            jqJumpBtn.on( "click", this.onPhaseJumpBtn_Click.bind( this, jqJumpBtn ) );
            jqJumpBtn.prop( "disabled", ServiceLocator.game.stateMachine.currentStateId == game_state_id.SETUP );
        }
        let jqPlayBtnList: JQuery<HTMLElement> = this._jqRoot.find( "button.play" );
        for ( let i: number = 0; i < jqPlayBtnList.length; ++i )
        {
            let jqPlayBtn: JQuery<HTMLElement> = jqPlayBtnList.eq( i );
            let isSkip: boolean = false;
            if ( jqPlayBtn.data( "subphaseid" ) == undefined )
            {
                isSkip = ServiceLocator.game.phaseNavigator.isSkipGamePhase( parseInt( jqPlayBtn.data( "phaseid" ) as string ) );
            }
            else
            {
                isSkip = ServiceLocator.game.phaseNavigator.isSkipGamePhase( parseInt( jqPlayBtn.data( "phaseid" ) as string ), parseInt( jqPlayBtn.data( "subphaseid" ) as string ) );
            }
            if ( isSkip )
            {
                jqPlayBtn.addClass( "skip" );
            }
            jqPlayBtn.on( "click", this.onPhasePlayBtn_Click.bind( this, jqPlayBtn ) );
        }
        let jqResourceBtn: JQuery<HTMLElement> = this._jqRoot.find( "button.add" );
        if ( !ServiceLocator.game.phaseNavigator.isResPhaseAddToken )
        {
            jqResourceBtn.addClass( "skip" );
        }
        jqResourceBtn.on( "click", this.onToggleResourceBtn_Click.bind( this ) );
        let jqDrawBtn: JQuery<HTMLElement> = this._jqRoot.find( "button.draw" );
        if ( !ServiceLocator.game.phaseNavigator.isResPhaseDrawCard )
        {
            jqDrawBtn.addClass( "skip" );
        }
        jqDrawBtn.on( "click", this.onToggleDrawBtn_Click.bind( this ) );
        let jqStagingDrawBtn: JQuery<HTMLElement> = this._jqRoot.find( "button.draw_encounter" );
        if ( !ServiceLocator.game.phaseNavigator.isStagingSubphaseDrawCard )
        {
            jqStagingDrawBtn.addClass( "skip" );
        }
        jqStagingDrawBtn.on( "click", this.onToggleStagingDrawBtn_Click.bind( this ) );
    }

    public init(): void
    {
        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ { id: PhaseDiagramView.kResId, isPreloadImages: true } ] } );

            
        this._jqRoot.find( "#phaseDiagramAutoSkipCheckbox" ).prop( "checked", ServiceLocator.game.phaseNavigator.isAutoSkip );

        this._jqRoot.find( ".secondary>.breakdown>*" ).hide();

        this._curPhaseIndex = ServiceLocator.game.stateMachine.currentStateId;
        this.highlightCurrentPhase();
        this.highlightCurrentSubphase();
        let jqDetailBtn: JQuery<HTMLElement> = this._jqRoot.find( ".phase" ).eq( this._curPhaseIndex ).find( "button.detail" );
        if ( jqDetailBtn.length > 0 )
        {
            this.onPhaseDetailBtn_Click( jqDetailBtn );
        }

        // Listen to events.
        jQuery( window ).on( "resize", this._bfOnWindowResized );
        ServiceLocator.game.allyActionManager.addListener( this, [ player_action_type.SET_GAME_PHASE_ENABLED, player_action_type.SET_AUTO_SKIP_EMPTY_PHASES ] );
    }

    public end(): void
    {
        this._onStateRequested.removeAll();

        // Cleanup events.
        jQuery( window ).off( "resize", this._bfOnWindowResized );
        ServiceLocator.game.allyActionManager.removeListener( this, [ player_action_type.SET_GAME_PHASE_ENABLED, player_action_type.SET_AUTO_SKIP_EMPTY_PHASES ] );

        super.end();
    }

    // private:
    
    private highlightCurrentPhase(): void
    {
        this._jqRoot.find( ".primary>.phase" ).eq( this._curPhaseIndex ).addClass( "current" );
    }

    private highlightCurrentSubphase(): void
    {
        let jqSubphaseList: JQuery<HTMLElement> = this._jqRoot.find( ".secondary>.breakdown" ).eq( this._curPhaseIndex ).children( ".subphase" );
        const kCurSuphaseIndex: number = this.findCurrentSubphaseIndex();
        if ( kCurSuphaseIndex != null )
        {
            jqSubphaseList.eq( kCurSuphaseIndex ).addClass( "current" );
        }
    }

    private findCurrentSubphaseIndex(): number
    {
        let result: number = null;

        switch ( ServiceLocator.game.stateMachine.currentStateId )
        {
            case game_state_id.SETUP:
            {
                result = ( ServiceLocator.game.stateMachine.currentState as SetupState ).findCurrentSubstate();
                result -= 1;
                if ( result < 0 )
                {
                    result = 0;
                }
                else if ( result > 1 )
                {
                    result = 1;
                }
                break;
            }

            case game_state_id.QUEST: 
            {
                result = ( ServiceLocator.game.stateMachine.currentState as QuestState ).findCurrentSubstate();
                break;
            }

            case game_state_id.ENCOUNTER:
            {
                result = ( ServiceLocator.game.stateMachine.currentState as EncounterState ).findCurrentSubstate();
                break;
            }

            case game_state_id.COMBAT:
            {
                result = ( ServiceLocator.game.stateMachine.currentState as CombatState ).findCurrentSubstate();
                break;
            }
        }

        return result;
    }

    private updateContentMaxHeight(): void
    {
        const kPanelInnerHeight: number = jQuery( window ).innerHeight() * 0.8 - 40;
        this._jqRoot.find( ".gb-panel>.content" ).css( "max-height", kPanelInnerHeight.toString() + "px" );
    }

    // #endregion //


    // #region IOppActionListener //

    public onOpponentActionReceived( action: IOpponentAction ): void
    {
        switch ( action.playerActionType )
        {
            case player_action_type.SET_GAME_PHASE_ENABLED:
                {
                    let subphaseId: number = null;
                    if ( action.args[ 1 ] != null )
                    {
                        subphaseId = action.args[ 1 ];
                    }
                    this.updatePhaseDiagramSkipButtons( action.args[ 0 ], subphaseId, action.args[ 2 ] );
                    break;
                }

            case player_action_type.SET_AUTO_SKIP_EMPTY_PHASES:
                {
                    this._jqRoot.find( "#phaseDiagramAutoSkipCheckbox" ).prop( "checked", action.args[ 0 ] as boolean );
                    break;
                }

            case player_action_type.SET_RESOURCE_PHASE_ADD_TOKEN:
                {
                    let jqToggleBtn: JQuery<HTMLElement> = this._jqRoot.find( "button.add" );
                    if ( !ServiceLocator.game.phaseNavigator.isResPhaseAddToken )
                    {
                        jqToggleBtn.addClass( "skip" );
                    }
                    else
                    {
                        jqToggleBtn.removeClass( "skip" );
                    }
                    break;
                }

            case player_action_type.SET_RESOURCE_PHASE_DRAW_CARD:
                {
                    let jqToggleBtn: JQuery<HTMLElement> = this._jqRoot.find( "button.draw" );
                    if ( !ServiceLocator.game.phaseNavigator.isResPhaseDrawCard )
                    {
                        jqToggleBtn.addClass( "skip" );
                    }
                    else
                    {
                        jqToggleBtn.removeClass( "skip" );
                    }
                    break;
                }

            case player_action_type.SET_STAGING_SUBPHASE_DRAW_CARD:
                {
                    let jqToggleBtn: JQuery<HTMLElement> = this._jqRoot.find( "button.draw_encounter" );
                    if ( !ServiceLocator.game.phaseNavigator.isStagingSubphaseDrawCard )
                    {
                        jqToggleBtn.addClass( "skip" );
                    }
                    else
                    {
                        jqToggleBtn.removeClass( "skip" );
                    }
                    break;
                }
        }
    }

    // #endregion // 


    // #region Input Callbacks //

    private onSelf_Closed(): void
    {   
        ServiceLocator.viewManager.fadeOut( this );
    }

    private onAutoSkipCheckBox_Click(): void
    {
        const isSkip: boolean = this._jqRoot.find( "#phaseDiagramAutoSkipCheckbox" ).prop( "checked" );
        ServiceLocator.game.phaseNavigator.isAutoSkip = isSkip; 

        ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn.onGamePhaseDiagramChanged();
        if ( Session.allyId )
        {  
            ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.onGamePhaseDiagramChanged();
        }

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_AUTO_SKIP_EMPTY_PHASES, null, [ isSkip ] );
    }

    private onPhaseDetailBtn_Click( jqDetailBtn: JQuery<HTMLElement> ): void
    {
        const kIndex: number = parseInt( jqDetailBtn.data( "index" ) as string );

        if ( jqDetailBtn.hasClass( "more" ) )
        {
            jqDetailBtn.removeClass( "more" );
            jqDetailBtn.addClass( "less" );

            this._jqRoot.find( ".secondary>.breakdown" ).eq( kIndex ).children().show();
        }
        else
        {
            jqDetailBtn.removeClass( "less" );
            jqDetailBtn.addClass( "more" );

            this._jqRoot.find( ".secondary>.breakdown" ).eq( kIndex ).children().hide();
        }

        this.updateContentMaxHeight();
    }

    private onPhaseJumpBtn_Click( jqJumpBtn: JQuery<HTMLElement> ): void
    {
        const kToGameStateId: number = parseInt( jqJumpBtn.data( "phaseid" ) as string );
        let isJumpValid: boolean = !ServiceLocator.game.phaseNavigator.isSkipGamePhase( kToGameStateId )
            && !ServiceLocator.game.phaseNavigator.isPhaseEmpty( kToGameStateId, null );
        if ( isJumpValid 
            && ( kToGameStateId == game_state_id.QUEST || kToGameStateId == game_state_id.ENCOUNTER || kToGameStateId == game_state_id.COMBAT ) )
        {
            isJumpValid = ServiceLocator.game.phaseNavigator.findLastPlayableSubstateId( kToGameStateId ) != null;
        }
        if ( isJumpValid )
        {
            if ( !Session.allyId )
            {
                let jumpConfirmationView: InfoMessageView = new InfoMessageView();
                jumpConfirmationView.message = jQuery.i18n( "PHASE_JUMP_CONFIRMATION" ).replace( "#", jqJumpBtn.siblings( ".text" ).text() );
                jumpConfirmationView.btnLayoutType = btn_layout_type.DOUBLE;
                jumpConfirmationView.init();
                let sb: SignalBinding = jumpConfirmationView.onClosed.addOnce( this.onJumpConfirmationPopup_Closed, this );
                sb.params = [ kToGameStateId ];
                ServiceLocator.viewManager.fadeIn( jumpConfirmationView, view_layer_id.POPUP );
            }
            else
            {
                let waitForAllyConfirmationView: InfoMessageView = new InfoMessageView();
                waitForAllyConfirmationView.alias = "waitForAllyConfirmationView";
                waitForAllyConfirmationView.message = jQuery.i18n( "WAIT_FOR_ALLY_CONFIRMATION" );
                waitForAllyConfirmationView.btnLayoutType = btn_layout_type.DOUBLE;
                waitForAllyConfirmationView.init();
                waitForAllyConfirmationView.root.find( "#infoMessageAcceptBtn" ).hide();
                waitForAllyConfirmationView.onClosed.addOnce( this.onWaitForAllyConfirmationPopup_Closed, this );
                ServiceLocator.viewManager.fadeIn( waitForAllyConfirmationView, view_layer_id.POPUP );

                // Multiplayer.
                ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ACTION_REQUESTED, null, [ action_request_type.JUMP_TO_GAME_PHASE, kToGameStateId, jqJumpBtn.siblings( ".text" ).data( "i18n" ) ] );
            }
        }
    }

        private onJumpConfirmationPopup_Closed( gameStateId: game_state_id, result: boolean ): void
        {
            if ( result )
            {
                ServiceLocator.game.stateMachine.onStateEntered.addOnce( ( state: State ) => {
                    let phaseIntroState: PhaseIntroState = state as PhaseIntroState;
                    phaseIntroState.nextStateId = gameStateId; 
                } );
                ServiceLocator.game.stateMachine.request( game_state_id.PHASE_INTRO );

                this.onSelf_Closed();
            }
        }

        private onWaitForAllyConfirmationPopup_Closed(): void
        {
            // Multiplayer.
            ServiceLocator.socketIOManager.game.notifyAction( player_action_type.ACTION_REQUEST_CANCELLED, null, null );
        }

    private onPhasePlayBtn_Click( jqPlayBtn: JQuery<HTMLElement> ): void
    {
        let isSkip: boolean = !jqPlayBtn.hasClass( "skip" );
        const kPhaseId: number = parseInt( jqPlayBtn.data( "phaseid" ) as string );
        let subphaseId: number = null;
        if ( jqPlayBtn.data( "subphaseid" ) != undefined )
        {
            subphaseId = parseInt( jqPlayBtn.data( "subphaseid" ) as string );
        }

        ServiceLocator.game.phaseNavigator.setSkipGamePhase( kPhaseId, subphaseId, isSkip );
        this.updatePhaseDiagramSkipButtons( kPhaseId, subphaseId, isSkip );

        if ( ServiceLocator.game.stateMachine.currentStateId > game_state_id.SETUP )
        {
            ServiceLocator.game.cGameWorld.cPlayerArea.cSideControls.cEndTurnBtn.onGamePhaseDiagramChanged();
            if ( Session.allyId )
            {
                ServiceLocator.game.cGameWorld.cAllyArea.cSideControls.cEndTurnBtn.onGamePhaseDiagramChanged();
            }
        }

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_GAME_PHASE_ENABLED, null, [ kPhaseId, subphaseId, isSkip ] );
    }

        private updatePhaseDiagramSkipButtons( phaseId: number, subphaseId: number, isSkip: boolean ): void
        {
            let queryStr: string = "button.play[data-phaseid=\"" + phaseId.toString() + "\"]";
            if ( subphaseId != null )
            {
                queryStr += "[data-subphaseid=\"" + subphaseId.toString() + "\"]";
            }
            let jqSelectedSkipBtn: JQuery<HTMLElement> = this._jqRoot.find( queryStr ).first();
            if ( isSkip )
            {
                jqSelectedSkipBtn.addClass( "skip" );
            }
            else
            {
                jqSelectedSkipBtn.removeClass( "skip" );
            }
            
            if ( subphaseId == null )
            {
                // Phase.
                if ( isSkip )
                {
                    this._jqRoot.find( ".subphase>button.play[data-phaseid=\"" + phaseId.toString() + "\"]" ).addClass( "skip" );  
                }
                else
                {
                    this._jqRoot.find( ".subphase>button.play[data-phaseid=\"" + phaseId.toString() + "\"]" ).removeClass( "skip" );  
                }
            }
            else
            {
                // Subphase.
                if ( ServiceLocator.game.phaseNavigator.isSkipGamePhase( phaseId ) )
                {
                    this._jqRoot.find( ".phase>button.play[data-phaseid=\"" + phaseId.toString() + "\"]" ).addClass( "skip" );
                }
                else
                {
                    this._jqRoot.find( ".phase>button.play[data-phaseid=\"" + phaseId.toString() + "\"]" ).removeClass( "skip" );
                }
            }
        }

    private onToggleResourceBtn_Click( jqEvent: JQuery.Event ): void
    {
        let jqToggleBtn: JQuery<HTMLElement> = jQuery( jqEvent.target );
        if ( jqToggleBtn.hasClass( "skip" ) )
        {
            jqToggleBtn.removeClass( "skip" );
        }
        else 
        {
            jqToggleBtn.addClass( "skip" );
        }
        ServiceLocator.game.phaseNavigator.isResPhaseAddToken = !jqToggleBtn.hasClass( "skip" );

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_RESOURCE_PHASE_ADD_TOKEN, null, [ ServiceLocator.game.phaseNavigator.isResPhaseAddToken ] );
    }

    private onToggleDrawBtn_Click( jqEvent: JQuery.Event ): void
    {
        let jqToggleBtn: JQuery<HTMLElement> = jQuery( jqEvent.target );
        if ( jqToggleBtn.hasClass( "skip" ) )
        {
            jqToggleBtn.removeClass( "skip" );
        }
        else 
        {
            jqToggleBtn.addClass( "skip" );
        }
        ServiceLocator.game.phaseNavigator.isResPhaseDrawCard = !jqToggleBtn.hasClass( "skip" );

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_RESOURCE_PHASE_DRAW_CARD, null, [ ServiceLocator.game.phaseNavigator.isResPhaseDrawCard ] );
    }

    private onToggleStagingDrawBtn_Click( jqEvent: JQuery.Event ): void
    {
        let jqToggleBtn: JQuery<HTMLElement> = jQuery( jqEvent.target );
        if ( jqToggleBtn.hasClass( "skip" ) )
        {
            jqToggleBtn.removeClass( "skip" );
        }
        else 
        {
            jqToggleBtn.addClass( "skip" );
        }
        ServiceLocator.game.phaseNavigator.isStagingSubphaseDrawCard = !jqToggleBtn.hasClass( "skip" );

        // Multiplayer.
        ServiceLocator.socketIOManager.game.notifyAction( player_action_type.SET_STAGING_SUBPHASE_DRAW_CARD, null, [ ServiceLocator.game.phaseNavigator.isStagingSubphaseDrawCard ] );
    }

    private onWindow_Resized(): void
    {
        this.updateContentMaxHeight();
    }

    // #endregion //
}