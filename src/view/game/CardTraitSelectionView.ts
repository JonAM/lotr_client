import WindowedView from "../WindowedView";

import ServiceLocator from "../../ServiceLocator";

import Signal from "../../lib/signals/Signal";


export default class CardTraitSelectionView extends WindowedView
{
    // #region Attributes //

    // private:

    private _arrTrait: Array<string> = null;
    
    // Signals.
    private _onAccepted: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "game-card_trait_selection";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    public set selectedTraits( value: Array<string> ) {this._arrTrait = value; }

    // Signals.
    public get onAccepted(): Signal { return this._onAccepted; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( CardTraitSelectionView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#ctsAcceptBtn" ).on( "click", this.onAcceptBtn_Click.bind( this ) );
        this._jqRoot.find( "#ctsCancelBtn" ).on( "click", this.onSelf_Closed.bind( this ) );
    }

    public init(): void
    {
        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ { id: CardTraitSelectionView.kResId, isPreloadImages: true } ] } );

        if ( !this._arrTrait )
        {
            this._arrTrait = new Array<string>();
        }

        this.initRoleTraits();
        this.initLocationTraits();
        this.initPersonTraits();
        this.initOtherTraits();

    }

    public end(): void
    {
        this._onAccepted.removeAll();

        super.end();
    }

    // private:

    private initRoleTraits(): void
    {
        const kArrTrait: Array<string> = [ 
            "Crab", "Crane", "Dragon", "Keeper", "Lion", 
            "Phoenix", "Rōnin", "Scorpion", "Seeker", "Unicorn" ];
        this._jqRoot.find( "#ctsRoleTraits" ).append( this.createTraitCheckboxes( kArrTrait ) );
    }

    private initLocationTraits(): void
    {
        const kArrTrait: Array<string> = [ 
            "Academy", "Battlefield", "Castle", "City", "Dōjō", 
            "Garden", "Inn", "Kaiu Wall", "Laboratory", "Landmark", 
            "Library", "Marketplace", "Mine", "Outpost", "Palace", 
            "River", "Road", "Shrine", "Tea House", "Temple", 
            "Village", "Wasteland" ];
        this._jqRoot.find( "#ctsLocationTraits" ).append( this.createTraitCheckboxes( kArrTrait ) );
    }

    private initPersonTraits(): void
    {
        const kArrTrait: Array<string> = [ 
            "Actor", "Army", "Battle Maiden", "Berserker", "Bushi", 
            "Cavalry", "Champion", "Commander", "Courtier", "Creature", 
            "Crown Prince", "Daimyō", "Duelist", "Elemental Master", "Engineer", 
            "Geisha", "Goblin", "Heretic", "Informant", "Kenshinzen", 
            "Magistrate", "Merchant", "Monk", "Mythic", "Nezumi", 
            "Oni", "Peasant", "Rōnin", "Scholar", "Scout", 
            "Shadow", "Shinobi", "Shugenja", "Spirit", "Storyteller", 
            "Tattooed", "Wily Trader", "Yōjimbō" ];
        this._jqRoot.find( "#ctsPersonTraits" ).append( this.createTraitCheckboxes( kArrTrait ) );
    }

    private initOtherTraits(): void
    {
        const kArrTrait: Array<string> = [ 
            "Armor", "Banner", "Brotherhood", "Condition", "Creature", 
            "Curse", "Deer Clan", "Dragonfly Clan", "Festival", "Follower", 
            "Fox Clan", "Gaijin", "Heresy", "Imperial", "Item", 
            "Jade", "Kihō", "Mahō", "Mantis Clan", "Mask", 
            "Meishōdō", "Mount", "Omen", "Philosophy", "Poison", 
            "Ritual", "Seal", "Shadowlands", "Spell", "Tactic", 
            "Tattoo", "Technique", "Trap", "Weapon" ];
        this._jqRoot.find( "#ctsOtherTraits" ).append( this.createTraitCheckboxes( kArrTrait ) );
    }

    private createTraitCheckboxes( arrTrait: Array<string> ): JQuery<HTMLElement>
    {
        let jqDocFrament: JQuery<HTMLElement> = jQuery( document.createDocumentFragment() );
        let traitCounter: number = 0;
        let jqRow: JQuery<HTMLElement> = jQuery( "<div class=\"row\"></div>" );
        for ( let trait of arrTrait )
        {
            let jqCheckbox: JQuery<HTMLElement> = jQuery( "<input class=\"gb-custom_pointer sfx_check_click\" type=\"checkbox\" value=\"" + trait + "\">" );
            if ( this._arrTrait.indexOf( trait ) >= 0 )
            {
                jqCheckbox.prop( "checked", true );
            }
            jqCheckbox.on( "click", this.onCheckBox_Click.bind( this, jqCheckbox ) );
            let jqTrait: JQuery<HTMLElement> = jQuery( "<div class=\"trait\"></div>")
            jqTrait.append( jqCheckbox );
            jqTrait.append( "<div class=\"name gb-text-body-normal gb-text-ellipsis\">" + trait + "</div>" );
            jqRow.append( jqTrait );

            ++traitCounter;

            if ( traitCounter % 4 == 0 )
            {
                jqDocFrament.append( jqRow );
                jqRow = jQuery( "<div class=\"row\"></div>" ); 
            }
        }

        if ( traitCounter % 4 != 0 )
        {
            jqDocFrament.append( jqRow );
        }

        return jqDocFrament;
    }

    // #endregion //


    // #region Input Callbacks //

    private onSelf_Closed(): void
    {   
        ServiceLocator.viewManager.fadeOut( this );
    }

    private onCheckBox_Click( jqCheckbox: JQuery<HTMLElement> ): void
    {
        if ( jqCheckbox.prop( "checked" ) )
        {
            this._arrTrait.push( jqCheckbox.val() as string );
        }
        else
        {
            this._arrTrait.splice( this._arrTrait.indexOf( jqCheckbox.val() as string ), 1 );
        }
    }

    private onAcceptBtn_Click(): void
    {
        this._onAccepted.dispatch( this._arrTrait );

        this.onSelf_Closed();
    }

    // #endregion //
}