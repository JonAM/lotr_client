import WindowedView from "../../WindowedView";

import CampaignLogVO, { ISerializedCampaignLog } from "../../../vo/CampaignLogVO";
import CampaignLogDetailView from "../../common/CampaignLogView";
import ServiceLocator from "../../../ServiceLocator";
import Session from "../../../Session";
import Utils from "../../../Utils";
import { view_layer_id } from "../../../service/ViewManager";
import InfoMessageView from "../../common/popup/InfoMessageView";
import Signal from "../../../lib/signals/Signal";
import GameObject from "../../../game/GameObject";


export default class SaveCampaignLogView extends WindowedView
{
    // #region Attributes //

    // private:

    private _arrCampaignLogVO: Array<CampaignLogVO> = null;

    // Signals.
    private _onClosed: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "game-victory-save_campaign_log";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    // Signals.
    public get onClosed(): Signal { return this._onClosed; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( SaveCampaignLogView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#campaignLogSkipBtn" ).on( "click", this.onSkipBtn_Click.bind( this ) );
        this._jqRoot.find( "#campaignLogSaveBtn" ).on( "click", this.onSaveBtn_Click.bind( this ) );
    }

    public init(): void
    {
        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ 
                // 1st level.
                { id: SaveCampaignLogView.kResId, isPreloadImages: true },
                // 2nd level.
                { id: CampaignLogDetailView.kResId, isPreloadImages: false } ],
            isServerDataRequired: true } );

        ServiceLocator.dbConnector.post( { 
            serviceName: "CampaignLogDAO",
            methodName: "retrieve",
            parameters: [ Session.playerId ] },
            this.onDbCampaignLogRetrieve_Result.bind( this ) );
    }

    public end(): void
    {
        this._onClosed.removeAll();

        super.end();
    }

    // private:

    private createItemHtml( campaignLogVO: CampaignLogVO ): JQuery<HTMLElement>
    {
        let result: JQuery<HTMLElement> = null;

        result = jQuery( 
            "<div class=\"item gb-custom_pointer sfx_item_click\">" 
                + "<div class=\"title gb-text-body-normal gb-text-ellipsis\">" + campaignLogVO.title + "</div>"
                + "<div class=\"created gb-text-body-normal gb-text-ellipsis\">" + jQuery.i18n( "TIME_AGO" ).replace( "#", Utils.web.createElapsedTimeStr( campaignLogVO.whenTimestamp ) ) + "</div>"
                + "<button class=\"detail sfx_btn_click\"></button>"
                + "<button class=\"remove sfx_btn_click\"></button>"
            + "</div>" );
        result.find( "button.detail" ).on( "click", this.onDetailBtn_Click.bind( this, result ) );
        result.find( "button" ).on( "click", this.onRemoveBtn_Click.bind( this, result ) );
        result.on( "click", this.onItem_Click.bind( this, result ) );
        
        return result;
    }

    // #endregion //


    // #region Input Callbacks //

    private onItem_Click( jqItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        let jqItems: JQuery<HTMLElement> = this._jqRoot.find( "#campaignLogList>.item" );
        const kSelCampaignLogVO: CampaignLogVO = this._arrCampaignLogVO[ jqItems.index( jqItem ) ];

        jqItems.removeClass( "selected" );
        jqItem.addClass( "selected" ); 

        this._jqRoot.find( "#campaignLogTitle" ).val( kSelCampaignLogVO.title );
    }

    private onRemoveBtn_Click( jqItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        jqEvent.stopPropagation();

        this._jqLoadingAnimation.show();

        const kItemIndex: number = this._jqRoot.find( "#campaignLogList>.item" ).index( jqItem );
        const kCampaignLogVO: CampaignLogVO = this._arrCampaignLogVO[ kItemIndex ];

        ServiceLocator.dbConnector.post( { 
            serviceName: "CampaignLogDAO",
            methodName: "remove",
            parameters: [ kCampaignLogVO.title, Session.playerId, Session.token ] },
            this.onDbCampaignLogRemove_Result.bind( this, kItemIndex ) );
    }

    private onDetailBtn_Click( jqItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        jqEvent.stopImmediatePropagation();

        let treasureListDetailView: CampaignLogDetailView = new CampaignLogDetailView();
        const kItemIndex: number = this._jqRoot.find( "#campaignLogList>.item" ).index( jqItem );
        const kCampaignLogVO: CampaignLogVO = this._arrCampaignLogVO[ kItemIndex ];
        treasureListDetailView.campaignLogVO = kCampaignLogVO;
        treasureListDetailView.init();
        ServiceLocator.viewManager.fadeIn( treasureListDetailView, view_layer_id.POPUP );
    }

    private onSkipBtn_Click(): void
    {
        this._onClosed.dispatch();

        ServiceLocator.viewManager.fadeOut( this );
    }

    private onSaveBtn_Click(): void
    {
        const kTitle: string = ( this._jqRoot.find( "#campaignLogTitle" ).val() as string ).trim();
        if ( kTitle.length == 0 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "INVALID_TITLE" ), view_layer_id.POPUP );
        }
        else
        {
            this._jqLoadingAnimation.show();

            const kCampaignLog: GameObject = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "campaign_log" );
            const kSerializedCampaignLog: ISerializedCampaignLog = kCampaignLog.cCampaignLog.campaignLogVO.serialize();
            ServiceLocator.dbConnector.post( { 
                serviceName: "CampaignLogDAO",
                methodName: "add",
                parameters: [ 
                    kTitle, 
                    kSerializedCampaignLog.username, 
                    kSerializedCampaignLog.playerHeroes,
                    kSerializedCampaignLog.ally, 
                    kSerializedCampaignLog.allyHeroes,
                    kSerializedCampaignLog.fallenHeroes, 
                    kSerializedCampaignLog.threatPenalty,
                    kSerializedCampaignLog.notes,
                    kSerializedCampaignLog.completedScenarios,
                    kSerializedCampaignLog.boons,
                    kSerializedCampaignLog.burdens,
                    Session.token ] },
                this.onDbCampaignLogAdd_Result.bind( this ) );
        }
    }

    // #endregion //


    // #region Other Callbacks //

    private onDbCampaignLogRetrieve_Result( arrCampaignLogVO: Array<CampaignLogVO> ): void
    {
        this._arrCampaignLogVO = CampaignLogVO.parseArray( arrCampaignLogVO );
        let jqCampaignLogList: JQuery<HTMLElement> = this._jqRoot.find( "#campaignLogList" );
        for ( let saveGameVO of this._arrCampaignLogVO )
        {
            jqCampaignLogList.append( this.createItemHtml( saveGameVO ) );
        }
        this.registerSfx( jqCampaignLogList );

        this._viewPreloader.onServerDataLoaded();
    }

    private onDbCampaignLogAdd_Result( result: number ): void
    {
        if ( !this._id ) { return; }

        if ( result == 0 )
        {
            let infoMessageView: InfoMessageView = Utils.web.showInfoMessage( jQuery.i18n( "CAMPAIGN_LOG_SAVED" ), view_layer_id.POPUP );
            infoMessageView.onClosed.addOnce( () => { this.onSkipBtn_Click(); } );
        }
        else if ( result == 1 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "MAX_CAPACITY_REACHED" ), view_layer_id.POPUP );
        }
        else if ( result == 10 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
        }
        else if ( result == 11 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "INVALID_USER_TOKEN" ), view_layer_id.POPUP );

            // TODO: Go back to login screen.
        }

        this._jqLoadingAnimation.hide();
    }

    private onDbCampaignLogRemove_Result( itemIndex: number, result: number ): void
    {
        if ( !this._id ) { return; }

        if ( result == 0 )
        {
            this._arrCampaignLogVO.splice( itemIndex, 1 );
            this._jqRoot.find( "#campaignLogList>.item" ).eq( itemIndex ).remove();
        }
        else if ( result == 1 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "CAMPAIGN_LOG_NOT_FOUND" ), view_layer_id.POPUP );
        }
        else if ( result == 10 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
        }
        else if ( result == 11 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "INVALID_USER_TOKEN" ), view_layer_id.POPUP );

            // TODO: Go back to login screen.
        }
        
        this._jqLoadingAnimation.hide();
    }

    // #endregion //
}