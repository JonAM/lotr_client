import WindowedView from "../../WindowedView";

import TreasureListVO from "../../../vo/TreasureListVO";
import TreasureListDetailView from "../../gateway/game_room/TreasureListDetailView";
import ServiceLocator from "../../../ServiceLocator";
import Session from "../../../Session";
import Utils from "../../../Utils";
import { view_layer_id } from "../../../service/ViewManager";
import InfoMessageView from "../../common/popup/InfoMessageView";
import Signal from "../../../lib/signals/Signal";


export default class SaveTreasureListView extends WindowedView
{
    // #region Attributes //

    // private:

    private _arrTreasureListVO: Array<TreasureListVO> = null;

    // Signals.
    private _onClosed: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "game-victory-save_treasure_list";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    // Signals.
    public get onClosed(): Signal { return this._onClosed; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( SaveTreasureListView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#treasureListSkipBtn" ).on( "click", this.onSkipBtn_Click.bind( this ) );
        this._jqRoot.find( "#treasureListSaveBtn" ).on( "click", this.onSaveBtn_Click.bind( this ) );
    }

    public init(): void
    {
        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ 
                // 1st level.
                { id: SaveTreasureListView.kResId, isPreloadImages: true },
                // 2nd level.
                { id: TreasureListDetailView.kResId, isPreloadImages: false } ],
            isServerDataRequired: true } );

        ServiceLocator.dbConnector.post( { 
            serviceName: "TreasureListDAO",
            methodName: "retrieve",
            parameters: [ Session.playerId ] },
            this.onDbTreasureListRetrieve_Result.bind( this ) );
    }

    public end(): void
    {
        this._onClosed.removeAll();

        super.end();
    }

    // private:

    private createItemHtml( treasureListVO: TreasureListVO ): JQuery<HTMLElement>
    {
        let result: JQuery<HTMLElement> = null;

        result = jQuery( 
            "<div class=\"item gb-custom_pointer sfx_item_click\">" 
                + "<div class=\"title gb-text-body-normal gb-text-ellipsis\">" + treasureListVO.title + "</div>"
                + "<div class=\"created gb-text-body-normal gb-text-ellipsis\">" + jQuery.i18n( "TIME_AGO" ).replace( "#", Utils.web.createElapsedTimeStr( treasureListVO.whenTimestamp ) ) + "</div>"
                + "<button class=\"detail sfx_btn_click\"></button>"
                + "<button class=\"remove sfx_btn_click\"></button>"
            + "</div>" );
        result.find( "button.detail" ).on( "click", this.onDetailBtn_Click.bind( this, result ) );
        result.find( "button" ).on( "click", this.onRemoveBtn_Click.bind( this, result ) );
        result.on( "click", this.onItem_Click.bind( this, result ) );
        
        return result;
    }

    // #endregion //


    // #region Input Callbacks //

    private onItem_Click( jqItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        let jqItems: JQuery<HTMLElement> = this._jqRoot.find( "#treasureListList>.item" );
        const kSelTreasureListVO: TreasureListVO = this._arrTreasureListVO[ jqItems.index( jqItem ) ];

        jqItems.removeClass( "selected" );
        jqItem.addClass( "selected" ); 

        this._jqRoot.find( "#treasureListTitle" ).val( kSelTreasureListVO.title );
    }

    private onRemoveBtn_Click( jqItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        jqEvent.stopPropagation();

        this._jqLoadingAnimation.show();

        const kItemIndex: number = this._jqRoot.find( "#treasureListList>.item" ).index( jqItem );
        const kTreasureListVO: TreasureListVO = this._arrTreasureListVO[ kItemIndex ];

        ServiceLocator.dbConnector.post( { 
            serviceName: "TreasureListDAO",
            methodName: "remove",
            parameters: [ kTreasureListVO.title, Session.playerId, Session.token ] },
            this.onDbTreasureListRemove_Result.bind( this, kItemIndex ) );
    }

    private onDetailBtn_Click( jqItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        jqEvent.stopImmediatePropagation();

        let treasureListDetailView: TreasureListDetailView = new TreasureListDetailView();
        const kItemIndex: number = this._jqRoot.find( "#treasureListList>.item" ).index( jqItem );
        const kTreasureListVO: TreasureListVO = this._arrTreasureListVO[ kItemIndex ];
        treasureListDetailView.treasureListVO = kTreasureListVO;
        treasureListDetailView.init();
        ServiceLocator.viewManager.fadeIn( treasureListDetailView, view_layer_id.POPUP );
    }

    private onSkipBtn_Click(): void
    {
        this._onClosed.dispatch();

        ServiceLocator.viewManager.fadeOut( this );
    }

    private onSaveBtn_Click(): void
    {
        const kTitle: string = ( this._jqRoot.find( "#treasureListTitle" ).val() as string ).trim();
        if ( kTitle.length == 0 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "INVALID_TITLE" ), view_layer_id.POPUP );
        }
        else
        {
            this._jqLoadingAnimation.show();

            let isUpdate: boolean = false;
            for ( let treasureListVO of this._arrTreasureListVO )
            {
                if ( treasureListVO.title == kTitle )
                {
                    isUpdate = true;
                    break;
                }
            }

            let arrTreasureIndex: Array<number> = ServiceLocator.game.cGameWorld.cCustomPanelManager.find( "treasure_list" )
                .cTreasureList.findCheckedIndexes();
            if ( isUpdate )
            {
                ServiceLocator.dbConnector.post( { 
                    serviceName: "TreasureListDAO",
                    methodName: "update",
                    parameters: [ 
                        kTitle, 
                        Session.playerId, 
                        Session.scenarioId, 
                        arrTreasureIndex.length > 0 ? Utils.intArrayToStr( arrTreasureIndex ) : "",
                        Session.token ] },
                    this.onDbTreasureListUpdate_Result.bind( this ) );
            }
            else
            {
                ServiceLocator.dbConnector.post( { 
                    serviceName: "TreasureListDAO",
                    methodName: "add",
                    parameters: [ 
                        kTitle, 
                        Session.playerId, 
                        Utils.strArrayToStr( Session.playerDeck.hero ),
                        Session.allyId ? Session.allyId : "", 
                        Session.allyDeck ? Utils.strArrayToStr( Session.allyDeck.hero ) : "",
                        Session.scenarioId, 
                        arrTreasureIndex.length > 0 ? Utils.intArrayToStr( arrTreasureIndex ) : "",
                        Session.token ] },
                    this.onDbTreasureListAdd_Result.bind( this ) );
            }
        }
    }

    // #endregion //


    // #region Other Callbacks //

    private onDbTreasureListRetrieve_Result( arrTreasureListVO: Array<TreasureListVO> ): void
    {
        this._arrTreasureListVO = TreasureListVO.parseArray( arrTreasureListVO );
        let jqTreasureListList: JQuery<HTMLElement> = this._jqRoot.find( "#treasureListList" );
        for ( let saveGameVO of this._arrTreasureListVO )
        {
            jqTreasureListList.append( this.createItemHtml( saveGameVO ) );
        }
        this.registerSfx( jqTreasureListList );

        this._viewPreloader.onServerDataLoaded();
    }

    private onDbTreasureListUpdate_Result( result: number ): void
    {
        if ( !this._id ) { return; }

        if ( result == 0 )
        {
            let infoMessageView: InfoMessageView = Utils.web.showInfoMessage( jQuery.i18n( "TREASURE_LIST_SAVED" ), view_layer_id.POPUP );
            infoMessageView.onClosed.addOnce( () => { this.onSkipBtn_Click(); } );
        }
        else if ( result == 10 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
        }
        else if ( result == 11 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "INVALID_USER_TOKEN" ), view_layer_id.POPUP );

            // TODO: Go back to login screen.
        }

        this._jqLoadingAnimation.hide();
    }

    private onDbTreasureListAdd_Result( result: number ): void
    {
        if ( !this._id ) { return; }

        if ( result == 0 )
        {
            let infoMessageView: InfoMessageView = Utils.web.showInfoMessage( jQuery.i18n( "TREASURE_LIST_SAVED" ), view_layer_id.POPUP );
            infoMessageView.onClosed.addOnce( () => { this.onSkipBtn_Click(); } );
        }
        else if ( result == 1 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "MAX_CAPACITY_REACHED" ), view_layer_id.POPUP );
        }
        else if ( result == 10 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
        }
        else if ( result == 11 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "INVALID_USER_TOKEN" ), view_layer_id.POPUP );

            // TODO: Go back to login screen.
        }

        this._jqLoadingAnimation.hide();
    }

    private onDbTreasureListRemove_Result( itemIndex: number, result: number ): void
    {
        if ( !this._id ) { return; }

        if ( result == 0 )
        {
            this._arrTreasureListVO.splice( itemIndex, 1 );
            this._jqRoot.find( "#treasureListList>.item" ).eq( itemIndex ).remove();
        }
        else if ( result == 1 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "TREASURE_LIST_NOT_FOUND" ), view_layer_id.POPUP );
        }
        else if ( result == 10 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
        }
        else if ( result == 11 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "INVALID_USER_TOKEN" ), view_layer_id.POPUP );

            // TODO: Go back to login screen.
        }
        
        this._jqLoadingAnimation.hide();
    }

    // #endregion //
}