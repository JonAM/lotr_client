import WindowedView from "../../WindowedView";

import ServiceLocator from "../../../ServiceLocator";

import Signal from "../../../lib/signals/Signal";


export default class CustomTextEditionView extends WindowedView
{
    // #region Attributes //

    // private:
    private _text: string = null;

    // Signals.
    private _onAccepted: Signal = new Signal();
    private _onClosed: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "game-card_token-custom_text_edition";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    public set text( value: string ) { this._text = value; }

    // Signals.
    public get onAccepted(): Signal { return this._onAccepted; }
    public get onClosed(): Signal { return this._onClosed; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( CustomTextEditionView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#cteAcceptBtn" ).on( "click", this.onAcceptBtn_Click.bind( this ) );
        this._jqRoot.find( "#cteCancelBtn" ).on( "click", this.onSelf_Closed.bind( this ) );
    }

    public init(): void
    {
        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ { id: CustomTextEditionView.kResId, isPreloadImages: true } ] } );

        if ( this._text )
        {
            this._jqRoot.find( "#cteText" ).val( this._text );
        }
    }

    public end(): void
    {
        this._onAccepted.removeAll();
        this._onClosed.removeAll();

        super.end();
    }

    // #endregion //


    // #region Input Callbacks //

    private onSelf_Closed(): void
    {   
        ServiceLocator.viewManager.fadeOut( this );

        this._onClosed.dispatch();
    }

    private onAcceptBtn_Click(): void
    {
        this._onAccepted.dispatch( this._jqRoot.find( "#cteText" ).val() as string );

        this.onSelf_Closed();
    }

    // #endregion //
}