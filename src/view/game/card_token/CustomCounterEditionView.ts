import WindowedView from "../../WindowedView";

import ServiceLocator from "../../../ServiceLocator";

import Signal from "../../../lib/signals/Signal";


export default class CustomCounterEditionView extends WindowedView
{
    // #region Attributes //

    // private:

    private _count: number = null;
    private _tooltip: string = null;
    private _iconId: string = null;

    // Signals.
    private _onAccepted: Signal = new Signal();
    private _onClosed: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "game-card_token-custom_counter_edition";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    public set count( value: number ) { this._count = value; }
    public set tooltip( value: string ) { this._tooltip = value; }
    public set iconId( value: string ) { this._iconId = value; }

    // Signals.
    public get onAccepted(): Signal { return this._onAccepted; }
    public get onClosed(): Signal { return this._onClosed; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( CustomCounterEditionView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#cceIcon>.row>img" ).on( "click", this.onIconBtn_Click.bind( this ) );
        this._jqRoot.find( "#cceAcceptBtn" ).on( "click", this.onAcceptBtn_Click.bind( this ) );
        this._jqRoot.find( "#cceCancelBtn" ).on( "click", this.onSelf_Closed.bind( this ) );
    }

    public init(): void
    {
        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ { id: CustomCounterEditionView.kResId, isPreloadImages: true } ] } );

        this._jqRoot.find( "#cceCount" ).val( this._count != null ? this._count.toString() : "0" );
        if ( this._tooltip )
        {
            this._jqRoot.find( "#cceTooltip" ).val( this._tooltip );
        }
        if ( this._iconId )
        {
            this._jqRoot.find( "#cceIcon>.row>img.selected" ).removeClass( "selected" );
            this._jqRoot.find( "#cceIcon>.row>img[data-icon=\"" + this._iconId + "\"]" ).addClass( "selected" );
        }
    }

    public end(): void
    {
        this._onAccepted.removeAll();
        this._onClosed.removeAll();

        super.end();
    }

    // #endregion //


    // #region Input Callbacks //

    private onSelf_Closed(): void
    {   
        ServiceLocator.viewManager.fadeOut( this );

        this._onClosed.dispatch();
    }

    private onAcceptBtn_Click(): void
    {
        let count: number = parseInt( this._jqRoot.find( "#cceCount" ).val() as string );
        if ( Number.isNaN( count ) )
        {
            count = 0;
        }
        this._onAccepted.dispatch( 
            count,
            ( this._jqRoot.find( "#cceTooltip" ).val() as string ).trim(),
            this._jqRoot.find( "#cceIcon>.row>img.selected" ).data( "icon" ) as string );

        this.onSelf_Closed();
    }

    private onIconBtn_Click( jqEvent: JQuery.Event ): void
    {
        let jqIconBtn: JQuery<HTMLElement> = jQuery( jqEvent.target );
        this._jqRoot.find( "#cceIcon>.row>img.selected" ).removeClass( "selected" );
        jqIconBtn.addClass( "selected" );
    }

    // #endregion //
}