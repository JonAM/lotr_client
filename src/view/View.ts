import ServiceLocator from "../ServiceLocator";
import Utils from "../Utils";

import { IFileLoadInfo } from "../lib/HtmlFileLoader";
import Signal from "../lib/signals/Signal";
import ViewPreloader from "./ViewPreloader";


export default abstract class View
{
    // #region Attributes //

    // protected:

    protected _id: string = null;
    protected _alias: string = null;
    protected _cssNamespace: string = null;
    protected _arrStyleId: Array<string> = new Array<string>();
    protected _jqRoot: JQuery<HTMLElement> = null;
    protected _jqLoadingAnimation: JQuery<HTMLElement> = null;
    protected _viewPreloader: ViewPreloader = null;

    private static _autoIdSufix: number = 0;

    // Signals.
    private _onPreloaded: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get id(): string { return this._id };
    public get alias(): string { return this._alias; };
    public get cssNamespace(): string { return this._cssNamespace};
    public get styleIds(): Array<string> { return this._arrStyleId; };
    public get root(): JQuery<HTMLElement> { return this._jqRoot; };
    public get loadingAnimation(): JQuery<HTMLElement> { return this._jqLoadingAnimation; };
    
    public set alias( value: string ) { this._alias = value; };

    // Signals.
    public get onPreloaded(): Signal { return this._onPreloaded; }

    // #endregion //


    // #region Methods //

    protected constructor( resId: string ) 
    {
        // Retrieve the HTML document.
        let htmlResource: PIXI.LoaderResource = ServiceLocator.resourceStack.find( resId );
        if ( !htmlResource )
        {
            throw new Error( "Cannot find " + resId + " in ServiceLocator.resourceLoader" );
        }

        const kDomParser: DOMParser = new DOMParser();
        let htmlDoc: HTMLDocument = kDomParser.parseFromString( htmlResource.data, "text/html" );

        // Init attributes.
        let cssLinks: HTMLCollectionOf<HTMLLinkElement> = htmlDoc.head.getElementsByTagName( "link" );
        for ( let i = 0; i < cssLinks.length; ++i )
        {
            this._arrStyleId.push( Utils.web.hrefToId( cssLinks[ i ].href ) );
        }

        this._id = resId + "_" + View._autoIdSufix.toString();
        View._autoIdSufix += 1;
        if ( View._autoIdSufix >= Number.MAX_SAFE_INTEGER )
        {
            View._autoIdSufix = 0;
        }
        this._cssNamespace = resId;

        // Create the root element.
        this._jqRoot = jQuery( "<div/>", { id: this._id } );
        this._jqRoot.attr( "class", htmlDoc.body.className ); // to be used as CSS namespace.
        this._jqRoot.append( htmlDoc.body.innerHTML );

        // Localisation.
        this._jqRoot.i18n();

        // Add sfx.
        this.registerSfx( this._jqRoot );
    }

    // public:

    /**
     * Virtual. To be called before this view has been added to the document.
     */
    public init(): void 
    {
        this._jqLoadingAnimation = this._jqRoot.find( "#loadingAnimation" );
        if ( this._jqLoadingAnimation.length > 0 )
        {
            this._jqLoadingAnimation.hide();
            this._jqLoadingAnimation.append( Utils.web.createLoadingAnimHtml() );
        }
    }
    
    /**
     * Virtual. To be called after this view has been added to the document.
     */
    public postinit(): void {};
    
    /**
     * Virtual. To be called before this view has been removed from the document.
     */
    public preend(): void {}
    
    /**
     * Virtual. To be called after this view has been removed from the document.
     */
    public end(): void
    {
        if ( this._viewPreloader )
        {
            this._viewPreloader.end();
            this._viewPreloader = null;
        }

        // Owned signals.
        this._onPreloaded.removeAll();
        this._onPreloaded = null;

        this._id = null;
        this._cssNamespace = null;
        this._arrStyleId = null;
        this._jqLoadingAnimation = null;

        this._jqRoot.remove();
        this._jqRoot = null;
    }

    // Virtual.
    public show(): void
    {
        this._jqRoot.css( "display", "block" );
    }

    // Virtual.
    public hide(): void
    {
        this._jqRoot.css( "display", "none" );
    }

    public isVisible(): boolean
    {
        return this._jqRoot.css( "display" ) != "none";
    }

    // Virtual.
    public scale(): void {}

    // protected:

    protected preload( preloadParams: IPreloadParams ): void
    {
        this._viewPreloader = new ViewPreloader();
        this._viewPreloader.jqContent = preloadParams.jqContent;
        this._viewPreloader.isServerDataLoaded = !preloadParams.isServerDataRequired;
        if ( preloadParams.htmlFileInfoList )
        {
            this._viewPreloader.htmlFileInfoList = preloadParams.htmlFileInfoList;
        }
        if ( preloadParams.onCompleteCb )
        {
            this._viewPreloader.onCompleteCb = preloadParams.onCompleteCb;
        }
        if ( preloadParams.onHtmlFilesLoadedCb )
        {
            this._viewPreloader.onHtmlFilesLoadedCb = preloadParams.onHtmlFilesLoadedCb;
        }
        this._viewPreloader.loadingAnimation = this._jqLoadingAnimation;
        this._viewPreloader.onPreloaded.addOnce( () => { this._onPreloaded.dispatch(); } );
        this._viewPreloader.init();
    }

    protected addStyleIds( arrStyleId: Array<string> ): void
    {
        for ( let styleId of arrStyleId )
        {
            if ( this._arrStyleId.indexOf( styleId ) == -1 )
            {
                this._arrStyleId.push( styleId );
            }
        }
    }

    protected registerSfx( jqRoot: JQuery<HTMLElement> ): void
    {
        jqRoot.find( ".sfx_btn_click" ).on( "click", () => { ServiceLocator.audioManager.playSfx( "button_click" ); } );
        jqRoot.find( ".sfx_check_click" ).on( "click", () => { ServiceLocator.audioManager.playSfx( "button_click" ); } );
        jqRoot.find( ".sfx_combo_click" ).on( "click", () => { ServiceLocator.audioManager.playSfx( "button_click" ); } );
        jqRoot.find( ".sfx_item_click" ).on( "click", () => { ServiceLocator.audioManager.playSfx( "button_click" ); } );
        jqRoot.find( ".sfx_tab_click" ).on( "click", () => { ServiceLocator.audioManager.playSfx( "button_click" ); } );
    }

    // #endregion //
}

export interface IPreloadParams
{
    jqContent: JQuery<HTMLElement>;
    htmlFileInfoList?: Array<IFileLoadInfo>;
    isServerDataRequired?: boolean;
    onCompleteCb?: () => void; // server data and html files.
    onHtmlFilesLoadedCb?: () => void;
}