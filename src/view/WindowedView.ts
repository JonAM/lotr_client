import View from "./View";


export default abstract class WindowedView extends View
{
    // #region Attributes //

    // private:

    protected _scaleFactor: number = 1;

    // Constants.
    private static readonly _kMargin: number = 50;

    // #endregion //


    // #region Properties //

    public get scaleFactor(): number { return this._scaleFactor; }

    // #endregion //


    // #region Methods //

    protected constructor( resId: string )
    {
        super( resId );
    }

    // public:

    // Virtual.
    public scale(): void
    {
        let jqWindow: JQuery<HTMLElement> = null;
        if ( this._jqRoot.hasClass( "gb-popup-centered" ) )
        {
            jqWindow = this._jqRoot;
        }
        else
        {
            jqWindow = this._jqRoot.find( ".gb-popup-centered" );
        }
        this._scaleFactor = this.findScaleFactor( jqWindow[ 0 ], jQuery( window ).width(), jQuery( window ).height() );
        jqWindow.css( "transform", "translateX(-50%) translateY(-50%) scale(" + this._scaleFactor.toString() + ")" );
    
        this.watchWindowHeight( jqWindow, jqWindow.height() );
    }

    // private:

    private findScaleFactor( window: HTMLElement, winWidth: number, winHeight: number ): number
    {
        const kWindowedViewHeight: number = window.offsetHeight + WindowedView._kMargin;
        const kWindowedViewWidth: number = window.offsetWidth + WindowedView._kMargin;
        
        let heightScaleFactor: number = 1;
        if ( kWindowedViewHeight > winHeight )
        {
            heightScaleFactor = 1 - ( kWindowedViewHeight - winHeight ) / kWindowedViewHeight;
        }
        let widthScaleFactor: number = 1;
        if ( kWindowedViewWidth > winWidth )
        {
            widthScaleFactor = 1 - ( kWindowedViewWidth - winWidth ) / kWindowedViewWidth;
        }

        return Math.min( heightScaleFactor, widthScaleFactor );
    }

    private watchWindowHeight( jqWindow: JQuery<HTMLElement>, height: number ) 
    { 
        requestAnimationFrame( () => { 
            if ( this._id )
            {
                if ( jqWindow.height() != height )
                {
                    this.scale();
                } 
                else
                {
                    this.watchWindowHeight( jqWindow, height );
                }
            }
        } ); 
    }

    // #endregion //
}