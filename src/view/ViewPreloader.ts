import HtmlFileLoader, { IFileLoadInfo } from "../lib/HtmlFileLoader";
import Signal from "../lib/signals/Signal";


export default class ViewPreloader
{
    // #region Attributes //

    // private:

    private _jqContent: JQuery<HTMLElement> = null;
    private _isServerDataLoaded: boolean = null;
    private _arrHtmlFileInfo: Array<IFileLoadInfo> = null;
    private _jqLoadingAnimation: JQuery<HTMLElement> = null;
    private _onCompleteCb: () => void = null;
    private _onHtmlFilesLoadedCb: () => void = null;

    private _isWebFilesLoaded: boolean = null;
    private _htmlFileLoader: HtmlFileLoader = null;

    // Signal.
    private _onPreloaded: Signal = new Signal();

    // #endregion //


    // #region Properties //

    public get isServerDataLoaded(): boolean { return this._isServerDataLoaded; }

    public set jqContent( value: JQuery<HTMLElement> ) { this._jqContent = value; }
    public set isServerDataLoaded( value: boolean ) { this._isServerDataLoaded = value; }
    public set htmlFileInfoList( value: Array<IFileLoadInfo> ) { this._arrHtmlFileInfo = value; }
    public set loadingAnimation( value: JQuery<HTMLElement> ) { this._jqLoadingAnimation = value; }
    public set onCompleteCb( value: () => void ) { this._onCompleteCb = value; }
    public set onHtmlFilesLoadedCb( value: () => void ) { this._onHtmlFilesLoadedCb = value; }

    // Signals.
    public get onPreloaded(): Signal { return this._onPreloaded; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()  {}
    
    public init(): void
    {
        console.assert( this._jqContent != null, "ViewPreloader.ts :: init() :: this._jqContent cannot be null." );
        console.assert( this._isServerDataLoaded != null, "ViewPreloader.ts :: init() :: this._isServerDataLoaded cannot be null." );
        console.assert( this._jqLoadingAnimation != null, "ViewPreloader.ts :: init() :: this._jqLoadingAnimation cannot be null." );
    
        this._jqLoadingAnimation.show();
        this._jqContent.hide();

        this._isWebFilesLoaded = this._arrHtmlFileInfo == null;
        if ( !this._isWebFilesLoaded )
        {
            // HTML file loader.
            this._htmlFileLoader = new HtmlFileLoader();
            this._htmlFileLoader.htmlFileInfoList = this._arrHtmlFileInfo;
            this._htmlFileLoader.onCompleted.addOnce( this.onHtmlFiles_Loaded, this );
            this._htmlFileLoader.load();
        }
    }

    public end(): void
    {
        this._htmlFileLoader.end();
        this._htmlFileLoader = null;

        this._jqContent = null;
        this._isServerDataLoaded = null;
        this._isWebFilesLoaded = null;
        this._arrHtmlFileInfo = null;
        this._jqLoadingAnimation = null;
        this._onCompleteCb = null;
        this._onHtmlFilesLoadedCb = null;

        // Owned signals.
        this._onPreloaded.removeAll();
        this._onPreloaded = null;
    }

    public isDone(): boolean
    {
        return this._isServerDataLoaded && this._isWebFilesLoaded;
    }

    public onServerDataLoaded(): void
    {
        this._isServerDataLoaded = true;
        this.checkTaskListState();
    }

    // private:

    private checkTaskListState(): void
    {
        if ( this._isServerDataLoaded && this._isWebFilesLoaded )
        {
            if ( this._onCompleteCb )
            {
                this._onCompleteCb();
            }

            this._onPreloaded.dispatch();

            this._jqLoadingAnimation.fadeOut( "fast" );
            this._jqContent.show();
        }
    }

    // #endregion //


    // #region Callbacks //

    private onHtmlFiles_Loaded(): void
    {
        this._isWebFilesLoaded = true;

        if ( this._onHtmlFilesLoadedCb )
        {
            this._onHtmlFilesLoadedCb();
        }

        this.checkTaskListState();
    }

    // #endregion //
}