import CardDB, { ICard } from "../../game/CardDB";
import ServiceLocator from "../../ServiceLocator";


export default class DeckParser
{
    // #region Attributes //
    
    // private:

    private _text: string = null;

    private _arrInvalidCardId: Array<string> = null;

    // #endregion //


    // #region Properties //

    public get invalidCardIds(): Array<string> { return this._arrInvalidCardId; }

    public set text( value: string ) { this._text = value; }

    // #endregion //

    
    // #region Methods //

    // public:

    public constructor()
    {
        this._arrInvalidCardId = new Array<string>();
    }

    public parseText() : IDeck
    {
        console.assert( this._text != null, "DeckParser.ts :: parse() :: this._text cannot be null." );

        let deck: IDeck = this.createDeckForRingsDbText();
        this.validateDeck( deck );
        
        return deck;
    }

        private validateDeck( deck: IDeck ): void
        {
            const kArrCardId: Array<string> = deck.hero.concat( deck.player ).concat( deck.sideboard );
            if ( kArrCardId.length == 0 )
            {
                console.error( "DeckParser.ts :: parse() :: Invalid text: " + kArrCardId );
                throw error_type.INVALID_FORMAT;
            }
            else
            {
                if ( this._arrInvalidCardId.length > 0 )
                {
                    console.error( "DeckParser.ts :: parse() :: DB error: " + this._arrInvalidCardId );
                    throw error_type.DB_ERROR;
                }
            }
        }

    // private:

    private createDeckForRingsDbText(): IDeck 
    {
        let deck: IDeck = {
            hero: [],
            player: [],
            contract: null,
            sideboard: [] };

        let rawDeck: Array<string> = this._text.split( /\n+/ );
        const kHeroHeaderRegex: RegExp = /Hero \(\d\)/;
        const kContractHeaderRegex: RegExp = /Contract \(\d\)/;
        const kCardHeaderRegex: RegExp = /[^\(]+ \(\d+\)/;
        const kHeroItemRegex: RegExp = /([^\(]+) \(([^\)\d]+)\)/;
        const kCardItemRegex: RegExp = /(\d)x  ([^\(]+) \(([^\)]+)\)/;
        let scanPhase: number = 0;
        for ( let deckEntry of rawDeck )
        {
            if ( scanPhase == 0 )
            {
                if ( kHeroHeaderRegex.test( deckEntry ) )
                {
                    scanPhase = 1;
                }
            }
            else if ( scanPhase == 1 )
            {
                if ( kContractHeaderRegex.test( deckEntry ) )
                {
                    scanPhase = 2;
                }
                else if ( kCardHeaderRegex.test( deckEntry ) )
                {
                    scanPhase = 3;
                }
                else
                {
                    let match: RegExpExecArray = kHeroItemRegex.exec( deckEntry );
                    if ( match != null ) 
                    {
                        const kPackName: string = this.fixRingsDbPackName( match[ 2 ] );
                        let cardEntry: [string, ICard] = ServiceLocator.cardDb.findEntryByCardTitle( match[ 1 ], kPackName );
                        let isValidCard: boolean = cardEntry != null;
                        if ( isValidCard )
                        {
                            let card: ICard = cardEntry[ 1 ];
                            isValidCard = card.type_code && card.type_code == "hero";
                        }
                        if ( isValidCard )
                        {
                            deck.hero.push( cardEntry[ 0 ] );
                        }
                        else 
                        {
                            const kInvalidCardId: string = match[ 1 ] + " (" + match[ 2 ] + ")";
                            if ( this._arrInvalidCardId.indexOf( kInvalidCardId ) == -1 )
                            {
                                this._arrInvalidCardId.push( kInvalidCardId );
                            }
                        }
                    }
                }
            }
            else if ( scanPhase == 2 )
            {
                if ( kCardHeaderRegex.test( deckEntry ) )
                {
                    scanPhase = 3;
                }
                else
                {
                    let match: RegExpExecArray = kCardItemRegex.exec( deckEntry );
                    if ( match != null ) 
                    {
                        const kPackName: string = this.fixRingsDbPackName( match[ 3 ] );
                        let cardEntry: [string, ICard] = ServiceLocator.cardDb.findEntryByCardTitle( match[ 2 ], kPackName );
                        let isValidCard: boolean = cardEntry != null;
                        if ( isValidCard )
                        {
                            let card: ICard = cardEntry[ 1 ];
                            isValidCard = card.type_code && card.type_code == "contract";
                        }
                        if ( isValidCard )
                        {
                            deck.contract = cardEntry[ 0 ];
                        }
                        else 
                        {
                            const kInvalidCardId: string = match[ 2 ] + " (" + match[ 3 ] + ")";
                            if ( this._arrInvalidCardId.indexOf( kInvalidCardId ) )
                            {
                                this._arrInvalidCardId.push( kInvalidCardId );
                            }
                        }
                    }
                }
            }
            else if ( scanPhase == 3 )
            {
                if ( deckEntry == "Sideboard" )
                {
                    scanPhase = 4;
                }
                else
                {
                    let match: RegExpExecArray = kCardItemRegex.exec( deckEntry );
                    if ( match != null ) 
                    {
                        const kCardCount: number = parseInt( match[ 1 ] );
                        for ( let i: number = 0; i < kCardCount; ++i ) 
                        {
                            const kPackName: string = this.fixRingsDbPackName( match[ 3 ] );
                            let cardEntry: [string, ICard] = ServiceLocator.cardDb.findEntryByCardTitle( match[ 2 ], kPackName );
                            let isValidCard: boolean = cardEntry != null;
                            if ( isValidCard )
                            {
                                let card: ICard = cardEntry[ 1 ];
                                isValidCard = card.type_code 
                                    && ( card.type_code == "ally" || card.type_code == "attachment" || card.type_code == "event" || card.type_code == "player-side-quest" );
                            }
                            if ( isValidCard )
                            {
                                deck.player.push( cardEntry[ 0 ] );
                            }
                            else 
                            {
                                const kInvalidCardId: string = match[ 2 ] + " (" + match[ 3 ] + ")";
                                if ( this._arrInvalidCardId.indexOf( kInvalidCardId ) )
                                {
                                    this._arrInvalidCardId.push( kInvalidCardId );
                                }
                            }
                        } 
                    }
                }
            }
            else if ( scanPhase == 4 )
            {
                if ( deckEntry == "Deck built on http://ringsdb.com." )
                {
                    break;
                }

                let match: RegExpExecArray = kCardItemRegex.exec( deckEntry );
                if ( match ) 
                {
                    const kCardCount: number = parseInt( match[ 1 ] );
                    for ( let i: number = 0; i < kCardCount; ++i ) 
                    {
                        const kPackName: string = this.fixRingsDbPackName( match[ 3 ] );
                        let cardEntry: [string, ICard] = ServiceLocator.cardDb.findEntryByCardTitle( match[ 2 ], kPackName );
                        let isValidCard: boolean = cardEntry != null;
                        if ( isValidCard )
                        {
                            let card: ICard = cardEntry[ 1 ];
                            isValidCard = card.type_code 
                                && ( card.type_code == "hero" || card.type_code == "ally" || card.type_code == "attachment" || card.type_code == "event" || card.type_code == "player-side-quest" || card.type_code == "contract" );
                        }
                        if ( isValidCard )
                        {
                            deck.sideboard.push( cardEntry[ 0 ] );
                        }
                        else 
                        {
                            const kInvalidCardId: string = match[ 2 ] + " (" + match[ 3 ] + ")";
                            if ( this._arrInvalidCardId.indexOf( kInvalidCardId ) )
                            {
                                this._arrInvalidCardId.push( kInvalidCardId );
                            }
                        }
                    } 
                }
                
                if ( !match )
                {
                    let match: RegExpExecArray = kHeroItemRegex.exec( deckEntry );
                    if ( match ) 
                    {
                        const kPackName: string = this.fixRingsDbPackName( match[ 2 ] );
                        let cardEntry: [string, ICard] = ServiceLocator.cardDb.findEntryByCardTitle( match[ 1 ], kPackName );
                        let isValidCard: boolean = cardEntry != null;
                        if ( isValidCard )
                        {
                            let card: ICard = cardEntry[ 1 ];
                            isValidCard = card.type_code && card.type_code == "hero";
                        }
                        if ( isValidCard )
                        {
                            deck.sideboard.push( cardEntry[ 0 ] );
                        }
                        else 
                        {
                            const kInvalidCardId: string = match[ 1 ] + " (" + match[ 2 ] + ")";
                            if ( this._arrInvalidCardId.indexOf( kInvalidCardId ) == -1 )
                            {
                                this._arrInvalidCardId.push( kInvalidCardId );
                            }
                        }
                    }
                }
            }
        }

        return deck;
    }

        private fixRingsDbPackName( packName: string ): string
        {
            let result: string = packName;

            if ( result == "Over Hill and Under Hill"  || result == "On the Doorstep" )
            {
                result = "The Hobbit: " + result;
            }
            else if ( result.indexOf( "ALeP - " ) == 0 )
            {
                result = result.substr( 7 );
            }

            return result;
        }

    // #endregion //
}

export const enum error_type
{
    INVALID_FORMAT = 0,
    DB_ERROR
}

export interface IDeck
{
	hero: Array<string>;
	player: Array<string>;
    contract: string;
    sideboard: Array<string>;
}