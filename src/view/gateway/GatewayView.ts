import View from "../View";
import Config from "../../Config";
import ServiceLocator from "../../ServiceLocator";
import { sound_group_id } from "../../service/AudioManager";


export default class GatewayView extends View
{
    // #region Attributes //

    // private:

    // Constants.
    private static readonly _kResId: string = "gateway-gateway";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( GatewayView._kResId );

        // Attach event handlers.
        this._jqRoot.find( "#gatewayTavernAmbientToggleBtn" ).on( "click", this.onTavernAmbientToggleBtn_Click.bind( this ) );
        this._jqRoot.find( "#gatewayTavernAmbientVolumeBtn" ).on( "click", this.onTavernAmbientVolumeBtn_Click.bind( this ) );
        this._jqRoot.find( "#gatewayTavernAmbientVolumeBtn" ).on( "rightclick dragstart", this.onTavernAmbientVolumeBtn_RightClick.bind( this ) );
    }

    public init(): void
    {
        this._jqRoot.find( "#gatewayVersion" ).text( "v" + Config.kBuildVersion );

        this._jqRoot.find( "#gatewayTavernAmbient" ).hide();
    }

    public onUserConnected(): void
    {
        this._jqRoot.find( "#gatewayTavernAmbient" ).show();

        this._jqRoot.find( "#gatewayTavernAmbientVolumeBtn>.text" ).text( ( ServiceLocator.savedData.data.ambientVolume * 10 ).toString() );
    }

    public onUserDisconnected(): void
    {
        this._jqRoot.find( "#gatewayTavernAmbient" ).hide();
    }

    // #endregion //


    private onTavernAmbientToggleBtn_Click(): void
    {
        this._jqRoot.find( "#gatewayTavernAmbient" ).toggleClass( "on" );

        const kIsTavernAmbient: boolean = this._jqRoot.find( "#gatewayTavernAmbient" ).hasClass( "on" ); 
        ServiceLocator.audioManager.mute( !kIsTavernAmbient, sound_group_id.AMBIENT );

        ServiceLocator.savedData.data.isTavernAmbient = kIsTavernAmbient;
        ServiceLocator.savedData.save();
    }

    private onTavernAmbientVolumeBtn_Click( jqEvent: JQuery.Event ): void
    {
        let volume: number = ServiceLocator.savedData.data.ambientVolume * 10;
        if ( volume < 10 )
        {
            volume += 1;

            // Sfx.
            ServiceLocator.audioManager.playSfx( "counter_up" );
        }

        this.updateTavernAmbientVolume( volume );
    }

        private updateTavernAmbientVolume( volume: number ): void
        {
            const kAudioManagerVolume: number = volume / 10;
            if ( kAudioManagerVolume != ServiceLocator.savedData.data.ambientVolume )
            {
                ServiceLocator.audioManager.changeVolume( kAudioManagerVolume, sound_group_id.AMBIENT );
    
                ServiceLocator.savedData.data.ambientVolume = kAudioManagerVolume;
                ServiceLocator.savedData.save();
            
                this._jqRoot.find( "#gatewayTavernAmbientVolumeBtn>.text" ).text( volume.toString() );
            }
        }

    private onTavernAmbientVolumeBtn_RightClick( jqEvent: JQuery.Event ): void
    {
        let volume: number = ServiceLocator.savedData.data.ambientVolume * 10;
        if ( volume > 0 )
        {
            volume -= 1;

            // Sfx.
            ServiceLocator.audioManager.playSfx( "counter_down" );
        }

        this.updateTavernAmbientVolume( volume );
    }
}