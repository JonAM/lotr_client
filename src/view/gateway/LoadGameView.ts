import WindowedView from "../WindowedView";

import ServiceLocator from "../../ServiceLocator";
import Session from "../../Session";
import Utils from "../../Utils";
import { view_layer_id } from "../../service/ViewManager";

import SaveGameVO from "../../vo/SaveGameVO";
import Signal from "../../lib/signals/Signal";
import SaveGameDetailView from "../common/SaveGameDetailView";


export default class LoadGameView extends WindowedView
{
    // #region Attributes //

    // private:

    private _arrSaveGameVO: Array<SaveGameVO> = null;

    private _selSaveGameVO: SaveGameVO = null;

    // Signals.
    private _onClosed: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "gateway-load_game";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    public set saveGames( value: Array<SaveGameVO> ) { this._arrSaveGameVO = value; }

    // Signals.
    public get onClosed(): Signal { return this._onClosed; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( LoadGameView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#loadGameBackBtn" ).on( "click", this.onBackBtn_Click.bind( this ) );
        this._jqRoot.find( "#loadGameLoadBtn" ).on( "click", this.onLoadBtn_Click.bind( this ) );
    }

    public init(): void
    {
        console.assert( this._arrSaveGameVO != null, "LoadGameView.ts :: init() :: this._arrSaveGameVO cannot be null." );

        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ 
                // 1st level.
                { id: LoadGameView.kResId, isPreloadImages: true },
                // 2nd level.
                { id: SaveGameDetailView.kResId, isPreloadImages: false } ] } );

        this._jqRoot.find( "#loadGameLoadBtn" ).prop( "disabled", true );
        let jqLoadGameList: JQuery<HTMLElement> = this._jqRoot.find( "#loadGameList" );
        for ( let saveGameVO of this._arrSaveGameVO )
        {
            jqLoadGameList.append( this.createItemHtml( saveGameVO ) );
        }
        this.registerSfx( jqLoadGameList );
    }

    public end(): void
    {
        this._onClosed.removeAll();

        ServiceLocator.socketIOManager.gateway.onGameLoaded.remove( this.onSyncGame_Loaded, this );

        super.end();
    }

    // private:

    private createItemHtml( saveGameVO: SaveGameVO ): JQuery<HTMLElement>
    {
        let result: JQuery<HTMLElement> = null;

        result = jQuery( 
            "<div class=\"item gb-custom_pointer sfx_item_click\">" 
                + "<div class=\"title gb-text-body-normal gb-text-ellipsis\">" + saveGameVO.title + "</div>"
                + "<div class=\"created gb-text-body-normal gb-text-ellipsis\">" + jQuery.i18n( "TIME_AGO" ).replace( "#", Utils.web.createElapsedTimeStr( saveGameVO.msSinceCreation ) ) + "</div>"
                + "<button class=\"detail sfx_btn_click\"></button>"
                + "<button class=\"remove sfx_btn_click\"></button>"
            + "</div>" );
        result.find( "button.detail" ).on( "click", this.onDetailBtn_Click.bind( this, result ) );
        result.find( "button.remove" ).on( "click", this.onRemoveBtn_Click.bind( this, result ) );
        result.on( "click", this.onItem_Click.bind( this, result ) );
        
        return result;
    }

    // #endregion //


    // #region Sync Callbacks //

    private onSyncGame_Loaded(): void
    {
        ServiceLocator.viewManager.fadeOut( this );
    }

    // #endregion //


    // #region Input Callbacks //

    private onBackBtn_Click(): void
    {
        this._onClosed.dispatch();

        ServiceLocator.viewManager.fadeOut( this );
    }

    private onLoadBtn_Click(): void
    {
        this._jqLoadingAnimation.show();

        ServiceLocator.socketIOManager.gateway.onGameLoaded.add( this.onSyncGame_Loaded, this );
        ServiceLocator.socketIOManager.gateway.loadGame( JSON.parse( this._selSaveGameVO.content ) );
    }

    private onItem_Click( jqItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        let jqItems: JQuery<HTMLElement> = this._jqRoot.find( "#loadGameList>.item" );
        this._selSaveGameVO = this._arrSaveGameVO[ jqItems.index( jqItem ) ];

        jqItems.removeClass( "selected" );
        jqItem.addClass( "selected" ); 

        this._jqRoot.find( "#loadGameLoadBtn" ).prop( "disabled", false );
    }

    private onRemoveBtn_Click( jqItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        jqEvent.stopPropagation();

        this._jqLoadingAnimation.show();

        const kItemIndex: number = this._jqRoot.find( "#loadGameList>.item" ).index( jqItem );
        const kSaveGameVO: SaveGameVO = this._arrSaveGameVO[ kItemIndex ];

        ServiceLocator.dbConnector.post( { 
            serviceName: "SaveGameDAO",
            methodName: "remove",
            parameters: [ kSaveGameVO.title, Session.playerId, Session.token ] },
            this.onDbSaveGameRemove_Result.bind( this, kItemIndex ) );
    }

    private onDetailBtn_Click( jqItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        jqEvent.stopImmediatePropagation();

        let saveGameDetailView: SaveGameDetailView = new SaveGameDetailView();
        const kItemIndex: number = this._jqRoot.find( "#loadGameList>.item" ).index( jqItem );
        const kSaveGameVO: SaveGameVO = this._arrSaveGameVO[ kItemIndex ];
        saveGameDetailView.saveGameVO = kSaveGameVO;
        saveGameDetailView.init();
        ServiceLocator.viewManager.fadeIn( saveGameDetailView, view_layer_id.POPUP );
    }

    // #endregion //


    // #region Callbacks //

    private onDbSaveGameRemove_Result( itemIndex: number, result: number ): void
    {
        if ( !this._id ) { return; }

        if ( result == 0 )
        {
            if ( this._selSaveGameVO == this._arrSaveGameVO[ itemIndex ] )
            {
                this._selSaveGameVO = null;
                this._jqRoot.find( "#loadGameLoadBtn" ).prop( "disabled", true );
            }

            this._arrSaveGameVO.splice( itemIndex, 1 );
            this._jqRoot.find( "#loadGameList>.item" ).eq( itemIndex ).remove();
        }
        else if ( result == 1 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "SAVE_GAME_NOT_FOUND" ), view_layer_id.POPUP );
        }
        else if ( result == 10 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
        }
        else if ( result == 11 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "INVALID_USER_TOKEN" ), view_layer_id.POPUP );

            // TODO: Go back to login screen.
        }
        
        this._jqLoadingAnimation.hide();
    }

    // #endregion //
}