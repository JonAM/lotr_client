import View from "../View";

import ServiceLocator from "../../ServiceLocator";
import Session from "../../Session";
import PlayerProgressVO from "../../vo/PlayerProgressVO";
import Signal from "../../lib/signals/Signal";
import { ICardSet, IScenario } from "../../game/ScenarioDB";
import Utils from "../../Utils";
import WindowedView from "../WindowedView";


export default class ScenarioSelectionView extends WindowedView
{
    // #region Attributes //

    // private:

    private _arrPlayerProgressVO: Array<PlayerProgressVO> = null;
    private _jqSortedItems: JQuery<HTMLElement> = null;

    // Signals.
    private _onScenarioSelected: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "gateway-scenario_selection";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    // Signals.
    public get onScenarioSelected(): Signal { return this._onScenarioSelected; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( ScenarioSelectionView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#ppCloseBtn" ).on( "click", this.onCloseBtn_Click.bind( this ) );
        this._jqRoot.find( "#ppCardSetSelect" ).on( "change", this.onCardSet_Selected.bind( this ) );
        this._jqRoot.find( "#ppHideNightmareCheckbox" ).on( "click", this.onHideNightmareCheckbox_Click.bind( this ) );
    }

    public init(): void
    {
        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ { id: ScenarioSelectionView.kResId, isPreloadImages: true } ],
            isServerDataRequired: true } );

        if ( ServiceLocator.savedData.data.isHideNightmareScenarios )
        {
            this._jqRoot.find( "#ppHideNightmareCheckbox" ).prop( "checked", true );
        }

        this.loadCardSets();

        ServiceLocator.dbConnector.post( { 
            serviceName: "PlayerProgressDAO",
            methodName: "getProgress",
            parameters: [ Session.playerId ] },
            this.onPlayerProgress_Retrieved.bind( this ) );
    }

        private loadCardSets(): void
        {
            const kArrOptGroup: Array<Object> = [
                {
                    "label": "FFG",
                    "optionSubgroups": [ 
                        [ "01", "22", "02", "-", "03", "04", "-", "05", "06", "-", "07", "08", "-", "09", "10", "-", "11", "12", "-", "16", "17", "-", "18", "19", "-", "20", "21" ],
                        [ "13", "14" ],
                        [ "23" ],
                        [ "15" ] ]
                },
                {
                    "label": "Tales from the Cards",
                    "optionSubgroups": [ [ "80", "81" ] ]
                },
                {
                    "label": "ALeP",
                    "optionSubgroups": [ 
                        [ "90", "91" ],
                        [ "92" ] ]
                }
            ]
            let jqSelect: JQuery<HTMLElement> = this._jqRoot.find( "#ppCardSetSelect" );
            jqSelect.append( "<option disabled></option>" );
            for ( let optGroup of kArrOptGroup )
            {
                let jqOptGroup: JQuery<HTMLElement> = jQuery( "<optgroup label=\"" + optGroup[ "label" ] + "\"></optgroup>" );
                for ( let optionSubgroup of optGroup[ "optionSubgroups" ] )
                {
                    for ( let option of optionSubgroup )
                    {
                        let jqOption: JQuery<HTMLElement> = null;
                        if ( option == "-" )
                        {
                            jqOption = jQuery( "<option disabled>• • •</option>" );
                        }
                        else
                        {
                            const kCardSet: ICardSet = ServiceLocator.scenarioDb.find( option );
                            jqOption = jQuery( "<option value=\"" + option + "\">" + kCardSet.name + "</option>" );
                            if ( option == ServiceLocator.savedData.data.scenarioSelectionCardSet )
                            {
                                jqOption.prop( "selected", true );
                            }
                        }
                        jqOptGroup.append( jqOption );
                    }
                    jqOptGroup.append( "<option disabled></option>" );
                }
                
                jqSelect.append( jqOptGroup );
            }
        }

    public end(): void
    {
        this._onScenarioSelected.removeAll();

        super.end();
    }

    // private:

    private loadScenarios( cardSetId: string, arrPlayerProgressVO: Array<PlayerProgressVO> ): void
    {
        let jqScenarios: JQuery<HTMLElement> = this._jqRoot.find( "#ppScenarios" );
        jqScenarios.empty();

        let cardSet: ICardSet = ServiceLocator.scenarioDb.find( cardSetId );
        let jqRow: JQuery<HTMLElement> = jQuery( "<div class=\"row\"></div>" );
        for ( let scenario of cardSet.scenarios )
        {
            let bestScore: IBestScore = { single: null, multiplayer: null };
            for ( let playerProgressVO of arrPlayerProgressVO )
            {
                if ( playerProgressVO.scenarioId == scenario.id )
                {
                    if ( playerProgressVO.playerTwo == null )
                    {
                        if ( bestScore.single == null || playerProgressVO.score > bestScore.single.score )
                        {
                            bestScore.single = { score: playerProgressVO.score, when: playerProgressVO.whenTimestamp };
                        }
                    }
                    else
                    {
                        if ( bestScore.multiplayer == null || playerProgressVO.score > bestScore.multiplayer.score )
                        {
                            const kAlly: string = playerProgressVO.playerTwo == Session.playerId ? playerProgressVO.playerOne : playerProgressVO.playerTwo;
                            bestScore.multiplayer = { 
                                score: playerProgressVO.score, 
                                ally: kAlly,
                                when: playerProgressVO.whenTimestamp };
                        }
                    }
                }
            }
            if ( bestScore.single == null && bestScore.multiplayer == null )
            {
                bestScore = null;
            }
            let jqScenarioItem: JQuery<HTMLElement> = this.createScenarioItem( scenario, bestScore );
            if ( !scenario.under_construction )
            {
                jqScenarioItem.one( "click", this.onScenarioItem_Click.bind( this, scenario.id ) );
            }
            jqRow.append( jqScenarioItem );
            if ( jqRow.children().length == 2 )
            {        
                jqScenarios.append( jqRow );
                jqRow = jQuery( "<div class=\"row\"></div>" );
            }

            this._jqSortedItems = jqScenarios.find( ".item" );
        }
        if ( jqRow.length > 0 )
        {
            jqScenarios.append( jqRow );
        }
    }

        private createScenarioItem( scenario: IScenario, bestScore: IBestScore ): JQuery<HTMLElement>
        {
            let bestScoreHtml: string = "";
            let completedHtml: string = "";
            let underConstructionHtml: string = "";
            if ( bestScore )
            {
                bestScoreHtml = "<div class=\"best_score\">"
                if ( bestScore.single )
                {
                    bestScoreHtml += 
                        "<div class=\"single_player gb-text-shadow\">"
                            + "<img class=\"player_count\" src=\"./resources/images/gateway/single_player.png\">"
                            + "<div class=\"value gb-text-body-normal white gb-text-ellipsis\">" + bestScore.single.score.toString() + "</div>"
                            + "<div class=\"separator white\">|</div>"
                            + "<div class=\"when gb-text-body-small white\">" + jQuery.i18n( "TIME_AGO" ).replace( "#", Utils.web.createElapsedTimeStr( bestScore.single.when ) ) + "</div>"
                        + "</div>";
                }
                if ( bestScore.multiplayer )
                {
                    bestScoreHtml += 
                        "<div class=\"multiplayer gb-text-shadow\">"
                            + "<img class=\"player_count\" src=\"./resources/images/gateway/multiplayer.png\">"
                            + "<div class=\"value gb-text-body-normal white gb-text-ellipsis\">" + bestScore.multiplayer.score.toString() + "</div>"
                            + "<div class=\"separator white\">|</div>"
                            + "<div class=\"ally gb-text-body-small white\">" + bestScore.multiplayer.ally + "</div>"
                            + "<div class=\"separator white\">|</div>"
                            + "<div class=\"when gb-text-body-small white\">" + jQuery.i18n( "TIME_AGO" ).replace( "#", Utils.web.createElapsedTimeStr( bestScore.multiplayer.when ) ) + "</div>"
                        + "</div>";
                }
                bestScoreHtml += "</div>";

                completedHtml = "<img class=\"completed\" src=\"./resources/images/gateway/scenario_completed.png\">";
            }
            if ( scenario.under_construction )
            {
                underConstructionHtml = "<img class=\"completed\" src=\"./resources/images/gateway/scenario_under_construction.png\">";
            }

            let jqScenarioItem: JQuery<HTMLElement> = jQuery( 
                "<button class=\"item\">"
                    + "<img class=\"product_icon\" src=\"./resources/images/common/scenario/" + scenario.product_icon + ".png\">"
                    + "<div class=\"detail\">"
                        + "<div class=\"title gb-text-header-normal gb-text-shadow\">" + ( scenario.name ? scenario.name : scenario.encounter_set.main ) + "</div>"
                        + bestScoreHtml
                    + "</div>"
                    + completedHtml
                    + underConstructionHtml
                + "</button>" );
            if ( scenario.type == "nightmare" )
            {
                jqScenarioItem.addClass( "nightmare" );
            }

            return jqScenarioItem;
        }

    // #endregion //


    // #region Input Callbacks //

    private onCloseBtn_Click(): void
    {
        ServiceLocator.viewManager.fadeOut( this );
    }

    private onCardSet_Selected(): void
    {
        const kSelCardSet: string = this._jqRoot.find( "#ppCardSetSelect" ).val() as string;
        this.loadScenarios(  kSelCardSet, this._arrPlayerProgressVO );
        this.filterNightmareScenarios();

        ServiceLocator.savedData.data.scenarioSelectionCardSet = kSelCardSet;
        ServiceLocator.savedData.save();
    }

    private onHideNightmareCheckbox_Click(): void
    {
        ServiceLocator.savedData.data.isHideNightmareScenarios = !ServiceLocator.savedData.data.isHideNightmareScenarios;
        ServiceLocator.savedData.save();

        this.filterNightmareScenarios();
    }

        private filterNightmareScenarios(): void
        {
            let jqRows: JQuery<HTMLElement> = this._jqRoot.find( "#ppScenarios>.row" );
            let jqItems: JQuery<HTMLElement> = this._jqRoot.find( "#ppScenarios>.row>.item" );
            
            if ( ServiceLocator.savedData.data.isHideNightmareScenarios )
            {
                this._jqRoot.find( "#ppScenarios>.row>.nightmare" ).hide();

                let jqNormalItems: JQuery<HTMLElement> = jqItems.not( ".nightmare" );
                let jqNightmareItems: JQuery<HTMLElement> = jqItems.filter( ".nightmare" );
                jqItems = jQuery( jQuery.merge( jqNormalItems, jqNightmareItems ) );
            }
            else
            {
                this._jqRoot.find( "#ppScenarios>.row>.nightmare" ).show();

                jqItems = this._jqSortedItems;
            }

            for ( let i: number = 0; i < jqRows.length; ++i )
            {
                for ( let j: number = 0; j < 2; ++j )
                {
                    const kItemIndex: number = j + 2 * i;
                    if ( kItemIndex < jqItems.length )
                    {
                        jqRows.get( i ).append( jqItems.get( kItemIndex ) );
                    }
                }
            }
        }

    private onScenarioItem_Click( scenarioId: string, jqEvent: JQuery.Event ): void
    {
        ServiceLocator.viewManager.fadeOut( this );
        
        this._onScenarioSelected.dispatch( scenarioId, jQuery( jqEvent.currentTarget ).clone() );
        
    }

    // #endregion //


    // #region Other Callbacks //

    private onPlayerProgress_Retrieved( arrPlayerProgress: Array<Object> ): void
    {
        this._arrPlayerProgressVO = PlayerProgressVO.parseArray( arrPlayerProgress );
        this.loadScenarios( ServiceLocator.savedData.data.scenarioSelectionCardSet, this._arrPlayerProgressVO );
        this.filterNightmareScenarios();
        
        this._viewPreloader.onServerDataLoaded();
    }

    // #endregion //
}

interface IBestScore
{
    single: ISingleBestScore;
    multiplayer: IMultiplayerBestScore;
}

interface ISingleBestScore
{
    score: number;
    when: number;
}

interface IMultiplayerBestScore
{
    score: number;
    ally: string;
    when: number;
}