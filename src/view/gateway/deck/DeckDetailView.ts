import WindowedView from "../../WindowedView";

import Config from "../../../Config";
import Path from "../../../Path";
import ServiceLocator from "../../../ServiceLocator";

import { ICard } from "../../../game/CardDB";
import DeckVO from "../../../vo/DeckVO";
import { IDeck } from "../DeckParser";
import { IFileLoadInfo } from "../../../lib/HtmlFileLoader";


export default class DeckDetailView extends WindowedView
{
    // #region Attributes //

    // private:

    private _deckVO: DeckVO = null;

    // Constants.
    private static readonly _kResId: string = "gateway-deck-deck_detail";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    public set deckVO( value: DeckVO ) { this._deckVO = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( DeckDetailView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#deckDetailBackBtn" ).on( "click", this.onBackBtn_Click.bind( this ) );
    }

    public init(): void
    {
        console.assert( this._deckVO != null, "DeckDetailView.ts :: init() :: this._deckVO cannot be null." );

        super.init();

        let arrHtmlFileInfo: Array<IFileLoadInfo> = [  
            // 1st level.
            { id: DeckDetailView.kResId, isPreloadImages: true } ];
        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: arrHtmlFileInfo } );

        this._jqRoot.find( "#deckDetailTitle" ).text( this._deckVO.title );

        const kDeck: IDeck = JSON.parse( this._deckVO.content );
        this.initContent( kDeck );
    }

    // private:

    private initContent( deck: IDeck ): void
    {
        let mapHero: Map<string, number> = new Map<string, number>();
        let mapAlly: Map<string, number> = new Map<string, number>();
        let mapAttachment: Map<string, number> = new Map<string, number>();
        let mapEvent: Map<string, number> = new Map<string, number>();
        let mapSideQuest: Map<string, number> = new Map<string, number>();
        let mapSideboard: Map<string, number> = new Map<string, number>();
        const kArrCardId: Array<string> = deck.hero.concat( deck.player );
        for ( let cardId of kArrCardId )
        {
            let mapCardTypeAux: Map<string, number> = null;
            let cardInfo: ICard = ServiceLocator.cardDb.find( cardId );
            switch ( cardInfo.type_code )
            {
                case "hero": { mapCardTypeAux = mapHero; break; }
                case "ally": { mapCardTypeAux = mapAlly; break; }
                case "attachment": { mapCardTypeAux = mapAttachment; break; }
                case "event": { mapCardTypeAux = mapEvent; break; }
                case "player-side-quest": { mapCardTypeAux = mapSideQuest; break; }
            }
            if ( mapCardTypeAux.has( cardId ) )
            {
                mapCardTypeAux.set( cardId, mapCardTypeAux.get( cardId ) + 1 );
            }
            else
            {
                mapCardTypeAux.set( cardId, 1 );
            }
        }
        if ( deck.sideboard )
        {
            for ( let cardId of deck.sideboard )
            {
                if ( mapSideboard.has( cardId ) )
                {
                    mapSideboard.set( cardId, mapSideboard.get( cardId ) + 1 );
                }
                else
                {
                    mapSideboard.set( cardId, 1 );
                }
            }
        }
        
        if ( mapHero.size == 0 )
        {
            this._jqRoot.find( "#deckDetailHero" ).hide();
        }
        else
        {
            this._jqRoot.find( "#deckDetailHeroValue" ).append( this.createCardListHtml( mapHero ) );
        }
        if ( mapAlly.size == 0 )
        {
            this._jqRoot.find( "#deckDetailAlly" ).hide();
        }
        else
        {
            this._jqRoot.find( "#deckDetailAllyValue" ).append( this.createCardListHtml( mapAlly ) );
        }
        if ( mapAttachment.size == 0 )
        {
            this._jqRoot.find( "#deckDetailAttachment" ).hide();
        }
        else
        {
            this._jqRoot.find( "#deckDetailAttachmentValue" ).append( this.createCardListHtml( mapAttachment ) );
        }
        if ( mapEvent.size == 0 )
        {
            this._jqRoot.find( "#deckDetailEvent" ).hide();
        }
        else
        {
            this._jqRoot.find( "#deckDetailEventValue" ).append( this.createCardListHtml( mapEvent ) );
        }
        if ( mapSideQuest.size == 0 )
        {
            this._jqRoot.find( "#deckDetailSideQuest" ).hide();
        }
        else
        {
            this._jqRoot.find( "#deckDetailSideQuestValue" ).append( this.createCardListHtml( mapSideQuest ) );
        }
        if ( mapSideboard.size == 0 )
        {
            this._jqRoot.find( "#deckDetailSideboard" ).hide();
        }
        else
        {
            this._jqRoot.find( "#deckDetailSideboardValue" ).append( this.createCardListHtml( mapSideboard ) );
        }
    }

    private createCardListHtml( mapCardCount: Map<string, number> ): string
    {
        let result: string = "";

        mapCardCount.forEach( function( value: number, key: string ) {
            result += "<div>" + value.toString() + "x " + ServiceLocator.cardDb.find( key ).name + "</div>"
        } );

        return result;
    }

    // #endregion //


    // #region Input Callbacks //

    private onBackBtn_Click(): void
    {
        ServiceLocator.viewManager.fadeOut( this );
    }

    // #endregion //
}