import WindowedView from "../../WindowedView";

import { IFileLoadInfo } from "../../../lib/HtmlFileLoader";
import ServiceLocator from "../../../ServiceLocator";
import Session from "../../../Session";
import Utils from "../../../Utils";
import { view_layer_id } from "../../../service/ViewManager";

import Signal from "../../../lib/signals/Signal";
import DeckCreationView from "./DeckCreationView";
import DeckDetailView from "./DeckDetailView";
import DeckVO from "../../../vo/DeckVO";
import DeckValidator from "./DeckValidator";


export default class DeckManagementView extends WindowedView
{
    // #region Attributes //

    // private:

    private _isSelection: boolean = false;

    private _arrDeckVO: Array<DeckVO> = null;
    private _selDeckVO: DeckVO = null;

    // Signals.
    private _onClosed: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "gateway-deck-deck_management";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    public set isSelection( value: boolean ) { this._isSelection = value; }

    // Signals.
    public get onClosed(): Signal { return this._onClosed; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( DeckManagementView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#deckManagementBackBtn" ).on( "click", this.onBackBtn_Click.bind( this ) );
        this._jqRoot.find( "#deckManagementAcceptBtn" ).on( "click", this.onAcceptBtn_Click.bind( this ) );
        this._jqRoot.find( "#deckManagementCreateBtn" ).on( "click", this.onCreateBtn_Click.bind( this ) );
    }

    public init(): void
    {
        super.init();

        let arrHtmlFileInfo: Array<IFileLoadInfo> = [  
            // 1st level.
            { id: DeckManagementView.kResId, isPreloadImages: true },
            // 2ns level.
            { id: DeckCreationView.kResId, isPreloadImages: false },
            { id: DeckDetailView.kResId, isPreloadImages: false } ];
        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: arrHtmlFileInfo,
            isServerDataRequired: true } );

        if ( !this._isSelection )
        {
            this._jqRoot.find( "#deckManagementAcceptBtn" ).hide();
            this._jqRoot.find( "#deckManagementBackBtn" ).text( jQuery.i18n( "BACK" ) );
        }
        else
        {
            this._jqRoot.find( "#deckManagementTitle" ).text( jQuery.i18n( "DECK_SELECTION" ) );
        }

        ServiceLocator.dbConnector.post( { 
            serviceName: "DeckDAO",
            methodName: "retrieve",
            parameters: [ Session.playerId ] },
            this.onDbDeckRetrieve_Result.bind( this ) );
    }

    public end(): void
    {
        this._onClosed.removeAll();

        super.end();
    }

    // private:

    private createDeckItemHtml( deckVO: DeckVO ): JQuery<HTMLElement>
    {
        let result: JQuery<HTMLElement> = null;

        result = jQuery( 
            "<div class=\"item\">" 
                + "<div class=\"title gb-text-body-normal gb-text-ellipsis\">" + deckVO.title + "</div>"
                + "<button class=\"detail sfx_btn_click\"></button>"
                + "<button class=\"remove sfx_btn_click\"></button>"
            + "</div>" );
        result.find( "button.detail" ).on( "click", this.onDetailBtn_Click.bind( this, result ) );
        result.find( "button.remove" ).on( "click", this.onRemoveBtn_Click.bind( this, result ) );
        if ( this._isSelection )
        {
            result.on( "click", this.onDeckItem_Click.bind( this ) );
        }
        
        return result;
    }

    // #endregion //


    // #region Input Callbacks //

    private onBackBtn_Click(): void
    {
        this._onClosed.dispatch( null );
        ServiceLocator.viewManager.fadeOut( this );
    }

    private onAcceptBtn_Click(): void
    {
        let deckValidator: DeckValidator = new DeckValidator();
        if ( deckValidator.validate( JSON.parse( this._selDeckVO.content ) ) )
        {
            this._onClosed.dispatch( this._selDeckVO );
            ServiceLocator.viewManager.fadeOut( this );
        }
    }

    private onCreateBtn_Click(): void
    {
        let deckCreationView: DeckCreationView = new DeckCreationView();
        deckCreationView.onClosed.add( this.onDeckCreationView_Closed, this );
        deckCreationView.init();
        ServiceLocator.viewManager.fadeIn( deckCreationView, view_layer_id.POPUP );
    }

    private onDetailBtn_Click( jqDeckItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        jqEvent.stopPropagation();

        let deckDetailView: DeckDetailView = new DeckDetailView();
        const kDeckIndex: number = this._jqRoot.find( "#deckManagementList>.item" ).index( jqDeckItem );
        deckDetailView.deckVO = this._arrDeckVO[ kDeckIndex ];
        deckDetailView.init();
        ServiceLocator.viewManager.fadeIn( deckDetailView, view_layer_id.POPUP );
    }

    private onRemoveBtn_Click( jqDeckItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        jqEvent.stopPropagation();

        this._jqLoadingAnimation.show();

        const kDeckIndex: number = this._jqRoot.find( "#deckManagementList>.item" ).index( jqDeckItem );
        const kDeckVO: DeckVO = this._arrDeckVO[ kDeckIndex ];

        ServiceLocator.dbConnector.post( { 
            serviceName: "DeckDAO",
            methodName: "remove",
            parameters: [ kDeckVO.title, Session.playerId, Session.token ] },
            this.onDbDeckRemove_Result.bind( this, kDeckIndex ) );
    }

    private onDeckItem_Click( jqEvent: JQuery.Event ): void
    {
        let jqSelDeckItem: JQuery<HTMLElement> = jQuery( jqEvent.target );
        let jqDeckItems: JQuery<HTMLElement> = this._jqRoot.find( "#deckManagementList>.item" );
        this._selDeckVO = this._arrDeckVO[ jqDeckItems.index( jqSelDeckItem ) ];

        jqDeckItems.removeClass( "selected" );
        jqSelDeckItem.addClass( "selected" ); 

        this._jqRoot.find( "#deckManagementAcceptBtn" ).prop( "disabled", false );
    }

    // #endregion //


    // #region Other Callbacks //

    private onDbDeckRetrieve_Result( arrDeckVO: Array<DeckVO> ): void
    {
        if ( !this._id ) { return; }

        this._arrDeckVO = DeckVO.parseArray( arrDeckVO );
        
        this._selDeckVO = null;
        this._jqRoot.find( "#deckManagementList>.item" ).removeClass( "selected" );
        this._jqRoot.find( "#deckManagementAcceptBtn" ).prop( "disabled", true );

        let jqDeckManagementList: JQuery<HTMLElement> = this._jqRoot.find( "#deckManagementList" ).empty();
        for ( let deckVO of arrDeckVO )
        {
            jqDeckManagementList.append( this.createDeckItemHtml( deckVO ) );
        }
        this.registerSfx( jqDeckManagementList );

        if ( !this._viewPreloader.isServerDataLoaded )
        {
            this._viewPreloader.onServerDataLoaded();
        }
        else
        {
            this._jqLoadingAnimation.hide();
        }
    }

    private onDbDeckRemove_Result( deckIndex: number, result: number ): void
    {
        if ( !this._id ) { return; }

        if ( result == 0 )
        {
            if ( this._selDeckVO == this._arrDeckVO[ deckIndex ] )
            {
                this._selDeckVO = null;
                this._jqRoot.find( "#deckManagementAcceptBtn" ).prop( "disabled", true );
            }

            this._arrDeckVO.splice( deckIndex, 1 );
            this._jqRoot.find( "#deckManagementList>.item" ).eq( deckIndex ).remove();
        }
        else if ( result == 1 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "DECK_NOT_FOUND" ), view_layer_id.POPUP );
        }
        else if ( result == 10 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
        }
        else if ( result == 11 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "INVALID_USER_TOKEN" ), view_layer_id.POPUP );

            // TODO: Go back to login screen.
        }
        
        this._jqLoadingAnimation.hide();
    }

    private onDeckCreationView_Closed(): void
    {
        this._jqLoadingAnimation.show();

        ServiceLocator.dbConnector.post( { 
            serviceName: "DeckDAO",
            methodName: "retrieve",
            parameters: [ Session.playerId ] },
            this.onDbDeckRetrieve_Result.bind( this ) );
    }

    //  #endregion //
}