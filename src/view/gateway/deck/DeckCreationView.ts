import WindowedView from "../../WindowedView";

import { IFileLoadInfo } from "../../../lib/HtmlFileLoader";
import ServiceLocator from "../../../ServiceLocator";
import Session from "../../../Session";

import Signal from "../../../lib/signals/Signal";
import DeckParser, { IDeck, error_type } from "../DeckParser";
import Utils from "../../../Utils";
import Url from "../../../Url";
import InfoMessageView from "../../common/popup/InfoMessageView";
import { view_layer_id } from "../../../service/ViewManager";
import Path from "../../../Path";
import DeckValidator from "./DeckValidator";


export default class DeckCreationView extends WindowedView
{
    // #region Attributes //

    // private:

    // Signals.
    private _onClosed: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "gateway-deck-deck_creation";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    // Signals.
    public get onClosed(): Signal { return this._onClosed; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( DeckCreationView._kResId );

        // Attach UI event handlers.
        //this._jqRoot.find( "#deckCreationHowToRingsDbLinkBtn" ).on( "click", this.onHowToRingsDbLinkBtn_Click.bind( this ) );
        this._jqRoot.find( "#deckCreationHowToRingsDbTextBtn" ).on( "click", this.onHowToRingsDbTextBtn_Click.bind( this ) );
        this._jqRoot.find( "#deckCreationBackBtn" ).on( "click", this.onBackBtn_Click.bind( this ) );
        this._jqRoot.find( "#deckCreationCreateBtn" ).on( "click", this.onCreateBtn_Click.bind( this ) );
        this._jqRoot.find( "#deckCreationTitle" ).on( "keyup", this.onEnterKey_Pressed.bind( this ) );
        this._jqRoot.find( "#deckCreationRingsDbText" ).on( "keyup", this.onEnterKey_Pressed.bind( this ) );
    }

    public init(): void
    {
        super.init();

        let arrHtmlFileInfo: Array<IFileLoadInfo> = [  
            // 1st level.
            { id: DeckCreationView.kResId, isPreloadImages: true } ];
        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: arrHtmlFileInfo } );
    }

    public end(): void
    {
        this._onClosed.dispatch();

        super.end();
    }

    // private:

    private parseDeck( text: string, deckFormat: deck_format_type ): IDeck
    {
        let deck: IDeck = null;

        let deckParser: DeckParser = new DeckParser();
        deckParser.text = text;
        try 
        {
            if ( deckFormat == deck_format_type.RINGS_DB_LINK )
            {
                //deck = deckParser.parseLink();
            }
            else if ( deckFormat == deck_format_type.RINGS_DB_TEXT )
            {
                deck = deckParser.parseText();
            }
        }
        catch ( errorType )
        {
            if ( errorType == error_type.INVALID_FORMAT )
            {
                Utils.web.showInfoMessage( jQuery.i18n( "INVALID_DECK_TEXT_FORMAT" ), view_layer_id.POPUP );
            }
            else if ( errorType == error_type.DB_ERROR )
            {
                let messageHtml: string = jQuery.i18n( "CARD_TITLE_NOT_IN_DB" ) + ":<br><ul style=\"text-align:left;\">";
                for ( let invalidCardId of deckParser.invalidCardIds )
                {
                    messageHtml += "<li>" + invalidCardId + "</li>";
                }
                messageHtml += "</ul>";
                Utils.web.showInfoMessage( messageHtml, view_layer_id.POPUP );
            }
        }

        return deck;
    }

    private saveDeck( deckTitle: string, deck: IDeck ): void
    {
        this._jqLoadingAnimation.show();

        ServiceLocator.dbConnector.post( { 
            serviceName: "DeckDAO",
            methodName: "add",
            parameters: [ 
                deckTitle, 
                Session.playerId, 
                JSON.stringify( deck ), 
                Session.token ] },
            this.onDbAddDeck_Result.bind( this ) );
    }

    // #endregion //


    // #region Input Callbacks //

    private onHowToRingsDbLinkBtn_Click(): void
    {
        window.open( Url.webServer + "/" + Path.kResGateway + "how_to_ringsdb_link.jpg" );
    }

    private onHowToRingsDbTextBtn_Click(): void
    {
        window.open( Url.webServer + "/" + Path.kResGateway + "how_to_ringsdb_text.jpg" );
    }

    private onBackBtn_Click(): void
    {
        this._onClosed.dispatch();
        ServiceLocator.viewManager.fadeOut( this );
    }

    private onCreateBtn_Click(): void
    {
        let deckTitle: string = ( this._jqRoot.find( "#deckCreationTitle" ).val() as string ).trim();
        if ( deckTitle.length == 0 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "INVALID_TITLE" ), view_layer_id.POPUP );
        }
        else
        {
            /*let text: string = ( this._jqRoot.find( "#deckCreationRingsDbLink" ).val() as string ).trim();
            if ( text.length > 0 )
            {
                this._jqLoadingAnimation.show();

                const kDeckIdRegex: RegExp = /\/(\d+)\//;
                const kMatch: RegExpExecArray = kDeckIdRegex.exec( text );
                if ( kMatch != null ) 
                {
                    jQuery.get( Url.ringsDbServer + "/api/public/decklist/" + kMatch[ 1 ], this.onRingsDbRequest_Result.bind( this, deckTitle ) )
                        .fail( () => { 
                            Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
                            this._jqLoadingAnimation.hide();
                        } );
                }
            }*/

            let text: string = ( this._jqRoot.find( "#deckCreationRingsDbText" ).val() as string ).trim();
            if ( text.length > 0 )
            {
                this._jqLoadingAnimation.show();

                try
                {
                    const kDeck: IDeck = this.parseDeck( text, deck_format_type.RINGS_DB_TEXT );
                    let deckValidator: DeckValidator = new DeckValidator();
                    if ( kDeck && deckValidator.validate( kDeck ) )
                    {
                        this.saveDeck( deckTitle, kDeck );
                    }
                    else
                    {
                        this._jqLoadingAnimation.hide();
                    }
                }
                catch ( err )
                {
                    Utils.web.showInfoMessage( jQuery.i18n( "INVALID_DECK_ID" ), view_layer_id.POPUP );
                    
                    this._jqLoadingAnimation.hide();
                }
            }
        }
    }

    private onEnterKey_Pressed( jqEvent: JQuery.Event ): void
    {
        if ( jqEvent.key == "Enter" )
        {
            jqEvent.preventDefault();

            this.onCreateBtn_Click();
        }
    }

    // #endregion //


    // #region Other Callbacks //

    private onDbAddDeck_Result( result: number ): void
    {
        if ( !this._id ) { return; }

        switch ( result )
        {
            case 0:
            {
                let infoMessageView: InfoMessageView = Utils.web.showInfoMessage( jQuery.i18n( "DECK_CREATED" ), view_layer_id.POPUP );
                infoMessageView.onClosed.addOnce( () => { this.onBackBtn_Click(); } );
                break;
            }
            
            case 1:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "DUPLICATED_TITLE" ), view_layer_id.POPUP );
                break;
            }

            case 2:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "MAX_CAPACITY_REACHED" ), view_layer_id.POPUP );
                break;
            }

            case 10:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
                break;
            }

            case 11:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "INVALID_USER_TOKEN" ), view_layer_id.POPUP );

                // TODO: Go back to login screen.
                break;
            }
        }

        this._jqLoadingAnimation.hide();
    }

    private onRingsDbRequest_Result( deckTitle: string, data: any ): void
    {
        try
        {
            const kDeck: IDeck = this.parseDeck( JSON.stringify( data[ "slots" ] ), deck_format_type.RINGS_DB_LINK );
            let deckValidator: DeckValidator = new DeckValidator();
            if ( kDeck && deckValidator.validate( kDeck ) )
            {
                this.saveDeck( deckTitle, kDeck );
            }
            else
            {
                this._jqLoadingAnimation.hide();
            }
        }
        catch ( err )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "INVALID_DECK_ID" ), view_layer_id.POPUP );

            this._jqLoadingAnimation.hide();
        }
    }

    // #endregion //
}

const enum deck_format_type
{
    RINGS_DB_LINK = 0,
    RINGS_DB_TEXT
}