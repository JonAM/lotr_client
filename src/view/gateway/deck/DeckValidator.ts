import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import { view_layer_id } from "../../../service/ViewManager";

import { IDeck } from "../DeckParser";


export default class DeckValidator
{
    // #region Methods //

    // public:

    public constructor() {}

    public validate( deck: IDeck ): boolean
    {
        let arrErrorMessage: Array<string> = new Array<string>();

        this.validateDeckSizes( deck, arrErrorMessage );
        this.validateDeckCards( deck, arrErrorMessage );

        if ( arrErrorMessage.length > 0 )
        {
            let messageHtml: string = jQuery.i18n( "INVALID_DECK" ) + ":<br><ul style=\"text-align:left;\">";
            for ( let errorMessage of arrErrorMessage )
            {
                messageHtml += "<li>" + errorMessage + "</li>";
            }
            messageHtml += "</ul>";
            Utils.web.showInfoMessage( messageHtml, view_layer_id.POPUP );
        }

        return arrErrorMessage.length == 0;
    }

    private validateDeckSizes( deck: IDeck, arrErrorMessage: Array<string> ): void
    {
        if ( deck.player.length < 50 )
        {
            arrErrorMessage.push( jQuery.i18n( "INVALID_PLAYER_DECK_SIZE" )
                .replace( "#0", deck.player.length.toString() ) );
        }
        if ( ( !deck.contract || deck.contract != "022147" )
            && ( deck.hero.length < 1 || deck.hero.length > 3 ) )
        {
            arrErrorMessage.push( jQuery.i18n( "INVALID_HERO_COUNT" )
                .replace( "#0", deck.hero.length.toString() )
                .replace( "#1", "3" ) );
        }
        else if ( deck.contract 
            && deck.contract == "022147" // bonds of friendship.
            && ( deck.hero.length < 1 || deck.hero.length > 4 ) )
        {
            arrErrorMessage.push( jQuery.i18n( "INVALID_HERO_COUNT" )
                .replace( "#0", deck.hero.length.toString() )
                .replace( "#1", "4" ) );
        }
    }

    private validateDeckCards( deck: IDeck, arrErrorMessage: Array<string> ): void
    {
        let tooManyCopiesCards: string = ""; 
        let mapCardCopyCount: Map<string, number> = new Map<string, number>();
        for ( let cardId of deck.hero )
        {
            if ( mapCardCopyCount.has( cardId ) )
            {
                mapCardCopyCount.set( cardId, mapCardCopyCount.get( cardId ) + 1 );
            }
            else
            {
                mapCardCopyCount.set( cardId, 1 );
            }
        }
        mapCardCopyCount.forEach( function( value: number, key: string ) {
            if ( value > 1 )
            {
                tooManyCopiesCards += ServiceLocator.cardDb.find( key ).name + " x" + value.toString() + " (max. 1), ";
            } } );
        let arrCardId: Array<string> = deck.player.concat( deck.sideboard );
        for ( let cardId of arrCardId )
        {
            if ( mapCardCopyCount.has( cardId ) )
            {
                mapCardCopyCount.set( cardId, mapCardCopyCount.get( cardId ) + 1 );
            }
            else
            {
                mapCardCopyCount.set( cardId, 1 );
            }
        }
        mapCardCopyCount.forEach( function( value: number, key: string ) {
            if ( value > 3 )
            {
                tooManyCopiesCards += ServiceLocator.cardDb.find( key ).name + " x" + value.toString() + " (max. 3), ";
            } } );
        if ( tooManyCopiesCards.length > 0 )
        {
            arrErrorMessage.push( jQuery.i18n( "INVALID_CARD_COPY_COUNT" ) .replace( "#0", tooManyCopiesCards.substr( 0, tooManyCopiesCards.length - 2 ) ) );
        }
    }

    // #endregion //
}