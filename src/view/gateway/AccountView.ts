import WindowedView from "../WindowedView";

import { IFileLoadInfo } from "../../lib/HtmlFileLoader";
import ServiceLocator from "../../ServiceLocator";
import Session from "../../Session";
import Utils from "../../Utils";
import { view_layer_id } from "../../service/ViewManager";

import InfoMessageView, { btn_layout_type } from "../common/popup/InfoMessageView";
import Signal from "../../lib/signals/Signal";


export default class AccountView extends WindowedView
{
    // #region Attributes //

    // private:

    // Quick access.
    private _jqReceiveNotificationsCheckbox: JQuery<HTMLElement> = null;

    // Constants.
    private static readonly _kResId: string = "gateway-account";

    // Signals.
    private _onClosed: Signal = new Signal();
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    // Signals.
    public get onClosed(): Signal { return this._onClosed; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( AccountView._kResId );

        // Quick access.
        this._jqReceiveNotificationsCheckbox = this._jqRoot.find( "#accountReceiveNotificationsCheckbox" );

        // Attach UI event handlers.
        this._jqRoot.find( "#accountReceiveNotificationsCheckbox" ).on( "click", this.onReceiveNotificationsCheckbox_Click.bind( this ) );
        this._jqRoot.find( "#accountDeleteBtn" ).on( "click", this.onDeleteBtn_Click.bind( this ) );
        this._jqRoot.find( "#accountBackBtn" ).on( "click", this.onBackBtn_Click.bind( this ) );
    }

    public init(): void
    {
        super.init();

        let arrHtmlFileInfo: Array<IFileLoadInfo> = [  
            // 1st level.
            { id: AccountView.kResId, isPreloadImages: true } ];
        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: arrHtmlFileInfo,
            isServerDataRequired: true } );

        ServiceLocator.dbConnector.post( { 
            serviceName: "UserDAO",
            methodName: "getSettings",
            parameters: [ Session.playerId, Session.token ] },
            this.onDbGetSettings_Result.bind( this ) );
    }
    
    public end(): void
    {
        this._onClosed.removeAll();

        super.end();
    }

    // #endregion //


    // #region Input Callbacks //

    private onReceiveNotificationsCheckbox_Click(): void
    {
        this._jqLoadingAnimation.show();

        ServiceLocator.dbConnector.post( { 
            serviceName: "UserDAO",
            methodName: "setSettings",
            parameters: [ Session.playerId, Session.token, this._jqReceiveNotificationsCheckbox.prop( "checked" ) ] },
            this.onDbSetSettings_Result.bind( this ) );
    }

    private onBackBtn_Click(): void
    {
        ServiceLocator.viewManager.fadeOut( this );
    }

    private onDeleteBtn_Click(): void
    {
        let deleteConfirmationView: InfoMessageView = new InfoMessageView();
        deleteConfirmationView.message = jQuery.i18n( "DELETE_ACCOUNT_CONFIRMATION" );
        deleteConfirmationView.btnLayoutType = btn_layout_type.DOUBLE;
        deleteConfirmationView.onClosed.addOnce( this.onDeleteConfirmation_Closed, this );
        deleteConfirmationView.init();
        ServiceLocator.viewManager.fadeIn( deleteConfirmationView, view_layer_id.POPUP );
    }

    // #endregion //


    // #region DB Callbacks //

    private onDbGetSettings_Result( result: number, isReceiveNotifications: boolean ): void
    {
        if ( !this._id ) { return; }

        switch ( result )
        {
            case 0:
            {
                this._jqReceiveNotificationsCheckbox.prop( "checked", isReceiveNotifications );
                this._viewPreloader.onServerDataLoaded();
                break;
            }

            case 10:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
                this.onBackBtn_Click();
                break;
            }

            case 11:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "INVALID_USER_TOKEN" ), view_layer_id.POPUP );
                this.onBackBtn_Click();

                // TODO: Go back to login screen.
                break;
            }
        }
    }

    private onDbSetSettings_Result( result: number ): void
    {
        if ( !this._id ) { return; }

        switch ( result )
        {
            case 0: { this._jqLoadingAnimation.hide(); break; }

            case 10:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
                break;
            }

            case 11:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "INVALID_USER_TOKEN" ), view_layer_id.POPUP );

                // TODO: Go back to login screen.
                break;
            }
        }
    }

    private onDbDeleteAccount_Result( result: number ): void
    {
        if ( !this._id ) { return; }

        switch ( result )
        {
            case 0: 
            { 
                this._onClosed.dispatch( true );
                ServiceLocator.viewManager.remove( this );
                break; 
            }

            case 10:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
                break;
            }

            case 11:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "INVALID_USER_TOKEN" ), view_layer_id.POPUP );

                // TODO: Go back to login screen.
                break;
            }
        }
    }

    // #endregion //


    // #region Other Callbacks //

    private onDeleteConfirmation_Closed( result: boolean ): void
    {
        if ( result )
        {
            this._jqLoadingAnimation.show();

            ServiceLocator.dbConnector.post( { 
                serviceName: "UserDAO",
                methodName: "deleteAccount",
                parameters: [ Session.playerId, Session.token ] },
                this.onDbDeleteAccount_Result.bind( this ) );
        }
    }

    // #endregion //
}