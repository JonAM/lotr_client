import WindowedView from "../WindowedView";

import { IFileLoadInfo } from "../../lib/HtmlFileLoader";
import LoadGameView from "./LoadGameView";
import GameRoomCreatorController from "./game_room/GameRoomCreatorController";
import GameRoomGuestController from "./game_room/GameRoomGuestController";
import GameRoomController from "./game_room/GameRoomController";
import { sound_group_id } from "../../service/AudioManager";
import ServiceLocator from "../../ServiceLocator";
import TreasureListSelectionView from "./game_room/TreasureListSelectionView";
import CampaignLogSelectionView from "./game_room/CampaignLogSelectionView";
import DeckManagementView from "./deck/DeckManagementView";


export default class GameRoomView extends WindowedView
{
    // #region Attributes //

    // private:

    private _mode: game_room_type = null;
    private _controller: GameRoomController = null;

    // Constants.
    private static readonly _kResId: string = "gateway-game_room";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }
    public get controller(): GameRoomController { return this._controller; }

    public set mode( value: game_room_type ) { this._mode = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( GameRoomView._kResId );
    }

    public init(): void
    {
        console.assert( this._mode != null, "GameRoomView.ts :: init() :: this._mode cannot be null." );

        super.init();

        let arrHtmlFileInfo: Array<IFileLoadInfo> = null;
        if ( this._mode == game_room_type.HOST )
        {
            arrHtmlFileInfo = [  
                // 1st level.
                { id: GameRoomView.kResId, isPreloadImages: true },
                // 2nd level.
                { id: DeckManagementView.kResId, isPreloadImages: false },
                { id: TreasureListSelectionView.kResId, isPreloadImages: false },
                { id: CampaignLogSelectionView.kResId, isPreloadImages: false },
                { id: LoadGameView.kResId, isPreloadImages: false } ];

            this._controller = new GameRoomCreatorController();
        }
        else
        {
            arrHtmlFileInfo = [  
                // 1st level.
                { id: GameRoomView.kResId, isPreloadImages: true },
                // 2nd level.
                { id: DeckManagementView.kResId, isPreloadImages: false } ];

            this._controller = new GameRoomGuestController();
        }
        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: arrHtmlFileInfo,
            isServerDataRequired: true } );

        this._controller.view = this;
        this._controller.onServerDataLoaded.addOnce( this.onServerData_Loaded, this );
        this._controller.init();

        // Sfx.
        ServiceLocator.audioManager.playAmbient( "tavern_crowded_damped", { loop: true, randomStart: true } );
    }

    public end(): void
    {
        // Sfx.
        ServiceLocator.audioManager.stopAmbient( "tavern_crowded_damped" );

        this._controller.end();
        this._controller = null;

        this._mode = null;

        super.end();
    }

    // #endregion //


    // #region Callbacks //

    private onServerData_Loaded(): void
    {
        this._viewPreloader.onServerDataLoaded();
    }

    // #endregion //
}

export const enum game_room_type
{
    HOST = 0,
    GUEST
}