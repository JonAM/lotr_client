import WindowedView from "../../WindowedView";

import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import { view_layer_id } from "../../../service/ViewManager";

import InfoMessageView from "../../common/popup/InfoMessageView";


export default class RegisterView extends WindowedView
{
    // #region Attributes //

    // private:
    
    // Constants.
    private static readonly _kResId: string = "gateway-login-register";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( RegisterView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#registerBackBtn" ).on( "click", this.onBackBtn_Click.bind( this ) );
        this._jqRoot.find( "#registerDoBtn" ).on( "click", this.onRegisterBtn_Click.bind( this ) );
    }

    public init(): void
    {
        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ { id: RegisterView.kResId, isPreloadImages: true } ] } );
    }

    // #endregion //


    // #region Input Callbacks //

    private onRegisterBtn_Click(): void
    {
        const kUsername: string = ( this._jqRoot.find( "#registerUsername" ).val() as string ).substr( 0, 20 ).trim();
        const kPassword: string = ( this._jqRoot.find( "#registerPassword" ).val() as string ).substr( 0, 20 ).trim();
        const kRePassword: string = ( this._jqRoot.find( "#registerRePassword" ).val() as string ).substr( 0, 20 ).trim();
        const kEmail: string = ( this._jqRoot.find( "#registerEmail" ).val() as string ).substr( 0, 50 ).trim();
        
        let result: boolean =  kUsername.length > 0 && kPassword.length > 0 && kRePassword.length > 0 && kEmail.length > 0;
        if ( !result )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "ALL_FIELDS_REQUIRED" ), view_layer_id.POPUP );
        }

        // Format validations.
        if ( result )
        {
            // Validate username.
            const kUsernameRegEx: RegExp = /^[A-Za-z0-9_-]+$/;
            result = kUsernameRegEx.test( kUsername );
            if ( !result )
            {    
                Utils.web.showInfoMessage( jQuery.i18n( "INVALID_USERNAME_FORMAT" ), view_layer_id.POPUP );
            }

            if ( result )
            {
                // Validate password.
                const kPasswordRegEx: RegExp = /^[^ ]+$/;
                result = kPasswordRegEx.test( kPassword );
                if ( !result )
                {
                    Utils.web.showInfoMessage( jQuery.i18n( "INVALID_PASSWORD_FORMAT" ), view_layer_id.POPUP );
                }
            }

            if ( result )
            {
                // Validate repeated password.
                result = kPassword == kRePassword;
                if ( !result )
                {
                    Utils.web.showInfoMessage( jQuery.i18n( "NO_PASSWORD_MATCH" ), view_layer_id.POPUP );
                }
            }

            if ( result )
            {
                // Validate email.
                const kEmailRegEx: RegExp = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/g;
                result = kEmailRegEx.test( kEmail );
                if ( !result )
                {
                    Utils.web.showInfoMessage( jQuery.i18n( "INVALID_EMAIL_FORMAT" ), view_layer_id.POPUP );
                }
            }
        }

        if ( result )
        {
            this._jqLoadingAnimation.show();

            ServiceLocator.dbConnector.post( { 
                serviceName: "UserDAO",
                methodName: "register",
                parameters: [ kUsername, kPassword, kEmail ] },
                this.onDbRegister_Result.bind( this ) );
        }
    }

    private onBackBtn_Click(): void
    {
        ServiceLocator.viewManager.fadeOut( this );
    }

    // #endregion //


    // #region DB Callbacks /

    private onDbRegister_Result( result: number ): void
    {
        if ( this._id == null ) { return; }

        switch ( result )
        {
            case 0:
            {
                let im: InfoMessageView = Utils.web.showInfoMessage( jQuery.i18n( "USER_ACCOUNT_CREATED" ) + "<br><br>" + jQuery.i18n( "ACTIVATION_EMAIL_SENT" ), view_layer_id.POPUP );
                im.onClosed.addOnce( () => { this.onBackBtn_Click(); } );
                break;
            }

            case 1:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "USERNAME_ALREADY_TAKEN" ), view_layer_id.POPUP );
                break;
            }

            case 2:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "EMAIL_ALREADY_TAKEN" ), view_layer_id.POPUP );
                break;
            }

            case 10:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
                break;
            }
        }

        this._jqLoadingAnimation.hide();
    }

    // #endregion //
}