import WindowedView from "../../WindowedView";

import ServiceLocator from "../../../ServiceLocator";
import Utils from "../../../Utils";
import { view_layer_id } from "../../../service/ViewManager";

import InfoMessageView from "../../common/popup/InfoMessageView";


export default class ForgottenPasswordView extends WindowedView
{
    // #region Attributes //

    // private:

    // Constants.
    private static readonly _kResId: string = "gateway-login-forgotten_password";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( ForgottenPasswordView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#forgottenPasswordBackBtn" ).on( "click", this.onBackBtn_Click.bind( this ) );
        this._jqRoot.find( "#forgottenPasswordDoBtn" ).on( "click", this.onRecoverBtn_Click.bind( this ) );
    }

    public init(): void
    {
        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ { id: ForgottenPasswordView.kResId, isPreloadImages: true } ] } );
    }

    // #endregion //


    // #region Input Callbacks //

    private onRecoverBtn_Click( jqEvent: JQuery.Event ): void
    {
        const kEmail: string = ( this._jqRoot.find( "#forgottenPasswordEmail" ).val() as string ).trim();
        let result: boolean = kEmail.length > 0;
        if ( !result )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "ALL_FIELDS_REQUIRED" ), view_layer_id.POPUP );
        }

        if ( result )
        {
            // Validate email.
            const kEmailRegEx: RegExp = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/g;
            result = kEmailRegEx.test( kEmail );
            if ( !result )
            {
                Utils.web.showInfoMessage( jQuery.i18n( "INVALID_EMAIL_FORMAT" ), view_layer_id.POPUP );
            }
        }

        if ( result )
        {   
            this._jqLoadingAnimation.show();

            ServiceLocator.dbConnector.post( { 
                serviceName: "UserDAO",
                methodName: "recover",
                parameters: [ kEmail ] },
                this.onDbRecover_Result.bind( this ) );
        }
    }

    private onBackBtn_Click(): void
    {
        ServiceLocator.viewManager.fadeOut( this );
    }

    // #endregion //


    // #region DB Callbacks /

    private onDbRecover_Result( result: number ): void
    {
        if ( this._id == null ) { return; }

        switch ( result )
        {
            case 0:
            {
                let im: InfoMessageView = Utils.web.showInfoMessage( jQuery.i18n( "RECOVERY_EMAIL_SENT" ), view_layer_id.POPUP );
                im.onClosed.addOnce( () => { this.onBackBtn_Click(); } );
                break;
            }

            case 1:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "EMAIL_NOT_FOUND" ), view_layer_id.POPUP );
                break;
            }

            case 10:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
                break;
            }
        }

        this._jqLoadingAnimation.hide();
    }

    // #endregion //
}