import WindowedView from "../WindowedView";

import { IFileLoadInfo } from "../../lib/HtmlFileLoader";
import ServiceLocator from "../../ServiceLocator";
import Session from "../../Session";
import Url from "../../Url";
import { view_layer_id } from "../../service/ViewManager";

import GameRoomView from "./GameRoomView";
import NewsVO from "../../vo/NewsVo";
import DeckManagementView from "./deck/DeckManagementView";
import LoginView from "./LoginView";
import Signal from "../../lib/signals/Signal";
import AccountView from "./AccountView";
import ScenarioSelectionView from "../gateway/ScenarioSelectionView";
import { Elastic, Sine, TimelineLite, TweenMax } from "gsap";


export default class DojoView extends WindowedView
{
    // #region Attributes //

    // private:

    private _meowTimeline: TimelineLite = null;
    private _meowTimeout: number = null;

    // Signals.
    private _onPlay: Signal = new Signal();
    private _onSessionClosed: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "gateway-dojo";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    // Signals.
    public get onPlay(): Signal { return this._onPlay; }
    public get onSessionClosed(): Signal { return this._onSessionClosed; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( DojoView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#dojoLeaveBtn" ).on( "click", this.onLeaveBtn_Click.bind( this ) );
        this._jqRoot.find( "#dojoHelpBtn" ).on( "click", this.onHelpBtn_Click.bind( this ) );
        this._jqRoot.find( "#dojoDeckBtn" ).on( "click", this.onDeckBtn_Click.bind( this ) );
        this._jqRoot.find( "#dojoAccountBtn" ).on( "click", this.onAccountBtn_Click.bind( this ) );
        this._jqRoot.find( "#dojoDiscordBtn" ).on( "click", this.onDiscordBtn_Click.bind( this ) );
        this._jqRoot.find( "#dojoEmailBtn" ).on( "click", this.onEmailBtn_Click.bind( this ) );
        this._jqRoot.find( "#donate-button" ).on( "click", this.onDonateBtn_Click.bind( this ) );
        this._jqRoot.find( "#dojoPlayBtn" ).on( "click", this.onPlayBtn_Click.bind( this ) );
    }

    public init(): void
    {
        super.init();

        let arrHtmlFileInfo: Array<IFileLoadInfo> = [  
            // 1st level.
            { id: DojoView.kResId, isPreloadImages: true },
            // 2nd level.
            { id: LoginView.kResId, isPreloadImages: false },
            { id: GameRoomView.kResId, isPreloadImages: false },
            { id: DeckManagementView.kResId, isPreloadImages: false },
            { id: AccountView.kResId, isPreloadImages: false },
            { id: ScenarioSelectionView.kResId, isPreloadImages: false } ];
        this.preload( { 
            isServerDataRequired: true,
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: arrHtmlFileInfo } );

        this._jqRoot.find( "#dojoMeow" ).css( "opacity", 0 );
        this._meowTimeline = new TimelineLite();
        this._meowTimeline.fromTo( this._jqRoot.find( "#dojoMeow" ), { css: { scaleY: 0, opacity: 1 } }, { css: { scaleY: 1 }, ease: Elastic.easeOut, duration: 1 } );
        this._meowTimeline.to( this._jqRoot.find( "#dojoMeow" ), { css: { opacity: 0 }, ease: Sine.easeIn, duration: 1, callbackScope: this, onComplete: this.onMeowAnim_Complete }, "+=1" );    
        this._meowTimeline.pause();
        this.onMeowAnim_Complete();

        // Sfx.
        ServiceLocator.audioManager.playAmbient( "tavern_crowded", { loop: true, randomStart: true } );
    
        // Retrieve news.
        ServiceLocator.dbConnector.post( {
            serviceName: "NewsDAO",
            methodName: "retrieve" },
            this.onNews_Retrieved.bind( this ) );
    }

    public end(): void
    {
        // Sfx.
        ServiceLocator.audioManager.stopAmbient( "tavern_crowded" );

        this._onPlay.removeAll();
        this._onSessionClosed.removeAll();

        this._meowTimeline.kill();
        this._meowTimeline = null;
        if ( this._meowTimeout )
        {
            window.clearTimeout( this._meowTimeout );
            this._meowTimeout = null;
        }

        super.end();
    }

    // private:

    private createNewsItemHtml( newsVO: NewsVO ): JQuery<HTMLElement>
    {
        return jQuery( 
            "<div class=\"item\">" 
                + "<div class=\"title gb-text-header-normal\">" + newsVO.title + "</div>"
                + "<div class=\"date gb-text-body-normal\">" + newsVO.creationDate + "</div>"
                + "<div class=\"message gb-text-body-normal\">" + newsVO.message + "</div>"
            + "</div>" );
    }

    // #endregion //


    // #region Input Callbacks //

    private onLeaveBtn_Click(): void
    {
        Session.reset();
        Session.playerId = null;
        Session.token = null;

        ServiceLocator.socketIOManager.disconnect();

        this._onSessionClosed.dispatch();

        ServiceLocator.viewManager.fadeOut( this );
    }

    private onHelpBtn_Click(): void
    {
        window.open( Url.help );
    }

    private onDeckBtn_Click(): void
    {
        let deckManagementView: DeckManagementView = new DeckManagementView();
        deckManagementView.init();
        ServiceLocator.viewManager.fadeIn( deckManagementView, view_layer_id.POPUP );
    }

    private onDiscordBtn_Click(): void
    {
        window.open( "https://discord.gg/JF7xd2qngy" );
    }

    private onEmailBtn_Click(): void
    {
        window.open( "mailto:lotrtheprancingpony@gmail.com" );
        //window.open( "mailto:jon@theprancingpony.es" );
    }

    private onDonateBtn_Click(): void
    {
        ServiceLocator.audioManager.playSfx( "tip_jar" );
    }

    private onAccountBtn_Click(): void
    {
        let accountView: AccountView = new AccountView();
        accountView.onClosed.add( ( result: boolean ) => { if ( result ) { this.onLeaveBtn_Click(); } } );
        accountView.init();
        ServiceLocator.viewManager.fadeIn( accountView, view_layer_id.POPUP );
    }

    private onPlayBtn_Click(): void
    {
        Session.reset();

        this._onPlay.dispatch();
    }

    // #endregion //


    // #region Other Callbacks //

    private onNews_Retrieved( arrNews: Array<Object> ): void
    {
        if ( !this._id ) { return; }

        let jqDocumentFragment: JQuery<HTMLElement> = jQuery( document.createDocumentFragment() );
        let arrNewsVO: Array<NewsVO> = NewsVO.parseArray( arrNews );
        for ( let newsVO of arrNewsVO )
        {
            jqDocumentFragment.append( this.createNewsItemHtml( newsVO ) );
            if ( newsVO != arrNewsVO[ arrNewsVO.length - 1 ] )
            {
                jqDocumentFragment.append( "<hr class=\"rounded\">" );
            }
        }
        this._jqRoot.find( "#dojoNews" ).append( jqDocumentFragment );

        ServiceLocator.dbConnector.post( { 
            serviceName: "UserDAO",
            methodName: "getUserCount" },
            this.onDbGetUserCount_Result.bind( this ) );
    }

    private onDbGetUserCount_Result( userCount: number ): void
    {
        if ( !this._id ) { return; }

        this._jqRoot.find( "#dojoPlayerCount" ).text( jQuery.i18n( "PLAYER_COUNT" ).replace( "#", userCount.toString() ) );

        this._viewPreloader.onServerDataLoaded();
    }

    private onMeowAnim_Complete(): void
    {
        this._meowTimeout = window.setTimeout( () => { 
            if ( Math.random() >= 0.25 ) 
            { 
                this._meowTimeline.play( 0 ); 
            } 
            else
            {
                this.onMeowAnim_Complete();
            }
        }, 30000 - 15000 * Math.random() );
    }

    // #endregion //
}