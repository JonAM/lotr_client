import WindowedView from "../WindowedView";

import { IFileLoadInfo } from "../../lib/HtmlFileLoader";
import ServiceLocator from "../../ServiceLocator";
import Session from "../../Session";
import Utils from "../../Utils";

import DojoView from "./DojoView";
import Signal from "../../lib/signals/Signal";
import { view_layer_id } from "../../service/ViewManager";
import RegisterView from "./login/RegisterView";
import ForgottenPasswordView from "./login/ForgottenPasswordView";
import InfoMessageView, { btn_layout_type } from "../common/popup/InfoMessageView";
import SignalBinding from "../../lib/signals/SignalBinding";
import { sound_group_id } from "../../service/AudioManager";


export default class LoginView extends WindowedView
{
    // #region Attributes //

    // private:

    private _connectErrorCount: number = 0;

    // Quick access.
    private _jqRememberMeCheckbox: JQuery = null;
    
    // Signals.
    private _onLoginSuccess: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "gateway-login";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    // Signals.
    public get onLoginSuccess(): Signal { return this._onLoginSuccess; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( LoginView._kResId );

        // Quick access.
        this._jqRememberMeCheckbox = this._jqRoot.find( "#loginRememberMeCheckbox" );

        // Attach UI event handlers.
        this._jqRoot.find( "#loginUsername" ).on( "keyup", this.onEnterKey_Pressed.bind( this ) );
        this._jqRoot.find( "#loginPassword" ).on( "keyup", this.onEnterKey_Pressed.bind( this ) );
        this._jqRoot.find( "#loginConnectBtn" ).on( "click", this.onConnectBtn_Click.bind( this ) );
        this._jqRoot.find( "#loginRegisterBtn" ).on( "click", this.onRegisterBtn_Click.bind( this ) );
        this._jqRoot.find( "#loginForgottenPasswordLink" ).on( "click", this.onForgottenPasswordLink_Click.bind( this ) );
        this._jqRememberMeCheckbox.on( "click", this.onRememberMeCheckbox_Click.bind( this ) );
    }

    public init(): void
    {
        super.init();

        let arrHtmlFileInfo: Array<IFileLoadInfo> = [  
            // 1st level.
            { id: LoginView.kResId, isPreloadImages: true },
            // 2nd level.
            { id: DojoView.kResId, isPreloadImages: false },
            { id: RegisterView.kResId, isPreloadImages: false },
            { id: ForgottenPasswordView.kResId, isPreloadImages: false } ];
        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: arrHtmlFileInfo } );

        const kRememberMeStr: string = window.localStorage.getItem( "tpp_remember_me" );
        if ( kRememberMeStr )
        {
            this._jqRememberMeCheckbox.prop( "checked", true );

            const kRememberMe: Object = JSON.parse( kRememberMeStr );
            this._jqRoot.find( "#loginUsername" ).val( window.atob( kRememberMe[ "username" ] ) );
            this._jqRoot.find( "#loginPassword" ).val( window.atob( kRememberMe[ "password" ] ) );
        }

        // Listen to events.
        ServiceLocator.socketIOManager.gateway.onConnectionAccepted.add( this.onSyncConnection_Accepted, this );
        ServiceLocator.socketIOManager.gateway.onConnectionRejected.add( this.onSyncConnection_Rejected, this );
    }

    public end(): void
    {
        // Owned signals.
        this._onLoginSuccess.removeAll();

        // Cleanup events.
        ServiceLocator.socketIOManager.gateway.onConnectionAccepted.remove( this.onSyncConnection_Accepted, this );
        ServiceLocator.socketIOManager.gateway.onConnectionRejected.remove( this.onSyncConnection_Rejected, this );

        super.end();
    }

    // #endregion //


    // #region Input Callbacks //

    private onConnectBtn_Click(): void
    {
        const kUsername: string = ( this._jqRoot.find( "#loginUsername" ).val() as string ).trim();
        const kPassword: string = ( this._jqRoot.find( "#loginPassword" ).val() as string ).trim();
        if ( kUsername.length > 0 && kPassword.length > 0 )
        {
            this._jqLoadingAnimation.show();

            ServiceLocator.dbConnector.post( { 
                serviceName: "UserDAO",
                methodName: "login",
                parameters: [ kUsername, kPassword ] },
                this.onDbLogin_Result.bind( this ) );
        }
    }

    private onRegisterBtn_Click(): void
    {
        let registerView: RegisterView = new RegisterView();
        registerView.init();
        ServiceLocator.viewManager.fadeIn( registerView, view_layer_id.POPUP );
    }

    private onForgottenPasswordLink_Click(): void
    {
        let forgottenPasswordView = new ForgottenPasswordView();
        forgottenPasswordView.init();
        ServiceLocator.viewManager.fadeIn( forgottenPasswordView, view_layer_id.POPUP );
    }

    private onRememberMeCheckbox_Click(): void
    {
        if ( !this._jqRememberMeCheckbox.prop( "checked" ) )
        {
            window.localStorage.removeItem( "tpp_remember_me" );
        }
    }

    private onEnterKey_Pressed( jqEvent: JQuery.Event ): void
    {
        if ( jqEvent.key == "Enter" )
        {
            jqEvent.preventDefault();

            this.onConnectBtn_Click();
        }
    }

    // #endregion //


    // #region Sync Callbacks //

    private onSyncConnection_Accepted(): void
    {
        Session.playerId = ( this._jqRoot.find( "#loginUsername" ).val() as string ).trim();

        ServiceLocator.socketIOManager.onConnectError.remove( this.onSocket_ConnectError, this );
        
        this._onLoginSuccess.dispatch();
    }

    private onSyncConnection_Rejected( reasonCode: number ): void
    {
        Utils.web.showInfoMessage( jQuery.i18n( "ALREADY_LOGGED_IN" ), view_layer_id.POPUP );

        this._jqLoadingAnimation.hide();
    }

    // #endregion //


    // #region DB Callbacks /

    private onDbLogin_Result( result: number, token: string ): void
    {
        if ( result == 0 )
        {
            Session.token = token;

            this._connectErrorCount = 0;
            ServiceLocator.socketIOManager.onConnectError.add( this.onSocket_ConnectError, this );
            const kUsername: string = ( this._jqRoot.find( "#loginUsername" ).val() as string ).trim();
            ServiceLocator.socketIOManager.connect( kUsername );

            ServiceLocator.savedData.playerId = kUsername;
            ServiceLocator.savedData.init();
            //
            ServiceLocator.audioManager.mute( !ServiceLocator.savedData.data.isMusic, sound_group_id.MUSIC );
            ServiceLocator.audioManager.mute( !ServiceLocator.savedData.data.isSfx, sound_group_id.SFX );
            ServiceLocator.audioManager.changeVolume( ServiceLocator.savedData.data.musicVolume, sound_group_id.MUSIC );
            ServiceLocator.audioManager.changeVolume( ServiceLocator.savedData.data.sfxVolume, sound_group_id.SFX );
            ServiceLocator.audioManager.changeVolume( ServiceLocator.savedData.data.sfxVolume, sound_group_id.AMBIENT );

            if ( this._jqRememberMeCheckbox.prop( "checked" ) )
            {
                const kPassword: string = ( this._jqRoot.find( "#loginPassword" ).val() as string ).trim();
                const kRememberMeStr: string = JSON.stringify( { username: window.btoa( kUsername ), password: window.btoa( kPassword ) } );
                window.localStorage.setItem( "tpp_remember_me", kRememberMeStr );
            }
        }
        else if ( result == 1 )
        {
            this._jqLoadingAnimation.hide();
            Utils.web.showInfoMessage( jQuery.i18n( "INVALID_LOGIN_DATA" ), view_layer_id.POPUP );
        }
        else if ( result == 2 )
        {
            this._jqLoadingAnimation.hide();
            let infoMessageView: InfoMessageView = new InfoMessageView();
            infoMessageView.btnLayoutType = btn_layout_type.DOUBLE;
            infoMessageView.message = jQuery.i18n( "INACTIVE_ACCOUNT" )
            let sb: SignalBinding = infoMessageView.onClosed.addOnce( this.onInactiveAccountPopup_Closed, this );
            sb.params = [ ( this._jqRoot.find( "#loginUsername" ).val() as string ).trim() ];
            infoMessageView.init();
            ServiceLocator.viewManager.fadeIn( infoMessageView, view_layer_id.POPUP ); 
        }
        else if ( result == 10 )
        {
            this._jqLoadingAnimation.hide();
            Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
        }
    }

    private onSocket_ConnectError(): void
    {
        this._connectErrorCount += 1;

        if ( this._connectErrorCount >= 5 )
        {
            this._connectErrorCount = 0;
            ServiceLocator.socketIOManager.disconnect();

            Utils.web.showInfoMessage( jQuery.i18n( "SERVER_UNAVAILABLE" ), view_layer_id.POPUP );

            this._jqLoadingAnimation.hide();
        }
    }

    private onInactiveAccountPopup_Closed( username: string, result: boolean ): void
    {
        if ( result )
        {
            ServiceLocator.loadingScreen.show();

            ServiceLocator.dbConnector.post( { 
                serviceName: "UserDAO",
                methodName: "resendConfirmationEmail",
                parameters: [ username ] },
                this.onDbSendConfirmationEmail_Result.bind( this ) );
        }
    }

    private onDbSendConfirmationEmail_Result( result: number ): void
    {
        if ( result == 0 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "ACTIVATION_EMAIL_SENT" ), view_layer_id.POPUP );
        }
        else if ( result == 10 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
        }
        ServiceLocator.loadingScreen.hide();
    }

    // #endregion //
}