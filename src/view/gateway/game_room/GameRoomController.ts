import { IScenario } from "../../../game/ScenarioDB";
import Signal from "../../../lib/signals/Signal";
import CampaignLogVO from "../../../vo/CampaignLogVO";
import TreasureListVO from "../../../vo/TreasureListVO";
import GameRoomView from "../GameRoomView";


export default class GameRoomController
{
    // #region Attributes //

    // protected:

    protected _view: GameRoomView = null;

    protected _jqRoot: JQuery<HTMLElement> = null;
    protected _selTreasureListVO: TreasureListVO = null;
    protected _selCampaignLogVO: CampaignLogVO = null;

    // Signals.
    protected _onServerDataLoaded: Signal = new Signal();
    protected _onRoomLeft: Signal = new Signal();
    protected _onGameCreated: Signal = new Signal();
    protected _onGameLoaded: Signal = new Signal();
    
    // #endregion //


    // #region Properties //

    public set view( value: GameRoomView) { this._view = value; }

    // Signals.
    public get onServerDataLoaded(): Signal { return this._onServerDataLoaded; }
    public get onRoomLeft(): Signal { return this._onRoomLeft; }
    public get onGameCreated(): Signal { return this._onGameCreated; }
    public get onGameLoaded(): Signal { return this._onGameLoaded; }

    // #endregion //


    // #region Methods //

    // public:

    public init(): void
    {
        console.assert( this._view != null, "GameRoomCreatorController.ts :: init() :: this._view cannot be null." );
    
        this._jqRoot = this._view.root;

        this._jqRoot.find( "#gameRoomCampaignMode" ).hide();
        this._jqRoot.find( "#gameRoomCampaignLog" ).hide();
        this._jqRoot.find( "#gameRoomTreasureList" ).hide();
    }

    public end(): void
    {
        this._onServerDataLoaded.removeAll();
        this._onRoomLeft.removeAll();
        this._onGameCreated.removeAll();
        this._onGameLoaded.removeAll();
    }

    // protected:

    protected checkScenarioCustomPanel( scenario: IScenario, panelId: string ): boolean
    {
        return scenario.play_area 
            && scenario.play_area.custom_panels 
            && scenario.play_area.custom_panels.indexOf( panelId ) != -1;
    }

    // #endregion //
}