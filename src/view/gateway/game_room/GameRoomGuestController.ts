import { IDeck } from "./../DeckParser";
import ServiceLocator from "../../../ServiceLocator";
import Session from "../../../Session";
import Utils from "../../../Utils";
import { view_layer_id } from "../../../service/ViewManager";

import DeckVO from "../../../vo/DeckVO";
import { ISaveGame } from "../../game/SaveGameView";
import { IScenario } from "../../../game/ScenarioDB";
import GameRoomController from "./GameRoomController";
import { user_status_type } from "../../../service/socket_io/GatewaySocketIOController";
import PlayerProgressVO from "../../../vo/PlayerProgressVO";
import ChatView from "../../common/ChatView";
import CampaignLogVO, { ISerializedCampaignLog } from "../../../vo/CampaignLogVO";
import TreasureListVO, { ISerializedTreasureList } from "../../../vo/TreasureListVO";
import DeckManagementView from "../deck/DeckManagementView";


export default class GameRoomGuestController extends GameRoomController
{
    private _arrPlayerProgressVO: Array<PlayerProgressVO> = null;


    // #region Methods //

    // public:

    public constructor()
    {
        super();
    }

    public init(): void
    {
        super.init();

        // Attach UI event handlers.
        this._jqRoot.find( "#gameRoomLeaveBtn" ).on( "click", this.onLeaveBtn_Click.bind( this ) );
        this._jqRoot.find( "#gameRoomSelectDeckBtn" ).on( "click", this.onSelectDeckBtn_Click.bind( this ) );

        this._jqRoot.find( "#gameRoomPlayerList" ).prepend( this._jqRoot.find( "#gameRoomOther" ) );
        this._jqRoot.find( "#gameRoomStartBtn" ).css( "visibility", "hidden" );
        this._jqRoot.find( "#gameRoomLoadGameBtn" ).hide();
        this._jqRoot.find( "#gameRoomScenarioSelection>.item" ).prop( "disabled", true );
        this._jqRoot.find( "#gameRoomScenarioSelection>.item>.detail>.title" ).text( jQuery.i18n( "SCENARIO_NOT_SELECTED" ) );
        this._jqRoot.find( "#gameRoomMyUserId" ).text( Session.playerId );
        this._jqRoot.find( "#gameRoomOtherUserId" ).text( Session.allyId );
        this._jqRoot.find( "#gameRoomCampaignModeCheckbox" ).prop( "disabled", true );
        this._jqRoot.find( "#gameRoomSelectTreasureListBtn" ).prop( "disabled", true );
        this._jqRoot.find( "#gameRoomSelectCampaignLogBtn" ).prop( "disabled", true );

        ServiceLocator.socketIOManager.gateway.onGameRoomRetrieved.addOnce( this.onSyncGameRoom_Retrieved, this );
        ServiceLocator.socketIOManager.gateway.retrieveGameRoom();

        // Listen to events.
        ServiceLocator.socketIOManager.gateway.onUserReadyReceived.add( this.onSyncUserReady_Received, this );
        ServiceLocator.socketIOManager.gateway.onScenarioReceived.add( this.onSyncScenario_Received, this );
        ServiceLocator.socketIOManager.gateway.onIsCampaignModeReceived.add( this.onSyncIsCampaignMode_Received, this );
        ServiceLocator.socketIOManager.gateway.onCampaignLogReceived.add( this.onSyncCampaignLog_Received, this );
        ServiceLocator.socketIOManager.gateway.onTreasureListReceived.add( this.onSyncTreasureList_Received, this );
        ServiceLocator.socketIOManager.gateway.onUserLeftRoom.add( this.onSyncUser_LeftRoom, this );
        ServiceLocator.socketIOManager.gateway.onGameCreated.add( this.onSyncGame_Created, this );
        ServiceLocator.socketIOManager.gateway.onGameLoaded.add( this.onSyncGame_Loaded, this );
    }

    public end(): void
    {
        // Cleanup events.
        ServiceLocator.socketIOManager.gateway.onUserReadyReceived.remove( this.onSyncUserReady_Received, this );
        ServiceLocator.socketIOManager.gateway.onScenarioReceived.remove( this.onSyncScenario_Received, this );
        ServiceLocator.socketIOManager.gateway.onIsCampaignModeReceived.remove( this.onSyncIsCampaignMode_Received, this );
        ServiceLocator.socketIOManager.gateway.onCampaignLogReceived.remove( this.onSyncCampaignLog_Received, this );
        ServiceLocator.socketIOManager.gateway.onTreasureListReceived.remove( this.onSyncTreasureList_Received, this );
        ServiceLocator.socketIOManager.gateway.onUserLeftRoom.remove( this.onSyncUser_LeftRoom, this );
        ServiceLocator.socketIOManager.gateway.onGameCreated.remove( this.onSyncGame_Created, this );
        ServiceLocator.socketIOManager.gateway.onGameLoaded.remove( this.onSyncGame_Loaded, this );
    
        super.end();
    }

    // #endregion //


    // #region Input Callbacks //

    private onLeaveBtn_Click(): void
    {
        Session.reset();
        ServiceLocator.socketIOManager.gateway.leaveGameRoom( false );

        this._onRoomLeft.dispatch();
    }

    private onSelectDeckBtn_Click(): void
    {
        let deckManagementView: DeckManagementView = new DeckManagementView();
        deckManagementView.isSelection = true;
        deckManagementView.onClosed.addOnce( this.onDeckManagementView_Closed, this );
        deckManagementView.init();
        ServiceLocator.viewManager.fadeIn( deckManagementView, view_layer_id.POPUP );
    }

    // #endregion //


    // #region Sync Callbacks //

    private onSyncGameRoom_Retrieved( allyDeck: IDeck, scenarioId: string, isCampaignMode: boolean, campaignLog: ISerializedCampaignLog, treasureList: ISerializedTreasureList ): void
    {
        if ( allyDeck )
        {
            Session.allyDeck = allyDeck;
            this.onSyncUserReady_Received();
        }

        if ( scenarioId )
        {
            Session.scenarioId = scenarioId;

            const kScenarioInfo: IScenario = ServiceLocator.scenarioDb.findScenario( scenarioId );
            if ( this.checkScenarioCustomPanel( kScenarioInfo, "campaign_log" ) )
            {  
                Session.isCampaignMode = isCampaignMode;

                this._jqRoot.find( "#gameRoomCampaignModeCheckbox" ).prop( "checked", isCampaignMode );
                this._jqRoot.find( "#gameRoomCampaignMode" ).show();

                if ( campaignLog )
                {
                    let campaignLogVO: CampaignLogVO = new CampaignLogVO();
                    campaignLogVO.parse( campaignLog );
                    this._selCampaignLogVO = campaignLogVO;
                    this._jqRoot.find( "#gameRoomSelectCampaignLogBtn" ).text( campaignLogVO.title );

                    this._jqRoot.find( "#gameRoomCampaignLog" ).show();
                }
            }
            else if ( treasureList )
            {
                let treasureListVO: TreasureListVO = new TreasureListVO();
                treasureListVO.parse( treasureList );
                this._selTreasureListVO = treasureListVO;
                this._jqRoot.find( "#gameRoomSelectTreasureListBtn" ).text( treasureListVO.title );

                this._jqRoot.find( "#gameRoomTreasureList" ).show();
            }
        }

        ServiceLocator.dbConnector.post( { 
            serviceName: "PlayerProgressDAO",
            methodName: "getProgress",
            parameters: [ Session.playerId ] },
            this.onPlayerProgress_Retrieved.bind( this ) );
    }

        private onPlayerProgress_Retrieved( arrPlayerProgress: Array<Object> ): void
        {
            this._arrPlayerProgressVO = PlayerProgressVO.parseArray( arrPlayerProgress );

            if ( Session.scenarioId )
            {
                this.updateSelectedScenario( Session.scenarioId, this._arrPlayerProgressVO );
            }

            this._onServerDataLoaded.dispatch();
        }

            private updateSelectedScenario( scenarioId: string, arrPlayerProgressVO: Array<PlayerProgressVO> ): void
            {
                let jqScenarioSelection: JQuery<HTMLElement> = this._jqRoot.find( "#gameRoomScenarioSelection" );
                jqScenarioSelection.empty();

                let bestScore: IBestScore = { single: null, multiplayer: null };
                for ( let playerProgressVO of arrPlayerProgressVO )
                {
                    if ( playerProgressVO.scenarioId == scenarioId )
                    {
                        if ( playerProgressVO.playerTwo == null )
                        {
                            if ( bestScore.single == null || playerProgressVO.score > bestScore.single.score )
                            {
                                bestScore.single = { score: playerProgressVO.score, when: playerProgressVO.whenTimestamp };
                            }
                        }
                        else
                        {
                            if ( bestScore.multiplayer == null || playerProgressVO.score > bestScore.multiplayer.score )
                            {
                                const kAlly: string = playerProgressVO.playerTwo == Session.playerId ? playerProgressVO.playerOne : playerProgressVO.playerTwo;
                                bestScore.multiplayer = { 
                                    score: playerProgressVO.score, 
                                    ally: kAlly,
                                    when: playerProgressVO.whenTimestamp };
                            }
                        }
                    }
                }
                if ( bestScore.single == null && bestScore.multiplayer == null )
                {
                    bestScore = null;
                }

                const kScenario: IScenario = ServiceLocator.scenarioDb.findScenario( scenarioId );
                let jqScenarioItem: JQuery<HTMLElement> = this.createScenarioItem( kScenario, bestScore );
                jqScenarioSelection.append( jqScenarioItem );
            }

                private createScenarioItem( scenario: IScenario, bestScore: IBestScore ): JQuery<HTMLElement>
                {
                    let bestScoreHtml: string = "";
                    let completedHtml: string = "";
                    if ( bestScore )
                    {
                        bestScoreHtml = "<div class=\"best_score\">"
                        if ( bestScore.single )
                        {
                            bestScoreHtml += 
                                "<div class=\"single_player gb-text-shadow\">"
                                    + "<img class=\"player_count\" src=\"./resources/images/gateway/single_player.png\">"
                                    + "<div class=\"value gb-text-body-normal gb-text-ellipsis white\">" + bestScore.single.score.toString() + "</div>"
                                    + "<div class=\"separator white\">|</div>"
                                    + "<div class=\"when gb-text-body-small white\">" + jQuery.i18n( "TIME_AGO" ).replace( "#", Utils.web.createElapsedTimeStr( bestScore.single.when ) ) + "</div>"
                                + "</div>";
                        }
                        if ( bestScore.multiplayer )
                        {
                            bestScoreHtml += 
                                "<div class=\"multiplayer gb-text-shadow\">"
                                    + "<img class=\"player_count\" src=\"./resources/images/gateway/multiplayer.png\">"
                                    + "<div class=\"value gb-text-body-normal gb-text-ellipsis white\">" + bestScore.multiplayer.score.toString() + "</div>"
                                    + "<div class=\"separator white\">|</div>"
                                    + "<div class=\"ally gb-text-body-small white\">" + bestScore.multiplayer.ally + "</div>"
                                    + "<div class=\"separator white\">|</div>"
                                    + "<div class=\"when gb-text-body-small white\">" + jQuery.i18n( "TIME_AGO" ).replace( "#", Utils.web.createElapsedTimeStr( bestScore.multiplayer.when ) ) + "</div>"
                                + "</div>";
                        }
                        bestScoreHtml += "</div>";

                        completedHtml = "<img class=\"completed\" src=\"./resources/images/gateway/scenario_completed.png\">";
                    }

                    return jQuery( 
                        "<div class=\"item" + ( scenario.type == "nightmare" ? " nightmare" : "" ) + "\">"
                            + "<img class=\"product_icon\" src=\"./resources/images/common/scenario/" + scenario.product_icon + ".png\">"
                            + "<div class=\"detail\">"
                                + "<div class=\"title gb-text-header-normal\">" + ( scenario.name ? scenario.name : scenario.encounter_set.main ) + "</div>"
                                + bestScoreHtml
                            + "</div>"
                            + completedHtml
                        + "</div>" );
                }

    private onSyncUserReady_Received(): void
    {
        this._jqRoot.find( "#gameRoomOtherDeck" ).text( jQuery.i18n( "DECK_SELECTED" ) );
        this._jqRoot.find( "#gameRoomOtherStatus" ).addClass( "ready" );
    }

    private onSyncScenario_Received( scenarioId: string ): void
    {
        Session.scenarioId = scenarioId;

        this.updateSelectedScenario( scenarioId, this._arrPlayerProgressVO );

        const kScenarioInfo: IScenario = ServiceLocator.scenarioDb.findScenario( scenarioId );
        if ( this.checkScenarioCustomPanel( kScenarioInfo, "campaign_log" ) )
        {
            Session.isCampaignMode = true;

            this._jqRoot.find( "#gameRoomCampaignModeCheckbox" ).prop( "checked", true );
            this._jqRoot.find( "#gameRoomCampaignMode" ).show();
        }
        else
        {
            Session.isCampaignMode = false;

            this._jqRoot.find( "#gameRoomCampaignMode" ).hide();
        }

        this._jqRoot.find( "#gameRoomSelectCampaignLog" ).hide();
        this._jqRoot.find( "#gameRoomTreasureList" ).hide();

        this._selCampaignLogVO = null;
        this._selTreasureListVO = null;
    }

    private onSyncIsCampaignMode_Received( isCampaignMode: boolean ): void
    {
        Session.isCampaignMode = isCampaignMode;

        this._jqRoot.find( "#gameRoomCampaignModeCheckbox" ).prop( "checked", isCampaignMode );
    }

    private onSyncCampaignLog_Received( campaignLog: ISerializedCampaignLog ): void
    {
        let campaignLogVO: CampaignLogVO = new CampaignLogVO();
        campaignLogVO.parse( campaignLog );
        this._selCampaignLogVO = campaignLogVO;

        this._jqRoot.find( "#gameRoomSelectCampaignLogBtn" ).text( campaignLog.title );
        this._jqRoot.find( "#gameRoomSelectCampaignLog" ).show();
    }

    private onSyncTreasureList_Received( treasureList: ISerializedTreasureList ): void
    {
        let treasureListVO: TreasureListVO = new TreasureListVO();
        treasureListVO.parse( treasureList );
        this._selTreasureListVO = treasureListVO;

        this._jqRoot.find( "#gameRoomSelectTreasureListBtn" ).text( treasureList.title );
    }

    private onSyncUser_LeftRoom( userId: string ): void
    {
        Utils.web.showInfoMessage( jQuery.i18n( "USER_LEFT_ROOM" ).replace( "#", Session.allyId ), view_layer_id.TOPMOST );
        Session.reset();

        this._onRoomLeft.dispatch();
    }

    private onSyncGame_Created( allyDeck: IDeck, prngSeed: number, firstPlayerId: string, gameId: string ): void
    {
        Session.allyDeck = allyDeck;
        Session.gameId = gameId;

        this._onGameCreated.dispatch( prngSeed, firstPlayerId, false, this._selTreasureListVO, this._selCampaignLogVO );
    }

    private onSyncGame_Loaded( prngSeed: number, gameId: string, saveGame: ISaveGame ): void
    {
        Session.gameId = gameId;

        this._onGameLoaded.dispatch( prngSeed, saveGame, false );
    }

    // #endregion //


    // #region Other Callbacks //

    private onDeckManagementView_Closed( deckVO: DeckVO ): void
    {
        if ( deckVO )
        {
            Session.playerDeck = JSON.parse( deckVO.content );
            if ( !Session.playerDeck.sideboard )
            {
                Session.playerDeck.sideboard = new Array<string>();
            }
            
            Utils.web.shuffle( Session.playerDeck.player );
            ServiceLocator.socketIOManager.gateway.setReadyStatus( Session.playerDeck );
            this._jqRoot.find( "#gameRoomMyStatus" ).addClass( "ready" );
        }
    }

    // #endregion //
}

interface IBestScore
{
    single: ISingleBestScore;
    multiplayer: IMultiplayerBestScore;
}

interface ISingleBestScore
{
    score: number;
    when: number;
}

interface IMultiplayerBestScore
{
    score: number;
    ally: string;
    when: number;
}