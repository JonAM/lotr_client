import WindowedView from "../../WindowedView";

import Config from "../../../Config";
import Path from "../../../Path";
import ServiceLocator from "../../../ServiceLocator";

import { IScenario } from "../../../game/ScenarioDB";
import TreasureListVO from "../../../vo/TreasureListVO";
import { ICard } from "../../../game/CardDB";


export default class TreasureListDetailView extends WindowedView
{
    // #region Attributes //

    // private:

    private _treasureListVO: TreasureListVO = null;

    // Constants.
    private static readonly _kResId: string = "gateway-game_room-treasure_list_detail";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    public set treasureListVO( value: TreasureListVO ) { this._treasureListVO = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( TreasureListDetailView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#treasureListDetailBackBtn" ).on( "click", this.onBackBtn_Click.bind( this ) );
    }

    public init(): void
    {
        console.assert( this._treasureListVO != null, "TreasureListDetailView.ts :: init() :: this._treasureListVO cannot be null." );

        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ { id: TreasureListDetailView.kResId, isPreloadImages: true } ] } );

        this._jqRoot.find( "#treasureListDetailTitle" ).text( this._treasureListVO.title );

        this.initHeroes( this._treasureListVO.playerHeroes, this._jqRoot.find( "#treasureListDetailPlayerHeroes" ) );
        if ( this._treasureListVO.allyId != "" )
        {
            this.initHeroes( this._treasureListVO.allyHeroes, this._jqRoot.find( "#treasureListDetailAllyHeroes" ) );
        }
        else
        {
            this._jqRoot.find( "#treasureListDetailAlly" ).hide();
        }
        this.initList();
        this.initScenario();
    }

        private initHeroes( arrHeroId: Array<string>, jqTarget: JQuery<HTMLElement> ): void
        {
            let heroesHtml: string = "";
            for ( let heroId of arrHeroId )
            {
                heroesHtml += "<div class=\"gb-text-body-normal gb-text-ellipsis\">" + ServiceLocator.cardDb.find( heroId ).name + "</div>";
            }

            jqTarget.append( heroesHtml );
        }

        private initList(): void
        {
            let jqList: JQuery<HTMLElement> = this._jqRoot.find( "#treasureList" );

            let arrTreasureCard: Array<ICard> = ServiceLocator.cardDb.findByTypeCodes( [ "treasure" ] );
            let curTreasureSet: string = null;
            let itemIndex: number = 0;
            for ( let treasureCard of arrTreasureCard )
            {
                // Header.
                if ( !curTreasureSet || curTreasureSet != treasureCard.treasure_set )
                {
                    curTreasureSet = treasureCard.treasure_set;

                    jqList.append( "<div class=\"header gb-text-body-normal gb-text-ellipsis\">" + curTreasureSet + "</div>" );
                }

                // List item.
                jqList.append(
                    "<div class=\"row\">"
                        + "<input type=\"checkbox\" disabled" + ( this._treasureListVO.list.indexOf( itemIndex ) != -1 ? " checked" : "" ) + ">"
				        + "<div class=\"gb-text-body-normal gb-text-ellipsis\">" + treasureCard.name + "</div>"
			        + "</div>" );

                ++itemIndex;
            }
        }

        private initScenario(): void
        {
            const kScenarioInfo: IScenario = ServiceLocator.scenarioDb.findScenario( this._treasureListVO.scenarioId );
            this._jqRoot.find( ".scenario>img" ).prop( "src", Path.kResImages + "common/scenario/" + kScenarioInfo.product_icon + ".png" );
            this._jqRoot.find( ".scenario>.detail>.title" ).text( kScenarioInfo.name ? kScenarioInfo.name : kScenarioInfo.encounter_set.main );
            this._jqRoot.find( ".scenario>.detail>.set" ).text( ServiceLocator.scenarioDb.find( this._treasureListVO.scenarioId.substr( 0, 2 ) ).name );

            if ( this._treasureListVO.allyId != "" )
            {
                this._jqRoot.find( ".detail>.multiplayer>.ally" ).text(this._treasureListVO.allyId );
                this._jqRoot.find( ".detail>.single_player" ).hide();
            }
            else
            {
                this._jqRoot.find( ".detail>.multiplayer" ).hide();
            }

            if ( kScenarioInfo.type == "nightmare" )
            {
                this._jqRoot.find( ".scenario" ).addClass( "nightmare" );
            }
        }

    // #endregion //


    // #region Input Callbacks //

    private onBackBtn_Click(): void
    {
        ServiceLocator.viewManager.fadeOut( this );
    }

    // #endregion //
}