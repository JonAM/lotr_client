import WindowedView from "../../WindowedView";

import ServiceLocator from "../../../ServiceLocator";

import CampaignLogVO from "../../../vo/CampaignLogVO";


export default class CampaignLogDetailView extends WindowedView
{
    // #region Attributes //

    // private:

    private _campaignLogVO: CampaignLogVO = null;

    // Constants.
    private static readonly _kResId: string = "gateway-game_room-campaign_log_detail";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    public set campaignLogVO( value: CampaignLogVO ) { this._campaignLogVO = value; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( CampaignLogDetailView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#campaignLogDetailBackBtn" ).on( "click", this.onBackBtn_Click.bind( this ) );
    }

    public init(): void
    {
        console.assert( this._campaignLogVO != null, "CampaignLogDetailView.ts :: init() :: this._campaignLogVO cannot be null." );

        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ { id: CampaignLogDetailView.kResId, isPreloadImages: true } ] } );

        this._jqRoot.find( "#campaignLogDetailTitle" ).text( this._campaignLogVO.title );

        this.initCampaignLog();
    }

        private initCampaignLog(): void
        {
            // TODO:
        }

    // #endregion //


    // #region Input Callbacks //

    private onBackBtn_Click(): void
    {
        ServiceLocator.viewManager.fadeOut( this );
    }

    // #endregion //
}