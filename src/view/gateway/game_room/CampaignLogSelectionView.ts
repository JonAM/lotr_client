import WindowedView from "../../WindowedView";

import ServiceLocator from "../../../ServiceLocator";
import Session from "../../../Session";
import Utils from "../../../Utils";
import { view_layer_id } from "../../../service/ViewManager";

import Signal from "../../../lib/signals/Signal";
import CampaignLogVO from "../../../vo/CampaignLogVO";
import CampaignLogView from "../../common/CampaignLogView";


export default class CampaignLogSelectionView extends WindowedView
{
    // #region Attributes //

    // private:

    private _arrCampaignLogVO: Array<CampaignLogVO> = null;

    private _selCampaignLogVO: CampaignLogVO = null;

    // Signals.
    private _onClosed: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "gateway-game_room-campaign_log_selection";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    public set campaignLogs( value: Array<CampaignLogVO> ) { this._arrCampaignLogVO = value; }

    // Signals.
    public get onClosed(): Signal { return this._onClosed; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( CampaignLogSelectionView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#campaignLogSelectionBackBtn" ).on( "click", this.onBackBtn_Click.bind( this ) );
        this._jqRoot.find( "#campaignLogSelectionAcceptBtn" ).on( "click", this.onAcceptBtn_Click.bind( this ) );
    }

    public init(): void
    {
        console.assert( this._arrCampaignLogVO != null, "CampaignLogSelectionView.ts :: init() :: this._arrCampaignLogVO cannot be null." );

        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ 
                // 1st level.
                { id: CampaignLogSelectionView.kResId, isPreloadImages: true },
                // 2nd level.
                { id: CampaignLogView.kResId, isPreloadImages: false } ] } );

        this._jqRoot.find( "#campaignLogSelectionAcceptBtn" ).prop( "disabled", true );
        let jqCampaignLogSelectionList: JQuery<HTMLElement> = this._jqRoot.find( "#campaignLogSelectionList" );
        for ( let campaignLogVO of this._arrCampaignLogVO )
        {
            jqCampaignLogSelectionList.append( this.createItemHtml( campaignLogVO ) );
        }
        this.registerSfx( jqCampaignLogSelectionList );
    }

    public end(): void
    {
        this._onClosed.removeAll();

        super.end();
    }

    // private:

    private createItemHtml( campaignLogVO: CampaignLogVO ): JQuery<HTMLElement>
    {
        let result: JQuery<HTMLElement> = null;

        result = jQuery( 
            "<div class=\"item gb-custom_pointer sfx_item_click\">" 
                + "<div class=\"title gb-text-body-normal gb-text-ellipsis\">" + campaignLogVO.title + "</div>"
                + "<div class=\"created gb-text-body-normal gb-text-ellipsis\">" + jQuery.i18n( "TIME_AGO" ).replace( "#", Utils.web.createElapsedTimeStr( campaignLogVO.whenTimestamp ) ) + "</div>"
                + "<button class=\"detail sfx_btn_click\"></button>"
                + "<button class=\"remove sfx_btn_click\"></button>"
            + "</div>" );
        result.find( "button.detail" ).on( "click", this.onDetailBtn_Click.bind( this, result ) );
        result.find( "button.remove" ).on( "click", this.onRemoveBtn_Click.bind( this, result ) );
        result.on( "click", this.onItem_Click.bind( this, result ) );
        
        return result;
    }

    // #endregion //


    // #region Input Callbacks //

    private onBackBtn_Click(): void
    {
        this._onClosed.dispatch( null );

        ServiceLocator.viewManager.fadeOut( this );
    }

    private onAcceptBtn_Click(): void
    {
        this._onClosed.dispatch( this._selCampaignLogVO );

        ServiceLocator.viewManager.fadeOut( this );
    }

    private onItem_Click( jqItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        let jqItems: JQuery<HTMLElement> = this._jqRoot.find( "#campaignLogSelectionList>.item" );
        this._selCampaignLogVO = this._arrCampaignLogVO[ jqItems.index( jqItem ) ];

        jqItems.removeClass( "selected" );
        jqItem.addClass( "selected" ); 

        this._jqRoot.find( "#campaignLogSelectionAcceptBtn" ).prop( "disabled", false );
    }

    private onRemoveBtn_Click( jqItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        jqEvent.stopPropagation();

        this._jqLoadingAnimation.show();

        const kItemIndex: number = this._jqRoot.find( "#campaignLogSelectionList>.item" ).index( jqItem );
        const kCampaignLogVO: CampaignLogVO = this._arrCampaignLogVO[ kItemIndex ];

        ServiceLocator.dbConnector.post( { 
            serviceName: "CampaignLogDAO",
            methodName: "remove",
            parameters: [ kCampaignLogVO.title, Session.playerId, Session.token ] },
            this.onDbCampaignLogRemove_Result.bind( this, kItemIndex ) );
    }

    private onDetailBtn_Click( jqItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        jqEvent.stopImmediatePropagation();

        let campaignLogView: CampaignLogView = new CampaignLogView();
        const kItemIndex: number = this._jqRoot.find( "#campaignLogSelectionList>.item" ).index( jqItem );
        campaignLogView.campaignLogVO = this._arrCampaignLogVO[ kItemIndex ];
        campaignLogView.isEditable = false;
        campaignLogView.init();
        ServiceLocator.viewManager.fadeIn( campaignLogView, view_layer_id.POPUP );
    }

    // #endregion //


    // #region Callbacks //

    private onDbCampaignLogRemove_Result( itemIndex: number, result: number ): void
    {
        if ( !this._id ) { return; }

        if ( result == 0 )
        {
            if ( this._selCampaignLogVO == this._arrCampaignLogVO[ itemIndex ] )
            {
                this._selCampaignLogVO = null;
                this._jqRoot.find( "#campaignLogSelectionAcceptBtn" ).prop( "disabled", true );
            }

            this._arrCampaignLogVO.splice( itemIndex, 1 );
            this._jqRoot.find( "#campaignLogSelectionList>.item" ).eq( itemIndex ).remove();
        }
        else if ( result == 1 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "CAMPAIGN_LOG_NOT_FOUND" ), view_layer_id.POPUP );
        }
        else if ( result == 10 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
        }
        else if ( result == 11 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "INVALID_USER_TOKEN" ), view_layer_id.POPUP );

            // TODO: Go back to login screen.
        }
        
        this._jqLoadingAnimation.hide();
    }

    // #endregion //
}