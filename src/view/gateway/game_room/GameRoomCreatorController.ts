import { IDeck } from "./../DeckParser";
import ServiceLocator from "../../../ServiceLocator";
import Session from "../../../Session";
import Utils from "../../../Utils";
import { view_layer_id } from "../../../service/ViewManager";

import DeckVO from "../../../vo/DeckVO";
import LoadGameView from "./../LoadGameView";
import SaveGameVO from "../../../vo/SaveGameVO";
import { ISaveGame } from "../../game/SaveGameView";
import { IScenario } from "../../../game/ScenarioDB";
import View from "../../View";
import GameRoomController from "./GameRoomController";
import ScenarioSelectionView from "../ScenarioSelectionView";
import { user_status_type } from "../../../service/socket_io/GatewaySocketIOController";
import TreasureListSelectionView from "./TreasureListSelectionView";
import TreasureListVO from "../../../vo/TreasureListVO";
import CampaignLogVO from "../../../vo/CampaignLogVO";
import CampaignLogSelectionView from "./CampaignLogSelectionView";
import DeckManagementView from "../deck/DeckManagementView";


export default class GameRoomCreatorController extends GameRoomController
{
    // #region Attributes //

    // private:

    private _arrSaveGameVO: Array<SaveGameVO> = null;
    private _arrTreasureListVO: Array<TreasureListVO> = null;
    private _arrCampaignLogVO: Array<CampaignLogVO> = null;
    
    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super();
    }

    public init(): void
    {
        super.init();

        // Attach UI event handlers.
        this._jqRoot.find( "#gameRoomLoadGameBtn" ).on( "click", this.onLoadGameBtn_Click.bind( this ) );
        this._jqRoot.find( "#gameRoomLeaveBtn" ).on( "click", this.onLeaveBtn_Click.bind( this ) );
        this._jqRoot.find( "#gameRoomStartBtn" ).on( "click", this.onStartBtn_Click.bind( this ) );
        this._jqRoot.find( "#gameRoomScenarioSelection>.item" ).on( "click", this.onSelectScenarioBtn_Click.bind( this ) );
        this._jqRoot.find( "#gameRoomSelectDeckBtn" ).on( "click", this.onSelectDeckBtn_Click.bind( this ) );
        this._jqRoot.find( "#gameRoomCampaignModeCheckbox" ).on( "click", this.onIsCampaingModeCheckbox_Click.bind( this ) );
        this._jqRoot.find( "#gameRoomSelectCampaignLogBtn" ).on( "click", this.onSelectCampaignLogBtn_Click.bind( this ) );
        this._jqRoot.find( "#gameRoomSelectTreasureListBtn" ).on( "click", this.onSelectTreasureListBtn_Click.bind( this ) );
        
        this._jqRoot.find( "#gameRoomLoadGameBtn" ).hide();
        this._jqRoot.find( "#gameRoomMyUserId" ).text( Session.playerId );
        this._jqRoot.find( "#gameRoomOtherDeck, #gameRoomOtherStatus" ).hide();
        this._jqRoot.find( "#gameRoomStartBtn" ).prop( "disabled", true );

        ServiceLocator.dbConnector.post( { 
            serviceName: "GameRoomManager",
            methodName: "retrieveCreatorData",
            parameters: [ Session.playerId ] },
            this.onDbInitialCreatorData_Retrieved.bind( this ) );
        
        // Listen to events.
        ServiceLocator.socketIOManager.gateway.onGameInvitationAccepted.add( this.onSyncGameInvitation_Accepted, this );
        ServiceLocator.socketIOManager.gateway.onUserReadyReceived.add( this.onSyncUserReady_Received, this );
        ServiceLocator.socketIOManager.gateway.onUserLeftRoom.add( this.onSyncUser_LeftRoom, this );
        ServiceLocator.socketIOManager.gateway.onGameCreated.add( this.onSyncGame_Created, this );
        ServiceLocator.socketIOManager.gateway.onGameLoaded.add( this.onSyncGame_Loaded, this );
    }

    public end(): void
    {
        ServiceLocator.socketIOManager.gateway.clearLastGameRoom();
        
        // Cleanup events.
        ServiceLocator.socketIOManager.gateway.onGameInvitationAccepted.remove( this.onSyncGameInvitation_Accepted, this );
        ServiceLocator.socketIOManager.gateway.onUserReadyReceived.remove( this.onSyncUserReady_Received, this );
        ServiceLocator.socketIOManager.gateway.onUserLeftRoom.remove( this.onSyncUser_LeftRoom, this );
        ServiceLocator.socketIOManager.gateway.onGameCreated.remove( this.onSyncGame_Created, this );
        ServiceLocator.socketIOManager.gateway.onGameLoaded.remove( this.onSyncGame_Loaded, this );
    
        super.end();
    }

    // private:

    private checkStartGame(): void
    {
        let isDisabled: boolean = true;

        isDisabled = Session.scenarioId == null || Session.playerDeck == null;
        if ( !isDisabled && Session.allyId )
        {
            isDisabled = Session.allyDeck == null;
        }

        this._jqRoot.find( "#gameRoomStartBtn" ).prop( "disabled", isDisabled );
    }

    private updateLoadGameBtnVisibility(): void
    {
        if ( this._arrSaveGameVO.length > 0 )
        {
            this._jqRoot.find( "#gameRoomLoadGameBtn" ).show();
        }
        else
        {
            this._jqRoot.find( "#gameRoomLoadGameBtn" ).hide();
        }
    }

    // #endregion //


    // #region Input Callbacks //

    private onLoadGameBtn_Click(): void
    {
        let loadGameView: LoadGameView = new LoadGameView();
        loadGameView.alias = "load_game";
        loadGameView.saveGames = this._arrSaveGameVO;
        loadGameView.onClosed.addOnce( this.onLoadGameView_Closed, this );
        loadGameView.init();
        ServiceLocator.viewManager.fadeIn( loadGameView, view_layer_id.POPUP );
    }

    private onLeaveBtn_Click(): void
    {
        Session.reset();
        ServiceLocator.socketIOManager.gateway.leaveGameRoom( true );

        this._onRoomLeft.dispatch();
    }

    private onStartBtn_Click(): void
    {
        if ( Session.allyId )
        {
            ServiceLocator.socketIOManager.gateway.startGame();
        }
        else
        {
            // Offline game.
            ServiceLocator.socketIOManager.gateway.setUserStatus( user_status_type.PLAYING );
            
            this._onGameCreated.dispatch( Math.floor( Math.random() * 10000 ), Session.playerId, true, this._selTreasureListVO, this._selCampaignLogVO );
        }
    }

    private onSelectScenarioBtn_Click(): void
    {
        let playerProgressView: ScenarioSelectionView = new ScenarioSelectionView();
        playerProgressView.onScenarioSelected.addOnce( this.onScenario_Selected, this );
        playerProgressView.init();
        ServiceLocator.viewManager.fadeIn( playerProgressView, view_layer_id.POPUP );
    }

    private onSelectDeckBtn_Click(): void
    {
        let deckManagementView: DeckManagementView = new DeckManagementView();
        deckManagementView.isSelection = true;
        deckManagementView.onClosed.addOnce( this.onDeckManagementView_Closed, this );
        deckManagementView.init();
        ServiceLocator.viewManager.fadeIn( deckManagementView, view_layer_id.POPUP );
    }

    private onIsCampaingModeCheckbox_Click(): void
    {
        Session.isCampaignMode = !Session.isCampaignMode;

        if ( Session.isCampaignMode && this._arrCampaignLogVO.length > 0 )
        {
            this._jqRoot.find( "#gameRoomCampaignLog" ).show();
        }
        else
        {
            this._jqRoot.find( "#gameRoomCampaignLog" ).hide();
        }

        ServiceLocator.socketIOManager.gateway.setIsCampaignMode( Session.isCampaignMode );
    }

    private onSelectTreasureListBtn_Click(): void
    {
        let treasureListSelectionView: TreasureListSelectionView = new TreasureListSelectionView();
        treasureListSelectionView.alias = "select_treasure_list";
        treasureListSelectionView.treasureLists = this._arrTreasureListVO;
        treasureListSelectionView.onClosed.addOnce( this.onTreasureListSelectionView_Closed, this );
        treasureListSelectionView.init();
        ServiceLocator.viewManager.fadeIn( treasureListSelectionView, view_layer_id.POPUP );
    }

    private onSelectCampaignLogBtn_Click(): void
    {
        let campaignLogSelectionView: CampaignLogSelectionView = new CampaignLogSelectionView();
        campaignLogSelectionView.alias = "select_campaign_log";
        campaignLogSelectionView.campaignLogs = this._arrCampaignLogVO;
        campaignLogSelectionView.onClosed.addOnce( this.onCampaignLogSelectionView_Closed, this );
        campaignLogSelectionView.init();
        ServiceLocator.viewManager.fadeIn( campaignLogSelectionView, view_layer_id.POPUP );
    }

    // #endregion //


    // #region Sync Callbacks //

    private onSyncGameInvitation_Accepted(): void
    {
        this._jqRoot.find( "#gameRoomOtherDeck, #gameRoomOtherStatus" ).show();
        this._jqRoot.find( "#gameRoomOtherUserId" ).text( Session.allyId );

        this._jqRoot.find( "#gameRoomStartBtn" ).prop( "disabled", true );

        this.closeGameRoomPopups();

        this._view.loadingAnimation.show();

        ServiceLocator.dbConnector.post( { 
            serviceName: "GameRoomManager",
            methodName: "retrieveCreatorData",
            parameters: [ Session.playerId ] },
            this.onDbUpdatedCreatorData_Retrieved.bind( this ) );

        // Sfx
        ServiceLocator.audioManager.playSfx( "ally_arrived" );
    }

        private closeGameRoomPopups(): void
        {
            let arrLoadGameView: Array<View> = ServiceLocator.viewManager.findViewByAlias( "load_game" );
            if ( arrLoadGameView.length > 0 )
            {
                ServiceLocator.viewManager.fadeOut( arrLoadGameView[ 0 ] );
            }

            let arrSelectTreasureListView: Array<View> = ServiceLocator.viewManager.findViewByAlias( "select_treasure_list" );
            if ( arrSelectTreasureListView.length > 0 )
            {
                ServiceLocator.viewManager.fadeOut( arrSelectTreasureListView[ 0 ] );
            }

            let arrSelectCampaignLogView: Array<View> = ServiceLocator.viewManager.findViewByAlias( "select_campaign_log" );
            if ( arrSelectCampaignLogView.length > 0 )
            {
                ServiceLocator.viewManager.fadeOut( arrSelectCampaignLogView[ 0 ] );
            }
        }

    private onSyncUserReady_Received( allyDeck: IDeck ): void
    {
        this._jqRoot.find( "#gameRoomOtherDeck" ).text( jQuery.i18n( "DECK_SELECTED" ) );
        this._jqRoot.find( "#gameRoomOtherStatus" ).addClass( "ready" );

        Session.allyDeck = allyDeck;

        this.checkStartGame();
    }

    private onSyncUser_LeftRoom( userId: string ): void
    {
        Session.allyId = null;
        Session.allyDeck = null;

        this._jqRoot.find( "#gameRoomOtherUserId" ).text( "" );
        this._jqRoot.find( "#gameRoomOtherDeck, #gameRoomOtherStatus" ).hide();
        this._jqRoot.find( "#gameRoomOtherDeck" ).text( jQuery.i18n( "NO_DECK_SELECTED" ) );
        this._jqRoot.find( "#gameRoomOtherStatus" ).removeClass( "ready" );

        this.closeGameRoomPopups();

        this._view.loadingAnimation.show();

        ServiceLocator.dbConnector.post( { 
            serviceName: "GameRoomManager",
            methodName: "retrieveCreatorData",
            parameters: [ Session.playerId ] },
            this.onDbUpdatedCreatorData_Retrieved.bind( this ) );

        // Sfx.
        ServiceLocator.audioManager.playSfx( "ally_left" );
    }

    private onSyncGame_Created( allyDeck: IDeck, prngSeed: number, firstPlayerId: string, gameId: string ): void
    {
        Session.allyDeck = allyDeck;
        Session.gameId = gameId;

        this._onGameCreated.dispatch( prngSeed, firstPlayerId, true, this._selTreasureListVO, this._selCampaignLogVO );
    }

    private onSyncGame_Loaded( prngSeed: number, gameId: string, saveGame: ISaveGame ): void
    {
        Session.gameId = gameId;

        this._onGameLoaded.dispatch( prngSeed, saveGame, true );
    }

    // #endregion //


    // #region Other Callbacks //

    private onDbInitialCreatorData_Retrieved( arrSaveGameObj: Array<Object>, arrTreasureListObj: Array<Object>, arrCampaignLogObj: Array<Object> ): void
    {
        this._arrSaveGameVO = this.filterSaveGames( SaveGameVO.parseArray( arrSaveGameObj ) );
        this._arrTreasureListVO = this.filterTreasureLists( TreasureListVO.parseArray( arrTreasureListObj ) );
        this._arrCampaignLogVO = this.filterCampaignLogs( CampaignLogVO.parseArray( arrCampaignLogObj ) );

        if ( Session.allyId )
        {
            this.onSyncGameInvitation_Accepted();
        }
        else
        {
            this.updateLoadGameBtnVisibility();
        }
        
        this._onServerDataLoaded.dispatch();
    }
        
        private filterSaveGames( arrSaveGameVO: Array<SaveGameVO> ): Array<SaveGameVO>
        {
            let result: Array<SaveGameVO> = new Array<SaveGameVO>();

            for ( let saveGameVO of arrSaveGameVO )
            {
                if ( Session.allyId && saveGameVO.ally == Session.allyId 
                    || !Session.allyId && !saveGameVO.ally )
                {
                    result.push( saveGameVO );
                }
            }

            return result;
        }

        private filterTreasureLists( arrTreasureListVO: Array<TreasureListVO> ): Array<TreasureListVO>
        {
            let result: Array<TreasureListVO> = new Array<TreasureListVO>();

            for ( let treasureListVO of arrTreasureListVO )
            {
                if ( Session.allyId && ( treasureListVO.allyId == Session.allyId || treasureListVO.playerId == Session.allyId )
                    || !Session.allyId && treasureListVO.allyId == "" )
                {
                    result.push( treasureListVO );
                }
            }

            return result;
        }

        private filterCampaignLogs( arrCampaignLogVO: Array<CampaignLogVO> ): Array<CampaignLogVO>
        {
            let result: Array<CampaignLogVO> = new Array<CampaignLogVO>();

            for ( let campaignLogVO of arrCampaignLogVO )
            {
                if ( Session.allyId && ( campaignLogVO.allyId == Session.allyId || campaignLogVO.playerId == Session.allyId )
                    || !Session.allyId && campaignLogVO.allyId == "" )
                {
                    result.push( campaignLogVO );
                }
            }

            return result;
        }

    private onDbUpdatedCreatorData_Retrieved( arrSaveGameObj: Array<Object>, arrTreasureListObj: Array<Object>, arrCampaignLogObj: Array<Object> ): void
    {
        this._arrSaveGameVO = this.filterSaveGames( SaveGameVO.parseArray( arrSaveGameObj ) );
        this._arrTreasureListVO = this.filterTreasureLists( TreasureListVO.parseArray( arrTreasureListObj ) );
        this._arrCampaignLogVO = this.filterCampaignLogs( CampaignLogVO.parseArray( arrCampaignLogObj ) );

        this.updateLoadGameBtnVisibility();

        if ( Session.scenarioId )
        {
            if ( Session.isCampaignMode && this._arrCampaignLogVO.length > 0 )
            {
                this._jqRoot.find( "#gameRoomSelectCampaignLogBtn" ).text( jQuery.i18n( "SELECT" ) );
                this._jqRoot.find( "#gameRoomCampaignLog" ).show();
            }
            else
            {
                this._jqRoot.find( "#gameRoomCampaignLog" ).hide();
            }

            const kScenarioInfo: IScenario = ServiceLocator.scenarioDb.findScenario( Session.scenarioId );
            if ( this.checkScenarioCustomPanel( kScenarioInfo, "treasure_list" ) && this._arrTreasureListVO.length > 0 )
            {
                this._jqRoot.find( "#gameRoomSelectTreasureListBtn" ).text( jQuery.i18n( "SELECT" ) );
                this._jqRoot.find( "#gameRoomTreasureList" ).show();
            }
            else
            {
                this._jqRoot.find( "#gameRoomTreasureList" ).hide();
            }
        }

        this._selCampaignLogVO = null;
        this._selTreasureListVO = null;

        this.checkStartGame();

        this._view.loadingAnimation.hide();
    }

    private onDeckManagementView_Closed( deckVO: DeckVO ): void
    {
        if ( deckVO )
        {
            Session.playerDeck = JSON.parse( deckVO.content );
            if ( !Session.playerDeck.sideboard )
            {
                Session.playerDeck.sideboard = new Array<string>();
            }

            Utils.web.shuffle( Session.playerDeck.player );
            ServiceLocator.socketIOManager.gateway.setReadyStatus( Session.playerDeck );
            this._jqRoot.find( "#gameRoomMyStatus" ).addClass( "ready" );

            this.checkStartGame();
        }
    }

    private onScenario_Selected( scenarioId: string, jqScenarioItem: JQuery<HTMLElement> ): void
    {
        Session.scenarioId = scenarioId;

        this._jqRoot.find( "#gameRoomScenarioSelection" ).empty().append( jqScenarioItem );
        jqScenarioItem.on( "click", this.onSelectScenarioBtn_Click.bind( this ) );

        ServiceLocator.socketIOManager.gateway.setScenario( scenarioId );

        const kScenarioInfo: IScenario = ServiceLocator.scenarioDb.findScenario( scenarioId );
        if ( this.checkScenarioCustomPanel( kScenarioInfo, "campaign_log" ) )
        {
            Session.isCampaignMode = true;
            this._jqRoot.find( "#gameRoomCampaignModeCheckbox" ).prop( "checked", true );
            this._jqRoot.find( "#gameRoomCampaignMode" ).show();
        }
        else
        {
            Session.isCampaignMode = false;
            this._jqRoot.find( "#gameRoomCampaignMode" ).hide();
        }

        if ( Session.isCampaignMode && this._arrCampaignLogVO.length > 0 )
        {
            this._jqRoot.find( "#gameRoomSelectCampaignLogBtn" ).text( jQuery.i18n( "SELECT" ) );
            this._jqRoot.find( "#gameRoomCampaignLog" ).show();
        }
        else
        {
            this._jqRoot.find( "#gameRoomCampaignLog" ).hide();
        }

        if ( this.checkScenarioCustomPanel( kScenarioInfo, "treasure_list" ) && this._arrTreasureListVO.length > 0 )
        {
            this._jqRoot.find( "#gameRoomSelectTreasureListBtn" ).text( jQuery.i18n( "SELECT" ) );
            this._jqRoot.find( "#gameRoomTreasureList" ).show();
        }
        else
        {
            this._jqRoot.find( "#gameRoomTreasureList" ).hide();
        }

        this._selCampaignLogVO = null;
        this._selTreasureListVO = null;

        this.checkStartGame();
    }

    private onLoadGameView_Closed(): void
    {
        if ( this._arrSaveGameVO.length == 0 )
        {
            this._jqRoot.find( "#gameRoomLoadGameBtn" ).hide();

            this.updateLoadGameBtnVisibility();
        }
    }

    private onTreasureListSelectionView_Closed( treasureListVO: TreasureListVO ): void
    {
        if ( treasureListVO )
        {
            this._selTreasureListVO = treasureListVO;
        
            this._jqRoot.find( "#gameRoomSelectTreasureListBtn" ).text( treasureListVO.title );

            ServiceLocator.socketIOManager.gateway.setTreasureList( treasureListVO.serialize() );
        }

        if ( this._arrTreasureListVO.length == 0 )
        {
            this._jqRoot.find( "#gameRoomSelectTreasureListBtn" ).prop( "disabled", true );
        }
    }

    private onCampaignLogSelectionView_Closed( campaignLogVO: CampaignLogVO ): void
    {
        if ( campaignLogVO )
        {       
            this._selCampaignLogVO = campaignLogVO;

            this._jqRoot.find( "#gameRoomSelectCampaignLogBtn" ).text( campaignLogVO.title );

            ServiceLocator.socketIOManager.gateway.setCampaignLog( campaignLogVO.serialize() );
        }

        if ( this._arrCampaignLogVO.length == 0 )
        {
            this._jqRoot.find( "#gameRoomSelectCampaignLogBtn" ).prop( "disabled", true );
        }
    }

    // #endregion //
}