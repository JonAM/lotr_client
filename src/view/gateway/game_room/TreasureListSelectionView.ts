import WindowedView from "../../WindowedView";

import ServiceLocator from "../../../ServiceLocator";
import Session from "../../../Session";
import Utils from "../../../Utils";
import { view_layer_id } from "../../../service/ViewManager";

import TreasureListVO from "../../../vo/TreasureListVO";
import Signal from "../../../lib/signals/Signal";
import TreasureListDetailView from "./TreasureListDetailView";


export default class TreasureListSelectionView extends WindowedView
{
    // #region Attributes //

    // private:

    private _arrTreasureListVO: Array<TreasureListVO> = null;

    private _selTreasureListVO: TreasureListVO = null;

    // Signals.
    private _onClosed: Signal = new Signal();

    // Constants.
    private static readonly _kResId: string = "gateway-game_room-treasure_list_selection";
    
    // #endregion //


    // #region Properties //

    public static get kResId(): string { return this._kResId; }

    public set treasureLists( value: Array<TreasureListVO> ) { this._arrTreasureListVO = value; }

    // Signals.
    public get onClosed(): Signal { return this._onClosed; }

    // #endregion //


    // #region Methods //

    // public:

    public constructor()
    {
        super( TreasureListSelectionView._kResId );

        // Attach UI event handlers.
        this._jqRoot.find( "#treasureListSelectionBackBtn" ).on( "click", this.onBackBtn_Click.bind( this ) );
        this._jqRoot.find( "#treasureListSelectionAcceptBtn" ).on( "click", this.onAcceptBtn_Click.bind( this ) );
    }

    public init(): void
    {
        console.assert( this._arrTreasureListVO != null, "TreasureListSelectionView.ts :: init() :: this._arrTreasureListVO cannot be null." );

        super.init();

        this.preload( { 
            jqContent: this._jqRoot.children( ".content" ),
            htmlFileInfoList: [ 
                // 1st level.
                { id: TreasureListSelectionView.kResId, isPreloadImages: true },
                // 2nd level.
                { id: TreasureListDetailView.kResId, isPreloadImages: false } ] } );

        this._jqRoot.find( "#treasureListSelectionAcceptBtn" ).prop( "disabled", true );
        let jqTreasureListSelectionList: JQuery<HTMLElement> = this._jqRoot.find( "#treasureListSelectionList" );
        for ( let treasureListVO of this._arrTreasureListVO )
        {
            jqTreasureListSelectionList.append( this.createItemHtml( treasureListVO ) );
        }
        this.registerSfx( jqTreasureListSelectionList );
    }

    public end(): void
    {
        this._onClosed.removeAll();

        super.end();
    }

    // private:

    private createItemHtml( treasureListVO: TreasureListVO ): JQuery<HTMLElement>
    {
        let result: JQuery<HTMLElement> = null;

        result = jQuery( 
            "<div class=\"item gb-custom_pointer sfx_item_click\">" 
                + "<div class=\"title gb-text-body-normal gb-text-ellipsis\">" + treasureListVO.title + "</div>"
                + "<div class=\"created gb-text-body-normal gb-text-ellipsis\">" + jQuery.i18n( "TIME_AGO" ).replace( "#", Utils.web.createElapsedTimeStr( treasureListVO.whenTimestamp ) ) + "</div>"
                + "<button class=\"detail sfx_btn_click\"></button>"
                + "<button class=\"remove sfx_btn_click\"></button>"
            + "</div>" );
        result.find( "button.detail" ).on( "click", this.onDetailBtn_Click.bind( this, result ) );
        result.find( "button.remove" ).on( "click", this.onRemoveBtn_Click.bind( this, result ) );
        result.on( "click", this.onItem_Click.bind( this, result ) );
        
        return result;
    }

    // #endregion //


    // #region Input Callbacks //

    private onBackBtn_Click(): void
    {
        this._onClosed.dispatch( null );

        ServiceLocator.viewManager.fadeOut( this );
    }

    private onAcceptBtn_Click(): void
    {
        this._onClosed.dispatch( this._selTreasureListVO );

        ServiceLocator.viewManager.fadeOut( this );
    }

    private onItem_Click( jqItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        let jqItems: JQuery<HTMLElement> = this._jqRoot.find( "#treasureListSelectionList>.item" );
        this._selTreasureListVO = this._arrTreasureListVO[ jqItems.index( jqItem ) ];

        jqItems.removeClass( "selected" );
        jqItem.addClass( "selected" ); 

        this._jqRoot.find( "#treasureListSelectionAcceptBtn" ).prop( "disabled", false );
    }

    private onRemoveBtn_Click( jqItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        jqEvent.stopPropagation();

        this._jqLoadingAnimation.show();

        const kItemIndex: number = this._jqRoot.find( "#treasureListSelectionList>.item" ).index( jqItem );
        const kTreasureListVO: TreasureListVO = this._arrTreasureListVO[ kItemIndex ];

        ServiceLocator.dbConnector.post( { 
            serviceName: "TreasureListDAO",
            methodName: "remove",
            parameters: [ kTreasureListVO.title, Session.playerId, Session.token ] },
            this.onDbTreasureListRemove_Result.bind( this, kItemIndex ) );
    }

    private onDetailBtn_Click( jqItem: JQuery<HTMLElement>, jqEvent: JQuery.Event ): void
    {
        jqEvent.stopImmediatePropagation();

        let treasureListDetailView: TreasureListDetailView = new TreasureListDetailView();
        const kItemIndex: number = this._jqRoot.find( "#treasureListSelectionList>.item" ).index( jqItem );
        const kTreasureListVO: TreasureListVO = this._arrTreasureListVO[ kItemIndex ];
        treasureListDetailView.treasureListVO = kTreasureListVO;
        treasureListDetailView.init();
        ServiceLocator.viewManager.fadeIn( treasureListDetailView, view_layer_id.POPUP );
    }

    // #endregion //


    // #region Callbacks //

    private onDbTreasureListRemove_Result( itemIndex: number, result: number ): void
    {
        if ( !this._id ) { return; }

        if ( result == 0 )
        {
            if ( this._selTreasureListVO == this._arrTreasureListVO[ itemIndex ] )
            {
                this._selTreasureListVO = null;
                this._jqRoot.find( "#treasureListSelectionAcceptBtn" ).prop( "disabled", true );
            }

            this._arrTreasureListVO.splice( itemIndex, 1 );
            this._jqRoot.find( "#treasureListSelectionList>.item" ).eq( itemIndex ).remove();
        }
        else if ( result == 1 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "TREASURE_LIST_NOT_FOUND" ), view_layer_id.POPUP );
        }
        else if ( result == 10 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
        }
        else if ( result == 11 )
        {
            Utils.web.showInfoMessage( jQuery.i18n( "INVALID_USER_TOKEN" ), view_layer_id.POPUP );

            // TODO: Go back to login screen.
        }
        
        this._jqLoadingAnimation.hide();
    }

    // #endregion //
}