import { Howl } from "howler";
import ServiceLocator from "./ServiceLocator";


export default class Jukebox
{
    private _curSongIndex: number = null;
    private _curSong: Howl = null;
    private _playerLocation: player_location_type = null;
    
    private _arrSongId: Array<string> = null;


    public constructor() {}

    public init(): void 
    {
        this._arrSongId = [
            "concerning_hobbits_shire",
            "may_it_be",
            "misty_mountains",
            "riders_of_rohan" ];
    }

    public end(): void 
    {
        if ( this._curSong )
        {
            this._curSong.stop();
            this._curSong = null;
            
            this._curSongIndex = null;
        }

        this._arrSongId = null;
        this._playerLocation = null;
    }

    public play( songIndex: number ): void
    {
        if ( this._curSong && ( this._curSong.state() == "loading" || this._curSong.playing() ) ) { return; }

        switch ( this._playerLocation )
        {
            case player_location_type.MAIN_HALL:
            {
                this._curSongIndex = songIndex;
                this._curSong = ServiceLocator.audioManager.playAmbient( this._arrSongId[ songIndex ], null );
                break;
            }

            case player_location_type.ROOM:
            {
                this._curSongIndex = songIndex;
                this._curSong = ServiceLocator.audioManager.playAmbient( this._arrSongId[ songIndex ] + "_damped", null );
                break;
            }
        }
    }

    public onMainHallEntered(): void
    {
        this._playerLocation = player_location_type.MAIN_HALL;

        if ( this._curSong && this._curSong.playing() )
        {
            const kSeek: number = this._curSong.seek() as number;
            this._curSong.stop();
            
            this._curSong = ServiceLocator.audioManager.playAmbient( this._arrSongId[ this._curSongIndex ], null );
            this._curSong.seek( kSeek );
        }
    }

    public onRoomEntered(): void
    {
        this._playerLocation = player_location_type.ROOM;

        if ( this._curSong && this._curSong.playing() )
        {
            const kSeek: number = this._curSong.seek() as number;
            this._curSong.stop();

            this._curSong = ServiceLocator.audioManager.playAmbient( this._arrSongId[ this._curSongIndex ] + "_damped", null );
            this._curSong.seek( kSeek );
        }
    }

    public onLeft(): void
    {
        this._playerLocation = player_location_type.OUTSIDE;

        if ( this._curSong )
        {
            this._curSong.stop();
            this._curSong = null;

            this._curSongIndex = null;
        }

        this._curSongIndex = null;
        this._curSong = null;
    }
}

const enum player_location_type
{
    OUTSIDE = 0,
    MAIN_HALL,
    ROOM
}