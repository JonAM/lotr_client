import Config from "./Config";
import WebUtils from "./util/WebUtils";
import GameUtils from "./util/GameUtils";
import DebugUtils from "./util/DebugUtils";
import ImgUtils from "./util/ImgUtils";
import ActionLogUtils from "./util/ActionLogUtils";
import AnimUtils from "./util/AnimUtils";


export default class Utils
{
    private static _gWebUtils: WebUtils = new WebUtils();
    private static _gGameUtils: GameUtils = new GameUtils();
    private static _gImgUtils: ImgUtils = new ImgUtils();
    private static _gActionLogUtils: ActionLogUtils = new ActionLogUtils();
    private static _gAnimUtils: AnimUtils = new AnimUtils();
    private static _gDebugUtils: DebugUtils = new DebugUtils();


    public static get web(): WebUtils { return Utils._gWebUtils; }
    public static get game(): GameUtils { return Utils._gGameUtils; }
    public static get img(): ImgUtils { return Utils._gImgUtils; }
    public static get actionLog(): ActionLogUtils { return Utils._gActionLogUtils; }
    public static get anim(): AnimUtils { return Utils._gAnimUtils; }
    public static get debug(): DebugUtils { return Utils._gDebugUtils; }


    // #region Methods //

    private constructor() {}

    // public:

    public static log( message: string ): void
    {
        if ( Config.kIsLogMode )
        {
            console.log( message );
        }
    }

    /**
     * Returns a random number between from and to.
     * 
     * @param from 
     * @param to 
     * @param isToIncluded 
     */
    public static randInterval( from: number, to: number, isToIncluded: boolean = false ): number
    {
        return Math.floor( Math.random() * ( to - from + ( isToIncluded ? 1 : 0 ) ) ) + from;
    }
    
    public static strcmp( a: string, b: string ): number
    {
        return ( a < b ? -1 : ( a > b ? 1 : 0 ) );
    }

    /**
     * Transforms the given value in a number that fits in the range [0, rangeCount-1]
     * 
     * @param value 
     * @param rangeCount 
     */
    public static fitInRange( value: number, rangeCount: number ): number
    {
        let result: number = value;

        if ( result >= rangeCount )
        {
            result = result % rangeCount;
        }
        else if ( result < 0 )
        {
            result = result % rangeCount;
            result += rangeCount;
        }

        return result;
    }

    // IMPORTANT: If nothing to split, String.split returns an array with just one element, the original string.
    public static split( str: string, separator: string ): Array<string>
    {
        let result: Array<string> = [];

        if ( str.length > 0 )
        {
            result = str.split( separator );
        }

        return result;
    }

    public static permute( array: Array<any> ): Array<Array<any>>
    {
        let result: Array<Array<any>> = [ array.slice() ];
        let heap: Array<any> = new Array( array.length ).fill( 0 );
        let i: number = 1;
        while ( i < array.length )
        {
            if ( heap[i] < i ) 
            {
                this.swap( array, i, i % 2 && heap[ i ] );
                result.push( array.slice() );
                heap[ i ]++;
                i = 1;
            } 
            else 
            {
                heap[ i ] = 0;
                i++;
            }
        }
    
        return result;
    }

    public static swap( array: Array<any>, index: number, otherIndex: number ): void 
    {
        const kValueAtIndex = array[index ];
        array[ index ] = array[ otherIndex ];
        array[ otherIndex ] = kValueAtIndex;
    }

    public static arrayHasItem( array: Array<any>, item: any ): boolean
    {
        return array.indexOf( item ) >= 0;
    }

    public static strArrayToStr( strArray: Array<string> ): string
    {
        let result: string = "";

        for ( let str of strArray )
        {
            result += str + "-";
        }

        return result.substr( 0, result.length - 1 );
    }

    public static intArrayToStr( intArray: Array<number> ): string
    {
        let result: string = "";

        for ( let str of intArray )
        {
            result += str.toString() + "-";
        }

        return result.substr( 0, result.length - 1 );
    }

    // #endregion //
}