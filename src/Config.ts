export default class Config
{
    // #region Attributes //

    // public:

    public static readonly kBuildVersion: string = "0.00.000"; // IMPORTANT: Do not change it! Real value is set in gulpfile as part of release building process.
    public static readonly kIsDebugMode: boolean = true; // IMPORTANT: Do not change it! Real value is set in gulpfile as part of release building process.
    public static readonly kIsLogMode: boolean = true; // IMPORTANT: Do not change it! Real value is set in gulpfile as part of release building process.
    public static readonly kIsResourceLoaderQueryString: boolean = false; 
    public static readonly kArrAvailableLocale: Array<string> = [ "es_ES", "en_US" ];

    // #endregion //


    // #region Properties //

    public static get kQueryString(): string { return Config.kIsDebugMode ? "" : "?lotr=" + Config.kBuildVersion; }

    // #endregion //


    // #region Methods //

    // private:

    private constructor() {}

    // #endregion //
}