export default class Url
{
    // #region Attributes //
    
    // public:

    // private: 

    private static _gServerId: server_id = null;

    // Constants.
    private static readonly _kArrResServer: Array<string> = [ 
        "http://localhost:900", 
        /*"http://theprancingpony.es" ];*/ "http://theprancingpony.epizy.com" ];
        //"http://695e-213-177-214-136.ngrok.io" ]; // NGROK:
    private static readonly _kArrAmfPhpServer: Array<string> = [ 
        "http://localhost/lotr", // TEST: :5000 for mobile.
        /*"http://theprancingpony.es" ];*/ "http://theprancingpony.epizy.com" ];
        //"http://695e-213-177-214-136.ngrok.io" ]; // NGROK:
    private static readonly _kArrSocketIOServer: Array<string> = [ 
        "http://localhost:2053",
        /*"https://lotr-server.herokuapp.com"*/ "https://lotr-server.onrender.com" ];
    private static readonly _gRingsDbServer: string = "https://ringsdb.com";
    private static readonly _gRulesReference: string = "https://images-cdn.fantasyflightgames.com/filer_public/90/19/90191e4e-a341-4379-b398-5963b7a87ebf/mec01_online_only_rules_reference_for_website.pdf";
    private static readonly _gTutorialPlaylist: string = "https://www.youtube.com/playlist?list=PLxUMLEFRQWqXI7w0xbD-MXuEmkOFVBhJ4";

    // #endregion //


    // #region Properties //

    public static get serverId(): server_id { return this._gServerId; }
    public static get webServer(): string { return Url._kArrResServer[ this._gServerId ]; }
    public static get amfPhp(): string { return Url.findAmpPhp( this._gServerId ); }
    public static get socketIOServer(): string { return Url._kArrSocketIOServer[ this._gServerId ]; }
    public static get ringsDbServer(): string { return Url._gRingsDbServer; }
    public static get help(): string { return Url._gTutorialPlaylist; }
    public static get rulesReference(): string { return Url._gRulesReference; }

    public static set serverId( value: server_id ) { this._gServerId = value; }

    // #endregion //


    // #region Methods //

    // private:

    private constructor() {}

    // public:

    public static findAmpPhp( serverId: server_id ): string
    {
        return Url._kArrAmfPhpServer[ serverId ] + "/php/?contentType=application/json";
    }

    // #endregion //
}

export const enum server_id
{
    DEBUG = 0,
    RELEASE
}