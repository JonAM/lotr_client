import ServiceLocator from "./ServiceLocator";

import Config from "./Config";
import { main_state_id } from "./states/StateId";
import Url, { server_id } from "./Url";
import * as PIXI from "pixi.js";

// States.
import AppStartupState from "./states/AppStartupState";
import GameState from "./states/GameState";
import GatewayState from "./states/GatewayState";

import AudioManager from "./service/AudioManager";
import BrowserDependency from "./service/BrowserDependency";
import LoadingScreen from "./service/LoadingScreen";
import ResourceLoader from "./service/ResourceLoader";
import ResourceStack from "./service/ResourceStack";
import SavedData from "./service/SavedData";
import SocketIOManager from "./service/SocketIOManager";
import StateMachine from "./lib/StateMachine";
import ViewManager, { view_layer_id } from "./service/ViewManager";
import DbConnector from "./DbConnector";
import CardDB from "./game/CardDB";
import Utils from "./Utils";
import PasswordResetView from "./view/common/PasswordResetView";
import ScenarioDB from "./game/ScenarioDB";
import ResourceCache from "./service/ResourceCache";


class Main
{
    // #region Methods //

    public constructor()
    { 
        Url.serverId = Config.kIsDebugMode ? server_id.DEBUG : server_id.RELEASE;

        // Global components.
        let appStartupState: AppStartupState = new AppStartupState();
        appStartupState.onComplete.addOnce( this.onAppStartup_Complete, this );
        let stateMachine: StateMachine = new StateMachine();     
        stateMachine.add( main_state_id.APP_STARTUP, appStartupState );
        stateMachine.add( main_state_id.GATEWAY, new GatewayState() );
        let gameState = new GameState();
        stateMachine.add( main_state_id.GAME, gameState );
        stateMachine.init();
        
        ServiceLocator.stateMachine = stateMachine;
        if ( !Config.kIsDebugMode && "indexedDB" in window ) 
        {
            ServiceLocator.resourceCache = new ResourceCache();
            ServiceLocator.resourceCache.init();
        }
        ServiceLocator.audioManager = new AudioManager();
        ServiceLocator.browserDependency = new BrowserDependency();
        ServiceLocator.dbConnector = new DbConnector();
        ServiceLocator.game = gameState;
        ServiceLocator.loadingScreen = new LoadingScreen();
        ServiceLocator.resourceLoader = new ResourceLoader();
        ServiceLocator.resourceStack = new ResourceStack();
        ServiceLocator.savedData = new SavedData();
        ServiceLocator.socketIOManager = new SocketIOManager();
        ServiceLocator.viewManager = new ViewManager();

        PIXI.Ticker.shared.add( ( dt: number ) => { stateMachine.onUpdate( dt ); } );

        // Entry point.
        stateMachine.request( main_state_id.APP_STARTUP );
    }

    // #endregion //


    // #region DB Callbacks /

    private onDbConfirm_Result( result: number ): void
    {
        switch ( result )
        {
            case 0:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "USER_ACCOUNT_CONFIRMATION_COMPLETED" ), view_layer_id.POPUP );
                break;
            }

            case 1:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "USER_ACCOUNT_CONFIRMATION_FAILED" ), view_layer_id.POPUP );
                break;
            }

            case 10:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
                break;
            }
        }
    }

    private onDbSetSettings_Result( result: number ): void
    {
        switch ( result )
        {
            case 0:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "NOTIFICATIONS_DISABLED" ), view_layer_id.POPUP );
                break;
            }

            case 11:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "EXPIRED_LINK" ), view_layer_id.POPUP );
                break;
            }

            case 10:
            {
                Utils.web.showInfoMessage( jQuery.i18n( "DEFAULT_ERROR" ), view_layer_id.POPUP );
                break;
            }
        }
    }

    // #endregion //


    // #region Other Callbacks //

    private onAppStartup_Complete(): void 
    {
        ServiceLocator.cardDb = new CardDB();
        ServiceLocator.scenarioDb = new ScenarioDB();

        let testCanvas: HTMLCanvasElement = document.createElement( "canvas" );
        let gl: RenderingContext = testCanvas.getContext( "webgl" ) || testCanvas.getContext( "experimental-webgl" );
        if ( !( gl && gl instanceof WebGLRenderingContext ) ) 
        {
            Utils.web.showInfoMessage( jQuery.i18n( "NO_WEB_GL_SUPPORT" ), view_layer_id.POPUP );
        }

        const kUrlParams: URLSearchParams = new URLSearchParams( window.location.search );
        if ( kUrlParams.has( "aid" ) )
        {
            switch ( parseInt( kUrlParams.get( "aid" ) ) )
            {
                case link_action_type.ACTIVATE_ACCOUNT: 
                {
                    ServiceLocator.dbConnector.post( { 
                        serviceName: "UserDAO",
                        methodName: "confirm",
                        parameters: [ kUrlParams.get( "cid" ) ] },
                        this.onDbConfirm_Result.bind( this ) );
                    break;
                }

                case link_action_type.DISABLE_NOTIFICATIONS: 
                {
                    ServiceLocator.dbConnector.post( { 
                        serviceName: "UserDAO",
                        methodName: "setSettings",
                        parameters: [ atob( kUrlParams.get( "uid" ) ), atob( kUrlParams.get( "tk" ) ), false ] },
                        this.onDbSetSettings_Result.bind( this ) );
                    break;
                }

                case link_action_type.RESET_PASSWORD: 
                {
                    let passwordResetView: PasswordResetView = new PasswordResetView();
                    passwordResetView.username = atob( kUrlParams.get( "uid" ) );
                    passwordResetView.passwordHash = atob( kUrlParams.get( "pass" ) );
                    passwordResetView.init();
                    ServiceLocator.viewManager.fadeIn( passwordResetView, view_layer_id.POPUP );
                    break;
                }
            }
        }

        ServiceLocator.stateMachine.request( main_state_id.GATEWAY );
        // TEST: SKIP:
        //ServiceLocator.stateMachine.request( main_state_id.FAST_GATEWAY );
    }

    // #endregion //
}

// When the index page has finished loading, create our app.
window.onload = () =>
{
    var main: Main = new Main();
}

const enum link_action_type
{
    ACTIVATE_ACCOUNT = 0,
    DISABLE_NOTIFICATIONS,
    RESET_PASSWORD
}