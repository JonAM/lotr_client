/// <reference path='./jquery.d.ts'/>

interface I18N 
{
    init(): void;
    load( source: any, locale?: string ): any;
    parse( key: string, parameters: any[] ): any;
    destroy(): void;
}

interface JQuery 
{
    i18n( key: string, param1: string ): any;
    i18n( key: string ): string;
    i18n( options?: any ): I18N;
}

interface JQueryStatic 
{
    i18n( key: string, param1: string ): any;
    i18n( key: string ): string;
    i18n( options?: any ): I18N;  
}
