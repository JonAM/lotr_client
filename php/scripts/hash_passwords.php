<?php
    require_once dirname(__FILE__) . "/../Services/DAO/UserDAO.php";
?>

<html>
    <body>
        <h1>Hash passwords</h1>
        <?php
            $userDAO = new UserDAO();
            $userDAO->hashPasswords();
        ?> 
        <br>
        <div>Done.</div>
    </body>
</html>
