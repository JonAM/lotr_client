<?php
    require_once dirname(__FILE__) . '/../Services/Config.php';
    require_once dirname(__FILE__) . "/../Services/MailerFactory.php";

    require_once dirname(__FILE__) . "/../Services/DAO/UserDAO.php";
    require_once dirname(__FILE__) . "/../Services/DAO/NewsDAO.php";
    
    require_once dirname(__FILE__) . '/../VO/UserVO.php';
    require_once dirname(__FILE__) . '/../VO/NewsVO.php';
?>

<html>
    <body>
        <h1>Send newsletter</h1>
        <?php
            $userDAO = new UserDAO();
            $arrUserVO = $userDAO->getUsersWithNotifications()[ 0 ];
            $settings = new Config();
            $newsDAO = new NewsDAO();
            $newsVO = $newsDAO->retrieve()[ 0 ][ 0 ];

            for ( $i = $_GET[ "from" ]; $i < ( $_GET[ "from" ] + $_GET[ "count" ] ) && $i < count( $arrUserVO ); $i++ )
            {
				$userVO = $arrUserVO[ $i ];
                print( "<div>Sending to " . $userVO->username . " (" . $userVO->email . ") ... " );

                $mailerFactory = new MailerFactory();
                $mail = $mailerFactory->create();
                //
                $mail->addAddress( $userVO->email, "" );
                $mail->Subject = "The Prancing Pony Newsletter";
                $content = "<h2>" . $newsVO->title . "</h2>";
                $content = $content . $newsVO->message . "<br><br><br>";
                $content = $content . "<i>If you don't want to continue receiving notifications like this one, please, unsubscribe by using the following <a href=\"" . $settings->httpUrl . "?aid=1&uid=" . base64_encode( $userVO->username ) . "&tk=" . base64_encode( $userVO->token ) . "\">link</a> or accessing your account settings.</i>";
                $mail->msgHTML( $content ); 
                //
                $isSuccess = $mail->send();
            
                print( ( $isSuccess ? "Success!" : "Failed" ) . "</div>" );
            }
        ?> 
        <br>
        <div>Done.</div>
    </body>
</html>
