<?php

require_once dirname(__FILE__) . '/../Config.php';
require_once dirname(__FILE__) . '/../Datasource.php';

require_once dirname(__FILE__) . '/UserDAO.php';


class PlayerReportDAO 
{
	private $conn;
	private $settings;

	// public:
	public function __construct( $set = null, $con = null ) 
	{
		$this->settings = $set == null ? new Config() : $set;
		$this->conn = $con == null ? new Datasource( $this->settings->dbHost, $this->settings->dbName, $this->settings->dbUsername, $this->settings->dbPassword ) : $con;
	}

	public function send( $username, $reported, $description, $token )
	{
		$result = 0;
		
		$userDAO = new UserDAO();
		if ( $userDAO->validateToken( $username, $token ) )
		{
			if ( strlen( $description ) > 200 )
			{
				$result = 1;
			}

			if ( $result == 0 )
			{
				$sql = "INSERT INTO player_report (username, reported, description, creationDate) VALUES ('%s', '%s', '%s', '%s')";
				$isSuccess = $this->conn->_execute( $sql, $username, $reported, $description, date( "Y-m-d H:i:s" ) );
				if ( !$isSuccess )
				{
					$result = 10; // server error.
				}
			}
		}
		else
		{
			$result = 11; // invalid token.
		}

		return array( $result );
	}
}
?>