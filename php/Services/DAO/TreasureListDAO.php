<?php

require_once dirname(__FILE__) . '/../Config.php';
require_once dirname(__FILE__) . '/../Datasource.php';

require_once dirname(__FILE__) . '/../../VO/TreasureListVO.php';
require_once dirname(__FILE__) . '/UserDAO.php';


class TreasureListDAO 
{
	private $conn;
	private $settings;

	// public:
	public function __construct( $set = null, $con = null ) 
	{
		$this->settings = $set == null ? new Config() : $set;
		$this->conn = $con == null ? new Datasource( $this->settings->dbHost, $this->settings->dbName, $this->settings->dbUsername, $this->settings->dbPassword ) : $con;
	}

	public function retrieve( $username )
	{
		$result = null;
		
		$sql = "SELECT * FROM treasure_list WHERE username = '%s' OR ally = '%s'";
		$result = $this->_listQuery( $sql, $username, $username );
		
		return array( $result );
	}
	
	public function add( $title, $username, $playerHeroes, $ally, $allyHeroes, $scenarioId, $list, $token )
	{
		$result = 0;
		
		$userDAO = new UserDAO();
		if ( $userDAO->validateToken( $username, $token ) )
		{
			$sql = "SELECT * FROM treasure_list WHERE title = '%s' AND ( username = '%s' OR ally = '%s' )";
			$searchResult = $this->_listQuery( $sql, $title, $username, $username );
			if ( count( $searchResult ) > 0 )
			{
					$result = 10; // existing treasure list.
			}
			else
			{
				$sql = "SELECT * FROM treasure_list WHERE username = '%s'";
				$searchResult = $this->_listQuery( $sql, $username );
				if ( count( $searchResult ) >= 5 )
				{
					$result = 1; // capacity limit.
				}

				if ( $result == 0 )
				{
					$sql = "INSERT INTO treasure_list (title, creationDate, username, playerHeroes, ally, allyHeroes, scenarioId, list) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')";
					$isSuccess = $this->conn->_execute( $sql, $title, date( "Y-m-d H:i:s" ), $username, $playerHeroes, $ally, $allyHeroes, $scenarioId, $list );
					if ( !$isSuccess )
					{
						$result = 10; // server error.
					}
				}
			}
		}
		else
		{
			$result = 11; // invalid token.
		}

		return array( $result );
	}
	
	public function update( $title, $username, $scenarioId, $list, $token )
	{
		$result = 0;
		
		$userDAO = new UserDAO();
		if ( $userDAO->validateToken( $username, $token ) )
		{
			$sql = "SELECT * FROM treasure_list WHERE title = '%s' AND ( username = '%s' OR ally = '%s' )";
			$searchResult = $this->_listQuery( $sql, $title, $username, $username );
			if ( count( $searchResult ) > 0 )
			{
				if ( searchResult[ 0 ]->username == $username )
				{
					$sql = "UPDATE treasure_list SET scenarioId = '%s', list = '%s' WHERE title = '%s' AND username = '%s'";
				}
				else
				{
					$sql = "UPDATE treasure_list SET scenarioId = '%s', list = '%s' WHERE title = '%s' AND ally = '%s'";
				}
				$isSuccess = $this->conn->_execute( $sql, $scenarioId, $list, $title, $username );
				if ( !$isSuccess )
				{
					$result = 10; // server error.
				}
			}
			else
			{
				$result = 10; // treasure list not found.
			}
		}
		else
		{
			$result = 11; // invalid token.
		}

		return array( $result );
	}
	
	public function remove( $title, $username, $token )
	{
		$result = 1; // treasure list not found.

		$userDAO = new UserDAO();
		if ( $userDAO->validateToken( $username, $token ) )
		{
			$sql = "SELECT * FROM treasure_list WHERE title = '%s' AND username = '%s'";
			$searchResult = $this->_listQuery( $sql, $title, $username );
			if ( count( $searchResult ) > 0 )
			{
				$sql = "DELETE FROM treasure_list WHERE title = '%s' AND username = '%s'";
				$isSuccess = $this->conn->_execute( $sql, $title, $username );
				$result = $isSuccess ? 0 : 10;
			}
		}
		else
		{
			$result = 11; // invalid token.
		}

		return array( $result );
	}
	
	// private:

	private function _listQuery() 
	{
		$searchResult = array();
		$result = $this->conn->_execute( func_get_args() );
		$temp = new TreasureListVO();	
	
		while ( $row = $this->conn->_nextRow( $result ) ) 
		{
			$temp = new TreasureListVO();	
			
			$temp->title = $row[ 0 ];
			$temp->dateTimestamp = strtotime( $row[ 1 ] );
			$temp->username = $row[ 2 ];
			$temp->playerHeroes = $row[ 3 ];
			$temp->ally = $row[ 4 ];
			$temp->allyHeroes = $row[ 5 ];
			$temp->scenarioId = $row[ 6 ];
			$temp->list = $row[ 7 ];
			//
			$temp->whenTimestamp = time() - strtotime( $row[ 1 ] );
			
			$searchResult[] = $temp;
		}

		return $searchResult;
	}
}
?>