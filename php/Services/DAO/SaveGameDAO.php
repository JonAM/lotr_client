<?php

require_once dirname(__FILE__) . '/../Config.php';
require_once dirname(__FILE__) . '/../Datasource.php';

require_once dirname(__FILE__) . '/../../VO/SaveGameVO.php';
require_once dirname(__FILE__) . '/UserDAO.php';


class SaveGameDAO 
{
	private $conn;
	private $settings;

	// public:
	public function __construct( $set = null, $con = null ) 
	{
		$this->settings = $set == null ? new Config() : $set;
		$this->conn = $con == null ? new Datasource( $this->settings->dbHost, $this->settings->dbName, $this->settings->dbUsername, $this->settings->dbPassword ) : $con;
	}

	public function retrieve( $username )
	{
		$result = null;
		
		$sql = "SELECT * FROM save_game WHERE username = '%s'";
		$result = $this->_listQuery( $sql, $username );
		
		return array( $result );
	}

	public function add( $title, $username, $ally, $content, $token )
	{
		$result = 0;
		
		$userDAO = new UserDAO();
		if ( $userDAO->validateToken( $username, $token ) )
		{
			$sql = "SELECT * FROM save_game WHERE title = '%s' AND username = '%s'";
			$searchResult = $this->_listQuery( $sql, $title, $username );
			if ( count( $searchResult ) > 0 )
			{
				// Overwrite save game.
				$sql = "UPDATE save_game SET ally = '%s', content = '%s', creationDate = '%s' WHERE title = '%s' AND username = '%s'";
				$isSuccess = $this->conn->_execute( $sql, $ally, $content, date( "Y-m-d H:i:s" ), $title, $username );
				if ( !$isSuccess )
				{
					$result = 10; // server error.
				}
			}
			else
			{
				// New save game.
				if ( $result == 0 )
				{
					$sql = "SELECT * FROM save_game WHERE username = '%s'";
					$searchResult = $this->_listQuery( $sql, $username );
					if ( count( $searchResult ) >= 5 )
					{
						$result = 1; // capacity limit.
					}
				}

				if ( $result == 0 )
				{
					$sql = "INSERT INTO save_game (title, username, ally, creationDate, content) VALUES ('%s', '%s', '%s', '%s', '%s')";
					$isSuccess = $this->conn->_execute( $sql, $title, $username, $ally, date( "Y-m-d H:i:s" ), $content );
					if ( !$isSuccess )
					{
						$result = 10; // server error.
					}
				}
			}
		}
		else
		{
			$result = 11; // invalid token.
		}

		return array( $result );
	}
	
	public function remove( $title, $username, $token )
	{
		$result = 1; // save game not found.

		$userDAO = new UserDAO();
		if ( $userDAO->validateToken( $username, $token ) )
		{
			$sql = "SELECT * FROM save_game WHERE title = '%s' AND username = '%s'";
			$searchResult = $this->_listQuery( $sql, $title, $username );
			if ( count( $searchResult ) > 0 )
			{
				$sql = "DELETE FROM save_game WHERE title = '%s' AND username = '%s'";
				$isSuccess = $this->conn->_execute( $sql, $title, $username );
				$result = $isSuccess ? 0 : 10;
			}
		}
		else
		{
			$result = 11; // invalid token.
		}

		return array( $result );
	}
	
	// private:
	private function _listQuery() 
	{
		$searchResult = array();
		$result = $this->conn->_execute( func_get_args() );
		$temp = new SaveGameVO();	
	
		while ( $row = $this->conn->_nextRow( $result ) ) 
		{
			$temp = new SaveGameVO();	
			
			$temp->title = $row[ 0 ];
			$temp->username = $row[ 1 ];
			$temp->ally = $row[ 2 ];
			$temp->msSinceCreation = time() - strtotime( $row[ 3 ] );
			$temp->content = $row[ 4 ];
			
			$searchResult[] = $temp;
		}

		return $searchResult;
	}
}
?>