<?php

require_once dirname(__FILE__) . '/../Config.php';
require_once dirname(__FILE__) . '/../Datasource.php';

require_once dirname(__FILE__) . '/SaveGameDAO.php';
require_once dirname(__FILE__) . '/TreasureListDAO.php';
require_once dirname(__FILE__) . '/CampaignLogDAO.php';
require_once dirname(__FILE__) . '/UserDAO.php';


class GameRoomManager 
{
	private $conn;
	private $settings;

	// public:
	public function __construct( $set = null, $con = null ) 
	{
		$this->settings = $set == null ? new Config() : $set;
		$this->conn = $con == null ? new Datasource( $this->settings->dbHost, $this->settings->dbName, $this->settings->dbUsername, $this->settings->dbPassword ) : $con;
	}

	public function retrieveCreatorData( $username )
	{
		$saveGameDAO = new SaveGameDAO();
		$arrSaveGameVO = $saveGameDAO->retrieve( $username );
		
		$treasureListDAO = new TreasureListDAO();
		$arrTreasureListVO = $treasureListDAO->retrieve( $username );
		
		$campaignLogDAO = new CampaignLogDAO();
		$arrCampaignLogVO = $campaignLogDAO->retrieve( $username );
		
		return array( $arrSaveGameVO[ 0 ], $arrTreasureListVO[ 0 ], $arrCampaignLogVO[ 0 ] );
	}
}
?>