<?php

require_once dirname(__FILE__) . '/../Config.php';
require_once dirname(__FILE__) . '/../Datasource.php';

require_once dirname(__FILE__) . '/../../VO/CampaignLogVO.php';
require_once dirname(__FILE__) . '/UserDAO.php';


class CampaignLogDAO 
{
	private $conn;
	private $settings;

	// public:
	public function __construct( $set = null, $con = null ) 
	{
		$this->settings = $set == null ? new Config() : $set;
		$this->conn = $con == null ? new Datasource( $this->settings->dbHost, $this->settings->dbName, $this->settings->dbUsername, $this->settings->dbPassword ) : $con;
	}

	public function retrieve( $username )
	{
		$result = null;
		
		$sql = "SELECT * FROM campaign_log WHERE username = '%s' OR ally = '%s'";
		$result = $this->_listQuery( $sql, $username, $username );
		
		return array( $result );
	}
	
	public function add( $title, $username, $playerHeroes, $ally, $allyHeroes, $fallenHeroes, $threatPenalty, $notes, $completedScenarios, $boons, $burdens, $token )
	{
		$result = 0;
		
		$userDAO = new UserDAO();
		if ( $userDAO->validateToken( $username, $token ) )
		{
			$sql = "SELECT * FROM campaign_log WHERE title = '%s' AND username = '%s'";
			$searchResult = $this->_listQuery( $sql, $title, $username );
			if ( count( $searchResult ) > 0 )
			{
				// Update treasure list.
				$sql = "UPDATE campaign_log SET playerHeroes = '%s', allyHeroes = '%s', fallenHeroes = '%s', threatPenalty = %d, notes = '%s', completedScenarios = '%s', boons = '%s', burdens = '%s' WHERE title = '%s' AND username = '%s'";
				$isSuccess = $this->conn->_execute( $sql, $playerHeroes, $allyHeroes, $fallenHeroes, $threatPenalty, $notes, $completedScenarios, $boons, $burdens );
				if ( !$isSuccess )
				{
					$result = 10; // server error.
				}
			}
			else
			{
				$sql = "SELECT * FROM campaign_log WHERE username = '%s'";
				$searchResult = $this->_listQuery( $sql, $username );
				if ( count( $searchResult ) >= 5 )
				{
					$result = 1; // capacity limit.
				}

				if ( $result == 0 )
				{
					$sql = "INSERT INTO campaign_log (title, creationDate, username, playerHeroes, ally, allyHeroes, fallenHeroes, threatPenalty, notes, completedScenarios, boons, burdens) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s', '%s', '%s', '%s')";
					$isSuccess = $this->conn->_execute( $sql, $title, date( "Y-m-d H:i:s" ), $username, $playerHeroes, $ally, $allyHeroes, $fallenHeroes, $threatPenalty, $notes, $completedScenarios, $boons, $burdens );
					if ( !$isSuccess )
					{
						$result = 10; // server error.
					}
				}
			}
		}
		else
		{
			$result = 11; // invalid token.
		}

		return array( $result );
	}
	
	public function remove( $title, $username, $token )
	{
		$result = 1; // save game not found.

		$userDAO = new UserDAO();
		if ( $userDAO->validateToken( $username, $token ) )
		{
			$sql = "SELECT * FROM campaign_log WHERE title = '%s' AND username = '%s'";
			$searchResult = $this->_listQuery( $sql, $title, $username );
			if ( count( $searchResult ) > 0 )
			{
				$sql = "DELETE FROM campaign_log WHERE title = '%s' AND username = '%s'";
				$isSuccess = $this->conn->_execute( $sql, $title, $username );
				$result = $isSuccess ? 0 : 10;
			}
		}
		else
		{
			$result = 11; // invalid token.
		}

		return array( $result );
	}
	
	// private:

	private function _listQuery() 
	{
		$searchResult = array();
		$result = $this->conn->_execute( func_get_args() );
		$temp = new CampaignLogVO();	
	
		while ( $row = $this->conn->_nextRow( $result ) ) 
		{
			$temp = new CampaignLogVO();	
			
			$temp->title = $row[ 0 ];
			$temp->dateTimestamp = strtotime( $row[ 1 ] );
			$temp->username = $row[ 2 ];
			$temp->playerHeroes = $row[ 3 ];
			$temp->ally = $row[ 4 ];
			$temp->allyHeroes = $row[ 5 ];
			$temp->fallenHeroes = $row[ 6 ];
			$temp->threatPenalty = $row[ 7 ];
			$temp->notes = $row[ 8 ];
			$temp->completedScenarios = $row[ 9 ];
			$temp->boons = $row[ 10 ];
			$temp->burdens = $row[ 11 ];
			//
			$temp->whenTimestamp = time() - strtotime( $row[ 1 ] );
			
			$searchResult[] = $temp;
		}

		return $searchResult;
	}
}
?>