<?php

require_once dirname(__FILE__) . '/../Config.php';
require_once dirname(__FILE__) . '/../Datasource.php';

require_once dirname(__FILE__) . '/../../VO/DeckVO.php';
require_once dirname(__FILE__) . '/UserDAO.php';


class DeckDAO 
{
	private $conn;
	private $settings;

	// public:
	public function __construct( $set = null, $con = null ) 
	{
		$this->settings = $set == null ? new Config() : $set;
		$this->conn = $con == null ? new Datasource( $this->settings->dbHost, $this->settings->dbName, $this->settings->dbUsername, $this->settings->dbPassword ) : $con;
	}

	public function retrieve( $username )
	{
		$result = null;
		
		$sql = "SELECT * FROM deck WHERE username = '%s'";
		$result = $this->_listQuery( $sql, $username );
		
		return array( $result );
	}

	public function add( $title, $username, $content, $token )
	{
		$result = 0;
		
		$userDAO = new UserDAO();
		if ( $userDAO->validateToken( $username, $token ) )
		{
			$sql = "SELECT * FROM deck WHERE title = '%s' AND username = '%s'";
			$searchResult = $this->_listQuery( $sql, $title, $username );
			if ( count( $searchResult ) != 0 )
			{
				$result = 1; // duplicated title.
			}

			if ( $result == 0 )
			{
				$sql = "SELECT * FROM deck WHERE username = '%s'";
				$searchResult = $this->_listQuery( $sql, $username );
				if ( count( $searchResult ) >= 15 )
				{
					$result = 2; // capacity limit.
				}
			}

			if ( $result == 0 )
			{
				$sql = "INSERT INTO deck (title, username, content) VALUES ('%s', '%s', '%s')";
				$isSuccess = $this->conn->_execute( $sql, $title, $username, $content );
				if ( !$isSuccess )
				{
					$result = 10; // server error.
				}
			}
		}
		else
		{
			$result = 11; // invalid token.
		}

		return array( $result );
	}
	
	public function remove( $title, $username, $token )
	{
		$result = 1; // deck not found.

		$userDAO = new UserDAO();
		if ( $userDAO->validateToken( $username, $token ) )
		{
			$sql = "SELECT * FROM deck WHERE title = '%s' AND username = '%s'";
			$searchResult = $this->_listQuery( $sql, $title, $username );
			if ( count( $searchResult ) > 0 )
			{
				$sql = "DELETE FROM deck WHERE title = '%s' AND username = '%s'";
				$isSuccess = $this->conn->_execute( $sql, $title, $username );
				$result = $isSuccess ? 0 : 10;
			}
		}
		else
		{
			$result = 11; // invalid token.
		}

		return array( $result );
	}
	
	// private:
	private function _listQuery() 
	{
		$searchResult = array();
		$result = $this->conn->_execute( func_get_args() );
		$temp = new DeckVO();	
	
		while ( $row = $this->conn->_nextRow( $result ) ) 
		{
			$temp = new DeckVO();	
			
			$temp->title = $row[ 0 ];
			$temp->username = $row[ 1 ];
			$temp->content = $row[ 2 ];
			
			$searchResult[] = $temp;
		}

		return $searchResult;
	}
}
?>