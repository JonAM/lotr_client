<?php

require_once dirname(__FILE__) . '/../Config.php';
require_once dirname(__FILE__) . '/../Datasource.php';

require_once dirname(__FILE__) . '/../../VO/NewsVO.php';


class NewsDAO 
{
	private $conn;
	private $settings;

	// public:
	public function __construct( $set = null, $con = null ) 
	{
		$this->settings = $set == null ? new Config() : $set;
		$this->conn = $con == null ? new Datasource( $this->settings->dbHost, $this->settings->dbName, $this->settings->dbUsername, $this->settings->dbPassword ) : $con;
	}

	public function retrieve()
	{
		$result = null;
		
		$sql = "SELECT * FROM news ORDER BY creationDate DESC";
		$result = $this->_listQuery( $sql );
		
		return array( $result );
	}
	
	// private:
	private function _listQuery() 
	{
		$searchResult = array();
		$result = $this->conn->_execute( func_get_args() );
		$temp = new NewsVO();	
	
		while ( $row = $this->conn->_nextRow( $result ) ) 
		{
			$temp = new NewsVO();	
			
			$temp->creationDate = $row[ 0 ];
			$temp->title = $row[ 1 ];
			$temp->message = $row[ 2 ];
			
			$searchResult[] = $temp;
		}

		return $searchResult;
	}
}
?>