<?php

require_once dirname(__FILE__) . '/../Config.php';
require_once dirname(__FILE__) . '/../Datasource.php';

require_once dirname(__FILE__) . '/../../VO/GameHistoryVO.php';
require_once dirname(__FILE__) . '/UserDAO.php';


class GameHistoryDAO 
{
	private $conn;
	private $settings;

	// public:
	public function __construct( $set = null, $con = null ) 
	{
		$this->settings = $set == null ? new Config() : $set;
		$this->conn = $con == null ? new Datasource( $this->settings->dbHost, $this->settings->dbName, $this->settings->dbUsername, $this->settings->dbPassword ) : $con;
	}

	public function getHistory( $username )
	{
		$sql = "SELECT * FROM game_history WHERE username = '%s' ORDER BY date DESC";
		$arrGameHistoryVO = $this->_listQuery( $sql, $username );

		return $arrGameHistoryVO;
	}
	
	public function setHistory( $username, $scenarioId, $ally, $token )
	{
		$result = 0;
		
		$userDAO = new UserDAO();
		if ( $userDAO->validateToken( $username, $token ) )
		{
			$sql = "INSERT INTO game_history (username, date, scenarioId, ally) VALUES ('%s', '%s', '%s', '%s')";
			$isSuccess = $this->conn->_execute( $sql, $username, date( "Y-m-d H:i:s", time() ), $scenarioId, $ally );
			
			if ( !$isSuccess )
			{
				$result = 10; // server error.
			}
		}
		else
		{
			$result = 11; // invalid token.
		}

		return array( $result );
	}
	
	// private:

	private function _listQuery() 
	{
		$searchResult = array();
		$result = $this->conn->_execute( func_get_args() );
		$temp = new GameHistoryVO();	
	
		while ( $row = $this->conn->_nextRow( $result ) ) 
		{
			$temp = new GameHistoryVO();	
			
			$temp->username = $row[ 0 ];
			$temp->dateTimestamp = strtotime( $row[ 1 ] );
			$temp->scenarioId = $row[ 2 ];
			$temp->ally = $row[ 3 ];
			//
			$temp->whenTimestamp = time() - strtotime( $row[ 1 ] );
			
			$searchResult[] = $temp;
		}

		return $searchResult;
	}
}
?>