<?php

require_once dirname(__FILE__) . '/../Config.php';
require_once dirname(__FILE__) . '/../Datasource.php';

require_once dirname(__FILE__) . '/../../VO/PlayerProgressVO.php';
require_once dirname(__FILE__) . '/UserDAO.php';


class PlayerProgressDAO 
{
	private $conn;
	private $settings;

	// public:
	public function __construct( $set = null, $con = null ) 
	{
		$this->settings = $set == null ? new Config() : $set;
		$this->conn = $con == null ? new Datasource( $this->settings->dbHost, $this->settings->dbName, $this->settings->dbUsername, $this->settings->dbPassword ) : $con;
	}

	public function getProgress( $username )
	{
		$sql = "SELECT * FROM player_progress WHERE playerOne = '%s' OR playerTwo = '%s'";
		$arrPlayerProgressVO = $this->_listQuery( $sql, $username, $username );

		return array( $arrPlayerProgressVO );
	}
	
	public function setProgress( $playerOne, $playerOneDeck, $playerTwo, $playerTwoDeck, $scenarioId, $score, $token )
	{
		$result = 0;
		
		$userDAO = new UserDAO();
		if ( $userDAO->validateToken( $playerOne, $token ) )
		{
			$sql = "SELECT * FROM player_progress WHERE ( playerOne = '%s' OR playerOne = '%s' ) AND ( playerTwo = '%s' OR playerTwo = '%s' ) AND scenarioId = '%s'";
			$arrPlayerProgressVO = $this->_listQuery( $sql, $playerOne, $playerTwo, $playerTwo, $playerOne, $scenarioId );
			if ( count( $arrPlayerProgressVO ) == 0 )
			{
				$sql = "INSERT INTO player_progress (playerOne, playerOneDeck, playerTwo, playerTwoDeck, scenarioId, date, score) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', %d)";
				$isSuccess = $this->conn->_execute( $sql, $playerOne, $playerOneDeck, $playerTwo, $playerTwoDeck, $scenarioId, date( "Y-m-d H:i:s", time() ), $score );
				
				if ( !$isSuccess )
				{
					$result = 10; // server error.
				}
			}
			else
			{
				if ( $score > $arrPlayerProgressVO[ 0 ]->score )
				{
					$sql = "UPDATE player_progress SET playerOne = '%s', playerOneDeck = '%s', playerTwo = '%s', playerTwoDeck = '%s', date = '%s', score = %d WHERE ( playerOne = '%s' OR playerOne = '%s' ) AND ( playerTwo = '%s' OR playerTwo = '%s' ) AND scenarioId = '%s'";
					$isSuccess = $this->conn->_execute( $sql, $playerOne, $playerOneDeck, $playerTwo, $playerTwoDeck, date( "Y-m-d H:i:s", time() ), $score, $playerOne, $playerTwo, $playerTwo, $playerOne, $scenarioId );
					if ( !$isSuccess )
					{
						$result = 10; // server error.
					}
				}
			}
		}
		else
		{
			$result = 11; // invalid token.
		}

		return array( $result );
	}
	
	// private:

	private function _listQuery() 
	{
		$searchResult = array();
		$result = $this->conn->_execute( func_get_args() );
		$temp = new PlayerProgressVO();	
	
		while ( $row = $this->conn->_nextRow( $result ) ) 
		{
			$temp = new PlayerProgressVO();	
			
			$temp->playerOne = $row[ 0 ];
			$temp->playerOneDeck = $row[ 1 ];
			$temp->playerTwo = $row[ 2 ];
			$temp->playerTwoDeck = $row[ 3 ];
			$temp->dateTimestamp = strtotime( $row[ 4 ] );
			$temp->scenarioId = $row[ 5 ];
			$temp->score = $row[ 6 ];
			//
			$temp->whenTimestamp = time() - strtotime( $row[ 4 ] );
			
			$searchResult[] = $temp;
		}

		return $searchResult;
	}
}
?>