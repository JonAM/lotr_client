<?php

require_once dirname(__FILE__) . '/../Config.php';
require_once dirname(__FILE__) . '/../Datasource.php';
require_once dirname(__FILE__) . '/../MailerFactory.php';
require_once dirname(__FILE__) . '/../PHPMailer/Exception.php';
require_once dirname(__FILE__) . '/../PHPMailer/PHPMailer.php';
require_once dirname(__FILE__) . '/../PHPMailer/SMTP.php';

require_once dirname(__FILE__) . '/../../VO/UserVO.php';
require_once dirname(__FILE__) . '/GameHistoryDAO.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


class UserDAO 
{
	private $conn;
	private $settings;

	// public:
	public function __construct( $set = null, $con = null ) 
	{
		$this->settings = $set == null ? new Config() : $set;
		$this->conn = $con == null ? new Datasource( $this->settings->dbHost, $this->settings->dbName, $this->settings->dbUsername, $this->settings->dbPassword ) : $con;
	}

	public function register( $username, $password, $email )
	{
		$result = 0; 
		
		$sql = "SELECT * FROM user WHERE username = '%s'";
		$searchResult = $this->_listQuery( $sql, $username );
		if ( count( $searchResult ) > 0 )
		{
			$result = 1; // duplicated username.
		}

		if ( $result == 0 )
		{
			$sql = "SELECT * FROM user WHERE email = '%s'";
			$searchResult = $this->_listQuery( $sql, $email );
			if ( count( $searchResult ) > 0 )
			{
				if ( $searchResult[ 0 ]->confirmationId == 0 )
				{
					$result = 2; // duplicated email.
				}
				else
				{
					$sql = "DELETE FROM user WHERE email = '%s'";
					$isSuccess = $this->conn->_execute( $sql, $email );
					if ( !$isSuccess )
					{
						$result = 10;
					}
				}
			}
		}

		$confirmationId = null;
		if ( $result == 0 )
		{
			$confirmationId = time();

			$mailerFactory = new MailerFactory();
			$mail = $mailerFactory->create();
			
			$mail->addAddress( $email, "" );
			$mail->Subject = "The Prancing Pony account activation";
			$content = "Hi " . $username . ",<br><br>";
			$content = $content . "Welcome to The Prancing Pony! Please, click on the following link to activate your account.<br><br>";
			$content = $content . "<a href=\"" . $this->settings->httpUrl . "?aid=0&cid=" . $confirmationId . "\">Activate account</a><br><br>";
			$content = $content . "If you have problems activating your account, please, send an email to lotrtheprancingpony@gmail.com detailing the issue.<br><br>";
			//$content = $content . "If you have problems activating your account, please, send an email to jon@theprancingpony.es detailing the issue.<br><br>";
			$content = $content . "Have a great day,<br><br>";
			$content = $content . "- Jon<br>";
			$content = $content . "lotrtheprancingpony@gmail.com";
			//$content = $content . "jon@theprancingpony.es";
			$mail->msgHTML( $content ); 
		
			if ( !$mail->send() )
			{
				$result = 10; // server error.
			}
		}

		if ( $result == 0 )
		{
			$sql = "INSERT INTO user (username, password, email, joinDate, confirmationId) VALUES ('%s', '%s', '%s', '%s', '%d')";
			$isSuccess = $this->conn->_execute( $sql, $username, password_hash( $password, PASSWORD_DEFAULT ), $email, date( "Y-m-d H:i:s" ), $confirmationId );
			if ( !$isSuccess )
			{
				$result = 10; // server error.
			}
		}

		return array( $result );
	}
	
	public function login( $username, $password )
	{
		$result = 1; // wrong username/password.
		$token = null;
		
		$sql = "SELECT * FROM user WHERE username = '%s'";
		$searchResult = $this->_listQuery( $sql, $username );
		if ( count( $searchResult ) == 1 && password_verify( $password, $searchResult[ 0 ]->password ) )
		{
			if ( $searchResult[ 0 ]->confirmationId > 0 )
			{
				$result = 2;
			}
			else
			{
				$token = bin2hex( openssl_random_pseudo_bytes( 20 ) );

				$sql = "UPDATE user SET token = '%s', lastConnDate = '%s' WHERE username = '%s'";
				$isSuccess = $this->conn->_execute( $sql, $token, date( "Y-m-d H:i:s", time() ), $username );
				$result = $isSuccess ? 0 : 10; // success or server error.
			}
		}
		
		return array( $result, $token );
	}
	
	public function rate( $username, $rated, $isGood, $token )
	{
		$result = 0;
		
		if ( $this->validateToken( $username, $token ) )
		{
			$sql = "UPDATE user SET karma = karma " . ( $isGood ? "+ 2" : "- 1" ) . " WHERE username = '%s'";
			$isSuccess = $this->conn->_execute( $sql, $rated );
			if ( !$isSuccess )
			{
				$result = 10; // server error.
			}
		}
		else
		{
			$result = 11; // invalid token.
		}

		return array( $result );
	}

	public function validateToken( $username, $token )
	{
		if ( $this->settings->dbHost == "localhost" )
		{
			return true;
		}
		else
		{
			$sql = "SELECT * FROM user WHERE username = '%s' AND token = '%s'";
			$searchResult = $this->_listQuery( $sql, $username, $token );
			
			return count( $searchResult ) == 1;
		}
	}

	public function getProfile( $username )
	{
		$result = 1; // username not found.
		$arrUserVO = null;
		$arrGameHistoryVO = null;

		$sql = "SELECT * FROM user WHERE username = '%s'";
		$arrUserVO = $this->_listQuery( $sql, $username );
		if ( count( $arrUserVO ) == 1 )
		{
			$result = 0;

			$this->_hideSensitiveData( $arrUserVO[ 0 ] );

			$gameHistoryDAO = new GameHistoryDAO();
			$arrGameHistoryVO = $gameHistoryDAO->getHistory( $username );
		}

		return array( $result, $arrUserVO[ 0 ], $arrGameHistoryVO );
	}

	public function getSettings( $username, $token )
	{
		$result = 0;
		$isReceiveNotifications = false;

		if ( $this->validateToken( $username, $token ) )
		{
			$sql = "SELECT * FROM user WHERE username = '%s'";
			$arrUserVO = $this->_listQuery( $sql, $username );
			if ( count( $arrUserVO ) == 1 )
			{
				$result = 0; // success.

				$isReceiveNotifications = intval( $arrUserVO[ 0 ]->isReceiveNotifications );
			}
		}
		else
		{
			$result = 11; // invalid token.
		}

		return array( $result, $isReceiveNotifications );
	}

	public function setSettings( $username, $token, $isReceiveNotifications )
	{
		$result = 0;

		if ( $this->validateToken( $username, $token ) )
		{
			$sql = "UPDATE user SET isReceiveNotifications = " . ( $isReceiveNotifications ? "1" : "0" ) . " WHERE username = '%s'";
			$isSuccess = $this->conn->_execute( $sql, $username );
			if ( !$isSuccess )
			{
				$result = 10; // server error.
			}
		}
		else
		{
			$result = 11; // invalid token.
		}

		return array( $result, $isReceiveNotifications );
	}

	public function deleteAccount( $username, $token )
	{
		$result = 0;

		if ( $this->validateToken( $username, $token ) )
		{
			$sql = "DELETE FROM user WHERE username = '%s'";
			$isSuccess = $this->conn->_execute( $sql, $username );
			if ( !$isSuccess )
			{
				$result = 10;
			}
		}
		else
		{
			$result = 11; // invalid token.
		}

		return array( $result );
	}

	public function getUserList()
	{
		$arrUserVO = array();

		$sql = "SELECT * FROM user WHERE confirmationId = 0";
		$arrUserVO = $this->_listQuery( $sql );
		foreach ( $arrUserVO as &$userVO ) 
		{
			$this->_hideSensitiveData( $userVO );
		}

		return array( $arrUserVO );
	}

	public function getUsersWithNotifications()
	{
		$arrUserVO = array();

		$sql = "SELECT * FROM user WHERE confirmationId = 0 AND isReceiveNotifications = 1";
		$arrUserVO = $this->_listQuery( $sql );

		return array( $arrUserVO );
	}

	public function getUserCount()
	{
		$arrUserVO = array();

		$sql = "SELECT * FROM user WHERE confirmationId = 0";
		$arrUserVO = $this->_listQuery( $sql );

		return array( count( $arrUserVO ) );
	}

	public function resendConfirmationEmail( $username )
	{
		$result = 0; // success.
		
		$sql = "SELECT * FROM user WHERE username = '%s'";
		$searchResult = $this->_listQuery( $sql, $username );
		if ( count( $searchResult ) == 0 )
		{
			$result = 1; // username not found.
		}

		$confirmationId = null;
		if ( $result == 0 )
		{
			$confirmationId = time();

			$mailerFactory = new MailerFactory();
			$mail = $mailerFactory->create();
			
			$mail->addAddress( $searchResult[ 0 ]->email, "" );
			$mail->Subject = "The Prancing Pony account activation";
			$content = "Hi " . $username . ",<br><br>";
			$content = $content . "Welcome to The Prancing Pony! Please, click on the following link to activate your account.<br><br>";
			$content = $content . "<a href=\"" . $this->settings->httpUrl . "?aid=0&cid=" . $confirmationId . "\">Activate account</a><br><br>";
			$content = $content . "If you have problems activating your account, please, send an email to lotrtheprancingpony@gmail.com detailing the issue.<br><br>";
			//$content = $content . "If you have problems activating your account, please, send an email to jon@theprancingpony.es detailing the issue.<br><br>";
			$content = $content . "Have a great day,<br><br>";
			$content = $content . "- Jon<br>";
			$content = $content . "lotrtheprancingpony@gmail.com";
			//$content = $content . "jon@theprancingpony.es";
			$mail->msgHTML( $content ); 
		
			if ( !$mail->send() )
			{
				$result = 10; // server error.
			}
		}

		if ( $confirmationId != null )
		{
			$sql = "UPDATE user SET confirmationId = '%d' WHERE username = '%s'";
			$isSuccess = $this->conn->_execute( $sql, $confirmationId, $username );
			if ( !$isSuccess )
			{
				$result = 10; // server error.
			}
		}

		return array( $result );
	}

	public function recover( $email )
	{
		$result = 1; // email not found.

		$sql = "SELECT * FROM user WHERE email = '%s' AND confirmationId = 0";
		$arrUserVO = $this->_listQuery( $sql, $email );
		if ( count( $arrUserVO ) == 1 )
		{
			$result = 0; // success.

			$mailerFactory = new MailerFactory();
			$mail = $mailerFactory->create();

			$mail->addAddress( $email, "" );
			$mail->Subject = "The Prancing Pony password reset";
			$content = "Hi " . $arrUserVO[ 0 ]->username . ",<br><br>";
			$content = $content . "You are receiving this email because you have forgotten your password.<br><br>";
			$content = $content . "Please, click on the following link to reset your password.<br><br>";
			$content = $content . "<a href=\"" . $this->settings->httpUrl . "?aid=2&uid=" . base64_encode( $arrUserVO[ 0 ]->username ) . "&pass=" . base64_encode( $arrUserVO[ 0 ]->password ) . "\">Reset password</a><br><br>";
			$content = $content . "If you are still having problems to connect with your account, please, send an email to lotrtheprancingpony@gmail.com detailing the issue.<br><br>";
			//$content = $content . "If you are still having problems to connect with your account, please, send an email to jon@theprancingpony.es detailing the issue.<br><br>";
			$content = $content . "Have a great day,<br><br>";
			$content = $content . "- Jon<br>";
			$content = $content . "lotrtheprancingpony@gmail.com";
			//$content = $content . "jon@theprancingpony.es";
			$mail->msgHTML( $content ); 
		
			if ( !$mail->send() )
			{
				$result = 10; // server error.
			}
		} 

		return array( $result );
	}

	public function confirm( $cid )
	{
		$result = 1; // wrong cid.
		$token = null;
		
		$sql = "SELECT * FROM user WHERE confirmationId = '%d'";
		$searchResult = $this->_listQuery( $sql, $cid );
		if ( count( $searchResult ) == 1 )
		{
			$sql = "UPDATE user SET confirmationId = '%d' WHERE confirmationId = '%d'";
			$isSuccess = $this->conn->_execute( $sql, 0, $cid );
			$result = $isSuccess ? 0 : 10; // success or server error.
		}
		
		return array( $result );
	}
	
	public function resetPassword( $newPassword, $username, $passwordHash )
	{
		$result = 1; // username not found.
		$sql = "SELECT * FROM user WHERE username = '%s' AND password = '%s'";
		$searchResult = $this->_listQuery( $sql, $username, $passwordHash );
		if ( count( $searchResult ) == 1 )
		{
			$sql = "UPDATE user SET password = '%s' WHERE username = '%s'";
			$isSuccess = $this->conn->_execute( $sql, password_hash( $newPassword, PASSWORD_DEFAULT ), $username );
			$result = $isSuccess ? 0 : 10; // success or server error.
		}
		
		return array( $result );
	}
	
	// TODO: Delete after password hashing is done!
	public function hashPasswords()
	{
		$sql = "SELECT * FROM user";
		$searchResult = $this->_listQuery( $sql );
		foreach ( $searchResult as $usuarioVO )
		{
			$sql = "UPDATE user SET password = '%s' WHERE username = '%s'";
			$this->conn->_execute( $sql, password_hash( $usuarioVO->password, PASSWORD_DEFAULT ), $usuarioVO->username );
		}
	}
	
	// private:

	private function _hideSensitiveData( $userVO )
	{
		$userVO->password = null;
		$userVO->email = null;
		$userVO->token = null;
	}

	private function _listQuery() 
	{
		$searchResult = array();
		$result = $this->conn->_execute( func_get_args() );
		$temp = new UserVO();	
	
		while ( $row = $this->conn->_nextRow( $result ) ) 
		{
			$temp = new UserVO();	
			
			$temp->username = $row[ 0 ];
			$temp->password = $row[ 1 ];
			$temp->email = $row[ 2 ];
			$temp->joinDate = $row[ 3 ];
			$temp->karma = $row[ 4 ];
			$temp->token = $row[ 5 ];
			$temp->confirmationId = $row[ 6 ];
			if ( $row[ 7 ] == null )
			{
				$temp->lastConnTimestamp = -1;
			}
			else
			{
				$temp->lastConnTimestamp = time() - strtotime( $row[ 7 ] );
			}
			$temp->isReceiveNotifications = $row[ 8 ];
			
			$searchResult[] = $temp;
		}

		return $searchResult;
	}
}
?>