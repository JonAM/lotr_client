<?php

class Datasource
{
   var $dbLink;

   function __construct($dbHost, $dbName, $dbuser, $dbpasswd)
   {
      $this->dbLink = mysqli_connect($dbHost, $dbuser, $dbpasswd);
      mysqli_select_db($this->dbLink, $dbName);
      mysqli_set_charset($this->dbLink, 'utf8');	
   }

   function _execute()
   {  
	  if ( is_array(func_get_arg(0)) && is_array(func_get_arg(0)[0]) )
	  {
		  return $this->_vexecute(func_get_arg(0)[0]);
	  }
      else if ( is_array(func_get_arg(0)) ) {
         return $this->_vexecute(func_get_arg(0)); // Recibe un array con parametros
      }else{
         return $this->_vexecute(func_get_args()); // Recibe parametros separados
      }
   }
   
   function _vexecute($params)
   {
      $query = array_shift($params);
      $length = count($params);
      for ( $i = 0; $i < $length; $i++ ){
         $params[$i] = mysqli_real_escape_string($this->dbLink, $params[$i]);
      }
      $query = vsprintf($query, $params);
 
      $result = mysqli_query($this->dbLink, $query);
      $this->_checkErrors($query);
      
      return $result;
   }

   function _executeBlind($sql)
   {
      $result = mysqli_query($this->dbLink, $sql);
      return $result;
   }

   function _nextRow ($result)
   {
      //Cuando la select no devuelve nada o en caso de error result ser� false.
      //En ese caso para que mysql_fetch_array no casque hacemos esto.
      if ($result){
     	 $row = mysqli_fetch_array($result);
    	 return $row;
      }else{
	 return false;
      }
   }

   function _affectedRows ()
   {
	return mysqli_affected_rows($this->dbLink);
   }

   function _checkErrors($sql)
   {
      $err=mysqli_error($this->dbLink);
      $errno=mysqli_errno($this->dbLink);

      if($errno)
      {
         $message = "The following SQL command ".$sql." caused Database error: ".$err.".";

         print "Unrecowerable error has occurred. All data will be logged.";
         print "Please contact System Administrator for help! \n";
         print "<!-- ".$message." -->\n";
         exit;
      }
      else
      {
         return;
      }
   }
}

?>
