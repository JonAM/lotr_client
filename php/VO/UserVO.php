<?php

class UserVO
{
	public $username;
	public $password;
	public $email;
	public $joinDate;
	public $karma;
	public $token;
	public $confirmationId;
	public $lastConnTimestamp;
	public $isReceiveNotifications;
	
	public $_explicitType = "UserVO";
}

?>