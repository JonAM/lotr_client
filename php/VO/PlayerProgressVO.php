<?php

class PlayerProgressVO
{
	public $playerOne;
	public $playerOneDeck;
	public $playerTwo;
	public $playerTwoDeck;
	public $dateTimestamp;
	public $scenarioId;
	public $score;

	public $whenTimestamp;
	
	public $_explicitType = "PlayerProgressVO";
}

?>