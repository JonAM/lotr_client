<?php

class CampaignLogVO
{
	public $title;
	public $dateTimestamp;
	public $username;
	public $playerHeroes;
	public $ally;
	public $allyHeroes;
	public $fallenHeroes;
	public $threatPenalty;
	public $notes;
	public $completedScenarios;
	public $boons;
	public $burdens;
	
	public $whenTimestamp;
	
	public $_explicitType = "CampaignLogVO";
}

?>