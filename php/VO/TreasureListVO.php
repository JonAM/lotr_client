<?php

class TreasureListVO
{
	public $title;
	public $dateTimestamp;
	public $username;
	public $playerHeroes;
	public $ally;
	public $allyHeroes;
	public $scenarioId;
	public $list;
	
	public $whenTimestamp;
	
	public $_explicitType = "TreasureListVO";
}

?>