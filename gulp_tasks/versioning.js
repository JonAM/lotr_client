module.exports = ( function() 
    {   
        // #region Attributes //

        let dataStore = new ( require( "data-store" ) )( { path: process.cwd() + "/version.json" } );

        // Emums.
        const segment_type = { X: 0, Y: 1, Z: 2 }; // version number's format is x.yy.zzz

        // #endregion //


        // #region Methods //

        // public:

        function incrVersionNumber( index, isReset = false )
        {
            let result = null;
        
            let arrVersionNumber = findVersionNumber();
            if ( arrVersionNumber.length == 3 )
            {
                if ( arrVersionNumber[ segment_type.X ] == 9 && arrVersionNumber[ segment_type.Y ] == 99 && arrVersionNumber[ segment_type.Z ] == 999 )
                {
                    console.log( "versioning.js :: incrVersionNumber() :: Max version number reached." )
                }
                else
                {
                    arrVersionNumber[ index ] += 1;
                    if ( arrVersionNumber[ segment_type.Z ] > 999 )
                    {
                        arrVersionNumber[ segment_type.Z ] = 0;
                        arrVersionNumber[ segment_type.Y ] += 1;
                    }
                    if ( arrVersionNumber[ segment_type.Y ] > 99 )
                    {
                        arrVersionNumber[ segment_type.Y ] = 0;
                        arrVersionNumber[ segment_type.X ] += 1;
                    }
                    if ( arrVersionNumber[ segment_type.X ] > 9 )
                    {
                        arrVersionNumber[ segment_type.X ] = 9;
                        console.log( "versioning.js :: incrVersionNumber() :: The first digit of the version number cannot be greater than 9." )
                    }
                    else
                    {
                        if ( isReset )
                        {
                            if ( index == segment_type.X )
                            {
                                arrVersionNumber[ segment_type.Y ] = 0;
                                arrVersionNumber[ segment_type.Z ] = 0;
                            }
                            else if ( index == segment_type.Y )
                            {
                                arrVersionNumber[ segment_type.Z ] = 0;
                            }
                        }
                    }
        
                    result = saveVersionNumber( arrVersionNumber );
                }
            }
            
            return result;
        } 

        // private:

        function findVersionNumber()
        {
            let result = [];
        
            if ( !dataStore.hasOwn( "version" ) )
            {
                dataStore.set( "version", "0.00.000" );
                result = [ 0, 0, 0 ];
            }
            else
            {
                let arrExec = ( /(\d)\.(\d\d)\.(\d\d\d)/ ).exec( dataStore.get( "version" ) );
                if ( arrExec.length == 4 )
                {
                    console.log( "versioning.js :: findVersionNumber() :: The current version number is " + arrExec[ 0 ] );
                    for ( let i = 1; i < arrExec.length; ++i )
                    {
                        result.push( parseInt( arrExec[ i ] ) );
                    }
                }
                else
                {
                    console.log( "versioning.js :: findVersionNumber() :: Bad version number format." );
                }
            }
        
            return result;
        }

        function saveVersionNumber( arrVersion )
        {
            const kVersionNumber = arrVersion[ 0 ] + "." + ( "0" + arrVersion[ 1 ].toString().slice( -2 ) ) + "." + ( "00" + arrVersion[ 2 ].toString() ).slice( -3 );
            console.log( "versioning.js :: saveVersionNumber() :: The new version number is " + kVersionNumber );
            dataStore.set( "version", kVersionNumber );
        
            return kVersionNumber;
        }

        // #endregion //

        return { 
            incrVersionNumber: incrVersionNumber,
            segment_type: segment_type };
    } )();