// #region Attributes //

// Requires.
const autoprefixer    = require( "autoprefixer" );
const browserify      = require( "browserify" );
const buffer          = require( "vinyl-buffer" );
const csso            = require( "gulp-csso" );
const del             = require( "del" );
const gulp            = require( "gulp" );
const gulpif          = require( "gulp-if" );
const htmlmin         = require( "gulp-htmlmin" );
const postcss         = require( "gulp-postcss" );
const replace         = require( "gulp-replace" );
const runsequence     = require( "run-sequence" );
const sass            = require( "gulp-sass" )(require('node-sass'));
const source          = require( "vinyl-source-stream" );
const tsify           = require( "tsify" );
const terser          = require( "gulp-terser" );
const versioning      = require('./gulp_tasks/versioning' );
const watchify        = require( "watchify" );

var Readable = require( 'stream' ).Readable;

// Constants
// Paths.
const kTsSrcEntryPath = "src/Main.ts";
const kTsOutDevPath = "bin";
const kTsOutReleasePath = "release";
const kScssOutMainDevPath = "bin/css";
const kScssOutResDevPath = "bin/resources/css";

// Enums.
const publish_type = {
    RELEASE: 0,
    DEBUG: 1,
    LOCALHOST: 2 };

// #endregion //


// #region Methods //

// Development build.
function buildDebug()
{
    // It compiles .ts files into .js and copies the bundled file to the bin folder.
    return browserify( {
            basedir: ".",
            debug: true,
            entries: [ kTsSrcEntryPath ],
            cache: {},
            packageCache: {} } )
        .plugin( tsify )
        .bundle()
        .pipe( source( "game.js" ) )
        .pipe( buffer() )
        .pipe( gulp.dest( kTsOutDevPath ) );
}

function buildElectronDebug()
{
    // It compiles .ts files into .js and copies the bundled file to the bin folder.
    return browserify( {
            basedir: ".",
            debug: true,
            entries: [ kTsSrcEntryPath ],
            cache: {},
            packageCache: {} } )
        .plugin( tsify )
        .bundle()
        .pipe( source( "game.js" ) )
        .pipe( replace( /(Config\.kIsDebugMode = )true;/, "$1false;" ) )
        .pipe( buffer() )
        .pipe( gulp.dest( kTsOutDevPath ) );
}

function watchDebug()
{
    let b = browserify( {
        basedir: ".",
        debug: true,
        entries: [ kTsSrcEntryPath ],
        cache: {},
        packageCache: {} } );
    b.plugin( watchify );
    b.plugin( tsify );
    b.on( "update", bundle );
    b.on( "log", onLog );
    b.on( "error", console.error );
    bundle();

    console.log( "Completing starting bundle, please wait..." );

    function bundle() 
    {
        b.bundle()
            .pipe( source( "game.js" ) )
            .pipe( buffer() )
            .pipe( gulp.dest( kTsOutDevPath ) );
    };

    function onLog( message )
    {
        console.log( message + " - " + new Date().toLocaleString() );
    };
}

// Release build.
function cleanRelease()
{
    const arrPath = [ 
        kTsOutReleasePath + "/*.js",
        kTsOutReleasePath + "/*.html",
        kTsOutReleasePath + "/*.php",
        kTsOutReleasePath + "/resources/**",
        kTsOutReleasePath + "/css/**",
        kTsOutReleasePath + "/i18n/**",
        kTsOutReleasePath + "/js/**" ];
    return del( arrPath, { force: true } );
}

function publish( publishType )
{
    const kVersionNumber = versioning.incrVersionNumber( 2 );

    // Copy html.
    gulp.src( [ kTsOutDevPath + "/index.html" ], { nocase: false } )
        //.pipe( replace( /(src="game\.js)"/g, "$1?lotr=" + kVersionNumber + '"' ) )
        //.pipe( replace( /(href="css\/global\.css)"/g, "$1?lotr=" + kVersionNumber + '"' ) )
        .pipe( htmlmin( { collapseWhitespace: true, removeComments: true } ) )
        .pipe( gulp.dest( kTsOutReleasePath ) );
    gulp.src( [ kTsOutDevPath + "/resources/html/**" ], { nocase: false } )
        .pipe( replace( /\<![ \r\n\t]*(--([^\-]|[\r\n]|-[^\-])*--[ \r\n\t]*)\>/g, "" ) ) // so we don't later request commented img elements.
        //.pipe( replace( /(\.(png|gif|jpg))"/g, "$1?lotr=" + kVersionNumber + '"' ) )
        .pipe( htmlmin( { collapseWhitespace: true, removeComments: true } ) )
        .pipe( gulp.dest( kTsOutReleasePath + "/resources/html" ) );

    // Copy css.
    gulp.src( [ kTsOutDevPath + "/css/**" ], { nocase: false } )
        //.pipe( replace( /(\.(png|gif|jpg))"/g, "$1?lotr=" + kVersionNumber + '"' ) )
        .pipe( csso() )
        .pipe( gulp.dest( kTsOutReleasePath + "/css" ) );
    gulp.src( [ kTsOutDevPath + "/resources/css/**" ], { nocase: false } )
        //.pipe( replace( /(\.(png|gif|jpg))"/g, "$1?lotr=" + kVersionNumber + '"' ) )
        .pipe( csso() )
        .pipe( gulp.dest( kTsOutReleasePath + "/resources/css" ) );

    // Copy resources (excluding html and css).
    const arrResourcesPath = [
        kTsOutDevPath + "/resources/**",
        "!" + kTsOutDevPath + "/resources/html{,/**}",
        "!" + kTsOutDevPath + "/resources/css{,/**}" ];
    gulp.src( arrResourcesPath, { nocase: false } )
        .pipe( gulp.dest( kTsOutReleasePath + "/resources" ) );

    // Copy i18n.
    gulp.src( [ kTsOutDevPath + "/i18n/**" ], { nocase: false } )
        .pipe( gulp.dest( kTsOutReleasePath + "/i18n" ) );

    // .htaccess
    gulp.src( [ kTsOutDevPath + "/.htaccess" ], { nocase: false } )
        .pipe( gulp.dest( kTsOutReleasePath ) );

    // JS libraries.
    gulp.src( [ kTsOutDevPath + "/js/**" ], { nocase: false } )
        //.pipe( terser() )
        .pipe( gulp.dest( kTsOutReleasePath + "/js" ) );

    // It compiles .ts files into .js, bundles everything together, minifies it and copies it to the release folder.
    return browserify( {
        basedir: ".",
        debug: false,
        entries: [ kTsSrcEntryPath ],
        cache: {},
        packageCache: {} } )
        .plugin( tsify )
        .bundle( function( err, src) {
            if ( err )
            {
                console.log( err );
            }
            else
            {
                let r = new Readable();
                r.push( src );    
                r.push( null ); // end of file.
                r.pipe( source( "game.js" ) )
                    .pipe( replace( /(Config\.kBuildVersion = )"\d\.\d{2}\.\d{3}";/, '$1"' + kVersionNumber + '";' ) )
                    .pipe( gulpif( publishType == publish_type.LOCALHOST, replace( /(http:\/\/localhost):700/, "$1:701" ) ) ) 
                    .pipe( gulpif( publishType != publish_type.LOCALHOST, replace( /(Config\.kIsDebugMode = )true;/, "$1false;" ) ) )
                    .pipe( gulpif( publishType == publish_type.RELEASE, replace( /(Config\.kIsLogMode = )true;/, "$1false;" ) ) )  
                    .pipe( buffer() )
                    .pipe( gulpif( publishType == publish_type.RELEASE, terser() ) ) // TODO: Comment ???
                    .pipe( gulp.dest( kTsOutReleasePath ) );
                }
        } );
}

// SASS build.
function buildSass() 
{
    gulp.src( [ "scss/**", "!scss/index{,/**}" ], { nocase: false } )
        .pipe( sass() )
        .pipe( postcss( [ autoprefixer() ] ) )
        .pipe( gulp.dest( kScssOutResDevPath ) );

    return gulp.src( [ "scss/index/*", "!scss/index/global{,/**}" ], { nocase: false } )
        .pipe( sass() )
        .pipe( postcss( [ autoprefixer() ] ) )
        .pipe( gulp.dest( kScssOutMainDevPath ) );
}

// #endregion //


// #region Tasks //

gulp.task( "build_sass", buildSass );

// Debug.
// Game.
gulp.task( "build_debug", buildDebug );
gulp.task( "build_electron_debug", buildElectronDebug );
gulp.task( "watch_debug", watchDebug );
// Release.
gulp.task( "publish_release", () => { runsequence( "clean_release", "build_release" ); } );
gulp.task( "publish_debug", () => { runsequence( "clean_release", "build_release_no_uglify" ); } );
gulp.task( "publish_localhost", () => { runsequence( "clean_release", "build_release_for_localhost" ); } );
gulp.task( "clean_release", cleanRelease );
gulp.task( "build_release", publish.bind( this, publish_type.RELEASE ) );
gulp.task( "build_release_no_uglify", publish.bind( this, publish_type.DEBUG ) );
gulp.task( "build_release_for_localhost", publish.bind( this, publish_type.LOCALHOST ) );

// Versioning.
gulp.task( "incr_version_number_x", versioning.incrVersionNumber.bind( this, versioning.segment_type.X, true ) );
gulp.task( "incr_version_number_y", versioning.incrVersionNumber.bind( this, versioning.segment_type.Y, true ) );

// #endregion //