﻿/*
 * Created by SharpDevelop.
 * User: usuario
 * Date: 16/10/2019
 * Time: 10:37
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Json;

namespace DbGenerator
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
		}
		
		void SelectFileBtnClick(object sender, EventArgs e)
		{
			DialogResult result = this.folderBrowserDialog1.ShowDialog();

		    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(this.folderBrowserDialog1.SelectedPath))
		    {
		        this.selectedFileLbl.Text = this.folderBrowserDialog1.SelectedPath;
			        	
				this.parseBtn.Enabled = true;
		    }
		}
		
		void ParseBtnClick(object sender, EventArgs e)
		{
			this.parse();
		}
		
		void parse()
		{
			string result = "[ ";
			
			string[] directories = Directory.GetDirectories(this.selectedFileLbl.Text);
			for ( int i = 0; i < directories.Length; ++i )
			{
				string kDirectory = directories[ i ];
				int kFileCount = Directory.GetFiles( kDirectory ).Length;
				result += "[ \"" + kDirectory.Substring( kDirectory.Length - 3 ) + "\", " + ( kFileCount * 0.5 ).ToString() + " ]";
				
				if ( i < directories.Length - 1 )
				{
					result += ", ";
					if ( ( i + 1 ) % 5 == 0 )
					{
						result += "\n";
					}
				}
			}
			
			result += " ]";
			
			this.resultTextBox.Text = result;
		}
	}
}
