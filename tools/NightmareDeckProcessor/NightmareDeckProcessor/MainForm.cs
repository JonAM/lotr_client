﻿/*
 * Created by SharpDevelop.
 * User: usuario
 * Date: 16/10/2019
 * Time: 10:37
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Json;

namespace DbGenerator
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
		}
		
		void SelectFileBtnClick(object sender, EventArgs e)
		{
			this.openFileDialog1.ShowDialog();
		}
		
		void ParseBtnClick(object sender, EventArgs e)
		{
			this.parse();
		}
		
		void OpenFileDialog1FileOk(object sender, System.ComponentModel.CancelEventArgs e)
		{
			this.selectedFileLbl.Text = openFileDialog1.FileName;
			        	
			this.parseBtn.Enabled = true;
		}
		
		void parse()
		{
			string result = "[ ";
			
			string dbText = File.ReadAllText( openFileDialog1.FileName );
		
			string removedCardsRaw = Regex.Match( this.sourceTextBox.Text, @":(\\n|\\r)\\n(.+)(\\n|\\r)\\n(Then|Add|Shuffle|During)", RegexOptions.None ).Groups[ 2 ].Value;
			removedCardsRaw = removedCardsRaw.Replace( "\\r", "" );
			string[] removedCardEntries = Regex.Split( removedCardsRaw, @"\\n" );
			for ( int i = 0; i < removedCardEntries.Length; ++i )
			{
				string kCardEntry = removedCardEntries[ i ].Replace( "~", "" );
				int kCount = int.Parse( Regex.Match( kCardEntry, @"(\d)x ", RegexOptions.None ).Groups[ 1 ].Value );
				string kName = Regex.Match( kCardEntry, @"\dx (.+)", RegexOptions.None ).Groups[ 1 ].Value;
				
				string kCode = Regex.Match( dbText, @"""(\d+)"",\r\n\s\s\s\s""name"":\s""" + kName + @"""", RegexOptions.None ).Groups[ 1 ].Value;
				for ( int j = 0; j < kCount; ++j )
				{
					result += "\"" + ( kCode != "" ? kCode : kName ) + "\", ";
				}
			}
			
			result = result.Remove( result.Length - 2 );
			
			result += " ]";
			
			this.resultTextBox.Text = result;
		}
		void MainFormLoad(object sender, EventArgs e)
		{
	
		}
		void ResultTextBoxTextChanged(object sender, EventArgs e)
		{
	
		}
		void TextBox1TextChanged(object sender, EventArgs e)
		{
	
		}
	}
}
