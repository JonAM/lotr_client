﻿/*
 * Created by SharpDevelop.
 * User: usuario
 * Date: 16/10/2019
 * Time: 10:37
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace DbGenerator
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Button parseBtn;
		private System.Windows.Forms.Button selectFileBtn;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.TextBox resultTextBox;
		private System.Windows.Forms.Label selectedFileLbl;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
		private System.Windows.Forms.Button selectTextureMapDirBtn;
		private System.Windows.Forms.Label selectedDirLbl;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.parseBtn = new System.Windows.Forms.Button();
			this.selectFileBtn = new System.Windows.Forms.Button();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.resultTextBox = new System.Windows.Forms.TextBox();
			this.selectedFileLbl = new System.Windows.Forms.Label();
			this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
			this.selectTextureMapDirBtn = new System.Windows.Forms.Button();
			this.selectedDirLbl = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// parseBtn
			// 
			this.parseBtn.Location = new System.Drawing.Point(173, 493);
			this.parseBtn.Margin = new System.Windows.Forms.Padding(2);
			this.parseBtn.Name = "parseBtn";
			this.parseBtn.Size = new System.Drawing.Size(205, 64);
			this.parseBtn.TabIndex = 0;
			this.parseBtn.Text = "Parse";
			this.parseBtn.UseVisualStyleBackColor = true;
			this.parseBtn.Click += new System.EventHandler(this.ParseBtnClick);
			// 
			// selectFileBtn
			// 
			this.selectFileBtn.Location = new System.Drawing.Point(46, 32);
			this.selectFileBtn.Margin = new System.Windows.Forms.Padding(2);
			this.selectFileBtn.Name = "selectFileBtn";
			this.selectFileBtn.Size = new System.Drawing.Size(205, 64);
			this.selectFileBtn.TabIndex = 1;
			this.selectFileBtn.Text = "Select raw DB file";
			this.selectFileBtn.UseVisualStyleBackColor = true;
			this.selectFileBtn.Click += new System.EventHandler(this.SelectFileBtnClick);
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "openFileDialog1";
			this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.OpenFileDialog1FileOk);
			// 
			// resultTextBox
			// 
			this.resultTextBox.Location = new System.Drawing.Point(46, 220);
			this.resultTextBox.Margin = new System.Windows.Forms.Padding(2);
			this.resultTextBox.Multiline = true;
			this.resultTextBox.Name = "resultTextBox";
			this.resultTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.resultTextBox.Size = new System.Drawing.Size(457, 231);
			this.resultTextBox.TabIndex = 2;
			// 
			// selectedFileLbl
			// 
			this.selectedFileLbl.Location = new System.Drawing.Point(290, 52);
			this.selectedFileLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.selectedFileLbl.Name = "selectedFileLbl";
			this.selectedFileLbl.Size = new System.Drawing.Size(212, 19);
			this.selectedFileLbl.TabIndex = 3;
			this.selectedFileLbl.Text = "No file selected";
			// 
			// folderBrowserDialog1
			// 
			this.folderBrowserDialog1.ShowNewFolderButton = false;
			this.folderBrowserDialog1.HelpRequest += new System.EventHandler(this.FolderBrowserDialog1HelpRequest);
			// 
			// selectTextureMapDirBtn
			// 
			this.selectTextureMapDirBtn.Location = new System.Drawing.Point(46, 126);
			this.selectTextureMapDirBtn.Name = "selectTextureMapDirBtn";
			this.selectTextureMapDirBtn.Size = new System.Drawing.Size(205, 66);
			this.selectTextureMapDirBtn.TabIndex = 4;
			this.selectTextureMapDirBtn.Text = "Select texture map folder";
			this.selectTextureMapDirBtn.UseVisualStyleBackColor = true;
			this.selectTextureMapDirBtn.Click += new System.EventHandler(this.SelectTextureMapDirBtnClick);
			// 
			// selectedDirLbl
			// 
			this.selectedDirLbl.Location = new System.Drawing.Point(290, 149);
			this.selectedDirLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.selectedDirLbl.Name = "selectedDirLbl";
			this.selectedDirLbl.Size = new System.Drawing.Size(212, 19);
			this.selectedDirLbl.TabIndex = 5;
			this.selectedDirLbl.Text = "No folder selected";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.ClientSize = new System.Drawing.Size(557, 591);
			this.Controls.Add(this.selectedDirLbl);
			this.Controls.Add(this.selectTextureMapDirBtn);
			this.Controls.Add(this.selectedFileLbl);
			this.Controls.Add(this.resultTextBox);
			this.Controls.Add(this.selectFileBtn);
			this.Controls.Add(this.parseBtn);
			this.Margin = new System.Windows.Forms.Padding(2);
			this.Name = "MainForm";
			this.Text = "L5rDbGenerator";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
