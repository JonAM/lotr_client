﻿/*
 * Created by SharpDevelop.
 * User: usuario
 * Date: 16/10/2019
 * Time: 10:37
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Json;

namespace DbGenerator
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		private bool _isDbRawFileSelected = false;
		private bool _isTextureMapDirSelected = false;
		
		private SortedDictionary<string, string> _mapCardIdToTextureMapId = new SortedDictionary<string, string>();
		
    	private SortedDictionary<string, string> _mapBoonSide = new SortedDictionary<string, string>()
    		{
	            { "141077", "999003" }, { "145067B", "999003" }, { "146041", "999003" }, { "706021", "999003" }, { "704024", "999003" }, 
	            
	            { "141017", "999002" }, { "141018", "999002" }, { "141019", "999002" }, { "141020", "999002" }, { "141021", "999002" },
	            { "142011", "999002" }, { "142012", "999002" }, { "142013", "999002" }, { "142014", "999002" }, { "142015", "999002" },
	            { "142016", "999002" }, { "142017", "999002" }, { "142018", "999002" }, { "143015", "999002" }, { "143016", "999002" },
	            { "143017", "999002" }, { "143018", "999002" }, { "143019", "999002" }, { "143020", "999002" }, { "144014", "999002" },
	            { "145014", "999002" }, { "145015", "999002" }, { "902038", "999002" },	{ "902039", "999002" }, { "902040", "999002" }, 
	            { "902041", "999002" }
    	};
    	
    	private SortedDictionary<string, string> _mapBurdenSide = new SortedDictionary<string, string>()
    		{
	            { "141078", "999003" }, { "141079", "999003" }, { "141080", "999003" }, { "141081", "999003" }, { "141082", "999003" }, 
	            { "141083", "999003" }, { "142089", "999003" }, { "142090", "999003" }, { "142085", "999003" }, { "142086", "999003" }, 
	            { "142087", "999003" }, { "142088", "999003" }, { "142084", "999003" }, { "143068", "999003" }, { "144052", "999003" }, 
	            { "145062", "999003" }, { "145016", "999003" },
	            
	            { "143021", "999002" }, { "144015", "999002" },
	            
	            { "801042", "999003" }
    		};
        	
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
		}
		
		void SelectFileBtnClick(object sender, EventArgs e)
		{
			this.openFileDialog1.ShowDialog();
		}
		
		void SelectTextureMapDirBtnClick(object sender, EventArgs e)
		{
			DialogResult result = folderBrowserDialog1.ShowDialog();
	        if( result == DialogResult.OK )
	        {
	            this.selectedDirLbl.Text = folderBrowserDialog1.SelectedPath;
	            
	            this._isTextureMapDirSelected = true;
				this.parseBtn.Enabled = this._isDbRawFileSelected;
	        }
		}
		
		void ParseBtnClick(object sender, EventArgs e)
		{
			this.parse();
		}
		
		void OpenFileDialog1FileOk(object sender, System.ComponentModel.CancelEventArgs e)
		{
			this.selectedFileLbl.Text = openFileDialog1.FileName;
			       			
			this._isDbRawFileSelected = true;
			this.parseBtn.Enabled = this._isTextureMapDirSelected;
		}
		
		void parse()
		{
			this._mapCardIdToTextureMapId = this.createCardIdToTextureMapIdMap();
			
			string result = "{";
			
			string text = File.ReadAllText( openFileDialog1.FileName );
			
			// DB erratas.
			// Duplicated codes.
			var replace = new Regex( @"""code"": ""04076""" ); 
        	text = replace.Replace( text, @"""code"": ""04154""", 1 );
        	//
        	replace = new Regex( @"""code"": ""22073""" );
        	text = replace.Replace( text, @"""code"": ""22165""", 1 );
        	// Forced: During Setup -> Setup: During Setup (for setup instructions detection)
        	replace = new Regex( @"<b>Forced:</b> During setup" );
        	text = replace.Replace( text, @"<b>Setup:</b> During setup" );
        	
        	int customTextureMapId = 700;
        	string lastCustomPackCode = null;
        	
        	var mapTreasureSet = new SortedDictionary<string, string>()
        		{
		            { "131022", "We Must Away, Ere Break of Day" }, 
		            { "131021", "We Must Away, Ere Break of Day" }, 
		            { "131020", "We Must Away, Ere Break of Day" }, 
		            { "132019", "Flies and Spiders" }, 
		            { "132020", "The Lonely Mountain" }, 
		            { "132021", "The Lonely Mountain" }, 
		            { "132022", "The Lonely Mountain" }, 
		            { "132023", "The Lonely Mountain" }, 
		            { "132024", "The Lonely Mountain" }
		        };
        	
        	var mapDoubleSidedCardId = new SortedDictionary<string, string>()
		        {
		            { "009071", "009071A" }, { "009072", "009071B" },
		            
		            { "010127", "010127A" }, { "010128", "010127B" },
		            { "010109", "010109A" }, { "010110", "010109B" },
		            { "010111", "010111A" }, { "010112", "010111B" },
		            { "010113", "010113A" }, { "010114", "010113B" },
		            { "010011", "010011A" }, { "010012", "010011B" },
		            
		            { "011076", "011076A" }, { "011077", "011076B" },
		            { "011078", "011077A" }, { "011079", "011077B" },
		            
		            { "144075", "144075A" }, { "144076", "144075B" },	    
		            { "145067", "145067A" }, { "145068", "145067B" },
		            
		            { "022161", "022161A" }, 
		            { "022162", "022162A" }, 
		            { "022163", "022163A" }, 
		            { "022164", "022164A" }, 
		            
		            { "023006", "023006A" }, 
		            { "023007", "023007A" }, 
		            { "023008", "023008A" },
		            { "023009", "023009A" }, 
		            { "023010", "023010A" },
		            { "023011", "023011A" }, 
		            { "023013", "023013A" },
		            
		            { "017079", "017079A" }, { "017080", "017079B" },
		            
		            { "019052", "019052A" }, { "019053", "019052B" },
		            
		            { "021072", "021072A" }, { "021073", "021072B" },
		            
		            { "012094", "012094B" }, { "012095", "012094A" },
		            { "012098", "012098B" },
		            { "012068", "012068A" },
		            { "012069", "012069A" },
		            { "012072", "012072A" },
		            { "012073", "012073A" },
		            { "012074", "012074A" },
		     
		            { "011027", "011027A" },
		            { "011030", "011030A" },
		            { "011031", "011031A" },
		            { "011072", "011072A" },
		            { "011073", "011073A" },
		            { "011074", "011074A" },
		            
		            { "018069", "018069A" }, { "018070", "018069B" },
		            { "019143", "019143A" }, { "019144", "019143B" },	 
		            
		            { "145071", "145071A" }, { "145072", "145071B" },
		            { "146070", "146070A" }, { "146071", "146070B" },
		            
		            { "710010", "710010A" }, { "710011", "710010B" },
		            { "711010", "711010A" }, { "711011", "711010B" },
					{ "712010", "712010A" }, { "712011", "712010B" },
					{ "713010", "713010A" }, { "713011", "713010B" },
		            
		            { "760002", "760002A" },
		            { "760003", "760003A" },
		            { "760004", "760004A" },
		            { "760005", "760005A" },
		            { "760006", "760006A" },
		            { "760007", "760007A" },
		            { "760008", "760008A" },
		            
		            { "764002", "764002A" },
		            { "764003", "764003A" },
		            { "764004", "764004A" }
		        };

			string[] lines = Regex.Split( text, "\n  {\n" );
			for ( int i = 1; i < lines.Length; ++i )
			{
				string cardEntry = lines[ i ].Substring( 0, Regex.Match( lines[ i ], "\n  }" ).Index );
				if ( ( !Regex.IsMatch( cardEntry, @"""type_code"": ""gencon-setup"",\n", RegexOptions.None ) 
				    	|| Regex.IsMatch( cardEntry, @"""text"": ""<b>Side A</b> <b>Setup:</b>", RegexOptions.None ) ) // Gencon setup cards already included in scenarios rules pdf.
				    && !Regex.IsMatch( cardEntry, @"""name"": ""Navigation""", RegexOptions.None ) // Heading card.
				    && !( Regex.IsMatch( cardEntry, @"""code"": ""00002""", RegexOptions.None ) && Regex.IsMatch( cardEntry, @"""name"": ""Gandalf""", RegexOptions.None ) ) // Duplicate from gen-con.
				    && !( Regex.IsMatch( cardEntry, @"""code"": ""00002""", RegexOptions.None ) && Regex.IsMatch( cardEntry, @"""name"": ""Sam Gamgee""", RegexOptions.None ) ) // Duplicate from gen-con.
				    && !( Regex.IsMatch( cardEntry, @"""code"": ""00005""", RegexOptions.None ) && Regex.IsMatch( cardEntry, @"""name"": ""Legolas""", RegexOptions.None ) ) ) // Duplicate from gen-con.			   
				{
					// DB erratas.
					if ( Regex.IsMatch( cardEntry, @"""pack_name"": ""Into Fangorn Nightmare"",\n", RegexOptions.None ) )
					{
						// Into Fangorn Nightmare.
						cardEntry = Regex.Replace( cardEntry, @"""pack_code"": ""TCaON""", @"""pack_code"": ""IFN""" );
					}
					else if ( Regex.IsMatch( cardEntry, @"""pack_name"": ""The Battle of Five Armies Nightmare"",\n", RegexOptions.None ) )
					{
						// The Battle of Five Armies Nightmare.
						cardEntry = Regex.Replace( cardEntry, @"""pack_code"": ""FaSN""", @"""pack_code"": ""TBoFAN""" );
					} 
					else if ( Regex.IsMatch( cardEntry, @"""pack_code"": ""THftD""[^\d]+""position"": 47", RegexOptions.None ) )
					{
						// The Hunt for the Dreadnaught.
						cardEntry = Regex.Replace( cardEntry, @"""code"": ""23001""", @"""code"": ""23047""" );
					} 
					else if ( Regex.IsMatch( cardEntry, @"""pack_code"": ""AoDG""[^\d]+""position"": (31|32)", RegexOptions.None ) )
					{
						// Attack on Dol Guldur.
						cardEntry = Regex.Replace( cardEntry, @"""type_code"": ""campaign""", @"""type_code"": ""gencon-setup""" );
						cardEntry = Regex.Replace( cardEntry, @"""type_name"": ""Campaign""", @"""type_name"": ""GenCon Setup""" );
					} 
					else if ( Regex.IsMatch( cardEntry, @"""code"": ""718011"",\n", RegexOptions.None ) )
					{
						// Grimbeorn's Folly quest with quantity 3.
						cardEntry = Regex.Replace( cardEntry, @"""quantity"": 3", @"""quantity"": 1" );
						cardEntry = Regex.Replace( cardEntry, @"""deck_limit"": 3", @"""deck_limit"": 1" );
					}
					else if ( Regex.IsMatch( cardEntry, @"""code"": ""020100"",\n", RegexOptions.None ) )
					{
						// The Caves of Nibin-Dûm.
						cardEntry = Regex.Replace( cardEntry, @"""quest_points"": 0", @"""quest_points"": 255" );
					} 
					else if ( Regex.IsMatch( cardEntry, @"""code"": ""801116"",\n", RegexOptions.None ) )
					{
						// The Betrayal of Mîm quest with "when revealed" instead of "setup".
						cardEntry = Regex.Replace( cardEntry, @"<b>When Revealed:</b>", @"<b>Setup:</b>" );
					}
					else if ( Regex.IsMatch( cardEntry, @"""code"": ""005025"",\n", RegexOptions.None ) )
					{
						// Into Ithilien: Celador.
						cardEntry = Regex.Replace( cardEntry, @"""type_code"": ""objective"",\n", @"""type_code"": ""objective-ally"",\n""willpower"": 2,\n""attack"": 2,\n""defense"": 2,\n""health"": 3,\n" );
					}
					else if ( Regex.IsMatch( cardEntry, @"""code"": ""005026"",\n", RegexOptions.None ) )
					{
						// Into Ithilien: Ithilien Guardian.
						cardEntry = Regex.Replace( cardEntry, @"""type_code"": ""objective"",\n", @"""type_code"": ""objective-ally"",\n""willpower"": 2,\n""attack"": 2,\n""defense"": 1,\n""health"": 2,\n" );
					}
					
					// Custom scenario kits.
					if ( Regex.IsMatch( cardEntry, @"""pack_code"": ""(EfKD|TMoM|TWR|TWQ)"",.+""encounter_set"": """",", RegexOptions.None ) )
					{
						string packName = Regex.Match( cardEntry, @"""pack_name"": ""([^""]+)"",\n", RegexOptions.None ).Groups[ 1 ].Value;
						cardEntry = Regex.Replace( cardEntry, @"""encounter_set"": """"", @"""encounter_set"": """ + packName + @"""" );
					}
					
					// Pack code conflicts.
					if ( Regex.IsMatch( cardEntry, @"""pack_name"": ""The Dread Realm Nightmare"",\n", RegexOptions.None ) )
					{
						// The Drowned Ruins Nightmare and The Dread Realm Nightmare.
						cardEntry = Regex.Replace( cardEntry, @"""pack_code"": ""TDRN""", @"""pack_code"": ""TDrRN""" );
					}
					
					string code = Regex.Match( cardEntry, @"""code"": ""([^""]+)"",\n", RegexOptions.None ).Groups[ 1 ].Value;
					
					// Custom codes.
					if ( Regex.IsMatch( cardEntry, @"""pack_code"": ""(TMaO|TBoLT|TSoE|TOF|TRoB|FotBD|MatPP|TSoA|AoDG|TWQ|TWR|TMoM|EfKD)"",\n", RegexOptions.None ) 
					   || Regex.IsMatch( cardEntry, @"""pack_name"": ""[^""]+\sNightmare"",\n", RegexOptions.None ) )
					{ 
						// Gen-con and Nightmare.
						string packCode = Regex.Match( cardEntry, @"""pack_code"": ""([^""]+)"",\n", RegexOptions.None ).Groups[ 1 ].Value;
						if ( lastCustomPackCode != packCode )
						{
							lastCustomPackCode = packCode;
							customTextureMapId += 1;
						}
						
						code = customTextureMapId.ToString() + code.Substring( code.Length - 3 );
					}
					else if ( code.Length == 5 )
					{
						// Add "0" to code if its length is 5.
						code = "0" + code;
					}
					
					// Double-sided.
					bool isAddEntryForSideB = false;
					if ( mapDoubleSidedCardId.ContainsKey( code ) )
					{
						string doubleSidedCode = null;
						mapDoubleSidedCardId.TryGetValue( code, out doubleSidedCode );
						mapDoubleSidedCardId.Remove( code );
						code = doubleSidedCode;
					}
					else if ( Regex.IsMatch( cardEntry, @"""pack_code"": ""THftD"",\n", RegexOptions.None )
						&&	( Regex.IsMatch( cardEntry, @"""traits"": ""[^""]+Upgraded."",\n", RegexOptions.None )
					    	|| Regex.IsMatch( cardEntry, @"""traits"": ""[^""]+Damaged."",\n", RegexOptions.None ) ) )
					{
						// The Hunt for the Dreadnaught. Upgraded and damaged.
						code += "B";
					}
					else if ( ( Regex.IsMatch( cardEntry, @"""type_code"": ""contract""", RegexOptions.None )
					        	|| Regex.IsMatch( cardEntry, @"""type_code"": ""encounter-side-quest""", RegexOptions.None ) )
					        		&& Regex.IsMatch( cardEntry, @"""text"": ""<b>Side A<\/b>", RegexOptions.None )
					        || Regex.IsMatch( cardEntry, @"""type_code"": ""quest""", RegexOptions.None )
					    	|| Regex.IsMatch( cardEntry, @"""type_code"": ""nightmare-setup""", RegexOptions.None )	
					    	|| Regex.IsMatch( cardEntry, @"""type_code"": ""gencon-setup""", RegexOptions.None )			                        
							|| Regex.IsMatch( cardEntry, @"""type_code"": ""campaign""", RegexOptions.None ) )
					{
						// Contracts, campaigns, nightmare-setups, gencon-setups and quests (side A).
						code += "A";
						
						isAddEntryForSideB = true;
						
						if ( Regex.IsMatch( cardEntry, @"""type_code"": ""quest""", RegexOptions.None ) )
						{
							cardEntry = Regex.Replace( cardEntry, @"""type_code"": ""[^""]+""", @"""type_code"": ""quest-intro""" );
							cardEntry = Regex.Replace( cardEntry, @"""type_name"": ""[^""]+""", @"""type_name"": ""Quest Intro""" );
						}
					}
					
					cardEntry = Regex.Replace( cardEntry, @"""code"": ""(\d+)""", @"""code"": """ + code + @"""" );
					
					// Promotional.
					if ( Regex.IsMatch( cardEntry, @"""pack_code"": ""[^""]+-PRM"",\n", RegexOptions.None ) )
					{
						code += "-" + Regex.Match( cardEntry, @"""pack_code"": ""([^""]+)"",\n", RegexOptions.None ).Groups[ 1 ].Value;
					}
					
					string kSide = this.findCardSide( cardEntry, code );
					cardEntry += ",\n    \"side\": \"" + kSide + "\"";
					
					// Treasure cards.
					string treasureSet = null;
					if ( mapTreasureSet.TryGetValue( code, out treasureSet ) )
					{
						cardEntry += ",\n    \"treasure_set\": \"" + treasureSet + "\"";
					}
					
					// Boon and burden cards.
					if ( this._mapBoonSide.ContainsKey( code ) ) 
					{
						cardEntry += ",\n    \"is_boon\": \"true\"";
					}
					else if ( this._mapBurdenSide.ContainsKey( code ) ) 
					{
						cardEntry += ",\n    \"is_burden\": \"true\"";
					}
					
					string textureMapId = "";
					this._mapCardIdToTextureMapId.TryGetValue( code, out textureMapId );
					cardEntry += ",\n    \"texture_map_id\": \"" + textureMapId + "\"";
					
					result += "\n\t\"" + code + "\":\n"
						+ "\t\t{\n" + cardEntry + "\n\t\t}";
					
					if ( isAddEntryForSideB )
					{
						string entryForSideB = Regex.Replace( cardEntry, @"""code"": ""(\d+)A""", @"""code"": """ + code.Substring( 0, code.Length - 1 ) + @"B""" );
						entryForSideB = Regex.Replace( entryForSideB, @"""side"": ""(\d+)B""", @"""side"": """ + code.Substring( 0, code.Length - 1 ) + @"A""" );
						if ( Regex.IsMatch( cardEntry, @"""type_code"": ""quest-intro""", RegexOptions.None ) )
						{
							entryForSideB = Regex.Replace( entryForSideB, @"""type_code"": ""[^""]+""", @"""type_code"": ""quest""" );
							entryForSideB = Regex.Replace( entryForSideB, @"""type_name"": ""[^""]+""", @"""type_name"": ""Quest""" );
						}
						this._mapCardIdToTextureMapId.TryGetValue( code.Substring( 0, code.Length - 1 ) + "B", out textureMapId );
						entryForSideB = Regex.Replace( entryForSideB, @"""texture_map_id"": ""[^""]+""", @"""texture_map_id"": """ + textureMapId + @"""" );
						
						result += ",\n\t\"" + code.Substring( 0, code.Length - 1 ) + "B\":\n" + "\t\t{\n" + entryForSideB + "\n\t\t}";
					}
				
					if ( i < lines.Length - 1 )
					{
						result += ",";
					}
					
					// Extra quest card for Fog on the Barrow-downs.
					if ( code == "706004A" )
					{
						string entryForSideA = Regex.Replace( cardEntry, @"""code"": ""(\d+)A""", @"""code"": ""706022A""" );
						entryForSideA = Regex.Replace( entryForSideA, @"""side"": ""(\d+)B""", @"""side"": ""706022B""" );
						this._mapCardIdToTextureMapId.TryGetValue( "706022A", out textureMapId );
						entryForSideA = Regex.Replace( entryForSideA, @"""texture_map_id"": ""[^""]+""", @"""texture_map_id"": """ + textureMapId + @"""" );
						
						string entryForSideB = Regex.Replace( cardEntry, @"""code"": ""(\d+)A""", @"""code"": ""706022B""" );
						entryForSideB = Regex.Replace( entryForSideB, @"""side"": ""(\d+)B""", @"""side"": ""706022A""" );
						this._mapCardIdToTextureMapId.TryGetValue( "706022B", out textureMapId );
						entryForSideB = Regex.Replace( entryForSideB, @"""texture_map_id"": ""[^""]+""", @"""texture_map_id"": """ + textureMapId + @"""" );
						entryForSideB = Regex.Replace( entryForSideB, @"""type_code"": ""[^""]+""", @"""type_code"": ""quest""" );
						entryForSideB = Regex.Replace( entryForSideB, @"""type_name"": ""[^""]+""", @"""type_name"": ""Quest""" );
						
						result += "\n\t\"706022A\":\n"
							+ "\t\t{\n" + entryForSideA + "\n\t\t}," 
							+ "\n\t\"706022B\":\n"
							+ "\t\t{\n" + entryForSideB + "\n\t\t},";
					}
				}
			}
			
			result += "\n}";
		
        	this.addDoubleSidedCardEntries( ref result );
        	this.addCustomCardEntries( ref result );
			
			this.resultTextBox.Text = result;
		}
		
		SortedDictionary<string, string> createCardIdToTextureMapIdMap()
		{
			SortedDictionary<string, string> result = new SortedDictionary<string, string>();
			
			string[] directories = Directory.GetDirectories( this.selectedDirLbl.Text );
			for ( int i = 0; i < directories.Length; ++i )
			{
				string[] files = Directory.GetFiles( directories[ i ] );
				for ( int j = 0; j < files.Length; ++j )
				{
					string curFilePath = files[ j ];
					if ( curFilePath.IndexOf( ".json" ) != -1 )
					{
						string text = File.ReadAllText( curFilePath );
						string textureMapId = Regex.Match( text, @"""image"": ""([^\.]+)", RegexOptions.None ).Groups[ 1 ].Value;
						MatchCollection matches = Regex.Matches( text, @"    ""([^\.]+)\.jpg" );
						foreach ( Match match in matches )
						{
							result.Add( match.Groups[ 1 ].Value, textureMapId );
						}
					}
				}
			}
			
			return result;
		}
		
		void addDoubleSidedCardEntries( ref string dbText )
		{
			// Grotto / Underwater.
			// Save and remove existing grotto entries.
			string entry012099 = Regex.Match( dbText, @"(\t""012099""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			dbText = Regex.Replace( dbText, @"(\t""012099""[^\}]+\},\n)", "" );
			string entry012100 = Regex.Match( dbText, @"(\t""012100""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			dbText = Regex.Replace( dbText, @"(\t""012100""[^\}]+\},\n)", "" );
			string entry012101 = Regex.Match( dbText, @"(\t""012101""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			dbText = Regex.Replace( dbText, @"(\t""012101""[^\}]+\},\n)", "" );
			string entry012102 = Regex.Match( dbText, @"(\t""012102""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			dbText = Regex.Replace( dbText, @"(\t""012102""[^\}]+\},\n)", "" );
			string entry012103 = Regex.Match( dbText, @"(\t""012103""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			dbText = Regex.Replace( dbText, @"(\t""012103""[^\}]+\},\n)", "" );
			// Crear entrada 012098A con info de 012101 y cantidad 1.
			CardEntry cardEntryAux = new CardEntry( "012098A" );
			cardEntryAux.quantity = 1;
			this.addCardEntry( cardEntryAux, entry012101, ref dbText );
			// Crear entrada 012099B con info de 012099 y cantidad 1.
			// Crear entrada 012099A con info de 012103 y cantidad 1.
			cardEntryAux.code = "012099B";
			this.addCardEntry( cardEntryAux, entry012099, ref dbText );
			cardEntryAux.code = "012099A";
			this.addCardEntry( cardEntryAux, entry012103, ref dbText );
			// Crear entrada 012100B con info de 012100 y cantidad 1.
			// Crear entrada 012100A con info de 012101 y cantidad 1.
			cardEntryAux.code = "012100B";
			this.addCardEntry( cardEntryAux, entry012100, ref dbText );
			cardEntryAux.code = "012100A";
			this.addCardEntry( cardEntryAux, entry012101, ref dbText );
			// Crear entrada 012101B con info de 012100 y cantidad 1.
			// Crear entrada 012101A con info de 012103 y cantidad 1.
			cardEntryAux.code = "012101B";
			this.addCardEntry( cardEntryAux, entry012100, ref dbText );
			cardEntryAux.code = "012101A";
			this.addCardEntry( cardEntryAux, entry012103, ref dbText );
			// Crear entrada 012102B con info de 012099 y cantidad 2.
			// Crear entrada 012102A con info de 012102 y cantidad 2.
			cardEntryAux.code = "012102B";
			cardEntryAux.quantity = 2;
			this.addCardEntry( cardEntryAux, entry012099, ref dbText );
			cardEntryAux.code = "012102A";
			this.addCardEntry( cardEntryAux, entry012102, ref dbText );
			
			string entry765002 = Regex.Match( dbText, @"(\t""765002""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			dbText = Regex.Replace( dbText, @"(\t""765002""[^\}]+\},\n)", "" );
			string entry765003 = Regex.Match( dbText, @"(\t""765003""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			dbText = Regex.Replace( dbText, @"(\t""765003""[^\}]+\},\n)", "" );
			string entry765004 = Regex.Match( dbText, @"(\t""765004""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			dbText = Regex.Replace( dbText, @"(\t""765004""[^\}]+\},\n)", "" );
			string entry765005 = Regex.Match( dbText, @"(\t""765005""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			dbText = Regex.Replace( dbText, @"(\t""765005""[^\}]+\},\n)", "" );
			// Crear entrada 765002B con info de 765002 y cantidad 1.
			// Crear entrada 765002A con info de 765004 y cantidad 1.
			cardEntryAux.code = "765002B";
			cardEntryAux.quantity = 1;
			this.addCardEntry( cardEntryAux, entry765002, ref dbText );
			cardEntryAux.code = "765002A";
			this.addCardEntry( cardEntryAux, entry765004, ref dbText );
			// Crear entrada 765003B con info de 765002 y cantidad 1.
			// Crear entrada 765003A con info de 765005 y cantidad 1.
			cardEntryAux.code = "765003B";
			this.addCardEntry( cardEntryAux, entry765002, ref dbText );
			cardEntryAux.code = "765003A";
			this.addCardEntry( cardEntryAux, entry765005, ref dbText );
			// Crear entrada 765004B con info de 765003 y cantidad 1.
			// Crear entrada 765004A con info de 765004 y cantidad 1.
			cardEntryAux.code = "765004B";
			this.addCardEntry( cardEntryAux, entry765003, ref dbText );
			cardEntryAux.code = "765004A";
			this.addCardEntry( cardEntryAux, entry765004, ref dbText );
			// Crear entrada 765005B con info de 765003 y cantidad 1.
			// Crear entrada 765005A con info de 765005 y cantidad 1.
			cardEntryAux.code = "765005B";
			this.addCardEntry( cardEntryAux, entry765003, ref dbText );
			cardEntryAux.code = "765005A";
			this.addCardEntry( cardEntryAux, entry765005, ref dbText );
			
			// Temple of the Deceived.
			cardEntryAux.code = "012068B";
			cardEntryAux.threatStrength = 4;
			cardEntryAux.questPoints = 0;
			cardEntryAux.name = "Temple of the Deceived";
			cardEntryAux.quantity = -1;
			string entry012068 = Regex.Match( dbText, @"(\t""012068A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry012068, ref dbText );
			//
			cardEntryAux.code = "012069B";
			string entry012069 = Regex.Match( dbText, @"(\t""012069A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry012069, ref dbText );
			//
			// Lost Island.
			cardEntryAux.code = "012072B";
			cardEntryAux.threatStrength = 2;
			cardEntryAux.questPoints = 0;
			cardEntryAux.name = "Lost Island";
			string entry012072 = Regex.Match( dbText, @"(\t""012072A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry012072, ref dbText );
			//
			cardEntryAux.code = "012073B";
			string entry012073 = Regex.Match( dbText, @"(\t""012073A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry012073, ref dbText );
			//
			cardEntryAux.code = "012074B";
			string entry012074 = Regex.Match( dbText, @"(\t""012074A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry012074, ref dbText );
			

			// Lost Island.
			cardEntryAux.code = "011027B";
			string entry011027 = Regex.Match( dbText, @"(\t""011027A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry011027, ref dbText );
			//
			cardEntryAux.code = "011030B";
			string entry011030 = Regex.Match( dbText, @"(\t""011030A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry011030, ref dbText );
			//
			cardEntryAux.code = "011031B";
			string entry011031 = Regex.Match( dbText, @"(\t""011031A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry011031, ref dbText );
			//
			cardEntryAux.code = "011072B";
			string entry011072 = Regex.Match( dbText, @"(\t""011072A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry011072, ref dbText );
			//
			cardEntryAux.code = "011073B";
			string entry011073 = Regex.Match( dbText, @"(\t""011073A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry011073, ref dbText );
			//
			cardEntryAux.code = "011074B";
			string entry011074 = Regex.Match( dbText, @"(\t""011074A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry011074, ref dbText );
			
			
			// Lost Island.
			cardEntryAux.code = "760002B";
			string entry760002 = Regex.Match( dbText, @"(\t""760002A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry760002, ref dbText );
			//
			cardEntryAux.code = "760003B";
			string entry760003 = Regex.Match( dbText, @"(\t""760003A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry760003, ref dbText );
			//
			cardEntryAux.code = "760004B";
			string entry760004 = Regex.Match( dbText, @"(\t""760004A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry760004, ref dbText );
			//
			cardEntryAux.code = "760005B";
			string entry760005 = Regex.Match( dbText, @"(\t""760005A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry760005, ref dbText );
			//
			cardEntryAux.code = "760006B";
			string entry760006 = Regex.Match( dbText, @"(\t""760006A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry760006, ref dbText );
			//
			cardEntryAux.code = "760007B";
			string entry760007 = Regex.Match( dbText, @"(\t""760007A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry760007, ref dbText );
			//
			cardEntryAux.code = "760008B";
			string entry760008 = Regex.Match( dbText, @"(\t""760008A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry760008, ref dbText );
			
	
			// Edge of the Temple.
			cardEntryAux.code = "764002B";
			cardEntryAux.threatStrength = 3;
			cardEntryAux.questPoints = 0;
			cardEntryAux.name = "Edge of the Temple";
			string entry764002 = Regex.Match( dbText, @"(\t""764002A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry764002, ref dbText );
			//
			cardEntryAux.code = "764003B";
			string entry764003 = Regex.Match( dbText, @"(\t""764003A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry764003, ref dbText );
			//
			cardEntryAux.code = "764004B";
			string entry764004 = Regex.Match( dbText, @"(\t""764004A""[^\}]+\}),\n", RegexOptions.None ).Groups[ 1 ].Value;
			this.addCardEntry( cardEntryAux, entry764004, ref dbText );
		}
		
		void addCardEntry( CardEntry cardEntry, string baseEntry, ref string dbText )
		{
			string entryAux = baseEntry;
			if ( cardEntry.quantity > -1 )
			{
				entryAux = Regex.Replace( entryAux, @"""quantity"": \d", @"""quantity"": " + cardEntry.quantity.ToString() );
			}
			if ( cardEntry.threatStrength > -1 )
			{
				entryAux = Regex.Replace( entryAux, @"""threat_strength"": \d", @"""threat_strength"": " + cardEntry.threatStrength.ToString() );
			}
			if ( cardEntry.questPoints > -1 )
			{
				entryAux = Regex.Replace( entryAux, @"""quest_points"": \d", @"""quest_points"": " + cardEntry.questPoints.ToString() );
			}
			if ( cardEntry.name != null )
			{
				entryAux = Regex.Replace( entryAux, @"""name"": ""[^""]+""", @"""name"": """ + cardEntry.name + @"""" );
			}
			if ( cardEntry.code != null )
			{
				entryAux = Regex.Replace( entryAux, @"\t""[^""]+"":", "\t\"" + cardEntry.code + "\":" );
				entryAux = Regex.Replace( entryAux, @"""code"": ""[^""]+""", @"""code"": """ + cardEntry.code + @"""" );
			}
			string side = cardEntry.code.Substring( 0, cardEntry.code.Length - 1 );
			if ( cardEntry.code.IndexOf( "A" ) != -1 )
			{
				side += "B";
			}
			else if ( cardEntry.code.IndexOf( "B" ) != -1 )
			{
				side += "A";
			}
			entryAux = Regex.Replace( entryAux, @"""side"": ""[^""]+""", @"""side"": """ + side + @"""" );
			string textureMapId = "";
			this._mapCardIdToTextureMapId.TryGetValue( cardEntry.code, out textureMapId );
			if ( Regex.IsMatch( entryAux, @"""texture_map_id""", RegexOptions.None ) )
			{
				entryAux = Regex.Replace( entryAux, @"""texture_map_id"": ""[^""]+""", @"""texture_map_id"": """ + textureMapId + @"""" );
			}
			else
			{
				entryAux += ",\n    \"texture_map_id\": \"" + textureMapId + "\"";
			}
			var dbEndGroup = Regex.Match( dbText, @"(\t\t})\n", RegexOptions.None ).Groups[ 1 ];
			dbText = dbText.Insert( dbEndGroup.Index + dbEndGroup.Length, ",\n" + entryAux );
		}
		
		void addCustomCardEntries( ref string dbText )
		{
			const string kCustomCardEntries = ""
				+ "\t\"999001\":\n"
				+ "\t\t{\n" 
				+ "    \"pack_code\": \"Custom\",\n"
				+ "    \"pack_name\": \"Custom Set\",\n"
				+ "    \"is_official\": false,\n"
				+ "    \"type_code\": \"enemy\",\n"
				+ "    \"type_name\": \"Enemy\",\n"
				+ "    \"sphere_code\": \"none\",\n"
				+ "    \"sphere_name\": \"None\",\n"
				+ "    \"encounter_set\": \"\",\n"
				+ "    \"position\": 1,\n"
				+ "    \"code\": \"999001\",\n"
				+ "    \"name\": \"Enemy\",\n"
				+ "    \"traits\": \"\",\n"
				+ "    \"text\": \"\",\n"
				+ "    \"flavor\": \"\",\n"
				+ "    \"is_unique\": false,\n"
				+ "    \"engagement_cost\": \"0\",\n"
				+ "    \"threat_strength\": 0,\n"
				+ "    \"attack\": 1,\n"
				+ "    \"defense\": 1,\n"
				+ "    \"health\": 1,\n"
				+ "    \"quantity\": -1,\n"
				+ "    \"deck_limit\": -1,\n"
				+ "    \"illustrator\": \"\",\n"
				+ "    \"octgnid\": \"\",\n"
				+ "    \"has_errata\": false,\n"
				+ "    \"url\": \"\",\n"
				+ "    \"imagesrc\": \"\",\n"
				+ "    \"side\": \"999003\",\n"
				+ "    \"texture_map_id\": \"LOTR999-0\"\n"
				+ "\t\t},\n"
				+ "\t\"999002\":\n"
				+ "\t\t{\n" 
				+ "    \"pack_code\": \"Custom\",\n"
				+ "    \"pack_name\": \"Custom Set\",\n"
				+ "    \"is_official\": false,\n"
				+ "    \"type_code\": \"player-back\",\n"
				+ "    \"type_name\": \"Player Back\",\n"
				+ "    \"sphere_code\": \"none\",\n"
				+ "    \"sphere_name\": \"None\",\n"
				+ "    \"encounter_set\": \"\",\n"
				+ "    \"position\": 2,\n"
				+ "    \"code\": \"999002\",\n"
				+ "    \"name\": \"Player Back\",\n"
				+ "    \"traits\": \"\",\n"
				+ "    \"text\": \"\",\n"
				+ "    \"flavor\": \"\",\n"
				+ "    \"is_unique\": false,\n"
				+ "    \"quantity\": -1,\n"
				+ "    \"deck_limit\": -1,\n"
				+ "    \"illustrator\": \"\",\n"
				+ "    \"octgnid\": \"\",\n"
				+ "    \"has_errata\": false,\n"
				+ "    \"url\": \"\",\n"
				+ "    \"imagesrc\": \"\",\n"
				+ "    \"side\": \"\",\n"
				+ "    \"texture_map_id\": \"LOTR999-0\"\n"
				+ "\t\t},\n"
				+ "\t\"999003\":\n"
				+ "\t\t{\n" 
				+ "    \"pack_code\": \"Custom\",\n"
				+ "    \"pack_name\": \"Custom Set\",\n"
				+ "    \"is_official\": false,\n"
				+ "    \"type_code\": \"encounter-back\",\n"
				+ "    \"type_name\": \"Encounter Back\",\n"
				+ "    \"sphere_code\": \"none\",\n"
				+ "    \"sphere_name\": \"None\",\n"
				+ "    \"encounter_set\": \"\",\n"
				+ "    \"position\": 3,\n"
				+ "    \"code\": \"999003\",\n"
				+ "    \"name\": \"Encounter Back\",\n"
				+ "    \"traits\": \"\",\n"
				+ "    \"text\": \"\",\n"
				+ "    \"flavor\": \"\",\n"
				+ "    \"is_unique\": false,\n"
				+ "    \"quantity\": -1,\n"
				+ "    \"deck_limit\": -1,\n"
				+ "    \"illustrator\": \"\",\n"
				+ "    \"octgnid\": \"\",\n"
				+ "    \"has_errata\": false,\n"
				+ "    \"url\": \"\",\n"
				+ "    \"imagesrc\": \"\",\n"
				+ "    \"side\": \"\",\n"
				+ "    \"texture_map_id\": \"LOTR999-0\"\n"
				+ "\t\t}";
			
			var dbEndGroup = Regex.Match( dbText, @"(\t\t})\n", RegexOptions.None ).Groups[ 1 ];
			dbText = dbText.Insert( dbEndGroup.Index + dbEndGroup.Length, ",\n" + kCustomCardEntries );
		}
		
		string findCardSide( string cardEntry, string code )
	    {
	        string result = null;
	        
	        // Boon.
	        this._mapBoonSide.TryGetValue( code, out result );
	        if ( result == null )
	        {
	        	// Burden.
	        	this._mapBurdenSide.TryGetValue( code, out result );
	        }
	        if ( result == null )
	        {
		        if ( code == "007024" || code == "007025" )
		        {
		        	// Mugash.
		        	result = "999002";
		        }
		        else if ( Regex.IsMatch( cardEntry, @"""text"": ""Encounter\.", RegexOptions.None ) )
		        {
		            // Encounter.
		        	result = "999003";
		        }
		        else if ( code.IndexOf( "A" ) != -1 ) 
		        {
		        	result = code.Substring( 0, code.Length - 1 ) + "B";
		        }
		        else if ( code.IndexOf( "B" ) != -1 )
		        {
		        	result = code.Substring( 0, code.Length - 1 ) + "A";
		        }
		        else 
		        {
		        	string typeCode = Regex.Match( cardEntry, @"""type_code"": ""([^""]+)"",\n", RegexOptions.None ).Groups[ 1 ].Value;
			        switch ( typeCode )
			        {
			            case "objective":
			            case "objective-ally":
			            case "objective-location":
			        	case "ship-objective":
			        	case "ship-enemy":
			            case "enemy":
			            case "treachery":
			            case "encounter-side-quest":
			            case "location": { result = "999003"; break; }
			            
			            default: { result = "999002"; break; }
			        }
		        }
	        }
	
	        return result;
	    }
		void FolderBrowserDialog1HelpRequest(object sender, EventArgs e)
		{
	
		}
	}
	
	public struct CardEntry
    {
		public string code;
        public string name;
        public int threatStrength;
        public int questPoints;
        public int quantity;
        
        public CardEntry( string code )
        {
        	this.code = code;
	        this.name = null;
	        this.threatStrength = -1;
	        this.questPoints = -1;
	        this.quantity = -1;
        }
    }
}
