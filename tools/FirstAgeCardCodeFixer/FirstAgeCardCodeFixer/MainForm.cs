﻿/*
 * Created by SharpDevelop.
 * User: usuario
 * Date: 16/10/2019
 * Time: 10:37
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Json;

namespace FirstAgeCardCodeFixer
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
		}
		
		void SelectFileBtnClick(object sender, EventArgs e)
		{
			this.openFileDialog1.ShowDialog();
		}
		
		
		void SelectDirectoryBtnClick(object sender, EventArgs e)
		{
			this.folderBrowserDialog1.ShowDialog();
		}
		
		void ParseBtnClick(object sender, EventArgs e)
		{
			this.fix();
		}
		
		void OpenFileDialog1FileOk(object sender, System.ComponentModel.CancelEventArgs e)
		{
			this.selectedFileLbl.Text = openFileDialog1.FileName;
			        	
			this.fixBtn.Enabled = true;
		}
		
		void fix()
		{
			string text = File.ReadAllText( openFileDialog1.FileName );

			string[] lines = Regex.Split( text, "\n   {\n" );
			for ( int i = 1; i < lines.Length; ++i )
			{
				string cardEntry = lines[ i ].Substring( 0, Regex.Match( lines[ i ], "\n   }" ).Index );
				
				string cardName = Regex.Match( cardEntry, @"""name"":""([^""]+)""", RegexOptions.None ).Groups[ 1 ].Value;
				cardName = cardName.Replace( " ", "-" );
				string cardCode = Regex.Match( cardEntry, @"""code"":""(\d+)""", RegexOptions.None ).Groups[ 1 ].Value;
				
				string oldPath = this.folderBrowserDialog1.SelectedPath + "/" + cardName + "-Front-Face.png";
				if ( File.Exists( oldPath ) )
				{
					File.Move( oldPath, this.folderBrowserDialog1.SelectedPath + "/" + cardCode + ".png" );
				}
			}
		}
	}
}
