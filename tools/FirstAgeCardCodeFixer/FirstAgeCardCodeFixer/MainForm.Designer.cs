﻿/*
 * Created by SharpDevelop.
 * User: usuario
 * Date: 16/10/2019
 * Time: 10:37
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace FirstAgeCardCodeFixer
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Button fixBtn;
		private System.Windows.Forms.Button selectFileBtn;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Label selectedFileLbl;
		private System.Windows.Forms.Button selectDirectoryBtn;
		private System.Windows.Forms.Label selectDirectoryLbl;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.fixBtn = new System.Windows.Forms.Button();
			this.selectFileBtn = new System.Windows.Forms.Button();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.selectedFileLbl = new System.Windows.Forms.Label();
			this.selectDirectoryBtn = new System.Windows.Forms.Button();
			this.selectDirectoryLbl = new System.Windows.Forms.Label();
			this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
			this.SuspendLayout();
			// 
			// fixBtn
			// 
			this.fixBtn.Location = new System.Drawing.Point(149, 257);
			this.fixBtn.Margin = new System.Windows.Forms.Padding(2);
			this.fixBtn.Name = "fixBtn";
			this.fixBtn.Size = new System.Drawing.Size(205, 64);
			this.fixBtn.TabIndex = 0;
			this.fixBtn.Text = "Fix";
			this.fixBtn.UseVisualStyleBackColor = true;
			this.fixBtn.Click += new System.EventHandler(this.ParseBtnClick);
			// 
			// selectFileBtn
			// 
			this.selectFileBtn.Location = new System.Drawing.Point(46, 32);
			this.selectFileBtn.Margin = new System.Windows.Forms.Padding(2);
			this.selectFileBtn.Name = "selectFileBtn";
			this.selectFileBtn.Size = new System.Drawing.Size(205, 64);
			this.selectFileBtn.TabIndex = 1;
			this.selectFileBtn.Text = "Select file";
			this.selectFileBtn.UseVisualStyleBackColor = true;
			this.selectFileBtn.Click += new System.EventHandler(this.SelectFileBtnClick);
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "openFileDialog1";
			this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.OpenFileDialog1FileOk);
			// 
			// selectedFileLbl
			// 
			this.selectedFileLbl.Location = new System.Drawing.Point(290, 52);
			this.selectedFileLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.selectedFileLbl.Name = "selectedFileLbl";
			this.selectedFileLbl.Size = new System.Drawing.Size(212, 19);
			this.selectedFileLbl.TabIndex = 3;
			this.selectedFileLbl.Text = "No file selected";
			// 
			// selectDirectoryBtn
			// 
			this.selectDirectoryBtn.Location = new System.Drawing.Point(46, 125);
			this.selectDirectoryBtn.Margin = new System.Windows.Forms.Padding(2);
			this.selectDirectoryBtn.Name = "selectDirectoryBtn";
			this.selectDirectoryBtn.Size = new System.Drawing.Size(205, 64);
			this.selectDirectoryBtn.TabIndex = 4;
			this.selectDirectoryBtn.Text = "Select directory";
			this.selectDirectoryBtn.UseVisualStyleBackColor = true;
			this.selectDirectoryBtn.Click += new System.EventHandler(this.SelectDirectoryBtnClick);
			// 
			// selectDirectoryLbl
			// 
			this.selectDirectoryLbl.Location = new System.Drawing.Point(290, 147);
			this.selectDirectoryLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.selectDirectoryLbl.Name = "selectDirectoryLbl";
			this.selectDirectoryLbl.Size = new System.Drawing.Size(212, 19);
			this.selectDirectoryLbl.TabIndex = 5;
			this.selectDirectoryLbl.Text = "No directory selected";
			// 
			// folderBrowserDialog1
			// 
			this.folderBrowserDialog1.ShowNewFolderButton = false;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.ClientSize = new System.Drawing.Size(557, 367);
			this.Controls.Add(this.selectDirectoryLbl);
			this.Controls.Add(this.selectDirectoryBtn);
			this.Controls.Add(this.selectedFileLbl);
			this.Controls.Add(this.selectFileBtn);
			this.Controls.Add(this.fixBtn);
			this.Margin = new System.Windows.Forms.Padding(2);
			this.Name = "MainForm";
			this.Text = "L5rFirstAgeCardCodeFixer";
			this.ResumeLayout(false);

		}
	}
}
